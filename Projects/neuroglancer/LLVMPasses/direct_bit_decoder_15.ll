; ModuleID = './draco/src/draco/compression/bit_coders/direct_bit_decoder.cc'
source_filename = "./draco/src/draco/compression/bit_coders/direct_bit_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::DirectBitDecoder" = type { %"class.std::__2::vector", %"class.std::__2::__wrap_iter", i32 }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i32*, i32*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32* }
%"class.std::__2::__wrap_iter" = type { i32* }
%"class.std::__2::__wrap_iter.1" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.2" }
%"class.std::__2::__compressed_pair.2" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::allocator"* }
%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i32*, i32* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction" = type { i32*, i32*, i32** }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.4" = type { i8 }

$_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv = comdat any

$_ZNSt3__211__wrap_iterIPKjEC2IPjEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev = comdat any

$_ZN5draco13DecoderBuffer6DecodeIjEEbPT_ = comdat any

$_ZNK5draco13DecoderBuffer14remaining_sizeEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm = comdat any

$_ZN5draco13DecoderBuffer6DecodeEPvm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZNKSt3__211__wrap_iterIPjE4baseEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj = comdat any

$_ZNSt3__211__wrap_iterIPjEC2ES1_ = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIjEC2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIjEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIjE7destroyEPj = comdat any

$_ZNSt3__29allocatorIjE10deallocateEPjm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZN5draco13DecoderBuffer4PeekIjEEbPT_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE8__appendEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIjE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIjE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionC2EPPjm = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE27__invalidate_iterators_pastEPj = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco16DirectBitDecoderC1Ev = hidden unnamed_addr alias %"class.draco::DirectBitDecoder"* (%"class.draco::DirectBitDecoder"*), %"class.draco::DirectBitDecoder"* (%"class.draco::DirectBitDecoder"*)* @_ZN5draco16DirectBitDecoderC2Ev
@_ZN5draco16DirectBitDecoderD1Ev = hidden unnamed_addr alias %"class.draco::DirectBitDecoder"* (%"class.draco::DirectBitDecoder"*), %"class.draco::DirectBitDecoder"* (%"class.draco::DirectBitDecoder"*)* @_ZN5draco16DirectBitDecoderD2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC2Ev(%"class.draco::DirectBitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitDecoder"*, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.1", align 4
  store %"class.draco::DirectBitDecoder"* %this, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DirectBitDecoder"*, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  %bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %bits_) #6
  %pos_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %bits_2 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call3 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %bits_2) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %ref.tmp, i32 0, i32 0
  store i32* %call3, i32** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEC2IPjEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %pos_, %"class.std::__2::__wrap_iter.1"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #6
  %num_used_bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %num_used_bits_, align 4
  ret %"class.draco::DirectBitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* %0) #6
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.1", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %call = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this1, i32* %1) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEC2IPjEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::__wrap_iter.1"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.1"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.1"* %__u, %"class.std::__2::__wrap_iter.1"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.1"*, %"class.std::__2::__wrap_iter.1"** %__u.addr, align 4
  %call = call i32* @_ZNKSt3__211__wrap_iterIPjE4baseEv(%"class.std::__2::__wrap_iter.1"* %1) #6
  store i32* %call, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderD2Ev(%"class.draco::DirectBitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitDecoder"*, align 4
  store %"class.draco::DirectBitDecoder"* %this, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DirectBitDecoder"*, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  call void @_ZN5draco16DirectBitDecoder5ClearEv(%"class.draco::DirectBitDecoder"* %this1)
  %bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %bits_) #6
  ret %"class.draco::DirectBitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco16DirectBitDecoder5ClearEv(%"class.draco::DirectBitDecoder"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitDecoder"*, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp2 = alloca %"class.std::__2::__wrap_iter.1", align 4
  store %"class.draco::DirectBitDecoder"* %this, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DirectBitDecoder"*, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  %bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::vector"* %bits_) #6
  %num_used_bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %num_used_bits_, align 4
  %bits_3 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %bits_3) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %ref.tmp2, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEC2IPjEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %ref.tmp, %"class.std::__2::__wrap_iter.1"* nonnull align 4 dereferenceable(4) %ref.tmp2, i8* null) #6
  %pos_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %0 = bitcast %"class.std::__2::__wrap_iter"* %pos_ to i8*
  %1 = bitcast %"class.std::__2::__wrap_iter"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* %0) #6
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco16DirectBitDecoder13StartDecodingEPNS_13DecoderBufferE(%"class.draco::DirectBitDecoder"* %this, %"class.draco::DecoderBuffer"* %source_buffer) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DirectBitDecoder"*, align 4
  %source_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %size_in_bytes = alloca i32, align 4
  %num_32bit_elements = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp13 = alloca %"class.std::__2::__wrap_iter.1", align 4
  store %"class.draco::DirectBitDecoder"* %this, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %source_buffer, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %this1 = load %"class.draco::DirectBitDecoder"*, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  call void @_ZN5draco16DirectBitDecoder5ClearEv(%"class.draco::DirectBitDecoder"* %this1)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %0, i32* %size_in_bytes)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %size_in_bytes, align 4
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %size_in_bytes, align 4
  %and = and i32 %2, 3
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then2, label %if.end3

if.then2:                                         ; preds = %lor.lhs.false, %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end3:                                          ; preds = %lor.lhs.false
  %3 = load i32, i32* %size_in_bytes, align 4
  %conv = zext i32 %3 to i64
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call4 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp5 = icmp sgt i64 %conv, %call4
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end3
  store i1 false, i1* %retval, align 1
  br label %return

if.end7:                                          ; preds = %if.end3
  %5 = load i32, i32* %size_in_bytes, align 4
  %div = udiv i32 %5, 4
  store i32 %div, i32* %num_32bit_elements, align 4
  %bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %6 = load i32, i32* %num_32bit_elements, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %bits_, i32 %6)
  %7 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %bits_8 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call9 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %bits_8) #6
  %8 = bitcast i32* %call9 to i8*
  %9 = load i32, i32* %size_in_bytes, align 4
  %call10 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeEPvm(%"class.draco::DecoderBuffer"* %7, i8* %8, i32 %9)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.end7
  %bits_14 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call15 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv(%"class.std::__2::vector"* %bits_14) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %ref.tmp13, i32 0, i32 0
  store i32* %call15, i32** %coerce.dive, align 4
  %call16 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEC2IPjEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %ref.tmp, %"class.std::__2::__wrap_iter.1"* nonnull align 4 dereferenceable(4) %ref.tmp13, i8* null) #6
  %pos_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %10 = bitcast %"class.std::__2::__wrap_iter"* %pos_ to i8*
  %11 = bitcast %"class.std::__2::__wrap_iter"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 4, i1 false)
  %num_used_bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %num_used_bits_, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end12, %if.then11, %if.then6, %if.then2, %if.then
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i32*, i32** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this1, i32* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %sub = sub nsw i64 %0, %1
  ret i64 %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE8__appendEm(%"class.std::__2::vector"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load i32*, i32** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %7, i32 %8
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::vector"* %this1, i32* %add.ptr) #6
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeEPvm(%"class.draco::DecoderBuffer"* %this, i8* %out_data, i32 %size_to_decode) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_data.addr = alloca i8*, align 4
  %size_to_decode.addr = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_data, i8** %out_data.addr, align 4
  store i32 %size_to_decode, i32* %size_to_decode.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %2 = load i32, i32* %size_to_decode.addr, align 4
  %conv = zext i32 %2 to i64
  %add = add nsw i64 %1, %conv
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** %out_data.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  %6 = load i32, i32* %size_to_decode.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %3, i8* align 1 %add.ptr, i32 %6, i1 false)
  %7 = load i32, i32* %size_to_decode.addr, align 4
  %conv3 = zext i32 %7 to i64
  %pos_4 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %8 = load i64, i64* %pos_4, align 8
  %add5 = add nsw i64 %8, %conv3
  store i64 %add5, i64* %pos_4, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #6
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.1", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this1, i32* %1) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %0) #6
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %1) #6
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__211__wrap_iterIPjE4baseEv(%"class.std::__2::__wrap_iter.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.1"*, align 4
  store %"class.std::__2::__wrap_iter.1"* %this, %"class.std::__2::__wrap_iter.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.1"*, %"class.std::__2::__wrap_iter.1"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this, i32* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.1", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.1"* @_ZNSt3__211__wrap_iterIPjEC2ES1_(%"class.std::__2::__wrap_iter.1"* %retval, i32* %0) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %retval, i32 0, i32 0
  %1 = load i32*, i32** %coerce.dive, align 4
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.1"* @_ZNSt3__211__wrap_iterIPjEC2ES1_(%"class.std::__2::__wrap_iter.1"* returned %this, i32* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.1"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::__wrap_iter.1"* %this, %"class.std::__2::__wrap_iter.1"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.1"*, %"class.std::__2::__wrap_iter.1"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.1", %"class.std::__2::__wrap_iter.1"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__x.addr, align 4
  store i32* %0, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #6
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* %1) #6
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call8 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #6
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #6
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #6
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #6
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this1, i32* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #6
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 4, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = bitcast i32* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 1 %add.ptr, i32 4, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE8__appendEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %0) #6
  %1 = load i32*, i32** %call, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm(%"class.std::__2::vector"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %6) #6
  store %"class.std::__2::allocator"* %call2, %"class.std::__2::allocator"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %8 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::vector"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE27__invalidate_iterators_pastEPj(%"class.std::__2::vector"* %this1, i32* %0)
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %2 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %1, i32* %2) #6
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %3) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i32*, i32** %__new_end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %3) #6
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i32*, i32** %__pos_3, align 4
  %call4 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %4) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #8
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.2"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.2"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #6
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionC2EPPjm(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32** %__end_, i32 %0) #6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load i32*, i32** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load i32*, i32** %__end_2, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load i32*, i32** %__pos_4, align 4
  %call5 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %3) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3, i32* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load i32*, i32** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %4, i32 1
  store i32* %incdec.ptr, i32** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #6
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #6
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #6
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #6
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #6
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #6
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #6
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  store i32 0, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #6
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #6
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #6
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.2"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.2"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.3"*
  %5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %5) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.3"* %4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.2"* %__end_cap_) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.2"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.3"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) #6
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this1) #6
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #4 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #8
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #3

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %1) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__value_, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionC2EPPjm(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* returned %this, i32** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca i32**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  store i32** %__p, i32*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32**, i32*** %__p.addr, align 4
  %1 = load i32*, i32** %0, align 4
  store i32* %1, i32** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load i32**, i32*** %__p.addr, align 4
  %3 = load i32*, i32** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %3, i32 %4
  store i32* %add.ptr, i32** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load i32**, i32*** %__p.addr, align 4
  store i32** %5, i32*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load i32**, i32*** %__dest_, align 4
  store i32* %0, i32** %1, align 4
  ret %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #6
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #6
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #6
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.4", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.4", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.2"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE27__invalidate_iterators_pastEPj(%"class.std::__2::vector"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i32, i32* %call4, i32 %2
  %3 = bitcast i32* %add.ptr5 to i8*
  %call6 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call7 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr8 = getelementptr inbounds i32, i32* %call6, i32 %call7
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #6
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { builtin nounwind }
attributes #8 = { noreturn }
attributes #9 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
