; ModuleID = './draco/src/draco/compression/entropy/symbol_decoding.cc'
source_filename = "./draco/src/draco/compression/entropy/symbol_decoding.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::RAnsSymbolDecoder" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i32*, i32*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32* }
%"class.draco::RAnsDecoder" = type { %"class.std::__2::vector", %"class.std::__2::vector.1", %"struct.draco::AnsDecoder" }
%"class.std::__2::vector.1" = type { %"class.std::__2::__vector_base.2" }
%"class.std::__2::__vector_base.2" = type { %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"*, %"class.std::__2::__compressed_pair.3" }
%"struct.draco::rans_sym" = type { i32, i32 }
%"class.std::__2::__compressed_pair.3" = type { %"struct.std::__2::__compressed_pair_elem.4" }
%"struct.std::__2::__compressed_pair_elem.4" = type { %"struct.draco::rans_sym"* }
%"struct.draco::AnsDecoder" = type { i8*, i32, i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.5" = type { i8 }
%"class.std::__2::allocator.6" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.8" }
%"class.std::__2::__compressed_pair.8" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.9" }
%"struct.std::__2::__compressed_pair_elem.9" = type { %"class.std::__2::allocator"* }
%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i32*, i32* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction" = type { i32*, i32*, i32** }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.10" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__split_buffer.11" = type { %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"*, %"class.std::__2::__compressed_pair.12" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.13" }
%"struct.std::__2::__compressed_pair_elem.13" = type { %"class.std::__2::allocator.6"* }
%"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction" = type { %"class.std::__2::vector.1"*, %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"* }
%"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction" = type { %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** }
%"struct.std::__2::__has_construct.14" = type { i8 }
%"struct.std::__2::__has_max_size.15" = type { i8 }
%"struct.std::__2::__has_destroy.16" = type { i8 }
%"struct.draco::rans_dec_sym" = type { i32, i32, i32 }
%"class.draco::RAnsSymbolDecoder.17" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.draco::RAnsSymbolDecoder.18" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.draco::RAnsSymbolDecoder.19" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.draco::RAnsSymbolDecoder.20" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.draco::RAnsSymbolDecoder.21" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.draco::RAnsSymbolDecoder.22" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.draco::RAnsSymbolDecoder.23" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder" }
%"class.draco::RAnsSymbolDecoder.24" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.25" }
%"class.draco::RAnsDecoder.25" = type { %"class.std::__2::vector", %"class.std::__2::vector.1", %"struct.draco::AnsDecoder" }
%"class.draco::RAnsSymbolDecoder.26" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.27" }
%"class.draco::RAnsDecoder.27" = type { %"class.std::__2::vector", %"class.std::__2::vector.1", %"struct.draco::AnsDecoder" }
%"class.draco::RAnsSymbolDecoder.28" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.29" }
%"class.draco::RAnsDecoder.29" = type { %"class.std::__2::vector", %"class.std::__2::vector.1", %"struct.draco::AnsDecoder" }
%"class.draco::RAnsSymbolDecoder.30" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.31" }
%"class.draco::RAnsDecoder.31" = type { %"class.std::__2::vector", %"class.std::__2::vector.1", %"struct.draco::AnsDecoder" }
%"class.draco::RAnsSymbolDecoder.32" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.33" }
%"class.draco::RAnsDecoder.33" = type { %"class.std::__2::vector", %"class.std::__2::vector.1", %"struct.draco::AnsDecoder" }
%"class.draco::RAnsSymbolDecoder.34" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.35" }
%"class.draco::RAnsDecoder.35" = type { %"class.std::__2::vector", %"class.std::__2::vector.1", %"struct.draco::AnsDecoder" }
%"class.draco::RAnsSymbolDecoder.36" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.35" }
%"class.draco::RAnsSymbolDecoder.37" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.35" }
%"class.draco::RAnsSymbolDecoder.38" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.35" }
%"class.draco::RAnsSymbolDecoder.39" = type { %"class.std::__2::vector", i32, %"class.draco::RAnsDecoder.35" }

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZN5draco19DecodeTaggedSymbolsINS_17RAnsSymbolDecoderEEEbjiPNS_13DecoderBufferEPj = comdat any

$_ZN5draco16DecodeRawSymbolsINS_17RAnsSymbolDecoderEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

$_ZN5draco17RAnsSymbolDecoderILi5EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi5EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi5EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi5EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi5EE12DecodeSymbolEv = comdat any

$_ZN5draco13DecoderBuffer28DecodeLeastSignificantBits32EiPj = comdat any

$_ZN5draco17RAnsSymbolDecoderILi5EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi5EED2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZN5draco11RAnsDecoderILi12EEC2Ev = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIjEC2Ev = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev = comdat any

$_ZN5draco10AnsDecoderC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco8rans_symEEC2Ev = comdat any

$_ZNK5draco13DecoderBuffer17bitstream_versionEv = comdat any

$_ZN5draco13DecoderBuffer6DecodeIjEEbPT_ = comdat any

$_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm = comdat any

$_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj = comdat any

$_ZN5draco13DecoderBuffer4PeekIjEEbPT_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE8__appendEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIjEEPT_S2_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIjE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIjE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionC2EPPjm = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIjE7destroyEPj = comdat any

$_ZNSt3__29allocatorIjE10deallocateEPjm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE27__invalidate_iterators_pastEPj = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8__appendEm = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__destruct_at_endEPS2_ = comdat any

$_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE18__construct_at_endEm = comdat any

$_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEEC2EmmS5_ = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE21_ConstructTransactionC2ERS5_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE9constructIS3_JEEEvRS4_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIN5draco8rans_symEEEPT_S4_ = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE11__constructIS3_JEEEvNS_17integral_constantIbLb1EEERS4_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco8rans_symEE9constructIS2_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE8max_sizeERKS4_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS4_ = comdat any

$_ZNKSt3__29allocatorIN5draco8rans_symEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEEC2IDnS6_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE8allocateERS4_m = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco8rans_symEEEEEOT_RNS_16remove_referenceIS6_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco8rans_symEEELi1ELb0EEC2IS5_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco8rans_symEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco8rans_symEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE21_ConstructTransactionC2EPPS2_m = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE46__construct_backward_with_exception_guaranteesIS3_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS4_PT_SA_EE5valuesr31is_trivially_move_constructibleISA_EE5valueEvE4typeERS4_SB_SB_RSB_ = comdat any

$_ZNSt3__24swapIPN5draco8rans_symEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS5_EE5valueEvE4typeERS5_S8_ = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_ = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPN5draco8rans_symEEEONS_16remove_referenceIT_E4typeEOS6_ = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE10deallocateERS4_PS3_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE17__destruct_at_endEPS2_ = comdat any

$_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE17__destruct_at_endEPS2_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE7destroyIS3_EEvRS4_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE9__destroyIS3_EEvNS_17integral_constantIbLb1EEERS4_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco8rans_symEE7destroyEPS2_ = comdat any

$_ZNSt3__29allocatorIN5draco8rans_symEE10deallocateEPS2_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE27__invalidate_iterators_pastEPS2_ = comdat any

$_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE17__destruct_at_endEPS2_ = comdat any

$_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco13DecoderBuffer6DecodeIyEEbPT_ = comdat any

$_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE = comdat any

$_ZNK5draco13DecoderBuffer14remaining_sizeEv = comdat any

$_ZNK5draco13DecoderBuffer9data_headEv = comdat any

$_ZN5draco13DecoderBuffer7AdvanceEx = comdat any

$_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi = comdat any

$_ZN5draco13DecoderBuffer4PeekIyEEbPT_ = comdat any

$_ZN5draco11RAnsDecoderILi12EE9rans_readEv = comdat any

$_ZN5draco11RAnsDecoderILi12EE9fetch_symEPNS_12rans_dec_symEj = comdat any

$_ZNK5draco13DecoderBuffer18bit_decoder_activeEv = comdat any

$_ZN5draco13DecoderBuffer10BitDecoder7GetBitsEiPj = comdat any

$_ZN5draco13DecoderBuffer10BitDecoder6GetBitEv = comdat any

$_ZN5draco11RAnsDecoderILi12EE8read_endEv = comdat any

$_ZN5draco11RAnsDecoderILi12EED2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE5clearEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi1EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi2EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi3EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi4EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi5EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi6EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi7EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi8EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi9EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi10EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi11EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi12EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi13EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi14EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi15EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi16EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi17EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi18EEEEEbjPNS_13DecoderBufferEPj = comdat any

$_ZN5draco17RAnsSymbolDecoderILi1EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi1EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi1EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi1EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi1EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi1EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi1EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi2EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi2EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi2EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi2EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi2EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi2EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi2EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi3EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi3EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi3EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi3EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi3EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi3EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi3EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi4EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi4EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi4EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi4EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi4EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi4EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi4EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi6EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi6EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi6EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi6EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi6EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi6EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi6EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi7EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi7EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi7EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi7EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi7EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi7EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi7EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi8EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi8EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi8EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi8EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi8EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi8EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi8EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi9EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi9EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi9EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi9EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi9EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi9EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi9EED2Ev = comdat any

$_ZN5draco11RAnsDecoderILi13EEC2Ev = comdat any

$_ZN5draco11RAnsDecoderILi13EE24rans_build_look_up_tableEPKjj = comdat any

$_ZN5draco11RAnsDecoderILi13EE9read_initEPKhi = comdat any

$_ZN5draco11RAnsDecoderILi13EE9rans_readEv = comdat any

$_ZN5draco11RAnsDecoderILi13EE9fetch_symEPNS_12rans_dec_symEj = comdat any

$_ZN5draco11RAnsDecoderILi13EE8read_endEv = comdat any

$_ZN5draco11RAnsDecoderILi13EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi10EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi10EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi10EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi10EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi10EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi10EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi10EED2Ev = comdat any

$_ZN5draco11RAnsDecoderILi15EEC2Ev = comdat any

$_ZN5draco11RAnsDecoderILi15EE24rans_build_look_up_tableEPKjj = comdat any

$_ZN5draco11RAnsDecoderILi15EE9read_initEPKhi = comdat any

$_ZN5draco11RAnsDecoderILi15EE9rans_readEv = comdat any

$_ZN5draco11RAnsDecoderILi15EE9fetch_symEPNS_12rans_dec_symEj = comdat any

$_ZN5draco11RAnsDecoderILi15EE8read_endEv = comdat any

$_ZN5draco11RAnsDecoderILi15EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi11EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi11EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi11EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi11EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi11EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi11EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi11EED2Ev = comdat any

$_ZN5draco11RAnsDecoderILi16EEC2Ev = comdat any

$_ZN5draco11RAnsDecoderILi16EE24rans_build_look_up_tableEPKjj = comdat any

$_ZN5draco11RAnsDecoderILi16EE9read_initEPKhi = comdat any

$_ZN5draco11RAnsDecoderILi16EE9rans_readEv = comdat any

$_ZN5draco11RAnsDecoderILi16EE9fetch_symEPNS_12rans_dec_symEj = comdat any

$_ZN5draco11RAnsDecoderILi16EE8read_endEv = comdat any

$_ZN5draco11RAnsDecoderILi16EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi12EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi12EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi12EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi12EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi12EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi12EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi12EED2Ev = comdat any

$_ZN5draco11RAnsDecoderILi18EEC2Ev = comdat any

$_ZN5draco11RAnsDecoderILi18EE24rans_build_look_up_tableEPKjj = comdat any

$_ZN5draco11RAnsDecoderILi18EE9read_initEPKhi = comdat any

$_ZN5draco11RAnsDecoderILi18EE9rans_readEv = comdat any

$_ZN5draco11RAnsDecoderILi18EE9fetch_symEPNS_12rans_dec_symEj = comdat any

$_ZN5draco11RAnsDecoderILi18EE8read_endEv = comdat any

$_ZN5draco11RAnsDecoderILi18EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi13EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi13EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi13EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi13EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi13EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi13EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi13EED2Ev = comdat any

$_ZN5draco11RAnsDecoderILi19EEC2Ev = comdat any

$_ZN5draco11RAnsDecoderILi19EE24rans_build_look_up_tableEPKjj = comdat any

$_ZN5draco11RAnsDecoderILi19EE9read_initEPKhi = comdat any

$_ZN5draco11RAnsDecoderILi19EE9rans_readEv = comdat any

$_ZN5draco11RAnsDecoderILi19EE9fetch_symEPNS_12rans_dec_symEj = comdat any

$_ZN5draco11RAnsDecoderILi19EE8read_endEv = comdat any

$_ZN5draco11RAnsDecoderILi19EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi14EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi14EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi14EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi14EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi14EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi14EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi14EED2Ev = comdat any

$_ZN5draco11RAnsDecoderILi20EEC2Ev = comdat any

$_ZN5draco11RAnsDecoderILi20EE24rans_build_look_up_tableEPKjj = comdat any

$_ZN5draco11RAnsDecoderILi20EE9read_initEPKhi = comdat any

$_ZN5draco11RAnsDecoderILi20EE9rans_readEv = comdat any

$_ZN5draco11RAnsDecoderILi20EE9fetch_symEPNS_12rans_dec_symEj = comdat any

$_ZN5draco11RAnsDecoderILi20EE8read_endEv = comdat any

$_ZN5draco11RAnsDecoderILi20EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi15EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi15EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi15EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi15EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi15EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi15EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi15EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi16EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi16EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi16EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi16EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi16EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi16EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi16EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi17EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi17EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi17EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi17EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi17EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi17EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi17EED2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi18EEC2Ev = comdat any

$_ZN5draco17RAnsSymbolDecoderILi18EE6CreateEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17RAnsSymbolDecoderILi18EE11num_symbolsEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi18EE13StartDecodingEPNS_13DecoderBufferE = comdat any

$_ZN5draco17RAnsSymbolDecoderILi18EE12DecodeSymbolEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi18EE11EndDecodingEv = comdat any

$_ZN5draco17RAnsSymbolDecoderILi18EED2Ev = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco13DecodeSymbolsEjiPNS_13DecoderBufferEPj(i32 %num_values, i32 %num_components, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %num_components.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %scheme = alloca i8, align 1
  store i32 %num_values, i32* %num_values.addr, align 4
  store i32 %num_components, i32* %num_components.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %0 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %1, i8* %scheme)
  br i1 %call, label %if.end2, label %if.then1

if.then1:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end2:                                          ; preds = %if.end
  %2 = load i8, i8* %scheme, align 1
  %conv = zext i8 %2 to i32
  %cmp3 = icmp eq i32 %conv, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end2
  %3 = load i32, i32* %num_values.addr, align 4
  %4 = load i32, i32* %num_components.addr, align 4
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %call5 = call zeroext i1 @_ZN5draco19DecodeTaggedSymbolsINS_17RAnsSymbolDecoderEEEbjiPNS_13DecoderBufferEPj(i32 %3, i32 %4, %"class.draco::DecoderBuffer"* %5, i32* %6)
  store i1 %call5, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %if.end2
  %7 = load i8, i8* %scheme, align 1
  %conv6 = zext i8 %7 to i32
  %cmp7 = icmp eq i32 %conv6, 1
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.else
  %8 = load i32, i32* %num_values.addr, align 4
  %9 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %10 = load i32*, i32** %out_values.addr, align 4
  %call9 = call zeroext i1 @_ZN5draco16DecodeRawSymbolsINS_17RAnsSymbolDecoderEEEbjPNS_13DecoderBufferEPj(i32 %8, %"class.draco::DecoderBuffer"* %9, i32* %10)
  store i1 %call9, i1* %retval, align 1
  br label %return

if.end10:                                         ; preds = %if.else
  br label %if.end11

if.end11:                                         ; preds = %if.end10
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end11, %if.then8, %if.then4, %if.then1, %if.then
  %11 = load i1, i1* %retval, align 1
  ret i1 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco19DecodeTaggedSymbolsINS_17RAnsSymbolDecoderEEEbjiPNS_13DecoderBufferEPj(i32 %num_values, i32 %num_components, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %num_components.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %tag_decoder = alloca %"class.draco::RAnsSymbolDecoder", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %value_id = alloca i32, align 4
  %i = alloca i32, align 4
  %bit_length = alloca i32, align 4
  %j = alloca i32, align 4
  %val = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store i32 %num_components, i32* %num_components.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder"* @_ZN5draco17RAnsSymbolDecoderILi5EEC2Ev(%"class.draco::RAnsSymbolDecoder"* %tag_decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi5EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder"* %tag_decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi5EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder"* %tag_decoder, %"class.draco::DecoderBuffer"* %1)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %if.end
  %2 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %land.lhs.true, label %if.end8

land.lhs.true:                                    ; preds = %if.end4
  %call5 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi5EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder"* %tag_decoder)
  %cmp6 = icmp eq i32 %call5, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %land.lhs.true, %if.end4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call9 = call zeroext i1 @_ZN5draco13DecoderBuffer16StartBitDecodingEbPy(%"class.draco::DecoderBuffer"* %3, i1 zeroext false, i64* null)
  store i32 0, i32* %value_id, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc19, %if.end8
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %num_values.addr, align 4
  %cmp10 = icmp ult i32 %4, %5
  br i1 %cmp10, label %for.body, label %for.end20

for.body:                                         ; preds = %for.cond
  %call11 = call i32 @_ZN5draco17RAnsSymbolDecoderILi5EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder"* %tag_decoder)
  store i32 %call11, i32* %bit_length, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body
  %6 = load i32, i32* %j, align 4
  %7 = load i32, i32* %num_components.addr, align 4
  %cmp13 = icmp slt i32 %6, %7
  br i1 %cmp13, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond12
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %9 = load i32, i32* %bit_length, align 4
  %call15 = call zeroext i1 @_ZN5draco13DecoderBuffer28DecodeLeastSignificantBits32EiPj(%"class.draco::DecoderBuffer"* %8, i32 %9, i32* %val)
  br i1 %call15, label %if.end17, label %if.then16

if.then16:                                        ; preds = %for.body14
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %for.body14
  %10 = load i32, i32* %val, align 4
  %11 = load i32*, i32** %out_values.addr, align 4
  %12 = load i32, i32* %value_id, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %value_id, align 4
  %arrayidx = getelementptr inbounds i32, i32* %11, i32 %12
  store i32 %10, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end17
  %13 = load i32, i32* %j, align 4
  %inc18 = add nsw i32 %13, 1
  store i32 %inc18, i32* %j, align 4
  br label %for.cond12

for.end:                                          ; preds = %for.cond12
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %14 = load i32, i32* %num_components.addr, align 4
  %15 = load i32, i32* %i, align 4
  %add = add i32 %15, %14
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end20:                                        ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi5EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder"* %tag_decoder)
  %16 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  call void @_ZN5draco13DecoderBuffer14EndBitDecodingEv(%"class.draco::DecoderBuffer"* %16)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end20, %if.then16, %if.then7, %if.then3, %if.then
  %call21 = call %"class.draco::RAnsSymbolDecoder"* @_ZN5draco17RAnsSymbolDecoderILi5EED2Ev(%"class.draco::RAnsSymbolDecoder"* %tag_decoder) #8
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco16DecodeRawSymbolsINS_17RAnsSymbolDecoderEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %max_bit_length = alloca i8, align 1
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %max_bit_length)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8, i8* %max_bit_length, align 1
  %conv = zext i8 %1 to i32
  switch i32 %conv, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb2
    i32 3, label %sw.bb4
    i32 4, label %sw.bb6
    i32 5, label %sw.bb8
    i32 6, label %sw.bb10
    i32 7, label %sw.bb12
    i32 8, label %sw.bb14
    i32 9, label %sw.bb16
    i32 10, label %sw.bb18
    i32 11, label %sw.bb20
    i32 12, label %sw.bb22
    i32 13, label %sw.bb24
    i32 14, label %sw.bb26
    i32 15, label %sw.bb28
    i32 16, label %sw.bb30
    i32 17, label %sw.bb32
    i32 18, label %sw.bb34
  ]

sw.bb:                                            ; preds = %if.end
  %2 = load i32, i32* %num_values.addr, align 4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %4 = load i32*, i32** %out_values.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi1EEEEEbjPNS_13DecoderBufferEPj(i32 %2, %"class.draco::DecoderBuffer"* %3, i32* %4)
  store i1 %call1, i1* %retval, align 1
  br label %return

sw.bb2:                                           ; preds = %if.end
  %5 = load i32, i32* %num_values.addr, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %7 = load i32*, i32** %out_values.addr, align 4
  %call3 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi2EEEEEbjPNS_13DecoderBufferEPj(i32 %5, %"class.draco::DecoderBuffer"* %6, i32* %7)
  store i1 %call3, i1* %retval, align 1
  br label %return

sw.bb4:                                           ; preds = %if.end
  %8 = load i32, i32* %num_values.addr, align 4
  %9 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %10 = load i32*, i32** %out_values.addr, align 4
  %call5 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi3EEEEEbjPNS_13DecoderBufferEPj(i32 %8, %"class.draco::DecoderBuffer"* %9, i32* %10)
  store i1 %call5, i1* %retval, align 1
  br label %return

sw.bb6:                                           ; preds = %if.end
  %11 = load i32, i32* %num_values.addr, align 4
  %12 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %13 = load i32*, i32** %out_values.addr, align 4
  %call7 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi4EEEEEbjPNS_13DecoderBufferEPj(i32 %11, %"class.draco::DecoderBuffer"* %12, i32* %13)
  store i1 %call7, i1* %retval, align 1
  br label %return

sw.bb8:                                           ; preds = %if.end
  %14 = load i32, i32* %num_values.addr, align 4
  %15 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %16 = load i32*, i32** %out_values.addr, align 4
  %call9 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi5EEEEEbjPNS_13DecoderBufferEPj(i32 %14, %"class.draco::DecoderBuffer"* %15, i32* %16)
  store i1 %call9, i1* %retval, align 1
  br label %return

sw.bb10:                                          ; preds = %if.end
  %17 = load i32, i32* %num_values.addr, align 4
  %18 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %19 = load i32*, i32** %out_values.addr, align 4
  %call11 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi6EEEEEbjPNS_13DecoderBufferEPj(i32 %17, %"class.draco::DecoderBuffer"* %18, i32* %19)
  store i1 %call11, i1* %retval, align 1
  br label %return

sw.bb12:                                          ; preds = %if.end
  %20 = load i32, i32* %num_values.addr, align 4
  %21 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %22 = load i32*, i32** %out_values.addr, align 4
  %call13 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi7EEEEEbjPNS_13DecoderBufferEPj(i32 %20, %"class.draco::DecoderBuffer"* %21, i32* %22)
  store i1 %call13, i1* %retval, align 1
  br label %return

sw.bb14:                                          ; preds = %if.end
  %23 = load i32, i32* %num_values.addr, align 4
  %24 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %25 = load i32*, i32** %out_values.addr, align 4
  %call15 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi8EEEEEbjPNS_13DecoderBufferEPj(i32 %23, %"class.draco::DecoderBuffer"* %24, i32* %25)
  store i1 %call15, i1* %retval, align 1
  br label %return

sw.bb16:                                          ; preds = %if.end
  %26 = load i32, i32* %num_values.addr, align 4
  %27 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %28 = load i32*, i32** %out_values.addr, align 4
  %call17 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi9EEEEEbjPNS_13DecoderBufferEPj(i32 %26, %"class.draco::DecoderBuffer"* %27, i32* %28)
  store i1 %call17, i1* %retval, align 1
  br label %return

sw.bb18:                                          ; preds = %if.end
  %29 = load i32, i32* %num_values.addr, align 4
  %30 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %31 = load i32*, i32** %out_values.addr, align 4
  %call19 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi10EEEEEbjPNS_13DecoderBufferEPj(i32 %29, %"class.draco::DecoderBuffer"* %30, i32* %31)
  store i1 %call19, i1* %retval, align 1
  br label %return

sw.bb20:                                          ; preds = %if.end
  %32 = load i32, i32* %num_values.addr, align 4
  %33 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %34 = load i32*, i32** %out_values.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi11EEEEEbjPNS_13DecoderBufferEPj(i32 %32, %"class.draco::DecoderBuffer"* %33, i32* %34)
  store i1 %call21, i1* %retval, align 1
  br label %return

sw.bb22:                                          ; preds = %if.end
  %35 = load i32, i32* %num_values.addr, align 4
  %36 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %37 = load i32*, i32** %out_values.addr, align 4
  %call23 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi12EEEEEbjPNS_13DecoderBufferEPj(i32 %35, %"class.draco::DecoderBuffer"* %36, i32* %37)
  store i1 %call23, i1* %retval, align 1
  br label %return

sw.bb24:                                          ; preds = %if.end
  %38 = load i32, i32* %num_values.addr, align 4
  %39 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %40 = load i32*, i32** %out_values.addr, align 4
  %call25 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi13EEEEEbjPNS_13DecoderBufferEPj(i32 %38, %"class.draco::DecoderBuffer"* %39, i32* %40)
  store i1 %call25, i1* %retval, align 1
  br label %return

sw.bb26:                                          ; preds = %if.end
  %41 = load i32, i32* %num_values.addr, align 4
  %42 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %43 = load i32*, i32** %out_values.addr, align 4
  %call27 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi14EEEEEbjPNS_13DecoderBufferEPj(i32 %41, %"class.draco::DecoderBuffer"* %42, i32* %43)
  store i1 %call27, i1* %retval, align 1
  br label %return

sw.bb28:                                          ; preds = %if.end
  %44 = load i32, i32* %num_values.addr, align 4
  %45 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %46 = load i32*, i32** %out_values.addr, align 4
  %call29 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi15EEEEEbjPNS_13DecoderBufferEPj(i32 %44, %"class.draco::DecoderBuffer"* %45, i32* %46)
  store i1 %call29, i1* %retval, align 1
  br label %return

sw.bb30:                                          ; preds = %if.end
  %47 = load i32, i32* %num_values.addr, align 4
  %48 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %49 = load i32*, i32** %out_values.addr, align 4
  %call31 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi16EEEEEbjPNS_13DecoderBufferEPj(i32 %47, %"class.draco::DecoderBuffer"* %48, i32* %49)
  store i1 %call31, i1* %retval, align 1
  br label %return

sw.bb32:                                          ; preds = %if.end
  %50 = load i32, i32* %num_values.addr, align 4
  %51 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %52 = load i32*, i32** %out_values.addr, align 4
  %call33 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi17EEEEEbjPNS_13DecoderBufferEPj(i32 %50, %"class.draco::DecoderBuffer"* %51, i32* %52)
  store i1 %call33, i1* %retval, align 1
  br label %return

sw.bb34:                                          ; preds = %if.end
  %53 = load i32, i32* %num_values.addr, align 4
  %54 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %55 = load i32*, i32** %out_values.addr, align 4
  %call35 = call zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi18EEEEEbjPNS_13DecoderBufferEPj(i32 %53, %"class.draco::DecoderBuffer"* %54, i32* %55)
  store i1 %call35, i1* %retval, align 1
  br label %return

sw.default:                                       ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %sw.default, %sw.bb34, %sw.bb32, %sw.bb30, %sw.bb28, %sw.bb26, %sw.bb24, %sw.bb22, %sw.bb20, %sw.bb18, %sw.bb16, %sw.bb14, %sw.bb12, %sw.bb10, %sw.bb8, %sw.bb6, %sw.bb4, %sw.bb2, %sw.bb, %if.then
  %56 = load i1, i1* %retval, align 1
  ret i1 %56
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder"* @_ZN5draco17RAnsSymbolDecoderILi5EEC2Ev(%"class.draco::RAnsSymbolDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder"*, align 4
  store %"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder"*, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi5EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder"*, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi5EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder"*, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi5EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder"*, align 4
  store %"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder"*, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

declare zeroext i1 @_ZN5draco13DecoderBuffer16StartBitDecodingEbPy(%"class.draco::DecoderBuffer"*, i1 zeroext, i64*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi5EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder"*, align 4
  store %"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder"*, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer28DecodeLeastSignificantBits32EiPj(%"class.draco::DecoderBuffer"* %this, i32 %nbits, i32* %out_value) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %nbits.addr = alloca i32, align 4
  %out_value.addr = alloca i32*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %out_value, i32** %out_value.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco13DecoderBuffer18bit_decoder_activeEv(%"class.draco::DecoderBuffer"* %this1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %bit_decoder_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 3
  %0 = load i32, i32* %nbits.addr, align 4
  %1 = load i32*, i32** %out_value.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer10BitDecoder7GetBitsEiPj(%"class.draco::DecoderBuffer::BitDecoder"* %bit_decoder_, i32 %0, i32* %1)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi5EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder"*, align 4
  store %"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder"*, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

declare void @_ZN5draco13DecoderBuffer14EndBitDecodingEv(%"class.draco::DecoderBuffer"*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder"* @_ZN5draco17RAnsSymbolDecoderILi5EED2Ev(%"class.draco::RAnsSymbolDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder"*, align 4
  store %"class.draco::RAnsSymbolDecoder"* %this, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder"*, %"class.draco::RAnsSymbolDecoder"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder", %"class.draco::RAnsSymbolDecoder"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder"*, align 4
  store %"class.draco::RAnsDecoder"* %this, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder"*, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %lut_table_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %call3 = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_)
  ret %"class.draco::RAnsDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::__vector_base.2"* %0) #8
  ret %"class.std::__2::vector.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  store %"struct.draco::AnsDecoder"* %this, %"struct.draco::AnsDecoder"** %this.addr, align 4
  %this1 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %this.addr, align 4
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 0
  store i8* null, i8** %buf, align 4
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 1
  store i32 0, i32* %buf_offset, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %state, align 4
  ret %"struct.draco::AnsDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::__vector_base.2"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.2"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  store %"struct.draco::rans_sym"* null, %"struct.draco::rans_sym"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 1
  store %"struct.draco::rans_sym"* null, %"struct.draco::rans_sym"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.3"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.3"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.3"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.3"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.4"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.5"* %2)
  ret %"class.std::__2::__compressed_pair.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.4"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"struct.draco::rans_sym"* null, %"struct.draco::rans_sym"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.5"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  %call = call %"class.std::__2::allocator.6"* @_ZNSt3__29allocatorIN5draco8rans_symEEC2Ev(%"class.std::__2::allocator.6"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.6"* @_ZNSt3__29allocatorIN5draco8rans_symEEC2Ev(%"class.std::__2::allocator.6"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  ret %"class.std::__2::allocator.6"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %bitstream_version_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 5
  %0 = load i16, i16* %bitstream_version_, align 2
  ret i16 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i32*, i32** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this1, i32* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %out_val, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %out_val.addr = alloca i32*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %in = alloca i8, align 1
  store i32* %out_val, i32** %out_val.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %in)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8, i8* %in, align 1
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.end
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %2, %"class.draco::DecoderBuffer"* %3)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.then1
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.then1
  %4 = load i32*, i32** %out_val.addr, align 4
  %5 = load i32, i32* %4, align 4
  %shl = shl i32 %5, 7
  store i32 %shl, i32* %4, align 4
  %6 = load i8, i8* %in, align 1
  %conv5 = zext i8 %6 to i32
  %and6 = and i32 %conv5, 127
  %7 = load i32*, i32** %out_val.addr, align 4
  %8 = load i32, i32* %7, align 4
  %or = or i32 %8, %and6
  store i32 %or, i32* %7, align 4
  br label %if.end8

if.else:                                          ; preds = %if.end
  %9 = load i8, i8* %in, align 1
  %conv7 = zext i8 %9 to i32
  %10 = load i32*, i32** %out_val.addr, align 4
  store i32 %conv7, i32* %10, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.end4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end8, %if.then3, %if.then
  %11 = load i1, i1* %retval, align 1
  ret i1 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE8__appendEm(%"class.std::__2::vector"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load i32*, i32** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %7, i32 %8
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::vector"* %this1, i32* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %this, i32* %token_probs, i32 %num_symbols) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsDecoder"*, align 4
  %token_probs.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %cum_prob = alloca i32, align 4
  %act_prob = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::RAnsDecoder"* %this, %"class.draco::RAnsDecoder"** %this.addr, align 4
  store i32* %token_probs, i32** %token_probs.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder"*, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %lut_table_, i32 4096)
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %probability_table_, i32 %0)
  store i32 0, i32* %cum_prob, align 4
  store i32 0, i32* %act_prob, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %token_probs.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %probability_table_2 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_2, i32 %6) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call, i32 0, i32 0
  store i32 %5, i32* %prob, align 4
  %7 = load i32, i32* %cum_prob, align 4
  %probability_table_3 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_3, i32 %8) #8
  %cum_prob5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 0, i32 1
  store i32 %7, i32* %cum_prob5, align 4
  %9 = load i32*, i32** %token_probs.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %12 = load i32, i32* %cum_prob, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %cum_prob, align 4
  %13 = load i32, i32* %cum_prob, align 4
  %cmp7 = icmp ugt i32 %13, 4096
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %14 = load i32, i32* %act_prob, align 4
  store i32 %14, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %cum_prob, align 4
  %cmp9 = icmp ult i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32, i32* %i, align 4
  %lut_table_11 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 0
  %18 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_11, i32 %18) #8
  store i32 %17, i32* %call12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %20 = load i32, i32* %cum_prob, align 4
  store i32 %20, i32* %act_prob, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc14 = add i32 %21, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %22 = load i32, i32* %cum_prob, align 4
  %cmp16 = icmp ne i32 %22, 4096
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end15
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 4, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = bitcast i32* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 1 %add.ptr, i32 4, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE8__appendEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %0) #8
  %1 = load i32*, i32** %call, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm(%"class.std::__2::vector"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %6) #8
  store %"class.std::__2::allocator"* %call2, %"class.std::__2::allocator"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %8 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::vector"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE27__invalidate_iterators_pastEPj(%"class.std::__2::vector"* %this1, i32* %0)
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %2 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %1, i32* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i32*, i32** %__new_end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i32*, i32** %__pos_3, align 4
  %call4 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #9
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.8"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.8"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionC2EPPjm(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load i32*, i32** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load i32*, i32** %__end_2, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load i32*, i32** %__pos_4, align 4
  %call5 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3, i32* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load i32*, i32** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %4, i32 1
  store i32* %incdec.ptr, i32** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #8
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  store i32 0, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.8"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.8"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.8"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair.8"* %this, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.8"*, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.8"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.8"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.9"*
  %5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.9"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.9"* %4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.8"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.8"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.8"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.9"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.9"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.9", %"struct.std::__2::__compressed_pair_elem.9"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #4 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #9
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #10
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #3

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.8"*, align 4
  store %"class.std::__2::__compressed_pair.8"* %this, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.8"*, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.8"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.9"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %1) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.9", %"struct.std::__2::__compressed_pair_elem.9"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__value_, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.8"*, align 4
  store %"class.std::__2::__compressed_pair.8"* %this, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.8"*, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.8"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionC2EPPjm(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* returned %this, i32** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca i32**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  store i32** %__p, i32*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32**, i32*** %__p.addr, align 4
  %1 = load i32*, i32** %0, align 4
  store i32* %1, i32** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load i32**, i32*** %__p.addr, align 4
  %3 = load i32*, i32** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %3, i32 %4
  store i32* %add.ptr, i32** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load i32**, i32*** %__p.addr, align 4
  store i32** %5, i32*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load i32**, i32*** %__dest_, align 4
  store i32* %0, i32** %1, align 4
  ret %"struct.std::__2::__split_buffer<unsigned int, std::__2::allocator<unsigned int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.10", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.10", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.8"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.8"*, align 4
  store %"class.std::__2::__compressed_pair.8"* %this, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.8"*, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.8"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE27__invalidate_iterators_pastEPj(%"class.std::__2::vector"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i32, i32* %call4, i32 %2
  %3 = bitcast i32* %add.ptr5 to i8*
  %call6 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr8 = getelementptr inbounds i32, i32* %call6, i32 %call7
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8__appendEm(%"class.std::__2::vector.1"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %6, i32 0, i32 0
  %7 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %7, i32 %8
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__destruct_at_endEPS2_(%"class.std::__2::vector.1"* %this1, %"struct.draco::rans_sym"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %1, i32 %2
  ret %"struct.draco::rans_sym"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 1
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 0
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::rans_sym"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::rans_sym"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 8
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8__appendEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.6"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.11", align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %0) #8
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 1
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::rans_sym"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::rans_sym"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 8
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE18__construct_at_endEm(%"class.std::__2::vector.1"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %6) #8
  store %"class.std::__2::allocator.6"* %call2, %"class.std::__2::allocator.6"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE11__recommendEm(%"class.std::__2::vector.1"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  %8 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.11"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEEC2EmmS5_(%"struct.std::__2::__split_buffer.11"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.11"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE(%"class.std::__2::vector.1"* %this1, %"struct.std::__2::__split_buffer.11"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.11"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEED2Ev(%"struct.std::__2::__split_buffer.11"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__destruct_at_endEPS2_(%"class.std::__2::vector.1"* %this, %"struct.draco::rans_sym"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__new_last.addr = alloca %"struct.draco::rans_sym"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__new_last, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE27__invalidate_iterators_pastEPS2_(%"class.std::__2::vector.1"* %this1, %"struct.draco::rans_sym"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE17__destruct_at_endEPS2_(%"class.std::__2::__vector_base.2"* %1, %"struct.draco::rans_sym"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__annotate_shrinkEm(%"class.std::__2::vector.1"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE18__construct_at_endEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE21_ConstructTransactionC2ERS5_m(%"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__new_end_, align 4
  %cmp = icmp ne %"struct.draco::rans_sym"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_3, align 4
  %call4 = call %"struct.draco::rans_sym"* @_ZNSt3__212__to_addressIN5draco8rans_symEEEPT_S4_(%"struct.draco::rans_sym"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE9constructIS3_JEEEvRS4_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call2, %"struct.draco::rans_sym"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %5, i32 1
  store %"struct.draco::rans_sym"* %incdec.ptr, %"struct.draco::rans_sym"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE11__recommendEm(%"class.std::__2::vector.1"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8max_sizeEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #9
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.11"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEEC2EmmS5_(%"struct.std::__2::__split_buffer.11"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.11"* %this1, %"struct.std::__2::__split_buffer.11"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.11"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEEC2IDnS6_EEOT_OT0_(%"class.std::__2::__compressed_pair.12"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE7__allocEv(%"struct.std::__2::__split_buffer.11"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"struct.draco::rans_sym"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE8allocateERS4_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.draco::rans_sym"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 0
  store %"struct.draco::rans_sym"* %cond, %"struct.draco::rans_sym"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 0
  %4 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 2
  store %"struct.draco::rans_sym"* %add.ptr, %"struct.draco::rans_sym"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 1
  store %"struct.draco::rans_sym"* %add.ptr, %"struct.draco::rans_sym"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 0
  %6 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE9__end_capEv(%"struct.std::__2::__split_buffer.11"* %this1) #8
  store %"struct.draco::rans_sym"* %add.ptr6, %"struct.draco::rans_sym"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.11"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.11"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE21_ConstructTransactionC2EPPS2_m(%"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %__tx, %"struct.draco::rans_sym"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_2, align 4
  %cmp = icmp ne %"struct.draco::rans_sym"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE7__allocEv(%"struct.std::__2::__split_buffer.11"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_4, align 4
  %call5 = call %"struct.draco::rans_sym"* @_ZNSt3__212__to_addressIN5draco8rans_symEEEPT_S4_(%"struct.draco::rans_sym"* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE9constructIS3_JEEEvRS4_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call3, %"struct.draco::rans_sym"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %4, i32 1
  store %"struct.draco::rans_sym"* %incdec.ptr, %"struct.draco::rans_sym"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE(%"class.std::__2::vector.1"* %this, %"struct.std::__2::__split_buffer.11"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.11"* %__v, %"struct.std::__2::__split_buffer.11"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__annotate_deleteEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %0) #8
  %1 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %1, i32 0, i32 0
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %3, i32 0, i32 1
  %4 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE46__construct_backward_with_exception_guaranteesIS3_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS4_PT_SA_EE5valuesr31is_trivially_move_constructibleISA_EE5valueEvE4typeERS4_SB_SB_RSB_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::rans_sym"* %2, %"struct.draco::rans_sym"* %4, %"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco8rans_symEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS5_EE5valueEvE4typeERS5_S8_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__begin_3, %"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco8rans_symEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS5_EE5valueEvE4typeERS5_S8_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__end_5, %"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE9__end_capEv(%"struct.std::__2::__split_buffer.11"* %11) #8
  call void @_ZNSt3__24swapIPN5draco8rans_symEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS5_EE5valueEvE4typeERS5_S8_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %call7, %"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %12, i32 0, i32 1
  %13 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %14, i32 0, i32 0
  store %"struct.draco::rans_sym"* %13, %"struct.draco::rans_sym"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE14__annotate_newEm(%"class.std::__2::vector.1"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.1"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.11"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEED2Ev(%"struct.std::__2::__split_buffer.11"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.11"* %this1, %"struct.std::__2::__split_buffer.11"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE5clearEv(%"struct.std::__2::__split_buffer.11"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__first_, align 4
  %tobool = icmp ne %"struct.draco::rans_sym"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE7__allocEv(%"struct.std::__2::__split_buffer.11"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE8capacityEv(%"struct.std::__2::__split_buffer.11"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE10deallocateERS4_PS3_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::rans_sym"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.11"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  ret %"struct.draco::rans_sym"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE21_ConstructTransactionC2ERS5_m(%"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.1"* %__v, %"class.std::__2::vector.1"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  store %"class.std::__2::vector.1"* %0, %"class.std::__2::vector.1"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 1
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_, align 4
  store %"struct.draco::rans_sym"* %3, %"struct.draco::rans_sym"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.1"* %4 to %"class.std::__2::__vector_base.2"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %5, i32 0, i32 1
  %6 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %6, i32 %7
  store %"struct.draco::rans_sym"* %add.ptr, %"struct.draco::rans_sym"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE9constructIS3_JEEEvRS4_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::rans_sym"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.14", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.14"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE11__constructIS3_JEEEvNS_17integral_constantIbLb1EEERS4_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1, %"struct.draco::rans_sym"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::rans_sym"* @_ZNSt3__212__to_addressIN5draco8rans_symEEEPT_S4_(%"struct.draco::rans_sym"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  ret %"struct.draco::rans_sym"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction", %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 1
  store %"struct.draco::rans_sym"* %0, %"struct.draco::rans_sym"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::rans_sym, std::__2::allocator<draco::rans_sym>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE11__constructIS3_JEEEvNS_17integral_constantIbLb1EEERS4_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::rans_sym"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco8rans_symEE9constructIS2_JEEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %1, %"struct.draco::rans_sym"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco8rans_symEE9constructIS2_JEEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %this, %"struct.draco::rans_sym"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  %1 = bitcast %"struct.draco::rans_sym"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.draco::rans_sym"*
  %3 = bitcast %"struct.draco::rans_sym"* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 8, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8max_sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE8max_sizeERKS4_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::__vector_base.2"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE8max_sizeERKS4_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.15", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.15"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS4_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS4_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco8rans_symEE8max_sizeEv(%"class.std::__2::allocator.6"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco8rans_symEE8max_sizeEv(%"class.std::__2::allocator.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  ret i32 536870911
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco8rans_symEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::rans_sym"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::rans_sym"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 8
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__217__compressed_pairIPN5draco8rans_symENS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  ret %"struct.draco::rans_sym"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEEC2IDnS6_EEOT_OT0_(%"class.std::__2::__compressed_pair.12"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.6"* %__t2, %"class.std::__2::allocator.6"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.4"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.13"*
  %5 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco8rans_symEEEEEOT_RNS_16remove_referenceIS6_E4typeE(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco8rans_symEEELi1ELb0EEC2IS5_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* %4, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::rans_sym"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE8allocateERS4_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.draco::rans_sym"* @_ZNSt3__29allocatorIN5draco8rans_symEE8allocateEmPKv(%"class.std::__2::allocator.6"* %0, i32 %1, i8* null)
  ret %"struct.draco::rans_sym"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE7__allocEv(%"struct.std::__2::__split_buffer.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__end_cap_) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE9__end_capEv(%"struct.std::__2::__split_buffer.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__end_cap_) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco8rans_symEEEEEOT_RNS_16remove_referenceIS6_E4typeE(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %__t, %"class.std::__2::allocator.6"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__t.addr, align 4
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco8rans_symEEELi1ELb0EEC2IS5_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* returned %this, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  store %"class.std::__2::allocator.6"* %__u, %"class.std::__2::allocator.6"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco8rans_symEEEEEOT_RNS_16remove_referenceIS6_E4typeE(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.6"* %call, %"class.std::__2::allocator.6"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::rans_sym"* @_ZNSt3__29allocatorIN5draco8rans_symEE8allocateEmPKv(%"class.std::__2::allocator.6"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco8rans_symEE8max_sizeEv(%"class.std::__2::allocator.6"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 8
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"struct.draco::rans_sym"*
  ret %"struct.draco::rans_sym"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco8rans_symEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %1) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco8rans_symEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__value_, align 4
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE21_ConstructTransactionC2EPPS2_m(%"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* returned %this, %"struct.draco::rans_sym"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"** %this.addr, align 4
  store %"struct.draco::rans_sym"** %__p, %"struct.draco::rans_sym"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__p.addr, align 4
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %0, align 4
  store %"struct.draco::rans_sym"* %1, %"struct.draco::rans_sym"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__p.addr, align 4
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %3, i32 %4
  store %"struct.draco::rans_sym"* %add.ptr, %"struct.draco::rans_sym"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__p.addr, align 4
  store %"struct.draco::rans_sym"** %5, %"struct.draco::rans_sym"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__dest_, align 4
  store %"struct.draco::rans_sym"* %0, %"struct.draco::rans_sym"** %1, align 4
  ret %"struct.std::__2::__split_buffer<draco::rans_sym, std::__2::allocator<draco::rans_sym> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__annotate_deleteEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"struct.draco::rans_sym"* %call to i8*
  %call2 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::rans_sym"* %add.ptr to i8*
  %call4 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 %call5
  %2 = bitcast %"struct.draco::rans_sym"* %add.ptr6 to i8*
  %call7 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call7, i32 %call8
  %3 = bitcast %"struct.draco::rans_sym"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_(%"class.std::__2::vector.1"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE46__construct_backward_with_exception_guaranteesIS3_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS4_PT_SA_EE5valuesr31is_trivially_move_constructibleISA_EE5valueEvE4typeERS4_SB_SB_RSB_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %0, %"struct.draco::rans_sym"* %__begin1, %"struct.draco::rans_sym"* %__end1, %"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__begin1.addr = alloca %"struct.draco::rans_sym"*, align 4
  %__end1.addr = alloca %"struct.draco::rans_sym"*, align 4
  %__end2.addr = alloca %"struct.draco::rans_sym"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %0, %"class.std::__2::allocator.6"** %.addr, align 4
  store %"struct.draco::rans_sym"* %__begin1, %"struct.draco::rans_sym"** %__begin1.addr, align 4
  store %"struct.draco::rans_sym"* %__end1, %"struct.draco::rans_sym"** %__end1.addr, align 4
  store %"struct.draco::rans_sym"** %__end2, %"struct.draco::rans_sym"*** %__end2.addr, align 4
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end1.addr, align 4
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::rans_sym"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::rans_sym"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 8
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__end2.addr, align 4
  %5 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %5, i32 %idx.neg
  store %"struct.draco::rans_sym"* %add.ptr, %"struct.draco::rans_sym"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__end2.addr, align 4
  %8 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %7, align 4
  %9 = bitcast %"struct.draco::rans_sym"* %8 to i8*
  %10 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin1.addr, align 4
  %11 = bitcast %"struct.draco::rans_sym"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco8rans_symEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS5_EE5valueEvE4typeERS5_S8_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__x, %"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"struct.draco::rans_sym"**, align 4
  %__y.addr = alloca %"struct.draco::rans_sym"**, align 4
  %__t = alloca %"struct.draco::rans_sym"*, align 4
  store %"struct.draco::rans_sym"** %__x, %"struct.draco::rans_sym"*** %__x.addr, align 4
  store %"struct.draco::rans_sym"** %__y, %"struct.draco::rans_sym"*** %__y.addr, align 4
  %0 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__24moveIRPN5draco8rans_symEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %call, align 4
  store %"struct.draco::rans_sym"* %1, %"struct.draco::rans_sym"** %__t, align 4
  %2 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__24moveIRPN5draco8rans_symEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %call1, align 4
  %4 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__x.addr, align 4
  store %"struct.draco::rans_sym"* %3, %"struct.draco::rans_sym"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__24moveIRPN5draco8rans_symEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %call2, align 4
  %6 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__y.addr, align 4
  store %"struct.draco::rans_sym"* %5, %"struct.draco::rans_sym"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE14__annotate_newEm(%"class.std::__2::vector.1"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"struct.draco::rans_sym"* %call to i8*
  %call2 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::rans_sym"* %add.ptr to i8*
  %call4 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 %call5
  %2 = bitcast %"struct.draco::rans_sym"* %add.ptr6 to i8*
  %call7 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call7, i32 %3
  %4 = bitcast %"struct.draco::rans_sym"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_(%"class.std::__2::vector.1"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_(%"class.std::__2::vector.1"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  %call = call %"struct.draco::rans_sym"* @_ZNSt3__212__to_addressIN5draco8rans_symEEEPT_S4_(%"struct.draco::rans_sym"* %1) #8
  ret %"struct.draco::rans_sym"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNSt3__24moveIRPN5draco8rans_symEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::rans_sym"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.draco::rans_sym"**, align 4
  store %"struct.draco::rans_sym"** %__t, %"struct.draco::rans_sym"*** %__t.addr, align 4
  %0 = load %"struct.draco::rans_sym"**, %"struct.draco::rans_sym"*** %__t.addr, align 4
  ret %"struct.draco::rans_sym"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE5clearEv(%"struct.std::__2::__split_buffer.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE17__destruct_at_endEPS2_(%"struct.std::__2::__split_buffer.11"* %this1, %"struct.draco::rans_sym"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE10deallocateERS4_PS3_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::rans_sym"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco8rans_symEE10deallocateEPS2_m(%"class.std::__2::allocator.6"* %0, %"struct.draco::rans_sym"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE8capacityEv(%"struct.std::__2::__split_buffer.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE9__end_capEv(%"struct.std::__2::__split_buffer.11"* %this1) #8
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::rans_sym"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::rans_sym"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 8
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE17__destruct_at_endEPS2_(%"struct.std::__2::__split_buffer.11"* %this, %"struct.draco::rans_sym"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  %__new_last.addr = alloca %"struct.draco::rans_sym"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.10", align 1
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__new_last, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE17__destruct_at_endEPS2_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.11"* %this1, %"struct.draco::rans_sym"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE17__destruct_at_endEPS2_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.11"* %this, %"struct.draco::rans_sym"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.10", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  %__new_last.addr = alloca %"struct.draco::rans_sym"*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__new_last, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 2
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_, align 4
  %cmp = icmp ne %"struct.draco::rans_sym"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE7__allocEv(%"struct.std::__2::__split_buffer.11"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 2
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %3, i32 -1
  store %"struct.draco::rans_sym"* %incdec.ptr, %"struct.draco::rans_sym"** %__end_2, align 4
  %call3 = call %"struct.draco::rans_sym"* @_ZNSt3__212__to_addressIN5draco8rans_symEEEPT_S4_(%"struct.draco::rans_sym"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE7destroyIS3_EEvRS4_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::rans_sym"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE7destroyIS3_EEvRS4_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::rans_sym"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.16", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.16"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE9__destroyIS3_EEvNS_17integral_constantIbLb1EEERS4_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1, %"struct.draco::rans_sym"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE9__destroyIS3_EEvNS_17integral_constantIbLb1EEERS4_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::rans_sym"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco8rans_symEE7destroyEPS2_(%"class.std::__2::allocator.6"* %1, %"struct.draco::rans_sym"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco8rans_symEE7destroyEPS2_(%"class.std::__2::allocator.6"* %this, %"struct.draco::rans_sym"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco8rans_symEE10deallocateEPS2_m(%"class.std::__2::allocator.6"* %this, %"struct.draco::rans_sym"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"struct.draco::rans_sym"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__p, %"struct.draco::rans_sym"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__p.addr, align 4
  %1 = bitcast %"struct.draco::rans_sym"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 8
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__214__split_bufferIN5draco8rans_symERNS_9allocatorIS2_EEE9__end_capEv(%"struct.std::__2::__split_buffer.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.11"*, align 4
  store %"struct.std::__2::__split_buffer.11"* %this, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.11"*, %"struct.std::__2::__split_buffer.11"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.11", %"struct.std::__2::__split_buffer.11"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__end_cap_) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__217__compressed_pairIPN5draco8rans_symERNS_9allocatorIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::rans_sym"** @_ZNKSt3__222__compressed_pair_elemIPN5draco8rans_symELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #8
  ret %"struct.draco::rans_sym"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE27__invalidate_iterators_pastEPS2_(%"class.std::__2::vector.1"* %this, %"struct.draco::rans_sym"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__new_last.addr = alloca %"struct.draco::rans_sym"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__new_last, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE17__destruct_at_endEPS2_(%"class.std::__2::__vector_base.2"* %this, %"struct.draco::rans_sym"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  %__new_last.addr = alloca %"struct.draco::rans_sym"*, align 4
  %__soon_to_be_end = alloca %"struct.draco::rans_sym"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  store %"struct.draco::rans_sym"* %__new_last, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__end_, align 4
  store %"struct.draco::rans_sym"* %0, %"struct.draco::rans_sym"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %2 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"struct.draco::rans_sym"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %3 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %3, i32 -1
  store %"struct.draco::rans_sym"* %incdec.ptr, %"struct.draco::rans_sym"** %__soon_to_be_end, align 4
  %call2 = call %"struct.draco::rans_sym"* @_ZNSt3__212__to_addressIN5draco8rans_symEEEPT_S4_(%"struct.draco::rans_sym"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE7destroyIS3_EEvRS4_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::rans_sym"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 1
  store %"struct.draco::rans_sym"* %4, %"struct.draco::rans_sym"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__annotate_shrinkEm(%"class.std::__2::vector.1"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"struct.draco::rans_sym"* %call to i8*
  %call2 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::rans_sym"* %add.ptr to i8*
  %call4 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 %2
  %3 = bitcast %"struct.draco::rans_sym"* %add.ptr5 to i8*
  %call6 = call %"struct.draco::rans_sym"* @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call6, i32 %call7
  %4 = bitcast %"struct.draco::rans_sym"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_(%"class.std::__2::vector.1"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %this, i64* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i64*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i64* %out_val, i64** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i64*, i64** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIyEEbPT_(%"class.draco::DecoderBuffer"* %this1, i64* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 8
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %out_val, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %out_val.addr = alloca i64*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %in = alloca i8, align 1
  store i64* %out_val, i64** %out_val.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %in)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8, i8* %in, align 1
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.end
  %2 = load i64*, i64** %out_val.addr, align 4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %2, %"class.draco::DecoderBuffer"* %3)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.then1
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.then1
  %4 = load i64*, i64** %out_val.addr, align 4
  %5 = load i64, i64* %4, align 8
  %shl = shl i64 %5, 7
  store i64 %shl, i64* %4, align 8
  %6 = load i8, i8* %in, align 1
  %conv5 = zext i8 %6 to i32
  %and6 = and i32 %conv5, 127
  %conv7 = sext i32 %and6 to i64
  %7 = load i64*, i64** %out_val.addr, align 4
  %8 = load i64, i64* %7, align 8
  %or = or i64 %8, %conv7
  store i64 %or, i64* %7, align 8
  br label %if.end9

if.else:                                          ; preds = %if.end
  %9 = load i8, i8* %in, align 1
  %conv8 = zext i8 %9 to i64
  %10 = load i64*, i64** %out_val.addr, align 4
  store i64 %conv8, i64* %10, align 8
  br label %if.end9

if.end9:                                          ; preds = %if.else, %if.end4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end9, %if.then3, %if.then
  %11 = load i1, i1* %retval, align 1
  ret i1 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %sub = sub nsw i64 %0, %1
  ret i64 %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %data_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %this, i64 %bytes) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes.addr = alloca i64, align 8
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i64 %bytes, i64* %bytes.addr, align 8
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i64, i64* %bytes.addr, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, %0
  store i64 %add, i64* %pos_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %this, i8* %buf, i32 %offset) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::RAnsDecoder"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"class.draco::RAnsDecoder"* %this, %"class.draco::RAnsDecoder"** %this.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder"*, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf2 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 0
  store i8* %1, i8** %buf2, align 4
  %2 = load i8*, i8** %buf.addr, align 4
  %3 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %6, 1
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 1
  store i32 %sub5, i32* %buf_offset, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %8 = load i32, i32* %offset.addr, align 4
  %sub7 = sub nsw i32 %8, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 %sub7
  %9 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %9 to i32
  %and = and i32 %conv9, 63
  %ans_10 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_10, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end54

if.else:                                          ; preds = %if.end
  %10 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %10, 1
  br i1 %cmp11, label %if.then12, label %if.else23

if.then12:                                        ; preds = %if.else
  %11 = load i32, i32* %offset.addr, align 4
  %cmp13 = icmp slt i32 %11, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  %12 = load i32, i32* %offset.addr, align 4
  %sub16 = sub nsw i32 %12, 2
  %ans_17 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf_offset18 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_17, i32 0, i32 1
  store i32 %sub16, i32* %buf_offset18, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %14 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %add.ptr19 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr19)
  %and20 = and i32 %call, 16383
  %ans_21 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state22 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_21, i32 0, i32 2
  store i32 %and20, i32* %state22, align 4
  br label %if.end53

if.else23:                                        ; preds = %if.else
  %15 = load i32, i32* %x, align 4
  %cmp24 = icmp eq i32 %15, 2
  br i1 %cmp24, label %if.then25, label %if.else38

if.then25:                                        ; preds = %if.else23
  %16 = load i32, i32* %offset.addr, align 4
  %cmp26 = icmp slt i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.then25
  %17 = load i32, i32* %offset.addr, align 4
  %sub29 = sub nsw i32 %17, 3
  %ans_30 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf_offset31 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_30, i32 0, i32 1
  store i32 %sub29, i32* %buf_offset31, align 4
  %18 = load i8*, i8** %buf.addr, align 4
  %19 = load i32, i32* %offset.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %18, i32 %19
  %add.ptr33 = getelementptr inbounds i8, i8* %add.ptr32, i32 -3
  %call34 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr33)
  %and35 = and i32 %call34, 4194303
  %ans_36 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state37 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_36, i32 0, i32 2
  store i32 %and35, i32* %state37, align 4
  br label %if.end52

if.else38:                                        ; preds = %if.else23
  %20 = load i32, i32* %x, align 4
  %cmp39 = icmp eq i32 %20, 3
  br i1 %cmp39, label %if.then40, label %if.else50

if.then40:                                        ; preds = %if.else38
  %21 = load i32, i32* %offset.addr, align 4
  %sub41 = sub nsw i32 %21, 4
  %ans_42 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf_offset43 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_42, i32 0, i32 1
  store i32 %sub41, i32* %buf_offset43, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %23 = load i32, i32* %offset.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %22, i32 %23
  %add.ptr45 = getelementptr inbounds i8, i8* %add.ptr44, i32 -4
  %call46 = call i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %add.ptr45)
  %and47 = and i32 %call46, 1073741823
  %ans_48 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state49 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_48, i32 0, i32 2
  store i32 %and47, i32* %state49, align 4
  br label %if.end51

if.else50:                                        ; preds = %if.else38
  store i32 1, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.then40
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end28
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end15
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then4
  %ans_55 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state56 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_55, i32 0, i32 2
  %24 = load i32, i32* %state56, align 4
  %add = add i32 %24, 16384
  store i32 %add, i32* %state56, align 4
  %ans_57 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state58 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_57, i32 0, i32 2
  %25 = load i32, i32* %state58, align 4
  %cmp59 = icmp uge i32 %25, 4194304
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end54
  store i32 1, i32* %retval, align 4
  br label %return

if.end61:                                         ; preds = %if.end54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end61, %if.then60, %if.else50, %if.then27, %if.then14, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIyEEbPT_(%"class.draco::DecoderBuffer"* %this, i64* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i64*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i64* %out_val, i64** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 8, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 8
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i64*, i64** %out_val.addr, align 4
  %3 = bitcast i64* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %3, i8* align 1 %add.ptr, i32 8, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %vmem) #0 {
entry:
  %vmem.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %mem = alloca i8*, align 4
  store i8* %vmem, i8** %vmem.addr, align 4
  %0 = load i8*, i8** %vmem.addr, align 4
  store i8* %0, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 1
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %val, align 4
  %3 = load i8*, i8** %mem, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %5 = load i32, i32* %val, align 4
  %or = or i32 %5, %conv2
  store i32 %or, i32* %val, align 4
  %6 = load i32, i32* %val, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %vmem) #0 {
entry:
  %vmem.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %mem = alloca i8*, align 4
  store i8* %vmem, i8** %vmem.addr, align 4
  %0 = load i8*, i8** %vmem.addr, align 4
  store i8* %0, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 2
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 16
  store i32 %shl, i32* %val, align 4
  %3 = load i8*, i8** %mem, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %shl3 = shl i32 %conv2, 8
  %5 = load i32, i32* %val, align 4
  %or = or i32 %5, %shl3
  store i32 %or, i32* %val, align 4
  %6 = load i8*, i8** %mem, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %6, i32 0
  %7 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %7 to i32
  %8 = load i32, i32* %val, align 4
  %or6 = or i32 %8, %conv5
  store i32 %or6, i32* %val, align 4
  %9 = load i32, i32* %val, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %vmem) #0 {
entry:
  %vmem.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %mem = alloca i8*, align 4
  store i8* %vmem, i8** %vmem.addr, align 4
  %0 = load i8*, i8** %vmem.addr, align 4
  store i8* %0, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 3
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 24
  store i32 %shl, i32* %val, align 4
  %3 = load i8*, i8** %mem, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 2
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %shl3 = shl i32 %conv2, 16
  %5 = load i32, i32* %val, align 4
  %or = or i32 %5, %shl3
  store i32 %or, i32* %val, align 4
  %6 = load i8*, i8** %mem, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %6, i32 1
  %7 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %7 to i32
  %shl6 = shl i32 %conv5, 8
  %8 = load i32, i32* %val, align 4
  %or7 = or i32 %8, %shl6
  store i32 %or7, i32* %val, align 4
  %9 = load i8*, i8** %mem, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %10 to i32
  %11 = load i32, i32* %val, align 4
  %or10 = or i32 %11, %conv9
  store i32 %or10, i32* %val, align 4
  %12 = load i32, i32* %val, align 4
  ret i32 %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder"*, align 4
  %rem = alloca i32, align 4
  %quo = alloca i32, align 4
  %sym = alloca %"struct.draco::rans_dec_sym", align 4
  store %"class.draco::RAnsDecoder"* %this, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder"*, %"class.draco::RAnsDecoder"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %0, 16384
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %ans_2 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_2, i32 0, i32 1
  %1 = load i32, i32* %buf_offset, align 4
  %cmp3 = icmp sgt i32 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %2 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %2, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %ans_4 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state5 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_4, i32 0, i32 2
  %3 = load i32, i32* %state5, align 4
  %mul = mul i32 %3, 256
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %ans_7 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %buf_offset8 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_7, i32 0, i32 1
  %5 = load i32, i32* %buf_offset8, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %buf_offset8, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %dec
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %add = add i32 %mul, %conv
  %ans_9 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state10 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_9, i32 0, i32 2
  store i32 %add, i32* %state10, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %ans_11 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state12 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_11, i32 0, i32 2
  %7 = load i32, i32* %state12, align 4
  %div = udiv i32 %7, 4096
  store i32 %div, i32* %quo, align 4
  %ans_13 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_13, i32 0, i32 2
  %8 = load i32, i32* %state14, align 4
  %rem15 = urem i32 %8, 4096
  store i32 %rem15, i32* %rem, align 4
  %9 = load i32, i32* %rem, align 4
  call void @_ZN5draco11RAnsDecoderILi12EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder"* %this1, %"struct.draco::rans_dec_sym"* %sym, i32 %9)
  %10 = load i32, i32* %quo, align 4
  %prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 1
  %11 = load i32, i32* %prob, align 4
  %mul16 = mul i32 %10, %11
  %12 = load i32, i32* %rem, align 4
  %add17 = add i32 %mul16, %12
  %cum_prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 2
  %13 = load i32, i32* %cum_prob, align 4
  %sub = sub i32 %add17, %13
  %ans_18 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state19 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_18, i32 0, i32 2
  store i32 %sub, i32* %state19, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 0
  %14 = load i32, i32* %val, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11RAnsDecoderILi12EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder"* %this, %"struct.draco::rans_dec_sym"* %out, i32 %rem) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder"*, align 4
  %out.addr = alloca %"struct.draco::rans_dec_sym"*, align 4
  %rem.addr = alloca i32, align 4
  %symbol = alloca i32, align 4
  store %"class.draco::RAnsDecoder"* %this, %"class.draco::RAnsDecoder"** %this.addr, align 4
  store %"struct.draco::rans_dec_sym"* %out, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  store i32 %rem, i32* %rem.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder"*, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 0
  %0 = load i32, i32* %rem.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %symbol, align 4
  %2 = load i32, i32* %symbol, align 4
  %3 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %3, i32 0, i32 0
  store i32 %2, i32* %val, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 1
  %4 = load i32, i32* %symbol, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_, i32 %4) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 0, i32 0
  %5 = load i32, i32* %prob, align 4
  %6 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %prob3 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %6, i32 0, i32 1
  store i32 %5, i32* %prob3, align 4
  %probability_table_4 = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 1
  %7 = load i32, i32* %symbol, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_4, i32 %7) #8
  %cum_prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call5, i32 0, i32 1
  %8 = load i32, i32* %cum_prob, align 4
  %9 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %cum_prob6 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %9, i32 0, i32 2
  store i32 %8, i32* %cum_prob6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco13DecoderBuffer18bit_decoder_activeEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %bit_mode_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 4
  %0 = load i8, i8* %bit_mode_, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer10BitDecoder7GetBitsEiPj(%"class.draco::DecoderBuffer::BitDecoder"* %this, i32 %nbits, i32* %x) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer::BitDecoder"*, align 4
  %nbits.addr = alloca i32, align 4
  %x.addr = alloca i32*, align 4
  %value = alloca i32, align 4
  %bit = alloca i32, align 4
  store %"class.draco::DecoderBuffer::BitDecoder"* %this, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %x, i32** %x.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer::BitDecoder"*, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  store i32 0, i32* %value, align 4
  store i32 0, i32* %bit, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %bit, align 4
  %1 = load i32, i32* %nbits.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call = call i32 @_ZN5draco13DecoderBuffer10BitDecoder6GetBitEv(%"class.draco::DecoderBuffer::BitDecoder"* %this1)
  %2 = load i32, i32* %bit, align 4
  %shl = shl i32 %call, %2
  %3 = load i32, i32* %value, align 4
  %or = or i32 %3, %shl
  store i32 %or, i32* %value, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %bit, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %bit, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %x.addr, align 4
  store i32 %5, i32* %6, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco13DecoderBuffer10BitDecoder6GetBitEv(%"class.draco::DecoderBuffer::BitDecoder"* %this) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::DecoderBuffer::BitDecoder"*, align 4
  %off = alloca i32, align 4
  %byte_offset = alloca i32, align 4
  %bit_shift = alloca i32, align 4
  %bit = alloca i32, align 4
  store %"class.draco::DecoderBuffer::BitDecoder"* %this, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer::BitDecoder"*, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %bit_offset_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 2
  %0 = load i32, i32* %bit_offset_, align 4
  store i32 %0, i32* %off, align 4
  %1 = load i32, i32* %off, align 4
  %shr = lshr i32 %1, 3
  store i32 %shr, i32* %byte_offset, align 4
  %2 = load i32, i32* %off, align 4
  %and = and i32 %2, 7
  store i32 %and, i32* %bit_shift, align 4
  %bit_buffer_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %bit_buffer_, align 4
  %4 = load i32, i32* %byte_offset, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  %bit_buffer_end_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 1
  %5 = load i8*, i8** %bit_buffer_end_, align 4
  %cmp = icmp ult i8* %add.ptr, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %bit_buffer_2 = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 0
  %6 = load i8*, i8** %bit_buffer_2, align 4
  %7 = load i32, i32* %byte_offset, align 4
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %8 to i32
  %9 = load i32, i32* %bit_shift, align 4
  %shr3 = ashr i32 %conv, %9
  %and4 = and i32 %shr3, 1
  store i32 %and4, i32* %bit, align 4
  %10 = load i32, i32* %off, align 4
  %add = add i32 %10, 1
  %bit_offset_5 = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 2
  store i32 %add, i32* %bit_offset_5, align 4
  %11 = load i32, i32* %bit, align 4
  store i32 %11, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder"*, align 4
  store %"class.draco::RAnsDecoder"* %this, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder"*, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %0, 16384
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder"*, align 4
  store %"class.draco::RAnsDecoder"* %this, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder"*, %"class.draco::RAnsDecoder"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder", %"class.draco::RAnsDecoder"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %lut_table_) #8
  ret %"class.draco::RAnsDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE17__annotate_deleteEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::__vector_base.2"* %0) #8
  ret %"class.std::__2::vector.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::__vector_base.2"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.2"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  store %"class.std::__2::__vector_base.2"* %this1, %"class.std::__2::__vector_base.2"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  %cmp = icmp ne %"struct.draco::rans_sym"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE5clearEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE8capacityEv(%"class.std::__2::__vector_base.2"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco8rans_symEEEE10deallocateERS4_PS3_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::rans_sym"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %retval, align 4
  ret %"class.std::__2::__vector_base.2"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE5clearEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::rans_sym"*, %"struct.draco::rans_sym"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco8rans_symENS_9allocatorIS2_EEE17__destruct_at_endEPS2_(%"class.std::__2::__vector_base.2"* %this1, %"struct.draco::rans_sym"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi1EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.17", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.17"* @_ZN5draco17RAnsSymbolDecoderILi1EEC2Ev(%"class.draco::RAnsSymbolDecoder.17"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi1EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.17"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi1EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.17"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi1EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.17"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi1EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.17"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi1EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.17"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.17"* @_ZN5draco17RAnsSymbolDecoderILi1EED2Ev(%"class.draco::RAnsSymbolDecoder.17"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi2EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.18", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.18"* @_ZN5draco17RAnsSymbolDecoderILi2EEC2Ev(%"class.draco::RAnsSymbolDecoder.18"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi2EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.18"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi2EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.18"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi2EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.18"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi2EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.18"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi2EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.18"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.18"* @_ZN5draco17RAnsSymbolDecoderILi2EED2Ev(%"class.draco::RAnsSymbolDecoder.18"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi3EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.19", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.19"* @_ZN5draco17RAnsSymbolDecoderILi3EEC2Ev(%"class.draco::RAnsSymbolDecoder.19"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi3EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.19"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi3EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.19"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi3EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.19"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi3EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.19"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi3EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.19"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.19"* @_ZN5draco17RAnsSymbolDecoderILi3EED2Ev(%"class.draco::RAnsSymbolDecoder.19"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi4EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.20", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.20"* @_ZN5draco17RAnsSymbolDecoderILi4EEC2Ev(%"class.draco::RAnsSymbolDecoder.20"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi4EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.20"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi4EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.20"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi4EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.20"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi4EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.20"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi4EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.20"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.20"* @_ZN5draco17RAnsSymbolDecoderILi4EED2Ev(%"class.draco::RAnsSymbolDecoder.20"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi5EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder"* @_ZN5draco17RAnsSymbolDecoderILi5EEC2Ev(%"class.draco::RAnsSymbolDecoder"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi5EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi5EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi5EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi5EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi5EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder"* @_ZN5draco17RAnsSymbolDecoderILi5EED2Ev(%"class.draco::RAnsSymbolDecoder"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi6EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.21", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.21"* @_ZN5draco17RAnsSymbolDecoderILi6EEC2Ev(%"class.draco::RAnsSymbolDecoder.21"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi6EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.21"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi6EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.21"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi6EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.21"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi6EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.21"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi6EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.21"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.21"* @_ZN5draco17RAnsSymbolDecoderILi6EED2Ev(%"class.draco::RAnsSymbolDecoder.21"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi7EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.22", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.22"* @_ZN5draco17RAnsSymbolDecoderILi7EEC2Ev(%"class.draco::RAnsSymbolDecoder.22"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi7EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.22"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi7EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.22"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi7EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.22"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi7EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.22"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi7EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.22"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.22"* @_ZN5draco17RAnsSymbolDecoderILi7EED2Ev(%"class.draco::RAnsSymbolDecoder.22"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi8EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.23", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.23"* @_ZN5draco17RAnsSymbolDecoderILi8EEC2Ev(%"class.draco::RAnsSymbolDecoder.23"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi8EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.23"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi8EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.23"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi8EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.23"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi8EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.23"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi8EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.23"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.23"* @_ZN5draco17RAnsSymbolDecoderILi8EED2Ev(%"class.draco::RAnsSymbolDecoder.23"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi9EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.24", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.24"* @_ZN5draco17RAnsSymbolDecoderILi9EEC2Ev(%"class.draco::RAnsSymbolDecoder.24"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi9EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.24"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi9EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.24"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi9EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.24"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi9EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.24"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi9EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.24"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.24"* @_ZN5draco17RAnsSymbolDecoderILi9EED2Ev(%"class.draco::RAnsSymbolDecoder.24"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi10EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.26", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.26"* @_ZN5draco17RAnsSymbolDecoderILi10EEC2Ev(%"class.draco::RAnsSymbolDecoder.26"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi10EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.26"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi10EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.26"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi10EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.26"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi10EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.26"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi10EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.26"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.26"* @_ZN5draco17RAnsSymbolDecoderILi10EED2Ev(%"class.draco::RAnsSymbolDecoder.26"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi11EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.28", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.28"* @_ZN5draco17RAnsSymbolDecoderILi11EEC2Ev(%"class.draco::RAnsSymbolDecoder.28"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi11EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.28"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi11EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.28"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi11EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.28"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi11EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.28"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi11EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.28"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.28"* @_ZN5draco17RAnsSymbolDecoderILi11EED2Ev(%"class.draco::RAnsSymbolDecoder.28"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi12EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.30", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.30"* @_ZN5draco17RAnsSymbolDecoderILi12EEC2Ev(%"class.draco::RAnsSymbolDecoder.30"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi12EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.30"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi12EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.30"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi12EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.30"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi12EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.30"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi12EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.30"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.30"* @_ZN5draco17RAnsSymbolDecoderILi12EED2Ev(%"class.draco::RAnsSymbolDecoder.30"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi13EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.32", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.32"* @_ZN5draco17RAnsSymbolDecoderILi13EEC2Ev(%"class.draco::RAnsSymbolDecoder.32"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi13EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.32"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi13EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.32"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi13EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.32"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi13EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.32"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi13EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.32"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.32"* @_ZN5draco17RAnsSymbolDecoderILi13EED2Ev(%"class.draco::RAnsSymbolDecoder.32"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi14EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.34", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.34"* @_ZN5draco17RAnsSymbolDecoderILi14EEC2Ev(%"class.draco::RAnsSymbolDecoder.34"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi14EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.34"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi14EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.34"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi14EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.34"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi14EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.34"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi14EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.34"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.34"* @_ZN5draco17RAnsSymbolDecoderILi14EED2Ev(%"class.draco::RAnsSymbolDecoder.34"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi15EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.36", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.36"* @_ZN5draco17RAnsSymbolDecoderILi15EEC2Ev(%"class.draco::RAnsSymbolDecoder.36"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi15EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.36"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi15EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.36"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi15EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.36"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi15EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.36"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi15EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.36"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.36"* @_ZN5draco17RAnsSymbolDecoderILi15EED2Ev(%"class.draco::RAnsSymbolDecoder.36"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi16EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.37", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.37"* @_ZN5draco17RAnsSymbolDecoderILi16EEC2Ev(%"class.draco::RAnsSymbolDecoder.37"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi16EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.37"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi16EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.37"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi16EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.37"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi16EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.37"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi16EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.37"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.37"* @_ZN5draco17RAnsSymbolDecoderILi16EED2Ev(%"class.draco::RAnsSymbolDecoder.37"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi17EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.38", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.38"* @_ZN5draco17RAnsSymbolDecoderILi17EEC2Ev(%"class.draco::RAnsSymbolDecoder.38"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi17EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.38"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi17EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.38"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi17EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.38"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi17EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.38"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi17EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.38"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.38"* @_ZN5draco17RAnsSymbolDecoderILi17EED2Ev(%"class.draco::RAnsSymbolDecoder.38"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco24DecodeRawSymbolsInternalINS_17RAnsSymbolDecoderILi18EEEEEbjPNS_13DecoderBufferEPj(i32 %num_values, %"class.draco::DecoderBuffer"* %src_buffer, i32* %out_values) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %num_values.addr = alloca i32, align 4
  %src_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_values.addr = alloca i32*, align 4
  %decoder = alloca %"class.draco::RAnsSymbolDecoder.39", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %value = alloca i32, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  store %"class.draco::DecoderBuffer"* %src_buffer, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  store i32* %out_values, i32** %out_values.addr, align 4
  %call = call %"class.draco::RAnsSymbolDecoder.39"* @_ZN5draco17RAnsSymbolDecoderILi18EEC2Ev(%"class.draco::RAnsSymbolDecoder.39"* %decoder)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call1 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi18EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.39"* %decoder, %"class.draco::DecoderBuffer"* %0)
  br i1 %call1, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %call2 = call i32 @_ZNK5draco17RAnsSymbolDecoderILi18EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.39"* %decoder)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %src_buffer.addr, align 4
  %call6 = call zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi18EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.39"* %decoder, %"class.draco::DecoderBuffer"* %2)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_values.addr, align 4
  %cmp9 = icmp ult i32 %3, %4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call10 = call i32 @_ZN5draco17RAnsSymbolDecoderILi18EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.39"* %decoder)
  store i32 %call10, i32* %value, align 4
  %5 = load i32, i32* %value, align 4
  %6 = load i32*, i32** %out_values.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN5draco17RAnsSymbolDecoderILi18EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.39"* %decoder)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7, %if.then4, %if.then
  %call11 = call %"class.draco::RAnsSymbolDecoder.39"* @_ZN5draco17RAnsSymbolDecoderILi18EED2Ev(%"class.draco::RAnsSymbolDecoder.39"* %decoder) #8
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.17"* @_ZN5draco17RAnsSymbolDecoderILi1EEC2Ev(%"class.draco::RAnsSymbolDecoder.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.17"*, align 4
  store %"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.17"*, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi1EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.17"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.17"*, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi1EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.17"*, align 4
  store %"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.17"*, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi1EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.17"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.17"*, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi1EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.17"*, align 4
  store %"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.17"*, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi1EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.17"*, align 4
  store %"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.17"*, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.17"* @_ZN5draco17RAnsSymbolDecoderILi1EED2Ev(%"class.draco::RAnsSymbolDecoder.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.17"*, align 4
  store %"class.draco::RAnsSymbolDecoder.17"* %this, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.17"*, %"class.draco::RAnsSymbolDecoder.17"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.17", %"class.draco::RAnsSymbolDecoder.17"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.18"* @_ZN5draco17RAnsSymbolDecoderILi2EEC2Ev(%"class.draco::RAnsSymbolDecoder.18"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.18"*, align 4
  store %"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.18"*, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi2EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.18"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.18"*, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi2EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.18"*, align 4
  store %"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.18"*, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi2EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.18"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.18"*, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi2EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.18"*, align 4
  store %"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.18"*, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi2EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.18"*, align 4
  store %"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.18"*, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.18"* @_ZN5draco17RAnsSymbolDecoderILi2EED2Ev(%"class.draco::RAnsSymbolDecoder.18"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.18"*, align 4
  store %"class.draco::RAnsSymbolDecoder.18"* %this, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.18"*, %"class.draco::RAnsSymbolDecoder.18"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.18", %"class.draco::RAnsSymbolDecoder.18"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.19"* @_ZN5draco17RAnsSymbolDecoderILi3EEC2Ev(%"class.draco::RAnsSymbolDecoder.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.19"*, align 4
  store %"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.19"*, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi3EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.19"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.19"*, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi3EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.19"*, align 4
  store %"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.19"*, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi3EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.19"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.19"*, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi3EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.19"*, align 4
  store %"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.19"*, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi3EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.19"*, align 4
  store %"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.19"*, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.19"* @_ZN5draco17RAnsSymbolDecoderILi3EED2Ev(%"class.draco::RAnsSymbolDecoder.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.19"*, align 4
  store %"class.draco::RAnsSymbolDecoder.19"* %this, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.19"*, %"class.draco::RAnsSymbolDecoder.19"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.19", %"class.draco::RAnsSymbolDecoder.19"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.20"* @_ZN5draco17RAnsSymbolDecoderILi4EEC2Ev(%"class.draco::RAnsSymbolDecoder.20"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.20"*, align 4
  store %"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.20"*, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.20"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi4EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.20"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.20"*, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi4EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.20"*, align 4
  store %"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.20"*, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi4EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.20"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.20"*, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi4EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.20"*, align 4
  store %"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.20"*, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi4EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.20"*, align 4
  store %"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.20"*, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.20"* @_ZN5draco17RAnsSymbolDecoderILi4EED2Ev(%"class.draco::RAnsSymbolDecoder.20"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.20"*, align 4
  store %"class.draco::RAnsSymbolDecoder.20"* %this, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.20"*, %"class.draco::RAnsSymbolDecoder.20"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.20", %"class.draco::RAnsSymbolDecoder.20"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.20"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.21"* @_ZN5draco17RAnsSymbolDecoderILi6EEC2Ev(%"class.draco::RAnsSymbolDecoder.21"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.21"*, align 4
  store %"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.21"*, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.21"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi6EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.21"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.21"*, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi6EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.21"*, align 4
  store %"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.21"*, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi6EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.21"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.21"*, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi6EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.21"*, align 4
  store %"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.21"*, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi6EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.21"*, align 4
  store %"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.21"*, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.21"* @_ZN5draco17RAnsSymbolDecoderILi6EED2Ev(%"class.draco::RAnsSymbolDecoder.21"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.21"*, align 4
  store %"class.draco::RAnsSymbolDecoder.21"* %this, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.21"*, %"class.draco::RAnsSymbolDecoder.21"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.21", %"class.draco::RAnsSymbolDecoder.21"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.21"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.22"* @_ZN5draco17RAnsSymbolDecoderILi7EEC2Ev(%"class.draco::RAnsSymbolDecoder.22"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.22"*, align 4
  store %"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.22"*, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.22"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi7EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.22"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.22"*, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi7EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.22"*, align 4
  store %"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.22"*, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi7EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.22"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.22"*, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi7EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.22"*, align 4
  store %"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.22"*, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi7EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.22"*, align 4
  store %"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.22"*, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.22"* @_ZN5draco17RAnsSymbolDecoderILi7EED2Ev(%"class.draco::RAnsSymbolDecoder.22"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.22"*, align 4
  store %"class.draco::RAnsSymbolDecoder.22"* %this, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.22"*, %"class.draco::RAnsSymbolDecoder.22"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.22", %"class.draco::RAnsSymbolDecoder.22"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.22"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.23"* @_ZN5draco17RAnsSymbolDecoderILi8EEC2Ev(%"class.draco::RAnsSymbolDecoder.23"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.23"*, align 4
  store %"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.23"*, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EEC2Ev(%"class.draco::RAnsDecoder"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.23"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi8EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.23"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.23"*, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi12EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi8EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.23"*, align 4
  store %"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.23"*, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi8EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.23"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.23"*, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi12EE9read_initEPKhi(%"class.draco::RAnsDecoder"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi8EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.23"*, align 4
  store %"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.23"*, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE9rans_readEv(%"class.draco::RAnsDecoder"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi8EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.23"*, align 4
  store %"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.23"*, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi12EE8read_endEv(%"class.draco::RAnsDecoder"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.23"* @_ZN5draco17RAnsSymbolDecoderILi8EED2Ev(%"class.draco::RAnsSymbolDecoder.23"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.23"*, align 4
  store %"class.draco::RAnsSymbolDecoder.23"* %this, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.23"*, %"class.draco::RAnsSymbolDecoder.23"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder"* @_ZN5draco11RAnsDecoderILi12EED2Ev(%"class.draco::RAnsDecoder"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.23", %"class.draco::RAnsSymbolDecoder.23"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.23"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.24"* @_ZN5draco17RAnsSymbolDecoderILi9EEC2Ev(%"class.draco::RAnsSymbolDecoder.24"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.24"*, align 4
  store %"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.24"*, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.25"* @_ZN5draco11RAnsDecoderILi13EEC2Ev(%"class.draco::RAnsDecoder.25"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.24"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi9EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.24"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.24"*, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi13EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.25"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi9EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.24"*, align 4
  store %"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.24"*, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi9EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.24"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.24"*, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi13EE9read_initEPKhi(%"class.draco::RAnsDecoder.25"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi9EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.24"*, align 4
  store %"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.24"*, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi13EE9rans_readEv(%"class.draco::RAnsDecoder.25"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi9EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.24"*, align 4
  store %"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.24"*, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi13EE8read_endEv(%"class.draco::RAnsDecoder.25"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.24"* @_ZN5draco17RAnsSymbolDecoderILi9EED2Ev(%"class.draco::RAnsSymbolDecoder.24"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.24"*, align 4
  store %"class.draco::RAnsSymbolDecoder.24"* %this, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.24"*, %"class.draco::RAnsSymbolDecoder.24"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.25"* @_ZN5draco11RAnsDecoderILi13EED2Ev(%"class.draco::RAnsDecoder.25"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.24", %"class.draco::RAnsSymbolDecoder.24"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.24"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.25"* @_ZN5draco11RAnsDecoderILi13EEC2Ev(%"class.draco::RAnsDecoder.25"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.25"*, align 4
  store %"class.draco::RAnsDecoder.25"* %this, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.25"*, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %lut_table_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %call3 = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_)
  ret %"class.draco::RAnsDecoder.25"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco11RAnsDecoderILi13EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.25"* %this, i32* %token_probs, i32 %num_symbols) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsDecoder.25"*, align 4
  %token_probs.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %cum_prob = alloca i32, align 4
  %act_prob = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::RAnsDecoder.25"* %this, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  store i32* %token_probs, i32** %token_probs.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.25"*, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %lut_table_, i32 8192)
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %probability_table_, i32 %0)
  store i32 0, i32* %cum_prob, align 4
  store i32 0, i32* %act_prob, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %token_probs.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %probability_table_2 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_2, i32 %6) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call, i32 0, i32 0
  store i32 %5, i32* %prob, align 4
  %7 = load i32, i32* %cum_prob, align 4
  %probability_table_3 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_3, i32 %8) #8
  %cum_prob5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 0, i32 1
  store i32 %7, i32* %cum_prob5, align 4
  %9 = load i32*, i32** %token_probs.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %12 = load i32, i32* %cum_prob, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %cum_prob, align 4
  %13 = load i32, i32* %cum_prob, align 4
  %cmp7 = icmp ugt i32 %13, 8192
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %14 = load i32, i32* %act_prob, align 4
  store i32 %14, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %cum_prob, align 4
  %cmp9 = icmp ult i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32, i32* %i, align 4
  %lut_table_11 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 0
  %18 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_11, i32 %18) #8
  store i32 %17, i32* %call12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %20 = load i32, i32* %cum_prob, align 4
  store i32 %20, i32* %act_prob, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc14 = add i32 %21, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %22 = load i32, i32* %cum_prob, align 4
  %cmp16 = icmp ne i32 %22, 8192
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end15
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi13EE9read_initEPKhi(%"class.draco::RAnsDecoder.25"* %this, i8* %buf, i32 %offset) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::RAnsDecoder.25"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"class.draco::RAnsDecoder.25"* %this, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.25"*, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf2 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 0
  store i8* %1, i8** %buf2, align 4
  %2 = load i8*, i8** %buf.addr, align 4
  %3 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %6, 1
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 1
  store i32 %sub5, i32* %buf_offset, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %8 = load i32, i32* %offset.addr, align 4
  %sub7 = sub nsw i32 %8, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 %sub7
  %9 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %9 to i32
  %and = and i32 %conv9, 63
  %ans_10 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_10, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end54

if.else:                                          ; preds = %if.end
  %10 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %10, 1
  br i1 %cmp11, label %if.then12, label %if.else23

if.then12:                                        ; preds = %if.else
  %11 = load i32, i32* %offset.addr, align 4
  %cmp13 = icmp slt i32 %11, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  %12 = load i32, i32* %offset.addr, align 4
  %sub16 = sub nsw i32 %12, 2
  %ans_17 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf_offset18 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_17, i32 0, i32 1
  store i32 %sub16, i32* %buf_offset18, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %14 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %add.ptr19 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr19)
  %and20 = and i32 %call, 16383
  %ans_21 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state22 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_21, i32 0, i32 2
  store i32 %and20, i32* %state22, align 4
  br label %if.end53

if.else23:                                        ; preds = %if.else
  %15 = load i32, i32* %x, align 4
  %cmp24 = icmp eq i32 %15, 2
  br i1 %cmp24, label %if.then25, label %if.else38

if.then25:                                        ; preds = %if.else23
  %16 = load i32, i32* %offset.addr, align 4
  %cmp26 = icmp slt i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.then25
  %17 = load i32, i32* %offset.addr, align 4
  %sub29 = sub nsw i32 %17, 3
  %ans_30 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf_offset31 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_30, i32 0, i32 1
  store i32 %sub29, i32* %buf_offset31, align 4
  %18 = load i8*, i8** %buf.addr, align 4
  %19 = load i32, i32* %offset.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %18, i32 %19
  %add.ptr33 = getelementptr inbounds i8, i8* %add.ptr32, i32 -3
  %call34 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr33)
  %and35 = and i32 %call34, 4194303
  %ans_36 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state37 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_36, i32 0, i32 2
  store i32 %and35, i32* %state37, align 4
  br label %if.end52

if.else38:                                        ; preds = %if.else23
  %20 = load i32, i32* %x, align 4
  %cmp39 = icmp eq i32 %20, 3
  br i1 %cmp39, label %if.then40, label %if.else50

if.then40:                                        ; preds = %if.else38
  %21 = load i32, i32* %offset.addr, align 4
  %sub41 = sub nsw i32 %21, 4
  %ans_42 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf_offset43 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_42, i32 0, i32 1
  store i32 %sub41, i32* %buf_offset43, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %23 = load i32, i32* %offset.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %22, i32 %23
  %add.ptr45 = getelementptr inbounds i8, i8* %add.ptr44, i32 -4
  %call46 = call i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %add.ptr45)
  %and47 = and i32 %call46, 1073741823
  %ans_48 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state49 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_48, i32 0, i32 2
  store i32 %and47, i32* %state49, align 4
  br label %if.end51

if.else50:                                        ; preds = %if.else38
  store i32 1, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.then40
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end28
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end15
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then4
  %ans_55 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state56 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_55, i32 0, i32 2
  %24 = load i32, i32* %state56, align 4
  %add = add i32 %24, 32768
  store i32 %add, i32* %state56, align 4
  %ans_57 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state58 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_57, i32 0, i32 2
  %25 = load i32, i32* %state58, align 4
  %cmp59 = icmp uge i32 %25, 8388608
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end54
  store i32 1, i32* %retval, align 4
  br label %return

if.end61:                                         ; preds = %if.end54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end61, %if.then60, %if.else50, %if.then27, %if.then14, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi13EE9rans_readEv(%"class.draco::RAnsDecoder.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.25"*, align 4
  %rem = alloca i32, align 4
  %quo = alloca i32, align 4
  %sym = alloca %"struct.draco::rans_dec_sym", align 4
  store %"class.draco::RAnsDecoder.25"* %this, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.25"*, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %0, 32768
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %ans_2 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_2, i32 0, i32 1
  %1 = load i32, i32* %buf_offset, align 4
  %cmp3 = icmp sgt i32 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %2 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %2, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %ans_4 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state5 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_4, i32 0, i32 2
  %3 = load i32, i32* %state5, align 4
  %mul = mul i32 %3, 256
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %ans_7 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %buf_offset8 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_7, i32 0, i32 1
  %5 = load i32, i32* %buf_offset8, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %buf_offset8, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %dec
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %add = add i32 %mul, %conv
  %ans_9 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state10 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_9, i32 0, i32 2
  store i32 %add, i32* %state10, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %ans_11 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state12 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_11, i32 0, i32 2
  %7 = load i32, i32* %state12, align 4
  %div = udiv i32 %7, 8192
  store i32 %div, i32* %quo, align 4
  %ans_13 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_13, i32 0, i32 2
  %8 = load i32, i32* %state14, align 4
  %rem15 = urem i32 %8, 8192
  store i32 %rem15, i32* %rem, align 4
  %9 = load i32, i32* %rem, align 4
  call void @_ZN5draco11RAnsDecoderILi13EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.25"* %this1, %"struct.draco::rans_dec_sym"* %sym, i32 %9)
  %10 = load i32, i32* %quo, align 4
  %prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 1
  %11 = load i32, i32* %prob, align 4
  %mul16 = mul i32 %10, %11
  %12 = load i32, i32* %rem, align 4
  %add17 = add i32 %mul16, %12
  %cum_prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 2
  %13 = load i32, i32* %cum_prob, align 4
  %sub = sub i32 %add17, %13
  %ans_18 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state19 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_18, i32 0, i32 2
  store i32 %sub, i32* %state19, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 0
  %14 = load i32, i32* %val, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11RAnsDecoderILi13EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.25"* %this, %"struct.draco::rans_dec_sym"* %out, i32 %rem) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.25"*, align 4
  %out.addr = alloca %"struct.draco::rans_dec_sym"*, align 4
  %rem.addr = alloca i32, align 4
  %symbol = alloca i32, align 4
  store %"class.draco::RAnsDecoder.25"* %this, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  store %"struct.draco::rans_dec_sym"* %out, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  store i32 %rem, i32* %rem.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.25"*, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 0
  %0 = load i32, i32* %rem.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %symbol, align 4
  %2 = load i32, i32* %symbol, align 4
  %3 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %3, i32 0, i32 0
  store i32 %2, i32* %val, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 1
  %4 = load i32, i32* %symbol, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_, i32 %4) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 0, i32 0
  %5 = load i32, i32* %prob, align 4
  %6 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %prob3 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %6, i32 0, i32 1
  store i32 %5, i32* %prob3, align 4
  %probability_table_4 = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 1
  %7 = load i32, i32* %symbol, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_4, i32 %7) #8
  %cum_prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call5, i32 0, i32 1
  %8 = load i32, i32* %cum_prob, align 4
  %9 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %cum_prob6 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %9, i32 0, i32 2
  store i32 %8, i32* %cum_prob6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi13EE8read_endEv(%"class.draco::RAnsDecoder.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.25"*, align 4
  store %"class.draco::RAnsDecoder.25"* %this, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.25"*, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %0, 32768
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.25"* @_ZN5draco11RAnsDecoderILi13EED2Ev(%"class.draco::RAnsDecoder.25"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.25"*, align 4
  store %"class.draco::RAnsDecoder.25"* %this, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.25"*, %"class.draco::RAnsDecoder.25"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.25", %"class.draco::RAnsDecoder.25"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %lut_table_) #8
  ret %"class.draco::RAnsDecoder.25"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.26"* @_ZN5draco17RAnsSymbolDecoderILi10EEC2Ev(%"class.draco::RAnsSymbolDecoder.26"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.26"*, align 4
  store %"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.26"*, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.27"* @_ZN5draco11RAnsDecoderILi15EEC2Ev(%"class.draco::RAnsDecoder.27"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.26"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi10EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.26"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.26"*, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi15EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.27"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi10EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.26"*, align 4
  store %"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.26"*, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi10EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.26"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.26"*, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi15EE9read_initEPKhi(%"class.draco::RAnsDecoder.27"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi10EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.26"*, align 4
  store %"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.26"*, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi15EE9rans_readEv(%"class.draco::RAnsDecoder.27"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi10EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.26"*, align 4
  store %"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.26"*, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi15EE8read_endEv(%"class.draco::RAnsDecoder.27"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.26"* @_ZN5draco17RAnsSymbolDecoderILi10EED2Ev(%"class.draco::RAnsSymbolDecoder.26"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.26"*, align 4
  store %"class.draco::RAnsSymbolDecoder.26"* %this, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.26"*, %"class.draco::RAnsSymbolDecoder.26"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.27"* @_ZN5draco11RAnsDecoderILi15EED2Ev(%"class.draco::RAnsDecoder.27"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.26", %"class.draco::RAnsSymbolDecoder.26"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.26"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.27"* @_ZN5draco11RAnsDecoderILi15EEC2Ev(%"class.draco::RAnsDecoder.27"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.27"*, align 4
  store %"class.draco::RAnsDecoder.27"* %this, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.27"*, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %lut_table_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %call3 = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_)
  ret %"class.draco::RAnsDecoder.27"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco11RAnsDecoderILi15EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.27"* %this, i32* %token_probs, i32 %num_symbols) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsDecoder.27"*, align 4
  %token_probs.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %cum_prob = alloca i32, align 4
  %act_prob = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::RAnsDecoder.27"* %this, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  store i32* %token_probs, i32** %token_probs.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.27"*, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %lut_table_, i32 32768)
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %probability_table_, i32 %0)
  store i32 0, i32* %cum_prob, align 4
  store i32 0, i32* %act_prob, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %token_probs.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %probability_table_2 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_2, i32 %6) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call, i32 0, i32 0
  store i32 %5, i32* %prob, align 4
  %7 = load i32, i32* %cum_prob, align 4
  %probability_table_3 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_3, i32 %8) #8
  %cum_prob5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 0, i32 1
  store i32 %7, i32* %cum_prob5, align 4
  %9 = load i32*, i32** %token_probs.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %12 = load i32, i32* %cum_prob, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %cum_prob, align 4
  %13 = load i32, i32* %cum_prob, align 4
  %cmp7 = icmp ugt i32 %13, 32768
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %14 = load i32, i32* %act_prob, align 4
  store i32 %14, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %cum_prob, align 4
  %cmp9 = icmp ult i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32, i32* %i, align 4
  %lut_table_11 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 0
  %18 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_11, i32 %18) #8
  store i32 %17, i32* %call12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %20 = load i32, i32* %cum_prob, align 4
  store i32 %20, i32* %act_prob, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc14 = add i32 %21, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %22 = load i32, i32* %cum_prob, align 4
  %cmp16 = icmp ne i32 %22, 32768
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end15
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi15EE9read_initEPKhi(%"class.draco::RAnsDecoder.27"* %this, i8* %buf, i32 %offset) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::RAnsDecoder.27"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"class.draco::RAnsDecoder.27"* %this, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.27"*, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf2 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 0
  store i8* %1, i8** %buf2, align 4
  %2 = load i8*, i8** %buf.addr, align 4
  %3 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %6, 1
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 1
  store i32 %sub5, i32* %buf_offset, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %8 = load i32, i32* %offset.addr, align 4
  %sub7 = sub nsw i32 %8, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 %sub7
  %9 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %9 to i32
  %and = and i32 %conv9, 63
  %ans_10 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_10, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end54

if.else:                                          ; preds = %if.end
  %10 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %10, 1
  br i1 %cmp11, label %if.then12, label %if.else23

if.then12:                                        ; preds = %if.else
  %11 = load i32, i32* %offset.addr, align 4
  %cmp13 = icmp slt i32 %11, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  %12 = load i32, i32* %offset.addr, align 4
  %sub16 = sub nsw i32 %12, 2
  %ans_17 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf_offset18 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_17, i32 0, i32 1
  store i32 %sub16, i32* %buf_offset18, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %14 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %add.ptr19 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr19)
  %and20 = and i32 %call, 16383
  %ans_21 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state22 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_21, i32 0, i32 2
  store i32 %and20, i32* %state22, align 4
  br label %if.end53

if.else23:                                        ; preds = %if.else
  %15 = load i32, i32* %x, align 4
  %cmp24 = icmp eq i32 %15, 2
  br i1 %cmp24, label %if.then25, label %if.else38

if.then25:                                        ; preds = %if.else23
  %16 = load i32, i32* %offset.addr, align 4
  %cmp26 = icmp slt i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.then25
  %17 = load i32, i32* %offset.addr, align 4
  %sub29 = sub nsw i32 %17, 3
  %ans_30 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf_offset31 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_30, i32 0, i32 1
  store i32 %sub29, i32* %buf_offset31, align 4
  %18 = load i8*, i8** %buf.addr, align 4
  %19 = load i32, i32* %offset.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %18, i32 %19
  %add.ptr33 = getelementptr inbounds i8, i8* %add.ptr32, i32 -3
  %call34 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr33)
  %and35 = and i32 %call34, 4194303
  %ans_36 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state37 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_36, i32 0, i32 2
  store i32 %and35, i32* %state37, align 4
  br label %if.end52

if.else38:                                        ; preds = %if.else23
  %20 = load i32, i32* %x, align 4
  %cmp39 = icmp eq i32 %20, 3
  br i1 %cmp39, label %if.then40, label %if.else50

if.then40:                                        ; preds = %if.else38
  %21 = load i32, i32* %offset.addr, align 4
  %sub41 = sub nsw i32 %21, 4
  %ans_42 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf_offset43 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_42, i32 0, i32 1
  store i32 %sub41, i32* %buf_offset43, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %23 = load i32, i32* %offset.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %22, i32 %23
  %add.ptr45 = getelementptr inbounds i8, i8* %add.ptr44, i32 -4
  %call46 = call i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %add.ptr45)
  %and47 = and i32 %call46, 1073741823
  %ans_48 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state49 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_48, i32 0, i32 2
  store i32 %and47, i32* %state49, align 4
  br label %if.end51

if.else50:                                        ; preds = %if.else38
  store i32 1, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.then40
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end28
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end15
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then4
  %ans_55 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state56 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_55, i32 0, i32 2
  %24 = load i32, i32* %state56, align 4
  %add = add i32 %24, 131072
  store i32 %add, i32* %state56, align 4
  %ans_57 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state58 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_57, i32 0, i32 2
  %25 = load i32, i32* %state58, align 4
  %cmp59 = icmp uge i32 %25, 33554432
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end54
  store i32 1, i32* %retval, align 4
  br label %return

if.end61:                                         ; preds = %if.end54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end61, %if.then60, %if.else50, %if.then27, %if.then14, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi15EE9rans_readEv(%"class.draco::RAnsDecoder.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.27"*, align 4
  %rem = alloca i32, align 4
  %quo = alloca i32, align 4
  %sym = alloca %"struct.draco::rans_dec_sym", align 4
  store %"class.draco::RAnsDecoder.27"* %this, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.27"*, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %0, 131072
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %ans_2 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_2, i32 0, i32 1
  %1 = load i32, i32* %buf_offset, align 4
  %cmp3 = icmp sgt i32 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %2 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %2, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %ans_4 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state5 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_4, i32 0, i32 2
  %3 = load i32, i32* %state5, align 4
  %mul = mul i32 %3, 256
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %ans_7 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %buf_offset8 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_7, i32 0, i32 1
  %5 = load i32, i32* %buf_offset8, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %buf_offset8, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %dec
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %add = add i32 %mul, %conv
  %ans_9 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state10 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_9, i32 0, i32 2
  store i32 %add, i32* %state10, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %ans_11 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state12 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_11, i32 0, i32 2
  %7 = load i32, i32* %state12, align 4
  %div = udiv i32 %7, 32768
  store i32 %div, i32* %quo, align 4
  %ans_13 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_13, i32 0, i32 2
  %8 = load i32, i32* %state14, align 4
  %rem15 = urem i32 %8, 32768
  store i32 %rem15, i32* %rem, align 4
  %9 = load i32, i32* %rem, align 4
  call void @_ZN5draco11RAnsDecoderILi15EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.27"* %this1, %"struct.draco::rans_dec_sym"* %sym, i32 %9)
  %10 = load i32, i32* %quo, align 4
  %prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 1
  %11 = load i32, i32* %prob, align 4
  %mul16 = mul i32 %10, %11
  %12 = load i32, i32* %rem, align 4
  %add17 = add i32 %mul16, %12
  %cum_prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 2
  %13 = load i32, i32* %cum_prob, align 4
  %sub = sub i32 %add17, %13
  %ans_18 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state19 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_18, i32 0, i32 2
  store i32 %sub, i32* %state19, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 0
  %14 = load i32, i32* %val, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11RAnsDecoderILi15EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.27"* %this, %"struct.draco::rans_dec_sym"* %out, i32 %rem) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.27"*, align 4
  %out.addr = alloca %"struct.draco::rans_dec_sym"*, align 4
  %rem.addr = alloca i32, align 4
  %symbol = alloca i32, align 4
  store %"class.draco::RAnsDecoder.27"* %this, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  store %"struct.draco::rans_dec_sym"* %out, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  store i32 %rem, i32* %rem.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.27"*, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 0
  %0 = load i32, i32* %rem.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %symbol, align 4
  %2 = load i32, i32* %symbol, align 4
  %3 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %3, i32 0, i32 0
  store i32 %2, i32* %val, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 1
  %4 = load i32, i32* %symbol, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_, i32 %4) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 0, i32 0
  %5 = load i32, i32* %prob, align 4
  %6 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %prob3 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %6, i32 0, i32 1
  store i32 %5, i32* %prob3, align 4
  %probability_table_4 = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 1
  %7 = load i32, i32* %symbol, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_4, i32 %7) #8
  %cum_prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call5, i32 0, i32 1
  %8 = load i32, i32* %cum_prob, align 4
  %9 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %cum_prob6 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %9, i32 0, i32 2
  store i32 %8, i32* %cum_prob6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi15EE8read_endEv(%"class.draco::RAnsDecoder.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.27"*, align 4
  store %"class.draco::RAnsDecoder.27"* %this, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.27"*, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %0, 131072
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.27"* @_ZN5draco11RAnsDecoderILi15EED2Ev(%"class.draco::RAnsDecoder.27"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.27"*, align 4
  store %"class.draco::RAnsDecoder.27"* %this, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.27"*, %"class.draco::RAnsDecoder.27"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.27", %"class.draco::RAnsDecoder.27"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %lut_table_) #8
  ret %"class.draco::RAnsDecoder.27"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.28"* @_ZN5draco17RAnsSymbolDecoderILi11EEC2Ev(%"class.draco::RAnsSymbolDecoder.28"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.28"*, align 4
  store %"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.28"*, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.29"* @_ZN5draco11RAnsDecoderILi16EEC2Ev(%"class.draco::RAnsDecoder.29"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.28"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi11EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.28"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.28"*, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi16EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.29"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi11EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.28"*, align 4
  store %"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.28"*, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi11EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.28"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.28"*, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi16EE9read_initEPKhi(%"class.draco::RAnsDecoder.29"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi11EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.28"*, align 4
  store %"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.28"*, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi16EE9rans_readEv(%"class.draco::RAnsDecoder.29"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi11EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.28"*, align 4
  store %"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.28"*, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi16EE8read_endEv(%"class.draco::RAnsDecoder.29"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.28"* @_ZN5draco17RAnsSymbolDecoderILi11EED2Ev(%"class.draco::RAnsSymbolDecoder.28"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.28"*, align 4
  store %"class.draco::RAnsSymbolDecoder.28"* %this, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.28"*, %"class.draco::RAnsSymbolDecoder.28"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.29"* @_ZN5draco11RAnsDecoderILi16EED2Ev(%"class.draco::RAnsDecoder.29"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.28", %"class.draco::RAnsSymbolDecoder.28"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.28"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.29"* @_ZN5draco11RAnsDecoderILi16EEC2Ev(%"class.draco::RAnsDecoder.29"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.29"*, align 4
  store %"class.draco::RAnsDecoder.29"* %this, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.29"*, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %lut_table_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %call3 = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_)
  ret %"class.draco::RAnsDecoder.29"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco11RAnsDecoderILi16EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.29"* %this, i32* %token_probs, i32 %num_symbols) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsDecoder.29"*, align 4
  %token_probs.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %cum_prob = alloca i32, align 4
  %act_prob = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::RAnsDecoder.29"* %this, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  store i32* %token_probs, i32** %token_probs.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.29"*, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %lut_table_, i32 65536)
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %probability_table_, i32 %0)
  store i32 0, i32* %cum_prob, align 4
  store i32 0, i32* %act_prob, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %token_probs.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %probability_table_2 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_2, i32 %6) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call, i32 0, i32 0
  store i32 %5, i32* %prob, align 4
  %7 = load i32, i32* %cum_prob, align 4
  %probability_table_3 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_3, i32 %8) #8
  %cum_prob5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 0, i32 1
  store i32 %7, i32* %cum_prob5, align 4
  %9 = load i32*, i32** %token_probs.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %12 = load i32, i32* %cum_prob, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %cum_prob, align 4
  %13 = load i32, i32* %cum_prob, align 4
  %cmp7 = icmp ugt i32 %13, 65536
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %14 = load i32, i32* %act_prob, align 4
  store i32 %14, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %cum_prob, align 4
  %cmp9 = icmp ult i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32, i32* %i, align 4
  %lut_table_11 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 0
  %18 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_11, i32 %18) #8
  store i32 %17, i32* %call12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %20 = load i32, i32* %cum_prob, align 4
  store i32 %20, i32* %act_prob, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc14 = add i32 %21, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %22 = load i32, i32* %cum_prob, align 4
  %cmp16 = icmp ne i32 %22, 65536
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end15
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi16EE9read_initEPKhi(%"class.draco::RAnsDecoder.29"* %this, i8* %buf, i32 %offset) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::RAnsDecoder.29"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"class.draco::RAnsDecoder.29"* %this, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.29"*, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf2 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 0
  store i8* %1, i8** %buf2, align 4
  %2 = load i8*, i8** %buf.addr, align 4
  %3 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %6, 1
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 1
  store i32 %sub5, i32* %buf_offset, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %8 = load i32, i32* %offset.addr, align 4
  %sub7 = sub nsw i32 %8, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 %sub7
  %9 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %9 to i32
  %and = and i32 %conv9, 63
  %ans_10 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_10, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end54

if.else:                                          ; preds = %if.end
  %10 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %10, 1
  br i1 %cmp11, label %if.then12, label %if.else23

if.then12:                                        ; preds = %if.else
  %11 = load i32, i32* %offset.addr, align 4
  %cmp13 = icmp slt i32 %11, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  %12 = load i32, i32* %offset.addr, align 4
  %sub16 = sub nsw i32 %12, 2
  %ans_17 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf_offset18 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_17, i32 0, i32 1
  store i32 %sub16, i32* %buf_offset18, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %14 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %add.ptr19 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr19)
  %and20 = and i32 %call, 16383
  %ans_21 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state22 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_21, i32 0, i32 2
  store i32 %and20, i32* %state22, align 4
  br label %if.end53

if.else23:                                        ; preds = %if.else
  %15 = load i32, i32* %x, align 4
  %cmp24 = icmp eq i32 %15, 2
  br i1 %cmp24, label %if.then25, label %if.else38

if.then25:                                        ; preds = %if.else23
  %16 = load i32, i32* %offset.addr, align 4
  %cmp26 = icmp slt i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.then25
  %17 = load i32, i32* %offset.addr, align 4
  %sub29 = sub nsw i32 %17, 3
  %ans_30 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf_offset31 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_30, i32 0, i32 1
  store i32 %sub29, i32* %buf_offset31, align 4
  %18 = load i8*, i8** %buf.addr, align 4
  %19 = load i32, i32* %offset.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %18, i32 %19
  %add.ptr33 = getelementptr inbounds i8, i8* %add.ptr32, i32 -3
  %call34 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr33)
  %and35 = and i32 %call34, 4194303
  %ans_36 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state37 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_36, i32 0, i32 2
  store i32 %and35, i32* %state37, align 4
  br label %if.end52

if.else38:                                        ; preds = %if.else23
  %20 = load i32, i32* %x, align 4
  %cmp39 = icmp eq i32 %20, 3
  br i1 %cmp39, label %if.then40, label %if.else50

if.then40:                                        ; preds = %if.else38
  %21 = load i32, i32* %offset.addr, align 4
  %sub41 = sub nsw i32 %21, 4
  %ans_42 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf_offset43 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_42, i32 0, i32 1
  store i32 %sub41, i32* %buf_offset43, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %23 = load i32, i32* %offset.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %22, i32 %23
  %add.ptr45 = getelementptr inbounds i8, i8* %add.ptr44, i32 -4
  %call46 = call i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %add.ptr45)
  %and47 = and i32 %call46, 1073741823
  %ans_48 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state49 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_48, i32 0, i32 2
  store i32 %and47, i32* %state49, align 4
  br label %if.end51

if.else50:                                        ; preds = %if.else38
  store i32 1, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.then40
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end28
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end15
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then4
  %ans_55 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state56 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_55, i32 0, i32 2
  %24 = load i32, i32* %state56, align 4
  %add = add i32 %24, 262144
  store i32 %add, i32* %state56, align 4
  %ans_57 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state58 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_57, i32 0, i32 2
  %25 = load i32, i32* %state58, align 4
  %cmp59 = icmp uge i32 %25, 67108864
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end54
  store i32 1, i32* %retval, align 4
  br label %return

if.end61:                                         ; preds = %if.end54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end61, %if.then60, %if.else50, %if.then27, %if.then14, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi16EE9rans_readEv(%"class.draco::RAnsDecoder.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.29"*, align 4
  %rem = alloca i32, align 4
  %quo = alloca i32, align 4
  %sym = alloca %"struct.draco::rans_dec_sym", align 4
  store %"class.draco::RAnsDecoder.29"* %this, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.29"*, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %0, 262144
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %ans_2 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_2, i32 0, i32 1
  %1 = load i32, i32* %buf_offset, align 4
  %cmp3 = icmp sgt i32 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %2 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %2, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %ans_4 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state5 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_4, i32 0, i32 2
  %3 = load i32, i32* %state5, align 4
  %mul = mul i32 %3, 256
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %ans_7 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %buf_offset8 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_7, i32 0, i32 1
  %5 = load i32, i32* %buf_offset8, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %buf_offset8, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %dec
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %add = add i32 %mul, %conv
  %ans_9 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state10 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_9, i32 0, i32 2
  store i32 %add, i32* %state10, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %ans_11 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state12 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_11, i32 0, i32 2
  %7 = load i32, i32* %state12, align 4
  %div = udiv i32 %7, 65536
  store i32 %div, i32* %quo, align 4
  %ans_13 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_13, i32 0, i32 2
  %8 = load i32, i32* %state14, align 4
  %rem15 = urem i32 %8, 65536
  store i32 %rem15, i32* %rem, align 4
  %9 = load i32, i32* %rem, align 4
  call void @_ZN5draco11RAnsDecoderILi16EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.29"* %this1, %"struct.draco::rans_dec_sym"* %sym, i32 %9)
  %10 = load i32, i32* %quo, align 4
  %prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 1
  %11 = load i32, i32* %prob, align 4
  %mul16 = mul i32 %10, %11
  %12 = load i32, i32* %rem, align 4
  %add17 = add i32 %mul16, %12
  %cum_prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 2
  %13 = load i32, i32* %cum_prob, align 4
  %sub = sub i32 %add17, %13
  %ans_18 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state19 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_18, i32 0, i32 2
  store i32 %sub, i32* %state19, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 0
  %14 = load i32, i32* %val, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11RAnsDecoderILi16EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.29"* %this, %"struct.draco::rans_dec_sym"* %out, i32 %rem) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.29"*, align 4
  %out.addr = alloca %"struct.draco::rans_dec_sym"*, align 4
  %rem.addr = alloca i32, align 4
  %symbol = alloca i32, align 4
  store %"class.draco::RAnsDecoder.29"* %this, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  store %"struct.draco::rans_dec_sym"* %out, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  store i32 %rem, i32* %rem.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.29"*, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 0
  %0 = load i32, i32* %rem.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %symbol, align 4
  %2 = load i32, i32* %symbol, align 4
  %3 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %3, i32 0, i32 0
  store i32 %2, i32* %val, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 1
  %4 = load i32, i32* %symbol, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_, i32 %4) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 0, i32 0
  %5 = load i32, i32* %prob, align 4
  %6 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %prob3 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %6, i32 0, i32 1
  store i32 %5, i32* %prob3, align 4
  %probability_table_4 = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 1
  %7 = load i32, i32* %symbol, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_4, i32 %7) #8
  %cum_prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call5, i32 0, i32 1
  %8 = load i32, i32* %cum_prob, align 4
  %9 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %cum_prob6 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %9, i32 0, i32 2
  store i32 %8, i32* %cum_prob6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi16EE8read_endEv(%"class.draco::RAnsDecoder.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.29"*, align 4
  store %"class.draco::RAnsDecoder.29"* %this, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.29"*, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %0, 262144
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.29"* @_ZN5draco11RAnsDecoderILi16EED2Ev(%"class.draco::RAnsDecoder.29"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.29"*, align 4
  store %"class.draco::RAnsDecoder.29"* %this, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.29"*, %"class.draco::RAnsDecoder.29"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.29", %"class.draco::RAnsDecoder.29"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %lut_table_) #8
  ret %"class.draco::RAnsDecoder.29"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.30"* @_ZN5draco17RAnsSymbolDecoderILi12EEC2Ev(%"class.draco::RAnsSymbolDecoder.30"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.30"*, align 4
  store %"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.30"*, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.31"* @_ZN5draco11RAnsDecoderILi18EEC2Ev(%"class.draco::RAnsDecoder.31"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.30"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi12EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.30"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.30"*, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi18EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.31"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi12EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.30"*, align 4
  store %"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.30"*, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi12EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.30"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.30"*, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi18EE9read_initEPKhi(%"class.draco::RAnsDecoder.31"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi12EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.30"*, align 4
  store %"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.30"*, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi18EE9rans_readEv(%"class.draco::RAnsDecoder.31"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi12EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.30"*, align 4
  store %"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.30"*, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi18EE8read_endEv(%"class.draco::RAnsDecoder.31"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.30"* @_ZN5draco17RAnsSymbolDecoderILi12EED2Ev(%"class.draco::RAnsSymbolDecoder.30"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.30"*, align 4
  store %"class.draco::RAnsSymbolDecoder.30"* %this, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.30"*, %"class.draco::RAnsSymbolDecoder.30"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.31"* @_ZN5draco11RAnsDecoderILi18EED2Ev(%"class.draco::RAnsDecoder.31"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.30", %"class.draco::RAnsSymbolDecoder.30"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.30"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.31"* @_ZN5draco11RAnsDecoderILi18EEC2Ev(%"class.draco::RAnsDecoder.31"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.31"*, align 4
  store %"class.draco::RAnsDecoder.31"* %this, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.31"*, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %lut_table_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %call3 = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_)
  ret %"class.draco::RAnsDecoder.31"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco11RAnsDecoderILi18EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.31"* %this, i32* %token_probs, i32 %num_symbols) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsDecoder.31"*, align 4
  %token_probs.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %cum_prob = alloca i32, align 4
  %act_prob = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::RAnsDecoder.31"* %this, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  store i32* %token_probs, i32** %token_probs.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.31"*, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %lut_table_, i32 262144)
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %probability_table_, i32 %0)
  store i32 0, i32* %cum_prob, align 4
  store i32 0, i32* %act_prob, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %token_probs.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %probability_table_2 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_2, i32 %6) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call, i32 0, i32 0
  store i32 %5, i32* %prob, align 4
  %7 = load i32, i32* %cum_prob, align 4
  %probability_table_3 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_3, i32 %8) #8
  %cum_prob5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 0, i32 1
  store i32 %7, i32* %cum_prob5, align 4
  %9 = load i32*, i32** %token_probs.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %12 = load i32, i32* %cum_prob, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %cum_prob, align 4
  %13 = load i32, i32* %cum_prob, align 4
  %cmp7 = icmp ugt i32 %13, 262144
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %14 = load i32, i32* %act_prob, align 4
  store i32 %14, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %cum_prob, align 4
  %cmp9 = icmp ult i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32, i32* %i, align 4
  %lut_table_11 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 0
  %18 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_11, i32 %18) #8
  store i32 %17, i32* %call12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %20 = load i32, i32* %cum_prob, align 4
  store i32 %20, i32* %act_prob, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc14 = add i32 %21, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %22 = load i32, i32* %cum_prob, align 4
  %cmp16 = icmp ne i32 %22, 262144
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end15
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi18EE9read_initEPKhi(%"class.draco::RAnsDecoder.31"* %this, i8* %buf, i32 %offset) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::RAnsDecoder.31"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"class.draco::RAnsDecoder.31"* %this, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.31"*, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf2 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 0
  store i8* %1, i8** %buf2, align 4
  %2 = load i8*, i8** %buf.addr, align 4
  %3 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %6, 1
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 1
  store i32 %sub5, i32* %buf_offset, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %8 = load i32, i32* %offset.addr, align 4
  %sub7 = sub nsw i32 %8, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 %sub7
  %9 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %9 to i32
  %and = and i32 %conv9, 63
  %ans_10 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_10, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end54

if.else:                                          ; preds = %if.end
  %10 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %10, 1
  br i1 %cmp11, label %if.then12, label %if.else23

if.then12:                                        ; preds = %if.else
  %11 = load i32, i32* %offset.addr, align 4
  %cmp13 = icmp slt i32 %11, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  %12 = load i32, i32* %offset.addr, align 4
  %sub16 = sub nsw i32 %12, 2
  %ans_17 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf_offset18 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_17, i32 0, i32 1
  store i32 %sub16, i32* %buf_offset18, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %14 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %add.ptr19 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr19)
  %and20 = and i32 %call, 16383
  %ans_21 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state22 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_21, i32 0, i32 2
  store i32 %and20, i32* %state22, align 4
  br label %if.end53

if.else23:                                        ; preds = %if.else
  %15 = load i32, i32* %x, align 4
  %cmp24 = icmp eq i32 %15, 2
  br i1 %cmp24, label %if.then25, label %if.else38

if.then25:                                        ; preds = %if.else23
  %16 = load i32, i32* %offset.addr, align 4
  %cmp26 = icmp slt i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.then25
  %17 = load i32, i32* %offset.addr, align 4
  %sub29 = sub nsw i32 %17, 3
  %ans_30 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf_offset31 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_30, i32 0, i32 1
  store i32 %sub29, i32* %buf_offset31, align 4
  %18 = load i8*, i8** %buf.addr, align 4
  %19 = load i32, i32* %offset.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %18, i32 %19
  %add.ptr33 = getelementptr inbounds i8, i8* %add.ptr32, i32 -3
  %call34 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr33)
  %and35 = and i32 %call34, 4194303
  %ans_36 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state37 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_36, i32 0, i32 2
  store i32 %and35, i32* %state37, align 4
  br label %if.end52

if.else38:                                        ; preds = %if.else23
  %20 = load i32, i32* %x, align 4
  %cmp39 = icmp eq i32 %20, 3
  br i1 %cmp39, label %if.then40, label %if.else50

if.then40:                                        ; preds = %if.else38
  %21 = load i32, i32* %offset.addr, align 4
  %sub41 = sub nsw i32 %21, 4
  %ans_42 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf_offset43 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_42, i32 0, i32 1
  store i32 %sub41, i32* %buf_offset43, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %23 = load i32, i32* %offset.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %22, i32 %23
  %add.ptr45 = getelementptr inbounds i8, i8* %add.ptr44, i32 -4
  %call46 = call i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %add.ptr45)
  %and47 = and i32 %call46, 1073741823
  %ans_48 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state49 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_48, i32 0, i32 2
  store i32 %and47, i32* %state49, align 4
  br label %if.end51

if.else50:                                        ; preds = %if.else38
  store i32 1, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.then40
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end28
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end15
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then4
  %ans_55 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state56 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_55, i32 0, i32 2
  %24 = load i32, i32* %state56, align 4
  %add = add i32 %24, 1048576
  store i32 %add, i32* %state56, align 4
  %ans_57 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state58 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_57, i32 0, i32 2
  %25 = load i32, i32* %state58, align 4
  %cmp59 = icmp uge i32 %25, 268435456
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end54
  store i32 1, i32* %retval, align 4
  br label %return

if.end61:                                         ; preds = %if.end54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end61, %if.then60, %if.else50, %if.then27, %if.then14, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi18EE9rans_readEv(%"class.draco::RAnsDecoder.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.31"*, align 4
  %rem = alloca i32, align 4
  %quo = alloca i32, align 4
  %sym = alloca %"struct.draco::rans_dec_sym", align 4
  store %"class.draco::RAnsDecoder.31"* %this, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.31"*, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %0, 1048576
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %ans_2 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_2, i32 0, i32 1
  %1 = load i32, i32* %buf_offset, align 4
  %cmp3 = icmp sgt i32 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %2 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %2, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %ans_4 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state5 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_4, i32 0, i32 2
  %3 = load i32, i32* %state5, align 4
  %mul = mul i32 %3, 256
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %ans_7 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %buf_offset8 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_7, i32 0, i32 1
  %5 = load i32, i32* %buf_offset8, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %buf_offset8, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %dec
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %add = add i32 %mul, %conv
  %ans_9 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state10 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_9, i32 0, i32 2
  store i32 %add, i32* %state10, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %ans_11 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state12 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_11, i32 0, i32 2
  %7 = load i32, i32* %state12, align 4
  %div = udiv i32 %7, 262144
  store i32 %div, i32* %quo, align 4
  %ans_13 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_13, i32 0, i32 2
  %8 = load i32, i32* %state14, align 4
  %rem15 = urem i32 %8, 262144
  store i32 %rem15, i32* %rem, align 4
  %9 = load i32, i32* %rem, align 4
  call void @_ZN5draco11RAnsDecoderILi18EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.31"* %this1, %"struct.draco::rans_dec_sym"* %sym, i32 %9)
  %10 = load i32, i32* %quo, align 4
  %prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 1
  %11 = load i32, i32* %prob, align 4
  %mul16 = mul i32 %10, %11
  %12 = load i32, i32* %rem, align 4
  %add17 = add i32 %mul16, %12
  %cum_prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 2
  %13 = load i32, i32* %cum_prob, align 4
  %sub = sub i32 %add17, %13
  %ans_18 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state19 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_18, i32 0, i32 2
  store i32 %sub, i32* %state19, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 0
  %14 = load i32, i32* %val, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11RAnsDecoderILi18EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.31"* %this, %"struct.draco::rans_dec_sym"* %out, i32 %rem) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.31"*, align 4
  %out.addr = alloca %"struct.draco::rans_dec_sym"*, align 4
  %rem.addr = alloca i32, align 4
  %symbol = alloca i32, align 4
  store %"class.draco::RAnsDecoder.31"* %this, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  store %"struct.draco::rans_dec_sym"* %out, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  store i32 %rem, i32* %rem.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.31"*, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 0
  %0 = load i32, i32* %rem.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %symbol, align 4
  %2 = load i32, i32* %symbol, align 4
  %3 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %3, i32 0, i32 0
  store i32 %2, i32* %val, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 1
  %4 = load i32, i32* %symbol, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_, i32 %4) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 0, i32 0
  %5 = load i32, i32* %prob, align 4
  %6 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %prob3 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %6, i32 0, i32 1
  store i32 %5, i32* %prob3, align 4
  %probability_table_4 = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 1
  %7 = load i32, i32* %symbol, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_4, i32 %7) #8
  %cum_prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call5, i32 0, i32 1
  %8 = load i32, i32* %cum_prob, align 4
  %9 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %cum_prob6 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %9, i32 0, i32 2
  store i32 %8, i32* %cum_prob6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi18EE8read_endEv(%"class.draco::RAnsDecoder.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.31"*, align 4
  store %"class.draco::RAnsDecoder.31"* %this, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.31"*, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %0, 1048576
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.31"* @_ZN5draco11RAnsDecoderILi18EED2Ev(%"class.draco::RAnsDecoder.31"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.31"*, align 4
  store %"class.draco::RAnsDecoder.31"* %this, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.31"*, %"class.draco::RAnsDecoder.31"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.31", %"class.draco::RAnsDecoder.31"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %lut_table_) #8
  ret %"class.draco::RAnsDecoder.31"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.32"* @_ZN5draco17RAnsSymbolDecoderILi13EEC2Ev(%"class.draco::RAnsSymbolDecoder.32"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.32"*, align 4
  store %"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.32"*, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.33"* @_ZN5draco11RAnsDecoderILi19EEC2Ev(%"class.draco::RAnsDecoder.33"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.32"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi13EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.32"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.32"*, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi19EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.33"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi13EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.32"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.32"*, align 4
  store %"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.32"*, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi13EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.32"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.32"*, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi19EE9read_initEPKhi(%"class.draco::RAnsDecoder.33"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi13EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.32"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.32"*, align 4
  store %"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.32"*, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi19EE9rans_readEv(%"class.draco::RAnsDecoder.33"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi13EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.32"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.32"*, align 4
  store %"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.32"*, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi19EE8read_endEv(%"class.draco::RAnsDecoder.33"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.32"* @_ZN5draco17RAnsSymbolDecoderILi13EED2Ev(%"class.draco::RAnsSymbolDecoder.32"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.32"*, align 4
  store %"class.draco::RAnsSymbolDecoder.32"* %this, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.32"*, %"class.draco::RAnsSymbolDecoder.32"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.33"* @_ZN5draco11RAnsDecoderILi19EED2Ev(%"class.draco::RAnsDecoder.33"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.32", %"class.draco::RAnsSymbolDecoder.32"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.32"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.33"* @_ZN5draco11RAnsDecoderILi19EEC2Ev(%"class.draco::RAnsDecoder.33"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.33"*, align 4
  store %"class.draco::RAnsDecoder.33"* %this, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.33"*, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %lut_table_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %call3 = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_)
  ret %"class.draco::RAnsDecoder.33"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco11RAnsDecoderILi19EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.33"* %this, i32* %token_probs, i32 %num_symbols) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsDecoder.33"*, align 4
  %token_probs.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %cum_prob = alloca i32, align 4
  %act_prob = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::RAnsDecoder.33"* %this, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  store i32* %token_probs, i32** %token_probs.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.33"*, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %lut_table_, i32 524288)
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %probability_table_, i32 %0)
  store i32 0, i32* %cum_prob, align 4
  store i32 0, i32* %act_prob, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %token_probs.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %probability_table_2 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_2, i32 %6) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call, i32 0, i32 0
  store i32 %5, i32* %prob, align 4
  %7 = load i32, i32* %cum_prob, align 4
  %probability_table_3 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_3, i32 %8) #8
  %cum_prob5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 0, i32 1
  store i32 %7, i32* %cum_prob5, align 4
  %9 = load i32*, i32** %token_probs.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %12 = load i32, i32* %cum_prob, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %cum_prob, align 4
  %13 = load i32, i32* %cum_prob, align 4
  %cmp7 = icmp ugt i32 %13, 524288
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %14 = load i32, i32* %act_prob, align 4
  store i32 %14, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %cum_prob, align 4
  %cmp9 = icmp ult i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32, i32* %i, align 4
  %lut_table_11 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 0
  %18 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_11, i32 %18) #8
  store i32 %17, i32* %call12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %20 = load i32, i32* %cum_prob, align 4
  store i32 %20, i32* %act_prob, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc14 = add i32 %21, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %22 = load i32, i32* %cum_prob, align 4
  %cmp16 = icmp ne i32 %22, 524288
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end15
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi19EE9read_initEPKhi(%"class.draco::RAnsDecoder.33"* %this, i8* %buf, i32 %offset) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::RAnsDecoder.33"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"class.draco::RAnsDecoder.33"* %this, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.33"*, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf2 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 0
  store i8* %1, i8** %buf2, align 4
  %2 = load i8*, i8** %buf.addr, align 4
  %3 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %6, 1
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 1
  store i32 %sub5, i32* %buf_offset, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %8 = load i32, i32* %offset.addr, align 4
  %sub7 = sub nsw i32 %8, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 %sub7
  %9 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %9 to i32
  %and = and i32 %conv9, 63
  %ans_10 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_10, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end54

if.else:                                          ; preds = %if.end
  %10 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %10, 1
  br i1 %cmp11, label %if.then12, label %if.else23

if.then12:                                        ; preds = %if.else
  %11 = load i32, i32* %offset.addr, align 4
  %cmp13 = icmp slt i32 %11, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  %12 = load i32, i32* %offset.addr, align 4
  %sub16 = sub nsw i32 %12, 2
  %ans_17 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf_offset18 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_17, i32 0, i32 1
  store i32 %sub16, i32* %buf_offset18, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %14 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %add.ptr19 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr19)
  %and20 = and i32 %call, 16383
  %ans_21 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state22 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_21, i32 0, i32 2
  store i32 %and20, i32* %state22, align 4
  br label %if.end53

if.else23:                                        ; preds = %if.else
  %15 = load i32, i32* %x, align 4
  %cmp24 = icmp eq i32 %15, 2
  br i1 %cmp24, label %if.then25, label %if.else38

if.then25:                                        ; preds = %if.else23
  %16 = load i32, i32* %offset.addr, align 4
  %cmp26 = icmp slt i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.then25
  %17 = load i32, i32* %offset.addr, align 4
  %sub29 = sub nsw i32 %17, 3
  %ans_30 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf_offset31 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_30, i32 0, i32 1
  store i32 %sub29, i32* %buf_offset31, align 4
  %18 = load i8*, i8** %buf.addr, align 4
  %19 = load i32, i32* %offset.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %18, i32 %19
  %add.ptr33 = getelementptr inbounds i8, i8* %add.ptr32, i32 -3
  %call34 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr33)
  %and35 = and i32 %call34, 4194303
  %ans_36 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state37 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_36, i32 0, i32 2
  store i32 %and35, i32* %state37, align 4
  br label %if.end52

if.else38:                                        ; preds = %if.else23
  %20 = load i32, i32* %x, align 4
  %cmp39 = icmp eq i32 %20, 3
  br i1 %cmp39, label %if.then40, label %if.else50

if.then40:                                        ; preds = %if.else38
  %21 = load i32, i32* %offset.addr, align 4
  %sub41 = sub nsw i32 %21, 4
  %ans_42 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf_offset43 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_42, i32 0, i32 1
  store i32 %sub41, i32* %buf_offset43, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %23 = load i32, i32* %offset.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %22, i32 %23
  %add.ptr45 = getelementptr inbounds i8, i8* %add.ptr44, i32 -4
  %call46 = call i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %add.ptr45)
  %and47 = and i32 %call46, 1073741823
  %ans_48 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state49 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_48, i32 0, i32 2
  store i32 %and47, i32* %state49, align 4
  br label %if.end51

if.else50:                                        ; preds = %if.else38
  store i32 1, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.then40
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end28
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end15
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then4
  %ans_55 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state56 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_55, i32 0, i32 2
  %24 = load i32, i32* %state56, align 4
  %add = add i32 %24, 2097152
  store i32 %add, i32* %state56, align 4
  %ans_57 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state58 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_57, i32 0, i32 2
  %25 = load i32, i32* %state58, align 4
  %cmp59 = icmp uge i32 %25, 536870912
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end54
  store i32 1, i32* %retval, align 4
  br label %return

if.end61:                                         ; preds = %if.end54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end61, %if.then60, %if.else50, %if.then27, %if.then14, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi19EE9rans_readEv(%"class.draco::RAnsDecoder.33"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.33"*, align 4
  %rem = alloca i32, align 4
  %quo = alloca i32, align 4
  %sym = alloca %"struct.draco::rans_dec_sym", align 4
  store %"class.draco::RAnsDecoder.33"* %this, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.33"*, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %0, 2097152
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %ans_2 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_2, i32 0, i32 1
  %1 = load i32, i32* %buf_offset, align 4
  %cmp3 = icmp sgt i32 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %2 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %2, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %ans_4 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state5 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_4, i32 0, i32 2
  %3 = load i32, i32* %state5, align 4
  %mul = mul i32 %3, 256
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %ans_7 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %buf_offset8 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_7, i32 0, i32 1
  %5 = load i32, i32* %buf_offset8, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %buf_offset8, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %dec
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %add = add i32 %mul, %conv
  %ans_9 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state10 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_9, i32 0, i32 2
  store i32 %add, i32* %state10, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %ans_11 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state12 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_11, i32 0, i32 2
  %7 = load i32, i32* %state12, align 4
  %div = udiv i32 %7, 524288
  store i32 %div, i32* %quo, align 4
  %ans_13 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_13, i32 0, i32 2
  %8 = load i32, i32* %state14, align 4
  %rem15 = urem i32 %8, 524288
  store i32 %rem15, i32* %rem, align 4
  %9 = load i32, i32* %rem, align 4
  call void @_ZN5draco11RAnsDecoderILi19EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.33"* %this1, %"struct.draco::rans_dec_sym"* %sym, i32 %9)
  %10 = load i32, i32* %quo, align 4
  %prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 1
  %11 = load i32, i32* %prob, align 4
  %mul16 = mul i32 %10, %11
  %12 = load i32, i32* %rem, align 4
  %add17 = add i32 %mul16, %12
  %cum_prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 2
  %13 = load i32, i32* %cum_prob, align 4
  %sub = sub i32 %add17, %13
  %ans_18 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state19 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_18, i32 0, i32 2
  store i32 %sub, i32* %state19, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 0
  %14 = load i32, i32* %val, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11RAnsDecoderILi19EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.33"* %this, %"struct.draco::rans_dec_sym"* %out, i32 %rem) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.33"*, align 4
  %out.addr = alloca %"struct.draco::rans_dec_sym"*, align 4
  %rem.addr = alloca i32, align 4
  %symbol = alloca i32, align 4
  store %"class.draco::RAnsDecoder.33"* %this, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  store %"struct.draco::rans_dec_sym"* %out, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  store i32 %rem, i32* %rem.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.33"*, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 0
  %0 = load i32, i32* %rem.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %symbol, align 4
  %2 = load i32, i32* %symbol, align 4
  %3 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %3, i32 0, i32 0
  store i32 %2, i32* %val, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 1
  %4 = load i32, i32* %symbol, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_, i32 %4) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 0, i32 0
  %5 = load i32, i32* %prob, align 4
  %6 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %prob3 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %6, i32 0, i32 1
  store i32 %5, i32* %prob3, align 4
  %probability_table_4 = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 1
  %7 = load i32, i32* %symbol, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_4, i32 %7) #8
  %cum_prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call5, i32 0, i32 1
  %8 = load i32, i32* %cum_prob, align 4
  %9 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %cum_prob6 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %9, i32 0, i32 2
  store i32 %8, i32* %cum_prob6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi19EE8read_endEv(%"class.draco::RAnsDecoder.33"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.33"*, align 4
  store %"class.draco::RAnsDecoder.33"* %this, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.33"*, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %0, 2097152
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.33"* @_ZN5draco11RAnsDecoderILi19EED2Ev(%"class.draco::RAnsDecoder.33"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.33"*, align 4
  store %"class.draco::RAnsDecoder.33"* %this, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.33"*, %"class.draco::RAnsDecoder.33"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.33", %"class.draco::RAnsDecoder.33"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %lut_table_) #8
  ret %"class.draco::RAnsDecoder.33"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.34"* @_ZN5draco17RAnsSymbolDecoderILi14EEC2Ev(%"class.draco::RAnsSymbolDecoder.34"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.34"*, align 4
  store %"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.34"*, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EEC2Ev(%"class.draco::RAnsDecoder.35"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.34"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi14EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.34"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.34"*, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi20EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.35"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi14EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.34"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.34"*, align 4
  store %"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.34"*, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi14EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.34"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.34"*, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi20EE9read_initEPKhi(%"class.draco::RAnsDecoder.35"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi14EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.34"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.34"*, align 4
  store %"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.34"*, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE9rans_readEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi14EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.34"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.34"*, align 4
  store %"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.34"*, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE8read_endEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.34"* @_ZN5draco17RAnsSymbolDecoderILi14EED2Ev(%"class.draco::RAnsSymbolDecoder.34"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.34"*, align 4
  store %"class.draco::RAnsSymbolDecoder.34"* %this, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.34"*, %"class.draco::RAnsSymbolDecoder.34"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EED2Ev(%"class.draco::RAnsDecoder.35"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.34", %"class.draco::RAnsSymbolDecoder.34"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.34"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EEC2Ev(%"class.draco::RAnsDecoder.35"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.35"*, align 4
  store %"class.draco::RAnsDecoder.35"* %this, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.35"*, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %lut_table_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEC2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %call3 = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_)
  ret %"class.draco::RAnsDecoder.35"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco11RAnsDecoderILi20EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.35"* %this, i32* %token_probs, i32 %num_symbols) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsDecoder.35"*, align 4
  %token_probs.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %cum_prob = alloca i32, align 4
  %act_prob = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::RAnsDecoder.35"* %this, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  store i32* %token_probs, i32** %token_probs.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.35"*, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %lut_table_, i32 1048576)
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEE6resizeEm(%"class.std::__2::vector.1"* %probability_table_, i32 %0)
  store i32 0, i32* %cum_prob, align 4
  store i32 0, i32* %act_prob, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %token_probs.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %probability_table_2 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_2, i32 %6) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call, i32 0, i32 0
  store i32 %5, i32* %prob, align 4
  %7 = load i32, i32* %cum_prob, align 4
  %probability_table_3 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_3, i32 %8) #8
  %cum_prob5 = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call4, i32 0, i32 1
  store i32 %7, i32* %cum_prob5, align 4
  %9 = load i32*, i32** %token_probs.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %12 = load i32, i32* %cum_prob, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %cum_prob, align 4
  %13 = load i32, i32* %cum_prob, align 4
  %cmp7 = icmp ugt i32 %13, 1048576
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %14 = load i32, i32* %act_prob, align 4
  store i32 %14, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %cum_prob, align 4
  %cmp9 = icmp ult i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32, i32* %i, align 4
  %lut_table_11 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 0
  %18 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_11, i32 %18) #8
  store i32 %17, i32* %call12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %20 = load i32, i32* %cum_prob, align 4
  store i32 %20, i32* %act_prob, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc14 = add i32 %21, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %22 = load i32, i32* %cum_prob, align 4
  %cmp16 = icmp ne i32 %22, 1048576
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end15
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi20EE9read_initEPKhi(%"class.draco::RAnsDecoder.35"* %this, i8* %buf, i32 %offset) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::RAnsDecoder.35"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"class.draco::RAnsDecoder.35"* %this, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.35"*, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf2 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 0
  store i8* %1, i8** %buf2, align 4
  %2 = load i8*, i8** %buf.addr, align 4
  %3 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %3, 1
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %sub
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %6, 1
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 1
  store i32 %sub5, i32* %buf_offset, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %8 = load i32, i32* %offset.addr, align 4
  %sub7 = sub nsw i32 %8, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 %sub7
  %9 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %9 to i32
  %and = and i32 %conv9, 63
  %ans_10 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_10, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end54

if.else:                                          ; preds = %if.end
  %10 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %10, 1
  br i1 %cmp11, label %if.then12, label %if.else23

if.then12:                                        ; preds = %if.else
  %11 = load i32, i32* %offset.addr, align 4
  %cmp13 = icmp slt i32 %11, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  %12 = load i32, i32* %offset.addr, align 4
  %sub16 = sub nsw i32 %12, 2
  %ans_17 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf_offset18 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_17, i32 0, i32 1
  store i32 %sub16, i32* %buf_offset18, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %14 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %add.ptr19 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr19)
  %and20 = and i32 %call, 16383
  %ans_21 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state22 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_21, i32 0, i32 2
  store i32 %and20, i32* %state22, align 4
  br label %if.end53

if.else23:                                        ; preds = %if.else
  %15 = load i32, i32* %x, align 4
  %cmp24 = icmp eq i32 %15, 2
  br i1 %cmp24, label %if.then25, label %if.else38

if.then25:                                        ; preds = %if.else23
  %16 = load i32, i32* %offset.addr, align 4
  %cmp26 = icmp slt i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.then25
  %17 = load i32, i32* %offset.addr, align 4
  %sub29 = sub nsw i32 %17, 3
  %ans_30 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf_offset31 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_30, i32 0, i32 1
  store i32 %sub29, i32* %buf_offset31, align 4
  %18 = load i8*, i8** %buf.addr, align 4
  %19 = load i32, i32* %offset.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %18, i32 %19
  %add.ptr33 = getelementptr inbounds i8, i8* %add.ptr32, i32 -3
  %call34 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr33)
  %and35 = and i32 %call34, 4194303
  %ans_36 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state37 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_36, i32 0, i32 2
  store i32 %and35, i32* %state37, align 4
  br label %if.end52

if.else38:                                        ; preds = %if.else23
  %20 = load i32, i32* %x, align 4
  %cmp39 = icmp eq i32 %20, 3
  br i1 %cmp39, label %if.then40, label %if.else50

if.then40:                                        ; preds = %if.else38
  %21 = load i32, i32* %offset.addr, align 4
  %sub41 = sub nsw i32 %21, 4
  %ans_42 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf_offset43 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_42, i32 0, i32 1
  store i32 %sub41, i32* %buf_offset43, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %23 = load i32, i32* %offset.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %22, i32 %23
  %add.ptr45 = getelementptr inbounds i8, i8* %add.ptr44, i32 -4
  %call46 = call i32 @_ZN5dracoL12mem_get_le32EPKv(i8* %add.ptr45)
  %and47 = and i32 %call46, 1073741823
  %ans_48 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state49 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_48, i32 0, i32 2
  store i32 %and47, i32* %state49, align 4
  br label %if.end51

if.else50:                                        ; preds = %if.else38
  store i32 1, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.then40
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end28
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end15
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then4
  %ans_55 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state56 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_55, i32 0, i32 2
  %24 = load i32, i32* %state56, align 4
  %add = add i32 %24, 4194304
  store i32 %add, i32* %state56, align 4
  %ans_57 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state58 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_57, i32 0, i32 2
  %25 = load i32, i32* %state58, align 4
  %cmp59 = icmp uge i32 %25, 1073741824
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end54
  store i32 1, i32* %retval, align 4
  br label %return

if.end61:                                         ; preds = %if.end54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end61, %if.then60, %if.else50, %if.then27, %if.then14, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi20EE9rans_readEv(%"class.draco::RAnsDecoder.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.35"*, align 4
  %rem = alloca i32, align 4
  %quo = alloca i32, align 4
  %sym = alloca %"struct.draco::rans_dec_sym", align 4
  store %"class.draco::RAnsDecoder.35"* %this, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.35"*, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %0, 4194304
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %ans_2 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_2, i32 0, i32 1
  %1 = load i32, i32* %buf_offset, align 4
  %cmp3 = icmp sgt i32 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %2 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %2, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %ans_4 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state5 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_4, i32 0, i32 2
  %3 = load i32, i32* %state5, align 4
  %mul = mul i32 %3, 256
  %ans_6 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_6, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %ans_7 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %buf_offset8 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_7, i32 0, i32 1
  %5 = load i32, i32* %buf_offset8, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %buf_offset8, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %dec
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %add = add i32 %mul, %conv
  %ans_9 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state10 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_9, i32 0, i32 2
  store i32 %add, i32* %state10, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %ans_11 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state12 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_11, i32 0, i32 2
  %7 = load i32, i32* %state12, align 4
  %div = udiv i32 %7, 1048576
  store i32 %div, i32* %quo, align 4
  %ans_13 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_13, i32 0, i32 2
  %8 = load i32, i32* %state14, align 4
  %rem15 = urem i32 %8, 1048576
  store i32 %rem15, i32* %rem, align 4
  %9 = load i32, i32* %rem, align 4
  call void @_ZN5draco11RAnsDecoderILi20EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.35"* %this1, %"struct.draco::rans_dec_sym"* %sym, i32 %9)
  %10 = load i32, i32* %quo, align 4
  %prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 1
  %11 = load i32, i32* %prob, align 4
  %mul16 = mul i32 %10, %11
  %12 = load i32, i32* %rem, align 4
  %add17 = add i32 %mul16, %12
  %cum_prob = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 2
  %13 = load i32, i32* %cum_prob, align 4
  %sub = sub i32 %add17, %13
  %ans_18 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state19 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_18, i32 0, i32 2
  store i32 %sub, i32* %state19, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %sym, i32 0, i32 0
  %14 = load i32, i32* %val, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11RAnsDecoderILi20EE9fetch_symEPNS_12rans_dec_symEj(%"class.draco::RAnsDecoder.35"* %this, %"struct.draco::rans_dec_sym"* %out, i32 %rem) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.35"*, align 4
  %out.addr = alloca %"struct.draco::rans_dec_sym"*, align 4
  %rem.addr = alloca i32, align 4
  %symbol = alloca i32, align 4
  store %"class.draco::RAnsDecoder.35"* %this, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  store %"struct.draco::rans_dec_sym"* %out, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  store i32 %rem, i32* %rem.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.35"*, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 0
  %0 = load i32, i32* %rem.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %lut_table_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %symbol, align 4
  %2 = load i32, i32* %symbol, align 4
  %3 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %val = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %3, i32 0, i32 0
  store i32 %2, i32* %val, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 1
  %4 = load i32, i32* %symbol, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_, i32 %4) #8
  %prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call2, i32 0, i32 0
  %5 = load i32, i32* %prob, align 4
  %6 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %prob3 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %6, i32 0, i32 1
  store i32 %5, i32* %prob3, align 4
  %probability_table_4 = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 1
  %7 = load i32, i32* %symbol, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.draco::rans_sym"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEEixEm(%"class.std::__2::vector.1"* %probability_table_4, i32 %7) #8
  %cum_prob = getelementptr inbounds %"struct.draco::rans_sym", %"struct.draco::rans_sym"* %call5, i32 0, i32 1
  %8 = load i32, i32* %cum_prob, align 4
  %9 = load %"struct.draco::rans_dec_sym"*, %"struct.draco::rans_dec_sym"** %out.addr, align 4
  %cum_prob6 = getelementptr inbounds %"struct.draco::rans_dec_sym", %"struct.draco::rans_dec_sym"* %9, i32 0, i32 2
  store i32 %8, i32* %cum_prob6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco11RAnsDecoderILi20EE8read_endEv(%"class.draco::RAnsDecoder.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.35"*, align 4
  store %"class.draco::RAnsDecoder.35"* %this, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.35"*, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 2
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %ans_, i32 0, i32 2
  %0 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %0, 4194304
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EED2Ev(%"class.draco::RAnsDecoder.35"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsDecoder.35"*, align 4
  store %"class.draco::RAnsDecoder.35"* %this, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsDecoder.35"*, %"class.draco::RAnsDecoder.35"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco8rans_symENS_9allocatorIS2_EEED2Ev(%"class.std::__2::vector.1"* %probability_table_) #8
  %lut_table_ = getelementptr inbounds %"class.draco::RAnsDecoder.35", %"class.draco::RAnsDecoder.35"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %lut_table_) #8
  ret %"class.draco::RAnsDecoder.35"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.36"* @_ZN5draco17RAnsSymbolDecoderILi15EEC2Ev(%"class.draco::RAnsSymbolDecoder.36"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.36"*, align 4
  store %"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.36"*, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EEC2Ev(%"class.draco::RAnsDecoder.35"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.36"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi15EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.36"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.36"*, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi20EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.35"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi15EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.36"*, align 4
  store %"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.36"*, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi15EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.36"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.36"*, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi20EE9read_initEPKhi(%"class.draco::RAnsDecoder.35"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi15EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.36"*, align 4
  store %"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.36"*, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE9rans_readEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi15EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.36"*, align 4
  store %"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.36"*, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE8read_endEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.36"* @_ZN5draco17RAnsSymbolDecoderILi15EED2Ev(%"class.draco::RAnsSymbolDecoder.36"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.36"*, align 4
  store %"class.draco::RAnsSymbolDecoder.36"* %this, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.36"*, %"class.draco::RAnsSymbolDecoder.36"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EED2Ev(%"class.draco::RAnsDecoder.35"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.36", %"class.draco::RAnsSymbolDecoder.36"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.36"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.37"* @_ZN5draco17RAnsSymbolDecoderILi16EEC2Ev(%"class.draco::RAnsSymbolDecoder.37"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.37"*, align 4
  store %"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.37"*, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EEC2Ev(%"class.draco::RAnsDecoder.35"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi16EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.37"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.37"*, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi20EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.35"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi16EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.37"*, align 4
  store %"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.37"*, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi16EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.37"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.37"*, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi20EE9read_initEPKhi(%"class.draco::RAnsDecoder.35"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi16EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.37"*, align 4
  store %"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.37"*, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE9rans_readEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi16EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.37"*, align 4
  store %"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.37"*, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE8read_endEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.37"* @_ZN5draco17RAnsSymbolDecoderILi16EED2Ev(%"class.draco::RAnsSymbolDecoder.37"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.37"*, align 4
  store %"class.draco::RAnsSymbolDecoder.37"* %this, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.37"*, %"class.draco::RAnsSymbolDecoder.37"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EED2Ev(%"class.draco::RAnsDecoder.35"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.37", %"class.draco::RAnsSymbolDecoder.37"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.38"* @_ZN5draco17RAnsSymbolDecoderILi17EEC2Ev(%"class.draco::RAnsSymbolDecoder.38"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.38"*, align 4
  store %"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.38"*, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EEC2Ev(%"class.draco::RAnsDecoder.35"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.38"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi17EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.38"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.38"*, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi20EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.35"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi17EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.38"*, align 4
  store %"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.38"*, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi17EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.38"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.38"*, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi20EE9read_initEPKhi(%"class.draco::RAnsDecoder.35"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi17EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.38"*, align 4
  store %"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.38"*, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE9rans_readEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi17EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.38"*, align 4
  store %"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.38"*, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE8read_endEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.38"* @_ZN5draco17RAnsSymbolDecoderILi17EED2Ev(%"class.draco::RAnsSymbolDecoder.38"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.38"*, align 4
  store %"class.draco::RAnsSymbolDecoder.38"* %this, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.38"*, %"class.draco::RAnsSymbolDecoder.38"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EED2Ev(%"class.draco::RAnsDecoder.35"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.38", %"class.draco::RAnsSymbolDecoder.38"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.38"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.39"* @_ZN5draco17RAnsSymbolDecoderILi18EEC2Ev(%"class.draco::RAnsSymbolDecoder.39"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.39"*, align 4
  store %"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.39"*, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %probability_table_) #8
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  store i32 0, i32* %num_symbols_, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EEC2Ev(%"class.draco::RAnsDecoder.35"* %ans_)
  ret %"class.draco::RAnsSymbolDecoder.39"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi18EE6CreateEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.39"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %i = alloca i32, align 4
  %prob_data = alloca i8, align 1
  %token = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  %extra_bytes = alloca i32, align 4
  %prob = alloca i32, align 4
  %b = alloca i32, align 4
  %eb = alloca i8, align 1
  store %"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.39"*, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv3 = zext i16 %call2 to i32
  %cmp4 = icmp slt i32 %conv3, 512
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %num_symbols_)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.then5
  br label %if.end13

if.else:                                          ; preds = %if.end
  %num_symbols_9 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_symbols_9, %"class.draco::DecoderBuffer"* %3)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 0
  %num_symbols_14 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_symbols_14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE6resizeEm(%"class.std::__2::vector"* %probability_table_, i32 %4)
  %num_symbols_15 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_symbols_15, align 4
  %cmp16 = icmp eq i32 %5, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %if.end18
  %6 = load i32, i32* %i, align 4
  %num_symbols_19 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %7 = load i32, i32* %num_symbols_19, align 4
  %cmp20 = icmp ult i32 %6, %7
  br i1 %cmp20, label %for.body, label %for.end59

for.body:                                         ; preds = %for.cond
  store i8 0, i8* %prob_data, align 1
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %prob_data)
  br i1 %call21, label %if.end23, label %if.then22

if.then22:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end23:                                         ; preds = %for.body
  %9 = load i8, i8* %prob_data, align 1
  %conv24 = zext i8 %9 to i32
  %and = and i32 %conv24, 3
  store i32 %and, i32* %token, align 4
  %10 = load i32, i32* %token, align 4
  %cmp25 = icmp eq i32 %10, 3
  br i1 %cmp25, label %if.then26, label %if.else40

if.then26:                                        ; preds = %if.end23
  %11 = load i8, i8* %prob_data, align 1
  %conv27 = zext i8 %11 to i32
  %shr = ashr i32 %conv27, 2
  store i32 %shr, i32* %offset, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %offset, align 4
  %add = add i32 %12, %13
  %num_symbols_28 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %14 = load i32, i32* %num_symbols_28, align 4
  %cmp29 = icmp uge i32 %add, %14
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then26
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.then26
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.end31
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %offset, align 4
  %add33 = add i32 %16, 1
  %cmp34 = icmp ult i32 %15, %add33
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond32
  %probability_table_36 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %add37 = add i32 %17, %18
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_36, i32 %add37) #8
  store i32 0, i32* %call38, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %19 = load i32, i32* %j, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %20 = load i32, i32* %offset, align 4
  %21 = load i32, i32* %i, align 4
  %add39 = add i32 %21, %20
  store i32 %add39, i32* %i, align 4
  br label %if.end56

if.else40:                                        ; preds = %if.end23
  %22 = load i32, i32* %token, align 4
  store i32 %22, i32* %extra_bytes, align 4
  %23 = load i8, i8* %prob_data, align 1
  %conv41 = zext i8 %23 to i32
  %shr42 = ashr i32 %conv41, 2
  store i32 %shr42, i32* %prob, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc51, %if.else40
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %extra_bytes, align 4
  %cmp44 = icmp slt i32 %24, %25
  br i1 %cmp44, label %for.body45, label %for.end53

for.body45:                                       ; preds = %for.cond43
  %26 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %26, i8* %eb)
  br i1 %call46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %for.body45
  store i1 false, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %for.body45
  %27 = load i8, i8* %eb, align 1
  %conv49 = zext i8 %27 to i32
  %28 = load i32, i32* %b, align 4
  %add50 = add nsw i32 %28, 1
  %mul = mul nsw i32 8, %add50
  %sub = sub nsw i32 %mul, 2
  %shl = shl i32 %conv49, %sub
  %29 = load i32, i32* %prob, align 4
  %or = or i32 %29, %shl
  store i32 %or, i32* %prob, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %if.end48
  %30 = load i32, i32* %b, align 4
  %inc52 = add nsw i32 %30, 1
  store i32 %inc52, i32* %b, align 4
  br label %for.cond43

for.end53:                                        ; preds = %for.cond43
  %31 = load i32, i32* %prob, align 4
  %probability_table_54 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_54, i32 %32) #8
  store i32 %31, i32* %call55, align 4
  br label %if.end56

if.end56:                                         ; preds = %for.end53, %for.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %33 = load i32, i32* %i, align 4
  %inc58 = add i32 %33, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond

for.end59:                                        ; preds = %for.cond
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 2
  %probability_table_60 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 0
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %probability_table_60, i32 0) #8
  %num_symbols_62 = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %34 = load i32, i32* %num_symbols_62, align 4
  %call63 = call zeroext i1 @_ZN5draco11RAnsDecoderILi20EE24rans_build_look_up_tableEPKjj(%"class.draco::RAnsDecoder.35"* %ans_, i32* %call61, i32 %34)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %for.end59
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %for.end59
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end65, %if.then64, %if.then47, %if.then30, %if.then22, %if.then17, %if.then11, %if.then7, %if.then
  %35 = load i1, i1* %retval, align 1
  ret i1 %35
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17RAnsSymbolDecoderILi18EE11num_symbolsEv(%"class.draco::RAnsSymbolDecoder.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.39"*, align 4
  store %"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.39"*, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %num_symbols_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_symbols_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17RAnsSymbolDecoderILi18EE13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.39"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes_encoded = alloca i64, align 8
  %data_head = alloca i8*, align 4
  store %"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.39"*, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %1, i64* %bytes_encoded)
  br i1 %call2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %bytes_encoded, %"class.draco::DecoderBuffer"* %2)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  %3 = load i64, i64* %bytes_encoded, align 8
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call8 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %4)
  %cmp9 = icmp ugt i64 %3, %call8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end7
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call12 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %5)
  store i8* %call12, i8** %data_head, align 4
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %7 = load i64, i64* %bytes_encoded, align 8
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %6, i64 %7)
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %data_head, align 4
  %9 = load i64, i64* %bytes_encoded, align 8
  %conv13 = trunc i64 %9 to i32
  %call14 = call i32 @_ZN5draco11RAnsDecoderILi20EE9read_initEPKhi(%"class.draco::RAnsDecoder.35"* %ans_, i8* %8, i32 %conv13)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then10, %if.then5, %if.then3
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco17RAnsSymbolDecoderILi18EE12DecodeSymbolEv(%"class.draco::RAnsSymbolDecoder.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.39"*, align 4
  store %"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.39"*, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE9rans_readEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17RAnsSymbolDecoderILi18EE11EndDecodingEv(%"class.draco::RAnsSymbolDecoder.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.39"*, align 4
  store %"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.39"*, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 2
  %call = call i32 @_ZN5draco11RAnsDecoderILi20EE8read_endEv(%"class.draco::RAnsDecoder.35"* %ans_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::RAnsSymbolDecoder.39"* @_ZN5draco17RAnsSymbolDecoderILi18EED2Ev(%"class.draco::RAnsSymbolDecoder.39"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::RAnsSymbolDecoder.39"*, align 4
  store %"class.draco::RAnsSymbolDecoder.39"* %this, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsSymbolDecoder.39"*, %"class.draco::RAnsSymbolDecoder.39"** %this.addr, align 4
  %ans_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 2
  %call = call %"class.draco::RAnsDecoder.35"* @_ZN5draco11RAnsDecoderILi20EED2Ev(%"class.draco::RAnsDecoder.35"* %ans_) #8
  %probability_table_ = getelementptr inbounds %"class.draco::RAnsSymbolDecoder.39", %"class.draco::RAnsSymbolDecoder.39"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %probability_table_) #8
  ret %"class.draco::RAnsSymbolDecoder.39"* %this1
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nounwind }
attributes #9 = { noreturn }
attributes #10 = { builtin allocsize(0) }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
