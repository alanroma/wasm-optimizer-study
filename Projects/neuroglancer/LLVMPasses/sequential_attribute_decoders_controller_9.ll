; ModuleID = './draco/src/draco/compression/attributes/sequential_attribute_decoders_controller.cc'
source_filename = "./draco/src/draco/compression/attributes/sequential_attribute_decoders_controller.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::SequentialAttributeDecodersController" = type { %"class.draco::AttributesDecoder", %"class.std::__2::vector.124", %"class.std::__2::vector.136", %"class.std::__2::unique_ptr.144" }
%"class.draco::AttributesDecoder" = type { %"class.draco::AttributesDecoderInterface", %"class.std::__2::vector", %"class.std::__2::vector", %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i32*, i32*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32* }
%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.94", %"class.std::__2::vector", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.101" }
%"class.std::__2::unique_ptr.96" = type { %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.std::__2::__compressed_pair.101" = type { %"struct.std::__2::__compressed_pair_elem.102" }
%"struct.std::__2::__compressed_pair_elem.102" = type { %"class.std::__2::unique_ptr.96"* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type { %"class.draco::Options", %"class.std::__2::map.113" }
%"class.draco::Options" = type { %"class.std::__2::map" }
%"class.std::__2::map" = type { %"class.std::__2::__tree" }
%"class.std::__2::__tree" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.106", %"class.std::__2::__compressed_pair.111" }
%"class.std::__2::__tree_end_node" = type { %"class.std::__2::__tree_node_base"* }
%"class.std::__2::__tree_node_base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8, [3 x i8] }>
%"class.std::__2::__compressed_pair.106" = type { %"struct.std::__2::__compressed_pair_elem.107" }
%"struct.std::__2::__compressed_pair_elem.107" = type { %"class.std::__2::__tree_end_node" }
%"class.std::__2::__compressed_pair.111" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { i32 }
%"class.std::__2::map.113" = type { %"class.std::__2::__tree.114" }
%"class.std::__2::__tree.114" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.115", %"class.std::__2::__compressed_pair.119" }
%"class.std::__2::__compressed_pair.115" = type { %"struct.std::__2::__compressed_pair_elem.107" }
%"class.std::__2::__compressed_pair.119" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.58", [5 x %"class.std::__2::vector"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector.45" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.22" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.3", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.17", %"class.std::__2::__compressed_pair.19" }
%"class.std::__2::unique_ptr.3" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.6" }
%"struct.std::__2::__compressed_pair_elem.5" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.6" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.7" }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.13" }
%"struct.std::__2::__compressed_pair_elem.13" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.17" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.std::__2::__compressed_pair.19" = type { %"struct.std::__2::__compressed_pair_elem.20" }
%"struct.std::__2::__compressed_pair_elem.20" = type { float }
%"class.std::__2::unordered_map.22" = type { %"class.std::__2::__hash_table.23" }
%"class.std::__2::__hash_table.23" = type { %"class.std::__2::unique_ptr.24", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.39", %"class.std::__2::__compressed_pair.42" }
%"class.std::__2::unique_ptr.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.26", %"struct.std::__2::__compressed_pair_elem.28" }
%"struct.std::__2::__compressed_pair_elem.26" = type { %"struct.std::__2::__hash_node_base.27"** }
%"struct.std::__2::__hash_node_base.27" = type { %"struct.std::__2::__hash_node_base.27"* }
%"struct.std::__2::__compressed_pair_elem.28" = type { %"class.std::__2::__bucket_list_deallocator.29" }
%"class.std::__2::__bucket_list_deallocator.29" = type { %"class.std::__2::__compressed_pair.30" }
%"class.std::__2::__compressed_pair.30" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.35" }
%"struct.std::__2::__compressed_pair_elem.35" = type { %"struct.std::__2::__hash_node_base.27" }
%"class.std::__2::__compressed_pair.39" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.std::__2::__compressed_pair.42" = type { %"struct.std::__2::__compressed_pair_elem.20" }
%"class.std::__2::vector.45" = type { %"class.std::__2::__vector_base.46" }
%"class.std::__2::__vector_base.46" = type { %"class.std::__2::unique_ptr.47"*, %"class.std::__2::unique_ptr.47"*, %"class.std::__2::__compressed_pair.51" }
%"class.std::__2::unique_ptr.47" = type { %"class.std::__2::__compressed_pair.48" }
%"class.std::__2::__compressed_pair.48" = type { %"struct.std::__2::__compressed_pair_elem.49" }
%"struct.std::__2::__compressed_pair_elem.49" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.51" = type { %"struct.std::__2::__compressed_pair_elem.52" }
%"struct.std::__2::__compressed_pair_elem.52" = type { %"class.std::__2::unique_ptr.47"* }
%"class.std::__2::vector.58" = type { %"class.std::__2::__vector_base.59" }
%"class.std::__2::__vector_base.59" = type { %"class.std::__2::unique_ptr.60"*, %"class.std::__2::unique_ptr.60"*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::unique_ptr.60" = type { %"class.std::__2::__compressed_pair.61" }
%"class.std::__2::__compressed_pair.61" = type { %"struct.std::__2::__compressed_pair_elem.62" }
%"struct.std::__2::__compressed_pair_elem.62" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.70", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.82", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.63", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.63" = type { %"class.std::__2::__vector_base.64" }
%"class.std::__2::__vector_base.64" = type { i8*, i8*, %"class.std::__2::__compressed_pair.65" }
%"class.std::__2::__compressed_pair.65" = type { %"struct.std::__2::__compressed_pair_elem.66" }
%"struct.std::__2::__compressed_pair_elem.66" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.70" = type { %"class.std::__2::__compressed_pair.71" }
%"class.std::__2::__compressed_pair.71" = type { %"struct.std::__2::__compressed_pair_elem.72" }
%"struct.std::__2::__compressed_pair_elem.72" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.75" }
%"class.std::__2::vector.75" = type { %"class.std::__2::__vector_base.76" }
%"class.std::__2::__vector_base.76" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.77" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.77" = type { %"struct.std::__2::__compressed_pair_elem.78" }
%"struct.std::__2::__compressed_pair_elem.78" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.82" = type { %"class.std::__2::__compressed_pair.83" }
%"class.std::__2::__compressed_pair.83" = type { %"struct.std::__2::__compressed_pair_elem.84" }
%"struct.std::__2::__compressed_pair_elem.84" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { %"class.std::__2::unique_ptr.60"* }
%"class.std::__2::vector.124" = type { %"class.std::__2::__vector_base.125" }
%"class.std::__2::__vector_base.125" = type { %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"*, %"class.std::__2::__compressed_pair.131" }
%"class.std::__2::unique_ptr.126" = type { %"class.std::__2::__compressed_pair.127" }
%"class.std::__2::__compressed_pair.127" = type { %"struct.std::__2::__compressed_pair_elem.128" }
%"struct.std::__2::__compressed_pair_elem.128" = type { %"class.draco::SequentialAttributeDecoder"* }
%"class.draco::SequentialAttributeDecoder" = type { i32 (...)**, %"class.draco::PointCloudDecoder"*, %"class.draco::PointAttribute"*, i32, %"class.std::__2::unique_ptr.60" }
%"class.std::__2::__compressed_pair.131" = type { %"struct.std::__2::__compressed_pair_elem.132" }
%"struct.std::__2::__compressed_pair_elem.132" = type { %"class.std::__2::unique_ptr.126"* }
%"class.std::__2::vector.136" = type { %"class.std::__2::__vector_base.137" }
%"class.std::__2::__vector_base.137" = type { %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"*, %"class.std::__2::__compressed_pair.139" }
%"class.draco::IndexType.138" = type { i32 }
%"class.std::__2::__compressed_pair.139" = type { %"struct.std::__2::__compressed_pair_elem.140" }
%"struct.std::__2::__compressed_pair_elem.140" = type { %"class.draco::IndexType.138"* }
%"class.std::__2::unique_ptr.144" = type { %"class.std::__2::__compressed_pair.145" }
%"class.std::__2::__compressed_pair.145" = type { %"struct.std::__2::__compressed_pair_elem.146" }
%"struct.std::__2::__compressed_pair_elem.146" = type { %"class.draco::PointsSequencer"* }
%"class.draco::PointsSequencer" = type { i32 (...)**, %"class.std::__2::vector.136"* }
%"struct.std::__2::default_delete.148" = type { i8 }
%"struct.std::__2::default_delete.130" = type { i8 }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.149" }
%"class.std::__2::__compressed_pair.149" = type { %"struct.std::__2::__compressed_pair_elem.150" }
%"struct.std::__2::__compressed_pair_elem.150" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"class.draco::SequentialIntegerAttributeDecoder" = type { %"class.draco::SequentialAttributeDecoder", %"class.std::__2::unique_ptr.154" }
%"class.std::__2::unique_ptr.154" = type { %"class.std::__2::__compressed_pair.155" }
%"class.std::__2::__compressed_pair.155" = type { %"struct.std::__2::__compressed_pair_elem.156" }
%"struct.std::__2::__compressed_pair_elem.156" = type { %"class.draco::PredictionSchemeTypedDecoderInterface"* }
%"class.draco::PredictionSchemeTypedDecoderInterface" = type opaque
%"class.draco::SequentialQuantizationAttributeDecoder" = type { %"class.draco::SequentialIntegerAttributeDecoder", i32, %"class.std::__2::unique_ptr.159", float }
%"class.std::__2::unique_ptr.159" = type { %"class.std::__2::__compressed_pair.160" }
%"class.std::__2::__compressed_pair.160" = type { %"struct.std::__2::__compressed_pair_elem.161" }
%"struct.std::__2::__compressed_pair_elem.161" = type { float* }
%"struct.std::__2::__compressed_pair_elem.129" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.147" = type { i8 }
%"class.std::__2::allocator.142" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.141" = type { i8 }
%"class.std::__2::allocator.134" = type { i8 }
%"struct.std::__2::__has_destroy.164" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.133" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__has_destroy.165" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"*, %"class.std::__2::__compressed_pair.166" }
%"class.std::__2::__compressed_pair.166" = type { %"struct.std::__2::__compressed_pair_elem.132", %"struct.std::__2::__compressed_pair_elem.167" }
%"struct.std::__2::__compressed_pair_elem.167" = type { %"class.std::__2::allocator.134"* }
%"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction" = type { %"class.std::__2::vector.124"*, %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction" = type { %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::__has_construct.168" = type { i8 }
%"struct.std::__2::integral_constant.169" = type { i8 }
%"class.std::__2::__map_const_iterator" = type { %"class.std::__2::__tree_const_iterator" }
%"class.std::__2::__tree_const_iterator" = type { %"class.std::__2::__tree_end_node"* }
%"struct.std::__2::pair" = type { i32, %"class.draco::Options" }
%"struct.std::__2::__value_type" = type { %"struct.std::__2::pair" }
%"class.std::__2::__tree_node" = type { %"class.std::__2::__tree_node_base.base", %"struct.std::__2::__value_type" }
%"class.std::__2::__tree_node_base.base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8 }>
%"class.std::__2::__map_value_compare.121" = type { i8 }
%"struct.std::__2::less.122" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.120" = type { i8 }
%"class.std::__2::__tree_node.170" = type { %"class.std::__2::__tree_node_base.base", %"struct.std::__2::__value_type.171" }
%"struct.std::__2::__value_type.171" = type { %"struct.std::__2::pair.172" }
%"struct.std::__2::pair.172" = type { %"class.std::__2::basic_string", %"class.std::__2::basic_string" }
%"class.std::__2::__map_value_compare" = type { i8 }
%"struct.std::__2::less" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.112" = type { i8 }
%"class.std::__2::basic_string_view" = type { i8*, i32 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.std::__2::__compressed_pair_elem.151" = type { i8 }
%"class.std::__2::allocator.152" = type { i8 }

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm = comdat any

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEcvbEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEcvbEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco15PointsSequencer16GenerateSequenceEPNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEE = comdat any

$_ZN5draco17PointCloudDecoder11point_cloudEv = comdat any

$_ZN5draco10PointCloud9attributeEi = comdat any

$_ZN5draco17AttributesDecoder16DecodeAttributesEPNS_13DecoderBufferE = comdat any

$_ZNK5draco17PointCloudDecoder7optionsEv = comdat any

$_ZN5draco26SequentialAttributeDecoder9attributeEv = comdat any

$_ZNK5draco12DracoOptionsINS_17GeometryAttribute4TypeEE16GetAttributeBoolERKS2_RKNSt3__212basic_stringIcNS6_11char_traitsIcEENS6_9allocatorIcEEEEb = comdat any

$_ZNK5draco17GeometryAttribute14attribute_typeEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEDn = comdat any

$_ZN5draco37SequentialAttributeDecodersControllerD2Ev = comdat any

$_ZN5draco37SequentialAttributeDecodersControllerD0Ev = comdat any

$_ZNK5draco17AttributesDecoder14GetAttributeIdEi = comdat any

$_ZNK5draco17AttributesDecoder16GetNumAttributesEv = comdat any

$_ZNK5draco17AttributesDecoder10GetDecoderEv = comdat any

$_ZN5draco37SequentialAttributeDecodersController20GetPortableAttributeEi = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZN5draco17AttributesDecoderD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZN5draco26AttributesDecoderInterfaceD2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm = comdat any

$_ZNK5draco17AttributesDecoder27GetLocalIdForPointAttributeEi = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco15PointsSequencerEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEC2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_ = comdat any

$_ZNSt3__24swapIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco26SequentialAttributeDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNKSt3__214default_deleteIN5draco26SequentialAttributeDecoderEEclEPS2_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv = comdat any

$_ZNK5draco12DracoOptionsINS_17GeometryAttribute4TypeEE20FindAttributeOptionsERKS2_ = comdat any

$_ZNK5draco7Options11IsOptionSetERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE = comdat any

$_ZNKSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE4findERS9_ = comdat any

$_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEESF_ = comdat any

$_ZNKSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE3endEv = comdat any

$_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEptEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4findIS4_EENS_21__tree_const_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_ = comdat any

$_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE13__lower_boundIS4_EENS_21__tree_const_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_SJ_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISH_EEEE = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv = comdat any

$_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv = comdat any

$_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS3_RKS6_ = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEdeEv = comdat any

$_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS6_RKS3_ = comdat any

$_ZNSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE = comdat any

$_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_ = comdat any

$_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_ = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv = comdat any

$_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_ = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_ = comdat any

$_ZNKSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv = comdat any

$_ZNSt3__214pointer_traitsIPKNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS8_ = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEptEv = comdat any

$_ZNSt3__29addressofIKNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS9_ = comdat any

$_ZNSt3__214pointer_traitsIPKNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS7_ = comdat any

$_ZNSt3__29addressofIKNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_ = comdat any

$_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5countERSA_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE14__count_uniqueIS7_EEmRKT_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv = comdat any

$_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_ = comdat any

$_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv = comdat any

$_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_ = comdat any

$_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv = comdat any

$_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv = comdat any

$_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv = comdat any

$_ZNSt3__211char_traitsIcE7compareEPKcS3_m = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__211char_traitsIcE6lengthEPKc = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EEC2IRS3_vEEOT_ = comdat any

@_ZTVN5draco37SequentialAttributeDecodersControllerE = hidden unnamed_addr constant { [15 x i8*] } { [15 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::SequentialAttributeDecodersController"* (%"class.draco::SequentialAttributeDecodersController"*)* @_ZN5draco37SequentialAttributeDecodersControllerD2Ev to i8*), i8* bitcast (void (%"class.draco::SequentialAttributeDecodersController"*)* @_ZN5draco37SequentialAttributeDecodersControllerD0Ev to i8*), i8* bitcast (i1 (%"class.draco::AttributesDecoder"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)* @_ZN5draco17AttributesDecoder4InitEPNS_17PointCloudDecoderEPNS_10PointCloudE to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecodersController"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco37SequentialAttributeDecodersController27DecodeAttributesDecoderDataEPNS_13DecoderBufferE to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecodersController"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco37SequentialAttributeDecodersController16DecodeAttributesEPNS_13DecoderBufferE to i8*), i8* bitcast (i32 (%"class.draco::AttributesDecoder"*, i32)* @_ZNK5draco17AttributesDecoder14GetAttributeIdEi to i8*), i8* bitcast (i32 (%"class.draco::AttributesDecoder"*)* @_ZNK5draco17AttributesDecoder16GetNumAttributesEv to i8*), i8* bitcast (%"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)* @_ZNK5draco17AttributesDecoder10GetDecoderEv to i8*), i8* bitcast (%"class.draco::PointAttribute"* (%"class.draco::SequentialAttributeDecodersController"*, i32)* @_ZN5draco37SequentialAttributeDecodersController20GetPortableAttributeEi to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecodersController"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco37SequentialAttributeDecodersController24DecodePortableAttributesEPNS_13DecoderBufferE to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecodersController"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco37SequentialAttributeDecodersController36DecodeDataNeededByPortableTransformsEPNS_13DecoderBufferE to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecodersController"*)* @_ZN5draco37SequentialAttributeDecodersController35TransformAttributesToOriginalFormatEv to i8*), i8* bitcast (void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)* @_ZN5draco37SequentialAttributeDecodersController23CreateSequentialDecoderEh to i8*)] }, align 4
@.str = private unnamed_addr constant [25 x i8] c"skip_attribute_transform\00", align 1
@_ZTVN5draco17AttributesDecoderE = external unnamed_addr constant { [14 x i8*] }, align 4
@.str.1 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco37SequentialAttributeDecodersControllerC1ENSt3__210unique_ptrINS_15PointsSequencerENS1_14default_deleteIS3_EEEE = hidden unnamed_addr alias %"class.draco::SequentialAttributeDecodersController"* (%"class.draco::SequentialAttributeDecodersController"*, %"class.std::__2::unique_ptr.144"*), %"class.draco::SequentialAttributeDecodersController"* (%"class.draco::SequentialAttributeDecodersController"*, %"class.std::__2::unique_ptr.144"*)* @_ZN5draco37SequentialAttributeDecodersControllerC2ENSt3__210unique_ptrINS_15PointsSequencerENS1_14default_deleteIS3_EEEE

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::SequentialAttributeDecodersController"* @_ZN5draco37SequentialAttributeDecodersControllerC2ENSt3__210unique_ptrINS_15PointsSequencerENS1_14default_deleteIS3_EEEE(%"class.draco::SequentialAttributeDecodersController"* returned %this, %"class.std::__2::unique_ptr.144"* %sequencer) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %call = call %"class.draco::AttributesDecoder"* @_ZN5draco17AttributesDecoderC2Ev(%"class.draco::AttributesDecoder"* %0)
  %1 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTVN5draco37SequentialAttributeDecodersControllerE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %sequential_decoders_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.124"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.124"* %sequential_decoders_) #8
  %point_ids_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 2
  %call3 = call %"class.std::__2::vector.136"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.136"* %point_ids_) #8
  %sequencer_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 3
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.144"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.144"* nonnull align 4 dereferenceable(4) %sequencer) #8
  %call5 = call %"class.std::__2::unique_ptr.144"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.144"* %sequencer_, %"class.std::__2::unique_ptr.144"* nonnull align 4 dereferenceable(4) %call4) #8
  ret %"class.draco::SequentialAttributeDecodersController"* %this1
}

declare %"class.draco::AttributesDecoder"* @_ZN5draco17AttributesDecoderC2Ev(%"class.draco::AttributesDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.124"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.124"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call = call %"class.std::__2::__vector_base.125"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.125"* %0) #8
  ret %"class.std::__2::vector.124"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.136"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.136"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.136"*, align 4
  store %"class.std::__2::vector.136"* %this, %"class.std::__2::vector.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.136"* %this1 to %"class.std::__2::__vector_base.137"*
  %call = call %"class.std::__2::__vector_base.137"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.137"* %0) #8
  ret %"class.std::__2::vector.136"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.144"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.144"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  store %"class.std::__2::unique_ptr.144"* %__t, %"class.std::__2::unique_ptr.144"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.144"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.144"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.144"* returned %this, %"class.std::__2::unique_ptr.144"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  %ref.tmp = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.std::__2::unique_ptr.144"* %this, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.144"* %__u, %"class.std::__2::unique_ptr.144"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %__u.addr, align 4
  %call = call %"class.draco::PointsSequencer"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.144"* %0) #8
  store %"class.draco::PointsSequencer"* %call, %"class.draco::PointsSequencer"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.144"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__27forwardINS_14default_deleteIN5draco15PointsSequencerEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::__compressed_pair.145"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.145"* %__ptr_, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.144"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco37SequentialAttributeDecodersController27DecodeAttributesDecoderDataEPNS_13DecoderBufferE(%"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::DecoderBuffer"* %buffer) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %num_attributes = alloca i32, align 4
  %i = alloca i32, align 4
  %decoder_type = alloca i8, align 1
  %ref.tmp = alloca %"class.std::__2::unique_ptr.126", align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco17AttributesDecoder27DecodeAttributesDecoderDataEPNS_13DecoderBufferE(%"class.draco::AttributesDecoder"* %0, %"class.draco::DecoderBuffer"* %1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %3 = bitcast %"class.draco::AttributesDecoder"* %2 to i32 (%"class.draco::AttributesDecoder"*)***
  %vtable = load i32 (%"class.draco::AttributesDecoder"*)**, i32 (%"class.draco::AttributesDecoder"*)*** %3, align 4
  %vfn = getelementptr inbounds i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vtable, i64 6
  %4 = load i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vfn, align 4
  %call2 = call i32 %4(%"class.draco::AttributesDecoder"* %2)
  store i32 %call2, i32* %num_attributes, align 4
  %sequential_decoders_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %5 = load i32, i32* %num_attributes, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.124"* %sequential_decoders_, i32 %5)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %num_attributes, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call3 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %8, i8* %decoder_type)
  br i1 %call3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end5:                                          ; preds = %for.body
  %9 = load i8, i8* %decoder_type, align 1
  %10 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)***
  %vtable6 = load void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)**, void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)*** %10, align 4
  %vfn7 = getelementptr inbounds void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)*, void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)** %vtable6, i64 12
  %11 = load void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)*, void (%"class.std::__2::unique_ptr.126"*, %"class.draco::SequentialAttributeDecodersController"*, i8)** %vfn7, align 4
  call void %11(%"class.std::__2::unique_ptr.126"* sret align 4 %ref.tmp, %"class.draco::SequentialAttributeDecodersController"* %this1, i8 zeroext %9)
  %sequential_decoders_8 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %12 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_8, i32 %12) #8
  %call10 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.126"* %call9, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  %call11 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.126"* %ref.tmp) #8
  %sequential_decoders_12 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %13 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_12, i32 %13) #8
  %call14 = call zeroext i1 @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.126"* %call13) #8
  br i1 %call14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end5
  store i1 false, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %if.end5
  %sequential_decoders_17 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %14 = load i32, i32* %i, align 4
  %call18 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_17, i32 %14) #8
  %call19 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call18) #8
  %15 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %16 = bitcast %"class.draco::AttributesDecoder"* %15 to %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)***
  %vtable20 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)**, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*** %16, align 4
  %vfn21 = getelementptr inbounds %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vtable20, i64 7
  %17 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vfn21, align 4
  %call22 = call %"class.draco::PointCloudDecoder"* %17(%"class.draco::AttributesDecoder"* %15)
  %18 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %19 = load i32, i32* %i, align 4
  %20 = bitcast %"class.draco::AttributesDecoder"* %18 to i32 (%"class.draco::AttributesDecoder"*, i32)***
  %vtable23 = load i32 (%"class.draco::AttributesDecoder"*, i32)**, i32 (%"class.draco::AttributesDecoder"*, i32)*** %20, align 4
  %vfn24 = getelementptr inbounds i32 (%"class.draco::AttributesDecoder"*, i32)*, i32 (%"class.draco::AttributesDecoder"*, i32)** %vtable23, i64 5
  %21 = load i32 (%"class.draco::AttributesDecoder"*, i32)*, i32 (%"class.draco::AttributesDecoder"*, i32)** %vfn24, align 4
  %call25 = call i32 %21(%"class.draco::AttributesDecoder"* %18, i32 %19)
  %22 = bitcast %"class.draco::SequentialAttributeDecoder"* %call19 to i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)***
  %vtable26 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)**, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)*** %22, align 4
  %vfn27 = getelementptr inbounds i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)** %vtable26, i64 2
  %23 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)** %vfn27, align 4
  %call28 = call zeroext i1 %23(%"class.draco::SequentialAttributeDecoder"* %call19, %"class.draco::PointCloudDecoder"* %call22, i32 %call25)
  br i1 %call28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %if.end16
  store i1 false, i1* %retval, align 1
  br label %return

if.end30:                                         ; preds = %if.end16
  br label %for.inc

for.inc:                                          ; preds = %if.end30
  %24 = load i32, i32* %i, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then29, %if.then15, %if.then4, %if.then
  %25 = load i1, i1* %retval, align 1
  ret i1 %25
}

declare zeroext i1 @_ZN5draco17AttributesDecoder27DecodeAttributesDecoderDataEPNS_13DecoderBufferE(%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.124"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.124"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %7, i32 %8
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.124"* %this1, %"class.std::__2::unique_ptr.126"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.126"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__u, %"class.std::__2::unique_ptr.126"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__u.addr, align 4
  %call = call %"class.draco::SequentialAttributeDecoder"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.126"* %0) #8
  call void @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.126"* %this1, %"class.draco::SequentialAttributeDecoder"* %call) #8
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.126"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26SequentialAttributeDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %call2) #8
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.127"* %__ptr_) #8
  ret %"class.std::__2::unique_ptr.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.126"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.126"* %this1, %"class.draco::SequentialAttributeDecoder"* null) #8
  ret %"class.std::__2::unique_ptr.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNKSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__ptr_) #8
  %0 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %call, align 4
  %cmp = icmp ne %"class.draco::SequentialAttributeDecoder"* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNKSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__ptr_) #8
  %0 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %call, align 4
  ret %"class.draco::SequentialAttributeDecoder"* %0
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco37SequentialAttributeDecodersController16DecodeAttributesEPNS_13DecoderBufferE(%"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::DecoderBuffer"* %buffer) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %num_attributes = alloca i32, align 4
  %i = alloca i32, align 4
  %pa = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %sequencer_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 3
  %call = call zeroext i1 @_ZNKSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.144"* %sequencer_) #8
  br i1 %call, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %sequencer_2 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 3
  %call3 = call %"class.draco::PointsSequencer"* @_ZNKSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.144"* %sequencer_2) #8
  %point_ids_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 2
  %call4 = call zeroext i1 @_ZN5draco15PointsSequencer16GenerateSequenceEPNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEE(%"class.draco::PointsSequencer"* %call3, %"class.std::__2::vector.136"* %point_ids_)
  br i1 %call4, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %1 = bitcast %"class.draco::AttributesDecoder"* %0 to i32 (%"class.draco::AttributesDecoder"*)***
  %vtable = load i32 (%"class.draco::AttributesDecoder"*)**, i32 (%"class.draco::AttributesDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vtable, i64 6
  %2 = load i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vfn, align 4
  %call5 = call i32 %2(%"class.draco::AttributesDecoder"* %0)
  store i32 %call5, i32* %num_attributes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_attributes, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %6 = bitcast %"class.draco::AttributesDecoder"* %5 to %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)***
  %vtable6 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)**, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*** %6, align 4
  %vfn7 = getelementptr inbounds %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vtable6, i64 7
  %7 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vfn7, align 4
  %call8 = call %"class.draco::PointCloudDecoder"* %7(%"class.draco::AttributesDecoder"* %5)
  %call9 = call %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %call8)
  %8 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %9 = load i32, i32* %i, align 4
  %10 = bitcast %"class.draco::AttributesDecoder"* %8 to i32 (%"class.draco::AttributesDecoder"*, i32)***
  %vtable10 = load i32 (%"class.draco::AttributesDecoder"*, i32)**, i32 (%"class.draco::AttributesDecoder"*, i32)*** %10, align 4
  %vfn11 = getelementptr inbounds i32 (%"class.draco::AttributesDecoder"*, i32)*, i32 (%"class.draco::AttributesDecoder"*, i32)** %vtable10, i64 5
  %11 = load i32 (%"class.draco::AttributesDecoder"*, i32)*, i32 (%"class.draco::AttributesDecoder"*, i32)** %vfn11, align 4
  %call12 = call i32 %11(%"class.draco::AttributesDecoder"* %8, i32 %9)
  %call13 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %call9, i32 %call12)
  store %"class.draco::PointAttribute"* %call13, %"class.draco::PointAttribute"** %pa, align 4
  %sequencer_14 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 3
  %call15 = call %"class.draco::PointsSequencer"* @_ZNKSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.144"* %sequencer_14) #8
  %12 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pa, align 4
  %13 = bitcast %"class.draco::PointsSequencer"* %call15 to i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)***
  %vtable16 = load i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)**, i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)*** %13, align 4
  %vfn17 = getelementptr inbounds i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)*, i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)** %vtable16, i64 2
  %14 = load i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)*, i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)** %vfn17, align 4
  %call18 = call zeroext i1 %14(%"class.draco::PointsSequencer"* %call15, %"class.draco::PointAttribute"* %12)
  br i1 %call18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end20:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end20
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %17 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call21 = call zeroext i1 @_ZN5draco17AttributesDecoder16DecodeAttributesEPNS_13DecoderBufferE(%"class.draco::AttributesDecoder"* %16, %"class.draco::DecoderBuffer"* %17)
  store i1 %call21, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then19, %if.then
  %18 = load i1, i1* %retval, align 1
  ret i1 %18
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  store %"class.std::__2::unique_ptr.144"* %this, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNKSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %__ptr_) #8
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  %cmp = icmp ne %"class.draco::PointsSequencer"* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointsSequencer"* @_ZNKSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  store %"class.std::__2::unique_ptr.144"* %this, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNKSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %__ptr_) #8
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  ret %"class.draco::PointsSequencer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco15PointsSequencer16GenerateSequenceEPNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEE(%"class.draco::PointsSequencer"* %this, %"class.std::__2::vector.136"* %out_point_ids) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %out_point_ids.addr = alloca %"class.std::__2::vector.136"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  store %"class.std::__2::vector.136"* %out_point_ids, %"class.std::__2::vector.136"** %out_point_ids.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  %0 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %out_point_ids.addr, align 4
  %out_point_ids_ = getelementptr inbounds %"class.draco::PointsSequencer", %"class.draco::PointsSequencer"* %this1, i32 0, i32 1
  store %"class.std::__2::vector.136"* %0, %"class.std::__2::vector.136"** %out_point_ids_, align 4
  %1 = bitcast %"class.draco::PointsSequencer"* %this1 to i1 (%"class.draco::PointsSequencer"*)***
  %vtable = load i1 (%"class.draco::PointsSequencer"*)**, i1 (%"class.draco::PointsSequencer"*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::PointsSequencer"*)*, i1 (%"class.draco::PointsSequencer"*)** %vtable, i64 3
  %2 = load i1 (%"class.draco::PointsSequencer"*)*, i1 (%"class.draco::PointsSequencer"*)** %vfn, align 4
  %call = call zeroext i1 %2(%"class.draco::PointsSequencer"* %this1)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %point_cloud_, align 4
  ret %"class.draco::PointCloud"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %this, i32 %att_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %0 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.60"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.58"* %attributes_, i32 %0) #8
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.60"* %call) #8
  ret %"class.draco::PointAttribute"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17AttributesDecoder16DecodeAttributesEPNS_13DecoderBufferE(%"class.draco::AttributesDecoder"* %this, %"class.draco::DecoderBuffer"* %in_buffer) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::AttributesDecoder"* %this, %"class.draco::AttributesDecoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %this1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %1 = bitcast %"class.draco::AttributesDecoder"* %this1 to i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)***
  %vtable = load i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)**, i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)** %vtable, i64 9
  %2 = load i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)** %vfn, align 4
  %call = call zeroext i1 %2(%"class.draco::AttributesDecoder"* %this1, %"class.draco::DecoderBuffer"* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %4 = bitcast %"class.draco::AttributesDecoder"* %this1 to i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)***
  %vtable2 = load i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)**, i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)*** %4, align 4
  %vfn3 = getelementptr inbounds i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)** %vtable2, i64 10
  %5 = load i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoder"*, %"class.draco::DecoderBuffer"*)** %vfn3, align 4
  %call4 = call zeroext i1 %5(%"class.draco::AttributesDecoder"* %this1, %"class.draco::DecoderBuffer"* %3)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.end
  %6 = bitcast %"class.draco::AttributesDecoder"* %this1 to i1 (%"class.draco::AttributesDecoder"*)***
  %vtable7 = load i1 (%"class.draco::AttributesDecoder"*)**, i1 (%"class.draco::AttributesDecoder"*)*** %6, align 4
  %vfn8 = getelementptr inbounds i1 (%"class.draco::AttributesDecoder"*)*, i1 (%"class.draco::AttributesDecoder"*)** %vtable7, i64 11
  %7 = load i1 (%"class.draco::AttributesDecoder"*)*, i1 (%"class.draco::AttributesDecoder"*)** %vfn8, align 4
  %call9 = call zeroext i1 %7(%"class.draco::AttributesDecoder"* %this1)
  br i1 %call9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %if.end6
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end6
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end11, %if.then10, %if.then5, %if.then
  %8 = load i1, i1* %retval, align 1
  ret i1 %8
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco37SequentialAttributeDecodersController24DecodePortableAttributesEPNS_13DecoderBufferE(%"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::DecoderBuffer"* %in_buffer) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %num_attributes = alloca i32, align 4
  %i = alloca i32, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %1 = bitcast %"class.draco::AttributesDecoder"* %0 to i32 (%"class.draco::AttributesDecoder"*)***
  %vtable = load i32 (%"class.draco::AttributesDecoder"*)**, i32 (%"class.draco::AttributesDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vtable, i64 6
  %2 = load i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vfn, align 4
  %call = call i32 %2(%"class.draco::AttributesDecoder"* %0)
  store i32 %call, i32* %num_attributes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_attributes, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %sequential_decoders_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %5 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_, i32 %5) #8
  %call3 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call2) #8
  %point_ids_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 2
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %7 = bitcast %"class.draco::SequentialAttributeDecoder"* %call3 to i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)***
  %vtable4 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)**, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)*** %7, align 4
  %vfn5 = getelementptr inbounds i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)** %vtable4, i64 4
  %8 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)** %vfn5, align 4
  %call6 = call zeroext i1 %8(%"class.draco::SequentialAttributeDecoder"* %call3, %"class.std::__2::vector.136"* nonnull align 4 dereferenceable(12) %point_ids_, %"class.draco::DecoderBuffer"* %6)
  br i1 %call6, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco37SequentialAttributeDecodersController36DecodeDataNeededByPortableTransformsEPNS_13DecoderBufferE(%"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::DecoderBuffer"* %in_buffer) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %num_attributes = alloca i32, align 4
  %i = alloca i32, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %1 = bitcast %"class.draco::AttributesDecoder"* %0 to i32 (%"class.draco::AttributesDecoder"*)***
  %vtable = load i32 (%"class.draco::AttributesDecoder"*)**, i32 (%"class.draco::AttributesDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vtable, i64 6
  %2 = load i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vfn, align 4
  %call = call i32 %2(%"class.draco::AttributesDecoder"* %0)
  store i32 %call, i32* %num_attributes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_attributes, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %sequential_decoders_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %5 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_, i32 %5) #8
  %call3 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call2) #8
  %point_ids_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 2
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %7 = bitcast %"class.draco::SequentialAttributeDecoder"* %call3 to i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)***
  %vtable4 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)**, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)*** %7, align 4
  %vfn5 = getelementptr inbounds i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)** %vtable4, i64 5
  %8 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*, %"class.draco::DecoderBuffer"*)** %vfn5, align 4
  %call6 = call zeroext i1 %8(%"class.draco::SequentialAttributeDecoder"* %call3, %"class.std::__2::vector.136"* nonnull align 4 dereferenceable(12) %point_ids_, %"class.draco::DecoderBuffer"* %6)
  br i1 %call6, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco37SequentialAttributeDecodersController35TransformAttributesToOriginalFormatEv(%"class.draco::SequentialAttributeDecodersController"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  %num_attributes = alloca i32, align 4
  %i = alloca i32, align 4
  %attribute = alloca %"class.draco::PointAttribute"*, align 4
  %portable_attribute = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp19 = alloca %"class.std::__2::basic_string", align 4
  %cleanup.cond = alloca i1, align 1
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %1 = bitcast %"class.draco::AttributesDecoder"* %0 to i32 (%"class.draco::AttributesDecoder"*)***
  %vtable = load i32 (%"class.draco::AttributesDecoder"*)**, i32 (%"class.draco::AttributesDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vtable, i64 6
  %2 = load i32 (%"class.draco::AttributesDecoder"*)*, i32 (%"class.draco::AttributesDecoder"*)** %vfn, align 4
  %call = call i32 %2(%"class.draco::AttributesDecoder"* %0)
  store i32 %call, i32* %num_attributes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_attributes, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %6 = bitcast %"class.draco::AttributesDecoder"* %5 to %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)***
  %vtable2 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)**, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*** %6, align 4
  %vfn3 = getelementptr inbounds %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vtable2, i64 7
  %7 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vfn3, align 4
  %call4 = call %"class.draco::PointCloudDecoder"* %7(%"class.draco::AttributesDecoder"* %5)
  %call5 = call %"class.draco::DracoOptions"* @_ZNK5draco17PointCloudDecoder7optionsEv(%"class.draco::PointCloudDecoder"* %call4)
  %tobool = icmp ne %"class.draco::DracoOptions"* %call5, null
  br i1 %tobool, label %if.then, label %if.end28

if.then:                                          ; preds = %for.body
  %sequential_decoders_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_, i32 %8) #8
  %call7 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call6) #8
  %call8 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %call7)
  store %"class.draco::PointAttribute"* %call8, %"class.draco::PointAttribute"** %attribute, align 4
  %sequential_decoders_9 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %9 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_9, i32 %9) #8
  %call11 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call10) #8
  %call12 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder20GetPortableAttributeEv(%"class.draco::SequentialAttributeDecoder"* %call11)
  store %"class.draco::PointAttribute"* %call12, %"class.draco::PointAttribute"** %portable_attribute, align 4
  %10 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %portable_attribute, align 4
  %tobool13 = icmp ne %"class.draco::PointAttribute"* %10, null
  store i1 false, i1* %cleanup.cond, align 1
  br i1 %tobool13, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.then
  %11 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %12 = bitcast %"class.draco::AttributesDecoder"* %11 to %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)***
  %vtable14 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)**, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*** %12, align 4
  %vfn15 = getelementptr inbounds %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vtable14, i64 7
  %13 = load %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)*, %"class.draco::PointCloudDecoder"* (%"class.draco::AttributesDecoder"*)** %vfn15, align 4
  %call16 = call %"class.draco::PointCloudDecoder"* %13(%"class.draco::AttributesDecoder"* %11)
  %call17 = call %"class.draco::DracoOptions"* @_ZNK5draco17PointCloudDecoder7optionsEv(%"class.draco::PointCloudDecoder"* %call16)
  %14 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute, align 4
  %15 = bitcast %"class.draco::PointAttribute"* %14 to %"class.draco::GeometryAttribute"*
  %call18 = call i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %15)
  store i32 %call18, i32* %ref.tmp, align 4
  %call20 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp19, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0))
  store i1 true, i1* %cleanup.cond, align 1
  %call21 = call zeroext i1 @_ZNK5draco12DracoOptionsINS_17GeometryAttribute4TypeEE16GetAttributeBoolERKS2_RKNSt3__212basic_stringIcNS6_11char_traitsIcEENS6_9allocatorIcEEEEb(%"class.draco::DracoOptions"* %call17, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp19, i1 zeroext false)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.then
  %16 = phi i1 [ false, %if.then ], [ %call21, %land.rhs ]
  %cleanup.is_active = load i1, i1* %cleanup.cond, align 1
  br i1 %cleanup.is_active, label %cleanup.action, label %cleanup.done

cleanup.action:                                   ; preds = %land.end
  %call22 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp19) #8
  br label %cleanup.done

cleanup.done:                                     ; preds = %cleanup.action, %land.end
  br i1 %16, label %if.then23, label %if.end

if.then23:                                        ; preds = %cleanup.done
  %sequential_decoders_24 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %17 = load i32, i32* %i, align 4
  %call25 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_24, i32 %17) #8
  %call26 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call25) #8
  %call27 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %call26)
  %18 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %portable_attribute, align 4
  call void @_ZN5draco14PointAttribute8CopyFromERKS0_(%"class.draco::PointAttribute"* %call27, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %18)
  br label %for.inc

if.end:                                           ; preds = %cleanup.done
  br label %if.end28

if.end28:                                         ; preds = %if.end, %for.body
  %sequential_decoders_29 = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %19 = load i32, i32* %i, align 4
  %call30 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_29, i32 %19) #8
  %call31 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call30) #8
  %point_ids_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 2
  %20 = bitcast %"class.draco::SequentialAttributeDecoder"* %call31 to i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*)***
  %vtable32 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*)**, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*)*** %20, align 4
  %vfn33 = getelementptr inbounds i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*)** %vtable32, i64 6
  %21 = load i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*)*, i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.136"*)** %vfn33, align 4
  %call34 = call zeroext i1 %21(%"class.draco::SequentialAttributeDecoder"* %call31, %"class.std::__2::vector.136"* nonnull align 4 dereferenceable(12) %point_ids_)
  br i1 %call34, label %if.end36, label %if.then35

if.then35:                                        ; preds = %if.end28
  store i1 false, i1* %retval, align 1
  br label %return

if.end36:                                         ; preds = %if.end28
  br label %for.inc

for.inc:                                          ; preds = %if.end36, %if.then23
  %22 = load i32, i32* %i, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then35
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DracoOptions"* @_ZNK5draco17PointCloudDecoder7optionsEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 7
  %0 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %options_, align 4
  ret %"class.draco::DracoOptions"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.draco::SequentialAttributeDecoder"* %this, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %attribute_ = getelementptr inbounds %"class.draco::SequentialAttributeDecoder", %"class.draco::SequentialAttributeDecoder"* %this1, i32 0, i32 2
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute_, align 4
  ret %"class.draco::PointAttribute"* %0
}

declare %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder20GetPortableAttributeEv(%"class.draco::SequentialAttributeDecoder"*) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco12DracoOptionsINS_17GeometryAttribute4TypeEE16GetAttributeBoolERKS2_RKNSt3__212basic_stringIcNS6_11char_traitsIcEENS6_9allocatorIcEEEEb(%"class.draco::DracoOptions"* %this, i32* nonnull align 4 dereferenceable(4) %att_key, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, i1 zeroext %default_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DracoOptions"*, align 4
  %att_key.addr = alloca i32*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %default_val.addr = alloca i8, align 1
  %att_options = alloca %"class.draco::Options"*, align 4
  store %"class.draco::DracoOptions"* %this, %"class.draco::DracoOptions"** %this.addr, align 4
  store i32* %att_key, i32** %att_key.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %frombool = zext i1 %default_val to i8
  store i8 %frombool, i8* %default_val.addr, align 1
  %this1 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %this.addr, align 4
  %0 = load i32*, i32** %att_key.addr, align 4
  %call = call %"class.draco::Options"* @_ZNK5draco12DracoOptionsINS_17GeometryAttribute4TypeEE20FindAttributeOptionsERKS2_(%"class.draco::DracoOptions"* %this1, i32* nonnull align 4 dereferenceable(4) %0)
  store %"class.draco::Options"* %call, %"class.draco::Options"** %att_options, align 4
  %1 = load %"class.draco::Options"*, %"class.draco::Options"** %att_options, align 4
  %tobool = icmp ne %"class.draco::Options"* %1, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %"class.draco::Options"*, %"class.draco::Options"** %att_options, align 4
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call2 = call zeroext i1 @_ZNK5draco7Options11IsOptionSetERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.draco::Options"* %2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %3)
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %4 = load %"class.draco::Options"*, %"class.draco::Options"** %att_options, align 4
  %5 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %6 = load i8, i8* %default_val.addr, align 1
  %tobool3 = trunc i8 %6 to i1
  %call4 = call zeroext i1 @_ZNK5draco7Options7GetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"* %4, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %5, i1 zeroext %tobool3)
  store i1 %call4, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %global_options_ = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 0
  %7 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %8 = load i8, i8* %default_val.addr, align 1
  %tobool5 = trunc i8 %8 to i1
  %call6 = call zeroext i1 @_ZNK5draco7Options7GetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"* %global_options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %7, i1 zeroext %tobool5)
  store i1 %call6, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %attribute_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 7
  %0 = load i32, i32* %attribute_type_, align 8
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* returned %this, i8* %__s) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.149"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.149"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i8*, i8** %__s.addr, align 4
  %call3 = call i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %2) #8
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %call3)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #2

declare void @_ZN5draco14PointAttribute8CopyFromERKS0_(%"class.draco::PointAttribute"*, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92)) #1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco37SequentialAttributeDecodersController23CreateSequentialDecoderEh(%"class.std::__2::unique_ptr.126"* noalias sret align 4 %agg.result, %"class.draco::SequentialAttributeDecodersController"* %this, i8 zeroext %decoder_type) unnamed_addr #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  %decoder_type.addr = alloca i8, align 1
  %0 = bitcast %"class.std::__2::unique_ptr.126"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  store i8 %decoder_type, i8* %decoder_type.addr, align 1
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %1 = load i8, i8* %decoder_type.addr, align 1
  %conv = zext i8 %1 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb4
    i32 2, label %sw.bb8
  ]

sw.bb:                                            ; preds = %entry
  %call = call noalias nonnull i8* @_Znwm(i32 20) #9
  %2 = bitcast i8* %call to %"class.draco::SequentialAttributeDecoder"*
  %call2 = call %"class.draco::SequentialAttributeDecoder"* @_ZN5draco26SequentialAttributeDecoderC1Ev(%"class.draco::SequentialAttributeDecoder"* %2)
  %call3 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.126"* %agg.result, %"class.draco::SequentialAttributeDecoder"* %2) #8
  br label %return

sw.bb4:                                           ; preds = %entry
  %call5 = call noalias nonnull i8* @_Znwm(i32 24) #9
  %3 = bitcast i8* %call5 to %"class.draco::SequentialIntegerAttributeDecoder"*
  %call6 = call %"class.draco::SequentialIntegerAttributeDecoder"* @_ZN5draco33SequentialIntegerAttributeDecoderC1Ev(%"class.draco::SequentialIntegerAttributeDecoder"* %3)
  %4 = bitcast %"class.draco::SequentialIntegerAttributeDecoder"* %3 to %"class.draco::SequentialAttributeDecoder"*
  %call7 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.126"* %agg.result, %"class.draco::SequentialAttributeDecoder"* %4) #8
  br label %return

sw.bb8:                                           ; preds = %entry
  %call9 = call noalias nonnull i8* @_Znwm(i32 36) #9
  %5 = bitcast i8* %call9 to %"class.draco::SequentialQuantizationAttributeDecoder"*
  %call10 = call %"class.draco::SequentialQuantizationAttributeDecoder"* @_ZN5draco38SequentialQuantizationAttributeDecoderC1Ev(%"class.draco::SequentialQuantizationAttributeDecoder"* %5)
  %6 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %5 to %"class.draco::SequentialAttributeDecoder"*
  %call11 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.126"* %agg.result, %"class.draco::SequentialAttributeDecoder"* %6) #8
  br label %return

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  %call12 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.126"* %agg.result, i8* null) #8
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb8, %sw.bb4, %sw.bb
  ret void
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #3

declare %"class.draco::SequentialAttributeDecoder"* @_ZN5draco26SequentialAttributeDecoderC1Ev(%"class.draco::SequentialAttributeDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.126"* returned %this, %"class.draco::SequentialAttributeDecoder"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__p.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"* %__p, %"class.draco::SequentialAttributeDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.127"* %__ptr_, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.126"* %this1
}

declare %"class.draco::SequentialIntegerAttributeDecoder"* @_ZN5draco33SequentialIntegerAttributeDecoderC1Ev(%"class.draco::SequentialIntegerAttributeDecoder"* returned) unnamed_addr #1

declare %"class.draco::SequentialQuantizationAttributeDecoder"* @_ZN5draco38SequentialQuantizationAttributeDecoderC1Ev(%"class.draco::SequentialQuantizationAttributeDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.126"* returned %this, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %.addr = alloca i8*, align 4
  %ref.tmp = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  store %"class.draco::SequentialAttributeDecoder"* null, %"class.draco::SequentialAttributeDecoder"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.127"* %__ptr_, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::SequentialAttributeDecodersController"* @_ZN5draco37SequentialAttributeDecodersControllerD2Ev(%"class.draco::SequentialAttributeDecodersController"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTVN5draco37SequentialAttributeDecodersControllerE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %sequencer_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 3
  %call = call %"class.std::__2::unique_ptr.144"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.144"* %sequencer_) #8
  %point_ids_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.136"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.136"* %point_ids_) #8
  %sequential_decoders_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::vector.124"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.124"* %sequential_decoders_) #8
  %1 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %call4 = call %"class.draco::AttributesDecoder"* @_ZN5draco17AttributesDecoderD2Ev(%"class.draco::AttributesDecoder"* %1) #8
  ret %"class.draco::SequentialAttributeDecodersController"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco37SequentialAttributeDecodersControllerD0Ev(%"class.draco::SequentialAttributeDecodersController"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %call = call %"class.draco::SequentialAttributeDecodersController"* @_ZN5draco37SequentialAttributeDecodersControllerD2Ev(%"class.draco::SequentialAttributeDecodersController"* %this1) #8
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

declare zeroext i1 @_ZN5draco17AttributesDecoder4InitEPNS_17PointCloudDecoderEPNS_10PointCloudE(%"class.draco::AttributesDecoder"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17AttributesDecoder14GetAttributeIdEi(%"class.draco::AttributesDecoder"* %this, i32 %i) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  %i.addr = alloca i32, align 4
  store %"class.draco::AttributesDecoder"* %this, %"class.draco::AttributesDecoder"** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %point_attribute_ids_ = getelementptr inbounds %"class.draco::AttributesDecoder", %"class.draco::AttributesDecoder"* %this1, i32 0, i32 1
  %0 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %point_attribute_ids_, i32 %0) #8
  %1 = load i32, i32* %call, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17AttributesDecoder16GetNumAttributesEv(%"class.draco::AttributesDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.draco::AttributesDecoder"* %this, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %point_attribute_ids_ = getelementptr inbounds %"class.draco::AttributesDecoder", %"class.draco::AttributesDecoder"* %this1, i32 0, i32 1
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %point_attribute_ids_) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudDecoder"* @_ZNK5draco17AttributesDecoder10GetDecoderEv(%"class.draco::AttributesDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.draco::AttributesDecoder"* %this, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %point_cloud_decoder_ = getelementptr inbounds %"class.draco::AttributesDecoder", %"class.draco::AttributesDecoder"* %this1, i32 0, i32 3
  %0 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %point_cloud_decoder_, align 4
  ret %"class.draco::PointCloudDecoder"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco37SequentialAttributeDecodersController20GetPortableAttributeEi(%"class.draco::SequentialAttributeDecodersController"* %this, i32 %point_attribute_id) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.draco::PointAttribute"*, align 4
  %this.addr = alloca %"class.draco::SequentialAttributeDecodersController"*, align 4
  %point_attribute_id.addr = alloca i32, align 4
  %loc_id = alloca i32, align 4
  store %"class.draco::SequentialAttributeDecodersController"* %this, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  store i32 %point_attribute_id, i32* %point_attribute_id.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecodersController"*, %"class.draco::SequentialAttributeDecodersController"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecodersController"* %this1 to %"class.draco::AttributesDecoder"*
  %1 = load i32, i32* %point_attribute_id.addr, align 4
  %call = call i32 @_ZNK5draco17AttributesDecoder27GetLocalIdForPointAttributeEi(%"class.draco::AttributesDecoder"* %0, i32 %1)
  store i32 %call, i32* %loc_id, align 4
  %2 = load i32, i32* %loc_id, align 4
  %cmp = icmp slt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %sequential_decoders_ = getelementptr inbounds %"class.draco::SequentialAttributeDecodersController", %"class.draco::SequentialAttributeDecodersController"* %this1, i32 0, i32 1
  %3 = load i32, i32* %loc_id, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.124"* %sequential_decoders_, i32 %3) #8
  %call3 = call %"class.draco::SequentialAttributeDecoder"* @_ZNKSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.126"* %call2) #8
  %call4 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder20GetPortableAttributeEv(%"class.draco::SequentialAttributeDecoder"* %call3)
  store %"class.draco::PointAttribute"* %call4, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %retval, align 4
  ret %"class.draco::PointAttribute"* %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.60"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.58"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.58"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.58"* %this, %"class.std::__2::vector.58"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.58"*, %"class.std::__2::vector.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.58"* %this1 to %"class.std::__2::__vector_base.59"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.59", %"class.std::__2::__vector_base.59"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.60"*, %"class.std::__2::unique_ptr.60"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.60", %"class.std::__2::unique_ptr.60"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.60"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.60"*, align 4
  store %"class.std::__2::unique_ptr.60"* %this, %"class.std::__2::unique_ptr.60"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.60"*, %"class.std::__2::unique_ptr.60"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.60", %"class.std::__2::unique_ptr.60"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.61"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.61"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.61"*, align 4
  store %"class.std::__2::__compressed_pair.61"* %this, %"class.std::__2::__compressed_pair.61"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.61"*, %"class.std::__2::__compressed_pair.61"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.61"* %this1 to %"struct.std::__2::__compressed_pair_elem.62"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.62"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.62"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.62"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.62"* %this, %"struct.std::__2::__compressed_pair_elem.62"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.62"*, %"struct.std::__2::__compressed_pair_elem.62"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.62", %"struct.std::__2::__compressed_pair_elem.62"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.127"* returned %this, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  %__t1.addr = alloca %"class.draco::SequentialAttributeDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"** %__t1, %"class.draco::SequentialAttributeDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %1 = load %"class.draco::SequentialAttributeDecoder"**, %"class.draco::SequentialAttributeDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__27forwardIPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.128"* @_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.128"* %0, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.129"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.129"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.129"* %2)
  ret %"class.std::__2::__compressed_pair.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__27forwardIPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::SequentialAttributeDecoder"**, align 4
  store %"class.draco::SequentialAttributeDecoder"** %__t, %"class.draco::SequentialAttributeDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::SequentialAttributeDecoder"**, %"class.draco::SequentialAttributeDecoder"*** %__t.addr, align 4
  ret %"class.draco::SequentialAttributeDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.128"* @_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.128"* returned %this, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  %__u.addr = alloca %"class.draco::SequentialAttributeDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"** %__u, %"class.draco::SequentialAttributeDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.128", %"struct.std::__2::__compressed_pair_elem.128"* %this1, i32 0, i32 0
  %0 = load %"class.draco::SequentialAttributeDecoder"**, %"class.draco::SequentialAttributeDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__27forwardIPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %call, align 4
  store %"class.draco::SequentialAttributeDecoder"* %1, %"class.draco::SequentialAttributeDecoder"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.129"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.129"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.129"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.129"* %this, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.129"*, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.129"* %this1 to %"struct.std::__2::default_delete.130"*
  ret %"struct.std::__2::__compressed_pair_elem.129"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.144"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.144"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  store %"class.std::__2::unique_ptr.144"* %this, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.144"* %this1, %"class.draco::PointsSequencer"* null) #8
  ret %"class.std::__2::unique_ptr.144"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.136"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.136"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.136"*, align 4
  store %"class.std::__2::vector.136"* %this, %"class.std::__2::vector.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.136"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.136"* %this1 to %"class.std::__2::__vector_base.137"*
  %call = call %"class.std::__2::__vector_base.137"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.137"* %0) #8
  ret %"class.std::__2::vector.136"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.124"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.124"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.124"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call = call %"class.std::__2::__vector_base.125"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.125"* %0) #8
  ret %"class.std::__2::vector.124"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributesDecoder"* @_ZN5draco17AttributesDecoderD2Ev(%"class.draco::AttributesDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.draco::AttributesDecoder"* %this, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributesDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTVN5draco17AttributesDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %point_attribute_to_local_id_map_ = getelementptr inbounds %"class.draco::AttributesDecoder", %"class.draco::AttributesDecoder"* %this1, i32 0, i32 2
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector"* %point_attribute_to_local_id_map_) #8
  %point_attribute_ids_ = getelementptr inbounds %"class.draco::AttributesDecoder", %"class.draco::AttributesDecoder"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector"* %point_attribute_ids_) #8
  %1 = bitcast %"class.draco::AttributesDecoder"* %this1 to %"class.draco::AttributesDecoderInterface"*
  %call3 = call %"class.draco::AttributesDecoderInterface"* @_ZN5draco26AttributesDecoderInterfaceD2Ev(%"class.draco::AttributesDecoderInterface"* %1) #8
  ret %"class.draco::AttributesDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.144"* %this, %"class.draco::PointsSequencer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  %__p.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %__tmp = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.std::__2::unique_ptr.144"* %this, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__p, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %__ptr_) #8
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  store %"class.draco::PointsSequencer"* %0, %"class.draco::PointsSequencer"** %__tmp, align 4
  %1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %__ptr_2) #8
  store %"class.draco::PointsSequencer"* %1, %"class.draco::PointsSequencer"** %call3, align 4
  %2 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointsSequencer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.145"* %__ptr_4) #8
  %3 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_(%"struct.std::__2::default_delete.148"* %call5, %"class.draco::PointsSequencer"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.145"*, align 4
  store %"class.std::__2::__compressed_pair.145"* %this, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.145"*, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.145"* %this1 to %"struct.std::__2::__compressed_pair_elem.146"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.146"* %0) #8
  ret %"class.draco::PointsSequencer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.145"*, align 4
  store %"class.std::__2::__compressed_pair.145"* %this, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.145"*, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.145"* %this1 to %"struct.std::__2::__compressed_pair_elem.147"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.147"* %0) #8
  ret %"struct.std::__2::default_delete.148"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_(%"struct.std::__2::default_delete.148"* %this, %"class.draco::PointsSequencer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.148"*, align 4
  %__ptr.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"struct.std::__2::default_delete.148"* %this, %"struct.std::__2::default_delete.148"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__ptr, %"class.draco::PointsSequencer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.148"*, %"struct.std::__2::default_delete.148"** %this.addr, align 4
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointsSequencer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::PointsSequencer"* %0 to void (%"class.draco::PointsSequencer"*)***
  %vtable = load void (%"class.draco::PointsSequencer"*)**, void (%"class.draco::PointsSequencer"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::PointsSequencer"*)*, void (%"class.draco::PointsSequencer"*)** %vtable, i64 1
  %2 = load void (%"class.draco::PointsSequencer"*)*, void (%"class.draco::PointsSequencer"*)** %vfn, align 4
  call void %2(%"class.draco::PointsSequencer"* %0) #8
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.146"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.146"* %this, %"struct.std::__2::__compressed_pair_elem.146"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.146"*, %"struct.std::__2::__compressed_pair_elem.146"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.146", %"struct.std::__2::__compressed_pair_elem.146"* %this1, i32 0, i32 0
  ret %"class.draco::PointsSequencer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.147"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.147"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.147"* %this, %"struct.std::__2::__compressed_pair_elem.147"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.147"*, %"struct.std::__2::__compressed_pair_elem.147"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.147"* %this1 to %"struct.std::__2::default_delete.148"*
  ret %"struct.std::__2::default_delete.148"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.136"*, align 4
  store %"class.std::__2::vector.136"* %this, %"class.std::__2::vector.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %this.addr, align 4
  %call = call %"class.draco::IndexType.138"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.136"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.138"* %call to i8*
  %call2 = call %"class.draco::IndexType.138"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.136"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.136"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.138", %"class.draco::IndexType.138"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.138"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.138"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.136"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.136"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.138", %"class.draco::IndexType.138"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.138"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.138"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.136"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.136"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.138", %"class.draco::IndexType.138"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.138"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.136"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.137"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.137"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.137"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.137"*, align 4
  store %"class.std::__2::__vector_base.137"* %this, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  store %"class.std::__2::__vector_base.137"* %this1, %"class.std::__2::__vector_base.137"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType.138"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.137"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.142"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.137"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.137"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.142"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.138"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %retval, align 4
  ret %"class.std::__2::__vector_base.137"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.136"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.136"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.136"* %this, %"class.std::__2::vector.136"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.138"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.136"*, align 4
  store %"class.std::__2::vector.136"* %this, %"class.std::__2::vector.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.136"* %this1 to %"class.std::__2::__vector_base.137"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__begin_, align 4
  %call = call %"class.draco::IndexType.138"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.138"* %1) #8
  ret %"class.draco::IndexType.138"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.136"*, align 4
  store %"class.std::__2::vector.136"* %this, %"class.std::__2::vector.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.136"* %this1 to %"class.std::__2::__vector_base.137"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.137"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.136"*, align 4
  store %"class.std::__2::vector.136"* %this, %"class.std::__2::vector.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.136"*, %"class.std::__2::vector.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.136"* %this1 to %"class.std::__2::__vector_base.137"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.136"* %this1 to %"class.std::__2::__vector_base.137"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.138"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.138"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.138"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.138"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.138"*, align 4
  store %"class.draco::IndexType.138"* %__p, %"class.draco::IndexType.138"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__p.addr, align 4
  ret %"class.draco::IndexType.138"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.137"*, align 4
  store %"class.std::__2::__vector_base.137"* %this, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.138"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.137"* %this1) #8
  %0 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.138"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.138"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.138"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.137"*, align 4
  store %"class.std::__2::__vector_base.137"* %this, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.138"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.139"* %__end_cap_) #8
  ret %"class.draco::IndexType.138"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.138"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.139"*, align 4
  store %"class.std::__2::__compressed_pair.139"* %this, %"class.std::__2::__compressed_pair.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.139"*, %"class.std::__2::__compressed_pair.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.139"* %this1 to %"struct.std::__2::__compressed_pair_elem.140"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.138"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.140"* %0) #8
  ret %"class.draco::IndexType.138"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.138"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.140"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.140"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.140"* %this, %"struct.std::__2::__compressed_pair_elem.140"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.140"*, %"struct.std::__2::__compressed_pair_elem.140"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.140", %"struct.std::__2::__compressed_pair_elem.140"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.138"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.137"*, align 4
  store %"class.std::__2::__vector_base.137"* %this, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.137"* %this1, %"class.draco::IndexType.138"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.142"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.138"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.142"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.138"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.142"* %__a, %"class.std::__2::allocator.142"** %__a.addr, align 4
  store %"class.draco::IndexType.138"* %__p, %"class.draco::IndexType.138"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.142"*, %"class.std::__2::allocator.142"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.142"* %0, %"class.draco::IndexType.138"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.142"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.137"*, align 4
  store %"class.std::__2::__vector_base.137"* %this, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.142"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.139"* %__end_cap_) #8
  ret %"class.std::__2::allocator.142"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.137"* %this, %"class.draco::IndexType.138"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.137"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.138"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType.138"*, align 4
  store %"class.std::__2::__vector_base.137"* %this, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  store %"class.draco::IndexType.138"* %__new_last, %"class.draco::IndexType.138"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__end_, align 4
  store %"class.draco::IndexType.138"* %0, %"class.draco::IndexType.138"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType.138"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.142"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.137"* %this1) #8
  %3 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.138", %"class.draco::IndexType.138"* %3, i32 -1
  store %"class.draco::IndexType.138"* %incdec.ptr, %"class.draco::IndexType.138"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType.138"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.138"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.142"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.138"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.138"* %4, %"class.draco::IndexType.138"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.142"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.138"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.142"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.138"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.142"* %__a, %"class.std::__2::allocator.142"** %__a.addr, align 4
  store %"class.draco::IndexType.138"* %__p, %"class.draco::IndexType.138"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.142"*, %"class.std::__2::allocator.142"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.142"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.138"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.142"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.138"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.142"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.138"*, align 4
  store %"class.std::__2::allocator.142"* %__a, %"class.std::__2::allocator.142"** %__a.addr, align 4
  store %"class.draco::IndexType.138"* %__p, %"class.draco::IndexType.138"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.142"*, %"class.std::__2::allocator.142"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.142"* %1, %"class.draco::IndexType.138"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.142"* %this, %"class.draco::IndexType.138"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.142"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.138"*, align 4
  store %"class.std::__2::allocator.142"* %this, %"class.std::__2::allocator.142"** %this.addr, align 4
  store %"class.draco::IndexType.138"* %__p, %"class.draco::IndexType.138"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.142"*, %"class.std::__2::allocator.142"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.142"* %this, %"class.draco::IndexType.138"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.142"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.138"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.142"* %this, %"class.std::__2::allocator.142"** %this.addr, align 4
  store %"class.draco::IndexType.138"* %__p, %"class.draco::IndexType.138"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.142"*, %"class.std::__2::allocator.142"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.138"*, %"class.draco::IndexType.138"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.138"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.142"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.139"*, align 4
  store %"class.std::__2::__compressed_pair.139"* %this, %"class.std::__2::__compressed_pair.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.139"*, %"class.std::__2::__compressed_pair.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.139"* %this1 to %"struct.std::__2::__compressed_pair_elem.141"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.142"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.141"* %0) #8
  ret %"class.std::__2::allocator.142"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.142"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.141"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.141"* %this, %"struct.std::__2::__compressed_pair_elem.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.141"*, %"struct.std::__2::__compressed_pair_elem.141"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.141"* %this1 to %"class.std::__2::allocator.142"*
  ret %"class.std::__2::allocator.142"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.126"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.124"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.124"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.124"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.125"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.125"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  store %"class.std::__2::__vector_base.125"* %this1, %"class.std::__2::__vector_base.125"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.126"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.125"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.125"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.126"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %retval, align 4
  ret %"class.std::__2::__vector_base.125"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.124"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.126"* %1) #8
  ret %"class.std::__2::unique_ptr.126"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.125"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.126"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.126"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.125"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.132"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %0) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.132"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.132"* %this, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.132"*, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.132", %"struct.std::__2::__compressed_pair_elem.132"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.126"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.125"* %this1, %"class.std::__2::unique_ptr.126"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.134"* %0, %"class.std::__2::unique_ptr.126"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.131"* %__end_cap_) #8
  ret %"class.std::__2::allocator.134"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.125"* %this, %"class.std::__2::unique_ptr.126"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__new_last, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  store %"class.std::__2::unique_ptr.126"* %0, %"class.std::__2::unique_ptr.126"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.126"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %this1) #8
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %3, i32 -1
  store %"class.std::__2::unique_ptr.126"* %incdec.ptr, %"class.std::__2::unique_ptr.126"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.126"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.126"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.126"* %4, %"class.std::__2::unique_ptr.126"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.164", align 1
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.164"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.126"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.134"* %1, %"class.std::__2::unique_ptr.126"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.134"* %this, %"class.std::__2::unique_ptr.126"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::allocator.134"* %this, %"class.std::__2::allocator.134"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.126"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.134"* %this, %"class.std::__2::unique_ptr.126"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.134"* %this, %"class.std::__2::allocator.134"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.126"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.133"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %0) #8
  ret %"class.std::__2::allocator.134"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.133"* %this1 to %"class.std::__2::allocator.134"*
  ret %"class.std::__2::allocator.134"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributesDecoderInterface"* @_ZN5draco26AttributesDecoderInterfaceD2Ev(%"class.draco::AttributesDecoderInterface"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.draco::AttributesDecoderInterface"* %this, %"class.draco::AttributesDecoderInterface"** %this.addr, align 4
  %this1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %this.addr, align 4
  ret %"class.draco::AttributesDecoderInterface"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.165", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.165"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17AttributesDecoder27GetLocalIdForPointAttributeEi(%"class.draco::AttributesDecoder"* %this, i32 %point_attribute_id) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  %point_attribute_id.addr = alloca i32, align 4
  %id_map_size = alloca i32, align 4
  store %"class.draco::AttributesDecoder"* %this, %"class.draco::AttributesDecoder"** %this.addr, align 4
  store i32 %point_attribute_id, i32* %point_attribute_id.addr, align 4
  %this1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %this.addr, align 4
  %point_attribute_to_local_id_map_ = getelementptr inbounds %"class.draco::AttributesDecoder", %"class.draco::AttributesDecoder"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %point_attribute_to_local_id_map_) #8
  store i32 %call, i32* %id_map_size, align 4
  %0 = load i32, i32* %point_attribute_id.addr, align 4
  %1 = load i32, i32* %id_map_size, align 4
  %cmp = icmp sge i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %point_attribute_to_local_id_map_2 = getelementptr inbounds %"class.draco::AttributesDecoder", %"class.draco::AttributesDecoder"* %this1, i32 0, i32 2
  %2 = load i32, i32* %point_attribute_id.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %point_attribute_to_local_id_map_2, i32 %2) #8
  %3 = load i32, i32* %call3, align 4
  store i32 %3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNKSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNKSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %0) #8
  ret %"class.draco::SequentialAttributeDecoder"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNKSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.128", %"struct.std::__2::__compressed_pair_elem.128"* %this1, i32 0, i32 0
  ret %"class.draco::SequentialAttributeDecoder"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointsSequencer"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  %__t = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.std::__2::unique_ptr.144"* %this, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %__ptr_) #8
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  store %"class.draco::PointsSequencer"* %0, %"class.draco::PointsSequencer"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %__ptr_2) #8
  store %"class.draco::PointsSequencer"* null, %"class.draco::PointsSequencer"** %call3, align 4
  %1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__t, align 4
  ret %"class.draco::PointsSequencer"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__27forwardINS_14default_deleteIN5draco15PointsSequencerEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.148"*, align 4
  store %"struct.std::__2::default_delete.148"* %__t, %"struct.std::__2::default_delete.148"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.148"*, %"struct.std::__2::default_delete.148"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.148"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.144"*, align 4
  store %"class.std::__2::unique_ptr.144"* %this, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.144"*, %"class.std::__2::unique_ptr.144"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.144", %"class.std::__2::unique_ptr.144"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.145"* %__ptr_) #8
  ret %"struct.std::__2::default_delete.148"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.145"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.145"* returned %this, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.145"*, align 4
  %__t1.addr = alloca %"class.draco::PointsSequencer"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.148"*, align 4
  store %"class.std::__2::__compressed_pair.145"* %this, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  store %"class.draco::PointsSequencer"** %__t1, %"class.draco::PointsSequencer"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.148"* %__t2, %"struct.std::__2::default_delete.148"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.145"*, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.145"* %this1 to %"struct.std::__2::__compressed_pair_elem.146"*
  %1 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.146"* @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.146"* %0, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.145"* %this1 to %"struct.std::__2::__compressed_pair_elem.147"*
  %3 = load %"struct.std::__2::default_delete.148"*, %"struct.std::__2::default_delete.148"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__27forwardINS_14default_deleteIN5draco15PointsSequencerEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.147"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.147"* %2, %"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.145"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointsSequencer"**, align 4
  store %"class.draco::PointsSequencer"** %__t, %"class.draco::PointsSequencer"*** %__t.addr, align 4
  %0 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__t.addr, align 4
  ret %"class.draco::PointsSequencer"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.146"* @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.146"* returned %this, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.146"*, align 4
  %__u.addr = alloca %"class.draco::PointsSequencer"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.146"* %this, %"struct.std::__2::__compressed_pair_elem.146"** %this.addr, align 4
  store %"class.draco::PointsSequencer"** %__u, %"class.draco::PointsSequencer"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.146"*, %"struct.std::__2::__compressed_pair_elem.146"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.146", %"struct.std::__2::__compressed_pair_elem.146"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  store %"class.draco::PointsSequencer"* %1, %"class.draco::PointsSequencer"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.146"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.147"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.147"* returned %this, %"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.147"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.148"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.147"* %this, %"struct.std::__2::__compressed_pair_elem.147"** %this.addr, align 4
  store %"struct.std::__2::default_delete.148"* %__u, %"struct.std::__2::default_delete.148"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.147"*, %"struct.std::__2::__compressed_pair_elem.147"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.147"* %this1 to %"struct.std::__2::default_delete.148"*
  %1 = load %"struct.std::__2::default_delete.148"*, %"struct.std::__2::default_delete.148"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.148"* @_ZNSt3__27forwardINS_14default_deleteIN5draco15PointsSequencerEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.148"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.147"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.125"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.125"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.126"* null, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.126"* null, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.131"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.131"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.131"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.131"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.132"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.132"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.132"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.133"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.133"* %2)
  ret %"class.std::__2::__compressed_pair.131"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.132"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.132"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.132"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.132"* %this, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.132"*, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.132", %"struct.std::__2::__compressed_pair_elem.132"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.std::__2::unique_ptr.126"* null, %"class.std::__2::unique_ptr.126"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.132"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.133"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.133"* %this1 to %"class.std::__2::allocator.134"*
  %call = call %"class.std::__2::allocator.134"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.134"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.133"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.134"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.134"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.134"*, align 4
  store %"class.std::__2::allocator.134"* %this, %"class.std::__2::allocator.134"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %this.addr, align 4
  ret %"class.std::__2::allocator.134"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.137"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.137"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.137"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.137"* %this, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.137"*, %"class.std::__2::__vector_base.137"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.137"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.138"* null, %"class.draco::IndexType.138"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.138"* null, %"class.draco::IndexType.138"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.137", %"class.std::__2::__vector_base.137"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.139"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.139"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.137"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.139"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.139"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.139"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.139"* %this, %"class.std::__2::__compressed_pair.139"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.139"*, %"class.std::__2::__compressed_pair.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.139"* %this1 to %"struct.std::__2::__compressed_pair_elem.140"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.140"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.140"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.139"* %this1 to %"struct.std::__2::__compressed_pair_elem.141"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.141"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.141"* %2)
  ret %"class.std::__2::__compressed_pair.139"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.140"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.140"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.140"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.140"* %this, %"struct.std::__2::__compressed_pair_elem.140"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.140"*, %"struct.std::__2::__compressed_pair_elem.140"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.140", %"struct.std::__2::__compressed_pair_elem.140"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType.138"* null, %"class.draco::IndexType.138"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.140"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.141"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.141"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.141"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.141"* %this, %"struct.std::__2::__compressed_pair_elem.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.141"*, %"struct.std::__2::__compressed_pair_elem.141"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.141"* %this1 to %"class.std::__2::allocator.142"*
  %call = call %"class.std::__2::allocator.142"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.142"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.141"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.142"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.142"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.142"*, align 4
  store %"class.std::__2::allocator.142"* %this, %"class.std::__2::allocator.142"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.142"*, %"class.std::__2::allocator.142"** %this.addr, align 4
  ret %"class.std::__2::allocator.142"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.124"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.134"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.125"* %0) #8
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.124"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %6) #8
  store %"class.std::__2::allocator.134"* %call2, %"class.std::__2::allocator.134"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.124"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this1) #8
  %8 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %__v, i32 %9)
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.124"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.124"* %this, %"class.std::__2::unique_ptr.126"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__new_last, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.124"* %this1, %"class.std::__2::unique_ptr.126"* %0)
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.125"* %1, %"class.std::__2::unique_ptr.126"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.124"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.124"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.124"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__new_end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.126"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_3, align 4
  %call4 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.126"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call2, %"class.std::__2::unique_ptr.126"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %5, i32 1
  store %"class.std::__2::unique_ptr.126"* %incdec.ptr, %"class.std::__2::unique_ptr.126"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.124"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.124"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.124"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.166"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.166"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.std::__2::unique_ptr.126"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.126"* %cond, %"class.std::__2::unique_ptr.126"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"class.std::__2::unique_ptr.126"* %add.ptr, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.126"* %add.ptr, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store %"class.std::__2::unique_ptr.126"* %add.ptr6, %"class.std::__2::unique_ptr.126"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %__tx, %"class.std::__2::unique_ptr.126"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_2, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.126"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_4, align 4
  %call5 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.126"* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call3, %"class.std::__2::unique_ptr.126"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %4, i32 1
  store %"class.std::__2::unique_ptr.126"* %incdec.ptr, %"class.std::__2::unique_ptr.126"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.124"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.124"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %0) #8
  %1 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.126"* %2, %"class.std::__2::unique_ptr.126"* %4, %"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__end_5, %"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.125"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %call7, %"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"class.std::__2::unique_ptr.126"* %13, %"class.std::__2::unique_ptr.126"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this1) #8
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.124"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.124"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__first_, align 4
  %tobool = icmp ne %"class.std::__2::unique_ptr.126"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.126"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.132"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %0) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.132"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.132"* %this, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.132"*, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.132", %"struct.std::__2::__compressed_pair_elem.132"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.126"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.124"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.124"* %__v, %"class.std::__2::vector.124"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %__v.addr, align 4
  store %"class.std::__2::vector.124"* %0, %"class.std::__2::vector.124"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.124"* %1 to %"class.std::__2::__vector_base.125"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  store %"class.std::__2::unique_ptr.126"* %3, %"class.std::__2::unique_ptr.126"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.124"* %4 to %"class.std::__2::__vector_base.125"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %5, i32 0, i32 1
  %6 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %6, i32 %7
  store %"class.std::__2::unique_ptr.126"* %add.ptr, %"class.std::__2::unique_ptr.126"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.126"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.124"* %1 to %"class.std::__2::__vector_base.125"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %2, i32 0, i32 1
  store %"class.std::__2::unique_ptr.126"* %0, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.134"* %1, %"class.std::__2::unique_ptr.126"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.134"* %this, %"class.std::__2::unique_ptr.126"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::allocator.134"* %this, %"class.std::__2::allocator.134"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.126"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.126"*
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.126"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.126"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %ref.tmp = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  store %"class.draco::SequentialAttributeDecoder"* null, %"class.draco::SequentialAttributeDecoder"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.127"* %__ptr_, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.125"*, align 4
  store %"class.std::__2::__vector_base.125"* %this, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.125"*, %"class.std::__2::__vector_base.125"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.131"* %__end_cap_) #8
  ret %"class.std::__2::allocator.134"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.134"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.134"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.134"*, align 4
  store %"class.std::__2::allocator.134"* %this, %"class.std::__2::allocator.134"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.133"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %0) #8
  ret %"class.std::__2::allocator.134"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.133"* %this1 to %"class.std::__2::allocator.134"*
  ret %"class.std::__2::allocator.134"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.166"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.166"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.166"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.134"*, align 4
  store %"class.std::__2::__compressed_pair.166"* %this, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.134"* %__t2, %"class.std::__2::allocator.134"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.166"*, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.166"* %this1 to %"struct.std::__2::__compressed_pair_elem.132"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.132"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.132"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.166"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.167"*
  %5 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.167"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.167"* %4, %"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.166"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.134"* %0, i32 %1, i8* null)
  ret %"class.std::__2::unique_ptr.126"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.166"* %__end_cap_) #8
  ret %"class.std::__2::allocator.134"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.166"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.134"*, align 4
  store %"class.std::__2::allocator.134"* %__t, %"class.std::__2::allocator.134"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__t.addr, align 4
  ret %"class.std::__2::allocator.134"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.167"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.167"* returned %this, %"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.167"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.134"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.167"* %this, %"struct.std::__2::__compressed_pair_elem.167"** %this.addr, align 4
  store %"class.std::__2::allocator.134"* %__u, %"class.std::__2::allocator.134"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.167"*, %"struct.std::__2::__compressed_pair_elem.167"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.167", %"struct.std::__2::__compressed_pair_elem.167"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.134"* %call, %"class.std::__2::allocator.134"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.167"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.134"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.134"* %this, %"class.std::__2::allocator.134"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.134"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.1, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::unique_ptr.126"*
  ret %"class.std::__2::unique_ptr.126"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #6 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.166"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.166"*, align 4
  store %"class.std::__2::__compressed_pair.166"* %this, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.166"*, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.166"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.167"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.167"* %1) #8
  ret %"class.std::__2::allocator.134"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.167"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.167"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.167"* %this, %"struct.std::__2::__compressed_pair_elem.167"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.167"*, %"struct.std::__2::__compressed_pair_elem.167"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.167", %"struct.std::__2::__compressed_pair_elem.167"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__value_, align 4
  ret %"class.std::__2::allocator.134"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.166"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.166"*, align 4
  store %"class.std::__2::__compressed_pair.166"* %this, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.166"*, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.166"* %this1 to %"struct.std::__2::__compressed_pair_elem.132"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %0) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* returned %this, %"class.std::__2::unique_ptr.126"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"** %__p, %"class.std::__2::unique_ptr.126"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__p.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %0, align 4
  store %"class.std::__2::unique_ptr.126"* %1, %"class.std::__2::unique_ptr.126"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %3, i32 %4
  store %"class.std::__2::unique_ptr.126"* %add.ptr, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.126"** %5, %"class.std::__2::unique_ptr.126"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__dest_, align 4
  store %"class.std::__2::unique_ptr.126"* %0, %"class.std::__2::unique_ptr.126"** %1, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>, std::__2::allocator<std::__2::unique_ptr<draco::SequentialAttributeDecoder, std::__2::default_delete<draco::SequentialAttributeDecoder>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__begin1, %"class.std::__2::unique_ptr.126"* %__end1, %"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__begin1.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__end1.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__end2.addr = alloca %"class.std::__2::unique_ptr.126"**, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__begin1, %"class.std::__2::unique_ptr.126"** %__begin1.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__end1, %"class.std::__2::unique_ptr.126"** %__end1.addr, align 4
  store %"class.std::__2::unique_ptr.126"** %__end2, %"class.std::__2::unique_ptr.126"*** %__end2.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end1.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin1.addr, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.126"* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__end2.addr, align 4
  %4 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %3, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %4, i32 -1
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.126"* %add.ptr) #8
  %5 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end1.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %5, i32 -1
  store %"class.std::__2::unique_ptr.126"* %incdec.ptr, %"class.std::__2::unique_ptr.126"** %__end1.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %2, %"class.std::__2::unique_ptr.126"* %call, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %call1)
  %6 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__end2.addr, align 4
  %7 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %6, align 4
  %incdec.ptr2 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %7, i32 -1
  store %"class.std::__2::unique_ptr.126"* %incdec.ptr2, %"class.std::__2::unique_ptr.126"** %6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::unique_ptr.126"**, align 4
  %__y.addr = alloca %"class.std::__2::unique_ptr.126"**, align 4
  %__t = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"** %__x, %"class.std::__2::unique_ptr.126"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.126"** %__y, %"class.std::__2::unique_ptr.126"*** %__y.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %call, align 4
  store %"class.std::__2::unique_ptr.126"* %1, %"class.std::__2::unique_ptr.126"** %__t, align 4
  %2 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %call1, align 4
  %4 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %3, %"class.std::__2::unique_ptr.126"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %call2, align 4
  %6 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__y.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %5, %"class.std::__2::unique_ptr.126"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.124"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.126"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.124"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.124"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call7, i32 %3
  %4 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.124"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.168", align 1
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__args, %"class.std::__2::unique_ptr.126"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.168"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.126"* %2, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %__t, %"class.std::__2::unique_ptr.126"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.126"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::allocator.134"* %__a, %"class.std::__2::allocator.134"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__args, %"class.std::__2::unique_ptr.126"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.134"* %1, %"class.std::__2::unique_ptr.126"* %2, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %__t, %"class.std::__2::unique_ptr.126"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.126"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.134"* %this, %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.134"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::allocator.134"* %this, %"class.std::__2::allocator.134"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__p, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__args, %"class.std::__2::unique_ptr.126"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.134"*, %"class.std::__2::allocator.134"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.126"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.126"*
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %3) #8
  %call2 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.126"* %2, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %call) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.126"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.126"* returned %this, %"class.std::__2::unique_ptr.126"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %ref.tmp = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__u, %"class.std::__2::unique_ptr.126"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__u.addr, align 4
  %call = call %"class.draco::SequentialAttributeDecoder"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.126"* %0) #8
  store %"class.draco::SequentialAttributeDecoder"* %call, %"class.draco::SequentialAttributeDecoder"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.126"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26SequentialAttributeDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.127"* %__ptr_, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::SequentialAttributeDecoder"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__t = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__ptr_) #8
  %0 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %call, align 4
  store %"class.draco::SequentialAttributeDecoder"* %0, %"class.draco::SequentialAttributeDecoder"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__ptr_2) #8
  store %"class.draco::SequentialAttributeDecoder"* null, %"class.draco::SequentialAttributeDecoder"** %call3, align 4
  %1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %__t, align 4
  ret %"class.draco::SequentialAttributeDecoder"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26SequentialAttributeDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.130"*, align 4
  store %"struct.std::__2::default_delete.130"* %__t, %"struct.std::__2::default_delete.130"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.130"*, %"struct.std::__2::default_delete.130"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.130"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.127"* %__ptr_) #8
  ret %"struct.std::__2::default_delete.130"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.127"* returned %this, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  %__t1.addr = alloca %"class.draco::SequentialAttributeDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.130"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"** %__t1, %"class.draco::SequentialAttributeDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.130"* %__t2, %"struct.std::__2::default_delete.130"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %1 = load %"class.draco::SequentialAttributeDecoder"**, %"class.draco::SequentialAttributeDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__27forwardIPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.128"* @_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.128"* %0, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.129"*
  %3 = load %"struct.std::__2::default_delete.130"*, %"struct.std::__2::default_delete.130"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26SequentialAttributeDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.129"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.129"* %2, %"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %0) #8
  ret %"class.draco::SequentialAttributeDecoder"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.128", %"struct.std::__2::__compressed_pair_elem.128"* %this1, i32 0, i32 0
  ret %"class.draco::SequentialAttributeDecoder"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.129"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.129"* %0) #8
  ret %"struct.std::__2::default_delete.130"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.129"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.129"* %this, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.129"*, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.129"* %this1 to %"struct.std::__2::default_delete.130"*
  ret %"struct.std::__2::default_delete.130"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.129"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.129"* returned %this, %"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.129"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.130"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.129"* %this, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  store %"struct.std::__2::default_delete.130"* %__u, %"struct.std::__2::default_delete.130"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.129"*, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.129"* %this1 to %"struct.std::__2::default_delete.130"*
  %1 = load %"struct.std::__2::default_delete.130"*, %"struct.std::__2::default_delete.130"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26SequentialAttributeDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.130"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.129"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.126"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.126"**, align 4
  store %"class.std::__2::unique_ptr.126"** %__t, %"class.std::__2::unique_ptr.126"*** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"**, %"class.std::__2::unique_ptr.126"*** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.126"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this1, %"class.std::__2::unique_ptr.126"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.126"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this, %"class.std::__2::unique_ptr.126"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.169", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__new_last, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"class.std::__2::unique_ptr.126"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"class.std::__2::unique_ptr.126"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.169", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__new_last, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.126"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.134"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %3, i32 -1
  store %"class.std::__2::unique_ptr.126"* %incdec.ptr, %"class.std::__2::unique_ptr.126"** %__end_2, align 4
  %call3 = call %"class.std::__2::unique_ptr.126"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.126"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.134"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.126"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.166"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.166"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.166"*, align 4
  store %"class.std::__2::__compressed_pair.166"* %this, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.166"*, %"class.std::__2::__compressed_pair.166"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.166"* %this1 to %"struct.std::__2::__compressed_pair_elem.132"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.126"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %0) #8
  ret %"class.std::__2::unique_ptr.126"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.124"* %this, %"class.std::__2::unique_ptr.126"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.126"* %__new_last, %"class.std::__2::unique_ptr.126"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.124"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.126"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.124"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call4, i32 %2
  %3 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr5 to i8*
  %call6 = call %"class.std::__2::unique_ptr.126"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.124"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.124"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %call6, i32 %call7
  %4 = bitcast %"class.std::__2::unique_ptr.126"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.124"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.126"* %this, %"class.draco::SequentialAttributeDecoder"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.126"*, align 4
  %__p.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  %__tmp = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.std::__2::unique_ptr.126"* %this, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"* %__p, %"class.draco::SequentialAttributeDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.126"*, %"class.std::__2::unique_ptr.126"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__ptr_) #8
  %0 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %call, align 4
  store %"class.draco::SequentialAttributeDecoder"* %0, %"class.draco::SequentialAttributeDecoder"** %__tmp, align 4
  %1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__ptr_2) #8
  store %"class.draco::SequentialAttributeDecoder"* %1, %"class.draco::SequentialAttributeDecoder"** %call3, align 4
  %2 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::SequentialAttributeDecoder"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.126", %"class.std::__2::unique_ptr.126"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.127"* %__ptr_4) #8
  %3 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco26SequentialAttributeDecoderEEclEPS2_(%"struct.std::__2::default_delete.130"* %call5, %"class.draco::SequentialAttributeDecoder"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco26SequentialAttributeDecoderEEclEPS2_(%"struct.std::__2::default_delete.130"* %this, %"class.draco::SequentialAttributeDecoder"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.130"*, align 4
  %__ptr.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"struct.std::__2::default_delete.130"* %this, %"struct.std::__2::default_delete.130"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"* %__ptr, %"class.draco::SequentialAttributeDecoder"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.130"*, %"struct.std::__2::default_delete.130"** %this.addr, align 4
  %0 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::SequentialAttributeDecoder"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::SequentialAttributeDecoder"* %0 to void (%"class.draco::SequentialAttributeDecoder"*)***
  %vtable = load void (%"class.draco::SequentialAttributeDecoder"*)**, void (%"class.draco::SequentialAttributeDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::SequentialAttributeDecoder"*)*, void (%"class.draco::SequentialAttributeDecoder"*)** %vtable, i64 1
  %2 = load void (%"class.draco::SequentialAttributeDecoder"*)*, void (%"class.draco::SequentialAttributeDecoder"*)** %vfn, align 4
  call void %2(%"class.draco::SequentialAttributeDecoder"* %0) #8
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNKSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.145"*, align 4
  store %"class.std::__2::__compressed_pair.145"* %this, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.145"*, %"class.std::__2::__compressed_pair.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.145"* %this1 to %"struct.std::__2::__compressed_pair_elem.146"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.146"* %0) #8
  ret %"class.draco::PointsSequencer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.146"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.146"* %this, %"struct.std::__2::__compressed_pair_elem.146"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.146"*, %"struct.std::__2::__compressed_pair_elem.146"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.146", %"struct.std::__2::__compressed_pair_elem.146"* %this1, i32 0, i32 0
  ret %"class.draco::PointsSequencer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Options"* @_ZNK5draco12DracoOptionsINS_17GeometryAttribute4TypeEE20FindAttributeOptionsERKS2_(%"class.draco::DracoOptions"* %this, i32* nonnull align 4 dereferenceable(4) %att_key) #0 comdat {
entry:
  %retval = alloca %"class.draco::Options"*, align 4
  %this.addr = alloca %"class.draco::DracoOptions"*, align 4
  %att_key.addr = alloca i32*, align 4
  %it = alloca %"class.std::__2::__map_const_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__map_const_iterator", align 4
  store %"class.draco::DracoOptions"* %this, %"class.draco::DracoOptions"** %this.addr, align 4
  store i32* %att_key, i32** %att_key.addr, align 4
  %this1 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %this.addr, align 4
  %attribute_options_ = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %att_key.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE4findERS9_(%"class.std::__2::map.113"* %attribute_options_, i32* nonnull align 4 dereferenceable(4) %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %it, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %attribute_options_3 = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 1
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE3endEv(%"class.std::__2::map.113"* %attribute_options_3) #8
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %ref.tmp, i32 0, i32 0
  %coerce.dive6 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive5, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEESF_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %it, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %"class.draco::Options"* null, %"class.draco::Options"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call8 = call %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEptEv(%"class.std::__2::__map_const_iterator"* %it)
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call8, i32 0, i32 1
  store %"class.draco::Options"* %second, %"class.draco::Options"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %1 = load %"class.draco::Options"*, %"class.draco::Options"** %retval, align 4
  ret %"class.draco::Options"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco7Options11IsOptionSetERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call i32 @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5countERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  %cmp = icmp ugt i32 %call, 0
  ret i1 %cmp
}

declare zeroext i1 @_ZNK5draco7Options7GetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"*, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12), i1 zeroext) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE4findERS9_(%"class.std::__2::map.113"* %this, i32* nonnull align 4 dereferenceable(4) %__k) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map.113"*, align 4
  %__k.addr = alloca i32*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::map.113"* %this, %"class.std::__2::map.113"** %this.addr, align 4
  store i32* %__k, i32** %__k.addr, align 4
  %this1 = load %"class.std::__2::map.113"*, %"class.std::__2::map.113"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map.113", %"class.std::__2::map.113"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__k.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4findIS4_EENS_21__tree_const_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_(%"class.std::__2::__tree.114"* %__tree_, i32* nonnull align 4 dereferenceable(4) %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_(%"class.std::__2::__map_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %1) #8
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEESF_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %__x, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__map_const_iterator"* %__y, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %__i_1 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %1, i32 0, i32 0
  %call = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_1)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE3endEv(%"class.std::__2::map.113"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map.113"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::map.113"* %this, %"class.std::__2::map.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::map.113"*, %"class.std::__2::map.113"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map.113", %"class.std::__2::map.113"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.114"* %__tree_) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_(%"class.std::__2::__map_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #8
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEptEv(%"class.std::__2::__map_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEptEv(%"class.std::__2::__tree_const_iterator"* %__i_)
  %call2 = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %call)
  %call3 = call %"struct.std::__2::pair"* @_ZNSt3__214pointer_traitsIPKNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS8_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call2) #8
  ret %"struct.std::__2::pair"* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4findIS4_EENS_21__tree_const_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_(%"class.std::__2::__tree.114"* %this, i32* nonnull align 4 dereferenceable(4) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree.114"*, align 4
  %__v.addr = alloca i32*, align 4
  %__p = alloca %"class.std::__2::__tree_const_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::__tree.114"* %this, %"class.std::__2::__tree.114"** %this.addr, align 4
  store i32* %__v, i32** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree.114"*, %"class.std::__2::__tree.114"** %this.addr, align 4
  %0 = load i32*, i32** %__v.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv(%"class.std::__2::__tree.114"* %this1) #8
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.114"* %this1) #8
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE13__lower_boundIS4_EENS_21__tree_const_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_SJ_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISH_EEEE(%"class.std::__2::__tree.114"* %this1, i32* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__tree_node"* %call, %"class.std::__2::__tree_end_node"* %call2)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__p, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.114"* %this1) #8
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  %call6 = call zeroext i1 @_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__p, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call7 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.121"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.114"* %this1) #8
  %1 = load i32*, i32** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %__p)
  %call9 = call zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS3_RKS6_(%"class.std::__2::__map_value_compare.121"* %call7, i32* nonnull align 4 dereferenceable(4) %1, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %call8)
  %lnot = xor i1 %call9, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  br i1 %2, label %if.then, label %if.end

if.then:                                          ; preds = %land.end
  %3 = bitcast %"class.std::__2::__tree_const_iterator"* %retval to i8*
  %4 = bitcast %"class.std::__2::__tree_const_iterator"* %__p to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %land.end
  %call10 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.114"* %this1) #8
  %coerce.dive11 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call10, %"class.std::__2::__tree_end_node"** %coerce.dive11, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive12 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive12, align 4
  ret %"class.std::__2::__tree_end_node"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_(%"class.std::__2::__map_const_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__i.coerce) unnamed_addr #0 comdat {
entry:
  %__i = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__i, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__i.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %0 = bitcast %"class.std::__2::__tree_const_iterator"* %__i_ to i8*
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %__i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  ret %"class.std::__2::__map_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE13__lower_boundIS4_EENS_21__tree_const_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_SJ_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISH_EEEE(%"class.std::__2::__tree.114"* %this, i32* nonnull align 4 dereferenceable(4) %__v, %"class.std::__2::__tree_node"* %__root, %"class.std::__2::__tree_end_node"* %__result) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree.114"*, align 4
  %__v.addr = alloca i32*, align 4
  %__root.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__result.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree.114"* %this, %"class.std::__2::__tree.114"** %this.addr, align 4
  store i32* %__v, i32** %__v.addr, align 4
  store %"class.std::__2::__tree_node"* %__root, %"class.std::__2::__tree_node"** %__root.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__result, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %this1 = load %"class.std::__2::__tree.114"*, %"class.std::__2::__tree.114"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.121"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.114"* %this1) #8
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %1, i32 0, i32 1
  %2 = load i32*, i32** %__v.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS6_RKS3_(%"class.std::__2::__map_value_compare.121"* %call, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_, i32* nonnull align 4 dereferenceable(4) %2)
  br i1 %call2, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %4 = bitcast %"class.std::__2::__tree_node"* %3 to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %4, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %5 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node"* %5 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %8, %"class.std::__2::__tree_node"** %__root.addr, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  %9 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %10 = bitcast %"class.std::__2::__tree_node"* %9 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %10, i32 0, i32 1
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %12 = bitcast %"class.std::__2::__tree_node_base"* %11 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %12, %"class.std::__2::__tree_node"** %__root.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %13 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %call3 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE(%"class.std::__2::__tree_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %13) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %14 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv(%"class.std::__2::__tree.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.114"*, align 4
  store %"class.std::__2::__tree.114"* %this, %"class.std::__2::__tree.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.114"*, %"class.std::__2::__tree.114"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.114"* %this1) #8
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.114"*, align 4
  store %"class.std::__2::__tree.114"* %this, %"class.std::__2::__tree.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.114"*, %"class.std::__2::__tree.114"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.114", %"class.std::__2::__tree.114"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.115"* %__pair1_) #8
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #8
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %__x, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_const_iterator"* %__y, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.114"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree.114"*, align 4
  store %"class.std::__2::__tree.114"* %this, %"class.std::__2::__tree.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.114"*, %"class.std::__2::__tree.114"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.114"* %this1) #8
  %call2 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE(%"class.std::__2::__tree_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %call) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.121"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.114"*, align 4
  store %"class.std::__2::__tree.114"* %this, %"class.std::__2::__tree.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.114"*, %"class.std::__2::__tree.114"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree.114", %"class.std::__2::__tree.114"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.121"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %__pair3_) #8
  ret %"class.std::__2::__map_value_compare.121"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS3_RKS6_(%"class.std::__2::__map_value_compare.121"* %this, i32* nonnull align 4 dereferenceable(4) %__x, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare.121"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"class.std::__2::__map_value_compare.121"* %this, %"class.std::__2::__map_value_compare.121"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store %"struct.std::__2::__value_type"* %__y, %"struct.std::__2::__value_type"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare.121"*, %"class.std::__2::__map_value_compare.121"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare.121"* %this1 to %"struct.std::__2::less.122"*
  %1 = load i32*, i32** %__x.addr, align 4
  %2 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__y.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %2)
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call, i32 0, i32 0
  %call2 = call zeroext i1 @_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_(%"struct.std::__2::less.122"* %0, i32* nonnull align 4 dereferenceable(4) %1, i32* nonnull align 4 dereferenceable(4) %first)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call, i32 0, i32 1
  ret %"struct.std::__2::__value_type"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS6_RKS3_(%"class.std::__2::__map_value_compare.121"* %this, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare.121"*, align 4
  %__x.addr = alloca %"struct.std::__2::__value_type"*, align 4
  %__y.addr = alloca i32*, align 4
  store %"class.std::__2::__map_value_compare.121"* %this, %"class.std::__2::__map_value_compare.121"** %this.addr, align 4
  store %"struct.std::__2::__value_type"* %__x, %"struct.std::__2::__value_type"** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare.121"*, %"class.std::__2::__map_value_compare.121"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare.121"* %this1 to %"struct.std::__2::less.122"*
  %1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %1)
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call, i32 0, i32 0
  %2 = load i32*, i32** %__y.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_(%"struct.std::__2::less.122"* %0, i32* nonnull align 4 dereferenceable(4) %first, i32* nonnull align 4 dereferenceable(4) %2)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE(%"class.std::__2::__tree_const_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__p, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  store %"class.std::__2::__tree_end_node"* %0, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_(%"struct.std::__2::less.122"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::less.122"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::less.122"* %this, %"struct.std::__2::less.122"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::less.122"*, %"struct.std::__2::less.122"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp slt i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %this, %"struct.std::__2::__value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type", %"struct.std::__2::__value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__r, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %0) #8
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.107"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %0) #8
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__x, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.107"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.107"* %this, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.107"*, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.107", %"struct.std::__2::__compressed_pair_elem.107"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %__x, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_const_iterator"* %__y, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %2 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %__ptr_1 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_1, align 4
  %cmp = icmp eq %"class.std::__2::__tree_end_node"* %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.121"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.121"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %0) #8
  ret %"class.std::__2::__map_value_compare.121"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.121"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"class.std::__2::__map_value_compare.121"*
  ret %"class.std::__2::__map_value_compare.121"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__214pointer_traitsIPKNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS8_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__r, %"struct.std::__2::pair"** %__r.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__r.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__29addressofIKNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS9_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %0) #8
  ret %"struct.std::__2::pair"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEptEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call, i32 0, i32 1
  %call2 = call %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPKNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS7_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_) #8
  ret %"struct.std::__2::__value_type"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofIKNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS9_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPKNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS7_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__r, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %call = call %"struct.std::__2::__value_type"* @_ZNSt3__29addressofIKNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %0) #8
  ret %"struct.std::__2::__value_type"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__29addressofIKNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__x, %"struct.std::__2::__value_type"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__x.addr, align 4
  ret %"struct.std::__2::__value_type"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5countERSA_(%"class.std::__2::map"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__k) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %__k.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__k, %"class.std::__2::basic_string"** %__k.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %call = call i32 @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE14__count_uniqueIS7_EEmRKT_(%"class.std::__2::__tree"* %__tree_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE14__count_uniqueIS7_EEmRKT_(%"class.std::__2::__tree"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__k) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__k.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__rt = alloca %"class.std::__2::__tree_node.170"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__k, %"class.std::__2::basic_string"** %__k.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node.170"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this1) #8
  store %"class.std::__2::__tree_node.170"* %call, %"class.std::__2::__tree_node.170"** %__rt, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end9, %entry
  %0 = load %"class.std::__2::__tree_node.170"*, %"class.std::__2::__tree_node.170"** %__rt, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node.170"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #8
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %2 = load %"class.std::__2::__tree_node.170"*, %"class.std::__2::__tree_node.170"** %__rt, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.170", %"class.std::__2::__tree_node.170"* %2, i32 0, i32 1
  %call3 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %call2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"struct.std::__2::__value_type.171"* nonnull align 4 dereferenceable(24) %__value_)
  br i1 %call3, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %3 = load %"class.std::__2::__tree_node.170"*, %"class.std::__2::__tree_node.170"** %__rt, align 4
  %4 = bitcast %"class.std::__2::__tree_node.170"* %3 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %4, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %6 = bitcast %"class.std::__2::__tree_node_base"* %5 to %"class.std::__2::__tree_node.170"*
  store %"class.std::__2::__tree_node.170"* %6, %"class.std::__2::__tree_node.170"** %__rt, align 4
  br label %if.end9

if.else:                                          ; preds = %while.body
  %call4 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #8
  %7 = load %"class.std::__2::__tree_node.170"*, %"class.std::__2::__tree_node.170"** %__rt, align 4
  %__value_5 = getelementptr inbounds %"class.std::__2::__tree_node.170", %"class.std::__2::__tree_node.170"* %7, i32 0, i32 1
  %8 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %call6 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %call4, %"struct.std::__2::__value_type.171"* nonnull align 4 dereferenceable(24) %__value_5, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %8)
  br i1 %call6, label %if.then7, label %if.else8

if.then7:                                         ; preds = %if.else
  %9 = load %"class.std::__2::__tree_node.170"*, %"class.std::__2::__tree_node.170"** %__rt, align 4
  %10 = bitcast %"class.std::__2::__tree_node.170"* %9 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %10, i32 0, i32 1
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %12 = bitcast %"class.std::__2::__tree_node_base"* %11 to %"class.std::__2::__tree_node.170"*
  store %"class.std::__2::__tree_node.170"* %12, %"class.std::__2::__tree_node.170"** %__rt, align 4
  br label %if.end

if.else8:                                         ; preds = %if.else
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then7
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.else8
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.170"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_node.170"*
  ret %"class.std::__2::__tree_node.170"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.111"* %__pair3_) #8
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__x, %"struct.std::__2::__value_type.171"* nonnull align 4 dereferenceable(24) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  %__x.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__y.addr = alloca %"struct.std::__2::__value_type.171"*, align 4
  store %"class.std::__2::__map_value_compare"* %this, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__x, %"class.std::__2::basic_string"** %__x.addr, align 4
  store %"struct.std::__2::__value_type.171"* %__y, %"struct.std::__2::__value_type.171"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare"* %this1 to %"struct.std::__2::less"*
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__x.addr, align 4
  %2 = load %"struct.std::__2::__value_type.171"*, %"struct.std::__2::__value_type.171"** %__y.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.172"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.171"* %2)
  %first = getelementptr inbounds %"struct.std::__2::pair.172", %"struct.std::__2::pair.172"* %call, i32 0, i32 0
  %call2 = call zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %this, %"struct.std::__2::__value_type.171"* nonnull align 4 dereferenceable(24) %__x, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  %__x.addr = alloca %"struct.std::__2::__value_type.171"*, align 4
  %__y.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::__map_value_compare"* %this, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  store %"struct.std::__2::__value_type.171"* %__x, %"struct.std::__2::__value_type.171"** %__x.addr, align 4
  store %"class.std::__2::basic_string"* %__y, %"class.std::__2::basic_string"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare"* %this1 to %"struct.std::__2::less"*
  %1 = load %"struct.std::__2::__value_type.171"*, %"struct.std::__2::__value_type.171"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.172"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.171"* %1)
  %first = getelementptr inbounds %"struct.std::__2::pair.172", %"struct.std::__2::pair.172"* %call, i32 0, i32 0
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__y.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %2)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.106"* %__pair1_) #8
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #8
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.106"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.106"*, align 4
  store %"class.std::__2::__compressed_pair.106"* %this, %"class.std::__2::__compressed_pair.106"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.106"*, %"class.std::__2::__compressed_pair.106"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.106"* %this1 to %"struct.std::__2::__compressed_pair_elem.107"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %0) #8
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.112"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %0) #8
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.112"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.112"* %this, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.112"*, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.112"* %this1 to %"class.std::__2::__map_value_compare"*
  ret %"class.std::__2::__map_value_compare"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__x, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::less"*, align 4
  %__x.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__y.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"struct.std::__2::less"* %this, %"struct.std::__2::less"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__x, %"class.std::__2::basic_string"** %__x.addr, align 4
  store %"class.std::__2::basic_string"* %__y, %"class.std::__2::basic_string"** %__y.addr, align 4
  %this1 = load %"struct.std::__2::less"*, %"struct.std::__2::less"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__x.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #8
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.172"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.171"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type.171"*, align 4
  store %"struct.std::__2::__value_type.171"* %this, %"struct.std::__2::__value_type.171"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type.171"*, %"struct.std::__2::__value_type.171"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type.171", %"struct.std::__2::__value_type.171"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.172"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__lhs, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__rhs) #0 comdat {
entry:
  %__lhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__rhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__lhs, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  store %"class.std::__2::basic_string"* %__rhs, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_(%"class.std::__2::basic_string"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #8
  %cmp = icmp slt i32 %call, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string_view", align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv(%"class.std::__2::basic_string_view"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %0) #8
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_(%"class.std::__2::basic_string"* %this1, %"class.std::__2::basic_string_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string_view"* nonnull align 4 dereferenceable(8) %__t) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__t.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  %__sv = alloca %"class.std::__2::basic_string_view", align 4
  %__lhs_sz = alloca i32, align 4
  %__rhs_sz = alloca i32, align 4
  %__result = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string_view"* %__t, %"class.std::__2::basic_string_view"** %__t.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %__t.addr, align 4
  %1 = bitcast %"class.std::__2::basic_string_view"* %__sv to i8*
  %2 = bitcast %"class.std::__2::basic_string_view"* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #8
  store i32 %call, i32* %__lhs_sz, align 4
  %call2 = call i32 @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv(%"class.std::__2::basic_string_view"* %__sv) #8
  store i32 %call2, i32* %__rhs_sz, align 4
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #8
  %call4 = call i8* @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv(%"class.std::__2::basic_string_view"* %__sv) #8
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__lhs_sz, i32* nonnull align 4 dereferenceable(4) %__rhs_sz)
  %3 = load i32, i32* %call5, align 4
  %call6 = call i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %call3, i8* %call4, i32 %3) #8
  store i32 %call6, i32* %__result, align 4
  %4 = load i32, i32* %__result, align 4
  %cmp = icmp ne i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__result, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %__lhs_sz, align 4
  %7 = load i32, i32* %__rhs_sz, align 4
  %cmp7 = icmp ult i32 %6, %7
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %8 = load i32, i32* %__lhs_sz, align 4
  %9 = load i32, i32* %__rhs_sz, align 4
  %cmp10 = icmp ugt i32 %8, %9
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  store i32 1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end9
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end12, %if.then11, %if.then8, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv(%"class.std::__2::basic_string_view"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #8
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #8
  %call3 = call %"class.std::__2::basic_string_view"* @_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm(%"class.std::__2::basic_string_view"* %agg.result, i8* %call, i32 %call2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #8
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv(%"class.std::__2::basic_string_view"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__size = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %__s1, i8* %__s2, i32 %__n) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %__s1.addr = alloca i8*, align 4
  %__s2.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store i8* %__s1, i8** %__s1.addr, align 4
  store i8* %__s2, i8** %__s2.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %__s1.addr, align 4
  %2 = load i8*, i8** %__s2.addr, align 4
  %3 = load i32, i32* %__n.addr, align 4
  %call = call i32 @memcmp(i8* %1, i8* %2, i32 %3) #8
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #8
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #8
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv(%"class.std::__2::basic_string_view"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__data = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__data, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.149"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.149"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.149"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.149"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.149"*, align 4
  store %"class.std::__2::__compressed_pair.149"* %this, %"class.std::__2::__compressed_pair.149"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.149"*, %"class.std::__2::__compressed_pair.149"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.149"* %this1 to %"struct.std::__2::__compressed_pair_elem.150"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.150"* %0) #8
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.150"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.150"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.150"* %this, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.150"*, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.150", %"struct.std::__2::__compressed_pair_elem.150"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: nounwind
declare i32 @memcmp(i8*, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #8
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.149"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.149"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #8
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #0 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string_view"* @_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm(%"class.std::__2::basic_string_view"* returned %this, i8* %__s, i32 %__len) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  %__s.addr = alloca i8*, align 4
  %__len.addr = alloca i32, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__len, i32* %__len.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__data = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__s.addr, align 4
  store i8* %0, i8** %__data, align 4
  %__size = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__len.addr, align 4
  store i32 %1, i32* %__size, align 4
  ret %"class.std::__2::basic_string_view"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.149"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.149"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.149"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.149"* %this, %"class.std::__2::__compressed_pair.149"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.149"*, %"class.std::__2::__compressed_pair.149"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.149"* %this1 to %"struct.std::__2::__compressed_pair_elem.150"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.150"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.150"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair.149"* %this1 to %"struct.std::__2::__compressed_pair_elem.151"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call5 = call %"struct.std::__2::__compressed_pair_elem.151"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.151"* %2)
  ret %"class.std::__2::__compressed_pair.149"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %__s) #0 comdat {
entry:
  %__s.addr = alloca i8*, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %call = call i32 @strlen(i8* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.150"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.150"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.150"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.150"* %this, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.150"*, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.150", %"struct.std::__2::__compressed_pair_elem.150"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem.150"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.151"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.151"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.151"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.151"* %this, %"struct.std::__2::__compressed_pair_elem.151"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.151"*, %"struct.std::__2::__compressed_pair_elem.151"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.151"* %this1 to %"class.std::__2::allocator.152"*
  %call = call %"class.std::__2::allocator.152"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.152"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.151"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.152"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.152"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.152"*, align 4
  store %"class.std::__2::allocator.152"* %this, %"class.std::__2::allocator.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.152"*, %"class.std::__2::allocator.152"** %this.addr, align 4
  ret %"class.std::__2::allocator.152"* %this1
}

; Function Attrs: nounwind
declare i32 @strlen(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPN5draco26SequentialAttributeDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.127"* returned %this, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  %__t1.addr = alloca %"class.draco::SequentialAttributeDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"** %__t1, %"class.draco::SequentialAttributeDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %1 = load %"class.draco::SequentialAttributeDecoder"**, %"class.draco::SequentialAttributeDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__27forwardIRPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.128"* @_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.128"* %0, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.129"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.129"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26SequentialAttributeDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.129"* %2)
  ret %"class.std::__2::__compressed_pair.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__27forwardIRPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::SequentialAttributeDecoder"**, align 4
  store %"class.draco::SequentialAttributeDecoder"** %__t, %"class.draco::SequentialAttributeDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::SequentialAttributeDecoder"**, %"class.draco::SequentialAttributeDecoder"*** %__t.addr, align 4
  ret %"class.draco::SequentialAttributeDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.128"* @_ZNSt3__222__compressed_pair_elemIPN5draco26SequentialAttributeDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.128"* returned %this, %"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  %__u.addr = alloca %"class.draco::SequentialAttributeDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  store %"class.draco::SequentialAttributeDecoder"** %__u, %"class.draco::SequentialAttributeDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.128", %"struct.std::__2::__compressed_pair_elem.128"* %this1, i32 0, i32 0
  %0 = load %"class.draco::SequentialAttributeDecoder"**, %"class.draco::SequentialAttributeDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::SequentialAttributeDecoder"** @_ZNSt3__27forwardIRPN5draco26SequentialAttributeDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::SequentialAttributeDecoder"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %call, align 4
  store %"class.draco::SequentialAttributeDecoder"* %1, %"class.draco::SequentialAttributeDecoder"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.128"* %this1
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn }
attributes #8 = { nounwind }
attributes #9 = { builtin allocsize(0) }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
