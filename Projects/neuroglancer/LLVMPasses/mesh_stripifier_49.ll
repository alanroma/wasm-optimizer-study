; ModuleID = './draco/src/draco/mesh/mesh_stripifier.cc'
source_filename = "./draco/src/draco/mesh/mesh_stripifier.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::IndexType.125" = type { i32 }
%"class.draco::IndexType.146" = type { i32 }
%"class.draco::IndexType.104" = type { i32 }
%"class.draco::MeshStripifier" = type { %"class.draco::Mesh"*, %"class.std::__2::unique_ptr.110", [3 x %"class.std::__2::vector.144"], [3 x %"class.draco::IndexType.125"], %"class.draco::IndexTypeVector.152", i32, i32, %"class.draco::IndexType.104" }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.94", %"class.draco::IndexTypeVector.101" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.96" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.96" = type { %"struct.std::__2::__compressed_pair_elem.97" }
%"struct.std::__2::__compressed_pair_elem.97" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.101" = type { %"class.std::__2::vector.102" }
%"class.std::__2::vector.102" = type { %"class.std::__2::__vector_base.103" }
%"class.std::__2::__vector_base.103" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.105" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.104"] }
%"class.std::__2::__compressed_pair.105" = type { %"struct.std::__2::__compressed_pair_elem.106" }
%"struct.std::__2::__compressed_pair_elem.106" = type { %"struct.std::__2::array"* }
%"class.std::__2::unique_ptr.110" = type { %"class.std::__2::__compressed_pair.111" }
%"class.std::__2::__compressed_pair.111" = type { %"struct.std::__2::__compressed_pair_elem.112" }
%"struct.std::__2::__compressed_pair_elem.112" = type { %"class.draco::CornerTable"* }
%"class.draco::CornerTable" = type { %"class.draco::IndexTypeVector.113", %"class.draco::IndexTypeVector.122", %"class.draco::IndexTypeVector.131", i32, i32, i32, %"class.draco::IndexTypeVector.132", %"class.draco::ValenceCache" }
%"class.draco::IndexTypeVector.113" = type { %"class.std::__2::vector.114" }
%"class.std::__2::vector.114" = type { %"class.std::__2::__vector_base.115" }
%"class.std::__2::__vector_base.115" = type { %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"*, %"class.std::__2::__compressed_pair.117" }
%"class.draco::IndexType.116" = type { i32 }
%"class.std::__2::__compressed_pair.117" = type { %"struct.std::__2::__compressed_pair_elem.118" }
%"struct.std::__2::__compressed_pair_elem.118" = type { %"class.draco::IndexType.116"* }
%"class.draco::IndexTypeVector.122" = type { %"class.std::__2::vector.123" }
%"class.std::__2::vector.123" = type { %"class.std::__2::__vector_base.124" }
%"class.std::__2::__vector_base.124" = type { %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"*, %"class.std::__2::__compressed_pair.126" }
%"class.std::__2::__compressed_pair.126" = type { %"struct.std::__2::__compressed_pair_elem.127" }
%"struct.std::__2::__compressed_pair_elem.127" = type { %"class.draco::IndexType.125"* }
%"class.draco::IndexTypeVector.131" = type { %"class.std::__2::vector.123" }
%"class.draco::IndexTypeVector.132" = type { %"class.std::__2::vector.114" }
%"class.draco::ValenceCache" = type { %"class.draco::CornerTable"*, %"class.draco::IndexTypeVector.133", %"class.draco::IndexTypeVector.141" }
%"class.draco::IndexTypeVector.133" = type { %"class.std::__2::vector.134" }
%"class.std::__2::vector.134" = type { %"class.std::__2::__vector_base.135" }
%"class.std::__2::__vector_base.135" = type { i8*, i8*, %"class.std::__2::__compressed_pair.136" }
%"class.std::__2::__compressed_pair.136" = type { %"struct.std::__2::__compressed_pair_elem.137" }
%"struct.std::__2::__compressed_pair_elem.137" = type { i8* }
%"class.draco::IndexTypeVector.141" = type { %"class.std::__2::vector.87" }
%"class.std::__2::vector.144" = type { %"class.std::__2::__vector_base.145" }
%"class.std::__2::__vector_base.145" = type { %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"*, %"class.std::__2::__compressed_pair.147" }
%"class.std::__2::__compressed_pair.147" = type { %"struct.std::__2::__compressed_pair_elem.148" }
%"struct.std::__2::__compressed_pair_elem.148" = type { %"class.draco::IndexType.146"* }
%"class.draco::IndexTypeVector.152" = type { %"class.std::__2::vector.153" }
%"class.std::__2::vector.153" = type { i32*, i32, %"class.std::__2::__compressed_pair.154" }
%"class.std::__2::__compressed_pair.154" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__bit_reference" = type { i32*, i32 }
%"class.std::__2::allocator.150" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.149" = type { i8 }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.144"*, %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"* }
%"struct.std::__2::__split_buffer" = type { %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"*, %"class.std::__2::__compressed_pair.158" }
%"class.std::__2::__compressed_pair.158" = type { %"struct.std::__2::__compressed_pair_elem.148", %"struct.std::__2::__compressed_pair_elem.159" }
%"struct.std::__2::__compressed_pair_elem.159" = type { %"class.std::__2::allocator.150"* }
%"struct.std::__2::__has_construct" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.160" = type { i8 }

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv = comdat any

$_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco14MeshStripifier17GetOppositeCornerENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_ = comdat any

$_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNK5draco11CornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEbEixERKS3_ = comdat any

$_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv = comdat any

$_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4backEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8pop_backEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco14MeshStripifier18CornerToPointIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEneERKS2_ = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEES3_EixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco4Mesh15CornerToPointIdENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco4Mesh15CornerToPointIdEi = comdat any

$_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE = comdat any

$_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEplERKj = comdat any

$_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej = comdat any

$_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEppEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco11CornerTableENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco11CornerTableELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm = comdat any

$_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

@_ZN5dracoL19kInvalidCornerIndexE = internal constant %"class.draco::IndexType.125" { i32 -1 }, align 4
@_ZN5dracoL17kInvalidFaceIndexE = internal constant %"class.draco::IndexType.146" { i32 -1 }, align 4
@_ZN5dracoL18kInvalidPointIndexE = internal constant %"class.draco::IndexType.104" { i32 -1 }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco14MeshStripifier24GenerateStripsFromCornerEiNS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this, i32 %local_strip_id, i32 %ci.coerce) #0 {
entry:
  %ci = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::MeshStripifier"*, align 4
  %local_strip_id.addr = alloca i32, align 4
  %start_ci = alloca %"class.draco::IndexType.125", align 4
  %fi = alloca %"class.draco::IndexType.146", align 4
  %agg.tmp = alloca %"class.draco::IndexType.125", align 4
  %pass = alloca i32, align 4
  %ref.tmp = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp6 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp9 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp18 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp21 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp26 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp29 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp37 = alloca %"class.draco::IndexType.146", align 4
  %agg.tmp40 = alloca %"class.draco::IndexType.125", align 4
  %num_added_faces = alloca i32, align 4
  %ref.tmp46 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp48 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp56 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp59 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp68 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp71 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp78 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp79 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp87 = alloca %"class.draco::IndexType.146", align 4
  %agg.tmp90 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp99 = alloca %"class.std::__2::__bit_reference", align 4
  %i = alloca i32, align 4
  %ref.tmp117 = alloca %"class.std::__2::__bit_reference", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ci, i32 0, i32 0
  store i32 %ci.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshStripifier"* %this, %"class.draco::MeshStripifier"** %this.addr, align 4
  store i32 %local_strip_id, i32* %local_strip_id.addr, align 4
  %this1 = load %"class.draco::MeshStripifier"*, %"class.draco::MeshStripifier"** %this.addr, align 4
  %strip_faces_ = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 2
  %0 = load i32, i32* %local_strip_id.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.std::__2::vector.144"], [3 x %"class.std::__2::vector.144"]* %strip_faces_, i32 0, i32 %0
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.144"* %arrayidx) #6
  %1 = bitcast %"class.draco::IndexType.125"* %start_ci to i8*
  %2 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %corner_table_ = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_) #6
  %3 = bitcast %"class.draco::IndexType.125"* %agg.tmp to i8*
  %4 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive2, align 4
  %call3 = call i32 @_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call, i32 %5)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %fi, i32 0, i32 0
  store i32 %call3, i32* %coerce.dive4, align 4
  store i32 0, i32* %pass, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %pass, align 4
  %cmp = icmp slt i32 %6, 2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32, i32* %pass, align 4
  %cmp5 = icmp eq i32 %7, 1
  br i1 %cmp5, label %if.then, label %if.end45

if.then:                                          ; preds = %for.body
  %corner_table_7 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call8 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_7) #6
  %8 = bitcast %"class.draco::IndexType.125"* %agg.tmp9 to i8*
  %9 = bitcast %"class.draco::IndexType.125"* %start_ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %coerce.dive10 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp9, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive10, align 4
  %call11 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call8, i32 %10)
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp6, i32 0, i32 0
  store i32 %call11, i32* %coerce.dive12, align 4
  %coerce.dive13 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp6, i32 0, i32 0
  %11 = load i32, i32* %coerce.dive13, align 4
  %call14 = call i32 @_ZNK5draco14MeshStripifier17GetOppositeCornerENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this1, i32 %11)
  %coerce.dive15 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ref.tmp, i32 0, i32 0
  store i32 %call14, i32* %coerce.dive15, align 4
  %call16 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %ref.tmp, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call16, label %if.then17, label %if.end

if.then17:                                        ; preds = %if.then
  br label %for.end

if.end:                                           ; preds = %if.then
  %corner_table_19 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call20 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_19) #6
  %12 = bitcast %"class.draco::IndexType.125"* %agg.tmp21 to i8*
  %13 = bitcast %"class.draco::IndexType.125"* %start_ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 4, i1 false)
  %coerce.dive22 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp21, i32 0, i32 0
  %14 = load i32, i32* %coerce.dive22, align 4
  %call23 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call20, i32 %14)
  %coerce.dive24 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ref.tmp18, i32 0, i32 0
  store i32 %call23, i32* %coerce.dive24, align 4
  %call25 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %corner_table_27 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call28 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_27) #6
  %15 = bitcast %"class.draco::IndexType.125"* %agg.tmp29 to i8*
  %16 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 4, i1 false)
  %coerce.dive30 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp29, i32 0, i32 0
  %17 = load i32, i32* %coerce.dive30, align 4
  %call31 = call i32 @_ZNK5draco11CornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call28, i32 %17)
  %coerce.dive32 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ref.tmp26, i32 0, i32 0
  store i32 %call31, i32* %coerce.dive32, align 4
  %call33 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %call34 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.end
  br label %for.end

if.end36:                                         ; preds = %if.end
  %corner_table_38 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call39 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_38) #6
  %18 = bitcast %"class.draco::IndexType.125"* %agg.tmp40 to i8*
  %19 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 4, i1 false)
  %coerce.dive41 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp40, i32 0, i32 0
  %20 = load i32, i32* %coerce.dive41, align 4
  %call42 = call i32 @_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call39, i32 %20)
  %coerce.dive43 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %ref.tmp37, i32 0, i32 0
  store i32 %call42, i32* %coerce.dive43, align 4
  %call44 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.146"* %fi, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %ref.tmp37)
  br label %if.end45

if.end45:                                         ; preds = %if.end36, %for.body
  store i32 0, i32* %num_added_faces, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end86, %if.end45
  %is_face_visited_ = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEbEixERKS3_(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp46, %"class.draco::IndexTypeVector.152"* %is_face_visited_, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %fi)
  %call47 = call zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %ref.tmp46) #6
  %lnot = xor i1 %call47, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %is_face_visited_49 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEbEixERKS3_(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp48, %"class.draco::IndexTypeVector.152"* %is_face_visited_49, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %fi)
  %call50 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp48, i1 zeroext true) #6
  %strip_faces_51 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 2
  %21 = load i32, i32* %local_strip_id.addr, align 4
  %arrayidx52 = getelementptr inbounds [3 x %"class.std::__2::vector.144"], [3 x %"class.std::__2::vector.144"]* %strip_faces_51, i32 0, i32 %21
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.144"* %arrayidx52, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %fi)
  %22 = load i32, i32* %num_added_faces, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %num_added_faces, align 4
  %23 = load i32, i32* %num_added_faces, align 4
  %cmp53 = icmp sgt i32 %23, 1
  br i1 %cmp53, label %if.then54, label %if.end77

if.then54:                                        ; preds = %while.body
  %24 = load i32, i32* %num_added_faces, align 4
  %and = and i32 %24, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then55, label %if.else

if.then55:                                        ; preds = %if.then54
  %corner_table_57 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call58 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_57) #6
  %25 = bitcast %"class.draco::IndexType.125"* %agg.tmp59 to i8*
  %26 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 4, i1 false)
  %coerce.dive60 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp59, i32 0, i32 0
  %27 = load i32, i32* %coerce.dive60, align 4
  %call61 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call58, i32 %27)
  %coerce.dive62 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ref.tmp56, i32 0, i32 0
  store i32 %call61, i32* %coerce.dive62, align 4
  %call63 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %ref.tmp56)
  br label %if.end76

if.else:                                          ; preds = %if.then54
  %28 = load i32, i32* %pass, align 4
  %cmp64 = icmp eq i32 %28, 1
  br i1 %cmp64, label %if.then65, label %if.end67

if.then65:                                        ; preds = %if.else
  %call66 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %start_ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %ci)
  br label %if.end67

if.end67:                                         ; preds = %if.then65, %if.else
  %corner_table_69 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call70 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_69) #6
  %29 = bitcast %"class.draco::IndexType.125"* %agg.tmp71 to i8*
  %30 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 4, i1 false)
  %coerce.dive72 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp71, i32 0, i32 0
  %31 = load i32, i32* %coerce.dive72, align 4
  %call73 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call70, i32 %31)
  %coerce.dive74 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ref.tmp68, i32 0, i32 0
  store i32 %call73, i32* %coerce.dive74, align 4
  %call75 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %ref.tmp68)
  br label %if.end76

if.end76:                                         ; preds = %if.end67, %if.then55
  br label %if.end77

if.end77:                                         ; preds = %if.end76, %while.body
  %32 = bitcast %"class.draco::IndexType.125"* %agg.tmp79 to i8*
  %33 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 4, i1 false)
  %coerce.dive80 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp79, i32 0, i32 0
  %34 = load i32, i32* %coerce.dive80, align 4
  %call81 = call i32 @_ZNK5draco14MeshStripifier17GetOppositeCornerENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this1, i32 %34)
  %coerce.dive82 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ref.tmp78, i32 0, i32 0
  store i32 %call81, i32* %coerce.dive82, align 4
  %call83 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %ref.tmp78)
  %call84 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %ci, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call84, label %if.then85, label %if.end86

if.then85:                                        ; preds = %if.end77
  br label %while.end

if.end86:                                         ; preds = %if.end77
  %corner_table_88 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call89 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_88) #6
  %35 = bitcast %"class.draco::IndexType.125"* %agg.tmp90 to i8*
  %36 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 4, i1 false)
  %coerce.dive91 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp90, i32 0, i32 0
  %37 = load i32, i32* %coerce.dive91, align 4
  %call92 = call i32 @_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call89, i32 %37)
  %coerce.dive93 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %ref.tmp87, i32 0, i32 0
  store i32 %call92, i32* %coerce.dive93, align 4
  %call94 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.146"* %fi, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %ref.tmp87)
  br label %while.cond

while.end:                                        ; preds = %if.then85, %while.cond
  %38 = load i32, i32* %pass, align 4
  %cmp95 = icmp eq i32 %38, 1
  br i1 %cmp95, label %land.lhs.true, label %if.end107

land.lhs.true:                                    ; preds = %while.end
  %39 = load i32, i32* %num_added_faces, align 4
  %and96 = and i32 %39, 1
  %tobool97 = icmp ne i32 %and96, 0
  br i1 %tobool97, label %if.then98, label %if.end107

if.then98:                                        ; preds = %land.lhs.true
  %is_face_visited_100 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 4
  %strip_faces_101 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 2
  %40 = load i32, i32* %local_strip_id.addr, align 4
  %arrayidx102 = getelementptr inbounds [3 x %"class.std::__2::vector.144"], [3 x %"class.std::__2::vector.144"]* %strip_faces_101, i32 0, i32 %40
  %call103 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4backEv(%"class.std::__2::vector.144"* %arrayidx102) #6
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEbEixERKS3_(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp99, %"class.draco::IndexTypeVector.152"* %is_face_visited_100, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %call103)
  %call104 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp99, i1 zeroext false) #6
  %strip_faces_105 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 2
  %41 = load i32, i32* %local_strip_id.addr, align 4
  %arrayidx106 = getelementptr inbounds [3 x %"class.std::__2::vector.144"], [3 x %"class.std::__2::vector.144"]* %strip_faces_105, i32 0, i32 %41
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8pop_backEv(%"class.std::__2::vector.144"* %arrayidx106)
  br label %if.end107

if.end107:                                        ; preds = %if.then98, %land.lhs.true, %while.end
  br label %for.inc

for.inc:                                          ; preds = %if.end107
  %42 = load i32, i32* %pass, align 4
  %inc108 = add nsw i32 %42, 1
  store i32 %inc108, i32* %pass, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then35, %if.then17, %for.cond
  %strip_start_corners_ = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 3
  %43 = load i32, i32* %local_strip_id.addr, align 4
  %arrayidx109 = getelementptr inbounds [3 x %"class.draco::IndexType.125"], [3 x %"class.draco::IndexType.125"]* %strip_start_corners_, i32 0, i32 %43
  %call110 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %arrayidx109, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %start_ci)
  store i32 0, i32* %i, align 4
  br label %for.cond111

for.cond111:                                      ; preds = %for.inc123, %for.end
  %44 = load i32, i32* %i, align 4
  %strip_faces_112 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 2
  %45 = load i32, i32* %local_strip_id.addr, align 4
  %arrayidx113 = getelementptr inbounds [3 x %"class.std::__2::vector.144"], [3 x %"class.std::__2::vector.144"]* %strip_faces_112, i32 0, i32 %45
  %call114 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %arrayidx113) #6
  %cmp115 = icmp ult i32 %44, %call114
  br i1 %cmp115, label %for.body116, label %for.end125

for.body116:                                      ; preds = %for.cond111
  %is_face_visited_118 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 4
  %strip_faces_119 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 2
  %46 = load i32, i32* %local_strip_id.addr, align 4
  %arrayidx120 = getelementptr inbounds [3 x %"class.std::__2::vector.144"], [3 x %"class.std::__2::vector.144"]* %strip_faces_119, i32 0, i32 %46
  %47 = load i32, i32* %i, align 4
  %call121 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.144"* %arrayidx120, i32 %47) #6
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEbEixERKS3_(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp117, %"class.draco::IndexTypeVector.152"* %is_face_visited_118, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %call121)
  %call122 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp117, i1 zeroext false) #6
  br label %for.inc123

for.inc123:                                       ; preds = %for.body116
  %48 = load i32, i32* %i, align 4
  %inc124 = add nsw i32 %48, 1
  store i32 %inc124, i32* %i, align 4
  br label %for.cond111

for.end125:                                       ; preds = %for.cond111
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this1) #6
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.145"* %0) #6
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.144"* %this1, i32 %1) #6
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.144"* %this1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.110"*, align 4
  store %"class.std::__2::unique_ptr.110"* %this, %"class.std::__2::unique_ptr.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.110"*, %"class.std::__2::unique_ptr.110"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.110", %"class.std::__2::unique_ptr.110"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::CornerTable"** @_ZNKSt3__217__compressed_pairIPN5draco11CornerTableENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.111"* %__ptr_) #6
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %call, align 4
  ret %"class.draco::CornerTable"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.146", align 4
  %corner = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %corner, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.146"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 bitcast (%"class.draco::IndexType.146"* @_ZN5dracoL17kInvalidFaceIndexE to i8*), i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.125"* %corner)
  %div = udiv i32 %call2, 3
  %call3 = call %"class.draco::IndexType.146"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.146"* %retval, i32 %div)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %retval, i32 0, i32 0
  %1 = load i32, i32* %coerce.dive4, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14MeshStripifier17GetOppositeCornerENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this, i32 %ci.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.125", align 4
  %ci = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::MeshStripifier"*, align 4
  %oci = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca %"class.draco::IndexType.104", align 4
  %agg.tmp7 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp10 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp17 = alloca %"class.draco::IndexType.104", align 4
  %agg.tmp18 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp21 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp31 = alloca %"class.draco::IndexType.104", align 4
  %agg.tmp32 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp35 = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp42 = alloca %"class.draco::IndexType.104", align 4
  %agg.tmp43 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp46 = alloca %"class.draco::IndexType.125", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ci, i32 0, i32 0
  store i32 %ci.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshStripifier"* %this, %"class.draco::MeshStripifier"** %this.addr, align 4
  %this1 = load %"class.draco::MeshStripifier"*, %"class.draco::MeshStripifier"** %this.addr, align 4
  %corner_table_ = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_) #6
  %0 = bitcast %"class.draco::IndexType.125"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call3 = call i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call, i32 %2)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %oci, i32 0, i32 0
  store i32 %call3, i32* %coerce.dive4, align 4
  store i32 0, i32* %ref.tmp, align 4
  %call5 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKj(%"class.draco::IndexType.125"* %oci, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call5, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 bitcast (%"class.draco::IndexType.125"* @_ZN5dracoL19kInvalidCornerIndexE to i8*), i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %corner_table_8 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call9 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_8) #6
  %4 = bitcast %"class.draco::IndexType.125"* %agg.tmp10 to i8*
  %5 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp10, i32 0, i32 0
  %6 = load i32, i32* %coerce.dive11, align 4
  %call12 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call9, i32 %6)
  %coerce.dive13 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp7, i32 0, i32 0
  store i32 %call12, i32* %coerce.dive13, align 4
  %coerce.dive14 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp7, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive14, align 4
  %call15 = call i32 @_ZNK5draco14MeshStripifier18CornerToPointIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this1, i32 %7)
  %coerce.dive16 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %ref.tmp6, i32 0, i32 0
  store i32 %call15, i32* %coerce.dive16, align 4
  %corner_table_19 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call20 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_19) #6
  %8 = bitcast %"class.draco::IndexType.125"* %agg.tmp21 to i8*
  %9 = bitcast %"class.draco::IndexType.125"* %oci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %coerce.dive22 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp21, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive22, align 4
  %call23 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call20, i32 %10)
  %coerce.dive24 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp18, i32 0, i32 0
  store i32 %call23, i32* %coerce.dive24, align 4
  %coerce.dive25 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp18, i32 0, i32 0
  %11 = load i32, i32* %coerce.dive25, align 4
  %call26 = call i32 @_ZNK5draco14MeshStripifier18CornerToPointIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this1, i32 %11)
  %coerce.dive27 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %ref.tmp17, i32 0, i32 0
  store i32 %call26, i32* %coerce.dive27, align 4
  %call28 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.104"* %ref.tmp6, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br i1 %call28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end
  %12 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 bitcast (%"class.draco::IndexType.125"* @_ZN5dracoL19kInvalidCornerIndexE to i8*), i32 4, i1 false)
  br label %return

if.end30:                                         ; preds = %if.end
  %corner_table_33 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call34 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_33) #6
  %13 = bitcast %"class.draco::IndexType.125"* %agg.tmp35 to i8*
  %14 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 4, i1 false)
  %coerce.dive36 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp35, i32 0, i32 0
  %15 = load i32, i32* %coerce.dive36, align 4
  %call37 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call34, i32 %15)
  %coerce.dive38 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp32, i32 0, i32 0
  store i32 %call37, i32* %coerce.dive38, align 4
  %coerce.dive39 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp32, i32 0, i32 0
  %16 = load i32, i32* %coerce.dive39, align 4
  %call40 = call i32 @_ZNK5draco14MeshStripifier18CornerToPointIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this1, i32 %16)
  %coerce.dive41 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %ref.tmp31, i32 0, i32 0
  store i32 %call40, i32* %coerce.dive41, align 4
  %corner_table_44 = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 1
  %call45 = call %"class.draco::CornerTable"* @_ZNKSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.110"* %corner_table_44) #6
  %17 = bitcast %"class.draco::IndexType.125"* %agg.tmp46 to i8*
  %18 = bitcast %"class.draco::IndexType.125"* %oci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 4, i1 false)
  %coerce.dive47 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp46, i32 0, i32 0
  %19 = load i32, i32* %coerce.dive47, align 4
  %call48 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %call45, i32 %19)
  %coerce.dive49 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp43, i32 0, i32 0
  store i32 %call48, i32* %coerce.dive49, align 4
  %coerce.dive50 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp43, i32 0, i32 0
  %20 = load i32, i32* %coerce.dive50, align 4
  %call51 = call i32 @_ZNK5draco14MeshStripifier18CornerToPointIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this1, i32 %20)
  %coerce.dive52 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %ref.tmp42, i32 0, i32 0
  store i32 %call51, i32* %coerce.dive52, align 4
  %call53 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.104"* %ref.tmp31, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %ref.tmp42)
  br i1 %call53, label %if.then54, label %if.end55

if.then54:                                        ; preds = %if.end30
  %21 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 bitcast (%"class.draco::IndexType.125"* @_ZN5dracoL19kInvalidCornerIndexE to i8*), i32 4, i1 false)
  br label %return

if.end55:                                         ; preds = %if.end30
  %22 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  %23 = bitcast %"class.draco::IndexType.125"* %oci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end55, %if.then54, %if.then29, %if.then
  %coerce.dive56 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  %24 = load i32, i32* %coerce.dive56, align 4
  ret i32 %24
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.125", align 4
  %corner = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %corner, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.125"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.draco::IndexType.125"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType.125"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive2, align 4
  %call3 = call i32 @_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %4)
  %tobool = icmp ne i32 %call3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  store i32 1, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj(%"class.draco::IndexType.125"* %corner, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  store i32 %call4, i32* %coerce.dive5, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  store i32 2, i32* %ref.tmp6, align 4
  %call7 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEplERKj(%"class.draco::IndexType.125"* %corner, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  store i32 %call7, i32* %coerce.dive8, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive9, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %i.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %i, %"class.draco::IndexType.125"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp eq i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.125", align 4
  %corner = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.125", align 4
  %ref.tmp = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %corner, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.125"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEppEv(%"class.draco::IndexType.125"* %corner)
  %2 = bitcast %"class.draco::IndexType.125"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType.125"* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive3, align 4
  %call4 = call i32 @_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %4)
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %5 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  %6 = bitcast %"class.draco::IndexType.125"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  br label %cond.end

cond.false:                                       ; preds = %if.end
  store i32 3, i32* %ref.tmp, align 4
  %call5 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj(%"class.draco::IndexType.125"* %corner, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  store i32 %call5, i32* %coerce.dive6, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive7, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %i.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %i, %"class.draco::IndexType.125"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.125", align 4
  %corner = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp2 = alloca %"class.draco::IndexType.125", align 4
  %agg.tmp3 = alloca %"class.draco::IndexType.125", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType.125"* %agg.tmp3 to i8*
  %1 = bitcast %"class.draco::IndexType.125"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp3, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive4, align 4
  %call = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %2)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp2, i32 0, i32 0
  store i32 %call, i32* %coerce.dive5, align 4
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp2, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive6, align 4
  %call7 = call i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %3)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp, i32 0, i32 0
  store i32 %call7, i32* %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive9, align 4
  %call10 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %4)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  store i32 %call10, i32* %coerce.dive11, align 4
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive12, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.146"* %this, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.146"*, align 4
  %i.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.draco::IndexType.146"* %this, %"class.draco::IndexType.146"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %i, %"class.draco::IndexType.146"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.146"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEbEixERKS3_(%"class.std::__2::__bit_reference"* noalias sret align 4 %agg.result, %"class.draco::IndexTypeVector.152"* %this, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.152"*, align 4
  %index.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.draco::IndexTypeVector.152"* %this, %"class.draco::IndexTypeVector.152"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %index, %"class.draco::IndexType.146"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.152"*, %"class.draco::IndexTypeVector.152"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.152", %"class.draco::IndexTypeVector.152"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.146"* %0)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %agg.result, %"class.std::__2::vector.153"* %vector_, i32 %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__seg_, align 4
  %1 = load i32, i32* %0, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %2 = load i32, i32* %__mask_, align 4
  %and = and i32 %1, %2
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %this, i1 zeroext %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  %__x.addr = alloca i8, align 1
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %frombool = zext i1 %__x to i8
  store i8 %frombool, i8* %__x.addr, align 1
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %0 = load i8, i8* %__x.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__mask_, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %2 = load i32*, i32** %__seg_, align 4
  %3 = load i32, i32* %2, align 4
  %or = or i32 %3, %1
  store i32 %or, i32* %2, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %__mask_2 = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %4 = load i32, i32* %__mask_2, align 4
  %neg = xor i32 %4, -1
  %__seg_3 = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %5 = load i32*, i32** %__seg_3, align 4
  %6 = load i32, i32* %5, align 4
  %and = and i32 %6, %neg
  store i32 %and, i32* %5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret %"class.std::__2::__bit_reference"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.144"* %this, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__x, %"class.draco::IndexType.146"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.145"* %2) #6
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %call, align 4
  %cmp = icmp ne %"class.draco::IndexType.146"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.144"* %this1, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.144"* %this1, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4backEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %1, i32 -1
  ret %"class.draco::IndexType.146"* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8pop_backEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %1, i32 -1
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.144"* %this1, %"class.draco::IndexType.146"* %add.ptr) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.146"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.146"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.144"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %1, i32 %2
  ret %"class.draco::IndexType.146"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.146"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.146"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.146"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.146"* %this, %"class.draco::IndexType.146"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.146"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.125", align 4
  %corner = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.125"* %corner, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.125"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %opposite_corners_ = getelementptr inbounds %"class.draco::CornerTable", %"class.draco::CornerTable"* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.122"* %opposite_corners_, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %corner)
  %2 = bitcast %"class.draco::IndexType.125"* %retval to i8*
  %3 = bitcast %"class.draco::IndexType.125"* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive3, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKj(%"class.draco::IndexType.125"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14MeshStripifier18CornerToPointIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshStripifier"* %this, i32 %ci.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.104", align 4
  %ci = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::MeshStripifier"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.125", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ci, i32 0, i32 0
  store i32 %ci.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshStripifier"* %this, %"class.draco::MeshStripifier"** %this.addr, align 4
  %this1 = load %"class.draco::MeshStripifier"*, %"class.draco::MeshStripifier"** %this.addr, align 4
  %mesh_ = getelementptr inbounds %"class.draco::MeshStripifier", %"class.draco::MeshStripifier"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh_, align 4
  %1 = bitcast %"class.draco::IndexType.125"* %agg.tmp to i8*
  %2 = bitcast %"class.draco::IndexType.125"* %ci to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %agg.tmp, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive2, align 4
  %call = call i32 @_ZNK5draco4Mesh15CornerToPointIdENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::Mesh"* %0, i32 %3)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %retval, i32 0, i32 0
  store i32 %call, i32* %coerce.dive3, align 4
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %retval, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive4, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  %i.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %i, %"class.draco::IndexType.104"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp ne i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.122"* %this, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.122"*, align 4
  %index.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexTypeVector.122"* %this, %"class.draco::IndexTypeVector.122"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %index, %"class.draco::IndexType.125"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.122"*, %"class.draco::IndexTypeVector.122"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.122", %"class.draco::IndexTypeVector.122"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.125"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.123"* %vector_, i32 %call) #6
  ret %"class.draco::IndexType.125"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.123"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %1, i32 %2
  ret %"class.draco::IndexType.125"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh15CornerToPointIdENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::Mesh"* %this, i32 %ci.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.104", align 4
  %ci = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %ci, i32 0, i32 0
  store i32 %ci.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.125"* %ci)
  %call2 = call i32 @_ZNK5draco4Mesh15CornerToPointIdEi(%"class.draco::Mesh"* %this1, i32 %call)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %retval, i32 0, i32 0
  store i32 %call2, i32* %coerce.dive3, align 4
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %retval, i32 0, i32 0
  %0 = load i32, i32* %coerce.dive4, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh15CornerToPointIdEi(%"class.draco::Mesh"* %this, i32 %ci) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.104", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %ci.addr = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType.146", align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %ci, i32* %ci.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %0 = load i32, i32* %ci.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.125"* @_ZN5dracoL19kInvalidCornerIndexE)
  %cmp = icmp eq i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.draco::IndexType.104"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast (%"class.draco::IndexType.104"* @_ZN5dracoL18kInvalidPointIndexE to i8*), i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %ci.addr, align 4
  %div = sdiv i32 %2, 3
  %call2 = call %"class.draco::IndexType.146"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.146"* %agg.tmp, i32 %div)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %agg.tmp, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  %call3 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this1, i32 %3)
  %4 = load i32, i32* %ci.addr, align 4
  %rem = srem i32 %4, 3
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %call3, i32 %rem) #6
  %5 = bitcast %"class.draco::IndexType.104"* %retval to i8*
  %6 = bitcast %"class.draco::IndexType.104"* %call4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %retval, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive5, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.146", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %faces_, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %face_id)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.104"], [3 x %"class.draco::IndexType.104"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.104"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  %index.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %index, %"class.draco::IndexType.146"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.146"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %vector_, i32 %call) #6
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.draco::IndexType.146"* %this, %"class.draco::IndexType.146"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %corner = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.125"* %corner)
  %rem = urem i32 %call, 3
  ret i32 %rem
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj(%"class.draco::IndexType.125"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %sub = sub i32 %0, %2
  %call = call %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej(%"class.draco::IndexType.125"* %retval, i32 %sub)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEplERKj(%"class.draco::IndexType.125"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.125", align 4
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %add = add i32 %0, %2
  %call = call %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej(%"class.draco::IndexType.125"* %retval, i32 %add)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej(%"class.draco::IndexType.125"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEppEv(%"class.draco::IndexType.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::CornerTable"** @_ZNKSt3__217__compressed_pairIPN5draco11CornerTableENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.112"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::CornerTable"** @_ZNKSt3__222__compressed_pair_elemIPN5draco11CornerTableELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %0) #6
  ret %"class.draco::CornerTable"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::CornerTable"** @_ZNKSt3__222__compressed_pair_elemIPN5draco11CornerTableELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.112"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.112"* %this, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.112"*, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.112", %"struct.std::__2::__compressed_pair_elem.112"* %this1, i32 0, i32 0
  ret %"class.draco::CornerTable"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.145"*, align 4
  store %"class.std::__2::__vector_base.145"* %this, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.145"*, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.145"* %this1, %"class.draco::IndexType.146"* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.144"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %call = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %0 = bitcast %"class.draco::IndexType.146"* %call to i8*
  %call2 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.144"* %this1) #6
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.146"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType.146"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this1) #6
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType.146"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.144"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.145"* %this, %"class.draco::IndexType.146"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.145"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.std::__2::__vector_base.145"* %this, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__new_last, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.145"*, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  store %"class.draco::IndexType.146"* %0, %"class.draco::IndexType.146"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType.146"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.145"* %this1) #6
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %3, i32 -1
  store %"class.draco::IndexType.146"* %incdec.ptr, %"class.draco::IndexType.146"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType.146"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.146"* %incdec.ptr) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.146"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.146"* %4, %"class.draco::IndexType.146"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.146"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.146"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.145"*, align 4
  store %"class.std::__2::__vector_base.145"* %this, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.145"*, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.147"* %__end_cap_) #6
  ret %"class.std::__2::allocator.150"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.146"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.146"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  ret %"class.draco::IndexType.146"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.146"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.150"* %1, %"class.draco::IndexType.146"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.150"* %this, %"class.draco::IndexType.146"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.std::__2::allocator.150"* %this, %"class.std::__2::allocator.150"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.147"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.147"*, align 4
  store %"class.std::__2::__compressed_pair.147"* %this, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.147"*, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.149"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %0) #6
  ret %"class.std::__2::allocator.150"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.149"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.149"* %this, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.149"*, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.149"* %this1 to %"class.std::__2::allocator.150"*
  ret %"class.std::__2::allocator.150"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.144"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_, align 4
  %call = call %"class.draco::IndexType.146"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.146"* %1) #6
  ret %"class.draco::IndexType.146"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.145"* %0) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.145"*, align 4
  store %"class.std::__2::__vector_base.145"* %this, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.145"*, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.145"* %this1) #6
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.146"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.146"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.145"*, align 4
  store %"class.std::__2::__vector_base.145"* %this, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.145"*, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.147"* %__end_cap_) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.147"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.147"*, align 4
  store %"class.std::__2::__compressed_pair.147"* %this, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.147"*, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %0) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.148", %"struct.std::__2::__compressed_pair_elem.148"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.146"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector.153"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.153"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.153"* %this, %"class.std::__2::vector.153"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.153"*, %"class.std::__2::vector.153"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_reference"* sret align 4 %agg.result, %"class.std::__2::vector.153"* %this1, i32 %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector.153"* %this, i32 %__pos) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.153"*, align 4
  %__pos.addr = alloca i32, align 4
  store %"class.std::__2::vector.153"* %this, %"class.std::__2::vector.153"** %this.addr, align 4
  store i32 %__pos, i32* %__pos.addr, align 4
  %this1 = load %"class.std::__2::vector.153"*, %"class.std::__2::vector.153"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.153", %"class.std::__2::vector.153"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %1 = load i32, i32* %__pos.addr, align 4
  %div = udiv i32 %1, 32
  %add.ptr = getelementptr inbounds i32, i32* %0, i32 %div
  %2 = load i32, i32* %__pos.addr, align 4
  %rem = urem i32 %2, 32
  %shl = shl i32 1, %rem
  %call = call %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm(%"class.std::__2::__bit_reference"* %agg.result, i32* %add.ptr, i32 %shl) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm(%"class.std::__2::__bit_reference"* returned %this, i32* %__s, i32 %__m) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  %__s.addr = alloca i32*, align 4
  %__m.addr = alloca i32, align 4
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  store i32* %__s, i32** %__s.addr, align 4
  store i32 %__m, i32* %__m.addr, align 4
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__s.addr, align 4
  store i32* %0, i32** %__seg_, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__m.addr, align 4
  store i32 %1, i32* %__mask_, align 4
  ret %"class.std::__2::__bit_reference"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.145"*, align 4
  store %"class.std::__2::__vector_base.145"* %this, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.145"*, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.147"* %__end_cap_) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.144"* %this, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__args, %"class.draco::IndexType.146"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.144"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.145"* %0) #6
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__pos_, align 4
  %call3 = call %"class.draco::IndexType.146"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.146"* %1) #6
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %2) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.146"* %call3, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %3, i32 1
  store %"class.draco::IndexType.146"* %incdec.ptr, %"class.draco::IndexType.146"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.144"* %this, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__a = alloca %"class.std::__2::allocator.150"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__x, %"class.draco::IndexType.146"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.145"* %0) #6
  store %"class.std::__2::allocator.150"* %call, %"class.std::__2::allocator.150"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this1) #6
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.144"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this1) #6
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  %call6 = call %"class.draco::IndexType.146"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.146"* %3) #6
  %4 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %4) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %2, %"class.draco::IndexType.146"* %call6, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %5 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %5, i32 1
  store %"class.draco::IndexType.146"* %incdec.ptr, %"class.draco::IndexType.146"** %__end_8, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.144"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.147"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.147"*, align 4
  store %"class.std::__2::__compressed_pair.147"* %this, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.147"*, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %0) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.148", %"struct.std::__2::__compressed_pair_elem.148"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.146"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.144"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.144"* %__v, %"class.std::__2::vector.144"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %__v.addr, align 4
  store %"class.std::__2::vector.144"* %0, %"class.std::__2::vector.144"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.144"* %1 to %"class.std::__2::__vector_base.145"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  store %"class.draco::IndexType.146"* %3, %"class.draco::IndexType.146"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.144"* %4 to %"class.std::__2::__vector_base.145"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %6, i32 %7
  store %"class.draco::IndexType.146"* %add.ptr, %"class.draco::IndexType.146"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.146"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  store %"class.draco::IndexType.146"* %__args, %"class.draco::IndexType.146"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %3) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.146"* %2, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.draco::IndexType.146"* %__t, %"class.draco::IndexType.146"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__t.addr, align 4
  ret %"class.draco::IndexType.146"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.144"* %1 to %"class.std::__2::__vector_base.145"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %2, i32 0, i32 1
  store %"class.draco::IndexType.146"* %0, %"class.draco::IndexType.146"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  store %"class.draco::IndexType.146"* %__args, %"class.draco::IndexType.146"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %3) #6
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.150"* %1, %"class.draco::IndexType.146"* %2, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.150"* %this, %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.std::__2::allocator.150"* %this, %"class.std::__2::allocator.150"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  store %"class.draco::IndexType.146"* %__args, %"class.draco::IndexType.146"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.146"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.146"*
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.146"* nonnull align 4 dereferenceable(4) %3) #6
  %4 = bitcast %"class.draco::IndexType.146"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType.146"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.144"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.144"* %this1) #6
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #7
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.144"* %this1) #6
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.158"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType.146"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType.146"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.146"* %cond, %"class.draco::IndexType.146"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"class.draco::IndexType.146"* %add.ptr, %"class.draco::IndexType.146"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.146"* %add.ptr, %"class.draco::IndexType.146"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #6
  store %"class.draco::IndexType.146"* %add.ptr6, %"class.draco::IndexType.146"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.144"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.144"* %this1) #6
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.145"* %0) #6
  %1 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.146"* %2, %"class.draco::IndexType.146"* %4, %"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__begin_4) #6
  %8 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__end_6) #6
  %10 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.145"* %10) #6
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #6
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %call8) #6
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"class.draco::IndexType.146"* %13, %"class.draco::IndexType.146"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this1) #6
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.144"* %this1, i32 %call10) #6
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.144"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType.146"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.146"* %1, i32 %call3) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.145"* %0) #6
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call) #6
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #6
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.145"*, align 4
  store %"class.std::__2::__vector_base.145"* %this, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.145"*, %"class.std::__2::__vector_base.145"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.145", %"class.std::__2::__vector_base.145"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.147"* %__end_cap_) #6
  ret %"class.std::__2::allocator.150"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.150"* %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.150"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.150"*, align 4
  store %"class.std::__2::allocator.150"* %this, %"class.std::__2::allocator.150"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.147"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.147"*, align 4
  store %"class.std::__2::__compressed_pair.147"* %this, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.147"*, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.149"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %0) #6
  ret %"class.std::__2::allocator.150"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.149"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.149"* %this, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.149"*, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.149"* %this1 to %"class.std::__2::allocator.150"*
  ret %"class.std::__2::allocator.150"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.158"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.150"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.150"* %__t2, %"class.std::__2::allocator.150"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.148"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.159"*
  %5 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %5) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.159"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.159"* %4, %"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.158"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.146"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType.146"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.150"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType.146"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.158"* %__end_cap_) #6
  ret %"class.std::__2::allocator.150"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %__end_cap_) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.148"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.148", %"struct.std::__2::__compressed_pair_elem.148"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #6
  store %"class.draco::IndexType.146"* null, %"class.draco::IndexType.146"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.148"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.150"*, align 4
  store %"class.std::__2::allocator.150"* %__t, %"class.std::__2::allocator.150"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__t.addr, align 4
  ret %"class.std::__2::allocator.150"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.159"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.159"* returned %this, %"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.159"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.150"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.159"* %this, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  store %"class.std::__2::allocator.150"* %__u, %"class.std::__2::allocator.150"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.159"*, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.159", %"struct.std::__2::__compressed_pair_elem.159"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %0) #6
  store %"class.std::__2::allocator.150"* %call, %"class.std::__2::allocator.150"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.159"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.146"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.150"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.150"* %this, %"class.std::__2::allocator.150"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.150"* %this1) #6
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #7
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType.146"*
  ret %"class.draco::IndexType.146"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #3 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #7
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #8
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #2

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.158"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.159"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.159"* %1) #6
  ret %"class.std::__2::allocator.150"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.159"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.159"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.159"* %this, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.159"*, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.159", %"struct.std::__2::__compressed_pair_elem.159"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__value_, align 4
  ret %"class.std::__2::allocator.150"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %0) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %call = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %0 = bitcast %"class.draco::IndexType.146"* %call to i8*
  %call2 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.144"* %this1) #6
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.146"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this1) #6
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.146"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.144"* %this1) #6
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.146"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.144"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType.146"* %__begin1, %"class.draco::IndexType.146"* %__end1, %"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType.146"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.150"* %0, %"class.std::__2::allocator.150"** %.addr, align 4
  store %"class.draco::IndexType.146"* %__begin1, %"class.draco::IndexType.146"** %__begin1.addr, align 4
  store %"class.draco::IndexType.146"* %__end1, %"class.draco::IndexType.146"** %__end1.addr, align 4
  store %"class.draco::IndexType.146"** %__end2, %"class.draco::IndexType.146"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.146"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.146"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType.146"**, %"class.draco::IndexType.146"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %5, i32 %idx.neg
  store %"class.draco::IndexType.146"* %add.ptr, %"class.draco::IndexType.146"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType.146"**, %"class.draco::IndexType.146"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %7, align 4
  %9 = bitcast %"class.draco::IndexType.146"* %8 to i8*
  %10 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType.146"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType.146"**, align 4
  %__y.addr = alloca %"class.draco::IndexType.146"**, align 4
  %__t = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.draco::IndexType.146"** %__x, %"class.draco::IndexType.146"*** %__x.addr, align 4
  store %"class.draco::IndexType.146"** %__y, %"class.draco::IndexType.146"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType.146"**, %"class.draco::IndexType.146"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %0) #6
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %call, align 4
  store %"class.draco::IndexType.146"* %1, %"class.draco::IndexType.146"** %__t, align 4
  %2 = load %"class.draco::IndexType.146"**, %"class.draco::IndexType.146"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %2) #6
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %call1, align 4
  %4 = load %"class.draco::IndexType.146"**, %"class.draco::IndexType.146"*** %__x.addr, align 4
  store %"class.draco::IndexType.146"* %3, %"class.draco::IndexType.146"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__t) #6
  %5 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %call2, align 4
  %6 = load %"class.draco::IndexType.146"**, %"class.draco::IndexType.146"*** %__y.addr, align 4
  store %"class.draco::IndexType.146"* %5, %"class.draco::IndexType.146"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.144"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %call = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %0 = bitcast %"class.draco::IndexType.146"* %call to i8*
  %call2 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.144"* %this1) #6
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.146"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.144"* %this1) #6
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.146"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.146"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.144"* %this1) #6
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType.146"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.144"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.146"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.146"**, align 4
  store %"class.draco::IndexType.146"** %__t, %"class.draco::IndexType.146"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.146"**, %"class.draco::IndexType.146"*** %__t.addr, align 4
  ret %"class.draco::IndexType.146"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType.146"* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.146"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.150"* %__a, %"class.std::__2::allocator.150"** %__a.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.150"* %0, %"class.draco::IndexType.146"* %1, i32 %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.146"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.146"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType.146"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.146"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.160", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__new_last, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType.146"* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType.146"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.160", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__new_last, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.146"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.150"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.146", %"class.draco::IndexType.146"* %3, i32 -1
  store %"class.draco::IndexType.146"* %incdec.ptr, %"class.draco::IndexType.146"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType.146"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.146"* %incdec.ptr) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.150"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.146"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.150"* %this, %"class.draco::IndexType.146"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.150"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.150"* %this, %"class.std::__2::allocator.150"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__p, %"class.draco::IndexType.146"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.150"*, %"class.std::__2::allocator.150"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.146"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %__end_cap_) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.146"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %0) #6
  ret %"class.draco::IndexType.146"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.144"* %this, %"class.draco::IndexType.146"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.146"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__new_last, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.144"* %this1, %"class.draco::IndexType.146"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.144"* %this1) #6
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.144"* %this1 to %"class.std::__2::__vector_base.145"*
  %2 = load %"class.draco::IndexType.146"*, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.145"* %1, %"class.draco::IndexType.146"* %2) #6
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.144"* %this1, i32 %3) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.144"* %this, %"class.draco::IndexType.146"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.144"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.146"*, align 4
  store %"class.std::__2::vector.144"* %this, %"class.std::__2::vector.144"** %this.addr, align 4
  store %"class.draco::IndexType.146"* %__new_last, %"class.draco::IndexType.146"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.144"*, %"class.std::__2::vector.144"** %this.addr, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { noreturn }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
