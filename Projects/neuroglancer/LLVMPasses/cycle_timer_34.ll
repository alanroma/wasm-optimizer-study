; ModuleID = './draco/src/draco/core/cycle_timer.cc'
source_filename = "./draco/src/draco/core/cycle_timer.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::DracoTimer" = type { %struct.timeval, %struct.timeval }
%struct.timeval = type { i32, i32 }

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco10DracoTimer5StartEv(%"class.draco::DracoTimer"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::DracoTimer"*, align 4
  store %"class.draco::DracoTimer"* %this, %"class.draco::DracoTimer"** %this.addr, align 4
  %this1 = load %"class.draco::DracoTimer"*, %"class.draco::DracoTimer"** %this.addr, align 4
  %tv_start = getelementptr inbounds %"class.draco::DracoTimer", %"class.draco::DracoTimer"* %this1, i32 0, i32 0
  %call = call i32 @gettimeofday(%struct.timeval* %tv_start, i8* null)
  ret void
}

declare i32 @gettimeofday(%struct.timeval*, i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco10DracoTimer4StopEv(%"class.draco::DracoTimer"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::DracoTimer"*, align 4
  store %"class.draco::DracoTimer"* %this, %"class.draco::DracoTimer"** %this.addr, align 4
  %this1 = load %"class.draco::DracoTimer"*, %"class.draco::DracoTimer"** %this.addr, align 4
  %tv_end = getelementptr inbounds %"class.draco::DracoTimer", %"class.draco::DracoTimer"* %this1, i32 0, i32 1
  %call = call i32 @gettimeofday(%struct.timeval* %tv_end, i8* null)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i64 @_ZN5draco10DracoTimer7GetInMsEv(%"class.draco::DracoTimer"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::DracoTimer"*, align 4
  %seconds = alloca i64, align 8
  %milliseconds = alloca i64, align 8
  store %"class.draco::DracoTimer"* %this, %"class.draco::DracoTimer"** %this.addr, align 4
  %this1 = load %"class.draco::DracoTimer"*, %"class.draco::DracoTimer"** %this.addr, align 4
  %tv_end = getelementptr inbounds %"class.draco::DracoTimer", %"class.draco::DracoTimer"* %this1, i32 0, i32 1
  %tv_sec = getelementptr inbounds %struct.timeval, %struct.timeval* %tv_end, i32 0, i32 0
  %0 = load i32, i32* %tv_sec, align 4
  %tv_start = getelementptr inbounds %"class.draco::DracoTimer", %"class.draco::DracoTimer"* %this1, i32 0, i32 0
  %tv_sec2 = getelementptr inbounds %struct.timeval, %struct.timeval* %tv_start, i32 0, i32 0
  %1 = load i32, i32* %tv_sec2, align 4
  %sub = sub nsw i32 %0, %1
  %mul = mul nsw i32 %sub, 1000
  %conv = sext i32 %mul to i64
  store i64 %conv, i64* %seconds, align 8
  %tv_end3 = getelementptr inbounds %"class.draco::DracoTimer", %"class.draco::DracoTimer"* %this1, i32 0, i32 1
  %tv_usec = getelementptr inbounds %struct.timeval, %struct.timeval* %tv_end3, i32 0, i32 1
  %2 = load i32, i32* %tv_usec, align 4
  %tv_start4 = getelementptr inbounds %"class.draco::DracoTimer", %"class.draco::DracoTimer"* %this1, i32 0, i32 0
  %tv_usec5 = getelementptr inbounds %struct.timeval, %struct.timeval* %tv_start4, i32 0, i32 1
  %3 = load i32, i32* %tv_usec5, align 4
  %sub6 = sub nsw i32 %2, %3
  %div = sdiv i32 %sub6, 1000
  %conv7 = sext i32 %div to i64
  store i64 %conv7, i64* %milliseconds, align 8
  %4 = load i64, i64* %seconds, align 8
  %5 = load i64, i64* %milliseconds, align 8
  %add = add nsw i64 %4, %5
  ret i64 %add
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
