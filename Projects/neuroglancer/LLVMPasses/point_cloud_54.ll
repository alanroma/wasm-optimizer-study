; ModuleID = './draco/src/draco/point_cloud/point_cloud.cc'
source_filename = "./draco/src/draco/point_cloud/point_cloud.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::IndexType" = type { i32 }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete.81" = type { i8 }
%"class.std::__2::__wrap_iter" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::__wrap_iter.94" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::__wrap_iter.95" = type { i32* }
%"class.std::__2::__wrap_iter.96" = type { i32* }
%"class.std::__2::__wrap_iter.100" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::__wrap_iter.101" = type { %"class.std::__2::unique_ptr.40"* }
%"class.draco::BoundingBox" = type { %"class.draco::VectorD", %"class.draco::VectorD" }
%"class.draco::VectorD" = type { %"struct.std::__2::array" }
%"struct.std::__2::array" = type { [3 x float] }
%"struct.std::__2::__compressed_pair_elem.49" = type { i8 }
%"struct.std::__2::default_delete.50" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.80" = type { i8 }
%"class.std::__2::allocator.73" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.std::__2::allocator.73"* }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.68"*, %"class.draco::IndexType"*, %"class.draco::IndexType"* }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.draco::IndexType"** }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.72" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.99" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::allocator.47" = type { i8 }
%"struct.std::__2::__has_destroy.102" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.43" = type { i8 }
%"class.std::__2::allocator.32" = type { i8 }
%"struct.std::__2::__hash_node" = type { %"struct.std::__2::__hash_node_base.22", i32, %"struct.std::__2::__hash_value_type" }
%"struct.std::__2::__hash_value_type" = type { %"struct.std::__2::pair" }
%"struct.std::__2::pair" = type { %"class.std::__2::basic_string", %"class.std::__2::unique_ptr.108" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.103" }
%"class.std::__2::__compressed_pair.103" = type { %"struct.std::__2::__compressed_pair_elem.104" }
%"struct.std::__2::__compressed_pair_elem.104" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.std::__2::unique_ptr.108" = type { %"class.std::__2::__compressed_pair.109" }
%"class.std::__2::__compressed_pair.109" = type { %"struct.std::__2::__compressed_pair_elem.110" }
%"struct.std::__2::__compressed_pair_elem.110" = type { %"class.draco::Metadata"* }
%"struct.std::__2::__has_destroy.113" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.31" = type { i8 }
%"struct.std::__2::default_delete.112" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.111" = type { i8 }
%"class.std::__2::allocator.27" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.26" = type { i8 }
%"class.std::__2::allocator.10" = type { i8 }
%"struct.std::__2::__hash_node.114" = type { %"struct.std::__2::__hash_node_base", i32, %"struct.std::__2::__hash_value_type.115" }
%"struct.std::__2::__hash_value_type.115" = type { %"struct.std::__2::pair.116" }
%"struct.std::__2::pair.116" = type { %"class.std::__2::basic_string", %"class.draco::EntryValue" }
%"class.draco::EntryValue" = type { %"class.std::__2::vector.56" }
%"struct.std::__2::__has_destroy.117" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.9" = type { i8 }
%"class.std::__2::allocator.61" = type { i8 }
%"struct.std::__2::__has_destroy.118" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.60" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.6" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.46" = type { i8 }
%"class.std::__2::allocator.92" = type { i8 }
%"struct.std::__2::__has_destroy.119" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.91" = type { i8 }
%"class.std::__2::allocator.85" = type { i8 }
%"struct.std::__2::__has_destroy.120" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.84" = type { i8 }
%"struct.std::__2::default_delete.79" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.78" = type { i8 }
%"struct.std::__2::default_delete.67" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.66" = type { i8 }
%"struct.std::__2::__split_buffer.121" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.122" }
%"class.std::__2::__compressed_pair.122" = type { %"struct.std::__2::__compressed_pair_elem.83", %"struct.std::__2::__compressed_pair_elem.123" }
%"struct.std::__2::__compressed_pair_elem.123" = type { %"class.std::__2::allocator.85"* }
%"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction" = type { %"class.std::__2::vector.51"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"* }
%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** }
%"struct.std::__2::__has_construct.124" = type { i8 }
%"struct.std::__2::__has_max_size.125" = type { i8 }
%"struct.std::__2::__has_construct.126" = type { i8 }
%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction" = type { %"class.std::__2::vector.87"*, i32*, i32* }
%"struct.std::__2::__split_buffer.128" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.129" }
%"class.std::__2::__compressed_pair.129" = type { %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.130" }
%"struct.std::__2::__compressed_pair_elem.130" = type { %"class.std::__2::allocator.92"* }
%"struct.std::__2::__has_construct.127" = type { i8 }
%"struct.std::__2::__has_max_size.131" = type { i8 }

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv = comdat any

$_ZNK5draco17GeometryAttribute9unique_idEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEcvbEv = comdat any

$_ZNK5draco17GeometryAttribute14attribute_typeEv = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEDn = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZN5draco14PointAttribute18SetExplicitMappingEm = comdat any

$_ZN5draco14PointAttribute18SetIdentityMappingEv = comdat any

$_ZN5draco14PointAttribute6ResizeEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE9push_backERKi = comdat any

$_ZN5draco17GeometryAttribute13set_unique_idEj = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZN5draco10PointCloud9attributeEi = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5eraseENS_11__wrap_iterIPKS6_EE = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv = comdat any

$_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEplEl = comdat any

$_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2IPS6_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleISC_S8_EE5valueEvE4typeE = comdat any

$_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEcvbEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco16GeometryMetadata33DeleteAttributeMetadataByUniqueIdEi = comdat any

$_ZNSt3__24findINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE5beginEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE3endEv = comdat any

$_ZNSt3__2neIPiEEbRKNS_11__wrap_iterIT_EES6_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEE = comdat any

$_ZNSt3__211__wrap_iterIPKiEC2IPiEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm = comdat any

$_ZNSt3__214numeric_limitsIfE3maxEv = comdat any

$_ZN5draco7VectorDIfLi3EEC2ERKfS3_S3_ = comdat any

$_ZN5draco7VectorDIfLi3EEC2Ev = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco14PointAttribute4sizeEv = comdat any

$_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv = comdat any

$_ZN5draco7VectorDIfLi3EEixEi = comdat any

$_ZN5draco11BoundingBox19update_bounding_boxERKNS_7VectorDIfLi3EEE = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv = comdat any

$_ZN5draco10PointCloudD2Ev = comdat any

$_ZN5draco10PointCloudD0Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEpLEl = comdat any

$_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE4baseEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv = comdat any

$_ZNSt3__2neIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESC_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv = comdat any

$_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEptEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNK5draco17AttributeMetadata13att_unique_idEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5eraseENS_11__wrap_iterIPKS6_EE = comdat any

$_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2IPS6_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleISC_S8_EE5valueEvE4typeE = comdat any

$_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEppEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_ = comdat any

$_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES7_ = comdat any

$_ZNSt3__2eqIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EEbRKNS_11__wrap_iterIT_EERKNS8_IT0_EE = comdat any

$_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv = comdat any

$_ZNSt3__29addressofINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_RS7_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__2miIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES8_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNSA_IT0_EE = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6cbeginEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__24moveIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_ = comdat any

$_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_ = comdat any

$_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES8_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco17AttributeMetadataD2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco8MetadataD2Ev = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev = comdat any

$_ZN5draco10EntryValueD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26__moveIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_ = comdat any

$_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEET_S8_ = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__2eqIPiS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE = comdat any

$_ZNKSt3__211__wrap_iterIPiE4baseEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIfLb1EE3maxEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco10DataBuffer4ReadExPvm = comdat any

$_ZNK5draco10DataBuffer4dataEv = comdat any

$_ZNK5draco7VectorDIfLi3EEixEi = comdat any

$_ZNKSt3__25arrayIfLm3EEixEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco16GeometryMetadataD2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__25arrayIfLm3EEixEm = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIiEC2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_ = comdat any

$_ZN5draco14PointAttributeD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco22AttributeTransformDataD2Ev = comdat any

$_ZN5draco10DataBufferD2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_ = comdat any

$_ZNSt3__24swapIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__24moveIRPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE22__construct_one_at_endIJRKiEEEvDpOT_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIRKiEEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJRKiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIiE9constructIiJRKiEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIiE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIiE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_ = comdat any

$_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2ES7_ = comdat any

$_ZNSt3__2miIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES8_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNSA_IT0_EE = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6cbeginEv = comdat any

$_ZNSt3__24moveIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_ = comdat any

$_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE4baseEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_ = comdat any

$_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2ES8_ = comdat any

$_ZNSt3__26__moveIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_ = comdat any

$_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEET_S8_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPi = comdat any

$_ZNSt3__211__wrap_iterIPiEC2ES1_ = comdat any

$_ZNKSt3__211__wrap_iterIPiEdeEv = comdat any

$_ZNSt3__211__wrap_iterIPiEppEv = comdat any

$_ZNSt3__2miIPKiS2_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS4_IT0_EE = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE6cbeginEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__24moveIPiS1_EET0_T_S3_S2_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi = comdat any

$_ZNKSt3__211__wrap_iterIPKiE4baseEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE5beginEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPKi = comdat any

$_ZNSt3__211__wrap_iterIPKiEC2ES2_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26__moveIiiEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_ = comdat any

$_ZNSt3__213__unwrap_iterIPiEET_S2_ = comdat any

@_ZTVN5draco10PointCloudE = hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::PointCloud"* (%"class.draco::PointCloud"*)* @_ZN5draco10PointCloudD2Ev to i8*), i8* bitcast (void (%"class.draco::PointCloud"*)* @_ZN5draco10PointCloudD0Ev to i8*), i8* bitcast (void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)* @_ZN5draco10PointCloud12SetAttributeEiNSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE to i8*), i8* bitcast (void (%"class.draco::PointCloud"*, i32)* @_ZN5draco10PointCloud15DeleteAttributeEi to i8*)] }, align 4
@_ZN5dracoL27kInvalidAttributeValueIndexE = internal constant %"class.draco::IndexType" { i32 -1 }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco10PointCloudC1Ev = hidden unnamed_addr alias %"class.draco::PointCloud"* (%"class.draco::PointCloud"*), %"class.draco::PointCloud"* (%"class.draco::PointCloud"*)* @_ZN5draco10PointCloudC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointCloud"* @_ZN5draco10PointCloudC2Ev(%"class.draco::PointCloud"* returned %this) unnamed_addr #0 {
entry:
  %retval = alloca %"class.draco::PointCloud"*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  store %"class.draco::PointCloud"* %this1, %"class.draco::PointCloud"** %retval, align 4
  %0 = bitcast %"class.draco::PointCloud"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco10PointCloudE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %metadata_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* %metadata_) #8
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.51"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.51"* %attributes_) #8
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.std::__2::vector.87", %"class.std::__2::vector.87"* %array.begin, i32 5
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.std::__2::vector.87"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call3 = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.87"* %arrayctor.cur) #8
  %arrayctor.next = getelementptr inbounds %"class.std::__2::vector.87", %"class.std::__2::vector.87"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.std::__2::vector.87"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  store i32 0, i32* %num_points_, align 4
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %retval, align 4
  ret %"class.draco::PointCloud"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::GeometryMetadata"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  store %"class.draco::GeometryMetadata"* null, %"class.draco::GeometryMetadata"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.51"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.51"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call %"class.std::__2::__vector_base.52"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.52"* %0) #8
  ret %"class.std::__2::vector.51"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.88"* %0) #8
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco10PointCloud18NumNamedAttributesENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %this, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %type.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  %cmp = icmp eq i32 %0, -1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %type.addr, align 4
  %cmp2 = icmp sge i32 %1, 5
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %2 = load i32, i32* %type.addr, align 4
  %arrayidx = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 %2
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %arrayidx) #8
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco10PointCloud19GetNamedAttributeIdENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %this, i32 %type) #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %type.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  %call = call i32 @_ZNK5draco10PointCloud19GetNamedAttributeIdENS_17GeometryAttribute4TypeEi(%"class.draco::PointCloud"* %this1, i32 %0, i32 0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco10PointCloud19GetNamedAttributeIdENS_17GeometryAttribute4TypeEi(%"class.draco::PointCloud"* %this, i32 %type, i32 %i) #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %type.addr = alloca i32, align 4
  %i.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  %call = call i32 @_ZNK5draco10PointCloud18NumNamedAttributesENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %this1, i32 %0)
  %1 = load i32, i32* %i.addr, align 4
  %cmp = icmp sle i32 %call, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %2 = load i32, i32* %type.addr, align 4
  %arrayidx = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 %2
  %3 = load i32, i32* %i.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %arrayidx, i32 %3) #8
  %4 = load i32, i32* %call2, align 4
  store i32 %4, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %this, i32 %type) #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %type.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  %call = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeEi(%"class.draco::PointCloud"* %this1, i32 %0, i32 0)
  ret %"class.draco::PointAttribute"* %call
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeEi(%"class.draco::PointCloud"* %this, i32 %type, i32 %i) #0 {
entry:
  %retval = alloca %"class.draco::PointAttribute"*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %type.addr = alloca i32, align 4
  %i.addr = alloca i32, align 4
  %att_id = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  %1 = load i32, i32* %i.addr, align 4
  %call = call i32 @_ZNK5draco10PointCloud19GetNamedAttributeIdENS_17GeometryAttribute4TypeEi(%"class.draco::PointCloud"* %this1, i32 %0, i32 %1)
  store i32 %call, i32* %att_id, align 4
  %2 = load i32, i32* %att_id, align 4
  %cmp = icmp eq i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %3 = load i32, i32* %att_id, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_, i32 %3) #8
  %call3 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %call2) #8
  store %"class.draco::PointAttribute"* %call3, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %retval, align 4
  ret %"class.draco::PointAttribute"* %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.53"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud27GetNamedAttributeByUniqueIdENS_17GeometryAttribute4TypeEj(%"class.draco::PointCloud"* %this, i32 %type, i32 %unique_id) #0 {
entry:
  %retval = alloca %"class.draco::PointAttribute"*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %type.addr = alloca i32, align 4
  %unique_id.addr = alloca i32, align 4
  %att_id = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  store i32 %unique_id, i32* %unique_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 0, i32* %att_id, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %att_id, align 4
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %1 = load i32, i32* %type.addr, align 4
  %arrayidx = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 %1
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %arrayidx) #8
  %cmp = icmp ult i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %named_attribute_index_2 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %2 = load i32, i32* %type.addr, align 4
  %arrayidx3 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_2, i32 0, i32 %2
  %3 = load i32, i32* %att_id, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %arrayidx3, i32 %3) #8
  %4 = load i32, i32* %call4, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_, i32 %4) #8
  %call6 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %call5) #8
  %5 = bitcast %"class.draco::PointAttribute"* %call6 to %"class.draco::GeometryAttribute"*
  %call7 = call i32 @_ZNK5draco17GeometryAttribute9unique_idEv(%"class.draco::GeometryAttribute"* %5)
  %6 = load i32, i32* %unique_id.addr, align 4
  %cmp8 = icmp eq i32 %call7, %6
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %attributes_9 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %named_attribute_index_10 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %7 = load i32, i32* %type.addr, align 4
  %arrayidx11 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_10, i32 0, i32 %7
  %8 = load i32, i32* %att_id, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %arrayidx11, i32 %8) #8
  %9 = load i32, i32* %call12, align 4
  %call13 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_9, i32 %9) #8
  %call14 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %call13) #8
  store %"class.draco::PointAttribute"* %call14, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %att_id, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %att_id, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %11 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %retval, align 4
  ret %"class.draco::PointAttribute"* %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute9unique_idEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %unique_id_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 8
  %0 = load i32, i32* %unique_id_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud22GetAttributeByUniqueIdEj(%"class.draco::PointCloud"* %this, i32 %unique_id) #0 {
entry:
  %retval = alloca %"class.draco::PointAttribute"*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %unique_id.addr = alloca i32, align 4
  %att_id = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %unique_id, i32* %unique_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %unique_id.addr, align 4
  %call = call i32 @_ZNK5draco10PointCloud24GetAttributeIdByUniqueIdEj(%"class.draco::PointCloud"* %this1, i32 %0)
  store i32 %call, i32* %att_id, align 4
  %1 = load i32, i32* %att_id, align 4
  %cmp = icmp eq i32 %1, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %2 = load i32, i32* %att_id, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_, i32 %2) #8
  %call3 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %call2) #8
  store %"class.draco::PointAttribute"* %call3, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %retval, align 4
  ret %"class.draco::PointAttribute"* %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco10PointCloud24GetAttributeIdByUniqueIdEj(%"class.draco::PointCloud"* %this, i32 %unique_id) #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %unique_id.addr = alloca i32, align 4
  %att_id = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %unique_id, i32* %unique_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 0, i32* %att_id, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %att_id, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %attributes_) #8
  %cmp = icmp ult i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %attributes_2 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %1 = load i32, i32* %att_id, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_2, i32 %1) #8
  %call4 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %call3) #8
  %2 = bitcast %"class.draco::PointAttribute"* %call4 to %"class.draco::GeometryAttribute"*
  %call5 = call i32 @_ZNK5draco17GeometryAttribute9unique_idEv(%"class.draco::GeometryAttribute"* %2)
  %3 = load i32, i32* %unique_id.addr, align 4
  %cmp6 = icmp eq i32 %call5, %3
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load i32, i32* %att_id, align 4
  store i32 %4, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %5 = load i32, i32* %att_id, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %att_id, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN5draco10PointCloud12AddAttributeENSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloud"* %this, %"class.std::__2::unique_ptr.53"* %pa) #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %agg.tmp = alloca %"class.std::__2::unique_ptr.53", align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %attributes_) #8
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %pa) #8
  %call3 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.53"* %agg.tmp, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call2) #8
  %0 = bitcast %"class.draco::PointCloud"* %this1 to void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)***
  %vtable = load void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)**, void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)*** %0, align 4
  %vfn = getelementptr inbounds void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)*, void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)** %vtable, i64 2
  %1 = load void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)*, void (%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*)** %vfn, align 4
  call void %1(%"class.draco::PointCloud"* %this1, i32 %call, %"class.std::__2::unique_ptr.53"* %agg.tmp)
  %call4 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %agg.tmp) #8
  %attributes_5 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call6 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %attributes_5) #8
  %sub = sub i32 %call6, 1
  ret i32 %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__t, %"class.std::__2::unique_ptr.53"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.53"* returned %this, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %ref.tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__u, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %call = call %"class.draco::PointAttribute"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.53"* %0) #8
  store %"class.draco::PointAttribute"* %call, %"class.draco::PointAttribute"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.53"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.54"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.53"* %this1, %"class.draco::PointAttribute"* null) #8
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN5draco10PointCloud12AddAttributeERKNS_17GeometryAttributeEbj(%"class.draco::PointCloud"* %this, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %att, i1 zeroext %identity_mapping, i32 %num_attribute_values) #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %identity_mapping.addr = alloca i8, align 1
  %num_attribute_values.addr = alloca i32, align 4
  %pa = alloca %"class.std::__2::unique_ptr.53", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %att_id = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::unique_ptr.53", align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store %"class.draco::GeometryAttribute"* %att, %"class.draco::GeometryAttribute"** %att.addr, align 4
  %frombool = zext i1 %identity_mapping to i8
  store i8 %frombool, i8* %identity_mapping.addr, align 1
  store i32 %num_attribute_values, i32* %num_attribute_values.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %att.addr, align 4
  %1 = load i8, i8* %identity_mapping.addr, align 1
  %tobool = trunc i8 %1 to i1
  %2 = load i32, i32* %num_attribute_values.addr, align 4
  call void @_ZNK5draco10PointCloud15CreateAttributeERKNS_17GeometryAttributeEbj(%"class.std::__2::unique_ptr.53"* sret align 4 %pa, %"class.draco::PointCloud"* %this1, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %0, i1 zeroext %tobool, i32 %2)
  %call = call zeroext i1 @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %pa) #8
  %call3 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.53"* %agg.tmp, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call2) #8
  %call4 = call i32 @_ZN5draco10PointCloud12AddAttributeENSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloud"* %this1, %"class.std::__2::unique_ptr.53"* %agg.tmp)
  %call5 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %agg.tmp) #8
  store i32 %call4, i32* %att_id, align 4
  %3 = load i32, i32* %att_id, align 4
  store i32 %3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %call6 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %pa) #8
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco10PointCloud15CreateAttributeERKNS_17GeometryAttributeEbj(%"class.std::__2::unique_ptr.53"* noalias sret align 4 %agg.result, %"class.draco::PointCloud"* %this, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %att, i1 zeroext %identity_mapping, i32 %num_attribute_values) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %identity_mapping.addr = alloca i8, align 1
  %num_attribute_values.addr = alloca i32, align 4
  %pa = alloca %"class.std::__2::unique_ptr.53", align 4
  %0 = bitcast %"class.std::__2::unique_ptr.53"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store %"class.draco::GeometryAttribute"* %att, %"class.draco::GeometryAttribute"** %att.addr, align 4
  %frombool = zext i1 %identity_mapping to i8
  store i8 %frombool, i8* %identity_mapping.addr, align 1
  store i32 %num_attribute_values, i32* %num_attribute_values.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %att.addr, align 4
  %call = call i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %1)
  %cmp = icmp eq i32 %call, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.53"* %agg.result, i8* null) #8
  br label %return

if.end:                                           ; preds = %entry
  %call3 = call noalias nonnull i8* @_Znwm(i32 96) #9
  %2 = bitcast i8* %call3 to %"class.draco::PointAttribute"*
  %3 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %att.addr, align 4
  %call4 = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeC1ERKNS_17GeometryAttributeE(%"class.draco::PointAttribute"* %2, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %3)
  %call5 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.53"* %pa, %"class.draco::PointAttribute"* %2) #8
  %4 = load i8, i8* %identity_mapping.addr, align 1
  %tobool = trunc i8 %4 to i1
  br i1 %tobool, label %if.else, label %if.then6

if.then6:                                         ; preds = %if.end
  %call7 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %5 = load i32, i32* %num_points_, align 4
  call void @_ZN5draco14PointAttribute18SetExplicitMappingEm(%"class.draco::PointAttribute"* %call7, i32 %5)
  br label %if.end11

if.else:                                          ; preds = %if.end
  %call8 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  call void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %call8)
  %call9 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  %num_points_10 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %6 = load i32, i32* %num_points_10, align 4
  call void @_ZN5draco14PointAttribute6ResizeEm(%"class.draco::PointAttribute"* %call9, i32 %6)
  br label %if.end11

if.end11:                                         ; preds = %if.else, %if.then6
  %7 = load i32, i32* %num_attribute_values.addr, align 4
  %cmp12 = icmp ugt i32 %7, 0
  br i1 %cmp12, label %if.then13, label %if.end16

if.then13:                                        ; preds = %if.end11
  %call14 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  %8 = load i32, i32* %num_attribute_values.addr, align 4
  %call15 = call zeroext i1 @_ZN5draco14PointAttribute5ResetEm(%"class.draco::PointAttribute"* %call14, i32 %8)
  br label %if.end16

if.end16:                                         ; preds = %if.then13, %if.end11
  %call17 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.53"* %agg.result, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %pa) #8
  %call18 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %pa) #8
  br label %return

return:                                           ; preds = %if.end16, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  %cmp = icmp ne %"class.draco::PointAttribute"* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %attribute_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 7
  %0 = load i32, i32* %attribute_type_, align 8
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.53"* returned %this, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %.addr = alloca i8*, align 4
  %ref.tmp = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.54"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #1

declare %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeC1ERKNS_17GeometryAttributeE(%"class.draco::PointAttribute"* returned, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64)) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.53"* returned %this, %"class.draco::PointAttribute"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.54"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute18SetExplicitMappingEm(%"class.draco::PointAttribute"* %this, i32 %num_points) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %num_points.addr = alloca i32, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 0, i8* %identity_mapping_, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %0 = load i32, i32* %num_points.addr, align 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_(%"class.draco::IndexTypeVector"* %indices_map_, i32 %0, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) @_ZN5dracoL27kInvalidAttributeValueIndexE)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 1, i8* %identity_mapping_, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %indices_map_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute6ResizeEm(%"class.draco::PointAttribute"* %this, i32 %new_num_unique_entries) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %new_num_unique_entries.addr = alloca i32, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i32 %new_num_unique_entries, i32* %new_num_unique_entries.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %0 = load i32, i32* %new_num_unique_entries.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  store i32 %0, i32* %num_unique_entries_, align 8
  ret void
}

declare zeroext i1 @_ZN5draco14PointAttribute5ResetEm(%"class.draco::PointAttribute"*, i32) #2

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco10PointCloud12SetAttributeEiNSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloud"* %this, i32 %att_id, %"class.std::__2::unique_ptr.53"* %pa) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %attributes_) #8
  %0 = load i32, i32* %att_id.addr, align 4
  %cmp = icmp sle i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %attributes_2 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %1 = load i32, i32* %att_id.addr, align 4
  %add = add nsw i32 %1, 1
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.51"* %attributes_2, i32 %add)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call3 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  %2 = bitcast %"class.draco::PointAttribute"* %call3 to %"class.draco::GeometryAttribute"*
  %call4 = call i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %2)
  %cmp5 = icmp slt i32 %call4, 5
  br i1 %cmp5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %call7 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  %3 = bitcast %"class.draco::PointAttribute"* %call7 to %"class.draco::GeometryAttribute"*
  %call8 = call i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %3)
  %arrayidx = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 %call8
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE9push_backERKi(%"class.std::__2::vector.87"* %arrayidx, i32* nonnull align 4 dereferenceable(4) %att_id.addr)
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  %call10 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %pa) #8
  %4 = bitcast %"class.draco::PointAttribute"* %call10 to %"class.draco::GeometryAttribute"*
  %5 = load i32, i32* %att_id.addr, align 4
  call void @_ZN5draco17GeometryAttribute13set_unique_idEj(%"class.draco::GeometryAttribute"* %4, i32 %5)
  %call11 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %pa) #8
  %attributes_12 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %6 = load i32, i32* %att_id.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_12, i32 %6) #8
  %call14 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.53"* %call13, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call11) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.51"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.51"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %7, i32 %8
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.51"* %this1, %"class.std::__2::unique_ptr.53"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE9push_backERKi(%"class.std::__2::vector.87"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %2) #8
  %3 = load i32*, i32** %call, align 4
  %cmp = icmp ne i32* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE22__construct_one_at_endIJRKiEEEvDpOT_(%"class.std::__2::vector.87"* %this1, i32* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIRKiEEvOT_(%"class.std::__2::vector.87"* %this1, i32* nonnull align 4 dereferenceable(4) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17GeometryAttribute13set_unique_idEj(%"class.draco::GeometryAttribute"* %this, i32 %id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %id.addr = alloca i32, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4
  %unique_id_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 8
  store i32 %0, i32* %unique_id_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.53"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__u, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %call = call %"class.draco::PointAttribute"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.53"* %0) #8
  call void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.53"* %this1, %"class.draco::PointAttribute"* %call) #8
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.53"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %call2) #8
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco10PointCloud15DeleteAttributeEi(%"class.draco::PointCloud"* %this, i32 %att_id) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  %att_type = alloca i32, align 4
  %unique_id = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.94", align 4
  %ref.tmp10 = alloca %"class.std::__2::__wrap_iter.94", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.94", align 4
  %it = alloca %"class.std::__2::__wrap_iter.95", align 4
  %agg.tmp26 = alloca %"class.std::__2::__wrap_iter.95", align 4
  %agg.tmp29 = alloca %"class.std::__2::__wrap_iter.95", align 4
  %ref.tmp38 = alloca %"class.std::__2::__wrap_iter.95", align 4
  %agg.tmp47 = alloca %"class.std::__2::__wrap_iter.96", align 4
  %coerce51 = alloca %"class.std::__2::__wrap_iter.95", align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %att_id.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %att_id.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %attributes_) #8
  %cmp2 = icmp uge i32 %1, %call
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %for.end73

if.end:                                           ; preds = %lor.lhs.false
  %attributes_3 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %2 = load i32, i32* %att_id.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_3, i32 %2) #8
  %call5 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.53"* %call4) #8
  %3 = bitcast %"class.draco::PointAttribute"* %call5 to %"class.draco::GeometryAttribute"*
  %call6 = call i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %3)
  store i32 %call6, i32* %att_type, align 4
  %4 = load i32, i32* %att_id.addr, align 4
  %call7 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %this1, i32 %4)
  %5 = bitcast %"class.draco::PointAttribute"* %call7 to %"class.draco::GeometryAttribute"*
  %call8 = call i32 @_ZNK5draco17GeometryAttribute9unique_idEv(%"class.draco::GeometryAttribute"* %5)
  store i32 %call8, i32* %unique_id, align 4
  %attributes_9 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %attributes_11 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call12 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector.51"* %attributes_11) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %ref.tmp10, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call12, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  %6 = load i32, i32* %att_id.addr, align 4
  %call13 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEplEl(%"class.std::__2::__wrap_iter.94"* %ref.tmp10, i32 %6) #8
  %coerce.dive14 = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call13, %"class.std::__2::unique_ptr.53"** %coerce.dive14, align 4
  %call15 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2IPS6_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleISC_S8_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.94"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #8
  %coerce.dive16 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %7 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive16, align 4
  %call17 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5eraseENS_11__wrap_iterIPKS6_EE(%"class.std::__2::vector.51"* %attributes_9, %"class.std::__2::unique_ptr.53"* %7)
  %coerce.dive18 = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %coerce, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call17, %"class.std::__2::unique_ptr.53"** %coerce.dive18, align 4
  %metadata_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 1
  %call19 = call zeroext i1 @_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr"* %metadata_) #8
  br i1 %call19, label %if.then20, label %if.end23

if.then20:                                        ; preds = %if.end
  %metadata_21 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 1
  %call22 = call %"class.draco::GeometryMetadata"* @_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %metadata_21) #8
  %8 = load i32, i32* %unique_id, align 4
  call void @_ZN5draco16GeometryMetadata33DeleteAttributeMetadataByUniqueIdEi(%"class.draco::GeometryMetadata"* %call22, i32 %8)
  br label %if.end23

if.end23:                                         ; preds = %if.then20, %if.end
  %9 = load i32, i32* %att_type, align 4
  %cmp24 = icmp slt i32 %9, 5
  br i1 %cmp24, label %if.then25, label %if.end54

if.then25:                                        ; preds = %if.end23
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %10 = load i32, i32* %att_type, align 4
  %arrayidx = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 %10
  %call27 = call i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE5beginEv(%"class.std::__2::vector.87"* %arrayidx) #8
  %coerce.dive28 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %agg.tmp26, i32 0, i32 0
  store i32* %call27, i32** %coerce.dive28, align 4
  %named_attribute_index_30 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %11 = load i32, i32* %att_type, align 4
  %arrayidx31 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_30, i32 0, i32 %11
  %call32 = call i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE3endEv(%"class.std::__2::vector.87"* %arrayidx31) #8
  %coerce.dive33 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %agg.tmp29, i32 0, i32 0
  store i32* %call32, i32** %coerce.dive33, align 4
  %coerce.dive34 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %agg.tmp26, i32 0, i32 0
  %12 = load i32*, i32** %coerce.dive34, align 4
  %coerce.dive35 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %agg.tmp29, i32 0, i32 0
  %13 = load i32*, i32** %coerce.dive35, align 4
  %call36 = call i32* @_ZNSt3__24findINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_(i32* %12, i32* %13, i32* nonnull align 4 dereferenceable(4) %att_id.addr)
  %coerce.dive37 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %it, i32 0, i32 0
  store i32* %call36, i32** %coerce.dive37, align 4
  %named_attribute_index_39 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %14 = load i32, i32* %att_type, align 4
  %arrayidx40 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_39, i32 0, i32 %14
  %call41 = call i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE3endEv(%"class.std::__2::vector.87"* %arrayidx40) #8
  %coerce.dive42 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %ref.tmp38, i32 0, i32 0
  store i32* %call41, i32** %coerce.dive42, align 4
  %call43 = call zeroext i1 @_ZNSt3__2neIPiEEbRKNS_11__wrap_iterIT_EES6_(%"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %it, %"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %ref.tmp38) #8
  br i1 %call43, label %if.then44, label %if.end53

if.then44:                                        ; preds = %if.then25
  %named_attribute_index_45 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %15 = load i32, i32* %att_type, align 4
  %arrayidx46 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_45, i32 0, i32 %15
  %call48 = call %"class.std::__2::__wrap_iter.96"* @_ZNSt3__211__wrap_iterIPKiEC2IPiEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter.96"* %agg.tmp47, %"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %it, i8* null) #8
  %coerce.dive49 = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %agg.tmp47, i32 0, i32 0
  %16 = load i32*, i32** %coerce.dive49, align 4
  %call50 = call i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEE(%"class.std::__2::vector.87"* %arrayidx46, i32* %16)
  %coerce.dive52 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %coerce51, i32 0, i32 0
  store i32* %call50, i32** %coerce.dive52, align 4
  br label %if.end53

if.end53:                                         ; preds = %if.then44, %if.then25
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.end23
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc71, %if.end54
  %17 = load i32, i32* %i, align 4
  %cmp55 = icmp slt i32 %17, 5
  br i1 %cmp55, label %for.body, label %for.end73

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc, %for.body
  %18 = load i32, i32* %j, align 4
  %named_attribute_index_57 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %19 = load i32, i32* %i, align 4
  %arrayidx58 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_57, i32 0, i32 %19
  %call59 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %arrayidx58) #8
  %cmp60 = icmp ult i32 %18, %call59
  br i1 %cmp60, label %for.body61, label %for.end

for.body61:                                       ; preds = %for.cond56
  %named_attribute_index_62 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %20 = load i32, i32* %i, align 4
  %arrayidx63 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_62, i32 0, i32 %20
  %21 = load i32, i32* %j, align 4
  %call64 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %arrayidx63, i32 %21) #8
  %22 = load i32, i32* %call64, align 4
  %23 = load i32, i32* %att_id.addr, align 4
  %cmp65 = icmp sgt i32 %22, %23
  br i1 %cmp65, label %if.then66, label %if.end70

if.then66:                                        ; preds = %for.body61
  %named_attribute_index_67 = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %24 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_67, i32 0, i32 %24
  %25 = load i32, i32* %j, align 4
  %call69 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %arrayidx68, i32 %25) #8
  %26 = load i32, i32* %call69, align 4
  %dec = add nsw i32 %26, -1
  store i32 %dec, i32* %call69, align 4
  br label %if.end70

if.end70:                                         ; preds = %if.then66, %for.body61
  br label %for.inc

for.inc:                                          ; preds = %if.end70
  %27 = load i32, i32* %j, align 4
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond56

for.end:                                          ; preds = %for.cond56
  br label %for.inc71

for.inc71:                                        ; preds = %for.end
  %28 = load i32, i32* %i, align 4
  %inc72 = add nsw i32 %28, 1
  store i32 %inc72, i32* %i, align 4
  br label %for.cond

for.end73:                                        ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %this, i32 %att_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %0 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_, i32 %0) #8
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %call) #8
  ret %"class.draco::PointAttribute"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5eraseENS_11__wrap_iterIPKS6_EE(%"class.std::__2::vector.51"* %this, %"class.std::__2::unique_ptr.53"* %__position.coerce) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.94", align 4
  %__position = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__ps = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %__p = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__position, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %__position.coerce, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6cbeginEv(%"class.std::__2::vector.51"* %this1) #8
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call, %"class.std::__2::unique_ptr.53"** %coerce.dive2, align 4
  %call3 = call i32 @_ZNSt3__2miIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES8_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNSA_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__position, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  store i32 %call3, i32* %__ps, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %2 = load i32, i32* %__ps, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %1, i32 %2
  store %"class.std::__2::unique_ptr.53"* %add.ptr, %"class.std::__2::unique_ptr.53"** %__p, align 4
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p, align 4
  %add.ptr4 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %3, i32 1
  %4 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %4, i32 0, i32 1
  %5 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %6 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p, align 4
  %call5 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.53"* %add.ptr4, %"class.std::__2::unique_ptr.53"* %5, %"class.std::__2::unique_ptr.53"* %6)
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.51"* %this1, %"class.std::__2::unique_ptr.53"* %call5) #8
  %7 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p, align 4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %7, i32 -1
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.51"* %this1, %"class.std::__2::unique_ptr.53"* %add.ptr6)
  %8 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p, align 4
  %call7 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector.51"* %this1, %"class.std::__2::unique_ptr.53"* %8) #8
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call7, %"class.std::__2::unique_ptr.53"** %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %retval, i32 0, i32 0
  %9 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive9, align 4
  ret %"class.std::__2::unique_ptr.53"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.94", align 4
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector.51"* %this1, %"class.std::__2::unique_ptr.53"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.53"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEplEl(%"class.std::__2::__wrap_iter.94"* %this, i32 %__n) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.94", align 4
  %this.addr = alloca %"class.std::__2::__wrap_iter.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::__wrap_iter.94"* %this, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.94"*, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__wrap_iter.94"* %retval to i8*
  %1 = bitcast %"class.std::__2::__wrap_iter.94"* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %2 = load i32, i32* %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.94"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEpLEl(%"class.std::__2::__wrap_iter.94"* %retval, i32 %2) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %retval, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  ret %"class.std::__2::unique_ptr.53"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2IPS6_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleISC_S8_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::__wrap_iter.94"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.94"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.94"* %__u, %"class.std::__2::__wrap_iter.94"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.94"*, %"class.std::__2::__wrap_iter.94"** %__u.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.94"* %1) #8
  store %"class.std::__2::unique_ptr.53"* %call, %"class.std::__2::unique_ptr.53"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #8
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  %cmp = icmp ne %"class.draco::GeometryMetadata"* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::GeometryMetadata"* @_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #8
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  ret %"class.draco::GeometryMetadata"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco16GeometryMetadata33DeleteAttributeMetadataByUniqueIdEi(%"class.draco::GeometryMetadata"* %this, i32 %att_unique_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  %att_unique_id.addr = alloca i32, align 4
  %itr = alloca %"class.std::__2::__wrap_iter.100", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.100", align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter.101", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.100", align 4
  store %"class.draco::GeometryMetadata"* %this, %"class.draco::GeometryMetadata"** %this.addr, align 4
  store i32 %att_unique_id, i32* %att_unique_id.addr, align 4
  %this1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %att_metadatas_ = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector"* %att_metadatas_) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %itr, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %att_metadatas_2 = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::vector"* %att_metadatas_2) #8
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call3, %"class.std::__2::unique_ptr.40"** %coerce.dive4, align 4
  %call5 = call zeroext i1 @_ZNSt3__2neIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESC_(%"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %itr, %"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  br i1 %call5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call6 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEptEv(%"class.std::__2::__wrap_iter.100"* %itr) #8
  %call7 = call %"class.draco::AttributeMetadata"* @_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.40"* %call6) #8
  %call8 = call i32 @_ZNK5draco17AttributeMetadata13att_unique_idEv(%"class.draco::AttributeMetadata"* %call7)
  %0 = load i32, i32* %att_unique_id.addr, align 4
  %cmp = icmp eq i32 %call8, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %att_metadatas_9 = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call10 = call %"class.std::__2::__wrap_iter.101"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2IPS6_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleISC_S8_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter.101"* %agg.tmp, %"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %itr, i8* null) #8
  %coerce.dive11 = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %agg.tmp, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive11, align 4
  %call12 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5eraseENS_11__wrap_iterIPKS6_EE(%"class.std::__2::vector"* %att_metadatas_9, %"class.std::__2::unique_ptr.40"* %1)
  %coerce.dive13 = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %coerce, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call12, %"class.std::__2::unique_ptr.40"** %coerce.dive13, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %call14 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.100"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEppEv(%"class.std::__2::__wrap_iter.100"* %itr) #8
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__24findINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_(i32* %__first.coerce, i32* %__last.coerce, i32* nonnull align 4 dereferenceable(4) %__value_) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.95", align 4
  %__first = alloca %"class.std::__2::__wrap_iter.95", align 4
  %__last = alloca %"class.std::__2::__wrap_iter.95", align 4
  %__value_.addr = alloca i32*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %__first, i32 0, i32 0
  store i32* %__first.coerce, i32** %coerce.dive, align 4
  %coerce.dive1 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %__last, i32 0, i32 0
  store i32* %__last.coerce, i32** %coerce.dive1, align 4
  store i32* %__value_, i32** %__value_.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %call = call zeroext i1 @_ZNSt3__2neIPiEEbRKNS_11__wrap_iterIT_EES6_(%"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %__first, %"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %__last) #8
  br i1 %call, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPiEdeEv(%"class.std::__2::__wrap_iter.95"* %__first) #8
  %0 = load i32, i32* %call2, align 4
  %1 = load i32*, i32** %__value_.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp eq i32 %0, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.95"* @_ZNSt3__211__wrap_iterIPiEppEv(%"class.std::__2::__wrap_iter.95"* %__first) #8
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %3 = bitcast %"class.std::__2::__wrap_iter.95"* %retval to i8*
  %4 = bitcast %"class.std::__2::__wrap_iter.95"* %__first to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  %5 = load i32*, i32** %coerce.dive4, align 4
  ret i32* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE5beginEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.95", align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPi(%"class.std::__2::vector.87"* %this1, i32* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE3endEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.95", align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %call = call i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPi(%"class.std::__2::vector.87"* %this1, i32* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neIPiEEbRKNS_11__wrap_iterIT_EES6_(%"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  store %"class.std::__2::__wrap_iter.95"* %__x, %"class.std::__2::__wrap_iter.95"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.95"* %__y, %"class.std::__2::__wrap_iter.95"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %__x.addr, align 4
  %1 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqIPiS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE(%"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %1) #8
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEE(%"class.std::__2::vector.87"* %this, i32* %__position.coerce) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.95", align 4
  %__position = alloca %"class.std::__2::__wrap_iter.96", align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__ps = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.96", align 4
  %__p = alloca i32*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %__position, i32 0, i32 0
  store i32* %__position.coerce, i32** %coerce.dive, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE6cbeginEv(%"class.std::__2::vector.87"* %this1) #8
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %ref.tmp, i32 0, i32 0
  store i32* %call, i32** %coerce.dive2, align 4
  %call3 = call i32 @_ZNSt3__2miIPKiS2_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS4_IT0_EE(%"class.std::__2::__wrap_iter.96"* nonnull align 4 dereferenceable(4) %__position, %"class.std::__2::__wrap_iter.96"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  store i32 %call3, i32* %__ps, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__ps, align 4
  %add.ptr = getelementptr inbounds i32, i32* %1, i32 %2
  store i32* %add.ptr, i32** %__p, align 4
  %3 = load i32*, i32** %__p, align 4
  %add.ptr4 = getelementptr inbounds i32, i32* %3, i32 1
  %4 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %4, i32 0, i32 1
  %5 = load i32*, i32** %__end_, align 4
  %6 = load i32*, i32** %__p, align 4
  %call5 = call i32* @_ZNSt3__24moveIPiS1_EET0_T_S3_S2_(i32* %add.ptr4, i32* %5, i32* %6)
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::vector.87"* %this1, i32* %call5) #8
  %7 = load i32*, i32** %__p, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %7, i32 -1
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi(%"class.std::__2::vector.87"* %this1, i32* %add.ptr6)
  %8 = load i32*, i32** %__p, align 4
  %call7 = call i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPi(%"class.std::__2::vector.87"* %this1, i32* %8) #8
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  store i32* %call7, i32** %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  %9 = load i32*, i32** %coerce.dive9, align 4
  ret i32* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.96"* @_ZNSt3__211__wrap_iterIPKiEC2IPiEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter.96"* returned %this, %"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.96"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter.96"* %this, %"class.std::__2::__wrap_iter.96"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.95"* %__u, %"class.std::__2::__wrap_iter.95"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.96"*, %"class.std::__2::__wrap_iter.96"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %__u.addr, align 4
  %call = call i32* @_ZNKSt3__211__wrap_iterIPiE4baseEv(%"class.std::__2::__wrap_iter.95"* %1) #8
  store i32* %call, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco10PointCloud18ComputeBoundingBoxEv(%"class.draco::BoundingBox"* noalias sret align 4 %agg.result, %"class.draco::PointCloud"* %this) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %ref.tmp = alloca %"class.draco::VectorD", align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca %"class.draco::VectorD", align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %pc_att = alloca %"class.draco::PointAttribute"*, align 4
  %p = alloca %"class.draco::VectorD", align 4
  %i = alloca %"class.draco::IndexType", align 4
  %ref.tmp22 = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %0 = bitcast %"class.draco::BoundingBox"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %call = call float @_ZNSt3__214numeric_limitsIfE3maxEv() #8
  store float %call, float* %ref.tmp2, align 4
  %call4 = call float @_ZNSt3__214numeric_limitsIfE3maxEv() #8
  store float %call4, float* %ref.tmp3, align 4
  %call6 = call float @_ZNSt3__214numeric_limitsIfE3maxEv() #8
  store float %call6, float* %ref.tmp5, align 4
  %call7 = call %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKfS3_S3_(%"class.draco::VectorD"* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %call10 = call float @_ZNSt3__214numeric_limitsIfE3maxEv() #8
  %fneg = fneg float %call10
  store float %fneg, float* %ref.tmp9, align 4
  %call12 = call float @_ZNSt3__214numeric_limitsIfE3maxEv() #8
  %fneg13 = fneg float %call12
  store float %fneg13, float* %ref.tmp11, align 4
  %call15 = call float @_ZNSt3__214numeric_limitsIfE3maxEv() #8
  %fneg16 = fneg float %call15
  store float %fneg16, float* %ref.tmp14, align 4
  %call17 = call %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKfS3_S3_(%"class.draco::VectorD"* %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %call18 = call %"class.draco::BoundingBox"* @_ZN5draco11BoundingBoxC1ERKNS_7VectorDIfLi3EEES4_(%"class.draco::BoundingBox"* %agg.result, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %ref.tmp, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %ref.tmp8)
  %call19 = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %this1, i32 0)
  store %"class.draco::PointAttribute"* %call19, %"class.draco::PointAttribute"** %pc_att, align 4
  %call20 = call %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2Ev(%"class.draco::VectorD"* %p)
  %call21 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %i, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pc_att, align 4
  %call23 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %1)
  store i32 %call23, i32* %ref.tmp22, align 4
  %call24 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %i, i32* nonnull align 4 dereferenceable(4) %ref.tmp22)
  br i1 %call24, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pc_att, align 4
  %3 = bitcast %"class.draco::PointAttribute"* %2 to %"class.draco::GeometryAttribute"*
  %4 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %5 = bitcast %"class.draco::IndexType"* %i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %p, i32 0)
  %6 = bitcast float* %call25 to i8*
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %3, i32 %7, i8* %6)
  call void @_ZN5draco11BoundingBox19update_bounding_boxERKNS_7VectorDIfLi3EEE(%"class.draco::BoundingBox"* %agg.result, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %p)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %call26 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv(%"class.draco::IndexType"* %i)
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNSt3__214numeric_limitsIfE3maxEv() #0 comdat {
entry:
  %call = call float @_ZNSt3__223__libcpp_numeric_limitsIfLb1EE3maxEv() #8
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKfS3_S3_(%"class.draco::VectorD"* returned %this, float* nonnull align 4 dereferenceable(4) %c0, float* nonnull align 4 dereferenceable(4) %c1, float* nonnull align 4 dereferenceable(4) %c2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %c0.addr = alloca float*, align 4
  %c1.addr = alloca float*, align 4
  %c2.addr = alloca float*, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store float* %c0, float** %c0.addr, align 4
  store float* %c1, float** %c1.addr, align 4
  store float* %c2, float** %c2.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %v_, i32 0, i32 0
  %arrayinit.begin = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 0
  %0 = load float*, float** %c0.addr, align 4
  %1 = load float, float* %0, align 4
  store float %1, float* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %2 = load float*, float** %c1.addr, align 4
  %3 = load float, float* %2, align 4
  store float %3, float* %arrayinit.element, align 4
  %arrayinit.element2 = getelementptr inbounds float, float* %arrayinit.element, i32 1
  %4 = load float*, float** %c2.addr, align 4
  %5 = load float, float* %4, align 4
  store float %5, float* %arrayinit.element2, align 4
  ret %"class.draco::VectorD"* %this1
}

declare %"class.draco::BoundingBox"* @_ZN5draco11BoundingBoxC1ERKNS_7VectorDIfLi3EEES4_(%"class.draco::BoundingBox"* returned, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12), %"class.draco::VectorD"* nonnull align 4 dereferenceable(12)) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2Ev(%"class.draco::VectorD"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.draco::VectorD"*, align 4
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %i = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  store %"class.draco::VectorD"* %this1, %"class.draco::VectorD"** %retval, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this1, i32 %1)
  store float 0.000000e+00, float* %call, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %3 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %retval, align 4
  ret %"class.draco::VectorD"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %num_unique_entries_, align 8
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce, i8* %out_data) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %out_data.addr = alloca i8*, align 4
  %byte_pos = alloca i64, align 8
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store i8* %out_data, i8** %out_data.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  store i64 %add, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %3 = load i64, i64* %byte_pos, align 8
  %4 = load i8*, i8** %out_data.addr, align 4
  %byte_stride_2 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %5 = load i64, i64* %byte_stride_2, align 8
  %conv3 = trunc i64 %5 to i32
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %2, i64 %3, i8* %4, i32 %conv3)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this, i32 %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %i.addr = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %v_, i32 %0) #8
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11BoundingBox19update_bounding_boxERKNS_7VectorDIfLi3EEE(%"class.draco::BoundingBox"* %this, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %new_point) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::BoundingBox"*, align 4
  %new_point.addr = alloca %"class.draco::VectorD"*, align 4
  %i = alloca i32, align 4
  store %"class.draco::BoundingBox"* %this, %"class.draco::BoundingBox"** %this.addr, align 4
  store %"class.draco::VectorD"* %new_point, %"class.draco::VectorD"** %new_point.addr, align 4
  %this1 = load %"class.draco::BoundingBox"*, %"class.draco::BoundingBox"** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %new_point.addr, align 4
  %2 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %1, i32 %2)
  %3 = load float, float* %call, align 4
  %min_point_ = getelementptr inbounds %"class.draco::BoundingBox", %"class.draco::BoundingBox"* %this1, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %min_point_, i32 %4)
  %5 = load float, float* %call2, align 4
  %cmp3 = fcmp olt float %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %new_point.addr, align 4
  %7 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %6, i32 %7)
  %8 = load float, float* %call4, align 4
  %min_point_5 = getelementptr inbounds %"class.draco::BoundingBox", %"class.draco::BoundingBox"* %this1, i32 0, i32 0
  %9 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %min_point_5, i32 %9)
  store float %8, float* %call6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %10 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %new_point.addr, align 4
  %11 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %10, i32 %11)
  %12 = load float, float* %call7, align 4
  %max_point_ = getelementptr inbounds %"class.draco::BoundingBox", %"class.draco::BoundingBox"* %this1, i32 0, i32 1
  %13 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %max_point_, i32 %13)
  %14 = load float, float* %call8, align 4
  %cmp9 = fcmp ogt float %12, %14
  br i1 %cmp9, label %if.then10, label %if.end14

if.then10:                                        ; preds = %if.end
  %15 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %new_point.addr, align 4
  %16 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %15, i32 %16)
  %17 = load float, float* %call11, align 4
  %max_point_12 = getelementptr inbounds %"class.draco::BoundingBox", %"class.draco::BoundingBox"* %this1, i32 0, i32 1
  %18 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %max_point_12, i32 %18)
  store float %17, float* %call13, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then10, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZN5draco10PointCloudD2Ev(%"class.draco::PointCloud"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.draco::PointCloud"*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  store %"class.draco::PointCloud"* %this1, %"class.draco::PointCloud"** %retval, align 4
  %0 = bitcast %"class.draco::PointCloud"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco10PointCloudE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 0
  %1 = getelementptr inbounds %"class.std::__2::vector.87", %"class.std::__2::vector.87"* %array.begin, i32 5
  br label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %entry
  %arraydestroy.elementPast = phi %"class.std::__2::vector.87"* [ %1, %entry ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %"class.std::__2::vector.87", %"class.std::__2::vector.87"* %arraydestroy.elementPast, i32 -1
  %call = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* %arraydestroy.element) #8
  %arraydestroy.done = icmp eq %"class.std::__2::vector.87"* %arraydestroy.element, %array.begin
  br i1 %arraydestroy.done, label %arraydestroy.done2, label %arraydestroy.body

arraydestroy.done2:                               ; preds = %arraydestroy.body
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call3 = call %"class.std::__2::vector.51"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.51"* %attributes_) #8
  %metadata_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 1
  %call4 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %metadata_) #8
  %2 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %retval, align 4
  ret %"class.draco::PointCloud"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10PointCloudD0Ev(%"class.draco::PointCloud"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %call = call %"class.draco::PointCloud"* @_ZN5draco10PointCloudD2Ev(%"class.draco::PointCloud"* %this1) #8
  %0 = bitcast %"class.draco::PointCloud"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"** %__t1, %"class.draco::GeometryMetadata"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.49"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.49"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.49"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  store %"class.draco::GeometryMetadata"** %__t, %"class.draco::GeometryMetadata"*** %__t.addr, align 4
  %0 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__t.addr, align 4
  ret %"class.draco::GeometryMetadata"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"** %__u, %"class.draco::GeometryMetadata"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  store %"class.draco::GeometryMetadata"* %1, %"class.draco::GeometryMetadata"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.49"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.49"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.49"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.49"* %this, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.49"*, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.49"* %this1 to %"struct.std::__2::default_delete.50"*
  ret %"struct.std::__2::__compressed_pair_elem.49"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.54"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  %__t1.addr = alloca %"class.draco::PointAttribute"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__t1, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %1 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.55"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.55"* %0, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.80"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.80"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.80"* %2)
  ret %"class.std::__2::__compressed_pair.54"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"class.draco::PointAttribute"** %__t, %"class.draco::PointAttribute"*** %__t.addr, align 4
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t.addr, align 4
  ret %"class.draco::PointAttribute"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.55"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.55"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  %__u.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__u, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.55"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.80"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.80"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.80"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.80"* %this, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.80"*, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.80"* %this1 to %"struct.std::__2::default_delete.81"*
  ret %"struct.std::__2::__compressed_pair_elem.80"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_(%"class.draco::IndexTypeVector"* %this, i32 %size, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %size.addr = alloca i32, align 4
  %val.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store %"class.draco::IndexType"* %val, %"class.draco::IndexType"** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %val.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_(%"class.std::__2::vector.68"* %vector_, i32 %0, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_(%"class.std::__2::vector.68"* %this, i32 %__sz, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__sz.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_(%"class.std::__2::vector.68"* %this1, i32 %sub, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %4)
  br label %if.end4

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %__cs, align 4
  %6 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %5, %6
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %7 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %7, i32 0, i32 0
  %8 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %9 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %8, i32 %9
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.68"* %this1, %"class.draco::IndexType"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_(%"class.std::__2::vector.68"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__a = alloca %"class.std::__2::allocator.73"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.68"* %this1, i32 %5, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %7) #8
  store %"class.std::__2::allocator.73"* %call2, %"class.std::__2::allocator.73"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %8 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %8
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.68"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %9 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %9)
  %10 = load i32, i32* %__n.addr, align 4
  %11 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"struct.std::__2::__split_buffer"* %__v, i32 %10, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %11)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.68"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.68"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.68"* %this1, %"class.draco::IndexType"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %1, %"class.draco::IndexType"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.68"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.68"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %4) #8
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType"* %call4, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.68"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.97"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"class.draco::IndexType"* %cond, %"class.draco::IndexType"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store %"class.draco::IndexType"* %add.ptr6, %"class.draco::IndexType"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"struct.std::__2::__split_buffer"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, %"class.draco::IndexType"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_4, align 4
  %call5 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %3) #8
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call3, %"class.draco::IndexType"* %call5, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.68"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* %4, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"class.draco::IndexType"* %13, %"class.draco::IndexType"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.68"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.68"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.68"* %__v, %"class.std::__2::vector.68"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v.addr, align 4
  store %"class.std::__2::vector.68"* %0, %"class.std::__2::vector.68"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.68"* %4 to %"class.std::__2::__vector_base.69"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 1
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__t, %"class.draco::IndexType"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__t.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType"*
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  %4 = bitcast %"class.draco::IndexType"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.73"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.73"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.97"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.73"* %__t2, %"class.std::__2::allocator.73"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.71"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.71"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.98"*
  %5 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* %4, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.73"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.71"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.71"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.71"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::allocator.73"* %__t, %"class.std::__2::allocator.73"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__t.addr, align 4
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* returned %this, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  store %"class.std::__2::allocator.73"* %__u, %"class.std::__2::allocator.73"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.73"* %call, %"class.std::__2::allocator.73"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.98"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.73"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.73"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType"*
  ret %"class.draco::IndexType"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %1) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__value_, align 4
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* returned %this, %"class.draco::IndexType"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.draco::IndexType"** %__p, %"class.draco::IndexType"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %0, align 4
  store %"class.draco::IndexType"* %1, %"class.draco::IndexType"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 %4
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  store %"class.draco::IndexType"** %5, %"class.draco::IndexType"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__dest_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %1, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %0, %"class.std::__2::allocator.73"** %.addr, align 4
  store %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"** %__begin1.addr, align 4
  store %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** %__end1.addr, align 4
  store %"class.draco::IndexType"** %__end2, %"class.draco::IndexType"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 %idx.neg
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %7, align 4
  %9 = bitcast %"class.draco::IndexType"* %8 to i8*
  %10 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType"**, align 4
  %__y.addr = alloca %"class.draco::IndexType"**, align 4
  %__t = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"** %__x, %"class.draco::IndexType"*** %__x.addr, align 4
  store %"class.draco::IndexType"** %__y, %"class.draco::IndexType"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  store %"class.draco::IndexType"* %1, %"class.draco::IndexType"** %__t, align 4
  %2 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call1, align 4
  %4 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__x.addr, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call2, align 4
  %6 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__y.addr, align 4
  store %"class.draco::IndexType"* %5, %"class.draco::IndexType"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.68"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #8
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType"**, align 4
  store %"class.draco::IndexType"** %__t, %"class.draco::IndexType"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__t.addr, align 4
  ret %"class.draco::IndexType"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %0, %"class.draco::IndexType"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.99", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.99", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.68"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %vector_) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.94"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEpLEl(%"class.std::__2::__wrap_iter.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::__wrap_iter.94"* %this, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.94"*, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__i, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %1, i32 %0
  store %"class.std::__2::unique_ptr.53"* %add.ptr, %"class.std::__2::unique_ptr.53"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.94"*, align 4
  store %"class.std::__2::__wrap_iter.94"* %this, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.94"*, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__i, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.100", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.40"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.40"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESC_(%"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  store %"class.std::__2::__wrap_iter.100"* %__x, %"class.std::__2::__wrap_iter.100"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.100"* %__y, %"class.std::__2::__wrap_iter.100"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %__x.addr, align 4
  %1 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EEbRKNS_11__wrap_iterIT_EERKNS8_IT0_EE(%"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %1) #8
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.100", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.40"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.40"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEptEv(%"class.std::__2::__wrap_iter.100"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  store %"class.std::__2::__wrap_iter.100"* %this, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__i, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__29addressofINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_RS7_(%"class.std::__2::unique_ptr.40"* nonnull align 4 dereferenceable(4) %0) #8
  ret %"class.std::__2::unique_ptr.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.40"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_) #8
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  ret %"class.draco::AttributeMetadata"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17AttributeMetadata13att_unique_idEv(%"class.draco::AttributeMetadata"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.draco::AttributeMetadata"* %this, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %att_unique_id_ = getelementptr inbounds %"class.draco::AttributeMetadata", %"class.draco::AttributeMetadata"* %this1, i32 0, i32 1
  %0 = load i32, i32* %att_unique_id_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5eraseENS_11__wrap_iterIPKS6_EE(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.40"* %__position.coerce) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.100", align 4
  %__position = alloca %"class.std::__2::__wrap_iter.101", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__ps = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.101", align 4
  %__p = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %__position, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %__position.coerce, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6cbeginEv(%"class.std::__2::vector"* %this1) #8
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"** %coerce.dive2, align 4
  %call3 = call i32 @_ZNSt3__2miIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES8_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNSA_IT0_EE(%"class.std::__2::__wrap_iter.101"* nonnull align 4 dereferenceable(4) %__position, %"class.std::__2::__wrap_iter.101"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  store i32 %call3, i32* %__ps, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %2 = load i32, i32* %__ps, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %1, i32 %2
  store %"class.std::__2::unique_ptr.40"* %add.ptr, %"class.std::__2::unique_ptr.40"** %__p, align 4
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p, align 4
  %add.ptr4 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %3, i32 1
  %4 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  %5 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  %6 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p, align 4
  %call5 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__24moveIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.40"* %add.ptr4, %"class.std::__2::unique_ptr.40"* %5, %"class.std::__2::unique_ptr.40"* %6)
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.40"* %call5) #8
  %7 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p, align 4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %7, i32 -1
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.40"* %add.ptr6)
  %8 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p, align 4
  %call7 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.40"* %8) #8
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call7, %"class.std::__2::unique_ptr.40"** %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %retval, i32 0, i32 0
  %9 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive9, align 4
  ret %"class.std::__2::unique_ptr.40"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.101"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2IPS6_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleISC_S8_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter.101"* returned %this, %"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.101"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter.101"* %this, %"class.std::__2::__wrap_iter.101"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.100"* %__u, %"class.std::__2::__wrap_iter.100"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.101"*, %"class.std::__2::__wrap_iter.101"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %__u.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.100"* %1) #8
  store %"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.101"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.100"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEppEv(%"class.std::__2::__wrap_iter.100"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  store %"class.std::__2::__wrap_iter.100"* %this, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__i, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %0, i32 1
  store %"class.std::__2::unique_ptr.40"* %incdec.ptr, %"class.std::__2::unique_ptr.40"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.100"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.100", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.100"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES7_(%"class.std::__2::__wrap_iter.100"* %retval, %"class.std::__2::unique_ptr.40"* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  ret %"class.std::__2::unique_ptr.40"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.100"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES7_(%"class.std::__2::__wrap_iter.100"* returned %this, %"class.std::__2::unique_ptr.40"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::__wrap_iter.100"* %this, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__x, %"class.std::__2::unique_ptr.40"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %0, %"class.std::__2::unique_ptr.40"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.100"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EEbRKNS_11__wrap_iterIT_EERKNS8_IT0_EE(%"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.100"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  store %"class.std::__2::__wrap_iter.100"* %__x, %"class.std::__2::__wrap_iter.100"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.100"* %__y, %"class.std::__2::__wrap_iter.100"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %__x.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.100"* %0) #8
  %1 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %__y.addr, align 4
  %call1 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.100"* %1) #8
  %cmp = icmp eq %"class.std::__2::unique_ptr.40"* %call, %call1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.100"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.100"*, align 4
  store %"class.std::__2::__wrap_iter.100"* %this, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.100"*, %"class.std::__2::__wrap_iter.100"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.100", %"class.std::__2::__wrap_iter.100"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__i, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__29addressofINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_RS7_(%"class.std::__2::unique_ptr.40"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__x, %"class.std::__2::unique_ptr.40"** %__x.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__x.addr, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #8
  ret %"class.draco::AttributeMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.42"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES8_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNSA_IT0_EE(%"class.std::__2::__wrap_iter.101"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.101"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter.101"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.101"*, align 4
  store %"class.std::__2::__wrap_iter.101"* %__x, %"class.std::__2::__wrap_iter.101"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.101"* %__y, %"class.std::__2::__wrap_iter.101"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter.101"*, %"class.std::__2::__wrap_iter.101"** %__x.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.101"* %0) #8
  %1 = load %"class.std::__2::__wrap_iter.101"*, %"class.std::__2::__wrap_iter.101"** %__y.addr, align 4
  %call1 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.101"* %1) #8
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %call to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %call1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6cbeginEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.101", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector"* %this1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %retval, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.40"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__new_last, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.40"* %0)
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %1, %"class.std::__2::unique_ptr.40"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__24moveIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.40"* %__first, %"class.std::__2::unique_ptr.40"* %__last, %"class.std::__2::unique_ptr.40"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__last.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__result.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__first, %"class.std::__2::unique_ptr.40"** %__first.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__last, %"class.std::__2::unique_ptr.40"** %__last.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__result, %"class.std::__2::unique_ptr.40"** %__result.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__first.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.40"* %0)
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__last.addr, align 4
  %call1 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.40"* %1)
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__result.addr, align 4
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.40"* %2)
  %call3 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__26__moveIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"* %call1, %"class.std::__2::unique_ptr.40"* %call2)
  ret %"class.std::__2::unique_ptr.40"* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.40"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__new_last, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.101"*, align 4
  store %"class.std::__2::__wrap_iter.101"* %this, %"class.std::__2::__wrap_iter.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.101"*, %"class.std::__2::__wrap_iter.101"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__i, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.101", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.40"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* %call, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.40"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.101", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.101"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES8_(%"class.std::__2::__wrap_iter.101"* %retval, %"class.std::__2::unique_ptr.40"* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %coerce.dive, align 4
  ret %"class.std::__2::unique_ptr.40"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.101"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES8_(%"class.std::__2::__wrap_iter.101"* returned %this, %"class.std::__2::unique_ptr.40"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.101"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::__wrap_iter.101"* %this, %"class.std::__2::__wrap_iter.101"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__x, %"class.std::__2::unique_ptr.40"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.101"*, %"class.std::__2::__wrap_iter.101"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.101", %"class.std::__2::__wrap_iter.101"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %0, %"class.std::__2::unique_ptr.40"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.101"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this, %"class.std::__2::unique_ptr.40"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__new_last, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  store %"class.std::__2::unique_ptr.40"* %0, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.40"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %3, i32 -1
  store %"class.std::__2::unique_ptr.40"* %incdec.ptr, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.40"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.40"* %4, %"class.std::__2::unique_ptr.40"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.40"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call4, i32 %2
  %3 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr5 to i8*
  %call6 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call6, i32 %call7
  %4 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.102", align 1
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.102"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.40"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.44"* %__end_cap_) #8
  ret %"class.std::__2::allocator.47"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.47"* %1, %"class.std::__2::unique_ptr.40"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.47"* %this, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::allocator.47"* %this, %"class.std::__2::allocator.47"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.40"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.40"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.40"* %this1, %"class.draco::AttributeMetadata"* null) #8
  ret %"class.std::__2::unique_ptr.40"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.40"* %this, %"class.draco::AttributeMetadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__p.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  %__tmp = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__p, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_) #8
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  store %"class.draco::AttributeMetadata"* %0, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_2) #8
  store %"class.draco::AttributeMetadata"* %1, %"class.draco::AttributeMetadata"** %call3, align 4
  %2 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeMetadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %__ptr_4) #8
  %3 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::AttributeMetadata"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #8
  ret %"class.draco::AttributeMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.43"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %0) #8
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::AttributeMetadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__ptr, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeMetadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* %0) #8
  %1 = bitcast %"class.draco::AttributeMetadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.42"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.43"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.43"* %this, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.43"*, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.43"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.draco::AttributeMetadata"* %this, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeMetadata"* %this1 to %"class.draco::Metadata"*
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #8
  ret %"class.draco::AttributeMetadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map.17"*, align 4
  store %"class.std::__2::unordered_map.17"* %this, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map.17"*, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map.17", %"class.std::__2::unordered_map.17"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.18"* %__table_) #8
  ret %"class.std::__2::unordered_map.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map"*, align 4
  store %"class.std::__2::unordered_map"* %this, %"class.std::__2::unordered_map"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map"*, %"class.std::__2::unordered_map"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map", %"class.std::__2::unordered_map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* %__table_) #8
  ret %"class.std::__2::unordered_map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.18"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.29"* %__p1_) #8
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.22", %"struct.std::__2::__hash_node_base.22"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.18"* %this1, %"struct.std::__2::__hash_node_base.22"* %0) #8
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.19"* %__bucket_list_) #8
  ret %"class.std::__2::__hash_table.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.18"* %this, %"struct.std::__2::__hash_node_base.22"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  %__na = alloca %"class.std::__2::allocator.32"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__np, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.18"* %this1) #8
  store %"class.std::__2::allocator.32"* %call, %"class.std::__2::allocator.32"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base.22"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.22", %"struct.std::__2::__hash_node_base.22"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base.22"* %2, %"struct.std::__2::__hash_node_base.22"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.22"* %3) #8
  store %"struct.std::__2::__hash_node"* %call2, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node", %"struct.std::__2::__hash_node"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair"* %call3)
  %6 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node"* %7, i32 1) #8
  %8 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next, align 4
  store %"struct.std::__2::__hash_node_base.22"* %8, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret %"struct.std::__2::__hash_node_base.22"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.19"*, align 4
  store %"class.std::__2::unique_ptr.19"* %this, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.19"*, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.19"* %this1, i8* null) #8
  ret %"class.std::__2::unique_ptr.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.29"* %__p1_) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %this, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %this1) #8
  %0 = bitcast %"struct.std::__2::__hash_node_base.22"* %call to %"struct.std::__2::__hash_node"*
  ret %"struct.std::__2::__hash_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.99", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.113", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.113"* %ref.tmp to %"struct.std::__2::integral_constant.99"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %__n, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %0)
  %call1 = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call) #8
  ret %"struct.std::__2::pair"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.32"* %0, %"struct.std::__2::__hash_node"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %0) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__r, %"struct.std::__2::__hash_node_base.22"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %0) #8
  ret %"struct.std::__2::__hash_node_base.22"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__x, %"struct.std::__2::__hash_node_base.22"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base.22"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.99", align 1
  %.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"class.std::__2::allocator.32"* %0, %"class.std::__2::allocator.32"** %.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.108"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.108"* %second) #8
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #8
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.108"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.108"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.108"*, align 4
  store %"class.std::__2::unique_ptr.108"* %this, %"class.std::__2::unique_ptr.108"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.108"*, %"class.std::__2::unique_ptr.108"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.108"* %this1, %"class.draco::Metadata"* null) #8
  ret %"class.std::__2::unique_ptr.108"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.108"* %this, %"class.draco::Metadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.108"*, align 4
  %__p.addr = alloca %"class.draco::Metadata"*, align 4
  %__tmp = alloca %"class.draco::Metadata"*, align 4
  store %"class.std::__2::unique_ptr.108"* %this, %"class.std::__2::unique_ptr.108"** %this.addr, align 4
  store %"class.draco::Metadata"* %__p, %"class.draco::Metadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.108"*, %"class.std::__2::unique_ptr.108"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.108", %"class.std::__2::unique_ptr.108"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.109"* %__ptr_) #8
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %call, align 4
  store %"class.draco::Metadata"* %0, %"class.draco::Metadata"** %__tmp, align 4
  %1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.108", %"class.std::__2::unique_ptr.108"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.109"* %__ptr_2) #8
  store %"class.draco::Metadata"* %1, %"class.draco::Metadata"** %call3, align 4
  %2 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::Metadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.108", %"class.std::__2::unique_ptr.108"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.112"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.109"* %__ptr_4) #8
  %3 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.112"* %call5, %"class.draco::Metadata"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.109"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.109"*, align 4
  store %"class.std::__2::__compressed_pair.109"* %this, %"class.std::__2::__compressed_pair.109"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.109"*, %"class.std::__2::__compressed_pair.109"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.109"* %this1 to %"struct.std::__2::__compressed_pair_elem.110"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.110"* %0) #8
  ret %"class.draco::Metadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.112"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.109"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.109"*, align 4
  store %"class.std::__2::__compressed_pair.109"* %this, %"class.std::__2::__compressed_pair.109"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.109"*, %"class.std::__2::__compressed_pair.109"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.109"* %this1 to %"struct.std::__2::__compressed_pair_elem.111"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.112"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.111"* %0) #8
  ret %"struct.std::__2::default_delete.112"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.112"* %this, %"class.draco::Metadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.112"*, align 4
  %__ptr.addr = alloca %"class.draco::Metadata"*, align 4
  store %"struct.std::__2::default_delete.112"* %this, %"struct.std::__2::default_delete.112"** %this.addr, align 4
  store %"class.draco::Metadata"* %__ptr, %"class.draco::Metadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.112"*, %"struct.std::__2::default_delete.112"** %this.addr, align 4
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::Metadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #8
  %1 = bitcast %"class.draco::Metadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.110"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.110"* %this, %"struct.std::__2::__compressed_pair_elem.110"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.110"*, %"struct.std::__2::__compressed_pair_elem.110"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.110", %"struct.std::__2::__compressed_pair_elem.110"* %this1, i32 0, i32 0
  ret %"class.draco::Metadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.112"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.111"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.111"* %this, %"struct.std::__2::__compressed_pair_elem.111"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.111"*, %"struct.std::__2::__compressed_pair_elem.111"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.111"* %this1 to %"struct.std::__2::default_delete.112"*
  ret %"struct.std::__2::default_delete.112"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Metadata"*, align 4
  store %"class.draco::Metadata"* %this, %"class.draco::Metadata"** %this.addr, align 4
  %this1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %this.addr, align 4
  %sub_metadatas_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.17"* %sub_metadatas_) #8
  %entries_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* %entries_) #8
  ret %"class.draco::Metadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %this, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type", %"struct.std::__2::__hash_value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.32"* %this, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 24
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.22"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.19"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.19"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  store %"class.std::__2::unique_ptr.19"* %this, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.19"*, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %__ptr_) #8
  %1 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %call, align 4
  store %"struct.std::__2::__hash_node_base.22"** %1, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %__ptr_2) #8
  store %"struct.std::__2::__hash_node_base.22"** null, %"struct.std::__2::__hash_node_base.22"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base.22"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.20"* %__ptr_4) #8
  %3 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.24"* %call5, %"struct.std::__2::__hash_node_base.22"** %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.20"*, align 4
  store %"class.std::__2::__compressed_pair.20"* %this, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.20"*, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to %"struct.std::__2::__compressed_pair_elem.21"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %0) #8
  ret %"struct.std::__2::__hash_node_base.22"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.20"*, align 4
  store %"class.std::__2::__compressed_pair.20"* %this, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.20"*, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.23"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %1) #8
  ret %"class.std::__2::__bucket_list_deallocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.24"* %this, %"struct.std::__2::__hash_node_base.22"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.24"* %this1) #8
  %0 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.24"* %this1) #8
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.27"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base.22"** %0, i32 %1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.21"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.21"* %this, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.21"*, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.21"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.22"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.23", %"struct.std::__2::__compressed_pair_elem.23"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator.24"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.27"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base.22"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.27"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.27"* %__a, %"class.std::__2::allocator.27"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.27"*, %"class.std::__2::allocator.27"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.27"* %0, %"struct.std::__2::__hash_node_base.22"** %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.24", %"class.std::__2::__bucket_list_deallocator.24"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.25"* %__data_) #8
  ret %"class.std::__2::allocator.27"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.24", %"class.std::__2::__bucket_list_deallocator.24"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.25"* %__data_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.27"* %this, %"struct.std::__2::__hash_node_base.22"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.27"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.27"* %this, %"class.std::__2::allocator.27"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.27"*, %"class.std::__2::allocator.27"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base.22"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.25"*, align 4
  store %"class.std::__2::__compressed_pair.25"* %this, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.25"*, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.26"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.26"* %0) #8
  ret %"class.std::__2::allocator.27"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.26"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.26"* %this, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.26"*, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.26"* %this1 to %"class.std::__2::allocator.27"*
  ret %"class.std::__2::allocator.27"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.25"*, align 4
  store %"class.std::__2::__compressed_pair.25"* %this, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.25"*, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.7"* %__p1_) #8
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this1, %"struct.std::__2::__hash_node_base"* %0) #8
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr.0"* %__bucket_list_) #8
  ret %"class.std::__2::__hash_table"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this, %"struct.std::__2::__hash_node_base"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__na = alloca %"class.std::__2::allocator.10"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node.114"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"* %__np, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this1) #8
  store %"class.std::__2::allocator.10"* %call, %"class.std::__2::allocator.10"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base"* %2, %"struct.std::__2::__hash_node_base"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node.114"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %3) #8
  store %"struct.std::__2::__hash_node.114"* %call2, %"struct.std::__2::__hash_node.114"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node.114"*, %"struct.std::__2::__hash_node.114"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node.114", %"struct.std::__2::__hash_node.114"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair.116"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.115"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair.116"* %call3)
  %6 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node.114"*, %"struct.std::__2::__hash_node.114"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node.114"* %7, i32 1) #8
  %8 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next, align 4
  store %"struct.std::__2::__hash_node_base"* %8, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #8
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.0"*, align 4
  store %"class.std::__2::unique_ptr.0"* %this, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.0"*, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr.0"* %this1, i8* null) #8
  ret %"class.std::__2::unique_ptr.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.7"* %__p1_) #8
  ret %"class.std::__2::allocator.10"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node.114"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %this, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %this1) #8
  %0 = bitcast %"struct.std::__2::__hash_node_base"* %call to %"struct.std::__2::__hash_node.114"*
  ret %"struct.std::__2::__hash_node.114"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.116"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.116"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.99", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.117", align 1
  store %"class.std::__2::allocator.10"* %__a, %"class.std::__2::allocator.10"** %__a.addr, align 4
  store %"struct.std::__2::pair.116"* %__p, %"struct.std::__2::pair.116"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.117"* %ref.tmp to %"struct.std::__2::integral_constant.99"*
  %1 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.116"*, %"struct.std::__2::pair.116"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.116"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.116"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.115"* nonnull align 4 dereferenceable(24) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type.115"*, align 4
  store %"struct.std::__2::__hash_value_type.115"* %__n, %"struct.std::__2::__hash_value_type.115"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type.115"*, %"struct.std::__2::__hash_value_type.115"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.116"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.115"* %0)
  %call1 = call %"struct.std::__2::pair.116"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.116"* nonnull align 4 dereferenceable(24) %call) #8
  ret %"struct.std::__2::pair.116"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node.114"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.114"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.10"* %__a, %"class.std::__2::allocator.10"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node.114"* %__p, %"struct.std::__2::__hash_node.114"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node.114"*, %"struct.std::__2::__hash_node.114"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.10"* %0, %"struct.std::__2::__hash_node.114"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.9"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %0) #8
  ret %"class.std::__2::allocator.10"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.9"* %this1 to %"class.std::__2::allocator.10"*
  ret %"class.std::__2::allocator.10"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__r, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %0) #8
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__x, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair.116"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.99", align 1
  %.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.116"*, align 4
  store %"class.std::__2::allocator.10"* %0, %"class.std::__2::allocator.10"** %.addr, align 4
  store %"struct.std::__2::pair.116"* %__p, %"struct.std::__2::pair.116"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair.116"*, %"struct.std::__2::pair.116"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair.116"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.116"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.116"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.116"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.116"*, align 4
  store %"struct.std::__2::pair.116"* %this, %"struct.std::__2::pair.116"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair.116"*, %"struct.std::__2::pair.116"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.116", %"struct.std::__2::pair.116"* %this1, i32 0, i32 1
  %call = call %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* %second) #8
  %first = getelementptr inbounds %"struct.std::__2::pair.116", %"struct.std::__2::pair.116"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #8
  ret %"struct.std::__2::pair.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::EntryValue"*, align 4
  store %"class.draco::EntryValue"* %this, %"class.draco::EntryValue"** %this.addr, align 4
  %this1 = load %"class.draco::EntryValue"*, %"class.draco::EntryValue"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::EntryValue", %"class.draco::EntryValue"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* %data_) #8
  ret %"class.draco::EntryValue"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* %0) #8
  ret %"class.std::__2::vector.56"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.57"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store %"class.std::__2::__vector_base.57"* %this1, %"class.std::__2::__vector_base.57"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %retval, align 4
  ret %"class.std::__2::__vector_base.57"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.59"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.59"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.59"* %this, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.59"*, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.59", %"struct.std::__2::__compressed_pair_elem.59"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.118", align 1
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.118"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.60"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %0) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.60"* %this1 to %"class.std::__2::allocator.61"*
  ret %"class.std::__2::allocator.61"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.116"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.116"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair.116"*, align 4
  store %"struct.std::__2::pair.116"* %__x, %"struct.std::__2::pair.116"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair.116"*, %"struct.std::__2::pair.116"** %__x.addr, align 4
  ret %"struct.std::__2::pair.116"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.116"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type.115"*, align 4
  store %"struct.std::__2::__hash_value_type.115"* %this, %"struct.std::__2::__hash_value_type.115"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type.115"*, %"struct.std::__2::__hash_value_type.115"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type.115", %"struct.std::__2::__hash_value_type.115"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.116"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.10"* %this, %"struct.std::__2::__hash_node.114"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.114"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.10"* %this, %"class.std::__2::allocator.10"** %this.addr, align 4
  store %"struct.std::__2::__hash_node.114"* %__p, %"struct.std::__2::__hash_node.114"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node.114"*, %"struct.std::__2::__hash_node.114"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node.114"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 32
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.8"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr.0"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.0"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::unique_ptr.0"* %this, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.0"*, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #8
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %call, align 4
  store %"struct.std::__2::__hash_node_base"** %1, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #8
  store %"struct.std::__2::__hash_node_base"** null, %"struct.std::__2::__hash_node_base"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #8
  %3 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %call5, %"struct.std::__2::__hash_node_base"** %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #8
  ret %"struct.std::__2::__hash_node_base"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %1) #8
  ret %"class.std::__2::__bucket_list_deallocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %this, %"struct.std::__2::__hash_node_base"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #8
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #8
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base"** %0, i32 %1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %0, %"struct.std::__2::__hash_node_base"** %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.4"* %__data_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.4"* %__data_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %this, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.6"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.44"*, align 4
  store %"class.std::__2::__compressed_pair.44"* %this, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.44"*, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.46"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.46"* %0) #8
  ret %"class.std::__2::allocator.47"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.46"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.46"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.46"* %this, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.46"*, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.46"* %this1 to %"class.std::__2::allocator.47"*
  ret %"class.std::__2::allocator.47"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %1) #8
  ret %"class.std::__2::unique_ptr.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.44"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.40"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.44"*, align 4
  store %"class.std::__2::__compressed_pair.44"* %this, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.44"*, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.45"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.45"* %0) #8
  ret %"class.std::__2::unique_ptr.40"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.45"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.45"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.45"* %this, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.45"*, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.45", %"struct.std::__2::__compressed_pair_elem.45"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.40"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__26__moveIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.40"* %__first, %"class.std::__2::unique_ptr.40"* %__last, %"class.std::__2::unique_ptr.40"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__last.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__result.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__first, %"class.std::__2::unique_ptr.40"** %__first.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__last, %"class.std::__2::unique_ptr.40"** %__last.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__result, %"class.std::__2::unique_ptr.40"** %__result.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__first.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__last.addr, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.40"* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__first.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.40"* nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__result.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.40"* %3, %"class.std::__2::unique_ptr.40"* nonnull align 4 dereferenceable(4) %call) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %4, i32 1
  store %"class.std::__2::unique_ptr.40"* %incdec.ptr, %"class.std::__2::unique_ptr.40"** %__first.addr, align 4
  %5 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__result.addr, align 4
  %incdec.ptr2 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %5, i32 1
  store %"class.std::__2::unique_ptr.40"* %incdec.ptr2, %"class.std::__2::unique_ptr.40"** %__result.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__result.addr, align 4
  ret %"class.std::__2::unique_ptr.40"* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.40"* %__i) #0 comdat {
entry:
  %__i.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__i, %"class.std::__2::unique_ptr.40"** %__i.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__i.addr, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.40"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__t, %"class.std::__2::unique_ptr.40"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__u, %"class.std::__2::unique_ptr.40"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__u.addr, align 4
  %call = call %"class.draco::AttributeMetadata"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.40"* %0) #8
  call void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.40"* %this1, %"class.draco::AttributeMetadata"* %call) #8
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.40"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %call2) #8
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %__ptr_) #8
  ret %"class.std::__2::unique_ptr.40"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.40"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__t = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_) #8
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  store %"class.draco::AttributeMetadata"* %0, %"class.draco::AttributeMetadata"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_2) #8
  store %"class.draco::AttributeMetadata"* null, %"class.draco::AttributeMetadata"** %call3, align 4
  %1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__t, align 4
  ret %"class.draco::AttributeMetadata"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete"*, align 4
  store %"struct.std::__2::default_delete"* %__t, %"struct.std::__2::default_delete"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.40"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %__ptr_) #8
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqIPiS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE(%"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.95"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  store %"class.std::__2::__wrap_iter.95"* %__x, %"class.std::__2::__wrap_iter.95"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.95"* %__y, %"class.std::__2::__wrap_iter.95"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %__x.addr, align 4
  %call = call i32* @_ZNKSt3__211__wrap_iterIPiE4baseEv(%"class.std::__2::__wrap_iter.95"* %0) #8
  %1 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %__y.addr, align 4
  %call1 = call i32* @_ZNKSt3__211__wrap_iterIPiE4baseEv(%"class.std::__2::__wrap_iter.95"* %1) #8
  %cmp = icmp eq i32* %call, %call1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__211__wrap_iterIPiE4baseEv(%"class.std::__2::__wrap_iter.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  store %"class.std::__2::__wrap_iter.95"* %this, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNSt3__223__libcpp_numeric_limitsIfLb1EE3maxEv() #0 comdat {
entry:
  ret float 0x47EFFFFFE0000000
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %out_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %out_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %out_data, i8** %out_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_data.addr, align 4
  %call = call i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %1 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %add.ptr, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %data_) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this, i32 %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %i.addr = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %v_, i32 %0) #8
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 %0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* %0) #8
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.51"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.51"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.51"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call %"class.std::__2::__vector_base.52"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.52"* %0) #8
  ret %"class.std::__2::vector.51"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::GeometryMetadata"* null) #8
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.88"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store %"class.std::__2::__vector_base.88"* %this1, %"class.std::__2::__vector_base.88"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %retval, align 4
  ret %"class.std::__2::__vector_base.88"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.119", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.119"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.53"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.51"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.52"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.52"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.52"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  store %"class.std::__2::__vector_base.52"* %this1, %"class.std::__2::__vector_base.52"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.52"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.53"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %retval, align 4
  ret %"class.std::__2::__vector_base.52"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.51"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %1) #8
  ret %"class.std::__2::unique_ptr.53"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.52"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.82"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.82"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.82"*, align 4
  store %"class.std::__2::__compressed_pair.82"* %this, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.82"*, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.83"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %0) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.83"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.83"* %this, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.83"*, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.83", %"struct.std::__2::__compressed_pair_elem.83"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.53"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.52"* %this1, %"class.std::__2::unique_ptr.53"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.85"* %0, %"class.std::__2::unique_ptr.53"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.82"* %__end_cap_) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.52"* %this, %"class.std::__2::unique_ptr.53"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__new_last, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  store %"class.std::__2::unique_ptr.53"* %0, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %3, i32 -1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.53"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.53"* %4, %"class.std::__2::unique_ptr.53"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.120", align 1
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.120"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.53"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.85"* %1, %"class.std::__2::unique_ptr.53"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.85"* %this, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.85"* %this, %"class.std::__2::unique_ptr.53"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.82"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.82"*, align 4
  store %"class.std::__2::__compressed_pair.82"* %this, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.82"*, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.84"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.84"* %0) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.84"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.84"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.84"* %this, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.84"*, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.84"* %this1 to %"class.std::__2::allocator.85"*
  ret %"class.std::__2::allocator.85"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::GeometryMetadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  %__tmp = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"* %__p, %"class.draco::GeometryMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #8
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  store %"class.draco::GeometryMetadata"* %0, %"class.draco::GeometryMetadata"** %__tmp, align 4
  %1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #8
  store %"class.draco::GeometryMetadata"* %1, %"class.draco::GeometryMetadata"** %call3, align 4
  %2 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::GeometryMetadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_4) #8
  %3 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_(%"struct.std::__2::default_delete.50"* %call5, %"class.draco::GeometryMetadata"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret %"class.draco::GeometryMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.49"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.49"* %0) #8
  ret %"struct.std::__2::default_delete.50"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_(%"struct.std::__2::default_delete.50"* %this, %"class.draco::GeometryMetadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.50"*, align 4
  %__ptr.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"struct.std::__2::default_delete.50"* %this, %"struct.std::__2::default_delete.50"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"* %__ptr, %"class.draco::GeometryMetadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.50"*, %"struct.std::__2::default_delete.50"** %this.addr, align 4
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::GeometryMetadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataD2Ev(%"class.draco::GeometryMetadata"* %0) #8
  %1 = bitcast %"class.draco::GeometryMetadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.draco::GeometryMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.49"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.49"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.49"* %this, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.49"*, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.49"* %this1 to %"struct.std::__2::default_delete.50"*
  ret %"struct.std::__2::default_delete.50"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataD2Ev(%"class.draco::GeometryMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.draco::GeometryMetadata"* %this, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %att_metadatas_ = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector"* %att_metadatas_) #8
  %0 = bitcast %"class.draco::GeometryMetadata"* %this1 to %"class.draco::Metadata"*
  %call2 = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #8
  ret %"class.draco::GeometryMetadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.40"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.40"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.40"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this1, %"class.std::__2::unique_ptr.40"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.47"* %0, %"class.std::__2::unique_ptr.40"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.47"* %this, %"class.std::__2::unique_ptr.40"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.47"* %this, %"class.std::__2::allocator.47"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.40"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 %0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret %"class.draco::GeometryMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.draco::GeometryMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.52"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.52"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.52"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* null, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.53"* null, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.82"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.82"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.52"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.82"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.82"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.82"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.82"* %this, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.82"*, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.83"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.83"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.83"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.84"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.84"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.84"* %2)
  ret %"class.std::__2::__compressed_pair.82"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.83"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.83"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.83"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.83"* %this, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.83"*, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.83", %"struct.std::__2::__compressed_pair_elem.83"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.std::__2::unique_ptr.53"* null, %"class.std::__2::unique_ptr.53"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.83"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.84"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.84"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.84"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.84"* %this, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.84"*, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.84"* %this1 to %"class.std::__2::allocator.85"*
  %call = call %"class.std::__2::allocator.85"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.85"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.84"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.85"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.85"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  ret %"class.std::__2::allocator.85"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.88"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.89"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.89"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.88"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.89"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.89"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.90"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.90"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.91"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.91"* %2)
  ret %"class.std::__2::__compressed_pair.89"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.90"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.90"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.90"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.91"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.91"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  %call = call %"class.std::__2::allocator.92"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.92"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.91"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.92"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.92"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret %"class.std::__2::allocator.92"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__t = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_2) #8
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %call3, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__t, align 4
  ret %"class.draco::PointAttribute"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  store %"struct.std::__2::default_delete.81"* %__t, %"struct.std::__2::default_delete.81"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.81"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  ret %"struct.std::__2::default_delete.81"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.54"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  %__t1.addr = alloca %"class.draco::PointAttribute"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__t1, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.81"* %__t2, %"struct.std::__2::default_delete.81"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %1 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.55"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.55"* %0, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.80"*
  %3 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.80"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.80"* %2, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.54"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.80"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.80"* %0) #8
  ret %"struct.std::__2::default_delete.81"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.80"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.80"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.80"* %this, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.80"*, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.80"* %this1 to %"struct.std::__2::default_delete.81"*
  ret %"struct.std::__2::default_delete.81"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.80"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.80"* returned %this, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.80"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.80"* %this, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  store %"struct.std::__2::default_delete.81"* %__u, %"struct.std::__2::default_delete.81"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.80"*, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.80"* %this1 to %"struct.std::__2::default_delete.81"*
  %1 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.80"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.53"* %this, %"class.draco::PointAttribute"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %__tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__tmp, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_2) #8
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %call3, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointAttribute"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %__ptr_4) #8
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.81"* %call5, %"class.draco::PointAttribute"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.81"* %this, %"class.draco::PointAttribute"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  %__ptr.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"struct.std::__2::default_delete.81"* %this, %"struct.std::__2::default_delete.81"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__ptr, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointAttribute"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* %0) #8
  %1 = bitcast %"class.draco::PointAttribute"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.std::__2::unique_ptr.75"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.75"* %attribute_transform_data_) #8
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* %indices_map_) #8
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::unique_ptr.63"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.63"* %attribute_buffer_) #8
  ret %"class.draco::PointAttribute"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.75"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.75"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.75"*, align 4
  store %"class.std::__2::unique_ptr.75"* %this, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.75"*, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.75"* %this1, %"class.draco::AttributeTransformData"* null) #8
  ret %"class.std::__2::unique_ptr.75"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* %vector_) #8
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.63"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.63"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.63"* %this1, %"class.draco::DataBuffer"* null) #8
  ret %"class.std::__2::unique_ptr.63"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.75"* %this, %"class.draco::AttributeTransformData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.75"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %__tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.75"* %this, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.75"*, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %__ptr_) #8
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %__ptr_2) #8
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %call3, align 4
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.76"* %__ptr_4) #8
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.79"* %call5, %"class.draco::AttributeTransformData"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.76"*, align 4
  store %"class.std::__2::__compressed_pair.76"* %this, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.76"*, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.76"* %this1 to %"struct.std::__2::__compressed_pair_elem.77"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.77"* %0) #8
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.76"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.76"*, align 4
  store %"class.std::__2::__compressed_pair.76"* %this, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.76"*, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.76"* %this1 to %"struct.std::__2::__compressed_pair_elem.78"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.78"* %0) #8
  ret %"struct.std::__2::default_delete.79"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.79"* %this, %"class.draco::AttributeTransformData"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.79"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"struct.std::__2::default_delete.79"* %this, %"struct.std::__2::default_delete.79"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__ptr, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.79"*, %"struct.std::__2::default_delete.79"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeTransformData"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* %0) #8
  %1 = bitcast %"class.draco::AttributeTransformData"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.77"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.77"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.77"* %this, %"struct.std::__2::__compressed_pair_elem.77"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.77"*, %"struct.std::__2::__compressed_pair_elem.77"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.77", %"struct.std::__2::__compressed_pair_elem.77"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.78"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.78"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.78"* %this, %"struct.std::__2::__compressed_pair_elem.78"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.78"*, %"struct.std::__2::__compressed_pair_elem.78"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.78"* %this1 to %"struct.std::__2::default_delete.79"*
  ret %"struct.std::__2::default_delete.79"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %buffer_) #8
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* %data_) #8
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* %0) #8
  ret %"class.std::__2::vector.68"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.69"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.std::__2::__vector_base.69"* %this1, %"class.std::__2::__vector_base.69"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %retval, align 4
  ret %"class.std::__2::__vector_base.69"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.63"* %this, %"class.draco::DataBuffer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %__tmp = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_) #8
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__tmp, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_2) #8
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %call3, align 4
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.64"* %__ptr_4) #8
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete.67"* %call5, %"class.draco::DataBuffer"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.65"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %0) #8
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.66"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %0) #8
  ret %"struct.std::__2::default_delete.67"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete.67"* %this, %"class.draco::DataBuffer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.67"*, align 4
  %__ptr.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"struct.std::__2::default_delete.67"* %this, %"struct.std::__2::default_delete.67"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__ptr, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.67"*, %"struct.std::__2::default_delete.67"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %0) #8
  %1 = bitcast %"class.draco::DataBuffer"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.65"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.65"* %this, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.65"*, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.65", %"struct.std::__2::__compressed_pair_elem.65"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.66"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.66"* %this, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.66"*, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.66"* %this1 to %"struct.std::__2::default_delete.67"*
  ret %"struct.std::__2::default_delete.67"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.54"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  %__t1.addr = alloca %"class.draco::PointAttribute"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__t1, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %1 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.55"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.55"* %0, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.80"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.80"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.80"* %2)
  ret %"class.std::__2::__compressed_pair.54"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"class.draco::PointAttribute"** %__t, %"class.draco::PointAttribute"*** %__t.addr, align 4
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t.addr, align 4
  ret %"class.draco::PointAttribute"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.55"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.55"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  %__u.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__u, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.55"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.51"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.85"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.121", align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.52"* %0) #8
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.51"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %6) #8
  store %"class.std::__2::allocator.85"* %call2, %"class.std::__2::allocator.85"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.51"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  %8 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.121"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer.121"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.121"* %__v, i32 %9)
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.51"* %this1, %"struct.std::__2::__split_buffer.121"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.121"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer.121"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.51"* %this, %"class.std::__2::unique_ptr.53"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__new_last, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.51"* %this1, %"class.std::__2::unique_ptr.53"* %0)
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.52"* %1, %"class.std::__2::unique_ptr.53"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.51"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.82"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.51"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.51"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_3, align 4
  %call4 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call2, %"class.std::__2::unique_ptr.53"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %5, i32 1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr, %"class.std::__2::unique_ptr.53"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.51"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.51"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.121"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer.121"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.121"* %this1, %"struct.std::__2::__split_buffer.121"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.121"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.122"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.122"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.121"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.std::__2::unique_ptr.53"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %cond, %"class.std::__2::unique_ptr.53"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 2
  store %"class.std::__2::unique_ptr.53"* %add.ptr, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.53"* %add.ptr, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 0
  %6 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.121"* %this1) #8
  store %"class.std::__2::unique_ptr.53"* %add.ptr6, %"class.std::__2::unique_ptr.53"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.121"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.121"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %__tx, %"class.std::__2::unique_ptr.53"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_2, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.121"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_4, align 4
  %call5 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call3, %"class.std::__2::unique_ptr.53"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %4, i32 1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr, %"class.std::__2::unique_ptr.53"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.51"* %this, %"struct.std::__2::__split_buffer.121"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.121"* %__v, %"struct.std::__2::__split_buffer.121"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.51"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %0) #8
  %1 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.53"* %2, %"class.std::__2::unique_ptr.53"* %4, %"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__end_5, %"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.52"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.121"* %11) #8
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %call7, %"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %12, i32 0, i32 1
  %13 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %14, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %13, %"class.std::__2::unique_ptr.53"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.51"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.51"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.121"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer.121"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.121"* %this1, %"struct.std::__2::__split_buffer.121"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer.121"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first_, align 4
  %tobool = icmp ne %"class.std::__2::unique_ptr.53"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.121"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer.121"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.53"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.121"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.82"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.82"*, align 4
  store %"class.std::__2::__compressed_pair.82"* %this, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.82"*, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.83"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %0) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.83"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.83"* %this, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.83"*, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.83", %"struct.std::__2::__compressed_pair_elem.83"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.53"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.51"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.51"* %__v, %"class.std::__2::vector.51"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %__v.addr, align 4
  store %"class.std::__2::vector.51"* %0, %"class.std::__2::vector.51"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.51"* %1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  store %"class.std::__2::unique_ptr.53"* %3, %"class.std::__2::unique_ptr.53"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.51"* %4 to %"class.std::__2::__vector_base.52"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %5, i32 0, i32 1
  %6 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %6, i32 %7
  store %"class.std::__2::unique_ptr.53"* %add.ptr, %"class.std::__2::unique_ptr.53"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.124", align 1
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.124"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.53"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.51"* %1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %2, i32 0, i32 1
  store %"class.std::__2::unique_ptr.53"* %0, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.85"* %1, %"class.std::__2::unique_ptr.53"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.85"* %this, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.53"*
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.53"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.53"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %ref.tmp = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.54"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.125", align 1
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.125"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.82"* %__end_cap_) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.85"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.85"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.82"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.82"*, align 4
  store %"class.std::__2::__compressed_pair.82"* %this, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.82"*, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.84"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.84"* %0) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.84"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.84"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.84"* %this, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.84"*, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.84"* %this1 to %"class.std::__2::allocator.85"*
  ret %"class.std::__2::allocator.85"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.122"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.122"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.122"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.85"*, align 4
  store %"class.std::__2::__compressed_pair.122"* %this, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.85"* %__t2, %"class.std::__2::allocator.85"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.122"*, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.122"* %this1 to %"struct.std::__2::__compressed_pair_elem.83"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.83"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.83"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.122"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.123"*
  %5 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.123"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.123"* %4, %"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.85"* %0, i32 %1, i8* null)
  ret %"class.std::__2::unique_ptr.53"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.122"* %__end_cap_) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.122"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.85"*, align 4
  store %"class.std::__2::allocator.85"* %__t, %"class.std::__2::allocator.85"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__t.addr, align 4
  ret %"class.std::__2::allocator.85"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.123"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.123"* returned %this, %"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.123"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.85"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.123"* %this, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  store %"class.std::__2::allocator.85"* %__u, %"class.std::__2::allocator.85"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.123"*, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.123", %"struct.std::__2::__compressed_pair_elem.123"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.85"* %call, %"class.std::__2::allocator.85"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.123"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.85"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.85"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::unique_ptr.53"*
  ret %"class.std::__2::unique_ptr.53"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.122"*, align 4
  store %"class.std::__2::__compressed_pair.122"* %this, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.122"*, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.122"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.123"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %1) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.123"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.123"* %this, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.123"*, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.123", %"struct.std::__2::__compressed_pair_elem.123"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__value_, align 4
  ret %"class.std::__2::allocator.85"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.122"*, align 4
  store %"class.std::__2::__compressed_pair.122"* %this, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.122"*, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.122"* %this1 to %"struct.std::__2::__compressed_pair_elem.83"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %0) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* returned %this, %"class.std::__2::unique_ptr.53"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"** %__p, %"class.std::__2::unique_ptr.53"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__p.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %0, align 4
  store %"class.std::__2::unique_ptr.53"* %1, %"class.std::__2::unique_ptr.53"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %3, i32 %4
  store %"class.std::__2::unique_ptr.53"* %add.ptr, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.53"** %5, %"class.std::__2::unique_ptr.53"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__dest_, align 4
  store %"class.std::__2::unique_ptr.53"* %0, %"class.std::__2::unique_ptr.53"** %1, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>, std::__2::allocator<std::__2::unique_ptr<draco::PointAttribute, std::__2::default_delete<draco::PointAttribute>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__begin1, %"class.std::__2::unique_ptr.53"* %__end1, %"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__begin1.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__end1.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__end2.addr = alloca %"class.std::__2::unique_ptr.53"**, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__begin1, %"class.std::__2::unique_ptr.53"** %__begin1.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__end1, %"class.std::__2::unique_ptr.53"** %__end1.addr, align 4
  store %"class.std::__2::unique_ptr.53"** %__end2, %"class.std::__2::unique_ptr.53"*** %__end2.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end1.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin1.addr, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__end2.addr, align 4
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %3, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %4, i32 -1
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %add.ptr) #8
  %5 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end1.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %5, i32 -1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr, %"class.std::__2::unique_ptr.53"** %__end1.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %2, %"class.std::__2::unique_ptr.53"* %call, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call1)
  %6 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__end2.addr, align 4
  %7 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %6, align 4
  %incdec.ptr2 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %7, i32 -1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr2, %"class.std::__2::unique_ptr.53"** %6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::unique_ptr.53"**, align 4
  %__y.addr = alloca %"class.std::__2::unique_ptr.53"**, align 4
  %__t = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"** %__x, %"class.std::__2::unique_ptr.53"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.53"** %__y, %"class.std::__2::unique_ptr.53"*** %__y.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %call, align 4
  store %"class.std::__2::unique_ptr.53"* %1, %"class.std::__2::unique_ptr.53"** %__t, align 4
  %2 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %call1, align 4
  %4 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %3, %"class.std::__2::unique_ptr.53"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %call2, align 4
  %6 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__y.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %5, %"class.std::__2::unique_ptr.53"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.51"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.53"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call7, i32 %3
  %4 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.51"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.126", align 1
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__args, %"class.std::__2::unique_ptr.53"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.126"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.53"* %2, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__args, %"class.std::__2::unique_ptr.53"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.85"* %1, %"class.std::__2::unique_ptr.53"* %2, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__t, %"class.std::__2::unique_ptr.53"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.85"* %this, %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__args, %"class.std::__2::unique_ptr.53"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.53"*
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %3) #8
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.53"* %2, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.53"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.53"**, align 4
  store %"class.std::__2::unique_ptr.53"** %__t, %"class.std::__2::unique_ptr.53"*** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"**, %"class.std::__2::unique_ptr.53"*** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.53"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer.121"* %this1, %"class.std::__2::unique_ptr.53"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.121"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer.121"* %this, %"class.std::__2::unique_ptr.53"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.99", align 1
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__new_last, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.121"* %this1, %"class.std::__2::unique_ptr.53"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.121"* %this, %"class.std::__2::unique_ptr.53"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.99", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__new_last, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.121"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 2
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %3, i32 -1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr, %"class.std::__2::unique_ptr.53"** %__end_2, align 4
  %call3 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.53"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.121"*, align 4
  store %"struct.std::__2::__split_buffer.121"* %this, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.121"*, %"struct.std::__2::__split_buffer.121"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.121", %"struct.std::__2::__split_buffer.121"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.122"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.122"*, align 4
  store %"class.std::__2::__compressed_pair.122"* %this, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.122"*, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.122"* %this1 to %"struct.std::__2::__compressed_pair_elem.83"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %0) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.51"* %this, %"class.std::__2::unique_ptr.53"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__new_last, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.51"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.53"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call4, i32 %2
  %3 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr5 to i8*
  %call6 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call6, i32 %call7
  %4 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.51"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE22__construct_one_at_endIJRKiEEEvDpOT_(%"class.std::__2::vector.87"* %this, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__args.addr = alloca i32*, align 4
  %__tx = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.87"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %0) #8
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #8
  %2 = load i32*, i32** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %2) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call2, i32* %call3, i32* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIRKiEEvOT_(%"class.std::__2::vector.87"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__x.addr = alloca i32*, align 4
  %__a = alloca %"class.std::__2::allocator.92"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.128", align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %0) #8
  store %"class.std::__2::allocator.92"* %call, %"class.std::__2::allocator.92"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector.87"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer.128"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %__v, i32 0, i32 2
  %3 = load i32*, i32** %__end_, align 4
  %call6 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %3) #8
  %4 = load i32*, i32** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %2, i32* %call6, i32* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %__v, i32 0, i32 2
  %5 = load i32*, i32** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__end_8, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector.87"* %this1, %"struct.std::__2::__split_buffer.128"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer.128"* %__v) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.87"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.87"* %__v, %"class.std::__2::vector.87"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v.addr, align 4
  store %"class.std::__2::vector.87"* %0, %"class.std::__2::vector.87"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.87"* %4 to %"class.std::__2::__vector_base.88"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.127", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.127"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJRKiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJRKiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIiE9constructIiJRKiEEEvPT_DpOT0_(%"class.std::__2::allocator.92"* %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE9constructIiJRKiEEEvPT_DpOT0_(%"class.std::__2::allocator.92"* %this, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #8
  %4 = load i32, i32* %call, align 4
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector.87"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector.87"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer.128"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.128"* %this1, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.128"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.129"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.129"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this1) #8
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.128"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector.87"* %this, %"struct.std::__2::__split_buffer.128"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.128"* %__v, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %0) #8
  %1 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %11) #8
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector.87"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.87"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer.128"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.128"* %this1, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer.128"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer.128"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.128"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.131", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.131"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.92"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.92"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.129"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.129"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.92"* %__t2, %"class.std::__2::allocator.92"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.90"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.90"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.130"*
  %5 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.130"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.130"* %4, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.129"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator.92"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.129"* %__end_cap_) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %__t, %"class.std::__2::allocator.92"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__t.addr, align 4
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.130"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.130"* returned %this, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.130"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.130"* %this, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  store %"class.std::__2::allocator.92"* %__u, %"class.std::__2::allocator.92"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.130"*, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.130", %"struct.std::__2::__compressed_pair_elem.130"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.92"* %call, %"class.std::__2::allocator.92"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.130"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator.92"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.92"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.130"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.130"* %1) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.130"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.130"* %this, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.130"*, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.130", %"struct.std::__2::__compressed_pair_elem.130"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__value_, align 4
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %0, %"class.std::__2::allocator.92"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector.87"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer.128"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer.128"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.99", align 1
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.128"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.128"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.99", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector.51"* %this, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.94", align 4
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.94"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2ES7_(%"class.std::__2::__wrap_iter.94"* %retval, %"class.std::__2::unique_ptr.53"* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  ret %"class.std::__2::unique_ptr.53"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.94"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2ES7_(%"class.std::__2::__wrap_iter.94"* returned %this, %"class.std::__2::unique_ptr.53"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.94"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::__wrap_iter.94"* %this, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__x, %"class.std::__2::unique_ptr.53"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.94"*, %"class.std::__2::__wrap_iter.94"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.94", %"class.std::__2::__wrap_iter.94"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %0, %"class.std::__2::unique_ptr.53"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES8_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNSA_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %0) #8
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call1 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %1) #8
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %call to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %call1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6cbeginEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector.51"* %this1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.53"* %__first, %"class.std::__2::unique_ptr.53"* %__last, %"class.std::__2::unique_ptr.53"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__result.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__first, %"class.std::__2::unique_ptr.53"** %__first.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__last, %"class.std::__2::unique_ptr.53"** %__last.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__result, %"class.std::__2::unique_ptr.53"** %__result.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.53"* %0)
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__last.addr, align 4
  %call1 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.53"* %1)
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__result.addr, align 4
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.53"* %2)
  %call3 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__26__moveIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.53"* %call, %"class.std::__2::unique_ptr.53"* %call1, %"class.std::__2::unique_ptr.53"* %call2)
  ret %"class.std::__2::unique_ptr.53"* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__i, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_(%"class.std::__2::vector.51"* %this1, %"class.std::__2::unique_ptr.53"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.53"* %call, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.53"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_(%"class.std::__2::vector.51"* %this, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2ES8_(%"class.std::__2::__wrap_iter"* %retval, %"class.std::__2::unique_ptr.53"* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %coerce.dive, align 4
  ret %"class.std::__2::unique_ptr.53"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEC2ES8_(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::unique_ptr.53"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__x, %"class.std::__2::unique_ptr.53"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %0, %"class.std::__2::unique_ptr.53"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__26__moveIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEES7_EET0_T_S9_S8_(%"class.std::__2::unique_ptr.53"* %__first, %"class.std::__2::unique_ptr.53"* %__last, %"class.std::__2::unique_ptr.53"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__result.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__first, %"class.std::__2::unique_ptr.53"** %__first.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__last, %"class.std::__2::unique_ptr.53"** %__last.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__result, %"class.std::__2::unique_ptr.53"** %__result.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__last.addr, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__result.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.53"* %3, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %4, i32 1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr, %"class.std::__2::unique_ptr.53"** %__first.addr, align 4
  %5 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__result.addr, align 4
  %incdec.ptr2 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %5, i32 1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr2, %"class.std::__2::unique_ptr.53"** %__result.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__result.addr, align 4
  ret %"class.std::__2::unique_ptr.53"* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__213__unwrap_iterIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEET_S8_(%"class.std::__2::unique_ptr.53"* %__i) #0 comdat {
entry:
  %__i.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__i, %"class.std::__2::unique_ptr.53"** %__i.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__i.addr, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPi(%"class.std::__2::vector.87"* %this, i32* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.95", align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.95"* @_ZNSt3__211__wrap_iterIPiEC2ES1_(%"class.std::__2::__wrap_iter.95"* %retval, i32* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %retval, i32 0, i32 0
  %1 = load i32*, i32** %coerce.dive, align 4
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.95"* @_ZNSt3__211__wrap_iterIPiEC2ES1_(%"class.std::__2::__wrap_iter.95"* returned %this, i32* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::__wrap_iter.95"* %this, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__x.addr, align 4
  store i32* %0, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter.95"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPiEdeEv(%"class.std::__2::__wrap_iter.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  store %"class.std::__2::__wrap_iter.95"* %this, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.95"* @_ZNSt3__211__wrap_iterIPiEppEv(%"class.std::__2::__wrap_iter.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.95"*, align 4
  store %"class.std::__2::__wrap_iter.95"* %this, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.95"*, %"class.std::__2::__wrap_iter.95"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.95", %"class.std::__2::__wrap_iter.95"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %0, i32 1
  store i32* %incdec.ptr, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter.95"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miIPKiS2_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS4_IT0_EE(%"class.std::__2::__wrap_iter.96"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.96"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter.96"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.96"*, align 4
  store %"class.std::__2::__wrap_iter.96"* %__x, %"class.std::__2::__wrap_iter.96"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.96"* %__y, %"class.std::__2::__wrap_iter.96"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter.96"*, %"class.std::__2::__wrap_iter.96"** %__x.addr, align 4
  %call = call i32* @_ZNKSt3__211__wrap_iterIPKiE4baseEv(%"class.std::__2::__wrap_iter.96"* %0) #8
  %1 = load %"class.std::__2::__wrap_iter.96"*, %"class.std::__2::__wrap_iter.96"** %__y.addr, align 4
  %call1 = call i32* @_ZNKSt3__211__wrap_iterIPKiE4baseEv(%"class.std::__2::__wrap_iter.96"* %1) #8
  %sub.ptr.lhs.cast = ptrtoint i32* %call to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %call1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE6cbeginEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.96", align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE5beginEv(%"class.std::__2::vector.87"* %this1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %retval, i32 0, i32 0
  %0 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::vector.87"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi(%"class.std::__2::vector.87"* %this1, i32* %0)
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %2 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %1, i32* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector.87"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__24moveIPiS1_EET0_T_S3_S2_(i32* %__first, i32* %__last, i32* %__result) #0 comdat {
entry:
  %__first.addr = alloca i32*, align 4
  %__last.addr = alloca i32*, align 4
  %__result.addr = alloca i32*, align 4
  store i32* %__first, i32** %__first.addr, align 4
  store i32* %__last, i32** %__last.addr, align 4
  store i32* %__result, i32** %__result.addr, align 4
  %0 = load i32*, i32** %__first.addr, align 4
  %call = call i32* @_ZNSt3__213__unwrap_iterIPiEET_S2_(i32* %0)
  %1 = load i32*, i32** %__last.addr, align 4
  %call1 = call i32* @_ZNSt3__213__unwrap_iterIPiEET_S2_(i32* %1)
  %2 = load i32*, i32** %__result.addr, align 4
  %call2 = call i32* @_ZNSt3__213__unwrap_iterIPiEET_S2_(i32* %2)
  %call3 = call i32* @_ZNSt3__26__moveIiiEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i32* %call, i32* %call1, i32* %call2)
  ret i32* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi(%"class.std::__2::vector.87"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__211__wrap_iterIPKiE4baseEv(%"class.std::__2::__wrap_iter.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.96"*, align 4
  store %"class.std::__2::__wrap_iter.96"* %this, %"class.std::__2::__wrap_iter.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.96"*, %"class.std::__2::__wrap_iter.96"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE5beginEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.96", align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPKi(%"class.std::__2::vector.87"* %this1, i32* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__make_iterEPKi(%"class.std::__2::vector.87"* %this, i32* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.96", align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.96"* @_ZNSt3__211__wrap_iterIPKiEC2ES2_(%"class.std::__2::__wrap_iter.96"* %retval, i32* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %retval, i32 0, i32 0
  %1 = load i32*, i32** %coerce.dive, align 4
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.96"* @_ZNSt3__211__wrap_iterIPKiEC2ES2_(%"class.std::__2::__wrap_iter.96"* returned %this, i32* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.96"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::__wrap_iter.96"* %this, %"class.std::__2::__wrap_iter.96"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.96"*, %"class.std::__2::__wrap_iter.96"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.96", %"class.std::__2::__wrap_iter.96"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__x.addr, align 4
  store i32* %0, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector.87"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i32, i32* %call4, i32 %2
  %3 = bitcast i32* %add.ptr5 to i8*
  %call6 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr8 = getelementptr inbounds i32, i32* %call6, i32 %call7
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26__moveIiiEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i32* %__first, i32* %__last, i32* %__result) #0 comdat {
entry:
  %__first.addr = alloca i32*, align 4
  %__last.addr = alloca i32*, align 4
  %__result.addr = alloca i32*, align 4
  %__n = alloca i32, align 4
  store i32* %__first, i32** %__first.addr, align 4
  store i32* %__last, i32** %__last.addr, align 4
  store i32* %__result, i32** %__result.addr, align 4
  %0 = load i32*, i32** %__last.addr, align 4
  %1 = load i32*, i32** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %__n, align 4
  %2 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32*, i32** %__result.addr, align 4
  %4 = bitcast i32* %3 to i8*
  %5 = load i32*, i32** %__first.addr, align 4
  %6 = bitcast i32* %5 to i8*
  %7 = load i32, i32* %__n, align 4
  %mul = mul i32 %7, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %6, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i32*, i32** %__result.addr, align 4
  %9 = load i32, i32* %__n, align 4
  %add.ptr = getelementptr inbounds i32, i32* %8, i32 %9
  ret i32* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__213__unwrap_iterIPiEET_S2_(i32* %__i) #0 comdat {
entry:
  %__i.addr = alloca i32*, align 4
  store i32* %__i, i32** %__i.addr, align 4
  %0 = load i32*, i32** %__i.addr, align 4
  ret i32* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin allocsize(0) }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
