; ModuleID = './draco/src/draco/attributes/point_attribute.cc'
source_filename = "./draco/src/draco/attributes/point_attribute.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.11", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i8*, i8*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.4" }
%"class.std::__2::vector.4" = type { %"class.std::__2::__vector_base.5" }
%"class.std::__2::__vector_base.5" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.6" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.6" = type { %"struct.std::__2::__compressed_pair_elem.7" }
%"struct.std::__2::__compressed_pair_elem.7" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.11" = type { %"class.std::__2::__compressed_pair.12" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.13" }
%"struct.std::__2::__compressed_pair_elem.13" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::default_delete.15" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.14" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::random_access_iterator_tag" = type { i8 }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.4"*, %"class.draco::IndexType"*, %"class.draco::IndexType"* }
%"class.std::__2::allocator.9" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.8" = type { i8 }
%"struct.std::__2::integral_constant.16" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__has_select_on_container_copy_construction" = type { i8 }
%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i8*, i8* }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::__has_max_size.17" = type { i8 }
%"struct.std::__2::__has_destroy.18" = type { i8 }

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEC2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNK5draco14PointAttribute6bufferEv = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEaSERKS6_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEcvbEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZN5draco22AttributeTransformDataC2ERKS0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEDn = comdat any

$_ZNSt3__2eqIN5draco10DataBufferENS_14default_deleteIS2_EEEEbRKNS_10unique_ptrIT_T0_EEDn = comdat any

$_ZNK5draco17GeometryAttribute9data_typeEv = comdat any

$_ZNK5draco17GeometryAttribute14num_componentsEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEptEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEaSERKS7_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE19__copy_assign_allocERKS7_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6assignIPS4_EENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIS4_NS_15iterator_traitsISB_E9referenceEEE5valueEvE4typeESB_SB_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE19__copy_assign_allocERKS7_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__28distanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_15iterator_traitsIT_E15difference_typeES7_S7_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__27advanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEvRT_NS_15iterator_traitsIS6_E15difference_typeE = comdat any

$_ZNSt3__24copyIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endIPS4_EENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeESB_SB_m = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE13__vdeallocateEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__210__distanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_15iterator_traitsIT_E15difference_typeES7_S7_NS_26random_access_iterator_tagE = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__29__advanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEvRT_NS_15iterator_traitsIS6_E15difference_typeENS_26random_access_iterator_tagE = comdat any

$_ZNSt3__26__copyIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_ = comdat any

$_ZNSt3__213__unwrap_iterIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEET_S6_ = comdat any

$_ZNSt3__216__copy_constexprIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_ = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE25__construct_range_forwardIS5_S5_S5_S5_EENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PSA_RT_EE5valueEvE4typeERS6_PSG_SL_RSF_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZN5draco10DataBufferC2ERKS0_ = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEC2ERKS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE37select_on_container_copy_constructionERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2EOS2_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endIPhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES7_S7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKS2_ = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__24moveIRNS_9allocatorIhEEEEONS_16remove_referenceIT_E4typeEOS5_ = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnS3_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_9allocatorIhEEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2IS2_vEEOT_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIhE8max_sizeEv = comdat any

$_ZNSt3__29allocatorIhE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE25__construct_range_forwardIhhhhEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS6_RT_EE5valueEvE4typeERS2_PSC_SH_RSB_ = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZN5draco22AttributeTransformDataD2Ev = comdat any

$_ZN5draco10DataBufferD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco10DataBufferEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEcvbEv = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco14PointAttributeC1Ev = hidden unnamed_addr alias %"class.draco::PointAttribute"* (%"class.draco::PointAttribute"*), %"class.draco::PointAttribute"* (%"class.draco::PointAttribute"*)* @_ZN5draco14PointAttributeC2Ev
@_ZN5draco14PointAttributeC1ERKNS_17GeometryAttributeE = hidden unnamed_addr alias %"class.draco::PointAttribute"* (%"class.draco::PointAttribute"*, %"class.draco::GeometryAttribute"*), %"class.draco::PointAttribute"* (%"class.draco::PointAttribute"*, %"class.draco::GeometryAttribute"*)* @_ZN5draco14PointAttributeC2ERKNS_17GeometryAttributeE

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeC2Ev(%"class.draco::PointAttribute"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %call = call %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC2Ev(%"class.draco::GeometryAttribute"* %0)
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* %attribute_buffer_) #7
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.draco::IndexTypeVector"* %indices_map_)
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  store i32 0, i32* %num_unique_entries_, align 8
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 0, i8* %identity_mapping_, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call4 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_) #7
  ret %"class.draco::PointAttribute"* %this1
}

declare %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC2Ev(%"class.draco::GeometryAttribute"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::DataBuffer"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  store %"class.draco::DataBuffer"* null, %"class.draco::DataBuffer"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.draco::IndexTypeVector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.4"* %vector_) #7
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.11"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %ref.tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  store %"class.draco::AttributeTransformData"* null, %"class.draco::AttributeTransformData"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* %__ptr_, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeC2ERKNS_17GeometryAttributeE(%"class.draco::PointAttribute"* returned %this, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %att) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %att.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store %"class.draco::GeometryAttribute"* %att, %"class.draco::GeometryAttribute"** %att.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %att.addr, align 4
  %2 = bitcast %"class.draco::GeometryAttribute"* %0 to i8*
  %3 = bitcast %"class.draco::GeometryAttribute"* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 8 %3, i32 64, i1 false)
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* %attribute_buffer_) #7
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.draco::IndexTypeVector"* %indices_map_)
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  store i32 0, i32* %num_unique_entries_, align 8
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 0, i8* %identity_mapping_, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call3 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_) #7
  ret %"class.draco::PointAttribute"* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco14PointAttribute8CopyFromERKS0_(%"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %src_att) #0 {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %src_att.addr = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr", align 4
  %ref.tmp18 = alloca %"class.std::__2::unique_ptr.11", align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %src_att, %"class.draco::PointAttribute"** %src_att.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %call = call %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this1)
  %cmp = icmp eq %"class.draco::DataBuffer"* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call noalias nonnull i8* @_Znwm(i32 32) #8
  %0 = bitcast i8* %call2 to %"class.draco::DataBuffer"*
  %call3 = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferC1Ev(%"class.draco::DataBuffer"* %0)
  %call4 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* %ref.tmp, %"class.draco::DataBuffer"* %0) #7
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %attribute_buffer_, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %ref.tmp) #7
  %call6 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %ref.tmp) #7
  %1 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %attribute_buffer_7 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call8 = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr"* %attribute_buffer_7) #7
  call void @_ZN5draco17GeometryAttribute11ResetBufferEPNS_10DataBufferExx(%"class.draco::GeometryAttribute"* %1, %"class.draco::DataBuffer"* %call8, i64 0, i64 0)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %src_att.addr, align 4
  %4 = bitcast %"class.draco::PointAttribute"* %3 to %"class.draco::GeometryAttribute"*
  %call9 = call zeroext i1 @_ZN5draco17GeometryAttribute8CopyFromERKS0_(%"class.draco::GeometryAttribute"* %2, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %4)
  br i1 %call9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %if.end
  br label %if.end29

if.end11:                                         ; preds = %if.end
  %5 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %src_att.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %5, i32 0, i32 4
  %6 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %6 to i1
  %identity_mapping_12 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %identity_mapping_12, align 4
  %7 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %src_att.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %7, i32 0, i32 3
  %8 = load i32, i32* %num_unique_entries_, align 8
  %num_unique_entries_13 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  store i32 %8, i32* %num_unique_entries_13, align 8
  %9 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %src_att.addr, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %9, i32 0, i32 2
  %indices_map_14 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call15 = call nonnull align 4 dereferenceable(12) %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEaSERKS6_(%"class.draco::IndexTypeVector"* %indices_map_14, %"class.draco::IndexTypeVector"* nonnull align 4 dereferenceable(12) %indices_map_)
  %10 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %src_att.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %10, i32 0, i32 6
  %call16 = call zeroext i1 @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_) #7
  br i1 %call16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.end11
  %call19 = call noalias nonnull i8* @_Znwm(i32 40) #8
  %11 = bitcast i8* %call19 to %"class.draco::AttributeTransformData"*
  %12 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %src_att.addr, align 4
  %attribute_transform_data_20 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %12, i32 0, i32 6
  %call21 = call %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_20) #7
  %call22 = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataC2ERKS0_(%"class.draco::AttributeTransformData"* %11, %"class.draco::AttributeTransformData"* nonnull align 8 dereferenceable(40) %call21)
  %call23 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.11"* %ref.tmp18, %"class.draco::AttributeTransformData"* %11) #7
  %attribute_transform_data_24 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call25 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_24, %"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %ref.tmp18) #7
  %call26 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* %ref.tmp18) #7
  br label %if.end29

if.else:                                          ; preds = %if.end11
  %attribute_transform_data_27 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call28 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEDn(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_27, i8* null) #7
  br label %if.end29

if.end29:                                         ; preds = %if.then10, %if.else, %if.then17
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr"* %attribute_buffer_) #7
  ret %"class.draco::DataBuffer"* %call
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #3

declare %"class.draco::DataBuffer"* @_ZN5draco10DataBufferC1Ev(%"class.draco::DataBuffer"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* returned %this, %"class.draco::DataBuffer"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::DataBuffer"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #7
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::DataBuffer"* %call) #7
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #7
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10DataBufferEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %call2) #7
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #7
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::DataBuffer"* null) #7
  ret %"class.std::__2::unique_ptr"* %this1
}

declare void @_ZN5draco17GeometryAttribute11ResetBufferEPNS_10DataBufferExx(%"class.draco::GeometryAttribute"*, %"class.draco::DataBuffer"*, i64, i64) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #7
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  ret %"class.draco::DataBuffer"* %0
}

declare zeroext i1 @_ZN5draco17GeometryAttribute8CopyFromERKS0_(%"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64)) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEaSERKS6_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"* nonnull align 4 dereferenceable(12) %0) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexTypeVector"* %0, %"class.draco::IndexTypeVector"** %.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %.addr, align 4
  %vector_2 = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEaSERKS7_(%"class.std::__2::vector.4"* %vector_, %"class.std::__2::vector.4"* nonnull align 4 dereferenceable(12) %vector_2)
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #7
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  %cmp = icmp ne %"class.draco::AttributeTransformData"* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #7
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  ret %"class.draco::AttributeTransformData"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataC2ERKS0_(%"class.draco::AttributeTransformData"* returned %this, %"class.draco::AttributeTransformData"* nonnull align 8 dereferenceable(40) %data) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %data.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %data, %"class.draco::AttributeTransformData"** %data.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %transform_type_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %data.addr, align 4
  %transform_type_2 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %0, i32 0, i32 0
  %1 = load i32, i32* %transform_type_2, align 8
  store i32 %1, i32* %transform_type_, align 8
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %data.addr, align 4
  %buffer_3 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %2, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferC2ERKS0_(%"class.draco::DataBuffer"* %buffer_, %"class.draco::DataBuffer"* nonnull align 8 dereferenceable(32) %buffer_3)
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.11"* returned %this, %"class.draco::AttributeTransformData"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* %__ptr_, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.11"* %__u, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %call = call %"class.draco::AttributeTransformData"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.11"* %0) #7
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this1, %"class.draco::AttributeTransformData"* %call) #7
  %1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.11"* %1) #7
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %call2) #7
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #7
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this1, %"class.draco::AttributeTransformData"* null) #7
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEDn(%"class.std::__2::unique_ptr.11"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this1, %"class.draco::AttributeTransformData"* null) #7
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco14PointAttribute5ResetEm(%"class.draco::PointAttribute"* %this, i32 %num_attribute_values) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %num_attribute_values.addr = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr", align 4
  %entry_size = alloca i64, align 8
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i32 %num_attribute_values, i32* %num_attribute_values.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call = call zeroext i1 @_ZNSt3__2eqIN5draco10DataBufferENS_14default_deleteIS2_EEEEbRKNS_10unique_ptrIT_T0_EEDn(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %attribute_buffer_, i8* null) #7
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call noalias nonnull i8* @_Znwm(i32 32) #8
  %0 = bitcast i8* %call2 to %"class.draco::DataBuffer"*
  %call3 = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferC1Ev(%"class.draco::DataBuffer"* %0)
  %call4 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* %ref.tmp, %"class.draco::DataBuffer"* %0) #7
  %attribute_buffer_5 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %attribute_buffer_5, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %ref.tmp) #7
  %call7 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %ref.tmp) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %call8 = call i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %1)
  %call9 = call i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32 %call8)
  %2 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %call10 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %2)
  %conv = sext i8 %call10 to i32
  %mul = mul nsw i32 %call9, %conv
  %conv11 = sext i32 %mul to i64
  store i64 %conv11, i64* %entry_size, align 8
  %attribute_buffer_12 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call13 = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %attribute_buffer_12) #7
  %3 = load i32, i32* %num_attribute_values.addr, align 4
  %conv14 = zext i32 %3 to i64
  %4 = load i64, i64* %entry_size, align 8
  %mul15 = mul nsw i64 %conv14, %4
  %call16 = call zeroext i1 @_ZN5draco10DataBuffer6UpdateEPKvx(%"class.draco::DataBuffer"* %call13, i8* null, i64 %mul15)
  br i1 %call16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end
  %5 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %attribute_buffer_19 = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call20 = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr"* %attribute_buffer_19) #7
  %6 = load i64, i64* %entry_size, align 8
  call void @_ZN5draco17GeometryAttribute11ResetBufferEPNS_10DataBufferExx(%"class.draco::GeometryAttribute"* %5, %"class.draco::DataBuffer"* %call20, i64 %6, i64 0)
  %7 = load i32, i32* %num_attribute_values.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  store i32 %7, i32* %num_unique_entries_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17
  %8 = load i1, i1* %retval, align 1
  ret i1 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqIN5draco10DataBufferENS_14default_deleteIS2_EEEEbRKNS_10unique_ptrIT_T0_EEDn(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__x, i8* %0) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::unique_ptr"* %__x, %"class.std::__2::unique_ptr"** %__x.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__x.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr"* %1) #7
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

declare i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %data_type_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  %0 = load i8, i8* %num_components_, align 8
  ret i8 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #7
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  ret %"class.draco::DataBuffer"* %0
}

declare zeroext i1 @_ZN5draco10DataBuffer6UpdateEPKvx(%"class.draco::DataBuffer"*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"class.draco::DataBuffer"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"class.draco::DataBuffer"** %__t1, %"class.draco::DataBuffer"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"class.draco::DataBuffer"**, %"class.draco::DataBuffer"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__27forwardIPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__27forwardIPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::DataBuffer"**, align 4
  store %"class.draco::DataBuffer"** %__t, %"class.draco::DataBuffer"*** %__t.addr, align 4
  %0 = load %"class.draco::DataBuffer"**, %"class.draco::DataBuffer"*** %__t.addr, align 4
  ret %"class.draco::DataBuffer"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"class.draco::DataBuffer"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"class.draco::DataBuffer"** %__u, %"class.draco::DataBuffer"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.draco::DataBuffer"**, %"class.draco::DataBuffer"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__27forwardIPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  %__t1.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__t1, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %1 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* %0, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.14"* %2)
  ret %"class.std::__2::__compressed_pair.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"class.draco::AttributeTransformData"** %__t, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  ret %"class.draco::AttributeTransformData"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  %__u.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__u, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.14"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.14"* %this1 to %"struct.std::__2::default_delete.15"*
  ret %"struct.std::__2::__compressed_pair_elem.14"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEaSERKS7_(%"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__x.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"class.std::__2::vector.4"* %__x, %"class.std::__2::vector.4"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__x.addr, align 4
  %cmp = icmp ne %"class.std::__2::vector.4"* %this1, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %2 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__x.addr, align 4
  %3 = bitcast %"class.std::__2::vector.4"* %2 to %"class.std::__2::__vector_base.5"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE19__copy_assign_allocERKS7_(%"class.std::__2::__vector_base.5"* %1, %"class.std::__2::__vector_base.5"* nonnull align 4 dereferenceable(12) %3)
  %4 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__x.addr, align 4
  %5 = bitcast %"class.std::__2::vector.4"* %4 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %5, i32 0, i32 0
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %7 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__x.addr, align 4
  %8 = bitcast %"class.std::__2::vector.4"* %7 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %8, i32 0, i32 1
  %9 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6assignIPS4_EENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIS4_NS_15iterator_traitsISB_E9referenceEEE5valueEvE4typeESB_SB_(%"class.std::__2::vector.4"* %this1, %"class.draco::IndexType"* %6, %"class.draco::IndexType"* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret %"class.std::__2::vector.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE19__copy_assign_allocERKS7_(%"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"* nonnull align 4 dereferenceable(12) %__c) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  %__c.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  store %"class.std::__2::__vector_base.5"* %__c, %"class.std::__2::__vector_base.5"** %__c.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %0 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %__c.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE19__copy_assign_allocERKS7_NS_17integral_constantIbLb0EEE(%"class.std::__2::__vector_base.5"* %this1, %"class.std::__2::__vector_base.5"* nonnull align 4 dereferenceable(12) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6assignIPS4_EENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIS4_NS_15iterator_traitsISB_E9referenceEEE5valueEvE4typeESB_SB_(%"class.std::__2::vector.4"* %this, %"class.draco::IndexType"* %__first, %"class.draco::IndexType"* %__last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__last.addr = alloca %"class.draco::IndexType"*, align 4
  %__new_size = alloca i32, align 4
  %__mid = alloca %"class.draco::IndexType"*, align 4
  %__growing = alloca i8, align 1
  %__m = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %__last, %"class.draco::IndexType"** %__last.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %call = call i32 @_ZNSt3__28distanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_15iterator_traitsIT_E15difference_typeES7_S7_(%"class.draco::IndexType"* %0, %"class.draco::IndexType"* %1)
  store i32 %call, i32* %__new_size, align 4
  %2 = load i32, i32* %__new_size, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #7
  %cmp = icmp ule i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.else11

if.then:                                          ; preds = %entry
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %__mid, align 4
  store i8 0, i8* %__growing, align 1
  %4 = load i32, i32* %__new_size, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #7
  %cmp4 = icmp ugt i32 %4, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  store i8 1, i8* %__growing, align 1
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %5, %"class.draco::IndexType"** %__mid, align 4
  %call6 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #7
  call void @_ZNSt3__27advanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEvRT_NS_15iterator_traitsIS6_E15difference_typeE(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__mid, i32 %call6)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.then
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %7 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__mid, align 4
  %8 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %8, i32 0, i32 0
  %9 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call7 = call %"class.draco::IndexType"* @_ZNSt3__24copyIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_(%"class.draco::IndexType"* %6, %"class.draco::IndexType"* %7, %"class.draco::IndexType"* %9)
  store %"class.draco::IndexType"* %call7, %"class.draco::IndexType"** %__m, align 4
  %10 = load i8, i8* %__growing, align 1
  %tobool = trunc i8 %10 to i1
  br i1 %tobool, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end
  %11 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__mid, align 4
  %12 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %13 = load i32, i32* %__new_size, align 4
  %call9 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #7
  %sub = sub i32 %13, %call9
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endIPS4_EENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeESB_SB_m(%"class.std::__2::vector.4"* %this1, %"class.draco::IndexType"* %11, %"class.draco::IndexType"* %12, i32 %sub)
  br label %if.end10

if.else:                                          ; preds = %if.end
  %14 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__m, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.4"* %this1, %"class.draco::IndexType"* %14) #7
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then8
  br label %if.end13

if.else11:                                        ; preds = %entry
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE13__vdeallocateEv(%"class.std::__2::vector.4"* %this1) #7
  %15 = load i32, i32* %__new_size, align 4
  %call12 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.4"* %this1, i32 %15)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm(%"class.std::__2::vector.4"* %this1, i32 %call12)
  %16 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %17 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %18 = load i32, i32* %__new_size, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endIPS4_EENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeESB_SB_m(%"class.std::__2::vector.4"* %this1, %"class.draco::IndexType"* %16, %"class.draco::IndexType"* %17, i32 %18)
  br label %if.end13

if.end13:                                         ; preds = %if.else11, %if.end10
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.4"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE19__copy_assign_allocERKS7_NS_17integral_constantIbLb0EEE(%"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"* nonnull align 4 dereferenceable(12) %0) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant", align 1
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  %.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  store %"class.std::__2::__vector_base.5"* %0, %"class.std::__2::__vector_base.5"** %.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_15iterator_traitsIT_E15difference_typeES7_S7_(%"class.draco::IndexType"* %__first, %"class.draco::IndexType"* %__last) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__last.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %__last, %"class.draco::IndexType"** %__last.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %call = call i32 @_ZNSt3__210__distanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_15iterator_traitsIT_E15difference_typeES7_S7_NS_26random_access_iterator_tagE(%"class.draco::IndexType"* %0, %"class.draco::IndexType"* %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__27advanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEvRT_NS_15iterator_traitsIS6_E15difference_typeE(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %__i.addr = alloca %"class.draco::IndexType"**, align 4
  %__n.addr = alloca i32, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store %"class.draco::IndexType"** %__i, %"class.draco::IndexType"*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__i.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29__advanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEvRT_NS_15iterator_traitsIS6_E15difference_typeENS_26random_access_iterator_tagE(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__24copyIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_(%"class.draco::IndexType"* %__first, %"class.draco::IndexType"* %__last, %"class.draco::IndexType"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__last.addr = alloca %"class.draco::IndexType"*, align 4
  %__result.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %__last, %"class.draco::IndexType"** %__last.addr, align 4
  store %"class.draco::IndexType"* %__result, %"class.draco::IndexType"** %__result.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__213__unwrap_iterIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEET_S6_(%"class.draco::IndexType"* %0)
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %call1 = call %"class.draco::IndexType"* @_ZNSt3__213__unwrap_iterIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEET_S6_(%"class.draco::IndexType"* %1)
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__result.addr, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__213__unwrap_iterIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEET_S6_(%"class.draco::IndexType"* %2)
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__26__copyIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_(%"class.draco::IndexType"* %call, %"class.draco::IndexType"* %call1, %"class.draco::IndexType"* %call2)
  ret %"class.draco::IndexType"* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endIPS4_EENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeESB_SB_m(%"class.std::__2::vector.4"* %this, %"class.draco::IndexType"* %__first, %"class.draco::IndexType"* %__last, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__last.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %__last, %"class.draco::IndexType"** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.4"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %1) #7
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE25__construct_range_forwardIS5_S5_S5_S5_EENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PSA_RT_EE5valueEvE4typeERS6_PSG_SL_RSF_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* %3, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.4"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.4"* %this1, %"class.draco::IndexType"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #7
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %1, %"class.draco::IndexType"* %2) #7
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.4"* %this1, i32 %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE13__vdeallocateEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.4"* %this1) #7
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %2) #7
  %3 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %3, i32 0, i32 0
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %4, i32 %call3) #7
  %5 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %5) #7
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %call4, align 4
  %6 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %6, i32 0, i32 1
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__end_, align 4
  %7 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_5 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %7, i32 0, i32 0
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__begin_5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm(%"class.std::__2::vector.4"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.4"* %this1) #7
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %2) #7
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %4, i32 0, i32 1
  store %"class.draco::IndexType"* %call3, %"class.draco::IndexType"** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %5, i32 0, i32 0
  store %"class.draco::IndexType"* %call3, %"class.draco::IndexType"** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %6, i32 0, i32 0
  %7 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %9) #7
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %call5, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.4"* %this1, i32 0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.4"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.4"* %this1) #7
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #9
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #7
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_15iterator_traitsIT_E15difference_typeES7_S7_NS_26random_access_iterator_tagE(%"class.draco::IndexType"* %__first, %"class.draco::IndexType"* %__last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %__last, %"class.draco::IndexType"** %__last.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this1) #7
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #7
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #7
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29__advanceIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEvRT_NS_15iterator_traitsIS6_E15difference_typeENS_26random_access_iterator_tagE(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__i.addr = alloca %"class.draco::IndexType"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.draco::IndexType"** %__i, %"class.draco::IndexType"*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__i.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %2, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 %1
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__26__copyIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_(%"class.draco::IndexType"* %__first, %"class.draco::IndexType"* %__last, %"class.draco::IndexType"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__last.addr = alloca %"class.draco::IndexType"*, align 4
  %__result.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %__last, %"class.draco::IndexType"** %__last.addr, align 4
  store %"class.draco::IndexType"* %__result, %"class.draco::IndexType"** %__result.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__result.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__216__copy_constexprIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_(%"class.draco::IndexType"* %0, %"class.draco::IndexType"* %1, %"class.draco::IndexType"* %2)
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__213__unwrap_iterIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEET_S6_(%"class.draco::IndexType"* %__i) #0 comdat {
entry:
  %__i.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__i, %"class.draco::IndexType"** %__i.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__i.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__216__copy_constexprIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEES5_EET0_T_S7_S6_(%"class.draco::IndexType"* %__first, %"class.draco::IndexType"* %__last, %"class.draco::IndexType"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__last.addr = alloca %"class.draco::IndexType"*, align 4
  %__result.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store %"class.draco::IndexType"* %__last, %"class.draco::IndexType"** %__last.addr, align 4
  store %"class.draco::IndexType"* %__result, %"class.draco::IndexType"** %__result.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__last.addr, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__result.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %3, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %4, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__first.addr, align 4
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__result.addr, align 4
  %incdec.ptr1 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 1
  store %"class.draco::IndexType"* %incdec.ptr1, %"class.draco::IndexType"** %__result.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__result.addr, align 4
  ret %"class.draco::IndexType"* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %i.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store %"class.draco::IndexType"* %i, %"class.draco::IndexType"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.4"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.4"* %__v, %"class.std::__2::vector.4"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v.addr, align 4
  store %"class.std::__2::vector.4"* %0, %"class.std::__2::vector.4"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.4"* %4 to %"class.std::__2::__vector_base.5"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE25__construct_range_forwardIS5_S5_S5_S5_EENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PSA_RT_EE5valueEvE4typeERS6_PSG_SL_RSF_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType"*, align 4
  %__begin2.addr = alloca %"class.draco::IndexType"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %0, %"class.std::__2::allocator.9"** %.addr, align 4
  store %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"** %__begin1.addr, align 4
  store %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** %__end1.addr, align 4
  store %"class.draco::IndexType"** %__begin2, %"class.draco::IndexType"*** %__begin2.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__begin2.addr, align 4
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %4, align 4
  %6 = bitcast %"class.draco::IndexType"* %5 to i8*
  %7 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %8 = bitcast %"class.draco::IndexType"* %7 to i8*
  %9 = load i32, i32* %_Np, align 4
  %mul = mul i32 %9, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %8, i32 %mul, i1 false)
  %10 = load i32, i32* %_Np, align 4
  %11 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__begin2.addr, align 4
  %12 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %11, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %12, i32 %10
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %11, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #7
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 1
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #7
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.8"* %this1 to %"class.std::__2::allocator.9"*
  ret %"class.std::__2::allocator.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.4"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this1) #7
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.4"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #7
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.16", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant.16"*
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.16", align 1
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.9"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.9"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #7
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #7
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.5"* %0) #7
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.4"* %this1, i32 %1) #7
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.4"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.9"* %0, %"class.draco::IndexType"* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #7
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %this1, %"class.draco::IndexType"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.9"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #7
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.9"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.4"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.16", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant.16"*
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #7
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.16", align 1
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.9"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #7
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.8"* %this1 to %"class.std::__2::allocator.9"*
  ret %"class.std::__2::allocator.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.9"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.9"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType"*
  ret %"class.draco::IndexType"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #6 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #9
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #8
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferC2ERKS0_(%"class.draco::DataBuffer"* returned %this, %"class.draco::DataBuffer"* nonnull align 8 dereferenceable(32) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %.addr, align 4
  %data_2 = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEEC2ERKS3_(%"class.std::__2::vector"* %data_, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %data_2)
  %descriptor_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 1
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %.addr, align 4
  %descriptor_3 = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %2, i32 0, i32 1
  %3 = bitcast %"struct.draco::DataBufferDescriptor"* %descriptor_ to i8*
  %4 = bitcast %"struct.draco::DataBufferDescriptor"* %descriptor_3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %3, i8* align 8 %4, i32 16, i1 false)
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEEC2ERKS3_(%"class.std::__2::vector"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__x) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca %"class.std::__2::allocator", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator", align 1
  %__n = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %__x, %"class.std::__2::vector"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE37select_on_container_copy_constructionERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call)
  %call2 = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2EOS2_(%"class.std::__2::__vector_base"* %0, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %ref.tmp) #7
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %3) #7
  store i32 %call3, i32* %__n, align 4
  %4 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %5)
  %6 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %7 = bitcast %"class.std::__2::vector"* %6 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %7, i32 0, i32 0
  %8 = load i8*, i8** %__begin_, align 4
  %9 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %10 = bitcast %"class.std::__2::vector"* %9 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %10, i32 0, i32 1
  %11 = load i8*, i8** %__end_, align 4
  %12 = load i32, i32* %__n, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endIPhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES7_S7_m(%"class.std::__2::vector"* %this1, i8* %8, i8* %11, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE37select_on_container_copy_constructionERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_select_on_container_copy_construction", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_select_on_container_copy_construction"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2EOS2_(%"class.std::__2::__vector_base"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i8* null, i8** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i8* null, i8** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__24moveIRNS_9allocatorIhEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #7
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnS3_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #7
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #7
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  store i8* %call3, i8** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  store i8* %call3, i8** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load i8*, i8** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %9) #7
  store i8* %add.ptr, i8** %call5, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endIPhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES7_S7_m(%"class.std::__2::vector"* %this, i8* %__first, i8* %__last, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %1) #7
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE25__construct_range_forwardIhhhhEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS6_RT_EE5valueEvE4typeERS2_PSC_SH_RSB_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i8* %2, i8* %3, i8** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__24moveIRNS_9allocatorIhEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnS3_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorIhEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* %2, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorIhEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorIhEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIhE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i8, i8* %call7, i32 %0
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr8) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.16", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.17", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.17"* %ref.tmp to %"struct.std::__2::integral_constant.16"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.16", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIhE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #7
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #7
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i8*, i8** %__end_, align 4
  store i8* %3, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i8*, i8** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE25__construct_range_forwardIhhhhEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS6_RT_EE5valueEvE4typeERS2_PSC_SH_RSB_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__begin2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__begin2, i8*** %__begin2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i8**, i8*** %__begin2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %6 = load i8*, i8** %__begin1.addr, align 4
  %7 = load i32, i32* %_Np, align 4
  %mul = mul i32 %7, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %mul, i1 false)
  %8 = load i32, i32* %_Np, align 4
  %9 = load i8**, i8*** %__begin2.addr, align 4
  %10 = load i8*, i8** %9, align 4
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %8
  store i8* %add.ptr, i8** %9, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i8* %0, i8** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #7
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this, %"class.draco::AttributeTransformData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %__tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #7
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_2) #7
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %call3, align 4
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__ptr_4) #7
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.15"* %call5, %"class.draco::AttributeTransformData"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__t = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #7
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_2) #7
  store %"class.draco::AttributeTransformData"* null, %"class.draco::AttributeTransformData"** %call3, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__t, align 4
  ret %"class.draco::AttributeTransformData"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.15"*, align 4
  store %"struct.std::__2::default_delete.15"* %__t, %"struct.std::__2::default_delete.15"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.15"*, %"struct.std::__2::default_delete.15"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.15"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #7
  ret %"struct.std::__2::default_delete.15"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %0) #7
  ret %"struct.std::__2::default_delete.15"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %0) #7
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.15"* %this, %"class.draco::AttributeTransformData"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.15"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"struct.std::__2::default_delete.15"* %this, %"struct.std::__2::default_delete.15"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__ptr, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.15"*, %"struct.std::__2::default_delete.15"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeTransformData"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* %0) #7
  %1 = bitcast %"class.draco::AttributeTransformData"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %buffer_) #7
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* %data_) #7
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* %0) #7
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this1, i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator"* %0, i8* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #7
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.16", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.18", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.18"* %ref.tmp to %"struct.std::__2::integral_constant.16"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.16", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.14"* %this1 to %"struct.std::__2::default_delete.15"*
  ret %"struct.std::__2::default_delete.15"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %0) #7
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.4"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call %"class.std::__2::__vector_base.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.5"* %0) #7
  ret %"class.std::__2::vector.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.5"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.5"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.6"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.6"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.6"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.6"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.7"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.8"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.8"* %2)
  ret %"class.std::__2::__compressed_pair.6"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.7"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.8"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.8"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.8"* %this1 to %"class.std::__2::allocator.9"*
  %call = call %"class.std::__2::allocator.9"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.9"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.8"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.9"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.9"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  ret %"class.std::__2::allocator.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::DataBuffer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %__tmp = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #7
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__tmp, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #7
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %call3, align 4
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #7
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::DataBuffer"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #7
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #7
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::DataBuffer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__ptr, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %0) #7
  %1 = bitcast %"class.draco::DataBuffer"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"class.draco::DataBuffer"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"class.draco::DataBuffer"** %__t1, %"class.draco::DataBuffer"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"class.draco::DataBuffer"**, %"class.draco::DataBuffer"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__27forwardIRPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__27forwardIRPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::DataBuffer"**, align 4
  store %"class.draco::DataBuffer"** %__t, %"class.draco::DataBuffer"*** %__t.addr, align 4
  %0 = load %"class.draco::DataBuffer"**, %"class.draco::DataBuffer"*** %__t.addr, align 4
  ret %"class.draco::DataBuffer"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"class.draco::DataBuffer"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"class.draco::DataBuffer"** %__u, %"class.draco::DataBuffer"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.draco::DataBuffer"**, %"class.draco::DataBuffer"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__27forwardIRPN5draco10DataBufferEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::DataBuffer"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__t = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #7
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #7
  store %"class.draco::DataBuffer"* null, %"class.draco::DataBuffer"** %call3, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__t, align 4
  ret %"class.draco::DataBuffer"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10DataBufferEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete"*, align 4
  store %"struct.std::__2::default_delete"* %__t, %"struct.std::__2::default_delete"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #7
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  %__t1.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__t1, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %1 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* %0, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.14"* %2)
  ret %"class.std::__2::__compressed_pair.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"class.draco::AttributeTransformData"** %__t, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  ret %"class.draco::AttributeTransformData"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  %__u.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__u, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #7
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  %cmp = icmp ne %"class.draco::DataBuffer"* %0, null
  ret i1 %cmp
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { noreturn }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
