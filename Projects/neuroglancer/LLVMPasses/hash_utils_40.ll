; ModuleID = './draco/src/draco/core/hash_utils.cc'
source_filename = "./draco/src/draco/core/hash_utils.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

$_ZN5draco11HashCombineEyy = comdat any

$_ZNSt3__214numeric_limitsIyE3maxEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIyLb1EE3maxEv = comdat any

; Function Attrs: noinline nounwind optnone
define hidden i64 @_ZN5draco17FingerprintStringEPKcm(i8* %s, i32 %len) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %seed = alloca i64, align 8
  %hash_loop_count = alloca i32, align 4
  %hash = alloca i64, align 8
  %i = alloca i32, align 4
  %off = alloca i32, align 4
  %num_chars_left = alloca i32, align 4
  %new_hash = alloca i64, align 8
  %off2 = alloca i32, align 4
  %j = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i64 2271560481, i64* %seed, align 8
  %0 = load i32, i32* %len.addr, align 4
  %div = udiv i32 %0, 8
  %add = add nsw i32 %div, 1
  store i32 %add, i32* %hash_loop_count, align 4
  store i64 2271560481, i64* %hash, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc47, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %hash_loop_count, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end49

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %mul = mul nsw i32 %3, 8
  store i32 %mul, i32* %off, align 4
  %4 = load i32, i32* %len.addr, align 4
  %5 = load i32, i32* %off, align 4
  %sub = sub nsw i32 %4, %5
  store i32 %sub, i32* %num_chars_left, align 4
  store i64 2271560481, i64* %new_hash, align 8
  %6 = load i32, i32* %num_chars_left, align 4
  %cmp1 = icmp sgt i32 %6, 7
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %mul2 = mul nsw i32 %7, 8
  store i32 %mul2, i32* %off2, align 4
  %8 = load i8*, i8** %s.addr, align 4
  %9 = load i32, i32* %off2, align 4
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %10 to i64
  %shl = shl i64 %conv, 56
  %11 = load i8*, i8** %s.addr, align 4
  %12 = load i32, i32* %off2, align 4
  %add3 = add nsw i32 %12, 1
  %arrayidx4 = getelementptr inbounds i8, i8* %11, i32 %add3
  %13 = load i8, i8* %arrayidx4, align 1
  %conv5 = sext i8 %13 to i64
  %shl6 = shl i64 %conv5, 48
  %or = or i64 %shl, %shl6
  %14 = load i8*, i8** %s.addr, align 4
  %15 = load i32, i32* %off2, align 4
  %add7 = add nsw i32 %15, 2
  %arrayidx8 = getelementptr inbounds i8, i8* %14, i32 %add7
  %16 = load i8, i8* %arrayidx8, align 1
  %conv9 = sext i8 %16 to i64
  %shl10 = shl i64 %conv9, 40
  %or11 = or i64 %or, %shl10
  %17 = load i8*, i8** %s.addr, align 4
  %18 = load i32, i32* %off2, align 4
  %add12 = add nsw i32 %18, 3
  %arrayidx13 = getelementptr inbounds i8, i8* %17, i32 %add12
  %19 = load i8, i8* %arrayidx13, align 1
  %conv14 = sext i8 %19 to i64
  %shl15 = shl i64 %conv14, 32
  %or16 = or i64 %or11, %shl15
  %20 = load i8*, i8** %s.addr, align 4
  %21 = load i32, i32* %off2, align 4
  %add17 = add nsw i32 %21, 4
  %arrayidx18 = getelementptr inbounds i8, i8* %20, i32 %add17
  %22 = load i8, i8* %arrayidx18, align 1
  %conv19 = sext i8 %22 to i64
  %shl20 = shl i64 %conv19, 24
  %or21 = or i64 %or16, %shl20
  %23 = load i8*, i8** %s.addr, align 4
  %24 = load i32, i32* %off2, align 4
  %add22 = add nsw i32 %24, 5
  %arrayidx23 = getelementptr inbounds i8, i8* %23, i32 %add22
  %25 = load i8, i8* %arrayidx23, align 1
  %conv24 = sext i8 %25 to i64
  %shl25 = shl i64 %conv24, 16
  %or26 = or i64 %or21, %shl25
  %26 = load i8*, i8** %s.addr, align 4
  %27 = load i32, i32* %off2, align 4
  %add27 = add nsw i32 %27, 6
  %arrayidx28 = getelementptr inbounds i8, i8* %26, i32 %add27
  %28 = load i8, i8* %arrayidx28, align 1
  %conv29 = sext i8 %28 to i64
  %shl30 = shl i64 %conv29, 8
  %or31 = or i64 %or26, %shl30
  %29 = load i8*, i8** %s.addr, align 4
  %30 = load i32, i32* %off2, align 4
  %add32 = add nsw i32 %30, 7
  %arrayidx33 = getelementptr inbounds i8, i8* %29, i32 %add32
  %31 = load i8, i8* %arrayidx33, align 1
  %conv34 = sext i8 %31 to i64
  %or35 = or i64 %or31, %conv34
  store i64 %or35, i64* %new_hash, align 8
  br label %if.end

if.else:                                          ; preds = %for.body
  store i32 0, i32* %j, align 4
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc, %if.else
  %32 = load i32, i32* %j, align 4
  %33 = load i32, i32* %num_chars_left, align 4
  %cmp37 = icmp slt i32 %32, %33
  br i1 %cmp37, label %for.body38, label %for.end

for.body38:                                       ; preds = %for.cond36
  %34 = load i8*, i8** %s.addr, align 4
  %35 = load i32, i32* %off, align 4
  %36 = load i32, i32* %j, align 4
  %add39 = add nsw i32 %35, %36
  %arrayidx40 = getelementptr inbounds i8, i8* %34, i32 %add39
  %37 = load i8, i8* %arrayidx40, align 1
  %conv41 = sext i8 %37 to i64
  %38 = load i32, i32* %num_chars_left, align 4
  %39 = load i32, i32* %j, align 4
  %sub42 = sub nsw i32 %38, %39
  %mul43 = mul nsw i32 %sub42, 8
  %sub44 = sub nsw i32 64, %mul43
  %sh_prom = zext i32 %sub44 to i64
  %shl45 = shl i64 %conv41, %sh_prom
  %40 = load i64, i64* %new_hash, align 8
  %or46 = or i64 %40, %shl45
  store i64 %or46, i64* %new_hash, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body38
  %41 = load i32, i32* %j, align 4
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond36

for.end:                                          ; preds = %for.cond36
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %42 = load i64, i64* %new_hash, align 8
  %43 = load i64, i64* %hash, align 8
  %call = call i64 @_ZN5draco11HashCombineEyy(i64 %42, i64 %43)
  store i64 %call, i64* %hash, align 8
  br label %for.inc47

for.inc47:                                        ; preds = %if.end
  %44 = load i32, i32* %i, align 4
  %inc48 = add nsw i32 %44, 1
  store i32 %inc48, i32* %i, align 4
  br label %for.cond

for.end49:                                        ; preds = %for.cond
  %45 = load i64, i64* %hash, align 8
  %call50 = call i64 @_ZNSt3__214numeric_limitsIyE3maxEv() #1
  %sub51 = sub i64 %call50, 1
  %cmp52 = icmp ult i64 %45, %sub51
  br i1 %cmp52, label %if.then53, label %if.end55

if.then53:                                        ; preds = %for.end49
  %46 = load i64, i64* %hash, align 8
  %add54 = add i64 %46, 2
  store i64 %add54, i64* %hash, align 8
  br label %if.end55

if.end55:                                         ; preds = %if.then53, %for.end49
  %47 = load i64, i64* %hash, align 8
  ret i64 %47
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZN5draco11HashCombineEyy(i64 %a, i64 %b) #0 comdat {
entry:
  %a.addr = alloca i64, align 8
  %b.addr = alloca i64, align 8
  store i64 %a, i64* %a.addr, align 8
  store i64 %b, i64* %b.addr, align 8
  %0 = load i64, i64* %a.addr, align 8
  %add = add i64 %0, 1013
  %1 = load i64, i64* %b.addr, align 8
  %add1 = add i64 %1, 107
  %shl = shl i64 %add1, 1
  %xor = xor i64 %add, %shl
  ret i64 %xor
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNSt3__214numeric_limitsIyE3maxEv() #0 comdat {
entry:
  %call = call i64 @_ZNSt3__223__libcpp_numeric_limitsIyLb1EE3maxEv() #1
  ret i64 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNSt3__223__libcpp_numeric_limitsIyLb1EE3maxEv() #0 comdat {
entry:
  ret i64 -1
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
