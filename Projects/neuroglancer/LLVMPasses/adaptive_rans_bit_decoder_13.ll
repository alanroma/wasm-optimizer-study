; ModuleID = './draco/src/draco/compression/bit_coders/adaptive_rans_bit_decoder.cc'
source_filename = "./draco/src/draco/compression/bit_coders/adaptive_rans_bit_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::AdaptiveRAnsBitDecoder" = type { %"struct.draco::AnsDecoder", double }
%"struct.draco::AnsDecoder" = type { i8*, i32, i32 }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }

$_ZN5draco10AnsDecoderC2Ev = comdat any

$_ZN5draco13DecoderBuffer6DecodeIjEEbPT_ = comdat any

$_ZNK5draco13DecoderBuffer14remaining_sizeEv = comdat any

$_ZNK5draco13DecoderBuffer9data_headEv = comdat any

$_ZN5draco13DecoderBuffer7AdvanceEx = comdat any

$_ZN5draco17clamp_probabilityEd = comdat any

$_ZN5draco18update_probabilityEdb = comdat any

$_ZN5draco13DecoderBuffer4PeekIjEEbPT_ = comdat any

$_ZZN5draco18update_probabilityEdbE1w = comdat any

$_ZZN5draco18update_probabilityEdbE2w0 = comdat any

$_ZZN5draco18update_probabilityEdbE2w1 = comdat any

@_ZZN5draco18update_probabilityEdbE1w = linkonce_odr hidden constant double 1.280000e+02, comdat, align 8
@_ZZN5draco18update_probabilityEdbE2w0 = linkonce_odr hidden constant double 0x3FEFC00000000000, comdat, align 8
@_ZZN5draco18update_probabilityEdbE2w1 = linkonce_odr hidden constant double 7.812500e-03, comdat, align 8

@_ZN5draco22AdaptiveRAnsBitDecoderC1Ev = hidden unnamed_addr alias %"class.draco::AdaptiveRAnsBitDecoder"* (%"class.draco::AdaptiveRAnsBitDecoder"*), %"class.draco::AdaptiveRAnsBitDecoder"* (%"class.draco::AdaptiveRAnsBitDecoder"*)* @_ZN5draco22AdaptiveRAnsBitDecoderC2Ev
@_ZN5draco22AdaptiveRAnsBitDecoderD1Ev = hidden unnamed_addr alias %"class.draco::AdaptiveRAnsBitDecoder"* (%"class.draco::AdaptiveRAnsBitDecoder"*), %"class.draco::AdaptiveRAnsBitDecoder"* (%"class.draco::AdaptiveRAnsBitDecoder"*)* @_ZN5draco22AdaptiveRAnsBitDecoderD2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::AdaptiveRAnsBitDecoder"* @_ZN5draco22AdaptiveRAnsBitDecoderC2Ev(%"class.draco::AdaptiveRAnsBitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::AdaptiveRAnsBitDecoder"*, align 4
  store %"class.draco::AdaptiveRAnsBitDecoder"* %this, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::AdaptiveRAnsBitDecoder"*, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  %ans_decoder_ = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 0
  %call = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_decoder_)
  %p0_f_ = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 1
  store double 5.000000e-01, double* %p0_f_, align 8
  ret %"class.draco::AdaptiveRAnsBitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  store %"struct.draco::AnsDecoder"* %this, %"struct.draco::AnsDecoder"** %this.addr, align 4
  %this1 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %this.addr, align 4
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 0
  store i8* null, i8** %buf, align 4
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 1
  store i32 0, i32* %buf_offset, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %state, align 4
  ret %"struct.draco::AnsDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::AdaptiveRAnsBitDecoder"* @_ZN5draco22AdaptiveRAnsBitDecoderD2Ev(%"class.draco::AdaptiveRAnsBitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::AdaptiveRAnsBitDecoder"*, align 4
  store %"class.draco::AdaptiveRAnsBitDecoder"* %this, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::AdaptiveRAnsBitDecoder"*, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  call void @_ZN5draco22AdaptiveRAnsBitDecoder5ClearEv(%"class.draco::AdaptiveRAnsBitDecoder"* %this1)
  ret %"class.draco::AdaptiveRAnsBitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco22AdaptiveRAnsBitDecoder5ClearEv(%"class.draco::AdaptiveRAnsBitDecoder"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::AdaptiveRAnsBitDecoder"*, align 4
  store %"class.draco::AdaptiveRAnsBitDecoder"* %this, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::AdaptiveRAnsBitDecoder"*, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  %ans_decoder_ = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 0
  %call = call i32 @_ZN5dracoL12ans_read_endEPNS_10AnsDecoderE(%"struct.draco::AnsDecoder"* %ans_decoder_)
  %p0_f_ = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 1
  store double 5.000000e-01, double* %p0_f_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco22AdaptiveRAnsBitDecoder13StartDecodingEPNS_13DecoderBufferE(%"class.draco::AdaptiveRAnsBitDecoder"* %this, %"class.draco::DecoderBuffer"* %source_buffer) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::AdaptiveRAnsBitDecoder"*, align 4
  %source_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %size_in_bytes = alloca i32, align 4
  store %"class.draco::AdaptiveRAnsBitDecoder"* %this, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %source_buffer, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %this1 = load %"class.draco::AdaptiveRAnsBitDecoder"*, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  call void @_ZN5draco22AdaptiveRAnsBitDecoder5ClearEv(%"class.draco::AdaptiveRAnsBitDecoder"* %this1)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %0, i32* %size_in_bytes)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %size_in_bytes, align 4
  %conv = zext i32 %1 to i64
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call2 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %2)
  %cmp = icmp sgt i64 %conv, %call2
  br i1 %cmp, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.end
  %ans_decoder_ = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 0
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call5 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %3)
  %4 = load i32, i32* %size_in_bytes, align 4
  %call6 = call i32 @_ZN5dracoL13ans_read_initEPNS_10AnsDecoderEPKhi(%"struct.draco::AnsDecoder"* %ans_decoder_, i8* %call5, i32 %4)
  %cmp7 = icmp ne i32 %call6, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end4
  store i1 false, i1* %retval, align 1
  br label %return

if.end9:                                          ; preds = %if.end4
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %6 = load i32, i32* %size_in_bytes, align 4
  %conv10 = zext i32 %6 to i64
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %5, i64 %conv10)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end9, %if.then8, %if.then3, %if.then
  %7 = load i1, i1* %retval, align 1
  ret i1 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i32*, i32** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this1, i32* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %sub = sub nsw i64 %0, %1
  ret i64 %sub
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL13ans_read_initEPNS_10AnsDecoderEPKhi(%"struct.draco::AnsDecoder"* %ans, i8* %buf, i32 %offset) #0 {
entry:
  %retval = alloca i32, align 4
  %ans.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"struct.draco::AnsDecoder"* %ans, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %2 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf1 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %2, i32 0, i32 0
  store i8* %1, i8** %buf1, align 4
  %3 = load i8*, i8** %buf.addr, align 4
  %4 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %4, 1
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %sub
  %5 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %5 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %6 = load i32, i32* %x, align 4
  %cmp2 = icmp eq i32 %6, 0
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %7 = load i32, i32* %offset.addr, align 4
  %sub4 = sub nsw i32 %7, 1
  %8 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %8, i32 0, i32 1
  store i32 %sub4, i32* %buf_offset, align 4
  %9 = load i8*, i8** %buf.addr, align 4
  %10 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %10, 1
  %arrayidx6 = getelementptr inbounds i8, i8* %9, i32 %sub5
  %11 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %11 to i32
  %and = and i32 %conv7, 63
  %12 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %12, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end34

if.else:                                          ; preds = %if.end
  %13 = load i32, i32* %x, align 4
  %cmp8 = icmp eq i32 %13, 1
  br i1 %cmp8, label %if.then9, label %if.else18

if.then9:                                         ; preds = %if.else
  %14 = load i32, i32* %offset.addr, align 4
  %cmp10 = icmp slt i32 %14, 2
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.then9
  store i32 1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.then9
  %15 = load i32, i32* %offset.addr, align 4
  %sub13 = sub nsw i32 %15, 2
  %16 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %16, i32 0, i32 1
  store i32 %sub13, i32* %buf_offset14, align 4
  %17 = load i8*, i8** %buf.addr, align 4
  %18 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %18
  %add.ptr15 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr15)
  %and16 = and i32 %call, 16383
  %19 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state17 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %19, i32 0, i32 2
  store i32 %and16, i32* %state17, align 4
  br label %if.end33

if.else18:                                        ; preds = %if.else
  %20 = load i32, i32* %x, align 4
  %cmp19 = icmp eq i32 %20, 2
  br i1 %cmp19, label %if.then20, label %if.else31

if.then20:                                        ; preds = %if.else18
  %21 = load i32, i32* %offset.addr, align 4
  %cmp21 = icmp slt i32 %21, 3
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.then20
  store i32 1, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.then20
  %22 = load i32, i32* %offset.addr, align 4
  %sub24 = sub nsw i32 %22, 3
  %23 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset25 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %23, i32 0, i32 1
  store i32 %sub24, i32* %buf_offset25, align 4
  %24 = load i8*, i8** %buf.addr, align 4
  %25 = load i32, i32* %offset.addr, align 4
  %add.ptr26 = getelementptr inbounds i8, i8* %24, i32 %25
  %add.ptr27 = getelementptr inbounds i8, i8* %add.ptr26, i32 -3
  %call28 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr27)
  %and29 = and i32 %call28, 4194303
  %26 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state30 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %26, i32 0, i32 2
  store i32 %and29, i32* %state30, align 4
  br label %if.end32

if.else31:                                        ; preds = %if.else18
  store i32 1, i32* %retval, align 4
  br label %return

if.end32:                                         ; preds = %if.end23
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.end12
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then3
  %27 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state35 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %27, i32 0, i32 2
  %28 = load i32, i32* %state35, align 4
  %add = add i32 %28, 4096
  store i32 %add, i32* %state35, align 4
  %29 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state36 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %29, i32 0, i32 2
  %30 = load i32, i32* %state36, align 4
  %cmp37 = icmp uge i32 %30, 1048576
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.end34
  store i32 1, i32* %retval, align 4
  br label %return

if.end39:                                         ; preds = %if.end34
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end39, %if.then38, %if.else31, %if.then22, %if.then11, %if.then
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %data_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %this, i64 %bytes) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes.addr = alloca i64, align 8
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i64 %bytes, i64* %bytes.addr, align 8
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i64, i64* %bytes.addr, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, %0
  store i64 %add, i64* %pos_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco22AdaptiveRAnsBitDecoder13DecodeNextBitEv(%"class.draco::AdaptiveRAnsBitDecoder"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::AdaptiveRAnsBitDecoder"*, align 4
  %p0 = alloca i8, align 1
  %bit = alloca i8, align 1
  store %"class.draco::AdaptiveRAnsBitDecoder"* %this, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::AdaptiveRAnsBitDecoder"*, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  %p0_f_ = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 1
  %0 = load double, double* %p0_f_, align 8
  %call = call zeroext i8 @_ZN5draco17clamp_probabilityEd(double %0)
  store i8 %call, i8* %p0, align 1
  %ans_decoder_ = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 0
  %1 = load i8, i8* %p0, align 1
  %call2 = call i32 @_ZN5dracoL14rabs_desc_readEPNS_10AnsDecoderEh(%"struct.draco::AnsDecoder"* %ans_decoder_, i8 zeroext %1)
  %tobool = icmp ne i32 %call2, 0
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %bit, align 1
  %p0_f_3 = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 1
  %2 = load double, double* %p0_f_3, align 8
  %3 = load i8, i8* %bit, align 1
  %tobool4 = trunc i8 %3 to i1
  %call5 = call double @_ZN5draco18update_probabilityEdb(double %2, i1 zeroext %tobool4)
  %p0_f_6 = getelementptr inbounds %"class.draco::AdaptiveRAnsBitDecoder", %"class.draco::AdaptiveRAnsBitDecoder"* %this1, i32 0, i32 1
  store double %call5, double* %p0_f_6, align 8
  %4 = load i8, i8* %bit, align 1
  %tobool7 = trunc i8 %4 to i1
  ret i1 %tobool7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i8 @_ZN5draco17clamp_probabilityEd(double %p) #0 comdat {
entry:
  %p.addr = alloca double, align 8
  %p_int = alloca i32, align 4
  store double %p, double* %p.addr, align 8
  %0 = load double, double* %p.addr, align 8
  %mul = fmul double %0, 2.560000e+02
  %add = fadd double %mul, 5.000000e-01
  %conv = fptoui double %add to i32
  store i32 %conv, i32* %p_int, align 4
  %1 = load i32, i32* %p_int, align 4
  %cmp = icmp eq i32 %1, 256
  %conv1 = zext i1 %cmp to i32
  %2 = load i32, i32* %p_int, align 4
  %sub = sub i32 %2, %conv1
  store i32 %sub, i32* %p_int, align 4
  %3 = load i32, i32* %p_int, align 4
  %cmp2 = icmp eq i32 %3, 0
  %conv3 = zext i1 %cmp2 to i32
  %4 = load i32, i32* %p_int, align 4
  %add4 = add i32 %4, %conv3
  store i32 %add4, i32* %p_int, align 4
  %5 = load i32, i32* %p_int, align 4
  %conv5 = trunc i32 %5 to i8
  ret i8 %conv5
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL14rabs_desc_readEPNS_10AnsDecoderEh(%"struct.draco::AnsDecoder"* %ans, i8 zeroext %p0) #0 {
entry:
  %ans.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  %p0.addr = alloca i8, align 1
  %val = alloca i32, align 4
  %quot = alloca i32, align 4
  %rem = alloca i32, align 4
  %x = alloca i32, align 4
  %xn = alloca i32, align 4
  %p = alloca i8, align 1
  store %"struct.draco::AnsDecoder"* %ans, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  store i8 %p0, i8* %p0.addr, align 1
  %0 = load i8, i8* %p0.addr, align 1
  %conv = zext i8 %0 to i32
  %sub = sub i32 256, %conv
  %conv1 = trunc i32 %sub to i8
  store i8 %conv1, i8* %p, align 1
  %1 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %1, i32 0, i32 2
  %2 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %2, 4096
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %3, i32 0, i32 1
  %4 = load i32, i32* %buf_offset, align 4
  %cmp2 = icmp sgt i32 %4, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %5 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state3 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %5, i32 0, i32 2
  %6 = load i32, i32* %state3, align 4
  %mul = mul i32 %6, 256
  %7 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %7, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4
  %9 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset4 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %9, i32 0, i32 1
  %10 = load i32, i32* %buf_offset4, align 4
  %dec = add nsw i32 %10, -1
  store i32 %dec, i32* %buf_offset4, align 4
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %dec
  %11 = load i8, i8* %arrayidx, align 1
  %conv5 = zext i8 %11 to i32
  %add = add i32 %mul, %conv5
  %12 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state6 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %12, i32 0, i32 2
  store i32 %add, i32* %state6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %13 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state7 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %13, i32 0, i32 2
  %14 = load i32, i32* %state7, align 4
  store i32 %14, i32* %x, align 4
  %15 = load i32, i32* %x, align 4
  %div = udiv i32 %15, 256
  store i32 %div, i32* %quot, align 4
  %16 = load i32, i32* %x, align 4
  %rem8 = urem i32 %16, 256
  store i32 %rem8, i32* %rem, align 4
  %17 = load i32, i32* %quot, align 4
  %18 = load i8, i8* %p, align 1
  %conv9 = zext i8 %18 to i32
  %mul10 = mul i32 %17, %conv9
  store i32 %mul10, i32* %xn, align 4
  %19 = load i32, i32* %rem, align 4
  %20 = load i8, i8* %p, align 1
  %conv11 = zext i8 %20 to i32
  %cmp12 = icmp ult i32 %19, %conv11
  %conv13 = zext i1 %cmp12 to i32
  store i32 %conv13, i32* %val, align 4
  %21 = load i32, i32* %val, align 4
  %tobool = icmp ne i32 %21, 0
  br i1 %tobool, label %if.then14, label %if.else

if.then14:                                        ; preds = %if.end
  %22 = load i32, i32* %xn, align 4
  %23 = load i32, i32* %rem, align 4
  %add15 = add i32 %22, %23
  %24 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state16 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %24, i32 0, i32 2
  store i32 %add15, i32* %state16, align 4
  br label %if.end21

if.else:                                          ; preds = %if.end
  %25 = load i32, i32* %x, align 4
  %26 = load i32, i32* %xn, align 4
  %sub17 = sub i32 %25, %26
  %27 = load i8, i8* %p, align 1
  %conv18 = zext i8 %27 to i32
  %sub19 = sub i32 %sub17, %conv18
  %28 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state20 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %28, i32 0, i32 2
  store i32 %sub19, i32* %state20, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.else, %if.then14
  %29 = load i32, i32* %val, align 4
  ret i32 %29
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden double @_ZN5draco18update_probabilityEdb(double %old_p, i1 zeroext %bit) #0 comdat {
entry:
  %old_p.addr = alloca double, align 8
  %bit.addr = alloca i8, align 1
  store double %old_p, double* %old_p.addr, align 8
  %frombool = zext i1 %bit to i8
  store i8 %frombool, i8* %bit.addr, align 1
  %0 = load double, double* %old_p.addr, align 8
  %mul = fmul double %0, 0x3FEFC00000000000
  %1 = load i8, i8* %bit.addr, align 1
  %tobool = trunc i8 %1 to i1
  %lnot = xor i1 %tobool, true
  %conv = zext i1 %lnot to i32
  %conv1 = sitofp i32 %conv to double
  %mul2 = fmul double %conv1, 7.812500e-03
  %add = fadd double %mul, %mul2
  ret double %add
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco22AdaptiveRAnsBitDecoder28DecodeLeastSignificantBits32EiPj(%"class.draco::AdaptiveRAnsBitDecoder"* %this, i32 %nbits, i32* %value) #0 {
entry:
  %this.addr = alloca %"class.draco::AdaptiveRAnsBitDecoder"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  %result = alloca i32, align 4
  store %"class.draco::AdaptiveRAnsBitDecoder"* %this, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::AdaptiveRAnsBitDecoder"*, %"class.draco::AdaptiveRAnsBitDecoder"** %this.addr, align 4
  store i32 0, i32* %result, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %nbits.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %result, align 4
  %shl = shl i32 %1, 1
  %call = call zeroext i1 @_ZN5draco22AdaptiveRAnsBitDecoder13DecodeNextBitEv(%"class.draco::AdaptiveRAnsBitDecoder"* %this1)
  %conv = zext i1 %call to i32
  %add = add i32 %shl, %conv
  store i32 %add, i32* %result, align 4
  %2 = load i32, i32* %nbits.addr, align 4
  %dec = add nsw i32 %2, -1
  store i32 %dec, i32* %nbits.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %3 = load i32, i32* %result, align 4
  %4 = load i32*, i32** %value.addr, align 4
  store i32 %3, i32* %4, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12ans_read_endEPNS_10AnsDecoderE(%"struct.draco::AnsDecoder"* %ans) #0 {
entry:
  %ans.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  store %"struct.draco::AnsDecoder"* %ans, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %0 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %0, i32 0, i32 2
  %1 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %1, 4096
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %vmem) #0 {
entry:
  %vmem.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %mem = alloca i8*, align 4
  store i8* %vmem, i8** %vmem.addr, align 4
  %0 = load i8*, i8** %vmem.addr, align 4
  store i8* %0, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 1
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %val, align 4
  %3 = load i8*, i8** %mem, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %5 = load i32, i32* %val, align 4
  %or = or i32 %5, %conv2
  store i32 %or, i32* %val, align 4
  %6 = load i32, i32* %val, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %vmem) #0 {
entry:
  %vmem.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %mem = alloca i8*, align 4
  store i8* %vmem, i8** %vmem.addr, align 4
  %0 = load i8*, i8** %vmem.addr, align 4
  store i8* %0, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 2
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 16
  store i32 %shl, i32* %val, align 4
  %3 = load i8*, i8** %mem, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %shl3 = shl i32 %conv2, 8
  %5 = load i32, i32* %val, align 4
  %or = or i32 %5, %shl3
  store i32 %or, i32* %val, align 4
  %6 = load i8*, i8** %mem, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %6, i32 0
  %7 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %7 to i32
  %8 = load i32, i32* %val, align 4
  %or6 = or i32 %8, %conv5
  store i32 %or6, i32* %val, align 4
  %9 = load i32, i32* %val, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 4, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = bitcast i32* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 1 %add.ptr, i32 4, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
