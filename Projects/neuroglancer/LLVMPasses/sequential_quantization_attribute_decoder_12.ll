; ModuleID = './draco/src/draco/compression/attributes/sequential_quantization_attribute_decoder.cc'
source_filename = "./draco/src/draco/compression/attributes/sequential_quantization_attribute_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::SequentialQuantizationAttributeDecoder" = type { %"class.draco::SequentialIntegerAttributeDecoder", i32, %"class.std::__2::unique_ptr.111", float }
%"class.draco::SequentialIntegerAttributeDecoder" = type { %"class.draco::SequentialAttributeDecoder", %"class.std::__2::unique_ptr.106" }
%"class.draco::SequentialAttributeDecoder" = type { i32 (...)**, %"class.draco::PointCloudDecoder"*, %"class.draco::PointAttribute"*, i32, %"class.std::__2::unique_ptr.53" }
%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.94", %"class.std::__2::vector.87", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.101" }
%"class.std::__2::unique_ptr.96" = type { %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::__compressed_pair.101" = type { %"struct.std::__2::__compressed_pair_elem.102" }
%"struct.std::__2::__compressed_pair_elem.102" = type { %"class.std::__2::unique_ptr.96"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type opaque
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.std::__2::unique_ptr.106" = type { %"class.std::__2::__compressed_pair.107" }
%"class.std::__2::__compressed_pair.107" = type { %"struct.std::__2::__compressed_pair_elem.108" }
%"struct.std::__2::__compressed_pair_elem.108" = type { %"class.draco::PredictionSchemeTypedDecoderInterface"* }
%"class.draco::PredictionSchemeTypedDecoderInterface" = type { %"class.draco::PredictionSchemeDecoderInterface" }
%"class.draco::PredictionSchemeDecoderInterface" = type { %"class.draco::PredictionSchemeInterface" }
%"class.draco::PredictionSchemeInterface" = type { i32 (...)** }
%"class.std::__2::unique_ptr.111" = type { %"class.std::__2::__compressed_pair.112" }
%"class.std::__2::__compressed_pair.112" = type { %"struct.std::__2::__compressed_pair_elem.113" }
%"struct.std::__2::__compressed_pair_elem.113" = type { float* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::vector.116" = type opaque
%"class.draco::AttributeQuantizationTransform" = type { %"class.draco::AttributeTransform", i32, %"class.std::__2::vector.117", float }
%"class.draco::AttributeTransform" = type { i32 (...)** }
%"class.std::__2::vector.117" = type { %"class.std::__2::__vector_base.118" }
%"class.std::__2::__vector_base.118" = type { float*, float*, %"class.std::__2::__compressed_pair.119" }
%"class.std::__2::__compressed_pair.119" = type { %"struct.std::__2::__compressed_pair_elem.113" }
%"struct.std::__2::default_delete.115" = type { i8 }
%"class.draco::Dequantizer" = type { float }
%"struct.std::__2::__compressed_pair_elem.114" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.120" = type { i8 }
%"class.std::__2::allocator.121" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::default_delete.110" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.109" = type { i8 }
%"struct.std::__2::default_delete.81" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.80" = type { i8 }
%"struct.std::__2::default_delete.79" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.78" = type { i8 }
%"class.std::__2::allocator.61" = type { i8 }
%"struct.std::__2::__has_destroy.123" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.60" = type { i8 }
%"class.std::__2::allocator.73" = type { i8 }
%"struct.std::__2::__has_destroy.124" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.72" = type { i8 }
%"struct.std::__2::default_delete.67" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.66" = type { i8 }

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2ILb1EvEEv = comdat any

$_ZN5draco17PointCloudDecoder11point_cloudEv = comdat any

$_ZN5draco10PointCloud9attributeEi = comdat any

$_ZNK5draco17GeometryAttribute9data_typeEv = comdat any

$_ZNK5draco26SequentialAttributeDecoder7decoderEv = comdat any

$_ZNK5draco17PointCloudDecoder17bitstream_versionEv = comdat any

$_ZN5draco30AttributeQuantizationTransformC2Ev = comdat any

$_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv = comdat any

$_ZN5draco26SequentialAttributeDecoder9attributeEv = comdat any

$_ZNK5draco17GeometryAttribute14num_componentsEv = comdat any

$_ZN5draco26SequentialAttributeDecoder18portable_attributeEv = comdat any

$_ZN5draco30AttributeQuantizationTransformD2Ev = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_ = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEaSEOS4_ = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev = comdat any

$_ZN5draco17PointCloudDecoder6bufferEv = comdat any

$_ZN5draco13DecoderBuffer6DecodeEPvm = comdat any

$_ZN5draco13DecoderBuffer6DecodeIfEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZN5draco33SequentialIntegerAttributeDecoder24GetPortableAttributeDataEv = comdat any

$_ZNK5draco11Dequantizer15DequantizeFloatEi = comdat any

$_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm = comdat any

$_ZNK5draco14PointAttribute6bufferEv = comdat any

$_ZN5draco10DataBuffer5WriteExPKvm = comdat any

$_ZN5draco38SequentialQuantizationAttributeDecoderD2Ev = comdat any

$_ZN5draco38SequentialQuantizationAttributeDecoderD0Ev = comdat any

$_ZNK5draco33SequentialIntegerAttributeDecoder21GetNumValueComponentsEv = comdat any

$_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IS1_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPfEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IS1_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZN5draco18AttributeTransformC2Ev = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEEC2Ev = comdat any

$_ZN5draco18AttributeTransformD2Ev = comdat any

$_ZN5draco18AttributeTransformD0Ev = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIfEC2Ev = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEED2Ev = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEED2Ev = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIfEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE17__destruct_at_endEPf = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE7destroyIfEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9__destroyIfEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIfE7destroyEPf = comdat any

$_ZNSt3__29allocatorIfE10deallocateEPfm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv = comdat any

$_ZNK5draco14PointAttribute4sizeEv = comdat any

$_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZN5draco33SequentialIntegerAttributeDecoderD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEED2Ev = comdat any

$_ZN5draco26SequentialAttributeDecoderD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE5resetEPS3_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEEEclEPS3_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco14PointAttributeD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco22AttributeTransformDataD2Ev = comdat any

$_ZN5draco10DataBufferD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv = comdat any

$_ZNK5draco26SequentialAttributeDecoder9attributeEv = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIA_fEclIfEENS2_20_EnableIfConvertibleIT_E4typeEPS5_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IRS1_vEEOT_ = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetIPfEENS_9enable_ifIXsr28_CheckArrayPointerConversionIT_EE5valueEvE4typeES8_ = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIA_fEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE11get_deleterEv = comdat any

$_ZN5draco13DecoderBuffer4PeekIfEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

$_ZTVN5draco18AttributeTransformE = comdat any

@_ZTVN5draco38SequentialQuantizationAttributeDecoderE = hidden unnamed_addr constant { [17 x i8*] } { [17 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::SequentialQuantizationAttributeDecoder"* (%"class.draco::SequentialQuantizationAttributeDecoder"*)* @_ZN5draco38SequentialQuantizationAttributeDecoderD2Ev to i8*), i8* bitcast (void (%"class.draco::SequentialQuantizationAttributeDecoder"*)* @_ZN5draco38SequentialQuantizationAttributeDecoderD0Ev to i8*), i8* bitcast (i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32)* @_ZN5draco38SequentialQuantizationAttributeDecoder4InitEPNS_17PointCloudDecoderEi to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointAttribute"*)* @_ZN5draco26SequentialAttributeDecoder20InitializeStandaloneEPNS_14PointAttributeE to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.116"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco26SequentialAttributeDecoder23DecodePortableAttributeERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE to i8*), i8* bitcast (i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.std::__2::vector.116"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco38SequentialQuantizationAttributeDecoder35DecodeDataNeededByPortableTransformERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE to i8*), i8* bitcast (i1 (%"class.draco::SequentialIntegerAttributeDecoder"*, %"class.std::__2::vector.116"*)* @_ZN5draco33SequentialIntegerAttributeDecoder34TransformAttributeToOriginalFormatERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEE to i8*), i8* bitcast (i1 (%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PredictionSchemeInterface"*)* @_ZN5draco26SequentialAttributeDecoder20InitPredictionSchemeEPNS_25PredictionSchemeInterfaceE to i8*), i8* bitcast (i1 (%"class.draco::SequentialIntegerAttributeDecoder"*, %"class.std::__2::vector.116"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco33SequentialIntegerAttributeDecoder12DecodeValuesERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE to i8*), i8* bitcast (i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.std::__2::vector.116"*, %"class.draco::DecoderBuffer"*)* @_ZN5draco38SequentialQuantizationAttributeDecoder19DecodeIntegerValuesERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE to i8*), i8* bitcast (void (%"class.std::__2::unique_ptr.106"*, %"class.draco::SequentialIntegerAttributeDecoder"*, i32, i32)* @_ZN5draco33SequentialIntegerAttributeDecoder25CreateIntPredictionSchemeENS_22PredictionSchemeMethodENS_29PredictionSchemeTransformTypeE to i8*), i8* bitcast (i32 (%"class.draco::SequentialIntegerAttributeDecoder"*)* @_ZNK5draco33SequentialIntegerAttributeDecoder21GetNumValueComponentsEv to i8*), i8* bitcast (i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)* @_ZN5draco38SequentialQuantizationAttributeDecoder11StoreValuesEj to i8*), i8* bitcast (i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)* @_ZN5draco38SequentialQuantizationAttributeDecoder23DecodeQuantizedDataInfoEv to i8*), i8* bitcast (i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)* @_ZN5draco38SequentialQuantizationAttributeDecoder16DequantizeValuesEj to i8*)] }, align 4
@_ZTVN5draco30AttributeQuantizationTransformE = external unnamed_addr constant { [7 x i8*] }, align 4
@_ZTVN5draco18AttributeTransformE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::AttributeTransform"* (%"class.draco::AttributeTransform"*)* @_ZN5draco18AttributeTransformD2Ev to i8*), i8* bitcast (void (%"class.draco::AttributeTransform"*)* @_ZN5draco18AttributeTransformD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN5draco33SequentialIntegerAttributeDecoderE = external unnamed_addr constant { [15 x i8*] }, align 4
@_ZTVN5draco26SequentialAttributeDecoderE = external unnamed_addr constant { [11 x i8*] }, align 4

@_ZN5draco38SequentialQuantizationAttributeDecoderC1Ev = hidden unnamed_addr alias %"class.draco::SequentialQuantizationAttributeDecoder"* (%"class.draco::SequentialQuantizationAttributeDecoder"*), %"class.draco::SequentialQuantizationAttributeDecoder"* (%"class.draco::SequentialQuantizationAttributeDecoder"*)* @_ZN5draco38SequentialQuantizationAttributeDecoderC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::SequentialQuantizationAttributeDecoder"* @_ZN5draco38SequentialQuantizationAttributeDecoderC2Ev(%"class.draco::SequentialQuantizationAttributeDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialIntegerAttributeDecoder"*
  %call = call %"class.draco::SequentialIntegerAttributeDecoder"* @_ZN5draco33SequentialIntegerAttributeDecoderC2Ev(%"class.draco::SequentialIntegerAttributeDecoder"* %0)
  %1 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTVN5draco38SequentialQuantizationAttributeDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 1
  store i32 -1, i32* %quantization_bits_, align 4
  %min_value_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.111"* %min_value_) #7
  %max_value_dif_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %max_value_dif_, align 4
  ret %"class.draco::SequentialQuantizationAttributeDecoder"* %this1
}

declare %"class.draco::SequentialIntegerAttributeDecoder"* @_ZN5draco33SequentialIntegerAttributeDecoderC2Ev(%"class.draco::SequentialIntegerAttributeDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.111"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  %ref.tmp = alloca float*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  store float* null, float** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.112"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.112"* %__ptr_, float** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.111"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco38SequentialQuantizationAttributeDecoder4InitEPNS_17PointCloudDecoderEi(%"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::PointCloudDecoder"* %decoder, i32 %attribute_id) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  %decoder.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %attribute_id.addr = alloca i32, align 4
  %attribute = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  store %"class.draco::PointCloudDecoder"* %decoder, %"class.draco::PointCloudDecoder"** %decoder.addr, align 4
  store i32 %attribute_id, i32* %attribute_id.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialIntegerAttributeDecoder"*
  %1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %decoder.addr, align 4
  %2 = load i32, i32* %attribute_id.addr, align 4
  %call = call zeroext i1 @_ZN5draco33SequentialIntegerAttributeDecoder4InitEPNS_17PointCloudDecoderEi(%"class.draco::SequentialIntegerAttributeDecoder"* %0, %"class.draco::PointCloudDecoder"* %1, i32 %2)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %decoder.addr, align 4
  %call2 = call %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %3)
  %4 = load i32, i32* %attribute_id.addr, align 4
  %call3 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %call2, i32 %4)
  store %"class.draco::PointAttribute"* %call3, %"class.draco::PointAttribute"** %attribute, align 4
  %5 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute, align 4
  %6 = bitcast %"class.draco::PointAttribute"* %5 to %"class.draco::GeometryAttribute"*
  %call4 = call i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %6)
  %cmp = icmp ne i32 %call4, 9
  br i1 %cmp, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.end
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then
  %7 = load i1, i1* %retval, align 1
  ret i1 %7
}

declare zeroext i1 @_ZN5draco33SequentialIntegerAttributeDecoder4InitEPNS_17PointCloudDecoderEi(%"class.draco::SequentialIntegerAttributeDecoder"*, %"class.draco::PointCloudDecoder"*, i32) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %point_cloud_, align 4
  ret %"class.draco::PointCloud"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %this, i32 %att_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %0 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_, i32 %0) #7
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %call) #7
  ret %"class.draco::PointAttribute"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %data_type_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco38SequentialQuantizationAttributeDecoder19DecodeIntegerValuesERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE(%"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.std::__2::vector.116"* nonnull align 1 %point_ids, %"class.draco::DecoderBuffer"* %in_buffer) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  %point_ids.addr = alloca %"class.std::__2::vector.116"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  store %"class.std::__2::vector.116"* %point_ids, %"class.std::__2::vector.116"** %point_ids.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call = call %"class.draco::PointCloudDecoder"* @_ZNK5draco26SequentialAttributeDecoder7decoderEv(%"class.draco::SequentialAttributeDecoder"* %0)
  %call2 = call zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %call)
  %conv = zext i16 %call2 to i32
  %cmp = icmp slt i32 %conv, 512
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)***
  %vtable = load i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)**, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)*, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)** %vtable, i64 13
  %2 = load i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)*, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)** %vfn, align 4
  %call3 = call zeroext i1 %2(%"class.draco::SequentialQuantizationAttributeDecoder"* %this1)
  br i1 %call3, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %3 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialIntegerAttributeDecoder"*
  %4 = load %"class.std::__2::vector.116"*, %"class.std::__2::vector.116"** %point_ids.addr, align 4
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco33SequentialIntegerAttributeDecoder19DecodeIntegerValuesERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE(%"class.draco::SequentialIntegerAttributeDecoder"* %3, %"class.std::__2::vector.116"* nonnull align 1 %4, %"class.draco::DecoderBuffer"* %5)
  store i1 %call4, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudDecoder"* @_ZNK5draco26SequentialAttributeDecoder7decoderEv(%"class.draco::SequentialAttributeDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.draco::SequentialAttributeDecoder"* %this, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %decoder_ = getelementptr inbounds %"class.draco::SequentialAttributeDecoder", %"class.draco::SequentialAttributeDecoder"* %this1, i32 0, i32 1
  %0 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %decoder_, align 4
  ret %"class.draco::PointCloudDecoder"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %version_major_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  %0 = load i8, i8* %version_major_, align 4
  %conv = zext i8 %0 to i16
  %conv2 = zext i16 %conv to i32
  %shl = shl i32 %conv2, 8
  %version_minor_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 6
  %1 = load i8, i8* %version_minor_, align 1
  %conv3 = zext i8 %1 to i32
  %or = or i32 %shl, %conv3
  %conv4 = trunc i32 %or to i16
  ret i16 %conv4
}

declare zeroext i1 @_ZN5draco33SequentialIntegerAttributeDecoder19DecodeIntegerValuesERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE(%"class.draco::SequentialIntegerAttributeDecoder"*, %"class.std::__2::vector.116"* nonnull align 1, %"class.draco::DecoderBuffer"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco38SequentialQuantizationAttributeDecoder35DecodeDataNeededByPortableTransformERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE(%"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.std::__2::vector.116"* nonnull align 1 %point_ids, %"class.draco::DecoderBuffer"* %in_buffer) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  %point_ids.addr = alloca %"class.std::__2::vector.116"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %transform = alloca %"class.draco::AttributeQuantizationTransform", align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  store %"class.std::__2::vector.116"* %point_ids, %"class.std::__2::vector.116"** %point_ids.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call = call %"class.draco::PointCloudDecoder"* @_ZNK5draco26SequentialAttributeDecoder7decoderEv(%"class.draco::SequentialAttributeDecoder"* %0)
  %call2 = call zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %call)
  %conv = zext i16 %call2 to i32
  %cmp = icmp sge i32 %conv, 512
  br i1 %cmp, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)***
  %vtable = load i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)**, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)*, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)** %vtable, i64 13
  %2 = load i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)*, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*)** %vfn, align 4
  %call3 = call zeroext i1 %2(%"class.draco::SequentialQuantizationAttributeDecoder"* %this1)
  br i1 %call3, label %if.end, label %if.then4

if.then4:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  %call6 = call %"class.draco::AttributeQuantizationTransform"* @_ZN5draco30AttributeQuantizationTransformC2Ev(%"class.draco::AttributeQuantizationTransform"* %transform)
  %quantization_bits_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 1
  %3 = load i32, i32* %quantization_bits_, align 4
  %min_value_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 2
  %call7 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.111"* %min_value_) #7
  %4 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call8 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %4)
  %5 = bitcast %"class.draco::PointAttribute"* %call8 to %"class.draco::GeometryAttribute"*
  %call9 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %5)
  %conv10 = sext i8 %call9 to i32
  %max_value_dif_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 3
  %6 = load float, float* %max_value_dif_, align 4
  call void @_ZN5draco30AttributeQuantizationTransform13SetParametersEiPKfif(%"class.draco::AttributeQuantizationTransform"* %transform, i32 %3, float* %call7, i32 %conv10, float %6)
  %7 = bitcast %"class.draco::AttributeQuantizationTransform"* %transform to %"class.draco::AttributeTransform"*
  %8 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call11 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder18portable_attributeEv(%"class.draco::SequentialAttributeDecoder"* %8)
  %call12 = call zeroext i1 @_ZNK5draco18AttributeTransform19TransferToAttributeEPNS_14PointAttributeE(%"class.draco::AttributeTransform"* %7, %"class.draco::PointAttribute"* %call11)
  store i1 %call12, i1* %retval, align 1
  %call13 = call %"class.draco::AttributeQuantizationTransform"* @_ZN5draco30AttributeQuantizationTransformD2Ev(%"class.draco::AttributeQuantizationTransform"* %transform) #7
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeQuantizationTransform"* @_ZN5draco30AttributeQuantizationTransformC2Ev(%"class.draco::AttributeQuantizationTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to %"class.draco::AttributeTransform"*
  %call = call %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformC2Ev(%"class.draco::AttributeTransform"* %0) #7
  %1 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN5draco30AttributeQuantizationTransformE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  store i32 -1, i32* %quantization_bits_, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.117"* @_ZNSt3__26vectorIfNS_9allocatorIfEEEC2Ev(%"class.std::__2::vector.117"* %min_values_) #7
  %range_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %range_, align 4
  ret %"class.draco::AttributeQuantizationTransform"* %this1
}

declare void @_ZN5draco30AttributeQuantizationTransform13SetParametersEiPKfif(%"class.draco::AttributeQuantizationTransform"*, i32, float*, i32, float) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_) #7
  %0 = load float*, float** %call, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.draco::SequentialAttributeDecoder"* %this, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %attribute_ = getelementptr inbounds %"class.draco::SequentialAttributeDecoder", %"class.draco::SequentialAttributeDecoder"* %this1, i32 0, i32 2
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute_, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  %0 = load i8, i8* %num_components_, align 8
  ret i8 %0
}

declare zeroext i1 @_ZNK5draco18AttributeTransform19TransferToAttributeEPNS_14PointAttributeE(%"class.draco::AttributeTransform"*, %"class.draco::PointAttribute"*) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder18portable_attributeEv(%"class.draco::SequentialAttributeDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.draco::SequentialAttributeDecoder"* %this, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %portable_attribute_ = getelementptr inbounds %"class.draco::SequentialAttributeDecoder", %"class.draco::SequentialAttributeDecoder"* %this1, i32 0, i32 4
  %call = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %portable_attribute_) #7
  ret %"class.draco::PointAttribute"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeQuantizationTransform"* @_ZN5draco30AttributeQuantizationTransformD2Ev(%"class.draco::AttributeQuantizationTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN5draco30AttributeQuantizationTransformE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call = call %"class.std::__2::vector.117"* @_ZNSt3__26vectorIfNS_9allocatorIfEEED2Ev(%"class.std::__2::vector.117"* %min_values_) #7
  %1 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to %"class.draco::AttributeTransform"*
  %call2 = call %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformD2Ev(%"class.draco::AttributeTransform"* %1) #7
  ret %"class.draco::AttributeQuantizationTransform"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco38SequentialQuantizationAttributeDecoder11StoreValuesEj(%"class.draco::SequentialQuantizationAttributeDecoder"* %this, i32 %num_values) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  %num_values.addr = alloca i32, align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %0 = load i32, i32* %num_values.addr, align 4
  %1 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)***
  %vtable = load i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)**, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)*, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)** %vtable, i64 14
  %2 = load i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)*, i1 (%"class.draco::SequentialQuantizationAttributeDecoder"*, i32)** %vfn, align 4
  %call = call zeroext i1 %2(%"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 %0)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco38SequentialQuantizationAttributeDecoder23DecodeQuantizedDataInfoEv(%"class.draco::SequentialQuantizationAttributeDecoder"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  %num_components = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr.111", align 4
  %quantization_bits = alloca i8, align 1
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %0)
  %1 = bitcast %"class.draco::PointAttribute"* %call to %"class.draco::GeometryAttribute"*
  %call2 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %1)
  %conv = sext i8 %call2 to i32
  store i32 %conv, i32* %num_components, align 4
  %2 = load i32, i32* %num_components, align 4
  %3 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %2, i32 4)
  %4 = extractvalue { i32, i1 } %3, 1
  %5 = extractvalue { i32, i1 } %3, 0
  %6 = select i1 %4, i32 -1, i32 %5
  %call3 = call noalias nonnull i8* @_Znam(i32 %6) #8
  %7 = bitcast i8* %call3 to float*
  %call4 = call %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.111"* %ref.tmp, float* %7) #7
  %min_value_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEaSEOS4_(%"class.std::__2::unique_ptr.111"* %min_value_, %"class.std::__2::unique_ptr.111"* nonnull align 4 dereferenceable(4) %ref.tmp) #7
  %call6 = call %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.111"* %ref.tmp) #7
  %8 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call7 = call %"class.draco::PointCloudDecoder"* @_ZNK5draco26SequentialAttributeDecoder7decoderEv(%"class.draco::SequentialAttributeDecoder"* %8)
  %call8 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %call7)
  %min_value_9 = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 2
  %call10 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.111"* %min_value_9) #7
  %9 = bitcast float* %call10 to i8*
  %10 = load i32, i32* %num_components, align 4
  %mul = mul i32 4, %10
  %call11 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeEPvm(%"class.draco::DecoderBuffer"* %call8, i8* %9, i32 %mul)
  br i1 %call11, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %11 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call12 = call %"class.draco::PointCloudDecoder"* @_ZNK5draco26SequentialAttributeDecoder7decoderEv(%"class.draco::SequentialAttributeDecoder"* %11)
  %call13 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %call12)
  %max_value_dif_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 3
  %call14 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIfEEbPT_(%"class.draco::DecoderBuffer"* %call13, float* %max_value_dif_)
  br i1 %call14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %if.end
  %12 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call17 = call %"class.draco::PointCloudDecoder"* @_ZNK5draco26SequentialAttributeDecoder7decoderEv(%"class.draco::SequentialAttributeDecoder"* %12)
  %call18 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %call17)
  %call19 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %call18, i8* %quantization_bits)
  br i1 %call19, label %lor.lhs.false, label %if.then21

lor.lhs.false:                                    ; preds = %if.end16
  %13 = load i8, i8* %quantization_bits, align 1
  %conv20 = zext i8 %13 to i32
  %cmp = icmp sgt i32 %conv20, 31
  br i1 %cmp, label %if.then21, label %if.end22

if.then21:                                        ; preds = %lor.lhs.false, %if.end16
  store i1 false, i1* %retval, align 1
  br label %return

if.end22:                                         ; preds = %lor.lhs.false
  %14 = load i8, i8* %quantization_bits, align 1
  %conv23 = zext i8 %14 to i32
  %quantization_bits_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 1
  store i32 %conv23, i32* %quantization_bits_, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end22, %if.then21, %if.then15, %if.then
  %15 = load i1, i1* %retval, align 1
  ret i1 %15
}

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #2

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znam(i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.111"* returned %this, float* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  %__p.addr = alloca float*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.112"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.112"* %__ptr_, float** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.111"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEaSEOS4_(%"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.111"* %__u, %"class.std::__2::unique_ptr.111"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %__u.addr, align 4
  %call = call float* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE7releaseEv(%"class.std::__2::unique_ptr.111"* %0) #7
  call void @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetIPfEENS_9enable_ifIXsr28_CheckArrayPointerConversionIT_EE5valueEvE4typeES8_(%"class.std::__2::unique_ptr.111"* %this1, float* %call) #7
  %1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE11get_deleterEv(%"class.std::__2::unique_ptr.111"* %1) #7
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__27forwardINS_14default_deleteIA_fEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::default_delete.115"* nonnull align 1 dereferenceable(1) %call2) #7
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv(%"class.std::__2::__compressed_pair.112"* %__ptr_) #7
  ret %"class.std::__2::unique_ptr.111"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.111"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetEDn(%"class.std::__2::unique_ptr.111"* %this1, i8* null) #7
  ret %"class.std::__2::unique_ptr.111"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_, align 4
  ret %"class.draco::DecoderBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeEPvm(%"class.draco::DecoderBuffer"* %this, i8* %out_data, i32 %size_to_decode) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_data.addr = alloca i8*, align 4
  %size_to_decode.addr = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_data, i8** %out_data.addr, align 4
  store i32 %size_to_decode, i32* %size_to_decode.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %2 = load i32, i32* %size_to_decode.addr, align 4
  %conv = zext i32 %2 to i64
  %add = add nsw i64 %1, %conv
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** %out_data.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  %6 = load i32, i32* %size_to_decode.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %3, i8* align 1 %add.ptr, i32 %6, i1 false)
  %7 = load i32, i32* %size_to_decode.addr, align 4
  %conv3 = zext i32 %7 to i64
  %pos_4 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %8 = load i64, i64* %pos_4, align 8
  %add5 = add nsw i64 %8, %conv3
  store i64 %add5, i64* %pos_4, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIfEEbPT_(%"class.draco::DecoderBuffer"* %this, float* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca float*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store float* %out_val, float** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load float*, float** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIfEEbPT_(%"class.draco::DecoderBuffer"* %this1, float* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco38SequentialQuantizationAttributeDecoder16DequantizeValuesEj(%"class.draco::SequentialQuantizationAttributeDecoder"* %this, i32 %num_values) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  %num_values.addr = alloca i32, align 4
  %max_quantized_value = alloca i32, align 4
  %num_components = alloca i32, align 4
  %entry_size = alloca i32, align 4
  %att_val = alloca %"class.std::__2::unique_ptr.111", align 4
  %quant_val_id = alloca i32, align 4
  %out_byte_pos = alloca i32, align 4
  %dequantizer = alloca %"class.draco::Dequantizer", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %portable_attribute_data = alloca i32*, align 4
  %i = alloca i32, align 4
  %c = alloca i32, align 4
  %value = alloca float, align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  store i32 %num_values, i32* %num_values.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 1
  %0 = load i32, i32* %quantization_bits_, align 4
  %shl = shl i32 1, %0
  %sub = sub i32 %shl, 1
  store i32 %sub, i32* %max_quantized_value, align 4
  %1 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %1)
  %2 = bitcast %"class.draco::PointAttribute"* %call to %"class.draco::GeometryAttribute"*
  %call2 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %2)
  %conv = sext i8 %call2 to i32
  store i32 %conv, i32* %num_components, align 4
  %3 = load i32, i32* %num_components, align 4
  %mul = mul i32 4, %3
  store i32 %mul, i32* %entry_size, align 4
  %4 = load i32, i32* %num_components, align 4
  %5 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %4, i32 4)
  %6 = extractvalue { i32, i1 } %5, 1
  %7 = extractvalue { i32, i1 } %5, 0
  %8 = select i1 %6, i32 -1, i32 %7
  %call3 = call noalias nonnull i8* @_Znam(i32 %8) #8
  %9 = bitcast i8* %call3 to float*
  %call4 = call %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.111"* %att_val, float* %9) #7
  store i32 0, i32* %quant_val_id, align 4
  store i32 0, i32* %out_byte_pos, align 4
  %call5 = call %"class.draco::Dequantizer"* @_ZN5draco11DequantizerC1Ev(%"class.draco::Dequantizer"* %dequantizer)
  %max_value_dif_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 3
  %10 = load float, float* %max_value_dif_, align 4
  %11 = load i32, i32* %max_quantized_value, align 4
  %call6 = call zeroext i1 @_ZN5draco11Dequantizer4InitEfi(%"class.draco::Dequantizer"* %dequantizer, float %10, i32 %11)
  br i1 %call6, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %12 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialIntegerAttributeDecoder"*
  %call7 = call i32* @_ZN5draco33SequentialIntegerAttributeDecoder24GetPortableAttributeDataEv(%"class.draco::SequentialIntegerAttributeDecoder"* %12)
  store i32* %call7, i32** %portable_attribute_data, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc20, %if.end
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp ult i32 %13, %14
  br i1 %cmp, label %for.body, label %for.end22

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %c, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %15 = load i32, i32* %c, align 4
  %16 = load i32, i32* %num_components, align 4
  %cmp9 = icmp slt i32 %15, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load i32*, i32** %portable_attribute_data, align 4
  %18 = load i32, i32* %quant_val_id, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %quant_val_id, align 4
  %arrayidx = getelementptr inbounds i32, i32* %17, i32 %18
  %19 = load i32, i32* %arrayidx, align 4
  %call11 = call float @_ZNK5draco11Dequantizer15DequantizeFloatEi(%"class.draco::Dequantizer"* %dequantizer, i32 %19)
  store float %call11, float* %value, align 4
  %20 = load float, float* %value, align 4
  %min_value_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 2
  %21 = load i32, i32* %c, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.111"* %min_value_, i32 %21)
  %22 = load float, float* %call12, align 4
  %add = fadd float %20, %22
  store float %add, float* %value, align 4
  %23 = load float, float* %value, align 4
  %24 = load i32, i32* %c, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.111"* %att_val, i32 %24)
  store float %23, float* %call13, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %25 = load i32, i32* %c, align 4
  %inc14 = add nsw i32 %25, 1
  store i32 %inc14, i32* %c, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %26 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call15 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %26)
  %call16 = call %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %call15)
  %27 = load i32, i32* %out_byte_pos, align 4
  %conv17 = sext i32 %27 to i64
  %call18 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.111"* %att_val) #7
  %28 = bitcast float* %call18 to i8*
  %29 = load i32, i32* %entry_size, align 4
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %call16, i64 %conv17, i8* %28, i32 %29)
  %30 = load i32, i32* %entry_size, align 4
  %31 = load i32, i32* %out_byte_pos, align 4
  %add19 = add nsw i32 %31, %30
  store i32 %add19, i32* %out_byte_pos, align 4
  br label %for.inc20

for.inc20:                                        ; preds = %for.end
  %32 = load i32, i32* %i, align 4
  %inc21 = add i32 %32, 1
  store i32 %inc21, i32* %i, align 4
  br label %for.cond

for.end22:                                        ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end22, %if.then
  %call23 = call %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.111"* %att_val) #7
  %33 = load i1, i1* %retval, align 1
  ret i1 %33
}

declare %"class.draco::Dequantizer"* @_ZN5draco11DequantizerC1Ev(%"class.draco::Dequantizer"* returned) unnamed_addr #1

declare zeroext i1 @_ZN5draco11Dequantizer4InitEfi(%"class.draco::Dequantizer"*, float, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZN5draco33SequentialIntegerAttributeDecoder24GetPortableAttributeDataEv(%"class.draco::SequentialIntegerAttributeDecoder"* %this) #0 comdat {
entry:
  %retval = alloca i32*, align 4
  %this.addr = alloca %"class.draco::SequentialIntegerAttributeDecoder"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  store %"class.draco::SequentialIntegerAttributeDecoder"* %this, %"class.draco::SequentialIntegerAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialIntegerAttributeDecoder"*, %"class.draco::SequentialIntegerAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialIntegerAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder18portable_attributeEv(%"class.draco::SequentialAttributeDecoder"* %0)
  %call2 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %call)
  %cmp = icmp eq i32 %call2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32* null, i32** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %"class.draco::SequentialIntegerAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call3 = call %"class.draco::PointAttribute"* @_ZN5draco26SequentialAttributeDecoder18portable_attributeEv(%"class.draco::SequentialAttributeDecoder"* %1)
  %2 = bitcast %"class.draco::PointAttribute"* %call3 to %"class.draco::GeometryAttribute"*
  %call4 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp, i32 0)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  %call5 = call i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %2, i32 %3)
  %4 = bitcast i8* %call5 to i32*
  store i32* %4, i32** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32*, i32** %retval, align 4
  ret i32* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK5draco11Dequantizer15DequantizeFloatEi(%"class.draco::Dequantizer"* %this, i32 %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Dequantizer"*, align 4
  %val.addr = alloca i32, align 4
  store %"class.draco::Dequantizer"* %this, %"class.draco::Dequantizer"** %this.addr, align 4
  store i32 %val, i32* %val.addr, align 4
  %this1 = load %"class.draco::Dequantizer"*, %"class.draco::Dequantizer"** %this.addr, align 4
  %0 = load i32, i32* %val.addr, align 4
  %conv = sitofp i32 %0 to float
  %delta_ = getelementptr inbounds %"class.draco::Dequantizer", %"class.draco::Dequantizer"* %this1, i32 0, i32 0
  %1 = load float, float* %delta_, align 4
  %mul = fmul float %conv, %1
  ret float %mul
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.111"* %this, i32 %__i) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  %__i.addr = alloca i32, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  store i32 %__i, i32* %__i.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_) #7
  %0 = load float*, float** %call, align 4
  %1 = load i32, i32* %__i.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.63"* %attribute_buffer_) #7
  ret %"class.draco::DataBuffer"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %in_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %in_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %in_data, i8** %in_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %call = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %0 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %0 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %1 = load i8*, i8** %in_data.addr, align 4
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %1, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::SequentialQuantizationAttributeDecoder"* @_ZN5draco38SequentialQuantizationAttributeDecoderD2Ev(%"class.draco::SequentialQuantizationAttributeDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTVN5draco38SequentialQuantizationAttributeDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %min_value_ = getelementptr inbounds %"class.draco::SequentialQuantizationAttributeDecoder", %"class.draco::SequentialQuantizationAttributeDecoder"* %this1, i32 0, i32 2
  %call = call %"class.std::__2::unique_ptr.111"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.111"* %min_value_) #7
  %1 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to %"class.draco::SequentialIntegerAttributeDecoder"*
  %call2 = call %"class.draco::SequentialIntegerAttributeDecoder"* @_ZN5draco33SequentialIntegerAttributeDecoderD2Ev(%"class.draco::SequentialIntegerAttributeDecoder"* %1) #7
  ret %"class.draco::SequentialQuantizationAttributeDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco38SequentialQuantizationAttributeDecoderD0Ev(%"class.draco::SequentialQuantizationAttributeDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialQuantizationAttributeDecoder"*, align 4
  store %"class.draco::SequentialQuantizationAttributeDecoder"* %this, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialQuantizationAttributeDecoder"*, %"class.draco::SequentialQuantizationAttributeDecoder"** %this.addr, align 4
  %call = call %"class.draco::SequentialQuantizationAttributeDecoder"* @_ZN5draco38SequentialQuantizationAttributeDecoderD2Ev(%"class.draco::SequentialQuantizationAttributeDecoder"* %this1) #7
  %0 = bitcast %"class.draco::SequentialQuantizationAttributeDecoder"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

declare zeroext i1 @_ZN5draco26SequentialAttributeDecoder20InitializeStandaloneEPNS_14PointAttributeE(%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PointAttribute"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco26SequentialAttributeDecoder23DecodePortableAttributeERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE(%"class.draco::SequentialAttributeDecoder"*, %"class.std::__2::vector.116"* nonnull align 1, %"class.draco::DecoderBuffer"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco33SequentialIntegerAttributeDecoder34TransformAttributeToOriginalFormatERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEE(%"class.draco::SequentialIntegerAttributeDecoder"*, %"class.std::__2::vector.116"* nonnull align 1) unnamed_addr #1

declare zeroext i1 @_ZN5draco26SequentialAttributeDecoder20InitPredictionSchemeEPNS_25PredictionSchemeInterfaceE(%"class.draco::SequentialAttributeDecoder"*, %"class.draco::PredictionSchemeInterface"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco33SequentialIntegerAttributeDecoder12DecodeValuesERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_9allocatorIS5_EEEEPNS_13DecoderBufferE(%"class.draco::SequentialIntegerAttributeDecoder"*, %"class.std::__2::vector.116"* nonnull align 1, %"class.draco::DecoderBuffer"*) unnamed_addr #1

declare void @_ZN5draco33SequentialIntegerAttributeDecoder25CreateIntPredictionSchemeENS_22PredictionSchemeMethodENS_29PredictionSchemeTransformTypeE(%"class.std::__2::unique_ptr.106"* sret align 4, %"class.draco::SequentialIntegerAttributeDecoder"*, i32, i32) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco33SequentialIntegerAttributeDecoder21GetNumValueComponentsEv(%"class.draco::SequentialIntegerAttributeDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialIntegerAttributeDecoder"*, align 4
  store %"class.draco::SequentialIntegerAttributeDecoder"* %this, %"class.draco::SequentialIntegerAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialIntegerAttributeDecoder"*, %"class.draco::SequentialIntegerAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialIntegerAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call = call %"class.draco::PointAttribute"* @_ZNK5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %0)
  %1 = bitcast %"class.draco::PointAttribute"* %call to %"class.draco::GeometryAttribute"*
  %call2 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %1)
  %conv = sext i8 %call2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.112"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.112"* returned %this, float** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.112"*, align 4
  %__t1.addr = alloca float**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.112"* %this, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  store float** %__t1, float*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.112"*, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.112"* %this1 to %"struct.std::__2::__compressed_pair_elem.113"*
  %1 = load float**, float*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIPfEEOT_RNS_16remove_referenceIS2_E4typeE(float** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.113"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.113"* %0, float** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.112"* %this1 to %"struct.std::__2::__compressed_pair_elem.114"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.114"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.114"* %2)
  ret %"class.std::__2::__compressed_pair.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIPfEEOT_RNS_16remove_referenceIS2_E4typeE(float** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca float**, align 4
  store float** %__t, float*** %__t.addr, align 4
  %0 = load float**, float*** %__t.addr, align 4
  ret float** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.113"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.113"* returned %this, float** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.113"*, align 4
  %__u.addr = alloca float**, align 4
  store %"struct.std::__2::__compressed_pair_elem.113"* %this, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  store float** %__u, float*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.113"*, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.113", %"struct.std::__2::__compressed_pair_elem.113"* %this1, i32 0, i32 0
  %0 = load float**, float*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIPfEEOT_RNS_16remove_referenceIS2_E4typeE(float** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load float*, float** %call, align 4
  store float* %1, float** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.113"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.114"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.114"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.114"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.114"* %this, %"struct.std::__2::__compressed_pair_elem.114"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.114"*, %"struct.std::__2::__compressed_pair_elem.114"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.114"* %this1 to %"struct.std::__2::default_delete.115"*
  ret %"struct.std::__2::__compressed_pair_elem.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.53"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #7
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %0) #7
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformC2Ev(%"class.draco::AttributeTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransform"*, align 4
  store %"class.draco::AttributeTransform"* %this, %"class.draco::AttributeTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransform"*, %"class.draco::AttributeTransform"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeTransform"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN5draco18AttributeTransformE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"class.draco::AttributeTransform"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.117"* @_ZNSt3__26vectorIfNS_9allocatorIfEEEC2Ev(%"class.std::__2::vector.117"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.117"*, align 4
  store %"class.std::__2::vector.117"* %this, %"class.std::__2::vector.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.117"*, %"class.std::__2::vector.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.117"* %this1 to %"class.std::__2::__vector_base.118"*
  %call = call %"class.std::__2::__vector_base.118"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEEC2Ev(%"class.std::__2::__vector_base.118"* %0) #7
  ret %"class.std::__2::vector.117"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformD2Ev(%"class.draco::AttributeTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransform"*, align 4
  store %"class.draco::AttributeTransform"* %this, %"class.draco::AttributeTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransform"*, %"class.draco::AttributeTransform"** %this.addr, align 4
  ret %"class.draco::AttributeTransform"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco18AttributeTransformD0Ev(%"class.draco::AttributeTransform"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransform"*, align 4
  store %"class.draco::AttributeTransform"* %this, %"class.draco::AttributeTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransform"*, %"class.draco::AttributeTransform"** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.118"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEEC2Ev(%"class.std::__2::__vector_base.118"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.118"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.118"* %this, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.118"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 0
  store float* null, float** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 1
  store float* null, float** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.119"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.119"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.118"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.119"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.119"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.113"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.113"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.113"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.120"* %2)
  ret %"class.std::__2::__compressed_pair.119"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.113"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.113"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.113"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.113"* %this, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.113"*, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.113", %"struct.std::__2::__compressed_pair_elem.113"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store float* null, float** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.113"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.120"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"class.std::__2::allocator.121"*
  %call = call %"class.std::__2::allocator.121"* @_ZNSt3__29allocatorIfEC2Ev(%"class.std::__2::allocator.121"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.120"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.121"* @_ZNSt3__29allocatorIfEC2Ev(%"class.std::__2::allocator.121"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.121"*, align 4
  store %"class.std::__2::allocator.121"* %this, %"class.std::__2::allocator.121"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.121"*, %"class.std::__2::allocator.121"** %this.addr, align 4
  ret %"class.std::__2::allocator.121"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.117"* @_ZNSt3__26vectorIfNS_9allocatorIfEEED2Ev(%"class.std::__2::vector.117"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.117"*, align 4
  store %"class.std::__2::vector.117"* %this, %"class.std::__2::vector.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.117"*, %"class.std::__2::vector.117"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_deleteEv(%"class.std::__2::vector.117"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.117"* %this1 to %"class.std::__2::__vector_base.118"*
  %call = call %"class.std::__2::__vector_base.118"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEED2Ev(%"class.std::__2::__vector_base.118"* %0) #7
  ret %"class.std::__2::vector.117"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_deleteEv(%"class.std::__2::vector.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.117"*, align 4
  store %"class.std::__2::vector.117"* %this, %"class.std::__2::vector.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.117"*, %"class.std::__2::vector.117"** %this.addr, align 4
  %call = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector.117"* %this1) #7
  %0 = bitcast float* %call to i8*
  %call2 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector.117"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector.117"* %this1) #7
  %add.ptr = getelementptr inbounds float, float* %call2, i32 %call3
  %1 = bitcast float* %add.ptr to i8*
  %call4 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector.117"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector.117"* %this1) #7
  %add.ptr6 = getelementptr inbounds float, float* %call4, i32 %call5
  %2 = bitcast float* %add.ptr6 to i8*
  %call7 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector.117"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector.117"* %this1) #7
  %add.ptr9 = getelementptr inbounds float, float* %call7, i32 %call8
  %3 = bitcast float* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.117"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.118"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEED2Ev(%"class.std::__2::__vector_base.118"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.118"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.118"*, align 4
  store %"class.std::__2::__vector_base.118"* %this, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  store %"class.std::__2::__vector_base.118"* %this1, %"class.std::__2::__vector_base.118"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 0
  %0 = load float*, float** %__begin_, align 4
  %cmp = icmp ne float* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE5clearEv(%"class.std::__2::__vector_base.118"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.121"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base.118"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 0
  %1 = load float*, float** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::__vector_base.118"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm(%"class.std::__2::allocator.121"* nonnull align 1 dereferenceable(1) %call, float* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %retval, align 4
  ret %"class.std::__2::__vector_base.118"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.117"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.117"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.117"* %this, %"class.std::__2::vector.117"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.117"*, %"class.std::__2::vector.117"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.117"*, align 4
  store %"class.std::__2::vector.117"* %this, %"class.std::__2::vector.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.117"*, %"class.std::__2::vector.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.117"* %this1 to %"class.std::__2::__vector_base.118"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %0, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %call = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %1) #7
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.117"*, align 4
  store %"class.std::__2::vector.117"* %this, %"class.std::__2::vector.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.117"*, %"class.std::__2::vector.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.117"* %this1 to %"class.std::__2::__vector_base.118"*
  %call = call i32 @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::__vector_base.118"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.117"*, align 4
  store %"class.std::__2::vector.117"* %this, %"class.std::__2::vector.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.117"*, %"class.std::__2::vector.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.117"* %this1 to %"class.std::__2::__vector_base.118"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %0, i32 0, i32 1
  %1 = load float*, float** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.117"* %this1 to %"class.std::__2::__vector_base.118"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %2, i32 0, i32 0
  %3 = load float*, float** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %__p) #0 comdat {
entry:
  %__p.addr = alloca float*, align 4
  store float* %__p, float** %__p.addr, align 4
  %0 = load float*, float** %__p.addr, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::__vector_base.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.118"*, align 4
  store %"class.std::__2::__vector_base.118"* %this, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base.118"* %this1) #7
  %0 = load float*, float** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.118"*, align 4
  store %"class.std::__2::__vector_base.118"* %this, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair.119"* %__end_cap_) #7
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.113"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.113"* %0) #7
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.113"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.113"* %this, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.113"*, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.113", %"struct.std::__2::__compressed_pair_elem.113"* %this1, i32 0, i32 0
  ret float** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE5clearEv(%"class.std::__2::__vector_base.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.118"*, align 4
  store %"class.std::__2::__vector_base.118"* %this, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 0
  %0 = load float*, float** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::__vector_base.118"* %this1, float* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm(%"class.std::__2::allocator.121"* nonnull align 1 dereferenceable(1) %__a, float* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.121"*, align 4
  %__p.addr = alloca float*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.121"* %__a, %"class.std::__2::allocator.121"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.121"*, %"class.std::__2::allocator.121"** %__a.addr, align 4
  %1 = load float*, float** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIfE10deallocateEPfm(%"class.std::__2::allocator.121"* %0, float* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.121"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.118"*, align 4
  store %"class.std::__2::__vector_base.118"* %this, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.121"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %__end_cap_) #7
  ret %"class.std::__2::allocator.121"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::__vector_base.118"* %this, float* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.118"*, align 4
  %__new_last.addr = alloca float*, align 4
  %__soon_to_be_end = alloca float*, align 4
  store %"class.std::__2::__vector_base.118"* %this, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  store float* %__new_last, float** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.118"*, %"class.std::__2::__vector_base.118"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 1
  %0 = load float*, float** %__end_, align 4
  store float* %0, float** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load float*, float** %__new_last.addr, align 4
  %2 = load float*, float** %__soon_to_be_end, align 4
  %cmp = icmp ne float* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.121"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base.118"* %this1) #7
  %3 = load float*, float** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds float, float* %3, i32 -1
  store float* %incdec.ptr, float** %__soon_to_be_end, align 4
  %call2 = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE7destroyIfEEvRS2_PT_(%"class.std::__2::allocator.121"* nonnull align 1 dereferenceable(1) %call, float* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load float*, float** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.118", %"class.std::__2::__vector_base.118"* %this1, i32 0, i32 1
  store float* %4, float** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE7destroyIfEEvRS2_PT_(%"class.std::__2::allocator.121"* nonnull align 1 dereferenceable(1) %__a, float* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.121"*, align 4
  %__p.addr = alloca float*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.121"* %__a, %"class.std::__2::allocator.121"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.121"*, %"class.std::__2::allocator.121"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9__destroyIfEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.121"* nonnull align 1 dereferenceable(1) %1, float* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9__destroyIfEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.121"* nonnull align 1 dereferenceable(1) %__a, float* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.121"*, align 4
  %__p.addr = alloca float*, align 4
  store %"class.std::__2::allocator.121"* %__a, %"class.std::__2::allocator.121"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.121"*, %"class.std::__2::allocator.121"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIfE7destroyEPf(%"class.std::__2::allocator.121"* %1, float* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIfE7destroyEPf(%"class.std::__2::allocator.121"* %this, float* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.121"*, align 4
  %__p.addr = alloca float*, align 4
  store %"class.std::__2::allocator.121"* %this, %"class.std::__2::allocator.121"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.121"*, %"class.std::__2::allocator.121"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIfE10deallocateEPfm(%"class.std::__2::allocator.121"* %this, float* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.121"*, align 4
  %__p.addr = alloca float*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.121"* %this, %"class.std::__2::allocator.121"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.121"*, %"class.std::__2::allocator.121"** %this.addr, align 4
  %0 = load float*, float** %__p.addr, align 4
  %1 = bitcast float* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.121"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.121"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %0) #7
  ret %"class.std::__2::allocator.121"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.121"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"class.std::__2::allocator.121"*
  ret %"class.std::__2::allocator.121"* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %num_unique_entries_, align 8
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %byte_pos = alloca i64, align 8
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType"* %att_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call = call i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this1, i32 %2)
  store i64 %call, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %call3 = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %3)
  %4 = load i64, i64* %byte_pos, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call3, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  ret i64 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.56"* %data_, i32 0) #7
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.56"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.63"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_) #7
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  ret %"class.draco::DataBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.65"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %0) #7
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.65"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.65"* %this, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.65"*, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.65", %"struct.std::__2::__compressed_pair_elem.65"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::SequentialIntegerAttributeDecoder"* @_ZN5draco33SequentialIntegerAttributeDecoderD2Ev(%"class.draco::SequentialIntegerAttributeDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialIntegerAttributeDecoder"*, align 4
  store %"class.draco::SequentialIntegerAttributeDecoder"* %this, %"class.draco::SequentialIntegerAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialIntegerAttributeDecoder"*, %"class.draco::SequentialIntegerAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialIntegerAttributeDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTVN5draco33SequentialIntegerAttributeDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %prediction_scheme_ = getelementptr inbounds %"class.draco::SequentialIntegerAttributeDecoder", %"class.draco::SequentialIntegerAttributeDecoder"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.106"* @_ZNSt3__210unique_ptrIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEED2Ev(%"class.std::__2::unique_ptr.106"* %prediction_scheme_) #7
  %1 = bitcast %"class.draco::SequentialIntegerAttributeDecoder"* %this1 to %"class.draco::SequentialAttributeDecoder"*
  %call2 = call %"class.draco::SequentialAttributeDecoder"* @_ZN5draco26SequentialAttributeDecoderD2Ev(%"class.draco::SequentialAttributeDecoder"* %1) #7
  ret %"class.draco::SequentialIntegerAttributeDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.106"* @_ZNSt3__210unique_ptrIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEED2Ev(%"class.std::__2::unique_ptr.106"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  store %"class.std::__2::unique_ptr.106"* %this, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE5resetEPS3_(%"class.std::__2::unique_ptr.106"* %this1, %"class.draco::PredictionSchemeTypedDecoderInterface"* null) #7
  ret %"class.std::__2::unique_ptr.106"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::SequentialAttributeDecoder"* @_ZN5draco26SequentialAttributeDecoderD2Ev(%"class.draco::SequentialAttributeDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.draco::SequentialAttributeDecoder"* %this, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::SequentialAttributeDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTVN5draco26SequentialAttributeDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %portable_attribute_ = getelementptr inbounds %"class.draco::SequentialAttributeDecoder", %"class.draco::SequentialAttributeDecoder"* %this1, i32 0, i32 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %portable_attribute_) #7
  ret %"class.draco::SequentialAttributeDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE5resetEPS3_(%"class.std::__2::unique_ptr.106"* %this, %"class.draco::PredictionSchemeTypedDecoderInterface"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  %__p.addr = alloca %"class.draco::PredictionSchemeTypedDecoderInterface"*, align 4
  %__tmp = alloca %"class.draco::PredictionSchemeTypedDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.106"* %this, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  store %"class.draco::PredictionSchemeTypedDecoderInterface"* %__p, %"class.draco::PredictionSchemeTypedDecoderInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PredictionSchemeTypedDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %__ptr_) #7
  %0 = load %"class.draco::PredictionSchemeTypedDecoderInterface"*, %"class.draco::PredictionSchemeTypedDecoderInterface"** %call, align 4
  store %"class.draco::PredictionSchemeTypedDecoderInterface"* %0, %"class.draco::PredictionSchemeTypedDecoderInterface"** %__tmp, align 4
  %1 = load %"class.draco::PredictionSchemeTypedDecoderInterface"*, %"class.draco::PredictionSchemeTypedDecoderInterface"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PredictionSchemeTypedDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %__ptr_2) #7
  store %"class.draco::PredictionSchemeTypedDecoderInterface"* %1, %"class.draco::PredictionSchemeTypedDecoderInterface"** %call3, align 4
  %2 = load %"class.draco::PredictionSchemeTypedDecoderInterface"*, %"class.draco::PredictionSchemeTypedDecoderInterface"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PredictionSchemeTypedDecoderInterface"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__217__compressed_pairIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.107"* %__ptr_4) #7
  %3 = load %"class.draco::PredictionSchemeTypedDecoderInterface"*, %"class.draco::PredictionSchemeTypedDecoderInterface"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEEEclEPS3_(%"struct.std::__2::default_delete.110"* %call5, %"class.draco::PredictionSchemeTypedDecoderInterface"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PredictionSchemeTypedDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.107"*, align 4
  store %"class.std::__2::__compressed_pair.107"* %this, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.107"*, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.107"* %this1 to %"struct.std::__2::__compressed_pair_elem.108"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PredictionSchemeTypedDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.108"* %0) #7
  ret %"class.draco::PredictionSchemeTypedDecoderInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__217__compressed_pairIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEENS_14default_deleteIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.107"*, align 4
  store %"class.std::__2::__compressed_pair.107"* %this, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.107"*, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.107"* %this1 to %"struct.std::__2::__compressed_pair_elem.109"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.109"* %0) #7
  ret %"struct.std::__2::default_delete.110"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEEEclEPS3_(%"struct.std::__2::default_delete.110"* %this, %"class.draco::PredictionSchemeTypedDecoderInterface"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.110"*, align 4
  %__ptr.addr = alloca %"class.draco::PredictionSchemeTypedDecoderInterface"*, align 4
  store %"struct.std::__2::default_delete.110"* %this, %"struct.std::__2::default_delete.110"** %this.addr, align 4
  store %"class.draco::PredictionSchemeTypedDecoderInterface"* %__ptr, %"class.draco::PredictionSchemeTypedDecoderInterface"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.110"*, %"struct.std::__2::default_delete.110"** %this.addr, align 4
  %0 = load %"class.draco::PredictionSchemeTypedDecoderInterface"*, %"class.draco::PredictionSchemeTypedDecoderInterface"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PredictionSchemeTypedDecoderInterface"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::PredictionSchemeTypedDecoderInterface"* %0 to void (%"class.draco::PredictionSchemeTypedDecoderInterface"*)***
  %vtable = load void (%"class.draco::PredictionSchemeTypedDecoderInterface"*)**, void (%"class.draco::PredictionSchemeTypedDecoderInterface"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::PredictionSchemeTypedDecoderInterface"*)*, void (%"class.draco::PredictionSchemeTypedDecoderInterface"*)** %vtable, i64 1
  %2 = load void (%"class.draco::PredictionSchemeTypedDecoderInterface"*)*, void (%"class.draco::PredictionSchemeTypedDecoderInterface"*)** %vfn, align 4
  call void %2(%"class.draco::PredictionSchemeTypedDecoderInterface"* %0) #7
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PredictionSchemeTypedDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco37PredictionSchemeTypedDecoderInterfaceIiiEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.108"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.108"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.108"* %this, %"struct.std::__2::__compressed_pair_elem.108"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.108"*, %"struct.std::__2::__compressed_pair_elem.108"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.108", %"struct.std::__2::__compressed_pair_elem.108"* %this1, i32 0, i32 0
  ret %"class.draco::PredictionSchemeTypedDecoderInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco37PredictionSchemeTypedDecoderInterfaceIiiEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.109"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.109"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.109"* %this, %"struct.std::__2::__compressed_pair_elem.109"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.109"*, %"struct.std::__2::__compressed_pair_elem.109"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.109"* %this1 to %"struct.std::__2::default_delete.110"*
  ret %"struct.std::__2::default_delete.110"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.53"* %this1, %"class.draco::PointAttribute"* null) #7
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.53"* %this, %"class.draco::PointAttribute"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %__tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #7
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__tmp, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_2) #7
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %call3, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointAttribute"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %__ptr_4) #7
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.81"* %call5, %"class.draco::PointAttribute"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %0) #7
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.80"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.80"* %0) #7
  ret %"struct.std::__2::default_delete.81"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.81"* %this, %"class.draco::PointAttribute"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  %__ptr.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"struct.std::__2::default_delete.81"* %this, %"struct.std::__2::default_delete.81"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__ptr, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointAttribute"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* %0) #7
  %1 = bitcast %"class.draco::PointAttribute"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.80"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.80"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.80"* %this, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.80"*, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.80"* %this1 to %"struct.std::__2::default_delete.81"*
  ret %"struct.std::__2::default_delete.81"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.std::__2::unique_ptr.75"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.75"* %attribute_transform_data_) #7
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* %indices_map_) #7
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::unique_ptr.63"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.63"* %attribute_buffer_) #7
  ret %"class.draco::PointAttribute"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.75"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.75"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.75"*, align 4
  store %"class.std::__2::unique_ptr.75"* %this, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.75"*, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.75"* %this1, %"class.draco::AttributeTransformData"* null) #7
  ret %"class.std::__2::unique_ptr.75"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* %vector_) #7
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.63"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.63"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.63"* %this1, %"class.draco::DataBuffer"* null) #7
  ret %"class.std::__2::unique_ptr.63"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.75"* %this, %"class.draco::AttributeTransformData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.75"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %__tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.75"* %this, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.75"*, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %__ptr_) #7
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %__ptr_2) #7
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %call3, align 4
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.76"* %__ptr_4) #7
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.79"* %call5, %"class.draco::AttributeTransformData"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.76"*, align 4
  store %"class.std::__2::__compressed_pair.76"* %this, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.76"*, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.76"* %this1 to %"struct.std::__2::__compressed_pair_elem.77"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.77"* %0) #7
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.76"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.76"*, align 4
  store %"class.std::__2::__compressed_pair.76"* %this, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.76"*, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.76"* %this1 to %"struct.std::__2::__compressed_pair_elem.78"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.78"* %0) #7
  ret %"struct.std::__2::default_delete.79"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.79"* %this, %"class.draco::AttributeTransformData"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.79"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"struct.std::__2::default_delete.79"* %this, %"struct.std::__2::default_delete.79"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__ptr, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.79"*, %"struct.std::__2::default_delete.79"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeTransformData"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* %0) #7
  %1 = bitcast %"class.draco::AttributeTransformData"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.77"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.77"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.77"* %this, %"struct.std::__2::__compressed_pair_elem.77"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.77"*, %"struct.std::__2::__compressed_pair_elem.77"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.77", %"struct.std::__2::__compressed_pair_elem.77"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.78"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.78"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.78"* %this, %"struct.std::__2::__compressed_pair_elem.78"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.78"*, %"struct.std::__2::__compressed_pair_elem.78"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.78"* %this1 to %"struct.std::__2::default_delete.79"*
  ret %"struct.std::__2::default_delete.79"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %buffer_) #7
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* %data_) #7
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* %0) #7
  ret %"class.std::__2::vector.56"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #7
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #7
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #7
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #7
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.57"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store %"class.std::__2::__vector_base.57"* %this1, %"class.std::__2::__vector_base.57"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %retval, align 4
  ret %"class.std::__2::__vector_base.57"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #7
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this1) #7
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.59"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %0) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.59"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.59"* %this, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.59"*, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.59", %"struct.std::__2::__compressed_pair_elem.59"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this1, i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %0, i8* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #7
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #7
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.123", align 1
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.123"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.60"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %0) #7
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.60"* %this1 to %"class.std::__2::allocator.61"*
  ret %"class.std::__2::allocator.61"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* %0) #7
  ret %"class.std::__2::vector.68"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #7
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #7
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.69"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.std::__2::__vector_base.69"* %this1, %"class.std::__2::__vector_base.69"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %retval, align 4
  ret %"class.std::__2::__vector_base.69"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #7
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this1) #7
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #7
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #7
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this1, %"class.draco::IndexType"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %0, %"class.draco::IndexType"* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #7
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #7
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.124", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.124"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #7
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.63"* %this, %"class.draco::DataBuffer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %__tmp = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_) #7
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__tmp, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_2) #7
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %call3, align 4
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.64"* %__ptr_4) #7
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete.67"* %call5, %"class.draco::DataBuffer"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.65"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %0) #7
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.66"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %0) #7
  ret %"struct.std::__2::default_delete.67"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete.67"* %this, %"class.draco::DataBuffer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.67"*, align 4
  %__ptr.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"struct.std::__2::default_delete.67"* %this, %"struct.std::__2::default_delete.67"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__ptr, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.67"*, %"struct.std::__2::default_delete.67"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %0) #7
  %1 = bitcast %"class.draco::DataBuffer"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.65"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.65"* %this, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.65"*, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.65", %"struct.std::__2::__compressed_pair_elem.65"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.66"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.66"* %this, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.66"*, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.66"* %this1 to %"struct.std::__2::default_delete.67"*
  ret %"struct.std::__2::default_delete.67"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNK5draco26SequentialAttributeDecoder9attributeEv(%"class.draco::SequentialAttributeDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::SequentialAttributeDecoder"*, align 4
  store %"class.draco::SequentialAttributeDecoder"* %this, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::SequentialAttributeDecoder"*, %"class.draco::SequentialAttributeDecoder"** %this.addr, align 4
  %attribute_ = getelementptr inbounds %"class.draco::SequentialAttributeDecoder", %"class.draco::SequentialAttributeDecoder"* %this1, i32 0, i32 2
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute_, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetEDn(%"class.std::__2::unique_ptr.111"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca float*, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_) #7
  %1 = load float*, float** %call, align 4
  store float* %1, float** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_2) #7
  store float* null, float** %call3, align 4
  %2 = load float*, float** %__tmp, align 4
  %tobool = icmp ne float* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv(%"class.std::__2::__compressed_pair.112"* %__ptr_4) #7
  %3 = load float*, float** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIA_fEclIfEENS2_20_EnableIfConvertibleIT_E4typeEPS5_(%"struct.std::__2::default_delete.115"* %call5, float* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.112"*, align 4
  store %"class.std::__2::__compressed_pair.112"* %this, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.112"*, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.112"* %this1 to %"struct.std::__2::__compressed_pair_elem.113"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.113"* %0) #7
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv(%"class.std::__2::__compressed_pair.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.112"*, align 4
  store %"class.std::__2::__compressed_pair.112"* %this, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.112"*, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.112"* %this1 to %"struct.std::__2::__compressed_pair_elem.114"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.114"* %0) #7
  ret %"struct.std::__2::default_delete.115"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIA_fEclIfEENS2_20_EnableIfConvertibleIT_E4typeEPS5_(%"struct.std::__2::default_delete.115"* %this, float* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.115"*, align 4
  %__ptr.addr = alloca float*, align 4
  store %"struct.std::__2::default_delete.115"* %this, %"struct.std::__2::default_delete.115"** %this.addr, align 4
  store float* %__ptr, float** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.115"*, %"struct.std::__2::default_delete.115"** %this.addr, align 4
  %0 = load float*, float** %__ptr.addr, align 4
  %isnull = icmp eq float* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast float* %0 to i8*
  call void @_ZdaPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.113"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.113"* %this, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.113"*, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.113", %"struct.std::__2::__compressed_pair_elem.113"* %this1, i32 0, i32 0
  ret float** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.114"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.114"* %this, %"struct.std::__2::__compressed_pair_elem.114"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.114"*, %"struct.std::__2::__compressed_pair_elem.114"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.114"* %this1 to %"struct.std::__2::default_delete.115"*
  ret %"struct.std::__2::default_delete.115"* %0
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.112"*, align 4
  store %"class.std::__2::__compressed_pair.112"* %this, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.112"*, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.112"* %this1 to %"struct.std::__2::__compressed_pair_elem.113"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.113"* %0) #7
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.112"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.112"* returned %this, float** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.112"*, align 4
  %__t1.addr = alloca float**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.112"* %this, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  store float** %__t1, float*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.112"*, %"class.std::__2::__compressed_pair.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.112"* %this1 to %"struct.std::__2::__compressed_pair_elem.113"*
  %1 = load float**, float*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE(float** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.113"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.113"* %0, float** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.112"* %this1 to %"struct.std::__2::__compressed_pair_elem.114"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.114"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.114"* %2)
  ret %"class.std::__2::__compressed_pair.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE(float** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca float**, align 4
  store float** %__t, float*** %__t.addr, align 4
  %0 = load float**, float*** %__t.addr, align 4
  ret float** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.113"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.113"* returned %this, float** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.113"*, align 4
  %__u.addr = alloca float**, align 4
  store %"struct.std::__2::__compressed_pair_elem.113"* %this, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  store float** %__u, float*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.113"*, %"struct.std::__2::__compressed_pair_elem.113"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.113", %"struct.std::__2::__compressed_pair_elem.113"* %this1, i32 0, i32 0
  %0 = load float**, float*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE(float** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load float*, float** %call, align 4
  store float* %1, float** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.113"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetIPfEENS_9enable_ifIXsr28_CheckArrayPointerConversionIT_EE5valueEvE4typeES8_(%"class.std::__2::unique_ptr.111"* %this, float* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  %__p.addr = alloca float*, align 4
  %__tmp = alloca float*, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_) #7
  %0 = load float*, float** %call, align 4
  store float* %0, float** %__tmp, align 4
  %1 = load float*, float** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_2) #7
  store float* %1, float** %call3, align 4
  %2 = load float*, float** %__tmp, align 4
  %tobool = icmp ne float* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv(%"class.std::__2::__compressed_pair.112"* %__ptr_4) #7
  %3 = load float*, float** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIA_fEclIfEENS2_20_EnableIfConvertibleIT_E4typeEPS5_(%"struct.std::__2::default_delete.115"* %call5, float* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE7releaseEv(%"class.std::__2::unique_ptr.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  %__t = alloca float*, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_) #7
  %0 = load float*, float** %call, align 4
  store float* %0, float** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.112"* %__ptr_2) #7
  store float* null, float** %call3, align 4
  %1 = load float*, float** %__t, align 4
  ret float* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__27forwardINS_14default_deleteIA_fEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::default_delete.115"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.115"*, align 4
  store %"struct.std::__2::default_delete.115"* %__t, %"struct.std::__2::default_delete.115"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.115"*, %"struct.std::__2::default_delete.115"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.115"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE11get_deleterEv(%"class.std::__2::unique_ptr.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.111"*, align 4
  store %"class.std::__2::unique_ptr.111"* %this, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.111"*, %"class.std::__2::unique_ptr.111"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.111", %"class.std::__2::unique_ptr.111"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.115"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv(%"class.std::__2::__compressed_pair.112"* %__ptr_) #7
  ret %"struct.std::__2::default_delete.115"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIfEEbPT_(%"class.draco::DecoderBuffer"* %this, float* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca float*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store float* %out_val, float** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 4, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load float*, float** %out_val.addr, align 4
  %3 = bitcast float* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 1 %add.ptr, i32 4, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind readnone speculatable willreturn }
attributes #3 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { cold noreturn nounwind }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
