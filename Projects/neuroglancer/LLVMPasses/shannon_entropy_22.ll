; ModuleID = './draco/src/draco/compression/entropy/shannon_entropy.cc'
source_filename = "./draco/src/draco/compression/entropy/shannon_entropy.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::ShannonEntropyTracker" = type { %"class.std::__2::vector", [4 x i8], %"struct.draco::ShannonEntropyTracker::EntropyData" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i32*, i32*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32* }
%"struct.draco::ShannonEntropyTracker::EntropyData" = type <{ double, i32, i32, i32, [4 x i8] }>
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i32*, i32* }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.std::__2::allocator"* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction" = type { i32*, i32*, i32** }
%"struct.std::__2::integral_constant.3" = type { i8 }

$_ZNSt3__26vectorIiNS_9allocatorIiEEEC2EmRKi = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZN5draco21ShannonEntropyTracker11EntropyDataC2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE6resizeEmRKi = comdat any

$_Z4log2IiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_ = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE18__construct_at_endEmRKi = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIiEC2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIiE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorIiE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJRKiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__29allocatorIiE9constructIiJRKiEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE8__appendEmRKi = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE18__construct_at_endEmRKi = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionC2EPPim = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco21ShannonEntropyTrackerC1Ev = hidden unnamed_addr alias %"class.draco::ShannonEntropyTracker"* (%"class.draco::ShannonEntropyTracker"*), %"class.draco::ShannonEntropyTracker"* (%"class.draco::ShannonEntropyTracker"*)* @_ZN5draco21ShannonEntropyTrackerC2Ev

; Function Attrs: noinline nounwind optnone
define hidden i64 @_ZN5draco21ComputeShannonEntropyEPKjiiPi(i32* %symbols, i32 %num_symbols, i32 %max_value, i32* %out_num_unique_symbols) #0 {
entry:
  %symbols.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %max_value.addr = alloca i32, align 4
  %out_num_unique_symbols.addr = alloca i32*, align 4
  %num_unique_symbols = alloca i32, align 4
  %symbol_frequencies = alloca %"class.std::__2::vector", align 4
  %ref.tmp = alloca i32, align 4
  %i = alloca i32, align 4
  %total_bits = alloca double, align 8
  %num_symbols_d = alloca double, align 8
  %i3 = alloca i32, align 4
  store i32* %symbols, i32** %symbols.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  store i32 %max_value, i32* %max_value.addr, align 4
  store i32* %out_num_unique_symbols, i32** %out_num_unique_symbols.addr, align 4
  store i32 0, i32* %num_unique_symbols, align 4
  %0 = load i32, i32* %max_value.addr, align 4
  %add = add nsw i32 %0, 1
  store i32 0, i32* %ref.tmp, align 4
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2EmRKi(%"class.std::__2::vector"* %symbol_frequencies, i32 %add, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %symbols.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %symbol_frequencies, i32 %5) #7
  %6 = load i32, i32* %call1, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %call1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc2 = add nsw i32 %7, 1
  store i32 %inc2, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store double 0.000000e+00, double* %total_bits, align 8
  %8 = load i32, i32* %num_symbols.addr, align 4
  %conv = sitofp i32 %8 to double
  store double %conv, double* %num_symbols_d, align 8
  store i32 0, i32* %i3, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc16, %for.end
  %9 = load i32, i32* %i3, align 4
  %10 = load i32, i32* %max_value.addr, align 4
  %add5 = add nsw i32 %10, 1
  %cmp6 = icmp slt i32 %9, %add5
  br i1 %cmp6, label %for.body7, label %for.end18

for.body7:                                        ; preds = %for.cond4
  %11 = load i32, i32* %i3, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %symbol_frequencies, i32 %11) #7
  %12 = load i32, i32* %call8, align 4
  %cmp9 = icmp sgt i32 %12, 0
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body7
  %13 = load i32, i32* %num_unique_symbols, align 4
  %inc10 = add nsw i32 %13, 1
  store i32 %inc10, i32* %num_unique_symbols, align 4
  %14 = load i32, i32* %i3, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %symbol_frequencies, i32 %14) #7
  %15 = load i32, i32* %call11, align 4
  %conv12 = sitofp i32 %15 to double
  %16 = load i32, i32* %i3, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %symbol_frequencies, i32 %16) #7
  %17 = load i32, i32* %call13, align 4
  %conv14 = sitofp i32 %17 to double
  %18 = load double, double* %num_symbols_d, align 8
  %div = fdiv double %conv14, %18
  %19 = call double @llvm.log2.f64(double %div)
  %mul = fmul double %conv12, %19
  %20 = load double, double* %total_bits, align 8
  %add15 = fadd double %20, %mul
  store double %add15, double* %total_bits, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body7
  br label %for.inc16

for.inc16:                                        ; preds = %if.end
  %21 = load i32, i32* %i3, align 4
  %inc17 = add nsw i32 %21, 1
  store i32 %inc17, i32* %i3, align 4
  br label %for.cond4

for.end18:                                        ; preds = %for.cond4
  %22 = load i32*, i32** %out_num_unique_symbols.addr, align 4
  %tobool = icmp ne i32* %22, null
  br i1 %tobool, label %if.then19, label %if.end20

if.then19:                                        ; preds = %for.end18
  %23 = load i32, i32* %num_unique_symbols, align 4
  %24 = load i32*, i32** %out_num_unique_symbols.addr, align 4
  store i32 %23, i32* %24, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %for.end18
  %25 = load double, double* %total_bits, align 8
  %fneg = fneg double %25
  %conv21 = fptosi double %fneg to i64
  %call22 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector"* %symbol_frequencies) #7
  ret i64 %conv21
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2EmRKi(%"class.std::__2::vector"* returned %this, i32 %__n, i32* nonnull align 4 dereferenceable(4) %__x) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base"* %0) #7
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  %4 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE18__construct_at_endEmRKi(%"class.std::__2::vector"* %this1, i32 %3, i32* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log2.f64(double) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base"* %0) #7
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden double @_ZN5draco27ComputeBinaryShannonEntropyEjj(i32 %num_values, i32 %num_true_values) #0 {
entry:
  %retval = alloca double, align 8
  %num_values.addr = alloca i32, align 4
  %num_true_values.addr = alloca i32, align 4
  %true_freq = alloca double, align 8
  %false_freq = alloca double, align 8
  store i32 %num_values, i32* %num_values.addr, align 4
  store i32 %num_true_values, i32* %num_true_values.addr, align 4
  %0 = load i32, i32* %num_values.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store double 0.000000e+00, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %num_true_values.addr, align 4
  %cmp1 = icmp eq i32 %1, 0
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %num_values.addr, align 4
  %3 = load i32, i32* %num_true_values.addr, align 4
  %cmp2 = icmp eq i32 %2, %3
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  store double 0.000000e+00, double* %retval, align 8
  br label %return

if.end4:                                          ; preds = %lor.lhs.false
  %4 = load i32, i32* %num_true_values.addr, align 4
  %conv = uitofp i32 %4 to double
  %5 = load i32, i32* %num_values.addr, align 4
  %conv5 = uitofp i32 %5 to double
  %div = fdiv double %conv, %conv5
  store double %div, double* %true_freq, align 8
  %6 = load double, double* %true_freq, align 8
  %sub = fsub double 1.000000e+00, %6
  store double %sub, double* %false_freq, align 8
  %7 = load double, double* %true_freq, align 8
  %8 = load double, double* %true_freq, align 8
  %9 = call double @llvm.log2.f64(double %8)
  %mul = fmul double %7, %9
  %10 = load double, double* %false_freq, align 8
  %11 = load double, double* %false_freq, align 8
  %12 = call double @llvm.log2.f64(double %11)
  %mul6 = fmul double %10, %12
  %add = fadd double %mul, %mul6
  %fneg = fneg double %add
  store double %fneg, double* %retval, align 8
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %13 = load double, double* %retval, align 8
  ret double %13
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::ShannonEntropyTracker"* @_ZN5draco21ShannonEntropyTrackerC2Ev(%"class.draco::ShannonEntropyTracker"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::ShannonEntropyTracker"*, align 4
  store %"class.draco::ShannonEntropyTracker"* %this, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  %this1 = load %"class.draco::ShannonEntropyTracker"*, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  %frequencies_ = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector"* %frequencies_) #7
  %entropy_data_ = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 2
  %call2 = call %"struct.draco::ShannonEntropyTracker::EntropyData"* @_ZN5draco21ShannonEntropyTracker11EntropyDataC2Ev(%"struct.draco::ShannonEntropyTracker::EntropyData"* %entropy_data_)
  ret %"class.draco::ShannonEntropyTracker"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base"* %0) #7
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::ShannonEntropyTracker::EntropyData"* @_ZN5draco21ShannonEntropyTracker11EntropyDataC2Ev(%"struct.draco::ShannonEntropyTracker::EntropyData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::ShannonEntropyTracker::EntropyData"*, align 4
  store %"struct.draco::ShannonEntropyTracker::EntropyData"* %this, %"struct.draco::ShannonEntropyTracker::EntropyData"** %this.addr, align 4
  %this1 = load %"struct.draco::ShannonEntropyTracker::EntropyData"*, %"struct.draco::ShannonEntropyTracker::EntropyData"** %this.addr, align 4
  %entropy_norm = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %this1, i32 0, i32 0
  store double 0.000000e+00, double* %entropy_norm, align 8
  %num_values = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %this1, i32 0, i32 1
  store i32 0, i32* %num_values, align 8
  %max_symbol = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %this1, i32 0, i32 2
  store i32 0, i32* %max_symbol, align 4
  %num_unique_symbols = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %this1, i32 0, i32 3
  store i32 0, i32* %num_unique_symbols, align 8
  ret %"struct.draco::ShannonEntropyTracker::EntropyData"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco21ShannonEntropyTracker4PeekEPKji(%"struct.draco::ShannonEntropyTracker::EntropyData"* noalias sret align 8 %agg.result, %"class.draco::ShannonEntropyTracker"* %this, i32* %symbols, i32 %num_symbols) #0 {
entry:
  %this.addr = alloca %"class.draco::ShannonEntropyTracker"*, align 4
  %symbols.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  store %"class.draco::ShannonEntropyTracker"* %this, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  store i32* %symbols, i32** %symbols.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::ShannonEntropyTracker"*, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  %0 = load i32*, i32** %symbols.addr, align 4
  %1 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZN5draco21ShannonEntropyTracker13UpdateSymbolsEPKjib(%"struct.draco::ShannonEntropyTracker::EntropyData"* sret align 8 %agg.result, %"class.draco::ShannonEntropyTracker"* %this1, i32* %0, i32 %1, i1 zeroext false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco21ShannonEntropyTracker13UpdateSymbolsEPKjib(%"struct.draco::ShannonEntropyTracker::EntropyData"* noalias sret align 8 %agg.result, %"class.draco::ShannonEntropyTracker"* %this, i32* %symbols, i32 %num_symbols, i1 zeroext %push_changes) #0 {
entry:
  %this.addr = alloca %"class.draco::ShannonEntropyTracker"*, align 4
  %symbols.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  %push_changes.addr = alloca i8, align 1
  %i = alloca i32, align 4
  %symbol = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %old_symbol_entropy_norm = alloca double, align 8
  %frequency = alloca i32*, align 4
  %new_symbol_entropy_norm = alloca double, align 8
  %i27 = alloca i32, align 4
  %symbol31 = alloca i32, align 4
  store %"class.draco::ShannonEntropyTracker"* %this, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  store i32* %symbols, i32** %symbols.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %frombool = zext i1 %push_changes to i8
  store i8 %frombool, i8* %push_changes.addr, align 1
  %this1 = load %"class.draco::ShannonEntropyTracker"*, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  %entropy_data_ = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 2
  %0 = bitcast %"struct.draco::ShannonEntropyTracker::EntropyData"* %agg.result to i8*
  %1 = bitcast %"struct.draco::ShannonEntropyTracker::EntropyData"* %entropy_data_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 8 %1, i32 24, i1 false)
  %2 = load i32, i32* %num_symbols.addr, align 4
  %num_values = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %agg.result, i32 0, i32 1
  %3 = load i32, i32* %num_values, align 8
  %add = add nsw i32 %3, %2
  store i32 %add, i32* %num_values, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %num_symbols.addr, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i32*, i32** %symbols.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  %8 = load i32, i32* %arrayidx, align 4
  store i32 %8, i32* %symbol, align 4
  %frequencies_ = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %frequencies_) #7
  %9 = load i32, i32* %symbol, align 4
  %cmp2 = icmp ule i32 %call, %9
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %frequencies_3 = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 0
  %10 = load i32, i32* %symbol, align 4
  %add4 = add i32 %10, 1
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE6resizeEmRKi(%"class.std::__2::vector"* %frequencies_3, i32 %add4, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  store double 0.000000e+00, double* %old_symbol_entropy_norm, align 8
  %frequencies_5 = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 0
  %11 = load i32, i32* %symbol, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %frequencies_5, i32 %11) #7
  store i32* %call6, i32** %frequency, align 4
  %12 = load i32*, i32** %frequency, align 4
  %13 = load i32, i32* %12, align 4
  %cmp7 = icmp sgt i32 %13, 1
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end
  %14 = load i32*, i32** %frequency, align 4
  %15 = load i32, i32* %14, align 4
  %conv = sitofp i32 %15 to double
  %16 = load i32*, i32** %frequency, align 4
  %17 = load i32, i32* %16, align 4
  %call9 = call double @_Z4log2IiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %17) #7
  %mul = fmul double %conv, %call9
  store double %mul, double* %old_symbol_entropy_norm, align 8
  br label %if.end17

if.else:                                          ; preds = %if.end
  %18 = load i32*, i32** %frequency, align 4
  %19 = load i32, i32* %18, align 4
  %cmp10 = icmp eq i32 %19, 0
  br i1 %cmp10, label %if.then11, label %if.end16

if.then11:                                        ; preds = %if.else
  %num_unique_symbols = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %agg.result, i32 0, i32 3
  %20 = load i32, i32* %num_unique_symbols, align 8
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %num_unique_symbols, align 8
  %21 = load i32, i32* %symbol, align 4
  %max_symbol = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %agg.result, i32 0, i32 2
  %22 = load i32, i32* %max_symbol, align 4
  %cmp12 = icmp ugt i32 %21, %22
  br i1 %cmp12, label %if.then13, label %if.end15

if.then13:                                        ; preds = %if.then11
  %23 = load i32, i32* %symbol, align 4
  %max_symbol14 = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %agg.result, i32 0, i32 2
  store i32 %23, i32* %max_symbol14, align 4
  br label %if.end15

if.end15:                                         ; preds = %if.then13, %if.then11
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.else
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.then8
  %24 = load i32*, i32** %frequency, align 4
  %25 = load i32, i32* %24, align 4
  %inc18 = add nsw i32 %25, 1
  store i32 %inc18, i32* %24, align 4
  %26 = load i32*, i32** %frequency, align 4
  %27 = load i32, i32* %26, align 4
  %conv19 = sitofp i32 %27 to double
  %28 = load i32*, i32** %frequency, align 4
  %29 = load i32, i32* %28, align 4
  %call20 = call double @_Z4log2IiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %29) #7
  %mul21 = fmul double %conv19, %call20
  store double %mul21, double* %new_symbol_entropy_norm, align 8
  %30 = load double, double* %new_symbol_entropy_norm, align 8
  %31 = load double, double* %old_symbol_entropy_norm, align 8
  %sub = fsub double %30, %31
  %entropy_norm = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %agg.result, i32 0, i32 0
  %32 = load double, double* %entropy_norm, align 8
  %add22 = fadd double %32, %sub
  store double %add22, double* %entropy_norm, align 8
  br label %for.inc

for.inc:                                          ; preds = %if.end17
  %33 = load i32, i32* %i, align 4
  %inc23 = add nsw i32 %33, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = load i8, i8* %push_changes.addr, align 1
  %tobool = trunc i8 %34 to i1
  br i1 %tobool, label %if.then24, label %if.else26

if.then24:                                        ; preds = %for.end
  %entropy_data_25 = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 2
  %35 = bitcast %"struct.draco::ShannonEntropyTracker::EntropyData"* %entropy_data_25 to i8*
  %36 = bitcast %"struct.draco::ShannonEntropyTracker::EntropyData"* %agg.result to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %35, i8* align 8 %36, i32 20, i1 false)
  br label %if.end38

if.else26:                                        ; preds = %for.end
  store i32 0, i32* %i27, align 4
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc35, %if.else26
  %37 = load i32, i32* %i27, align 4
  %38 = load i32, i32* %num_symbols.addr, align 4
  %cmp29 = icmp slt i32 %37, %38
  br i1 %cmp29, label %for.body30, label %for.end37

for.body30:                                       ; preds = %for.cond28
  %39 = load i32*, i32** %symbols.addr, align 4
  %40 = load i32, i32* %i27, align 4
  %arrayidx32 = getelementptr inbounds i32, i32* %39, i32 %40
  %41 = load i32, i32* %arrayidx32, align 4
  store i32 %41, i32* %symbol31, align 4
  %frequencies_33 = getelementptr inbounds %"class.draco::ShannonEntropyTracker", %"class.draco::ShannonEntropyTracker"* %this1, i32 0, i32 0
  %42 = load i32, i32* %symbol31, align 4
  %call34 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector"* %frequencies_33, i32 %42) #7
  %43 = load i32, i32* %call34, align 4
  %dec = add nsw i32 %43, -1
  store i32 %dec, i32* %call34, align 4
  br label %for.inc35

for.inc35:                                        ; preds = %for.body30
  %44 = load i32, i32* %i27, align 4
  %inc36 = add nsw i32 %44, 1
  store i32 %inc36, i32* %i27, align 4
  br label %for.cond28

for.end37:                                        ; preds = %for.cond28
  br label %if.end38

if.end38:                                         ; preds = %for.end37, %if.then24
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco21ShannonEntropyTracker4PushEPKji(%"struct.draco::ShannonEntropyTracker::EntropyData"* noalias sret align 8 %agg.result, %"class.draco::ShannonEntropyTracker"* %this, i32* %symbols, i32 %num_symbols) #0 {
entry:
  %this.addr = alloca %"class.draco::ShannonEntropyTracker"*, align 4
  %symbols.addr = alloca i32*, align 4
  %num_symbols.addr = alloca i32, align 4
  store %"class.draco::ShannonEntropyTracker"* %this, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  store i32* %symbols, i32** %symbols.addr, align 4
  store i32 %num_symbols, i32* %num_symbols.addr, align 4
  %this1 = load %"class.draco::ShannonEntropyTracker"*, %"class.draco::ShannonEntropyTracker"** %this.addr, align 4
  %0 = load i32*, i32** %symbols.addr, align 4
  %1 = load i32, i32* %num_symbols.addr, align 4
  call void @_ZN5draco21ShannonEntropyTracker13UpdateSymbolsEPKjib(%"struct.draco::ShannonEntropyTracker::EntropyData"* sret align 8 %agg.result, %"class.draco::ShannonEntropyTracker"* %this1, i32* %0, i32 %1, i1 zeroext true)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE6resizeEmRKi(%"class.std::__2::vector"* %this, i32 %__sz, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__sz.addr = alloca i32, align 4
  %__x.addr = alloca i32*, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  %4 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE8__appendEmRKi(%"class.std::__2::vector"* %this1, i32 %sub, i32* nonnull align 4 dereferenceable(4) %4)
  br label %if.end4

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %__cs, align 4
  %6 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %5, %6
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %7 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %7, i32 0, i32 0
  %8 = load i32*, i32** %__begin_, align 4
  %9 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %8, i32 %9
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::vector"* %this1, i32* %add.ptr) #7
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden double @_Z4log2IiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %__lcpp_x) #0 comdat {
entry:
  %__lcpp_x.addr = alloca i32, align 4
  store i32 %__lcpp_x, i32* %__lcpp_x.addr, align 4
  %0 = load i32, i32* %__lcpp_x.addr, align 4
  %conv = sitofp i32 %0 to double
  %1 = call double @llvm.log2.f64(double %conv)
  ret double %1
}

; Function Attrs: noinline nounwind optnone
define hidden i64 @_ZN5draco21ShannonEntropyTracker19GetNumberOfDataBitsERKNS0_11EntropyDataE(%"struct.draco::ShannonEntropyTracker::EntropyData"* nonnull align 8 dereferenceable(20) %entropy_data) #0 {
entry:
  %retval = alloca i64, align 8
  %entropy_data.addr = alloca %"struct.draco::ShannonEntropyTracker::EntropyData"*, align 4
  store %"struct.draco::ShannonEntropyTracker::EntropyData"* %entropy_data, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %0 = load %"struct.draco::ShannonEntropyTracker::EntropyData"*, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %num_values = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %0, i32 0, i32 1
  %1 = load i32, i32* %num_values, align 8
  %cmp = icmp slt i32 %1, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i64 0, i64* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %"struct.draco::ShannonEntropyTracker::EntropyData"*, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %num_values1 = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %2, i32 0, i32 1
  %3 = load i32, i32* %num_values1, align 8
  %conv = sitofp i32 %3 to double
  %4 = load %"struct.draco::ShannonEntropyTracker::EntropyData"*, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %num_values2 = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %4, i32 0, i32 1
  %5 = load i32, i32* %num_values2, align 8
  %call = call double @_Z4log2IiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %5) #7
  %mul = fmul double %conv, %call
  %6 = load %"struct.draco::ShannonEntropyTracker::EntropyData"*, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %entropy_norm = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %6, i32 0, i32 0
  %7 = load double, double* %entropy_norm, align 8
  %sub = fsub double %mul, %7
  %8 = call double @llvm.ceil.f64(double %sub)
  %conv3 = fptosi double %8 to i64
  store i64 %conv3, i64* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i64, i64* %retval, align 8
  ret i64 %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.ceil.f64(double) #1

; Function Attrs: noinline nounwind optnone
define hidden i64 @_ZN5draco21ShannonEntropyTracker24GetNumberOfRAnsTableBitsERKNS0_11EntropyDataE(%"struct.draco::ShannonEntropyTracker::EntropyData"* nonnull align 8 dereferenceable(20) %entropy_data) #0 {
entry:
  %entropy_data.addr = alloca %"struct.draco::ShannonEntropyTracker::EntropyData"*, align 4
  store %"struct.draco::ShannonEntropyTracker::EntropyData"* %entropy_data, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %0 = load %"struct.draco::ShannonEntropyTracker::EntropyData"*, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %max_symbol = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %0, i32 0, i32 2
  %1 = load i32, i32* %max_symbol, align 4
  %add = add nsw i32 %1, 1
  %2 = load %"struct.draco::ShannonEntropyTracker::EntropyData"*, %"struct.draco::ShannonEntropyTracker::EntropyData"** %entropy_data.addr, align 4
  %num_unique_symbols = getelementptr inbounds %"struct.draco::ShannonEntropyTracker::EntropyData", %"struct.draco::ShannonEntropyTracker::EntropyData"* %2, i32 0, i32 3
  %3 = load i32, i32* %num_unique_symbols, align 8
  %call = call i64 @_ZN5dracoL33ApproximateRAnsFrequencyTableBitsEii(i32 %add, i32 %3)
  ret i64 %call
}

; Function Attrs: noinline nounwind optnone
define internal i64 @_ZN5dracoL33ApproximateRAnsFrequencyTableBitsEii(i32 %max_value, i32 %num_unique_symbols) #0 {
entry:
  %max_value.addr = alloca i32, align 4
  %num_unique_symbols.addr = alloca i32, align 4
  %table_zero_frequency_bits = alloca i64, align 8
  store i32 %max_value, i32* %max_value.addr, align 4
  store i32 %num_unique_symbols, i32* %num_unique_symbols.addr, align 4
  %0 = load i32, i32* %num_unique_symbols.addr, align 4
  %1 = load i32, i32* %max_value.addr, align 4
  %2 = load i32, i32* %num_unique_symbols.addr, align 4
  %sub = sub nsw i32 %1, %2
  %div = sdiv i32 %sub, 64
  %add = add nsw i32 %0, %div
  %mul = mul nsw i32 8, %add
  %conv = sext i32 %mul to i64
  store i64 %conv, i64* %table_zero_frequency_bits, align 8
  %3 = load i32, i32* %num_unique_symbols.addr, align 4
  %mul1 = mul nsw i32 8, %3
  %conv2 = sext i32 %mul1 to i64
  %4 = load i64, i64* %table_zero_frequency_bits, align 8
  %add3 = add nsw i64 %conv2, %4
  ret i64 %add3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE11__vallocateEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #7
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #7
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  store i32* %call3, i32** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  store i32* %call3, i32** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load i32*, i32** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %9) #7
  store i32* %add.ptr, i32** %call5, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE18__construct_at_endEmRKi(%"class.std::__2::vector"* %this, i32 %__n, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i32*, align 4
  %__tx = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i32*, i32** %__new_end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %3) #7
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i32*, i32** %__pos_3, align 4
  %call4 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %4) #7
  %5 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32* %call4, i32* nonnull align 4 dereferenceable(4) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %6, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #4 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #8
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #3

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #7
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #7
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJRKiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJRKiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__29allocatorIiE9constructIiJRKiEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE9constructIiJRKiEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKiEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  %4 = load i32, i32* %call, align 4
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base"* %this1, i32* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #7
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE8__appendEmRKi(%"class.std::__2::vector"* %this, i32 %__n, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i32*, align 4
  %__a = alloca %"class.std::__2::allocator"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %0) #7
  %1 = load i32*, i32** %call, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE18__construct_at_endEmRKi(%"class.std::__2::vector"* %this1, i32 %5, i32* nonnull align 4 dereferenceable(4) %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %7) #7
  store %"class.std::__2::allocator"* %call2, %"class.std::__2::allocator"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %8 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %8
  %call4 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %9 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %9)
  %10 = load i32, i32* %__n.addr, align 4
  %11 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE18__construct_at_endEmRKi(%"struct.std::__2::__split_buffer"* %__v, i32 %10, i32* nonnull align 4 dereferenceable(4) %11)
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::vector"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi(%"class.std::__2::vector"* %this1, i32* %0)
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %2 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base"* %1, i32* %2) #7
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #8
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #7
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE18__construct_at_endEmRKi(%"struct.std::__2::__split_buffer"* %this, i32 %__n, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i32*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionC2EPPim(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32** %__end_, i32 %0) #7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load i32*, i32** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load i32*, i32** %__end_2, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load i32*, i32** %__pos_4, align 4
  %call5 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %3) #7
  %4 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJRKiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3, i32* %call5, i32* nonnull align 4 dereferenceable(4) %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %5 = load i32*, i32** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #7
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #7
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #7
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #7
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #7
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #7
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.2"*
  %5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %5) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__end_cap_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) #7
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %1) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__value_, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionC2EPPim(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* returned %this, i32** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca i32**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  store i32** %__p, i32*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32**, i32*** %__p.addr, align 4
  %1 = load i32*, i32** %0, align 4
  store i32* %1, i32** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load i32**, i32*** %__p.addr, align 4
  %3 = load i32*, i32** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %3, i32 %4
  store i32* %add.ptr, i32** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load i32**, i32*** %__p.addr, align 4
  store i32** %5, i32*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load i32**, i32*** %__dest_, align 4
  store i32* %0, i32** %1, align 4
  ret %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #7
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #7
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.3", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.3", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi(%"class.std::__2::vector"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i32, i32* %call4, i32 %2
  %3 = bitcast i32* %add.ptr5 to i8*
  %call6 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call7 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr8 = getelementptr inbounds i32, i32* %call6, i32 %call7
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #7
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable willreturn }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { noreturn }
attributes #9 = { builtin allocsize(0) }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
