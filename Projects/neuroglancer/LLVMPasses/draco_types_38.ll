; ModuleID = './draco/src/draco/core/draco_types.cc'
source_filename = "./draco/src/draco/core/draco_types.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32 %dt) #0 {
entry:
  %retval = alloca i32, align 4
  %dt.addr = alloca i32, align 4
  store i32 %dt, i32* %dt.addr, align 4
  %0 = load i32, i32* %dt.addr, align 4
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb1
    i32 4, label %sw.bb1
    i32 5, label %sw.bb2
    i32 6, label %sw.bb2
    i32 7, label %sw.bb3
    i32 8, label %sw.bb3
    i32 9, label %sw.bb4
    i32 10, label %sw.bb5
    i32 11, label %sw.bb6
  ]

sw.bb:                                            ; preds = %entry, %entry
  store i32 1, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry, %entry
  store i32 2, i32* %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry, %entry
  store i32 4, i32* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry, %entry
  store i32 8, i32* %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i32 4, i32* %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco18IsDataTypeIntegralENS_8DataTypeE(i32 %dt) #0 {
entry:
  %retval = alloca i1, align 1
  %dt.addr = alloca i32, align 4
  store i32 %dt, i32* %dt.addr, align 4
  %0 = load i32, i32* %dt.addr, align 4
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb
    i32 4, label %sw.bb
    i32 5, label %sw.bb
    i32 6, label %sw.bb
    i32 7, label %sw.bb
    i32 8, label %sw.bb
    i32 11, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry
  store i1 true, i1* %retval, align 1
  br label %return

sw.default:                                       ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %sw.default, %sw.bb
  %1 = load i1, i1* %retval, align 1
  ret i1 %1
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
