; ModuleID = './draco/src/draco/core/bit_utils.cc'
source_filename = "./draco/src/draco/core/bit_utils.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

$_ZN5draco24ConvertSignedIntToSymbolIiEENSt3__213make_unsignedIT_E4typeES3_ = comdat any

$_ZN5draco24ConvertSymbolToSignedIntIjEENSt3__211make_signedIT_E4typeES3_ = comdat any

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco26ConvertSignedIntsToSymbolsEPKiiPj(i32* %in, i32 %in_values, i32* %out) #0 {
entry:
  %in.addr = alloca i32*, align 4
  %in_values.addr = alloca i32, align 4
  %out.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store i32* %in, i32** %in.addr, align 4
  store i32 %in_values, i32* %in_values.addr, align 4
  store i32* %out, i32** %out.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %in_values.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32*, i32** %in.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  %call = call i32 @_ZN5draco24ConvertSignedIntToSymbolIiEENSt3__213make_unsignedIT_E4typeES3_(i32 %4)
  %5 = load i32*, i32** %out.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds i32, i32* %5, i32 %6
  store i32 %call, i32* %arrayidx1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco24ConvertSignedIntToSymbolIiEENSt3__213make_unsignedIT_E4typeES3_(i32 %val) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %val.addr = alloca i32, align 4
  %ret = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4
  %0 = load i32, i32* %val.addr, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4
  %shl = shl i32 %1, 1
  store i32 %shl, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4
  %add = add nsw i32 %2, 1
  %sub = sub nsw i32 0, %add
  store i32 %sub, i32* %val.addr, align 4
  %3 = load i32, i32* %val.addr, align 4
  store i32 %3, i32* %ret, align 4
  %4 = load i32, i32* %ret, align 4
  %shl1 = shl i32 %4, 1
  store i32 %shl1, i32* %ret, align 4
  %5 = load i32, i32* %ret, align 4
  %or = or i32 %5, 1
  store i32 %or, i32* %ret, align 4
  %6 = load i32, i32* %ret, align 4
  store i32 %6, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco26ConvertSymbolsToSignedIntsEPKjiPi(i32* %in, i32 %in_values, i32* %out) #0 {
entry:
  %in.addr = alloca i32*, align 4
  %in_values.addr = alloca i32, align 4
  %out.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store i32* %in, i32** %in.addr, align 4
  store i32 %in_values, i32* %in_values.addr, align 4
  store i32* %out, i32** %out.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %in_values.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32*, i32** %in.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  %call = call i32 @_ZN5draco24ConvertSymbolToSignedIntIjEENSt3__211make_signedIT_E4typeES3_(i32 %4)
  %5 = load i32*, i32** %out.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds i32, i32* %5, i32 %6
  store i32 %call, i32* %arrayidx1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN5draco24ConvertSymbolToSignedIntIjEENSt3__211make_signedIT_E4typeES3_(i32 %val) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %val.addr = alloca i32, align 4
  %is_positive = alloca i8, align 1
  %ret = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4
  %0 = load i32, i32* %val.addr, align 4
  %and = and i32 %0, 1
  %tobool = icmp ne i32 %and, 0
  %lnot = xor i1 %tobool, true
  %frombool = zext i1 %lnot to i8
  store i8 %frombool, i8* %is_positive, align 1
  %1 = load i32, i32* %val.addr, align 4
  %shr = lshr i32 %1, 1
  store i32 %shr, i32* %val.addr, align 4
  %2 = load i8, i8* %is_positive, align 1
  %tobool1 = trunc i8 %2 to i1
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4
  store i32 %3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %val.addr, align 4
  store i32 %4, i32* %ret, align 4
  %5 = load i32, i32* %ret, align 4
  %sub = sub nsw i32 0, %5
  %sub2 = sub nsw i32 %sub, 1
  store i32 %sub2, i32* %ret, align 4
  %6 = load i32, i32* %ret, align 4
  store i32 %6, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
