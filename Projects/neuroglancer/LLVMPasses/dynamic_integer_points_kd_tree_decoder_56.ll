; ModuleID = './draco/src/draco/compression/point_cloud/algorithms/dynamic_integer_points_kd_tree_decoder.cc'
source_filename = "./draco/src/draco/compression/point_cloud/algorithms/dynamic_integer_points_kd_tree_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::DynamicIntegerPointsKdTreeDecoder" = type { i32, i32, i32, i32, %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.std::__2::vector", %"class.std::__2::vector", %"class.std::__2::vector.1", %"class.std::__2::vector.1" }
%"class.draco::DirectBitDecoder" = type { %"class.std::__2::vector", %"class.std::__2::__wrap_iter", i32 }
%"class.std::__2::__wrap_iter" = type { i32* }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i32*, i32*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32* }
%"class.std::__2::vector.1" = type { %"class.std::__2::__vector_base.2" }
%"class.std::__2::__vector_base.2" = type { %"class.std::__2::vector"*, %"class.std::__2::vector"*, %"class.std::__2::__compressed_pair.3" }
%"class.std::__2::__compressed_pair.3" = type { %"struct.std::__2::__compressed_pair_elem.4" }
%"struct.std::__2::__compressed_pair_elem.4" = type { %"class.std::__2::vector"* }
%"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus" = type { i32, i32, i32 }
%"class.draco::DynamicIntegerPointsKdTreeDecoder.8" = type { i32, i32, i32, i32, %"class.draco::RAnsBitDecoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.std::__2::vector", %"class.std::__2::vector", %"class.std::__2::vector.1", %"class.std::__2::vector.1" }
%"class.draco::RAnsBitDecoder" = type <{ %"struct.draco::AnsDecoder", i8, [3 x i8] }>
%"struct.draco::AnsDecoder" = type { i8*, i32, i32 }
%"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus" = type { i32, i32, i32 }
%"class.draco::DynamicIntegerPointsKdTreeDecoder.9" = type { i32, i32, i32, i32, %"class.draco::FoldedBit32Decoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.std::__2::vector", %"class.std::__2::vector", %"class.std::__2::vector.1", %"class.std::__2::vector.1" }
%"class.draco::FoldedBit32Decoder" = type { %"struct.std::__2::array", %"class.draco::RAnsBitDecoder" }
%"struct.std::__2::array" = type { [32 x %"class.draco::RAnsBitDecoder"] }
%"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus" = type { i32, i32, i32 }
%"class.draco::DynamicIntegerPointsKdTreeDecoder.10" = type { i32, i32, i32, i32, %"class.draco::FoldedBit32Decoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder", %"class.std::__2::vector", %"class.std::__2::vector", %"class.std::__2::vector.1", %"class.std::__2::vector.1" }
%"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus" = type { i32, i32, i32 }
%"class.std::__2::__wrap_iter.11" = type { i32* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i32*, i32* }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::allocator.6" = type { i8 }
%"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction" = type { %"class.std::__2::vector.1"*, %"class.std::__2::vector"*, %"class.std::__2::vector"* }
%"struct.std::__2::__compressed_pair_elem.5" = type { i8 }
%"struct.std::__2::__has_max_size.12" = type { i8 }
%"struct.std::__2::__has_construct.13" = type { i8 }
%"struct.std::__2::integral_constant.14" = type { i8 }
%"struct.std::__2::__has_select_on_container_copy_construction" = type { i8 }

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EEC5Ej = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj = comdat any

$_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi0EE9dimensionEv = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE12DecodeNumberEiPj = comdat any

$_ZN5draco16DirectBitDecoder28DecodeLeastSignificantBits32EiPj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE14DecodingStatusC5Ejjj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EEC5Ej = comdat any

$_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi2EE9dimensionEv = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE12DecodeNumberEiPj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE14DecodingStatusC5Ejjj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EEC5Ej = comdat any

$_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEEC2Ev = comdat any

$_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi4EE9dimensionEv = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE12DecodeNumberEiPj = comdat any

$_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEE28DecodeLeastSignificantBits32EiPj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE14DecodingStatusC5Ejjj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EEC5Ej = comdat any

$_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi6EE9dimensionEv = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEEixEm = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE12DecodeNumberEiPj = comdat any

$_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE14DecodingStatusC5Ejjj = comdat any

$_ZNSt3__2eqIPKjPjEEbRKNS_11__wrap_iterIT_EERKNS4_IT0_EE = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv = comdat any

$_ZNKSt3__211__wrap_iterIPKjEdeEv = comdat any

$_ZNSt3__211__wrap_iterIPKjEppEv = comdat any

$_ZNKSt3__211__wrap_iterIPKjEplEl = comdat any

$_ZNKSt3__211__wrap_iterIPKjE4baseEv = comdat any

$_ZNKSt3__211__wrap_iterIPjE4baseEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj = comdat any

$_ZNSt3__211__wrap_iterIPjEC2ES1_ = comdat any

$_ZNSt3__211__wrap_iterIPKjEpLEl = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEmRKj = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIjEC2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIjE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorIjE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIjEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIjE7destroyEPj = comdat any

$_ZNSt3__29allocatorIjE10deallocateEPjm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEEC2Ev = comdat any

$_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE11__vallocateEm = comdat any

$_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE18__construct_at_endEmRKS3_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEEC2Ev = comdat any

$_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE8allocateERS5_m = comdat any

$_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE8max_sizeERKS5_ = comdat any

$_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_ = comdat any

$_ZNKSt3__29allocatorINS_6vectorIjNS0_IjEEEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_ = comdat any

$_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE8capacityEv = comdat any

$_ZNSt3__212__to_addressINS_6vectorIjNS_9allocatorIjEEEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE21_ConstructTransactionC2ERS5_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE9constructIS4_JRKS4_EEEvRS5_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE11__constructIS4_JRKS4_EEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKNS_6vectorIjNS_9allocatorIjEEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEE9constructIS3_JRKS3_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEEC2ERKS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE37select_on_container_copy_constructionERKS2_ = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2EOS2_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endIPjEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES7_S7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKS2_ = comdat any

$_ZNSt3__24moveIRNS_9allocatorIjEEEEONS_16remove_referenceIT_E4typeEOS5_ = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnS3_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_9allocatorIjEEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2IS2_vEEOT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE25__construct_range_forwardIjjjjEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS6_RT_EE5valueEvE4typeERS2_PSC_SH_RSB_ = comdat any

$_ZNSt3__25arrayIN5draco14RAnsBitDecoderELm32EEC2Ev = comdat any

$_ZNSt3__25arrayIN5draco14RAnsBitDecoderELm32EEixEm = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EEC1Ej = weak_odr hidden unnamed_addr alias %"class.draco::DynamicIntegerPointsKdTreeDecoder"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder"*, i32), %"class.draco::DynamicIntegerPointsKdTreeDecoder"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder"*, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EEC2Ej
@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE14DecodingStatusC1Ejjj = weak_odr hidden unnamed_addr alias %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"*, i32, i32, i32), %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"*, i32, i32, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE14DecodingStatusC2Ejjj
@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EEC1Ej = weak_odr hidden unnamed_addr alias %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, i32), %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EEC2Ej
@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE14DecodingStatusC1Ejjj = weak_odr hidden unnamed_addr alias %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"*, i32, i32, i32), %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"*, i32, i32, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE14DecodingStatusC2Ejjj
@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EEC1Ej = weak_odr hidden unnamed_addr alias %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, i32), %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EEC2Ej
@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE14DecodingStatusC1Ejjj = weak_odr hidden unnamed_addr alias %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"*, i32, i32, i32), %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"*, i32, i32, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE14DecodingStatusC2Ejjj
@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EEC1Ej = weak_odr hidden unnamed_addr alias %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, i32), %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* (%"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EEC2Ej
@_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE14DecodingStatusC1Ejjj = weak_odr hidden unnamed_addr alias %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"*, i32, i32, i32), %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* (%"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"*, i32, i32, i32)* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE14DecodingStatusC2Ejjj

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"class.draco::DynamicIntegerPointsKdTreeDecoder"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EEC2Ej(%"class.draco::DynamicIntegerPointsKdTreeDecoder"* returned %this, i32 %dimension) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EEC5Ej) {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, align 4
  %dimension.addr = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %ref.tmp8 = alloca %"class.std::__2::vector", align 4
  %ref.tmp9 = alloca i32, align 4
  %ref.tmp15 = alloca %"class.std::__2::vector", align 4
  %ref.tmp16 = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  store i32 %dimension, i32* %dimension.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  %bit_length_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 0
  store i32 0, i32* %bit_length_, align 4
  %num_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 1
  store i32 0, i32* %num_points_, align 4
  %num_decoded_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %num_decoded_points_, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension.addr, align 4
  store i32 %0, i32* %dimension_, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 4
  %call = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %numbers_decoder_)
  %remaining_bits_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 5
  %call2 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %remaining_bits_decoder_)
  %axis_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 6
  %call3 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %axis_decoder_)
  %half_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 7
  %call4 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %half_decoder_)
  %p_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 8
  %1 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp, align 4
  %call5 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %p_, i32 %1, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %axes_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 9
  %2 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp6, align 4
  %call7 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %axes_, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %base_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 10
  %3 = load i32, i32* %dimension.addr, align 4
  %mul = mul i32 32, %3
  %add = add i32 %mul, 1
  %4 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp9, align 4
  %call10 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp8, i32 %4, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %call11 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %base_stack_, i32 %add, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp8)
  %call12 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp8) #7
  %levels_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 11
  %5 = load i32, i32* %dimension.addr, align 4
  %mul13 = mul i32 32, %5
  %add14 = add i32 %mul13, 1
  %6 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp16, align 4
  %call17 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp15, i32 %6, i32* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %call18 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %levels_stack_, i32 %add14, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp15)
  %call19 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp15) #7
  ret %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1
}

declare %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* returned %this, i32 %__n, i32* nonnull align 4 dereferenceable(4) %__x) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* %0) #7
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  %4 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEmRKj(%"class.std::__2::vector"* %this1, i32 %3, i32* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* returned %this, i32 %__n, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__x) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector.1"*, align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.std::__2::vector"* %__x, %"class.std::__2::vector"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  store %"class.std::__2::vector.1"* %this1, %"class.std::__2::vector.1"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEEC2Ev(%"class.std::__2::__vector_base.2"* %0) #7
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE11__vallocateEm(%"class.std::__2::vector.1"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  call void @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE18__construct_at_endEmRKS3_(%"class.std::__2::vector.1"* %this1, i32 %3, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %retval, align 4
  ret %"class.std::__2::vector.1"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* %0) #7
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi0EE9dimensionEv(%"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj(%"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this, i32 %num_remaining_points, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %levels, i32 %last_axis) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, align 4
  %num_remaining_points.addr = alloca i32, align 4
  %levels.addr = alloca %"class.std::__2::vector"*, align 4
  %last_axis.addr = alloca i32, align 4
  %best_axis = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  store i32 %num_remaining_points, i32* %num_remaining_points.addr, align 4
  store %"class.std::__2::vector"* %levels, %"class.std::__2::vector"** %levels.addr, align 4
  store i32 %last_axis, i32* %last_axis.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  %0 = load i32, i32* %last_axis.addr, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 3
  %1 = load i32, i32* %dimension_, align 4
  %sub = sub i32 %1, 1
  %cmp = icmp eq i32 %0, %sub
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load i32, i32* %last_axis.addr, align 4
  %add = add i32 %2, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %add, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden void @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE12DecodeNumberEiPj(%"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this, i32 %nbits, i32* %value) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder"** %this.addr, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder", %"class.draco::DynamicIntegerPointsKdTreeDecoder"* %this1, i32 0, i32 4
  %0 = load i32, i32* %nbits.addr, align 4
  %1 = load i32*, i32** %value.addr, align 4
  call void @_ZN5draco16DirectBitDecoder28DecodeLeastSignificantBits32EiPj(%"class.draco::DirectBitDecoder"* %numbers_decoder_, i32 %0, i32* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco16DirectBitDecoder28DecodeLeastSignificantBits32EiPj(%"class.draco::DirectBitDecoder"* %this, i32 %nbits, i32* %value) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DirectBitDecoder"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  %remaining = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.11", align 4
  %ref.tmp16 = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp20 = alloca %"class.std::__2::__wrap_iter.11", align 4
  %value_l = alloca i32, align 4
  %value_r = alloca i32, align 4
  store %"class.draco::DirectBitDecoder"* %this, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::DirectBitDecoder"*, %"class.draco::DirectBitDecoder"** %this.addr, align 4
  %num_used_bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  %0 = load i32, i32* %num_used_bits_, align 4
  %sub = sub i32 32, %0
  store i32 %sub, i32* %remaining, align 4
  %1 = load i32, i32* %nbits.addr, align 4
  %2 = load i32, i32* %remaining, align 4
  %cmp = icmp sle i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %bits_ = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %bits_) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %ref.tmp, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %call2 = call zeroext i1 @_ZNSt3__2eqIPKjPjEEbRKNS_11__wrap_iterIT_EERKNS4_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %pos_, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %ref.tmp) #7
  br i1 %call2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %3 = load i32*, i32** %value.addr, align 4
  store i32 0, i32* %3, align 4
  br label %if.end44

if.end:                                           ; preds = %if.then
  %pos_4 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPKjEdeEv(%"class.std::__2::__wrap_iter"* %pos_4) #7
  %4 = load i32, i32* %call5, align 4
  %num_used_bits_6 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  %5 = load i32, i32* %num_used_bits_6, align 4
  %shl = shl i32 %4, %5
  %6 = load i32, i32* %nbits.addr, align 4
  %sub7 = sub nsw i32 32, %6
  %shr = lshr i32 %shl, %sub7
  %7 = load i32*, i32** %value.addr, align 4
  store i32 %shr, i32* %7, align 4
  %8 = load i32, i32* %nbits.addr, align 4
  %num_used_bits_8 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  %9 = load i32, i32* %num_used_bits_8, align 4
  %add = add i32 %9, %8
  store i32 %add, i32* %num_used_bits_8, align 4
  %num_used_bits_9 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  %10 = load i32, i32* %num_used_bits_9, align 4
  %cmp10 = icmp eq i32 %10, 32
  br i1 %cmp10, label %if.then11, label %if.end15

if.then11:                                        ; preds = %if.end
  %pos_12 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEppEv(%"class.std::__2::__wrap_iter"* %pos_12) #7
  %num_used_bits_14 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %num_used_bits_14, align 4
  br label %if.end15

if.end15:                                         ; preds = %if.then11, %if.end
  br label %if.end44

if.else:                                          ; preds = %entry
  %pos_17 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %call18 = call i32* @_ZNKSt3__211__wrap_iterIPKjEplEl(%"class.std::__2::__wrap_iter"* %pos_17, i32 1) #7
  %coerce.dive19 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %ref.tmp16, i32 0, i32 0
  store i32* %call18, i32** %coerce.dive19, align 4
  %bits_21 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 0
  %call22 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %bits_21) #7
  %coerce.dive23 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %ref.tmp20, i32 0, i32 0
  store i32* %call22, i32** %coerce.dive23, align 4
  %call24 = call zeroext i1 @_ZNSt3__2eqIPKjPjEEbRKNS_11__wrap_iterIT_EERKNS4_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %ref.tmp16, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %ref.tmp20) #7
  br i1 %call24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.else
  %11 = load i32*, i32** %value.addr, align 4
  store i32 0, i32* %11, align 4
  br label %if.end44

if.end26:                                         ; preds = %if.else
  %pos_27 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPKjEdeEv(%"class.std::__2::__wrap_iter"* %pos_27) #7
  %12 = load i32, i32* %call28, align 4
  %num_used_bits_29 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  %13 = load i32, i32* %num_used_bits_29, align 4
  %shl30 = shl i32 %12, %13
  store i32 %shl30, i32* %value_l, align 4
  %14 = load i32, i32* %nbits.addr, align 4
  %15 = load i32, i32* %remaining, align 4
  %sub31 = sub nsw i32 %14, %15
  %num_used_bits_32 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  store i32 %sub31, i32* %num_used_bits_32, align 4
  %pos_33 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %call34 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEppEv(%"class.std::__2::__wrap_iter"* %pos_33) #7
  %pos_35 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 1
  %call36 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPKjEdeEv(%"class.std::__2::__wrap_iter"* %pos_35) #7
  %16 = load i32, i32* %call36, align 4
  %num_used_bits_37 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  %17 = load i32, i32* %num_used_bits_37, align 4
  %sub38 = sub i32 32, %17
  %shr39 = lshr i32 %16, %sub38
  store i32 %shr39, i32* %value_r, align 4
  %18 = load i32, i32* %value_l, align 4
  %num_used_bits_40 = getelementptr inbounds %"class.draco::DirectBitDecoder", %"class.draco::DirectBitDecoder"* %this1, i32 0, i32 2
  %19 = load i32, i32* %num_used_bits_40, align 4
  %sub41 = sub i32 32, %19
  %20 = load i32, i32* %remaining, align 4
  %sub42 = sub i32 %sub41, %20
  %shr43 = lshr i32 %18, %sub42
  %21 = load i32, i32* %value_r, align 4
  %or = or i32 %shr43, %21
  %22 = load i32*, i32** %value.addr, align 4
  store i32 %or, i32* %22, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then3, %if.then25, %if.end26, %if.end15
  ret void
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE14DecodingStatusC2Ejjj(%"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* returned %this, i32 %num_remaining_points_, i32 %last_axis_, i32 %stack_pos_) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi0EE14DecodingStatusC5Ejjj) {
entry:
  %this.addr = alloca %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"*, align 4
  %num_remaining_points_.addr = alloca i32, align 4
  %last_axis_.addr = alloca i32, align 4
  %stack_pos_.addr = alloca i32, align 4
  store %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* %this, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"** %this.addr, align 4
  store i32 %num_remaining_points_, i32* %num_remaining_points_.addr, align 4
  store i32 %last_axis_, i32* %last_axis_.addr, align 4
  store i32 %stack_pos_, i32* %stack_pos_.addr, align 4
  %this1 = load %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"*, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"** %this.addr, align 4
  %num_remaining_points = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* %this1, i32 0, i32 0
  %0 = load i32, i32* %num_remaining_points_.addr, align 4
  store i32 %0, i32* %num_remaining_points, align 4
  %last_axis = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* %this1, i32 0, i32 1
  %1 = load i32, i32* %last_axis_.addr, align 4
  store i32 %1, i32* %last_axis, align 4
  %stack_pos = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* %this1, i32 0, i32 2
  %2 = load i32, i32* %stack_pos_.addr, align 4
  store i32 %2, i32* %stack_pos, align 4
  ret %"struct.draco::DynamicIntegerPointsKdTreeDecoder<0>::DecodingStatus"* %this1
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EEC2Ej(%"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* returned %this, i32 %dimension) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EEC5Ej) {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, align 4
  %dimension.addr = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %ref.tmp8 = alloca %"class.std::__2::vector", align 4
  %ref.tmp9 = alloca i32, align 4
  %ref.tmp15 = alloca %"class.std::__2::vector", align 4
  %ref.tmp16 = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  store i32 %dimension, i32* %dimension.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  %bit_length_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 0
  store i32 0, i32* %bit_length_, align 4
  %num_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 1
  store i32 0, i32* %num_points_, align 4
  %num_decoded_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 2
  store i32 0, i32* %num_decoded_points_, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension.addr, align 4
  store i32 %0, i32* %dimension_, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 4
  %call = call %"class.draco::RAnsBitDecoder"* @_ZN5draco14RAnsBitDecoderC1Ev(%"class.draco::RAnsBitDecoder"* %numbers_decoder_)
  %remaining_bits_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 5
  %call2 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %remaining_bits_decoder_)
  %axis_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 6
  %call3 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %axis_decoder_)
  %half_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 7
  %call4 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %half_decoder_)
  %p_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 8
  %1 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp, align 4
  %call5 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %p_, i32 %1, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %axes_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 9
  %2 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp6, align 4
  %call7 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %axes_, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %base_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 10
  %3 = load i32, i32* %dimension.addr, align 4
  %mul = mul i32 32, %3
  %add = add i32 %mul, 1
  %4 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp9, align 4
  %call10 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp8, i32 %4, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %call11 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %base_stack_, i32 %add, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp8)
  %call12 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp8) #7
  %levels_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 11
  %5 = load i32, i32* %dimension.addr, align 4
  %mul13 = mul i32 32, %5
  %add14 = add i32 %mul13, 1
  %6 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp16, align 4
  %call17 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp15, i32 %6, i32* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %call18 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %levels_stack_, i32 %add14, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp15)
  %call19 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp15) #7
  ret %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1
}

declare %"class.draco::RAnsBitDecoder"* @_ZN5draco14RAnsBitDecoderC1Ev(%"class.draco::RAnsBitDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi2EE9dimensionEv(%"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj(%"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this, i32 %num_remaining_points, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %levels, i32 %last_axis) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, align 4
  %num_remaining_points.addr = alloca i32, align 4
  %levels.addr = alloca %"class.std::__2::vector"*, align 4
  %last_axis.addr = alloca i32, align 4
  %best_axis = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  store i32 %num_remaining_points, i32* %num_remaining_points.addr, align 4
  store %"class.std::__2::vector"* %levels, %"class.std::__2::vector"** %levels.addr, align 4
  store i32 %last_axis, i32* %last_axis.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  %0 = load i32, i32* %last_axis.addr, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 3
  %1 = load i32, i32* %dimension_, align 4
  %sub = sub i32 %1, 1
  %cmp = icmp eq i32 %0, %sub
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load i32, i32* %last_axis.addr, align 4
  %add = add i32 %2, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %add, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden void @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE12DecodeNumberEiPj(%"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this, i32 %nbits, i32* %value) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"** %this.addr, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.8", %"class.draco::DynamicIntegerPointsKdTreeDecoder.8"* %this1, i32 0, i32 4
  %0 = load i32, i32* %nbits.addr, align 4
  %1 = load i32*, i32** %value.addr, align 4
  call void @_ZN5draco14RAnsBitDecoder28DecodeLeastSignificantBits32EiPj(%"class.draco::RAnsBitDecoder"* %numbers_decoder_, i32 %0, i32* %1)
  ret void
}

declare void @_ZN5draco14RAnsBitDecoder28DecodeLeastSignificantBits32EiPj(%"class.draco::RAnsBitDecoder"*, i32, i32*) #1

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE14DecodingStatusC2Ejjj(%"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* returned %this, i32 %num_remaining_points_, i32 %last_axis_, i32 %stack_pos_) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi2EE14DecodingStatusC5Ejjj) {
entry:
  %this.addr = alloca %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"*, align 4
  %num_remaining_points_.addr = alloca i32, align 4
  %last_axis_.addr = alloca i32, align 4
  %stack_pos_.addr = alloca i32, align 4
  store %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* %this, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"** %this.addr, align 4
  store i32 %num_remaining_points_, i32* %num_remaining_points_.addr, align 4
  store i32 %last_axis_, i32* %last_axis_.addr, align 4
  store i32 %stack_pos_, i32* %stack_pos_.addr, align 4
  %this1 = load %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"*, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"** %this.addr, align 4
  %num_remaining_points = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* %this1, i32 0, i32 0
  %0 = load i32, i32* %num_remaining_points_.addr, align 4
  store i32 %0, i32* %num_remaining_points, align 4
  %last_axis = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* %this1, i32 0, i32 1
  %1 = load i32, i32* %last_axis_.addr, align 4
  store i32 %1, i32* %last_axis, align 4
  %stack_pos = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* %this1, i32 0, i32 2
  %2 = load i32, i32* %stack_pos_.addr, align 4
  store i32 %2, i32* %stack_pos, align 4
  ret %"struct.draco::DynamicIntegerPointsKdTreeDecoder<2>::DecodingStatus"* %this1
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EEC2Ej(%"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* returned %this, i32 %dimension) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EEC5Ej) {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, align 4
  %dimension.addr = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %ref.tmp8 = alloca %"class.std::__2::vector", align 4
  %ref.tmp9 = alloca i32, align 4
  %ref.tmp15 = alloca %"class.std::__2::vector", align 4
  %ref.tmp16 = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  store i32 %dimension, i32* %dimension.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  %bit_length_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 0
  store i32 0, i32* %bit_length_, align 4
  %num_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 1
  store i32 0, i32* %num_points_, align 4
  %num_decoded_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 2
  store i32 0, i32* %num_decoded_points_, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension.addr, align 4
  store i32 %0, i32* %dimension_, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 4
  %call = call %"class.draco::FoldedBit32Decoder"* @_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEEC2Ev(%"class.draco::FoldedBit32Decoder"* %numbers_decoder_)
  %remaining_bits_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 5
  %call2 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %remaining_bits_decoder_)
  %axis_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 6
  %call3 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %axis_decoder_)
  %half_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 7
  %call4 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %half_decoder_)
  %p_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 8
  %1 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp, align 4
  %call5 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %p_, i32 %1, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %axes_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 9
  %2 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp6, align 4
  %call7 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %axes_, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %base_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 10
  %3 = load i32, i32* %dimension.addr, align 4
  %mul = mul i32 32, %3
  %add = add i32 %mul, 1
  %4 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp9, align 4
  %call10 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp8, i32 %4, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %call11 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %base_stack_, i32 %add, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp8)
  %call12 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp8) #7
  %levels_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 11
  %5 = load i32, i32* %dimension.addr, align 4
  %mul13 = mul i32 32, %5
  %add14 = add i32 %mul13, 1
  %6 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp16, align 4
  %call17 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp15, i32 %6, i32* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %call18 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %levels_stack_, i32 %add14, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp15)
  %call19 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp15) #7
  ret %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::FoldedBit32Decoder"* @_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEEC2Ev(%"class.draco::FoldedBit32Decoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::FoldedBit32Decoder"*, align 4
  store %"class.draco::FoldedBit32Decoder"* %this, %"class.draco::FoldedBit32Decoder"** %this.addr, align 4
  %this1 = load %"class.draco::FoldedBit32Decoder"*, %"class.draco::FoldedBit32Decoder"** %this.addr, align 4
  %folded_number_decoders_ = getelementptr inbounds %"class.draco::FoldedBit32Decoder", %"class.draco::FoldedBit32Decoder"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco14RAnsBitDecoderELm32EEC2Ev(%"struct.std::__2::array"* %folded_number_decoders_)
  %bit_decoder_ = getelementptr inbounds %"class.draco::FoldedBit32Decoder", %"class.draco::FoldedBit32Decoder"* %this1, i32 0, i32 1
  %call2 = call %"class.draco::RAnsBitDecoder"* @_ZN5draco14RAnsBitDecoderC1Ev(%"class.draco::RAnsBitDecoder"* %bit_decoder_)
  ret %"class.draco::FoldedBit32Decoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi4EE9dimensionEv(%"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj(%"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this, i32 %num_remaining_points, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %levels, i32 %last_axis) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, align 4
  %num_remaining_points.addr = alloca i32, align 4
  %levels.addr = alloca %"class.std::__2::vector"*, align 4
  %last_axis.addr = alloca i32, align 4
  %best_axis = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  store i32 %num_remaining_points, i32* %num_remaining_points.addr, align 4
  store %"class.std::__2::vector"* %levels, %"class.std::__2::vector"** %levels.addr, align 4
  store i32 %last_axis, i32* %last_axis.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  %0 = load i32, i32* %last_axis.addr, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 3
  %1 = load i32, i32* %dimension_, align 4
  %sub = sub i32 %1, 1
  %cmp = icmp eq i32 %0, %sub
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load i32, i32* %last_axis.addr, align 4
  %add = add i32 %2, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %add, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden void @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE12DecodeNumberEiPj(%"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this, i32 %nbits, i32* %value) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"** %this.addr, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.9", %"class.draco::DynamicIntegerPointsKdTreeDecoder.9"* %this1, i32 0, i32 4
  %0 = load i32, i32* %nbits.addr, align 4
  %1 = load i32*, i32** %value.addr, align 4
  call void @_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEE28DecodeLeastSignificantBits32EiPj(%"class.draco::FoldedBit32Decoder"* %numbers_decoder_, i32 %0, i32* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEE28DecodeLeastSignificantBits32EiPj(%"class.draco::FoldedBit32Decoder"* %this, i32 %nbits, i32* %value) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::FoldedBit32Decoder"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  %result = alloca i32, align 4
  %i = alloca i32, align 4
  %bit = alloca i8, align 1
  store %"class.draco::FoldedBit32Decoder"* %this, %"class.draco::FoldedBit32Decoder"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::FoldedBit32Decoder"*, %"class.draco::FoldedBit32Decoder"** %this.addr, align 4
  store i32 0, i32* %result, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %nbits.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %folded_number_decoders_ = getelementptr inbounds %"class.draco::FoldedBit32Decoder", %"class.draco::FoldedBit32Decoder"* %this1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(13) %"class.draco::RAnsBitDecoder"* @_ZNSt3__25arrayIN5draco14RAnsBitDecoderELm32EEixEm(%"struct.std::__2::array"* %folded_number_decoders_, i32 %2) #7
  %call2 = call zeroext i1 @_ZN5draco14RAnsBitDecoder13DecodeNextBitEv(%"class.draco::RAnsBitDecoder"* %call)
  %frombool = zext i1 %call2 to i8
  store i8 %frombool, i8* %bit, align 1
  %3 = load i32, i32* %result, align 4
  %shl = shl i32 %3, 1
  %4 = load i8, i8* %bit, align 1
  %tobool = trunc i8 %4 to i1
  %conv = zext i1 %tobool to i32
  %add = add i32 %shl, %conv
  store i32 %add, i32* %result, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load i32, i32* %result, align 4
  %7 = load i32*, i32** %value.addr, align 4
  store i32 %6, i32* %7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE14DecodingStatusC2Ejjj(%"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* returned %this, i32 %num_remaining_points_, i32 %last_axis_, i32 %stack_pos_) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi4EE14DecodingStatusC5Ejjj) {
entry:
  %this.addr = alloca %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"*, align 4
  %num_remaining_points_.addr = alloca i32, align 4
  %last_axis_.addr = alloca i32, align 4
  %stack_pos_.addr = alloca i32, align 4
  store %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* %this, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"** %this.addr, align 4
  store i32 %num_remaining_points_, i32* %num_remaining_points_.addr, align 4
  store i32 %last_axis_, i32* %last_axis_.addr, align 4
  store i32 %stack_pos_, i32* %stack_pos_.addr, align 4
  %this1 = load %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"*, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"** %this.addr, align 4
  %num_remaining_points = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* %this1, i32 0, i32 0
  %0 = load i32, i32* %num_remaining_points_.addr, align 4
  store i32 %0, i32* %num_remaining_points, align 4
  %last_axis = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* %this1, i32 0, i32 1
  %1 = load i32, i32* %last_axis_.addr, align 4
  store i32 %1, i32* %last_axis, align 4
  %stack_pos = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* %this1, i32 0, i32 2
  %2 = load i32, i32* %stack_pos_.addr, align 4
  store i32 %2, i32* %stack_pos, align 4
  ret %"struct.draco::DynamicIntegerPointsKdTreeDecoder<4>::DecodingStatus"* %this1
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EEC2Ej(%"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* returned %this, i32 %dimension) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EEC5Ej) {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, align 4
  %dimension.addr = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %ref.tmp8 = alloca %"class.std::__2::vector", align 4
  %ref.tmp9 = alloca i32, align 4
  %ref.tmp15 = alloca %"class.std::__2::vector", align 4
  %ref.tmp16 = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  store i32 %dimension, i32* %dimension.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  %bit_length_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 0
  store i32 0, i32* %bit_length_, align 4
  %num_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 1
  store i32 0, i32* %num_points_, align 4
  %num_decoded_points_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 2
  store i32 0, i32* %num_decoded_points_, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension.addr, align 4
  store i32 %0, i32* %dimension_, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 4
  %call = call %"class.draco::FoldedBit32Decoder"* @_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEEC2Ev(%"class.draco::FoldedBit32Decoder"* %numbers_decoder_)
  %remaining_bits_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 5
  %call2 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %remaining_bits_decoder_)
  %axis_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 6
  %call3 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %axis_decoder_)
  %half_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 7
  %call4 = call %"class.draco::DirectBitDecoder"* @_ZN5draco16DirectBitDecoderC1Ev(%"class.draco::DirectBitDecoder"* %half_decoder_)
  %p_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 8
  %1 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp, align 4
  %call5 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %p_, i32 %1, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %axes_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 9
  %2 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp6, align 4
  %call7 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %axes_, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %base_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 10
  %3 = load i32, i32* %dimension.addr, align 4
  %mul = mul i32 32, %3
  %add = add i32 %mul, 1
  %4 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp9, align 4
  %call10 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp8, i32 %4, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %call11 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %base_stack_, i32 %add, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp8)
  %call12 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp8) #7
  %levels_stack_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 11
  %5 = load i32, i32* %dimension.addr, align 4
  %mul13 = mul i32 32, %5
  %add14 = add i32 %mul13, 1
  %6 = load i32, i32* %dimension.addr, align 4
  store i32 0, i32* %ref.tmp16, align 4
  %call17 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2EmRKj(%"class.std::__2::vector"* %ref.tmp15, i32 %6, i32* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %call18 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEEC2EmRKS3_(%"class.std::__2::vector.1"* %levels_stack_, i32 %add14, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp15)
  %call19 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %ref.tmp15) #7
  ret %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZNK5draco33DynamicIntegerPointsKdTreeDecoderILi6EE9dimensionEv(%"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 3
  %0 = load i32, i32* %dimension_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden i32 @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE7GetAxisEjRKNSt3__26vectorIjNS2_9allocatorIjEEEEj(%"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this, i32 %num_remaining_points, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %levels, i32 %last_axis) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, align 4
  %num_remaining_points.addr = alloca i32, align 4
  %levels.addr = alloca %"class.std::__2::vector"*, align 4
  %last_axis.addr = alloca i32, align 4
  %best_axis = alloca i32, align 4
  %axis = alloca i32, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  store i32 %num_remaining_points, i32* %num_remaining_points.addr, align 4
  store %"class.std::__2::vector"* %levels, %"class.std::__2::vector"** %levels.addr, align 4
  store i32 %last_axis, i32* %last_axis.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  store i32 0, i32* %best_axis, align 4
  %0 = load i32, i32* %num_remaining_points.addr, align 4
  %cmp = icmp ult i32 %0, 64
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 1, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %1 = load i32, i32* %axis, align 4
  %dimension_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 3
  %2 = load i32, i32* %dimension_, align 4
  %cmp2 = icmp ult i32 %1, %2
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %levels.addr, align 4
  %4 = load i32, i32* %best_axis, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %3, i32 %4) #7
  %5 = load i32, i32* %call, align 4
  %6 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %levels.addr, align 4
  %7 = load i32, i32* %axis, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %6, i32 %7) #7
  %8 = load i32, i32* %call3, align 4
  %cmp4 = icmp ugt i32 %5, %8
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %for.body
  %9 = load i32, i32* %axis, align 4
  store i32 %9, i32* %best_axis, align 4
  br label %if.end

if.end:                                           ; preds = %if.then5, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %axis, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end6

if.else:                                          ; preds = %entry
  %axis_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 6
  call void @_ZN5draco16DirectBitDecoder28DecodeLeastSignificantBits32EiPj(%"class.draco::DirectBitDecoder"* %axis_decoder_, i32 4, i32* %best_axis)
  br label %if.end6

if.end6:                                          ; preds = %if.else, %for.end
  %11 = load i32, i32* %best_axis, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden void @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE12DecodeNumberEiPj(%"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this, i32 %nbits, i32* %value) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  store %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"*, %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"** %this.addr, align 4
  %numbers_decoder_ = getelementptr inbounds %"class.draco::DynamicIntegerPointsKdTreeDecoder.10", %"class.draco::DynamicIntegerPointsKdTreeDecoder.10"* %this1, i32 0, i32 4
  %0 = load i32, i32* %nbits.addr, align 4
  %1 = load i32*, i32** %value.addr, align 4
  call void @_ZN5draco18FoldedBit32DecoderINS_14RAnsBitDecoderEE28DecodeLeastSignificantBits32EiPj(%"class.draco::FoldedBit32Decoder"* %numbers_decoder_, i32 %0, i32* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define weak_odr hidden %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* @_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE14DecodingStatusC2Ejjj(%"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* returned %this, i32 %num_remaining_points_, i32 %last_axis_, i32 %stack_pos_) unnamed_addr #0 comdat($_ZN5draco33DynamicIntegerPointsKdTreeDecoderILi6EE14DecodingStatusC5Ejjj) {
entry:
  %this.addr = alloca %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"*, align 4
  %num_remaining_points_.addr = alloca i32, align 4
  %last_axis_.addr = alloca i32, align 4
  %stack_pos_.addr = alloca i32, align 4
  store %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* %this, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"** %this.addr, align 4
  store i32 %num_remaining_points_, i32* %num_remaining_points_.addr, align 4
  store i32 %last_axis_, i32* %last_axis_.addr, align 4
  store i32 %stack_pos_, i32* %stack_pos_.addr, align 4
  %this1 = load %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"*, %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"** %this.addr, align 4
  %num_remaining_points = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* %this1, i32 0, i32 0
  %0 = load i32, i32* %num_remaining_points_.addr, align 4
  store i32 %0, i32* %num_remaining_points, align 4
  %last_axis = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* %this1, i32 0, i32 1
  %1 = load i32, i32* %last_axis_.addr, align 4
  store i32 %1, i32* %last_axis, align 4
  %stack_pos = getelementptr inbounds %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus", %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* %this1, i32 0, i32 2
  %2 = load i32, i32* %stack_pos_.addr, align 4
  store i32 %2, i32* %stack_pos, align 4
  ret %"struct.draco::DynamicIntegerPointsKdTreeDecoder<6>::DecodingStatus"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqIPKjPjEEbRKNS_11__wrap_iterIT_EERKNS4_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.11"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.11"* %__y, %"class.std::__2::__wrap_iter.11"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call i32* @_ZNKSt3__211__wrap_iterIPKjE4baseEv(%"class.std::__2::__wrap_iter"* %0) #7
  %1 = load %"class.std::__2::__wrap_iter.11"*, %"class.std::__2::__wrap_iter.11"** %__y.addr, align 4
  %call1 = call i32* @_ZNKSt3__211__wrap_iterIPjE4baseEv(%"class.std::__2::__wrap_iter.11"* %1) #7
  %cmp = icmp eq i32* %call, %call1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.11", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %call = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this1, i32* %1) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPKjEdeEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEppEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %0, i32 1
  store i32* %incdec.ptr, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__211__wrap_iterIPKjEplEl(%"class.std::__2::__wrap_iter"* %this, i32 %__n) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__wrap_iter"* %retval to i8*
  %1 = bitcast %"class.std::__2::__wrap_iter"* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %2 = load i32, i32* %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEpLEl(%"class.std::__2::__wrap_iter"* %retval, i32 %2) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %3 = load i32*, i32** %coerce.dive, align 4
  ret i32* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__211__wrap_iterIPKjE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__211__wrap_iterIPjE4baseEv(%"class.std::__2::__wrap_iter.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.11"*, align 4
  store %"class.std::__2::__wrap_iter.11"* %this, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.11"*, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this, i32* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.11", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.11"* @_ZNSt3__211__wrap_iterIPjEC2ES1_(%"class.std::__2::__wrap_iter.11"* %retval, i32* %0) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  %1 = load i32*, i32** %coerce.dive, align 4
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.11"* @_ZNSt3__211__wrap_iterIPjEC2ES1_(%"class.std::__2::__wrap_iter.11"* returned %this, i32* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.11"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::__wrap_iter.11"* %this, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.11"*, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__x.addr, align 4
  store i32* %0, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter.11"* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKjEpLEl(%"class.std::__2::__wrap_iter"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__i, align 4
  %add.ptr = getelementptr inbounds i32, i32* %1, i32 %0
  store i32* %add.ptr, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__vallocateEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #7
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #7
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  store i32* %call3, i32** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  store i32* %call3, i32** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load i32*, i32** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %9) #7
  store i32* %add.ptr, i32** %call5, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEmRKj(%"class.std::__2::vector"* %this, i32 %__n, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i32*, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i32*, i32** %__new_end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %3) #7
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i32*, i32** %__pos_3, align 4
  %call4 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %4) #7
  %5 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32* %call4, i32* nonnull align 4 dereferenceable(4) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %6, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #4 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #8
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #3

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #7
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #7
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  %4 = load i32, i32* %call, align 4
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this1, i32* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #7
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEEC2Ev(%"class.std::__2::__vector_base.2"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.2"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  store %"class.std::__2::vector"* null, %"class.std::__2::vector"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 1
  store %"class.std::__2::vector"* null, %"class.std::__2::vector"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.3"* @_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.3"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE11__vallocateEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE8max_sizeEv(%"class.std::__2::vector.1"* %this1) #7
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %2) #7
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call %"class.std::__2::vector"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE8allocateERS5_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %4, i32 0, i32 1
  store %"class.std::__2::vector"* %call3, %"class.std::__2::vector"** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %5, i32 0, i32 0
  store %"class.std::__2::vector"* %call3, %"class.std::__2::vector"** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %9) #7
  store %"class.std::__2::vector"* %add.ptr, %"class.std::__2::vector"** %call5, align 4
  call void @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE14__annotate_newEm(%"class.std::__2::vector.1"* %this1, i32 0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE18__construct_at_endEmRKS3_(%"class.std::__2::vector.1"* %this, i32 %__n, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.std::__2::vector"*, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.std::__2::vector"* %__x, %"class.std::__2::vector"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE21_ConstructTransactionC2ERS5_m(%"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__new_end_, align 4
  %cmp = icmp ne %"class.std::__2::vector"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %3) #7
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__pos_3, align 4
  %call4 = call %"class.std::__2::vector"* @_ZNSt3__212__to_addressINS_6vectorIjNS_9allocatorIjEEEEEEPT_S6_(%"class.std::__2::vector"* %4) #7
  %5 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE9constructIS4_JRKS4_EEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call2, %"class.std::__2::vector"* %call4, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %6, i32 1
  store %"class.std::__2::vector"* %incdec.ptr, %"class.std::__2::vector"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.3"* @_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.3"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.4"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.5"* %2)
  ret %"class.std::__2::__compressed_pair.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.4"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store %"class.std::__2::vector"* null, %"class.std::__2::vector"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.5"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  %call = call %"class.std::__2::allocator.6"* @_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEEC2Ev(%"class.std::__2::allocator.6"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.6"* @_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEEC2Ev(%"class.std::__2::allocator.6"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  ret %"class.std::__2::allocator.6"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE8max_sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE8max_sizeERKS5_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE8allocateERS5_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::vector"* @_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEE8allocateEmPKv(%"class.std::__2::allocator.6"* %0, i32 %1, i8* null)
  ret %"class.std::__2::vector"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #7
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #7
  ret %"class.std::__2::vector"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE14__annotate_newEm(%"class.std::__2::vector.1"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call %"class.std::__2::vector"* @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #7
  %0 = bitcast %"class.std::__2::vector"* %call to i8*
  %call2 = call %"class.std::__2::vector"* @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::vector"* %add.ptr to i8*
  %call4 = call %"class.std::__2::vector"* @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::vector"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::vector"* @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %call7, i32 %3
  %4 = bitcast %"class.std::__2::vector"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_(%"class.std::__2::vector.1"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE8max_sizeERKS5_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.12", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.12"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE7__allocEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #7
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_6vectorIjNS0_IjEEEEE8max_sizeEv(%"class.std::__2::allocator.6"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_6vectorIjNS0_IjEEEEE8max_sizeEv(%"class.std::__2::allocator.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  ret i32 357913941
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #7
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEE8allocateEmPKv(%"class.std::__2::allocator.6"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_6vectorIjNS0_IjEEEEE8max_sizeEv(%"class.std::__2::allocator.6"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::vector"*
  ret %"class.std::__2::vector"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE6secondEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #7
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_6vectorIjNS1_IjEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #7
  ret %"class.std::__2::vector"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  ret %"class.std::__2::vector"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_(%"class.std::__2::vector.1"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE4dataEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__begin_, align 4
  %call = call %"class.std::__2::vector"* @_ZNSt3__212__to_addressINS_6vectorIjNS_9allocatorIjEEEEEEPT_S6_(%"class.std::__2::vector"* %1) #7
  ret %"class.std::__2::vector"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE8capacityEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE8capacityEv(%"class.std::__2::__vector_base.2"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__212__to_addressINS_6vectorIjNS_9allocatorIjEEEEEEPT_S6_(%"class.std::__2::vector"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %__p, %"class.std::__2::vector"** %__p.addr, align 4
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__p.addr, align 4
  ret %"class.std::__2::vector"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE8capacityEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this1) #7
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::vector"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::vector"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNKSt3__213__vector_baseINS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNKSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #7
  ret %"class.std::__2::vector"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNKSt3__217__compressed_pairIPNS_6vectorIjNS_9allocatorIjEEEENS2_IS4_EEE5firstEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNKSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #7
  ret %"class.std::__2::vector"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::vector"** @_ZNKSt3__222__compressed_pair_elemIPNS_6vectorIjNS_9allocatorIjEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  ret %"class.std::__2::vector"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE21_ConstructTransactionC2ERS5_m(%"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.1"* %__v, %"class.std::__2::vector.1"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  store %"class.std::__2::vector.1"* %0, %"class.std::__2::vector.1"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__end_, align 4
  store %"class.std::__2::vector"* %3, %"class.std::__2::vector"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.1"* %4 to %"class.std::__2::__vector_base.2"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %5, i32 0, i32 1
  %6 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %6, i32 %7
  store %"class.std::__2::vector"* %add.ptr, %"class.std::__2::vector"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE9constructIS4_JRKS4_EEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::vector"* %__p, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca %"class.std::__2::vector"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.13", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store %"class.std::__2::vector"* %__p, %"class.std::__2::vector"** %__p.addr, align 4
  store %"class.std::__2::vector"* %__args, %"class.std::__2::vector"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.13"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__p.addr, align 4
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNSt3__27forwardIRKNS_6vectorIjNS_9allocatorIjEEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %3) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE11__constructIS4_JRKS4_EEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::vector"* %2, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS0_IjNS_9allocatorIjEEEENS1_IS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 1
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>, std::__2::allocator<std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_6vectorIjNS1_IjEEEEEEE11__constructIS4_JRKS4_EEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::vector"* %__p, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store %"class.std::__2::vector"* %__p, %"class.std::__2::vector"** %__p.addr, align 4
  store %"class.std::__2::vector"* %__args, %"class.std::__2::vector"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__p.addr, align 4
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNSt3__27forwardIRKNS_6vectorIjNS_9allocatorIjEEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %3) #7
  call void @_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEE9constructIS3_JRKS3_EEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %1, %"class.std::__2::vector"* %2, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNSt3__27forwardIRKNS_6vectorIjNS_9allocatorIjEEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %__t, %"class.std::__2::vector"** %__t.addr, align 4
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__t.addr, align 4
  ret %"class.std::__2::vector"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_6vectorIjNS0_IjEEEEE9constructIS3_JRKS3_EEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %this, %"class.std::__2::vector"* %__p, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store %"class.std::__2::vector"* %__p, %"class.std::__2::vector"** %__p.addr, align 4
  store %"class.std::__2::vector"* %__args, %"class.std::__2::vector"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::vector"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::vector"*
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNSt3__27forwardIRKNS_6vectorIjNS_9allocatorIjEEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %3) #7
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2ERKS3_(%"class.std::__2::vector"* %2, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2ERKS3_(%"class.std::__2::vector"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__x) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca %"class.std::__2::allocator", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator", align 1
  %__n = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %__x, %"class.std::__2::vector"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE37select_on_container_copy_constructionERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call)
  %call2 = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2EOS2_(%"class.std::__2::__vector_base"* %0, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %ref.tmp) #7
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %3) #7
  store i32 %call3, i32* %__n, align 4
  %4 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %5)
  %6 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %7 = bitcast %"class.std::__2::vector"* %6 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %7, i32 0, i32 0
  %8 = load i32*, i32** %__begin_, align 4
  %9 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %10 = bitcast %"class.std::__2::vector"* %9 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %10, i32 0, i32 1
  %11 = load i32*, i32** %__end_, align 4
  %12 = load i32, i32* %__n, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endIPjEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES7_S7_m(%"class.std::__2::vector"* %this1, i32* %8, i32* %11, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE37select_on_container_copy_constructionERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.14", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_select_on_container_copy_construction", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_select_on_container_copy_construction"* %ref.tmp to %"struct.std::__2::integral_constant.14"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2EOS2_(%"class.std::__2::__vector_base"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__24moveIRNS_9allocatorIjEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #7
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnS3_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endIPjEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES7_S7_m(%"class.std::__2::vector"* %this, i32* %__first, i32* %__last, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__first.addr = alloca i32*, align 4
  %__last.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__first, i32** %__first.addr, align 4
  store i32* %__last, i32** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %1) #7
  %2 = load i32*, i32** %__first.addr, align 4
  %3 = load i32*, i32** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE25__construct_range_forwardIjjjjEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS6_RT_EE5valueEvE4typeERS2_PSC_SH_RSB_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32* %2, i32* %3, i32** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.14", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__24moveIRNS_9allocatorIjEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnS3_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorIjEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* %2, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorIjEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorIjEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE25__construct_range_forwardIjjjjEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS6_RT_EE5valueEvE4typeERS2_PSC_SH_RSB_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__begin2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__begin2, i32*** %__begin2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32**, i32*** %__begin2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %6 = bitcast i32* %5 to i8*
  %7 = load i32*, i32** %__begin1.addr, align 4
  %8 = bitcast i32* %7 to i8*
  %9 = load i32, i32* %_Np, align 4
  %mul = mul i32 %9, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %8, i32 %mul, i1 false)
  %10 = load i32, i32* %_Np, align 4
  %11 = load i32**, i32*** %__begin2.addr, align 4
  %12 = load i32*, i32** %11, align 4
  %add.ptr = getelementptr inbounds i32, i32* %12, i32 %10
  store i32* %add.ptr, i32** %11, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco14RAnsBitDecoderELm32EEC2Ev(%"struct.std::__2::array"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::array"*, align 4
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  store %"struct.std::__2::array"* %this1, %"struct.std::__2::array"** %retval, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [32 x %"class.draco::RAnsBitDecoder"], [32 x %"class.draco::RAnsBitDecoder"]* %__elems_, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %array.begin, i32 32
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.draco::RAnsBitDecoder"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %"class.draco::RAnsBitDecoder"* @_ZN5draco14RAnsBitDecoderC1Ev(%"class.draco::RAnsBitDecoder"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.draco::RAnsBitDecoder"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %retval, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(13) %"class.draco::RAnsBitDecoder"* @_ZNSt3__25arrayIN5draco14RAnsBitDecoderELm32EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [32 x %"class.draco::RAnsBitDecoder"], [32 x %"class.draco::RAnsBitDecoder"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::RAnsBitDecoder"* %arrayidx
}

declare zeroext i1 @_ZN5draco14RAnsBitDecoder13DecodeNextBitEv(%"class.draco::RAnsBitDecoder"*) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { noreturn }
attributes #9 = { builtin allocsize(0) }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
