[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.0738867 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 7.1197e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0339429 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0465458 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00446189 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0165435 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00840852 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0303119 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0208795 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0891626 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0408821 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0161608 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0158271 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0186733 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0406013 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0251337 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00658029 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0174289 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00643778 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0251206 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.011887 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0267664 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00335795 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0101217 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0230861 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.011535 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0113908 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0223825 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0464314 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0344342 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00501489 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   3.6591e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00238929 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00515912 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      0.000723576 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00354309 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.020297 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.775617 seconds.
[PassRunner] (final validation)
