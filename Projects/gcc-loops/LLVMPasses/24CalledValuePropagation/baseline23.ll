; ModuleID = '/data/Code/wasm-optimizer-study/Projects/gcc-loops/LLVMPasses/24CalledValuePropagation/original.ll'
source_filename = "gcc-loops.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.A = type { [1024 x i32] }
%"class.std::__2::basic_ostream" = type { i32 (...)**, %"class.std::__2::basic_ios" }
%"class.std::__2::basic_ios" = type { %"class.std::__2::ios_base", %"class.std::__2::basic_ostream"*, i32 }
%"class.std::__2::ios_base" = type { i32 (...)**, i32, i32, i32, i32, i32, i8*, i8*, void (i32, %"class.std::__2::ios_base"*, i32)**, i32*, i32, i32, i32*, i32, i32, i8**, i32, i32 }
%"class.std::__2::locale::id" = type { %"struct.std::__2::once_flag", i32 }
%"struct.std::__2::once_flag" = type { i32 }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i32*, i32*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32* }
%class.Timer = type { i8*, i8, %struct.timeval, %struct.timeval }
%struct.timeval = type { i32, i32 }
%"class.std::__2::__wrap_iter" = type { i32* }
%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry" = type { i8, %"class.std::__2::basic_ostream"* }
%"class.std::__2::ostreambuf_iterator" = type { %"class.std::__2::basic_streambuf"* }
%"class.std::__2::basic_streambuf" = type { i32 (...)**, %"class.std::__2::locale", i8*, i8*, i8*, i8*, i8*, i8* }
%"class.std::__2::locale" = type { %"class.std::__2::locale::__imp"* }
%"class.std::__2::locale::__imp" = type opaque
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"struct.std::__2::iterator" = type { i8 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }
%"class.std::__2::allocator.4" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"class.std::__2::ctype" = type <{ %"class.std::__2::locale::facet", i16*, i8, [3 x i8] }>
%"class.std::__2::locale::facet" = type { %"class.std::__2::__shared_count" }
%"class.std::__2::__shared_count" = type { i32 (...)**, i32 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i32*, i32* }
%"struct.std::__2::__split_buffer" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.6" }
%"class.std::__2::__compressed_pair.6" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.7" }
%"struct.std::__2::__compressed_pair_elem.7" = type { %"class.std::__2::allocator"* }
%"struct.std::__2::__has_construct" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"class.std::length_error" = type { %"class.std::logic_error" }
%"class.std::logic_error" = type { %"class.std::exception", %"class.std::__2::__libcpp_refstring" }
%"class.std::exception" = type { i32 (...)** }
%"class.std::__2::__libcpp_refstring" = type { i8* }
%"struct.std::__2::integral_constant.8" = type { i8 }

$_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZN5TimerC2EPKcb = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj = comdat any

$_ZN5TimerD2Ev = comdat any

$_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E = comdat any

$_ZNSt3__23hexERNS_8ios_baseE = comdat any

$_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc = comdat any

$_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNSt3__28ios_base4setfEjj = comdat any

$_ZNSt3__28ios_base6unsetfEj = comdat any

$_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m = comdat any

$_ZNSt3__211char_traitsIcE6lengthEPKc = comdat any

$_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv = comdat any

$_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_ = comdat any

$_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE = comdat any

$_ZNKSt3__28ios_base5flagsEv = comdat any

$_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv = comdat any

$_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv = comdat any

$_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj = comdat any

$_ZNKSt3__28ios_base5widthEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__28ios_base5widthEl = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv = comdat any

$_ZNKSt3__28ios_base5rdbufEv = comdat any

$_ZNSt3__211char_traitsIcE11eq_int_typeEii = comdat any

$_ZNSt3__211char_traitsIcE3eofEv = comdat any

$_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc = comdat any

$_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE = comdat any

$_ZNKSt3__25ctypeIcE5widenEc = comdat any

$_ZNSt3__28ios_base8setstateEj = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIjEC2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIjEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIjE7destroyEPj = comdat any

$_ZNSt3__29allocatorIjE10deallocateEPjm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIjE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIjE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt12length_errorC2EPKc = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj = comdat any

$_ZNSt3__211__wrap_iterIPjEC2ES1_ = comdat any

$_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_ = comdat any

$_ZNKSt3__211__wrap_iterIPjEdeEv = comdat any

$_ZNSt3__211__wrap_iterIPjEppEv = comdat any

$_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE = comdat any

$_ZNKSt3__211__wrap_iterIPjE4baseEv = comdat any

@usa = hidden global [1024 x i16] zeroinitializer, align 16
@sa = hidden global [1024 x i16] zeroinitializer, align 16
@sb = hidden global [1024 x i16] zeroinitializer, align 16
@sc = hidden global [1024 x i16] zeroinitializer, align 16
@ua = hidden global [1024 x i32] zeroinitializer, align 16
@ia = hidden global [1024 x i32] zeroinitializer, align 16
@ib = hidden global [1024 x i32] zeroinitializer, align 16
@ic = hidden global [1024 x i32] zeroinitializer, align 16
@ub = hidden global [1024 x i32] zeroinitializer, align 16
@uc = hidden global [1024 x i32] zeroinitializer, align 16
@fa = hidden global [1024 x float] zeroinitializer, align 16
@fb = hidden global [1024 x float] zeroinitializer, align 16
@da = hidden global [1024 x float] zeroinitializer, align 16
@db = hidden global [1024 x float] zeroinitializer, align 16
@dc = hidden global [1024 x float] zeroinitializer, align 16
@dd = hidden global [1024 x float] zeroinitializer, align 16
@dj = hidden global [1024 x i32] zeroinitializer, align 16
@s = hidden global %struct.A zeroinitializer, align 4
@a = hidden global [2048 x i32] zeroinitializer, align 16
@b = hidden global [2048 x i32] zeroinitializer, align 16
@c = hidden global [2048 x i32] zeroinitializer, align 16
@d = hidden global [2048 x i32] zeroinitializer, align 16
@G = hidden global [32 x [1024 x i32]] zeroinitializer, align 16
@.str = private unnamed_addr constant [9 x i8] c"Example1\00", align 1
@.str.1 = private unnamed_addr constant [10 x i8] c"Example2a\00", align 1
@.str.2 = private unnamed_addr constant [10 x i8] c"Example2b\00", align 1
@.str.3 = private unnamed_addr constant [9 x i8] c"Example3\00", align 1
@.str.4 = private unnamed_addr constant [10 x i8] c"Example4a\00", align 1
@.str.5 = private unnamed_addr constant [10 x i8] c"Example4b\00", align 1
@.str.6 = private unnamed_addr constant [10 x i8] c"Example4c\00", align 1
@.str.7 = private unnamed_addr constant [9 x i8] c"Example7\00", align 1
@.str.8 = private unnamed_addr constant [9 x i8] c"Example8\00", align 1
@.str.9 = private unnamed_addr constant [9 x i8] c"Example9\00", align 1
@.str.10 = private unnamed_addr constant [11 x i8] c"Example10a\00", align 1
@.str.11 = private unnamed_addr constant [11 x i8] c"Example10b\00", align 1
@.str.12 = private unnamed_addr constant [10 x i8] c"Example11\00", align 1
@.str.13 = private unnamed_addr constant [10 x i8] c"Example12\00", align 1
@.str.14 = private unnamed_addr constant [10 x i8] c"Example23\00", align 1
@.str.15 = private unnamed_addr constant [10 x i8] c"Example24\00", align 1
@.str.16 = private unnamed_addr constant [10 x i8] c"Example25\00", align 1
@_ZNSt3__24coutE = external global %"class.std::__2::basic_ostream", align 4
@.str.17 = private unnamed_addr constant [11 x i8] c"Results: (\00", align 1
@.str.18 = private unnamed_addr constant [3 x i8] c"):\00", align 1
@.str.19 = private unnamed_addr constant [2 x i8] c" \00", align 1
@.str.20 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.21 = private unnamed_addr constant [3 x i8] c", \00", align 1
@.str.22 = private unnamed_addr constant [8 x i8] c", msec\0A\00", align 1
@_ZNSt3__25ctypeIcE2idE = external global %"class.std::__2::locale::id", align 4
@.str.23 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1
@_ZTISt12length_error = external constant i8*
@_ZTVSt12length_error = external unnamed_addr constant { [5 x i8*] }, align 4

; Function Attrs: noinline nounwind optnone
define hidden void @_Z8example1v() #0 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 256
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [2048 x i32], [2048 x i32]* @c, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx1, align 4
  %add = add nsw i32 %2, %4
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [2048 x i32], [2048 x i32]* @a, i32 0, i32 %5
  store i32 %add, i32* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example2aii(i32 %n, i32 %x) #0 {
entry:
  %n.addr = alloca i32, align 4
  %x.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %x.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %3
  store i32 %2, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example2bii(i32 %n, i32 %x) #0 {
entry:
  %n.addr = alloca i32, align 4
  %x.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n.addr, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %n.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [2048 x i32], [2048 x i32]* @c, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx1, align 4
  %and = and i32 %2, %4
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [2048 x i32], [2048 x i32]* @a, i32 0, i32 %5
  store i32 %and, i32* %arrayidx2, align 4
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z8example3iPiS_(i32 %n, i32* noalias %p, i32* noalias %q) #0 {
entry:
  %n.addr = alloca i32, align 4
  %p.addr = alloca i32*, align 4
  %q.addr = alloca i32*, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store i32* %q, i32** %q.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n.addr, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %n.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32*, i32** %q.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %1, i32 1
  store i32* %incdec.ptr, i32** %q.addr, align 4
  %2 = load i32, i32* %1, align 16
  %3 = load i32*, i32** %p.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i32, i32* %3, i32 1
  store i32* %incdec.ptr1, i32** %p.addr, align 4
  store i32 %2, i32* %3, align 16
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example4aiPiS_(i32 %n, i32* noalias %p, i32* noalias %q) #0 {
entry:
  %n.addr = alloca i32, align 4
  %p.addr = alloca i32*, align 4
  %q.addr = alloca i32*, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store i32* %q, i32** %q.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n.addr, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %n.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32*, i32** %q.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %1, i32 1
  store i32* %incdec.ptr, i32** %q.addr, align 4
  %2 = load i32, i32* %1, align 16
  %add = add nsw i32 %2, 5
  %3 = load i32*, i32** %p.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i32, i32* %3, i32 1
  store i32* %incdec.ptr1, i32** %p.addr, align 4
  store i32 %add, i32* %3, align 16
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example4biPiS_(i32 %n, i32* noalias %p, i32* noalias %q) #0 {
entry:
  %n.addr = alloca i32, align 4
  %p.addr = alloca i32*, align 4
  %q.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store i32* %q, i32** %q.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %add = add nsw i32 %2, 1
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %add
  %3 = load i32, i32* %arrayidx, align 4
  %4 = load i32, i32* %i, align 4
  %add1 = add nsw i32 %4, 3
  %arrayidx2 = getelementptr inbounds [2048 x i32], [2048 x i32]* @c, i32 0, i32 %add1
  %5 = load i32, i32* %arrayidx2, align 4
  %add3 = add nsw i32 %3, %5
  %6 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [2048 x i32], [2048 x i32]* @a, i32 0, i32 %6
  store i32 %add3, i32* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example4ciPiS_(i32 %n, i32* noalias %p, i32* noalias %q) #0 {
entry:
  %n.addr = alloca i32, align 4
  %p.addr = alloca i32*, align 4
  %q.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %MAX = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store i32* %q, i32** %q.addr, align 4
  store i32 4, i32* %MAX, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @a, i32 0, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  store i32 %3, i32* %j, align 4
  %4 = load i32, i32* %j, align 4
  %cmp1 = icmp sgt i32 %4, 4
  %5 = zext i1 %cmp1 to i64
  %cond = select i1 %cmp1, i32 4, i32 0
  %6 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %6
  store i32 %cond, i32* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z8example5iP1A(i32 %n, %struct.A* %s) #0 {
entry:
  %n.addr = alloca i32, align 4
  %s.addr = alloca %struct.A*, align 4
  %i = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.A* %s, %struct.A** %s.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.A*, %struct.A** %s.addr, align 4
  %ca = getelementptr inbounds %struct.A, %struct.A* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [1024 x i32], [1024 x i32]* %ca, i32 0, i32 %3
  store i32 5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z8example7i(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %x.addr, align 4
  %add = add nsw i32 %1, %2
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %add
  %3 = load i32, i32* %arrayidx, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [2048 x i32], [2048 x i32]* @a, i32 0, i32 %4
  store i32 %3, i32* %arrayidx1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z8example8i(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc5, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 32
  br i1 %cmp, label %for.body, label %for.end7

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %1 = load i32, i32* %j, align 4
  %cmp2 = icmp slt i32 %1, 1024
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %2 = load i32, i32* %x.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [32 x [1024 x i32]], [32 x [1024 x i32]]* @G, i32 0, i32 %3
  %4 = load i32, i32* %j, align 4
  %arrayidx4 = getelementptr inbounds [1024 x i32], [1024 x i32]* %arrayidx, i32 0, i32 %4
  store i32 %2, i32* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %5 = load i32, i32* %j, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc5

for.inc5:                                         ; preds = %for.end
  %6 = load i32, i32* %i, align 4
  %inc6 = add nsw i32 %6, 1
  store i32 %inc6, i32* %i, align 4
  br label %for.cond

for.end7:                                         ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z8example9Pj(i32* %ret) #0 {
entry:
  %ret.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %diff = alloca i32, align 4
  store i32* %ret, i32** %ret.addr, align 4
  store i32 0, i32* %diff, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [1024 x i32], [1024 x i32]* @ub, i32 0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [1024 x i32], [1024 x i32]* @uc, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx1, align 4
  %sub = sub i32 %2, %4
  %5 = load i32, i32* %diff, align 4
  %add = add i32 %5, %sub
  store i32 %add, i32* %diff, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i32, i32* %diff, align 4
  %8 = load i32*, i32** %ret.addr, align 4
  store i32 %7, i32* %8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z10example10aPsS_S_PiS0_S0_(i16* noalias %sa, i16* noalias %sb, i16* noalias %sc, i32* noalias %ia, i32* noalias %ib, i32* noalias %ic) #0 {
entry:
  %sa.addr = alloca i16*, align 4
  %sb.addr = alloca i16*, align 4
  %sc.addr = alloca i16*, align 4
  %ia.addr = alloca i32*, align 4
  %ib.addr = alloca i32*, align 4
  %ic.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store i16* %sa, i16** %sa.addr, align 4
  store i16* %sb, i16** %sb.addr, align 4
  store i16* %sc, i16** %sc.addr, align 4
  store i32* %ia, i32** %ia.addr, align 4
  store i32* %ib, i32** %ib.addr, align 4
  store i32* %ic, i32** %ic.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32*, i32** %ib.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  %4 = load i32*, i32** %ic.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx1, align 4
  %add = add nsw i32 %3, %6
  %7 = load i32*, i32** %ia.addr, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  store i32 %add, i32* %arrayidx2, align 4
  %9 = load i16*, i16** %sb.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx3, align 2
  %conv = sext i16 %11 to i32
  %12 = load i16*, i16** %sc.addr, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx4, align 2
  %conv5 = sext i16 %14 to i32
  %add6 = add nsw i32 %conv, %conv5
  %conv7 = trunc i32 %add6 to i16
  %15 = load i16*, i16** %sa.addr, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds i16, i16* %15, i32 %16
  store i16 %conv7, i16* %arrayidx8, align 2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z10example10bPsS_S_PiS0_S0_(i16* noalias %sa, i16* noalias %sb, i16* noalias %sc, i32* noalias %ia, i32* noalias %ib, i32* noalias %ic) #0 {
entry:
  %sa.addr = alloca i16*, align 4
  %sb.addr = alloca i16*, align 4
  %sc.addr = alloca i16*, align 4
  %ia.addr = alloca i32*, align 4
  %ib.addr = alloca i32*, align 4
  %ic.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store i16* %sa, i16** %sa.addr, align 4
  store i16* %sb, i16** %sb.addr, align 4
  store i16* %sc, i16** %sc.addr, align 4
  store i32* %ia, i32** %ia.addr, align 4
  store i32* %ib, i32** %ib.addr, align 4
  store i32* %ic, i32** %ic.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i16*, i16** %sb.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 %2
  %3 = load i16, i16* %arrayidx, align 2
  %conv = sext i16 %3 to i32
  %4 = load i32*, i32** %ia.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds i32, i32* %4, i32 %5
  store i32 %conv, i32* %arrayidx1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example11v() #0 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 512
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %mul = mul nsw i32 2, %1
  %add = add nsw i32 %mul, 1
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %add
  %2 = load i32, i32* %arrayidx, align 4
  %3 = load i32, i32* %i, align 4
  %mul1 = mul nsw i32 2, %3
  %add2 = add nsw i32 %mul1, 1
  %arrayidx3 = getelementptr inbounds [2048 x i32], [2048 x i32]* @c, i32 0, i32 %add2
  %4 = load i32, i32* %arrayidx3, align 4
  %mul4 = mul nsw i32 %2, %4
  %5 = load i32, i32* %i, align 4
  %mul5 = mul nsw i32 2, %5
  %arrayidx6 = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %mul5
  %6 = load i32, i32* %arrayidx6, align 4
  %7 = load i32, i32* %i, align 4
  %mul7 = mul nsw i32 2, %7
  %arrayidx8 = getelementptr inbounds [2048 x i32], [2048 x i32]* @c, i32 0, i32 %mul7
  %8 = load i32, i32* %arrayidx8, align 4
  %mul9 = mul nsw i32 %6, %8
  %sub = sub nsw i32 %mul4, %mul9
  %9 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [2048 x i32], [2048 x i32]* @a, i32 0, i32 %9
  store i32 %sub, i32* %arrayidx10, align 4
  %10 = load i32, i32* %i, align 4
  %mul11 = mul nsw i32 2, %10
  %arrayidx12 = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %mul11
  %11 = load i32, i32* %arrayidx12, align 4
  %12 = load i32, i32* %i, align 4
  %mul13 = mul nsw i32 2, %12
  %add14 = add nsw i32 %mul13, 1
  %arrayidx15 = getelementptr inbounds [2048 x i32], [2048 x i32]* @c, i32 0, i32 %add14
  %13 = load i32, i32* %arrayidx15, align 4
  %mul16 = mul nsw i32 %11, %13
  %14 = load i32, i32* %i, align 4
  %mul17 = mul nsw i32 2, %14
  %add18 = add nsw i32 %mul17, 1
  %arrayidx19 = getelementptr inbounds [2048 x i32], [2048 x i32]* @b, i32 0, i32 %add18
  %15 = load i32, i32* %arrayidx19, align 4
  %16 = load i32, i32* %i, align 4
  %mul20 = mul nsw i32 2, %16
  %arrayidx21 = getelementptr inbounds [2048 x i32], [2048 x i32]* @c, i32 0, i32 %mul20
  %17 = load i32, i32* %arrayidx21, align 4
  %mul22 = mul nsw i32 %15, %17
  %add23 = add nsw i32 %mul16, %mul22
  %18 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [2048 x i32], [2048 x i32]* @d, i32 0, i32 %18
  store i32 %add23, i32* %arrayidx24, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example12v() #0 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2048 x i32], [2048 x i32]* @a, i32 0, i32 %2
  store i32 %1, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %3 = load i32, i32* %i, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example13PPiS0_S_(i32** %A, i32** %B, i32* %out) #0 {
entry:
  %A.addr = alloca i32**, align 4
  %B.addr = alloca i32**, align 4
  %out.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %diff = alloca i32, align 4
  store i32** %A, i32*** %A.addr, align 4
  store i32** %B, i32*** %B.addr, align 4
  store i32* %out, i32** %out.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 32
  br i1 %cmp, label %for.body, label %for.end10

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %diff, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %1 = load i32, i32* %j, align 4
  %cmp2 = icmp slt i32 %1, 1024
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %2 = load i32**, i32*** %A.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32*, i32** %2, i32 %3
  %4 = load i32*, i32** %arrayidx, align 4
  %5 = load i32, i32* %j, align 4
  %arrayidx4 = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx4, align 4
  %7 = load i32**, i32*** %B.addr, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds i32*, i32** %7, i32 %8
  %9 = load i32*, i32** %arrayidx5, align 4
  %10 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %sub = sub nsw i32 %6, %11
  %12 = load i32, i32* %diff, align 4
  %add = add nsw i32 %12, %sub
  store i32 %add, i32* %diff, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %13 = load i32, i32* %j, align 4
  %add7 = add nsw i32 %13, 8
  store i32 %add7, i32* %j, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %14 = load i32, i32* %diff, align 4
  %15 = load i32*, i32** %out.addr, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds i32, i32* %15, i32 %16
  store i32 %14, i32* %arrayidx8, align 4
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end10:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example14PPiS0_S_(i32** %in, i32** %coeff, i32* %out) #0 {
entry:
  %in.addr = alloca i32**, align 4
  %coeff.addr = alloca i32**, align 4
  %out.addr = alloca i32*, align 4
  %k = alloca i32, align 4
  %j = alloca i32, align 4
  %i = alloca i32, align 4
  %sum = alloca i32, align 4
  store i32** %in, i32*** %in.addr, align 4
  store i32** %coeff, i32*** %coeff.addr, align 4
  store i32* %out, i32** %out.addr, align 4
  store i32 0, i32* %i, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %0 = load i32, i32* %k, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %sum, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc11, %for.body
  %1 = load i32, i32* %j, align 4
  %cmp2 = icmp slt i32 %1, 32
  br i1 %cmp2, label %for.body3, label %for.end13

for.body3:                                        ; preds = %for.cond1
  store i32 0, i32* %i, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body3
  %2 = load i32, i32* %i, align 4
  %cmp5 = icmp slt i32 %2, 1024
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %3 = load i32**, i32*** %in.addr, align 4
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %k, align 4
  %add = add nsw i32 %4, %5
  %arrayidx = getelementptr inbounds i32*, i32** %3, i32 %add
  %6 = load i32*, i32** %arrayidx, align 4
  %7 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %6, i32 %7
  %8 = load i32, i32* %arrayidx7, align 4
  %9 = load i32**, i32*** %coeff.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds i32*, i32** %9, i32 %10
  %11 = load i32*, i32** %arrayidx8, align 4
  %12 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds i32, i32* %11, i32 %12
  %13 = load i32, i32* %arrayidx9, align 4
  %mul = mul nsw i32 %8, %13
  %14 = load i32, i32* %sum, align 4
  %add10 = add nsw i32 %14, %mul
  store i32 %add10, i32* %sum, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %16 = load i32, i32* %j, align 4
  %inc12 = add nsw i32 %16, 1
  store i32 %inc12, i32* %j, align 4
  br label %for.cond1

for.end13:                                        ; preds = %for.cond1
  %17 = load i32, i32* %sum, align 4
  %18 = load i32*, i32** %out.addr, align 4
  %19 = load i32, i32* %k, align 4
  %arrayidx14 = getelementptr inbounds i32, i32* %18, i32 %19
  store i32 %17, i32* %arrayidx14, align 4
  br label %for.inc15

for.inc15:                                        ; preds = %for.end13
  %20 = load i32, i32* %k, align 4
  %inc16 = add nsw i32 %20, 1
  store i32 %inc16, i32* %k, align 4
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example21Pii(i32* %b, i32 %n) #0 {
entry:
  %b.addr = alloca i32*, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %a = alloca i32, align 4
  store i32* %b, i32** %b.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 0, i32* %a, align 4
  %0 = load i32, i32* %n.addr, align 4
  %sub = sub nsw i32 %0, 1
  store i32 %sub, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  %5 = load i32, i32* %a, align 4
  %add = add nsw i32 %5, %4
  store i32 %add, i32* %a, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i32, i32* %a, align 4
  %8 = load i32*, i32** %b.addr, align 4
  %arrayidx1 = getelementptr inbounds i32, i32* %8, i32 0
  store i32 %7, i32* %arrayidx1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example23PtPj(i16* %src, i32* %dst) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %dst.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4
  store i32* %dst, i32** %dst.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 256
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i16*, i16** %src.addr, align 4
  %incdec.ptr = getelementptr inbounds i16, i16* %1, i32 1
  store i16* %incdec.ptr, i16** %src.addr, align 4
  %2 = load i16, i16* %1, align 2
  %conv = zext i16 %2 to i32
  %shl = shl i32 %conv, 7
  %3 = load i32*, i32** %dst.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i32, i32* %3, i32 1
  store i32* %incdec.ptr1, i32** %dst.addr, align 4
  store i32 %shl, i32* %3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example24ss(i16 signext %x, i16 signext %y) #0 {
entry:
  %x.addr = alloca i16, align 2
  %y.addr = alloca i16, align 2
  %i = alloca i32, align 4
  store i16 %x, i16* %x.addr, align 2
  store i16 %y, i16* %y.addr, align 2
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [1024 x float], [1024 x float]* @fa, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [1024 x float], [1024 x float]* @fb, i32 0, i32 %3
  %4 = load float, float* %arrayidx1, align 4
  %cmp2 = fcmp olt float %2, %4
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %5 = load i16, i16* %x.addr, align 2
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %6 = load i16, i16* %y.addr, align 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i16 [ %5, %cond.true ], [ %6, %cond.false ]
  %conv = sext i16 %cond to i32
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [1024 x i32], [1024 x i32]* @ic, i32 0, i32 %7
  store i32 %conv, i32* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9example25v() #0 {
entry:
  %i = alloca i32, align 4
  %x = alloca i8, align 1
  %y = alloca i8, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [1024 x float], [1024 x float]* @da, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [1024 x float], [1024 x float]* @db, i32 0, i32 %3
  %4 = load float, float* %arrayidx1, align 4
  %cmp2 = fcmp olt float %2, %4
  %conv = zext i1 %cmp2 to i8
  store i8 %conv, i8* %x, align 1
  %5 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [1024 x float], [1024 x float]* @dc, i32 0, i32 %5
  %6 = load float, float* %arrayidx3, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [1024 x float], [1024 x float]* @dd, i32 0, i32 %7
  %8 = load float, float* %arrayidx4, align 4
  %cmp5 = fcmp olt float %6, %8
  %conv6 = zext i1 %cmp5 to i8
  store i8 %conv6, i8* %y, align 1
  %9 = load i8, i8* %x, align 1
  %conv7 = sext i8 %9 to i32
  %10 = load i8, i8* %y, align 1
  %conv8 = sext i8 %10 to i32
  %and = and i32 %conv7, %conv8
  %11 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds [1024 x i32], [1024 x i32]* @dj, i32 0, i32 %11
  store i32 %and, i32* %arrayidx9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z11init_memoryPvS_(i8* %start, i8* %end) #0 {
entry:
  %start.addr = alloca i8*, align 4
  %end.addr = alloca i8*, align 4
  %state = alloca i8, align 1
  store i8* %start, i8** %start.addr, align 4
  store i8* %end, i8** %end.addr, align 4
  store i8 1, i8* %state, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i8*, i8** %start.addr, align 4
  %1 = load i8*, i8** %end.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i8, i8* %state, align 1
  %conv = zext i8 %2 to i32
  %mul = mul nsw i32 %conv, 7
  %conv1 = trunc i32 %mul to i8
  store i8 %conv1, i8* %state, align 1
  %3 = load i8, i8* %state, align 1
  %conv2 = zext i8 %3 to i32
  %xor = xor i32 %conv2, 39
  %conv3 = trunc i32 %xor to i8
  store i8 %conv3, i8* %state, align 1
  %4 = load i8, i8* %state, align 1
  %conv4 = zext i8 %4 to i32
  %add = add nsw i32 %conv4, 1
  %conv5 = trunc i32 %add to i8
  store i8 %conv5, i8* %state, align 1
  %5 = load i8, i8* %state, align 1
  %6 = load i8*, i8** %start.addr, align 4
  store i8 %5, i8* %6, align 1
  %7 = load i8*, i8** %start.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %add.ptr, i8** %start.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z17init_memory_floatPfS_(float* %start, float* %end) #0 {
entry:
  %start.addr = alloca float*, align 4
  %end.addr = alloca float*, align 4
  %state = alloca float, align 4
  store float* %start, float** %start.addr, align 4
  store float* %end, float** %end.addr, align 4
  store float 1.000000e+00, float* %state, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load float*, float** %start.addr, align 4
  %1 = load float*, float** %end.addr, align 4
  %cmp = icmp ne float* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load float, float* %state, align 4
  %conv = fpext float %2 to double
  %mul = fmul double %conv, 1.100000e+00
  %conv1 = fptrunc double %mul to float
  store float %conv1, float* %state, align 4
  %3 = load float, float* %state, align 4
  %4 = load float*, float** %start.addr, align 4
  store float %3, float* %4, align 4
  %5 = load float*, float** %start.addr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %5, i32 1
  store float* %incdec.ptr, float** %start.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_Z13digest_memoryPvS_(i8* %start, i8* %end) #0 {
entry:
  %start.addr = alloca i8*, align 4
  %end.addr = alloca i8*, align 4
  %state = alloca i32, align 4
  store i8* %start, i8** %start.addr, align 4
  store i8* %end, i8** %end.addr, align 4
  store i32 1, i32* %state, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i8*, i8** %start.addr, align 4
  %1 = load i8*, i8** %end.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %state, align 4
  %mul = mul i32 %2, 3
  store i32 %mul, i32* %state, align 4
  %3 = load i8*, i8** %start.addr, align 4
  %4 = load i8, i8* %3, align 1
  %conv = zext i8 %4 to i32
  %5 = load i32, i32* %state, align 4
  %xor = xor i32 %5, %conv
  store i32 %xor, i32* %state, align 4
  %6 = load i32, i32* %state, align 4
  %shr = lshr i32 %6, 8
  %7 = load i32, i32* %state, align 4
  %shl = shl i32 %7, 8
  %xor1 = xor i32 %shr, %shl
  store i32 %xor1, i32* %state, align 4
  %8 = load i8*, i8** %start.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %add.ptr, i8** %start.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %9 = load i32, i32* %state, align 4
  ret i32 %9
}

; Function Attrs: noinline norecurse optnone
define hidden i32 @main(i32 %argc, i8** %argv) #1 {
entry:
  %retval = alloca i32, align 4
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 4
  %print_times = alloca i8, align 1
  %results = alloca %"class.std::__2::vector", align 4
  %dummy = alloca i32, align 4
  %Mi = alloca i32, align 4
  %atimer = alloca %class.Timer, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %atimer4 = alloca %class.Timer, align 4
  %i7 = alloca i32, align 4
  %r14 = alloca i32, align 4
  %atimer17 = alloca %class.Timer, align 4
  %i20 = alloca i32, align 4
  %r27 = alloca i32, align 4
  %atimer30 = alloca %class.Timer, align 4
  %i33 = alloca i32, align 4
  %r40 = alloca i32, align 4
  %atimer43 = alloca %class.Timer, align 4
  %i46 = alloca i32, align 4
  %r53 = alloca i32, align 4
  %atimer56 = alloca %class.Timer, align 4
  %i59 = alloca i32, align 4
  %r66 = alloca i32, align 4
  %atimer69 = alloca %class.Timer, align 4
  %i72 = alloca i32, align 4
  %r79 = alloca i32, align 4
  %atimer82 = alloca %class.Timer, align 4
  %i85 = alloca i32, align 4
  %r92 = alloca i32, align 4
  %atimer95 = alloca %class.Timer, align 4
  %i98 = alloca i32, align 4
  %r105 = alloca i32, align 4
  %atimer108 = alloca %class.Timer, align 4
  %i111 = alloca i32, align 4
  %r118 = alloca i32, align 4
  %atimer120 = alloca %class.Timer, align 4
  %i123 = alloca i32, align 4
  %r130 = alloca i32, align 4
  %atimer134 = alloca %class.Timer, align 4
  %i137 = alloca i32, align 4
  %r144 = alloca i32, align 4
  %atimer147 = alloca %class.Timer, align 4
  %i150 = alloca i32, align 4
  %r157 = alloca i32, align 4
  %atimer160 = alloca %class.Timer, align 4
  %i163 = alloca i32, align 4
  %r170 = alloca i32, align 4
  %atimer173 = alloca %class.Timer, align 4
  %i176 = alloca i32, align 4
  %r183 = alloca i32, align 4
  %atimer186 = alloca %class.Timer, align 4
  %i189 = alloca i32, align 4
  %r196 = alloca i32, align 4
  %atimer198 = alloca %class.Timer, align 4
  %i201 = alloca i32, align 4
  %r208 = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %agg.tmp214 = alloca %"class.std::__2::__wrap_iter", align 4
  %i222 = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 %argc, i32* %argc.addr, align 4
  store i8** %argv, i8*** %argv.addr, align 4
  store i8 1, i8* %print_times, align 1
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %results) #12
  store i32 0, i32* %dummy, align 4
  store i32 262144, i32* %Mi, align 4
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i32]* @ia to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i32]* @ib to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i32]* @ic to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ic, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i16]* @sa to i8*), i8* bitcast (i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sa, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i16]* @sb to i8*), i8* bitcast (i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sb, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i16]* @sc to i8*), i8* bitcast (i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sc, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([2048 x i32]* @a to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @a, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([2048 x i32]* @b to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @b, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([2048 x i32]* @c to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @c, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i32]* @ua to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ua, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i32]* @ub to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ub, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([1024 x i32]* @uc to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @uc, i64 1, i32 0) to i8*))
  call void @_Z11init_memoryPvS_(i8* bitcast ([32 x [1024 x i32]]* @G to i8*), i8* bitcast (i32* getelementptr inbounds ([32 x [1024 x i32]], [32 x [1024 x i32]]* @G, i32 0, i64 1, i32 0) to i8*))
  call void @_Z17init_memory_floatPfS_(float* getelementptr inbounds ([1024 x float], [1024 x float]* @fa, i32 0, i32 0), float* getelementptr inbounds ([1024 x float], [1024 x float]* @fa, i64 1, i32 0))
  call void @_Z17init_memory_floatPfS_(float* getelementptr inbounds ([1024 x float], [1024 x float]* @fb, i32 0, i32 0), float* getelementptr inbounds ([1024 x float], [1024 x float]* @fb, i64 1, i32 0))
  call void @_Z17init_memory_floatPfS_(float* getelementptr inbounds ([1024 x float], [1024 x float]* @da, i32 0, i32 0), float* getelementptr inbounds ([1024 x float], [1024 x float]* @da, i64 1, i32 0))
  call void @_Z17init_memory_floatPfS_(float* getelementptr inbounds ([1024 x float], [1024 x float]* @db, i32 0, i32 0), float* getelementptr inbounds ([1024 x float], [1024 x float]* @db, i64 1, i32 0))
  call void @_Z17init_memory_floatPfS_(float* getelementptr inbounds ([1024 x float], [1024 x float]* @dc, i32 0, i32 0), float* getelementptr inbounds ([1024 x float], [1024 x float]* @dc, i64 1, i32 0))
  call void @_Z17init_memory_floatPfS_(float* getelementptr inbounds ([1024 x float], [1024 x float]* @dd, i32 0, i32 0), float* getelementptr inbounds ([1024 x float], [1024 x float]* @dd, i64 1, i32 0))
  call void @_Z8example1v()
  %0 = load i8, i8* %print_times, align 1
  %tobool = trunc i8 %0 to i1
  %call1 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0), i1 zeroext %tobool)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 2621440
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @_Z8example1v()
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call2 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([2048 x i32]* @a to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @a, i32 0, i32 256) to i8*))
  store i32 %call2, i32* %r, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r)
  %call3 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer) #12
  call void @_Z9example2aii(i32 1024, i32 2)
  %3 = load i8, i8* %print_times, align 1
  %tobool5 = trunc i8 %3 to i1
  %call6 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer4, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i32 0, i32 0), i1 zeroext %tobool5)
  store i32 0, i32* %i7, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc11, %for.end
  %4 = load i32, i32* %i7, align 4
  %cmp9 = icmp slt i32 %4, 1048576
  br i1 %cmp9, label %for.body10, label %for.end13

for.body10:                                       ; preds = %for.cond8
  call void @_Z9example2aii(i32 1024, i32 2)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body10
  %5 = load i32, i32* %i7, align 4
  %inc12 = add nsw i32 %5, 1
  store i32 %inc12, i32* %i7, align 4
  br label %for.cond8

for.end13:                                        ; preds = %for.cond8
  %call15 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([2048 x i32]* @b to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @b, i32 0, i32 1024) to i8*))
  store i32 %call15, i32* %r14, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r14)
  %call16 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer4) #12
  call void @_Z9example2bii(i32 1024, i32 2)
  %6 = load i8, i8* %print_times, align 1
  %tobool18 = trunc i8 %6 to i1
  %call19 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer17, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.2, i32 0, i32 0), i1 zeroext %tobool18)
  store i32 0, i32* %i20, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc24, %for.end13
  %7 = load i32, i32* %i20, align 4
  %cmp22 = icmp slt i32 %7, 524288
  br i1 %cmp22, label %for.body23, label %for.end26

for.body23:                                       ; preds = %for.cond21
  call void @_Z9example2bii(i32 1024, i32 2)
  br label %for.inc24

for.inc24:                                        ; preds = %for.body23
  %8 = load i32, i32* %i20, align 4
  %inc25 = add nsw i32 %8, 1
  store i32 %inc25, i32* %i20, align 4
  br label %for.cond21

for.end26:                                        ; preds = %for.cond21
  %call28 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([2048 x i32]* @a to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @a, i32 0, i32 1024) to i8*))
  store i32 %call28, i32* %r27, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r27)
  %call29 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer17) #12
  call void @_Z8example3iPiS_(i32 1024, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  %9 = load i8, i8* %print_times, align 1
  %tobool31 = trunc i8 %9 to i1
  %call32 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer30, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.3, i32 0, i32 0), i1 zeroext %tobool31)
  store i32 0, i32* %i33, align 4
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc37, %for.end26
  %10 = load i32, i32* %i33, align 4
  %cmp35 = icmp slt i32 %10, 524288
  br i1 %cmp35, label %for.body36, label %for.end39

for.body36:                                       ; preds = %for.cond34
  call void @_Z8example3iPiS_(i32 1024, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  br label %for.inc37

for.inc37:                                        ; preds = %for.body36
  %11 = load i32, i32* %i33, align 4
  %inc38 = add nsw i32 %11, 1
  store i32 %inc38, i32* %i33, align 4
  br label %for.cond34

for.end39:                                        ; preds = %for.cond34
  %call41 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i32]* @ia to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i64 1, i32 0) to i8*))
  store i32 %call41, i32* %r40, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r40)
  %call42 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer30) #12
  call void @_Z9example4aiPiS_(i32 1024, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  %12 = load i8, i8* %print_times, align 1
  %tobool44 = trunc i8 %12 to i1
  %call45 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer43, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.4, i32 0, i32 0), i1 zeroext %tobool44)
  store i32 0, i32* %i46, align 4
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc50, %for.end39
  %13 = load i32, i32* %i46, align 4
  %cmp48 = icmp slt i32 %13, 524288
  br i1 %cmp48, label %for.body49, label %for.end52

for.body49:                                       ; preds = %for.cond47
  call void @_Z9example4aiPiS_(i32 1024, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  br label %for.inc50

for.inc50:                                        ; preds = %for.body49
  %14 = load i32, i32* %i46, align 4
  %inc51 = add nsw i32 %14, 1
  store i32 %inc51, i32* %i46, align 4
  br label %for.cond47

for.end52:                                        ; preds = %for.cond47
  %call54 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i32]* @ia to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i64 1, i32 0) to i8*))
  store i32 %call54, i32* %r53, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r53)
  %call55 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer43) #12
  call void @_Z9example4biPiS_(i32 1014, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  %15 = load i8, i8* %print_times, align 1
  %tobool57 = trunc i8 %15 to i1
  %call58 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer56, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.5, i32 0, i32 0), i1 zeroext %tobool57)
  store i32 0, i32* %i59, align 4
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc63, %for.end52
  %16 = load i32, i32* %i59, align 4
  %cmp61 = icmp slt i32 %16, 524288
  br i1 %cmp61, label %for.body62, label %for.end65

for.body62:                                       ; preds = %for.cond60
  call void @_Z9example4biPiS_(i32 1014, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  br label %for.inc63

for.inc63:                                        ; preds = %for.body62
  %17 = load i32, i32* %i59, align 4
  %inc64 = add nsw i32 %17, 1
  store i32 %inc64, i32* %i59, align 4
  br label %for.cond60

for.end65:                                        ; preds = %for.cond60
  %call67 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i32]* @ia to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i64 1, i32 0) to i8*))
  store i32 %call67, i32* %r66, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r66)
  %call68 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer56) #12
  call void @_Z9example4ciPiS_(i32 1024, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  %18 = load i8, i8* %print_times, align 1
  %tobool70 = trunc i8 %18 to i1
  %call71 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer69, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.6, i32 0, i32 0), i1 zeroext %tobool70)
  store i32 0, i32* %i72, align 4
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc76, %for.end65
  %19 = load i32, i32* %i72, align 4
  %cmp74 = icmp slt i32 %19, 524288
  br i1 %cmp74, label %for.body75, label %for.end78

for.body75:                                       ; preds = %for.cond73
  call void @_Z9example4ciPiS_(i32 1024, i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0))
  br label %for.inc76

for.inc76:                                        ; preds = %for.body75
  %20 = load i32, i32* %i72, align 4
  %inc77 = add nsw i32 %20, 1
  store i32 %inc77, i32* %i72, align 4
  br label %for.cond73

for.end78:                                        ; preds = %for.cond73
  %call80 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i32]* @ib to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i64 1, i32 0) to i8*))
  store i32 %call80, i32* %r79, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r79)
  %call81 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer69) #12
  call void @_Z8example7i(i32 4)
  %21 = load i8, i8* %print_times, align 1
  %tobool83 = trunc i8 %21 to i1
  %call84 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer82, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.7, i32 0, i32 0), i1 zeroext %tobool83)
  store i32 0, i32* %i85, align 4
  br label %for.cond86

for.cond86:                                       ; preds = %for.inc89, %for.end78
  %22 = load i32, i32* %i85, align 4
  %cmp87 = icmp slt i32 %22, 1048576
  br i1 %cmp87, label %for.body88, label %for.end91

for.body88:                                       ; preds = %for.cond86
  call void @_Z8example7i(i32 4)
  br label %for.inc89

for.inc89:                                        ; preds = %for.body88
  %23 = load i32, i32* %i85, align 4
  %inc90 = add nsw i32 %23, 1
  store i32 %inc90, i32* %i85, align 4
  br label %for.cond86

for.end91:                                        ; preds = %for.cond86
  %call93 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([2048 x i32]* @a to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @a, i32 0, i32 1024) to i8*))
  store i32 %call93, i32* %r92, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r92)
  %call94 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer82) #12
  call void @_Z8example8i(i32 8)
  %24 = load i8, i8* %print_times, align 1
  %tobool96 = trunc i8 %24 to i1
  %call97 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer95, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.8, i32 0, i32 0), i1 zeroext %tobool96)
  store i32 0, i32* %i98, align 4
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc102, %for.end91
  %25 = load i32, i32* %i98, align 4
  %cmp100 = icmp slt i32 %25, 65536
  br i1 %cmp100, label %for.body101, label %for.end104

for.body101:                                      ; preds = %for.cond99
  call void @_Z8example8i(i32 8)
  br label %for.inc102

for.inc102:                                       ; preds = %for.body101
  %26 = load i32, i32* %i98, align 4
  %inc103 = add nsw i32 %26, 1
  store i32 %inc103, i32* %i98, align 4
  br label %for.cond99

for.end104:                                       ; preds = %for.cond99
  %call106 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([32 x [1024 x i32]]* @G to i8*), i8* bitcast (i32* getelementptr inbounds ([32 x [1024 x i32]], [32 x [1024 x i32]]* @G, i32 0, i64 1, i32 0) to i8*))
  store i32 %call106, i32* %r105, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r105)
  %call107 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer95) #12
  call void @_Z8example9Pj(i32* %dummy)
  %27 = load i8, i8* %print_times, align 1
  %tobool109 = trunc i8 %27 to i1
  %call110 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer108, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.9, i32 0, i32 0), i1 zeroext %tobool109)
  store i32 0, i32* %i111, align 4
  br label %for.cond112

for.cond112:                                      ; preds = %for.inc115, %for.end104
  %28 = load i32, i32* %i111, align 4
  %cmp113 = icmp slt i32 %28, 524288
  br i1 %cmp113, label %for.body114, label %for.end117

for.body114:                                      ; preds = %for.cond112
  call void @_Z8example9Pj(i32* %dummy)
  br label %for.inc115

for.inc115:                                       ; preds = %for.body114
  %29 = load i32, i32* %i111, align 4
  %inc116 = add nsw i32 %29, 1
  store i32 %inc116, i32* %i111, align 4
  br label %for.cond112

for.end117:                                       ; preds = %for.cond112
  %30 = load i32, i32* %dummy, align 4
  store i32 %30, i32* %r118, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r118)
  %call119 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer108) #12
  call void @_Z10example10aPsS_S_PiS0_S0_(i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sa, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sb, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sc, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ic, i32 0, i32 0))
  %31 = load i8, i8* %print_times, align 1
  %tobool121 = trunc i8 %31 to i1
  %call122 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer120, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.10, i32 0, i32 0), i1 zeroext %tobool121)
  store i32 0, i32* %i123, align 4
  br label %for.cond124

for.cond124:                                      ; preds = %for.inc127, %for.end117
  %32 = load i32, i32* %i123, align 4
  %cmp125 = icmp slt i32 %32, 524288
  br i1 %cmp125, label %for.body126, label %for.end129

for.body126:                                      ; preds = %for.cond124
  call void @_Z10example10aPsS_S_PiS0_S0_(i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sa, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sb, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sc, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ic, i32 0, i32 0))
  br label %for.inc127

for.inc127:                                       ; preds = %for.body126
  %33 = load i32, i32* %i123, align 4
  %inc128 = add nsw i32 %33, 1
  store i32 %inc128, i32* %i123, align 4
  br label %for.cond124

for.end129:                                       ; preds = %for.cond124
  %call131 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i32]* @ia to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i64 1, i32 0) to i8*))
  %call132 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i16]* @sa to i8*), i8* bitcast (i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sa, i64 1, i32 0) to i8*))
  %add = add i32 %call131, %call132
  store i32 %add, i32* %r130, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r130)
  %call133 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer120) #12
  call void @_Z10example10bPsS_S_PiS0_S0_(i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sa, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sb, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sc, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ic, i32 0, i32 0))
  %34 = load i8, i8* %print_times, align 1
  %tobool135 = trunc i8 %34 to i1
  %call136 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer134, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.11, i32 0, i32 0), i1 zeroext %tobool135)
  store i32 0, i32* %i137, align 4
  br label %for.cond138

for.cond138:                                      ; preds = %for.inc141, %for.end129
  %35 = load i32, i32* %i137, align 4
  %cmp139 = icmp slt i32 %35, 1048576
  br i1 %cmp139, label %for.body140, label %for.end143

for.body140:                                      ; preds = %for.cond138
  call void @_Z10example10bPsS_S_PiS0_S0_(i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sa, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sb, i32 0, i32 0), i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @sc, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ib, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ic, i32 0, i32 0))
  br label %for.inc141

for.inc141:                                       ; preds = %for.body140
  %36 = load i32, i32* %i137, align 4
  %inc142 = add nsw i32 %36, 1
  store i32 %inc142, i32* %i137, align 4
  br label %for.cond138

for.end143:                                       ; preds = %for.cond138
  %call145 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i32]* @ia to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ia, i64 1, i32 0) to i8*))
  store i32 %call145, i32* %r144, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r144)
  %call146 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer134) #12
  call void @_Z9example11v()
  %37 = load i8, i8* %print_times, align 1
  %tobool148 = trunc i8 %37 to i1
  %call149 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer147, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.12, i32 0, i32 0), i1 zeroext %tobool148)
  store i32 0, i32* %i150, align 4
  br label %for.cond151

for.cond151:                                      ; preds = %for.inc154, %for.end143
  %38 = load i32, i32* %i150, align 4
  %cmp152 = icmp slt i32 %38, 524288
  br i1 %cmp152, label %for.body153, label %for.end156

for.body153:                                      ; preds = %for.cond151
  call void @_Z9example11v()
  br label %for.inc154

for.inc154:                                       ; preds = %for.body153
  %39 = load i32, i32* %i150, align 4
  %inc155 = add nsw i32 %39, 1
  store i32 %inc155, i32* %i150, align 4
  br label %for.cond151

for.end156:                                       ; preds = %for.cond151
  %call158 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([2048 x i32]* @d to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @d, i32 0, i32 1024) to i8*))
  store i32 %call158, i32* %r157, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r157)
  %call159 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer147) #12
  call void @_Z9example12v()
  %40 = load i8, i8* %print_times, align 1
  %tobool161 = trunc i8 %40 to i1
  %call162 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer160, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.13, i32 0, i32 0), i1 zeroext %tobool161)
  store i32 0, i32* %i163, align 4
  br label %for.cond164

for.cond164:                                      ; preds = %for.inc167, %for.end156
  %41 = load i32, i32* %i163, align 4
  %cmp165 = icmp slt i32 %41, 1048576
  br i1 %cmp165, label %for.body166, label %for.end169

for.body166:                                      ; preds = %for.cond164
  call void @_Z9example12v()
  br label %for.inc167

for.inc167:                                       ; preds = %for.body166
  %42 = load i32, i32* %i163, align 4
  %inc168 = add nsw i32 %42, 1
  store i32 %inc168, i32* %i163, align 4
  br label %for.cond164

for.end169:                                       ; preds = %for.cond164
  %call171 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([2048 x i32]* @a to i8*), i8* bitcast (i32* getelementptr inbounds ([2048 x i32], [2048 x i32]* @a, i32 0, i32 1024) to i8*))
  store i32 %call171, i32* %r170, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r170)
  %call172 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer160) #12
  call void @_Z9example23PtPj(i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @usa, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ua, i32 0, i32 0))
  %43 = load i8, i8* %print_times, align 1
  %tobool174 = trunc i8 %43 to i1
  %call175 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer173, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.14, i32 0, i32 0), i1 zeroext %tobool174)
  store i32 0, i32* %i176, align 4
  br label %for.cond177

for.cond177:                                      ; preds = %for.inc180, %for.end169
  %44 = load i32, i32* %i176, align 4
  %cmp178 = icmp slt i32 %44, 2097152
  br i1 %cmp178, label %for.body179, label %for.end182

for.body179:                                      ; preds = %for.cond177
  call void @_Z9example23PtPj(i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @usa, i32 0, i32 0), i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @ua, i32 0, i32 0))
  br label %for.inc180

for.inc180:                                       ; preds = %for.body179
  %45 = load i32, i32* %i176, align 4
  %inc181 = add nsw i32 %45, 1
  store i32 %inc181, i32* %i176, align 4
  br label %for.cond177

for.end182:                                       ; preds = %for.cond177
  %call184 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i16]* @usa to i8*), i8* bitcast (i16* getelementptr inbounds ([1024 x i16], [1024 x i16]* @usa, i32 0, i32 256) to i8*))
  store i32 %call184, i32* %r183, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r183)
  %call185 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer173) #12
  call void @_Z9example24ss(i16 signext 2, i16 signext 4)
  %46 = load i8, i8* %print_times, align 1
  %tobool187 = trunc i8 %46 to i1
  %call188 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer186, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.15, i32 0, i32 0), i1 zeroext %tobool187)
  store i32 0, i32* %i189, align 4
  br label %for.cond190

for.cond190:                                      ; preds = %for.inc193, %for.end182
  %47 = load i32, i32* %i189, align 4
  %cmp191 = icmp slt i32 %47, 524288
  br i1 %cmp191, label %for.body192, label %for.end195

for.body192:                                      ; preds = %for.cond190
  call void @_Z9example24ss(i16 signext 2, i16 signext 4)
  br label %for.inc193

for.inc193:                                       ; preds = %for.body192
  %48 = load i32, i32* %i189, align 4
  %inc194 = add nsw i32 %48, 1
  store i32 %inc194, i32* %i189, align 4
  br label %for.cond190

for.end195:                                       ; preds = %for.cond190
  store i32 0, i32* %r196, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r196)
  %call197 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer186) #12
  call void @_Z9example25v()
  %49 = load i8, i8* %print_times, align 1
  %tobool199 = trunc i8 %49 to i1
  %call200 = call %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* %atimer198, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.16, i32 0, i32 0), i1 zeroext %tobool199)
  store i32 0, i32* %i201, align 4
  br label %for.cond202

for.cond202:                                      ; preds = %for.inc205, %for.end195
  %50 = load i32, i32* %i201, align 4
  %cmp203 = icmp slt i32 %50, 524288
  br i1 %cmp203, label %for.body204, label %for.end207

for.body204:                                      ; preds = %for.cond202
  call void @_Z9example25v()
  br label %for.inc205

for.inc205:                                       ; preds = %for.body204
  %51 = load i32, i32* %i201, align 4
  %inc206 = add nsw i32 %51, 1
  store i32 %inc206, i32* %i201, align 4
  br label %for.cond202

for.end207:                                       ; preds = %for.cond202
  %call209 = call i32 @_Z13digest_memoryPvS_(i8* bitcast ([1024 x i32]* @dj to i8*), i8* bitcast (i32* getelementptr inbounds ([1024 x i32], [1024 x i32]* @dj, i64 1, i32 0) to i8*))
  store i32 %call209, i32* %r208, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %results, i32* nonnull align 4 dereferenceable(4) %r208)
  %call210 = call %class.Timer* @_ZN5TimerD2Ev(%class.Timer* %atimer198) #12
  %call211 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E(%"class.std::__2::basic_ostream"* @_ZNSt3__24coutE, %"class.std::__2::ios_base"* (%"class.std::__2::ios_base"*)* @_ZNSt3__23hexERNS_8ios_baseE)
  %call212 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) @_ZNSt3__24coutE, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.17, i32 0, i32 0))
  %call213 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv(%"class.std::__2::vector"* %results) #12
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  store i32* %call213, i32** %coerce.dive, align 4
  %call215 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %results) #12
  %coerce.dive216 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp214, i32 0, i32 0
  store i32* %call215, i32** %coerce.dive216, align 4
  %coerce.dive217 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %52 = load i32*, i32** %coerce.dive217, align 4
  %coerce.dive218 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp214, i32 0, i32 0
  %53 = load i32*, i32** %coerce.dive218, align 4
  %call219 = call i32 @_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_(i32* %52, i32* %53, i32 0)
  %call220 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEi(%"class.std::__2::basic_ostream"* %call212, i32 %call219)
  %call221 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call220, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.18, i32 0, i32 0))
  store i32 0, i32* %i222, align 4
  br label %for.cond223

for.cond223:                                      ; preds = %for.inc230, %for.end207
  %54 = load i32, i32* %i222, align 4
  %call224 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %results) #12
  %cmp225 = icmp ult i32 %54, %call224
  br i1 %cmp225, label %for.body226, label %for.end232

for.body226:                                      ; preds = %for.cond223
  %call227 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) @_ZNSt3__24coutE, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.19, i32 0, i32 0))
  %55 = load i32, i32* %i222, align 4
  %call228 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %results, i32 %55) #12
  %56 = load i32, i32* %call228, align 4
  %call229 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEj(%"class.std::__2::basic_ostream"* %call227, i32 %56)
  br label %for.inc230

for.inc230:                                       ; preds = %for.body226
  %57 = load i32, i32* %i222, align 4
  %inc231 = add i32 %57, 1
  store i32 %inc231, i32* %i222, align 4
  br label %for.cond223

for.end232:                                       ; preds = %for.cond223
  %call233 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) @_ZNSt3__24coutE, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.20, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  %call234 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %results) #12
  %58 = load i32, i32* %retval, align 4
  ret i32 %58
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* %0) #12
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.Timer* @_ZN5TimerC2EPKcb(%class.Timer* returned %this, i8* %title, i1 zeroext %print) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.Timer*, align 4
  %title.addr = alloca i8*, align 4
  %print.addr = alloca i8, align 1
  store %class.Timer* %this, %class.Timer** %this.addr, align 4
  store i8* %title, i8** %title.addr, align 4
  %frombool = zext i1 %print to i8
  store i8 %frombool, i8* %print.addr, align 1
  %this1 = load %class.Timer*, %class.Timer** %this.addr, align 4
  %Start = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 2
  %End = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 3
  %0 = load i8*, i8** %title.addr, align 4
  %Title = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 0
  store i8* %0, i8** %Title, align 4
  %1 = load i8, i8* %print.addr, align 1
  %tobool = trunc i8 %1 to i1
  %Print = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %Print, align 4
  %Start3 = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 2
  %call = call i32 @gettimeofday(%struct.timeval* %Start3, i8* null)
  ret %class.Timer* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %2) #12
  %3 = load i32*, i32** %call, align 4
  %cmp = icmp ne i32* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_(%"class.std::__2::vector"* %this1, i32* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_(%"class.std::__2::vector"* %this1, i32* nonnull align 4 dereferenceable(4) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.Timer* @_ZN5TimerD2Ev(%class.Timer* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.Timer*, align 4
  %this.addr = alloca %class.Timer*, align 4
  %mtime = alloca i32, align 4
  %s = alloca i32, align 4
  %us = alloca i32, align 4
  store %class.Timer* %this, %class.Timer** %this.addr, align 4
  %this1 = load %class.Timer*, %class.Timer** %this.addr, align 4
  store %class.Timer* %this1, %class.Timer** %retval, align 4
  %End = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 3
  %call = call i32 @gettimeofday(%struct.timeval* %End, i8* null)
  %End2 = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 3
  %tv_sec = getelementptr inbounds %struct.timeval, %struct.timeval* %End2, i32 0, i32 0
  %0 = load i32, i32* %tv_sec, align 4
  %Start = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 2
  %tv_sec3 = getelementptr inbounds %struct.timeval, %struct.timeval* %Start, i32 0, i32 0
  %1 = load i32, i32* %tv_sec3, align 4
  %sub = sub nsw i32 %0, %1
  store i32 %sub, i32* %s, align 4
  %End4 = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 3
  %tv_usec = getelementptr inbounds %struct.timeval, %struct.timeval* %End4, i32 0, i32 1
  %2 = load i32, i32* %tv_usec, align 4
  %Start5 = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 2
  %tv_usec6 = getelementptr inbounds %struct.timeval, %struct.timeval* %Start5, i32 0, i32 1
  %3 = load i32, i32* %tv_usec6, align 4
  %sub7 = sub nsw i32 %2, %3
  store i32 %sub7, i32* %us, align 4
  %4 = load i32, i32* %s, align 4
  %mul = mul nsw i32 %4, 1000
  %conv = sitofp i32 %mul to double
  %5 = load i32, i32* %us, align 4
  %conv8 = sitofp i32 %5 to double
  %div = fdiv double %conv8, 1.000000e+03
  %add = fadd double %conv, %div
  %add9 = fadd double %add, 5.000000e-01
  %conv10 = fptosi double %add9 to i32
  store i32 %conv10, i32* %mtime, align 4
  %Print = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 1
  %6 = load i8, i8* %Print, align 4
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %Title = getelementptr inbounds %class.Timer, %class.Timer* %this1, i32 0, i32 0
  %7 = load i8*, i8** %Title, align 4
  %call11 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) @_ZNSt3__24coutE, i8* %7)
  %call12 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call11, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.21, i32 0, i32 0))
  %8 = load i32, i32* %mtime, align 4
  %call13 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEl(%"class.std::__2::basic_ostream"* %call12, i32 %8)
  %call14 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call13, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.22, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %class.Timer*, %class.Timer** %retval, align 4
  ret %class.Timer* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E(%"class.std::__2::basic_ostream"* %this, %"class.std::__2::ios_base"* (%"class.std::__2::ios_base"*)* %__pf) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__pf.addr = alloca %"class.std::__2::ios_base"* (%"class.std::__2::ios_base"*)*, align 4
  store %"class.std::__2::basic_ostream"* %this, %"class.std::__2::basic_ostream"** %this.addr, align 4
  store %"class.std::__2::ios_base"* (%"class.std::__2::ios_base"*)* %__pf, %"class.std::__2::ios_base"* (%"class.std::__2::ios_base"*)** %__pf.addr, align 4
  %this1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %this.addr, align 4
  %0 = load %"class.std::__2::ios_base"* (%"class.std::__2::ios_base"*)*, %"class.std::__2::ios_base"* (%"class.std::__2::ios_base"*)** %__pf.addr, align 4
  %1 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8**
  %vtable = load i8*, i8** %1, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %2 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %2, align 4
  %3 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %vbase.offset
  %4 = bitcast i8* %add.ptr to %"class.std::__2::ios_base"*
  %call = call nonnull align 4 dereferenceable(72) %"class.std::__2::ios_base"* %0(%"class.std::__2::ios_base"* nonnull align 4 dereferenceable(72) %4)
  ret %"class.std::__2::basic_ostream"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(72) %"class.std::__2::ios_base"* @_ZNSt3__23hexERNS_8ios_baseE(%"class.std::__2::ios_base"* nonnull align 4 dereferenceable(72) %__str) #2 comdat {
entry:
  %__str.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %__str, %"class.std::__2::ios_base"** %__str.addr, align 4
  %0 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %__str.addr, align 4
  %call = call i32 @_ZNSt3__28ios_base4setfEjj(%"class.std::__2::ios_base"* %0, i32 8, i32 74)
  %1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %__str.addr, align 4
  ret %"class.std::__2::ios_base"* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__os, i8* %__str) #2 comdat {
entry:
  %__os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__str.addr = alloca i8*, align 4
  store %"class.std::__2::basic_ostream"* %__os, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  store i8* %__str, i8** %__str.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %1 = load i8*, i8** %__str.addr, align 4
  %2 = load i8*, i8** %__str.addr, align 4
  %call = call i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %2) #12
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %0, i8* %1, i32 %call)
  ret %"class.std::__2::basic_ostream"* %call1
}

declare nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEi(%"class.std::__2::basic_ostream"*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_(i32* %__first.coerce, i32* %__last.coerce, i32 %__init) #0 comdat {
entry:
  %__first = alloca %"class.std::__2::__wrap_iter", align 4
  %__last = alloca %"class.std::__2::__wrap_iter", align 4
  %__init.addr = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__first, i32 0, i32 0
  store i32* %__first.coerce, i32** %coerce.dive, align 4
  %coerce.dive1 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__last, i32 0, i32 0
  store i32* %__last.coerce, i32** %coerce.dive1, align 4
  store i32 %__init, i32* %__init.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %call = call zeroext i1 @_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__first, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__last) #12
  br i1 %call, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %0 = load i32, i32* %__init.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPjEdeEv(%"class.std::__2::__wrap_iter"* %__first) #12
  %1 = load i32, i32* %call2, align 4
  %add = add i32 %0, %1
  store i32 %add, i32* %__init.addr, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPjEppEv(%"class.std::__2::__wrap_iter"* %__first) #12
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %2 = load i32, i32* %__init.addr, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this1, i32* %1) #12
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %call = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this1, i32* %1) #12
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store i32* %call, i32** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load i32*, i32** %coerce.dive2, align 4
  ret i32* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

declare nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEj(%"class.std::__2::basic_ostream"*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #12
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* %0) #12
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: nofree nounwind
declare noundef i32 @gettimeofday(%struct.timeval* nocapture noundef, i8* nocapture noundef) #4

declare nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEl(%"class.std::__2::basic_ostream"*, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNSt3__28ios_base4setfEjj(%"class.std::__2::ios_base"* %this, i32 %__fmtfl, i32 %__mask) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__fmtfl.addr = alloca i32, align 4
  %__mask.addr = alloca i32, align 4
  %__r = alloca i32, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  store i32 %__fmtfl, i32* %__fmtfl.addr, align 4
  store i32 %__mask, i32* %__mask.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__fmtflags_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__fmtflags_, align 4
  store i32 %0, i32* %__r, align 4
  %1 = load i32, i32* %__mask.addr, align 4
  call void @_ZNSt3__28ios_base6unsetfEj(%"class.std::__2::ios_base"* %this1, i32 %1)
  %2 = load i32, i32* %__fmtfl.addr, align 4
  %3 = load i32, i32* %__mask.addr, align 4
  %and = and i32 %2, %3
  %__fmtflags_2 = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 1
  %4 = load i32, i32* %__fmtflags_2, align 4
  %or = or i32 %4, %and
  store i32 %or, i32* %__fmtflags_2, align 4
  %5 = load i32, i32* %__r, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__28ios_base6unsetfEj(%"class.std::__2::ios_base"* %this, i32 %__mask) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__mask.addr = alloca i32, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  store i32 %__mask, i32* %__mask.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %0 = load i32, i32* %__mask.addr, align 4
  %neg = xor i32 %0, -1
  %__fmtflags_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__fmtflags_, align 4
  %and = and i32 %1, %neg
  store i32 %and, i32* %__fmtflags_, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__os, i8* %__str, i32 %__len) #2 comdat {
entry:
  %__os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__str.addr = alloca i8*, align 4
  %__len.addr = alloca i32, align 4
  %__s = alloca %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry", align 4
  %ref.tmp = alloca %"class.std::__2::ostreambuf_iterator", align 4
  %agg.tmp = alloca %"class.std::__2::ostreambuf_iterator", align 4
  store %"class.std::__2::basic_ostream"* %__os, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  store i8* %__str, i8** %__str.addr, align 4
  store i32 %__len, i32* %__len.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %call = call %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %__s, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %0)
  %call1 = call zeroext i1 @_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %__s)
  br i1 %call1, label %if.then, label %if.end23

if.then:                                          ; preds = %entry
  %1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %call2 = call %"class.std::__2::ostreambuf_iterator"* @_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE(%"class.std::__2::ostreambuf_iterator"* %agg.tmp, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %1) #12
  %2 = load i8*, i8** %__str.addr, align 4
  %3 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %4 = bitcast %"class.std::__2::basic_ostream"* %3 to i8**
  %vtable = load i8*, i8** %4, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %5 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %5, align 4
  %6 = bitcast %"class.std::__2::basic_ostream"* %3 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %vbase.offset
  %7 = bitcast i8* %add.ptr to %"class.std::__2::ios_base"*
  %call3 = call i32 @_ZNKSt3__28ios_base5flagsEv(%"class.std::__2::ios_base"* %7)
  %and = and i32 %call3, 176
  %cmp = icmp eq i32 %and, 32
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %8 = load i8*, i8** %__str.addr, align 4
  %9 = load i32, i32* %__len.addr, align 4
  %add.ptr4 = getelementptr inbounds i8, i8* %8, i32 %9
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %10 = load i8*, i8** %__str.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %add.ptr4, %cond.true ], [ %10, %cond.false ]
  %11 = load i8*, i8** %__str.addr, align 4
  %12 = load i32, i32* %__len.addr, align 4
  %add.ptr5 = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %14 = bitcast %"class.std::__2::basic_ostream"* %13 to i8**
  %vtable6 = load i8*, i8** %14, align 4
  %vbase.offset.ptr7 = getelementptr i8, i8* %vtable6, i64 -12
  %15 = bitcast i8* %vbase.offset.ptr7 to i32*
  %vbase.offset8 = load i32, i32* %15, align 4
  %16 = bitcast %"class.std::__2::basic_ostream"* %13 to i8*
  %add.ptr9 = getelementptr inbounds i8, i8* %16, i32 %vbase.offset8
  %17 = bitcast i8* %add.ptr9 to %"class.std::__2::ios_base"*
  %18 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %19 = bitcast %"class.std::__2::basic_ostream"* %18 to i8**
  %vtable10 = load i8*, i8** %19, align 4
  %vbase.offset.ptr11 = getelementptr i8, i8* %vtable10, i64 -12
  %20 = bitcast i8* %vbase.offset.ptr11 to i32*
  %vbase.offset12 = load i32, i32* %20, align 4
  %21 = bitcast %"class.std::__2::basic_ostream"* %18 to i8*
  %add.ptr13 = getelementptr inbounds i8, i8* %21, i32 %vbase.offset12
  %22 = bitcast i8* %add.ptr13 to %"class.std::__2::basic_ios"*
  %call14 = call signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv(%"class.std::__2::basic_ios"* %22)
  %coerce.dive = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %agg.tmp, i32 0, i32 0
  %23 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %coerce.dive, align 4
  %call15 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_(%"class.std::__2::basic_streambuf"* %23, i8* %2, i8* %cond, i8* %add.ptr5, %"class.std::__2::ios_base"* nonnull align 4 dereferenceable(72) %17, i8 signext %call14)
  %coerce.dive16 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* %call15, %"class.std::__2::basic_streambuf"** %coerce.dive16, align 4
  %call17 = call zeroext i1 @_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv(%"class.std::__2::ostreambuf_iterator"* %ref.tmp) #12
  br i1 %call17, label %if.then18, label %if.end

if.then18:                                        ; preds = %cond.end
  %24 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %25 = bitcast %"class.std::__2::basic_ostream"* %24 to i8**
  %vtable19 = load i8*, i8** %25, align 4
  %vbase.offset.ptr20 = getelementptr i8, i8* %vtable19, i64 -12
  %26 = bitcast i8* %vbase.offset.ptr20 to i32*
  %vbase.offset21 = load i32, i32* %26, align 4
  %27 = bitcast %"class.std::__2::basic_ostream"* %24 to i8*
  %add.ptr22 = getelementptr inbounds i8, i8* %27, i32 %vbase.offset21
  %28 = bitcast i8* %add.ptr22 to %"class.std::__2::basic_ios"*
  call void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj(%"class.std::__2::basic_ios"* %28, i32 5)
  br label %if.end

if.end:                                           ; preds = %if.then18, %cond.end
  br label %if.end23

if.end23:                                         ; preds = %if.end, %entry
  %call24 = call %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %__s) #12
  %29 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  ret %"class.std::__2::basic_ostream"* %29
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %__s) #0 comdat {
entry:
  %__s.addr = alloca i8*, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %call = call i32 @strlen(i8* %0) #12
  ret i32 %call
}

declare %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* returned, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"*, align 4
  store %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %this, %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"*, %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"** %this.addr, align 4
  %__ok_ = getelementptr inbounds %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry", %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %this1, i32 0, i32 0
  %0 = load i8, i8* %__ok_, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::basic_streambuf"* @_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_(%"class.std::__2::basic_streambuf"* %__s.coerce, i8* %__ob, i8* %__op, i8* %__oe, %"class.std::__2::ios_base"* nonnull align 4 dereferenceable(72) %__iob, i8 signext %__fl) #2 comdat {
entry:
  %retval = alloca %"class.std::__2::ostreambuf_iterator", align 4
  %__s = alloca %"class.std::__2::ostreambuf_iterator", align 4
  %__ob.addr = alloca i8*, align 4
  %__op.addr = alloca i8*, align 4
  %__oe.addr = alloca i8*, align 4
  %__iob.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__fl.addr = alloca i8, align 1
  %__sz = alloca i32, align 4
  %__ns = alloca i32, align 4
  %__np = alloca i32, align 4
  %__sp = alloca %"class.std::__2::basic_string", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* %__s.coerce, %"class.std::__2::basic_streambuf"** %coerce.dive, align 4
  store i8* %__ob, i8** %__ob.addr, align 4
  store i8* %__op, i8** %__op.addr, align 4
  store i8* %__oe, i8** %__oe.addr, align 4
  store %"class.std::__2::ios_base"* %__iob, %"class.std::__2::ios_base"** %__iob.addr, align 4
  store i8 %__fl, i8* %__fl.addr, align 1
  %__sbuf_ = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_, align 4
  %cmp = icmp eq %"class.std::__2::basic_streambuf"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %2 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** %__oe.addr, align 4
  %4 = load i8*, i8** %__ob.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__sz, align 4
  %5 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %__iob.addr, align 4
  %call = call i32 @_ZNKSt3__28ios_base5widthEv(%"class.std::__2::ios_base"* %5)
  store i32 %call, i32* %__ns, align 4
  %6 = load i32, i32* %__ns, align 4
  %7 = load i32, i32* %__sz, align 4
  %cmp1 = icmp sgt i32 %6, %7
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %8 = load i32, i32* %__sz, align 4
  %9 = load i32, i32* %__ns, align 4
  %sub = sub nsw i32 %9, %8
  store i32 %sub, i32* %__ns, align 4
  br label %if.end3

if.else:                                          ; preds = %if.end
  store i32 0, i32* %__ns, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.else, %if.then2
  %10 = load i8*, i8** %__op.addr, align 4
  %11 = load i8*, i8** %__ob.addr, align 4
  %sub.ptr.lhs.cast4 = ptrtoint i8* %10 to i32
  %sub.ptr.rhs.cast5 = ptrtoint i8* %11 to i32
  %sub.ptr.sub6 = sub i32 %sub.ptr.lhs.cast4, %sub.ptr.rhs.cast5
  store i32 %sub.ptr.sub6, i32* %__np, align 4
  %12 = load i32, i32* %__np, align 4
  %cmp7 = icmp sgt i32 %12, 0
  br i1 %cmp7, label %if.then8, label %if.end15

if.then8:                                         ; preds = %if.end3
  %__sbuf_9 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %13 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_9, align 4
  %14 = load i8*, i8** %__ob.addr, align 4
  %15 = load i32, i32* %__np, align 4
  %call10 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %13, i8* %14, i32 %15)
  %16 = load i32, i32* %__np, align 4
  %cmp11 = icmp ne i32 %call10, %16
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.then8
  %__sbuf_13 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* null, %"class.std::__2::basic_streambuf"** %__sbuf_13, align 4
  %17 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %18 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 4, i1 false)
  br label %return

if.end14:                                         ; preds = %if.then8
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.end3
  %19 = load i32, i32* %__ns, align 4
  %cmp16 = icmp sgt i32 %19, 0
  br i1 %cmp16, label %if.then17, label %if.end27

if.then17:                                        ; preds = %if.end15
  %20 = load i32, i32* %__ns, align 4
  %21 = load i8, i8* %__fl.addr, align 1
  %call18 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc(%"class.std::__2::basic_string"* %__sp, i32 %20, i8 signext %21)
  %__sbuf_19 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %22 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_19, align 4
  %call20 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__sp) #12
  %23 = load i32, i32* %__ns, align 4
  %call21 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %22, i8* %call20, i32 %23)
  %24 = load i32, i32* %__ns, align 4
  %cmp22 = icmp ne i32 %call21, %24
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.then17
  %__sbuf_24 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* null, %"class.std::__2::basic_streambuf"** %__sbuf_24, align 4
  %25 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %26 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 4, i1 false)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end25:                                         ; preds = %if.then17
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end25, %if.then23
  %call26 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %__sp) #12
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end27

if.end27:                                         ; preds = %cleanup.cont, %if.end15
  %27 = load i8*, i8** %__oe.addr, align 4
  %28 = load i8*, i8** %__op.addr, align 4
  %sub.ptr.lhs.cast28 = ptrtoint i8* %27 to i32
  %sub.ptr.rhs.cast29 = ptrtoint i8* %28 to i32
  %sub.ptr.sub30 = sub i32 %sub.ptr.lhs.cast28, %sub.ptr.rhs.cast29
  store i32 %sub.ptr.sub30, i32* %__np, align 4
  %29 = load i32, i32* %__np, align 4
  %cmp31 = icmp sgt i32 %29, 0
  br i1 %cmp31, label %if.then32, label %if.end39

if.then32:                                        ; preds = %if.end27
  %__sbuf_33 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %30 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_33, align 4
  %31 = load i8*, i8** %__op.addr, align 4
  %32 = load i32, i32* %__np, align 4
  %call34 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %30, i8* %31, i32 %32)
  %33 = load i32, i32* %__np, align 4
  %cmp35 = icmp ne i32 %call34, %33
  br i1 %cmp35, label %if.then36, label %if.end38

if.then36:                                        ; preds = %if.then32
  %__sbuf_37 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* null, %"class.std::__2::basic_streambuf"** %__sbuf_37, align 4
  %34 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %35 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 4, i1 false)
  br label %return

if.end38:                                         ; preds = %if.then32
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.end27
  %36 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %__iob.addr, align 4
  %call40 = call i32 @_ZNSt3__28ios_base5widthEl(%"class.std::__2::ios_base"* %36, i32 0)
  %37 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %38 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end39, %if.then36, %cleanup, %if.then12, %if.then
  %coerce.dive41 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %retval, i32 0, i32 0
  %39 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %coerce.dive41, align 4
  ret %"class.std::__2::basic_streambuf"* %39

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::ostreambuf_iterator"* @_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE(%"class.std::__2::ostreambuf_iterator"* returned %this, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__s) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ostreambuf_iterator"*, align 4
  %__s.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  store %"class.std::__2::ostreambuf_iterator"* %this, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  store %"class.std::__2::basic_ostream"* %__s, %"class.std::__2::basic_ostream"** %__s.addr, align 4
  %this1 = load %"class.std::__2::ostreambuf_iterator"*, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::ostreambuf_iterator"* %this1 to %"struct.std::__2::iterator"*
  %__sbuf_ = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__s.addr, align 4
  %2 = bitcast %"class.std::__2::basic_ostream"* %1 to i8**
  %vtable = load i8*, i8** %2, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %3 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %3, align 4
  %4 = bitcast %"class.std::__2::basic_ostream"* %1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %vbase.offset
  %5 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  %call = call %"class.std::__2::basic_streambuf"* @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ios"* %5)
  store %"class.std::__2::basic_streambuf"* %call, %"class.std::__2::basic_streambuf"** %__sbuf_, align 4
  ret %"class.std::__2::ostreambuf_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__28ios_base5flagsEv(%"class.std::__2::ios_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__fmtflags_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__fmtflags_, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv(%"class.std::__2::basic_ios"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %call = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #12
  %__fill_ = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__fill_, align 4
  %call2 = call zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %call, i32 %0) #12
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc(%"class.std::__2::basic_ios"* %this1, i8 signext 32)
  %conv = sext i8 %call3 to i32
  %__fill_4 = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  store i32 %conv, i32* %__fill_4, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %__fill_5 = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  %1 = load i32, i32* %__fill_5, align 4
  %conv6 = trunc i32 %1 to i8
  ret i8 %conv6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv(%"class.std::__2::ostreambuf_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ostreambuf_iterator"*, align 4
  store %"class.std::__2::ostreambuf_iterator"* %this, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::ostreambuf_iterator"*, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  %__sbuf_ = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_, align 4
  %cmp = icmp eq %"class.std::__2::basic_streambuf"* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj(%"class.std::__2::basic_ios"* %this, i32 %__state) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__state.addr = alloca i32, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store i32 %__state, i32* %__state.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %1 = load i32, i32* %__state.addr, align 4
  call void @_ZNSt3__28ios_base8setstateEj(%"class.std::__2::ios_base"* %0, i32 %1)
  ret void
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* returned) unnamed_addr #5

; Function Attrs: argmemonly nofree nosync nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__28ios_base5widthEv(%"class.std::__2::ios_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__width_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 3
  %0 = load i32, i32* %__width_, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %this, i8* %__s, i32 %__n) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__s.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = bitcast %"class.std::__2::basic_streambuf"* %this1 to i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)***
  %vtable = load i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)**, i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)*, i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)** %vtable, i64 12
  %3 = load i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)*, i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)** %vfn, align 4
  %call = call i32 %3(%"class.std::__2::basic_streambuf"* %this1, i8* %0, i32 %1)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc(%"class.std::__2::basic_string"* returned %this, i32 %__n, i8 signext %__c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__n.addr = alloca i32, align 4
  %__c.addr = alloca i8, align 1
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load i8, i8* %__c.addr, align 1
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc(%"class.std::__2::basic_string"* %this1, i32 %1, i8 signext %2)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #12
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #12
  ret i8* %call2
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28ios_base5widthEl(%"class.std::__2::ios_base"* %this, i32 %__wide) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__wide.addr = alloca i32, align 4
  %__r = alloca i32, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  store i32 %__wide, i32* %__wide.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__width_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 3
  %0 = load i32, i32* %__width_, align 4
  store i32 %0, i32* %__r, align 4
  %1 = load i32, i32* %__wide.addr, align 4
  %__width_2 = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 3
  store i32 %1, i32* %__width_2, align 4
  %2 = load i32, i32* %__r, align 4
  ret i32 %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #12
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.2"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #12
  %call5 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc(%"class.std::__2::basic_string"*, i32, i8 signext) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.2"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::allocator.4"*
  %call = call %"class.std::__2::allocator.4"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.4"* %1) #12
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.4"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.4"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.4"*, align 4
  store %"class.std::__2::allocator.4"* %this, %"class.std::__2::allocator.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %this.addr, align 4
  ret %"class.std::__2::allocator.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #12
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #12
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %__r_) #12
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %__r_) #12
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %__r_) #12
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #12
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #12
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #0 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #12
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::basic_streambuf"* @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ios"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %call = call i8* @_ZNKSt3__28ios_base5rdbufEv(%"class.std::__2::ios_base"* %0)
  %1 = bitcast i8* %call to %"class.std::__2::basic_streambuf"*
  ret %"class.std::__2::basic_streambuf"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__28ios_base5rdbufEv(%"class.std::__2::ios_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__rdbuf_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 6
  %0 = load i8*, i8** %__rdbuf_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %__c1, i32 %__c2) #0 comdat {
entry:
  %__c1.addr = alloca i32, align 4
  %__c2.addr = alloca i32, align 4
  store i32 %__c1, i32* %__c1.addr, align 4
  store i32 %__c2, i32* %__c2.addr, align 4
  %0 = load i32, i32* %__c1.addr, align 4
  %1 = load i32, i32* %__c2.addr, align 4
  %cmp = icmp eq i32 %0, %1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE3eofEv() #0 comdat {
entry:
  ret i32 -1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc(%"class.std::__2::basic_ios"* %this, i8 signext %__c) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__c.addr = alloca i8, align 1
  %ref.tmp = alloca %"class.std::__2::locale", align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  call void @_ZNKSt3__28ios_base6getlocEv(%"class.std::__2::locale"* sret align 4 %ref.tmp, %"class.std::__2::ios_base"* %0)
  %call = call nonnull align 4 dereferenceable(13) %"class.std::__2::ctype"* @_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = load i8, i8* %__c.addr, align 1
  %call2 = call signext i8 @_ZNKSt3__25ctypeIcE5widenEc(%"class.std::__2::ctype"* %call, i8 signext %1)
  %call3 = call %"class.std::__2::locale"* @_ZNSt3__26localeD1Ev(%"class.std::__2::locale"* %ref.tmp) #12
  ret i8 %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(13) %"class.std::__2::ctype"* @_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %__l) #2 comdat {
entry:
  %__l.addr = alloca %"class.std::__2::locale"*, align 4
  store %"class.std::__2::locale"* %__l, %"class.std::__2::locale"** %__l.addr, align 4
  %0 = load %"class.std::__2::locale"*, %"class.std::__2::locale"** %__l.addr, align 4
  %call = call %"class.std::__2::locale::facet"* @_ZNKSt3__26locale9use_facetERNS0_2idE(%"class.std::__2::locale"* %0, %"class.std::__2::locale::id"* nonnull align 4 dereferenceable(8) @_ZNSt3__25ctypeIcE2idE)
  %1 = bitcast %"class.std::__2::locale::facet"* %call to %"class.std::__2::ctype"*
  ret %"class.std::__2::ctype"* %1
}

declare void @_ZNKSt3__28ios_base6getlocEv(%"class.std::__2::locale"* sret align 4, %"class.std::__2::ios_base"*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden signext i8 @_ZNKSt3__25ctypeIcE5widenEc(%"class.std::__2::ctype"* %this, i8 signext %__c) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ctype"*, align 4
  %__c.addr = alloca i8, align 1
  store %"class.std::__2::ctype"* %this, %"class.std::__2::ctype"** %this.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::ctype"*, %"class.std::__2::ctype"** %this.addr, align 4
  %0 = load i8, i8* %__c.addr, align 1
  %1 = bitcast %"class.std::__2::ctype"* %this1 to i8 (%"class.std::__2::ctype"*, i8)***
  %vtable = load i8 (%"class.std::__2::ctype"*, i8)**, i8 (%"class.std::__2::ctype"*, i8)*** %1, align 4
  %vfn = getelementptr inbounds i8 (%"class.std::__2::ctype"*, i8)*, i8 (%"class.std::__2::ctype"*, i8)** %vtable, i64 7
  %2 = load i8 (%"class.std::__2::ctype"*, i8)*, i8 (%"class.std::__2::ctype"*, i8)** %vfn, align 4
  %call = call signext i8 %2(%"class.std::__2::ctype"* %this1, i8 signext %0)
  ret i8 %call
}

; Function Attrs: nounwind
declare %"class.std::__2::locale"* @_ZNSt3__26localeD1Ev(%"class.std::__2::locale"* returned) unnamed_addr #5

declare %"class.std::__2::locale::facet"* @_ZNKSt3__26locale9use_facetERNS0_2idE(%"class.std::__2::locale"*, %"class.std::__2::locale::id"* nonnull align 4 dereferenceable(8)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__28ios_base8setstateEj(%"class.std::__2::ios_base"* %this, i32 %__state) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__state.addr = alloca i32, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  store i32 %__state, i32* %__state.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__rdstate_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 4
  %0 = load i32, i32* %__rdstate_, align 4
  %1 = load i32, i32* %__state.addr, align 4
  %or = or i32 %0, %1
  call void @_ZNSt3__28ios_base5clearEj(%"class.std::__2::ios_base"* %this1, i32 %or)
  ret void
}

declare void @_ZNSt3__28ios_base5clearEj(%"class.std::__2::ios_base"*, i32) #3

; Function Attrs: argmemonly nofree nounwind readonly
declare i32 @strlen(i8* nocapture) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #12
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #12
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #12
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* %1) #12
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #12
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #12
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %call8 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #12
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #12
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #12
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #12
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #12
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #12
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #12
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #12
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #12
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #12
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this1, i32* %0) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #12
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #12
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #12
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #2 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #2 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #2 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #13
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #12
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #12
  ret i32** %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_(%"class.std::__2::vector"* %this, i32* nonnull align 4 dereferenceable(4) %__args) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca i32*, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #12
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #12
  %2 = load i32*, i32** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %2) #12
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32* %call3, i32* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx) #12
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_(%"class.std::__2::vector"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca i32*, align 4
  %__a = alloca %"class.std::__2::allocator"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #12
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #12
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #12
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %3 = load i32*, i32** %__end_, align 4
  %call6 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %3) #12
  %4 = load i32*, i32** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %4) #12
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %2, i32* %call6, i32* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %5 = load i32*, i32** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__end_8, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #12
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #12
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #12
  call void @_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #12
  %4 = load i32, i32* %call, align 4
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #12
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #14
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #12
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.6"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.6"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #12
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #12
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #12
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #12
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #12
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #12
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #12
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #12
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #12
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #12
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #12
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #12
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #12
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #12
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #12
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #12
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #12
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #9

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #2 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #2 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #12
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #12
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #12
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #2 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %1) #12
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #12
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.6"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.6"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #12
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.7"*
  %5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %5) #12
  %call4 = call %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.7"* %4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.6"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #12
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #12
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.7"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) #12
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.7"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this1) #12
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.23, i32 0, i32 0)) #14
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline noreturn optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #10 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  %exception = call i8* @__cxa_allocate_exception(i32 8) #12
  %0 = bitcast i8* %exception to %"class.std::length_error"*
  %1 = load i8*, i8** %__msg.addr, align 4
  %call = call %"class.std::length_error"* @_ZNSt12length_errorC2EPKc(%"class.std::length_error"* %0, i8* %1)
  call void @__cxa_throw(i8* %exception, i8* bitcast (i8** @_ZTISt12length_error to i8*), i8* bitcast (%"class.std::length_error"* (%"class.std::length_error"*)* @_ZNSt12length_errorD1Ev to i8*)) #14
  unreachable
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #2 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #15
  ret i8* %call
}

declare i8* @__cxa_allocate_exception(i32)

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::length_error"* @_ZNSt12length_errorC2EPKc(%"class.std::length_error"* returned %this, i8* %__s) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::length_error"*, align 4
  %__s.addr = alloca i8*, align 4
  store %"class.std::length_error"* %this, %"class.std::length_error"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %this1 = load %"class.std::length_error"*, %"class.std::length_error"** %this.addr, align 4
  %0 = bitcast %"class.std::length_error"* %this1 to %"class.std::logic_error"*
  %1 = load i8*, i8** %__s.addr, align 4
  %call = call %"class.std::logic_error"* @_ZNSt11logic_errorC2EPKc(%"class.std::logic_error"* %0, i8* %1)
  %2 = bitcast %"class.std::length_error"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVSt12length_error, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  ret %"class.std::length_error"* %this1
}

; Function Attrs: nounwind
declare %"class.std::length_error"* @_ZNSt12length_errorD1Ev(%"class.std::length_error"* returned) unnamed_addr #5

declare void @__cxa_throw(i8*, i8*, i8*)

declare %"class.std::logic_error"* @_ZNSt11logic_errorC2EPKc(%"class.std::logic_error"* returned, i8*) unnamed_addr #3

; Function Attrs: nobuiltin nofree allocsize(0)
declare noalias noundef nonnull i8* @_Znwm(i32) #11

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %1) #12
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__value_, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #12
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #12
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #12
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #12
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #12
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #12
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #12
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #12
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.8", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.8", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #12
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #12
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #12
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #12
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj(%"class.std::__2::vector"* %this, i32* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPjEC2ES1_(%"class.std::__2::__wrap_iter"* %retval, i32* %0) #12
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %1 = load i32*, i32** %coerce.dive, align 4
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPjEC2ES1_(%"class.std::__2::__wrap_iter"* returned %this, i32* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__x.addr, align 4
  store i32* %0, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %1) #12
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__211__wrap_iterIPjEdeEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPjEppEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %0, i32 1
  store i32* %incdec.ptr, i32** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call i32* @_ZNKSt3__211__wrap_iterIPjE4baseEv(%"class.std::__2::__wrap_iter"* %0) #12
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call1 = call i32* @_ZNKSt3__211__wrap_iterIPjE4baseEv(%"class.std::__2::__wrap_iter"* %1) #12
  %cmp = icmp eq i32* %call, %call1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__211__wrap_iterIPjE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__i, align 4
  ret i32* %0
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline norecurse optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nofree nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nofree nosync nounwind willreturn }
attributes #7 = { argmemonly nofree nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { noinline noreturn optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nobuiltin nofree allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #12 = { nounwind }
attributes #13 = { builtin nounwind }
attributes #14 = { noreturn }
attributes #15 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
