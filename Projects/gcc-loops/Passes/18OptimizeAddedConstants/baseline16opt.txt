[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    2.5387e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000565095 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    3.1666e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00236831 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00291625 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000183292 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.013237 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0161903 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00230543 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0087431 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00308255 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0117325 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0613808 seconds.
[PassRunner] (final validation)
