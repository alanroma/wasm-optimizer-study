[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    2.9943e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000588491 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    3.1297e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00235532 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00294905 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000236878 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.00619098 seconds.
[PassRunner] (final validation)
