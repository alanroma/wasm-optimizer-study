[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        2.6512e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000674173 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        2.9607e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.00250367 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.00308878 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     0.00027302 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.0334252 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.0138276 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0161614 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.0021516 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.00900373 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.00298408 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.062405 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.0452582 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.00439761 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.0325582 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0166059 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00479126 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0107146 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.0158763 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0168735 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.0341707 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0153125 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00375014 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0156635 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00369148 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0147901 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                       0.00430399 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.0086626 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0125418 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.00202246 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.00777944 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.416319 seconds.
[PassRunner] (final validation)
