[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        2.7424e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000555947 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        3.1873e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.00239224 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.00296627 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     0.000263797 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.0328776 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.0133327 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0160114 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.00233655 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.00899601 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.00309799 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.060573 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.045386 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.00446095 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.0322714 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0165162 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00485808 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0107664 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.257722 seconds.
[PassRunner] (final validation)
