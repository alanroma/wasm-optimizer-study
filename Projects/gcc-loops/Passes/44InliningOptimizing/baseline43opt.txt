[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    2.7162e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000577261 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    3.2608e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00237191 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0029891 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000213616 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0133927 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0163203 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00221442 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00869438 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00301345 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.011947 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00182537 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0041505 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0295186 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0162882 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00421902 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0105849 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0157256 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0330556 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0150848 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0038214 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0154843 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00382624 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.014796 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00843346 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0121526 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0021371 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00778073 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0108561 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00792094 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0111601 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0143194 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0650231 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.369958 seconds.
[PassRunner] (final validation)
