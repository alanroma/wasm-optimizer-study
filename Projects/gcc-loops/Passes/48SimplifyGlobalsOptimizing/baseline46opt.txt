[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    2.6131e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000581532 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    3.0824e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00249751 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00293001 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000229213 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0133143 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0163951 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00221809 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00881876 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00312574 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0118468 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00210731 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00418788 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0290038 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0162667 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00445309 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0108572 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0158095 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0330575 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.015207 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00384456 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.015589 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00380117 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0148225 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00863952 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0125254 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00184815 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00767009 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0110619 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00788441 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0109683 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0137387 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0645182 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0550627 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00205379 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   2.0874e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.427013 seconds.
[PassRunner] (final validation)
