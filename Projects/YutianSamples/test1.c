#include <stdio.h>

int main() {
    printf("Sum V1\n");
    int sum = 0;
    for (int i = 0; i < 100; ++i)
    {
        for (int j = 0; j < 100; ++j)
        {
            sum += (i * 100 + j);
        }
    }
    sum += 10000;
    printf("%d\n", sum);
    return 0;
}
