[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    5.122e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000407269 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.71e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000442906 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00159261 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.1388e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00498517 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00519705 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000528316 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00239277 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00103992 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00257401 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000500463 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00141137 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0111333 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00517069 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00227199 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00201562 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00182351 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00551644 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00305695 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00062974 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00168551 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000614064 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00298289 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00194453 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00369278 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000354291 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00181314 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0658115 seconds.
[PassRunner] (final validation)
