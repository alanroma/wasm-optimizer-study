[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00265681 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.2382e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.0165734 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0047225 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00513868 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000499394 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00242959 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00103313 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.0411541 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00142432 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.265836 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00501389 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00256459 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00201998 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                   0.0021649 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00231278 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00553751 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00333278 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000952001 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00194844 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000915877 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00307651 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.000771896 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00195592 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00368057 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000332457 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00182918 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.00754902 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00167481 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00122957 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00292191 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.007032 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0266939 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000613086 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   6.464e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000892647 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.000352108 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      1.947e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.000295373 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.00757498 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.436737 seconds.
[PassRunner] (final validation)
