[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    5.086e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000629326 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.373e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000442542 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00177224 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.0664e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00487282 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00518143 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000513309 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00239283 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00102313 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0025352 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000489815 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00139631 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0110802 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0053805 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00227681 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00198855 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0018038 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00554728 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00307472 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000626164 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00168641 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000628366 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00303527 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00194599 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00369391 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00033256 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0018447 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00156212 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00163231 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00118076 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00287068 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.00496365 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0179911 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000748361 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   6.7e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000916564 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0980995 seconds.
[PassRunner] (final validation)
