[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00229425 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 1.5992e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.0130469 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00375918 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00419982 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000421825 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0020998 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00085265 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.0334105 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00106698 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.201183 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00449318 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0033073 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00185471 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                   0.00204026 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00196662 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00482633 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00276134 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00059434 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00158189 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000712805 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00263327 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.000771089 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00176253 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00299083 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000290139 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0016523 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.00658266 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00145801 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00107067 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00253467 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.00571291 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0252851 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000722023 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   6.467e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000768881 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.000303039 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      1.78e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.000262111 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.00677057 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.348069 seconds.
[PassRunner] (final validation)
