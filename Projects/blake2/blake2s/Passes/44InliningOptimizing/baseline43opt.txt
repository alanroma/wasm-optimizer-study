[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    4.985e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000330731 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    6.659e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000370112 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00127004 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.3206e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00383799 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00419314 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000421434 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00205358 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000849868 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0021327 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000427406 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00103622 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.00922311 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00433292 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00310231 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00175844 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0016583 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0047909 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00266676 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000536297 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0015931 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000534016 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00257639 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00173602 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00297752 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000291518 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0016332 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00134159 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00143749 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00109625 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00248341 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.00400755 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0667352 seconds.
[PassRunner] (final validation)
