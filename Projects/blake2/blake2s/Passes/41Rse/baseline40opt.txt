[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    4.833e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000404153 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.256e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000371761 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0012143 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 1.695e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00385849 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00423412 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000422369 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00204186 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000879428 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0021345 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000421011 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00104492 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.00925699 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00438501 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00311122 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00175664 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0016219 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00488051 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00259061 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000538805 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00157011 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000539579 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00255007 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00176755 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00298464 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000292694 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00165716 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00136229 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00143123 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0593529 seconds.
[PassRunner] (final validation)
