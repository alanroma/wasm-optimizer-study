[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    4.821e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000332573 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    6.688e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000373003 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00127651 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 1.7741e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0037898 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00421233 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000442324 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00206805 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000867138 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00212778 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000422965 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00104928 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.00944542 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00427877 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00310913 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0017678 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00161735 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00505344 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00282926 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000727218 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00176551 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00072587 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00280178 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00199522 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0031613 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000425146 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00165169 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00133942 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00143677 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00107878 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00246702 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0646679 seconds.
[PassRunner] (final validation)
