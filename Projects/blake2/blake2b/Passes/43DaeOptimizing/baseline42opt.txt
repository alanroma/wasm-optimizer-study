[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    4.874e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000496721 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.325e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000409134 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0013953 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.095e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00461884 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00467829 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00046296 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00223869 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000955198 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00238099 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000511032 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00114116 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0105961 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00503015 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00213052 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00187701 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00168927 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00514355 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00286199 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000586 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00159111 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000582006 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00280298 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00186705 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00329906 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000317654 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00178468 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00146134 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00153996 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00110231 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00270668 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0682909 seconds.
[PassRunner] (final validation)
