[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    4.729e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000457889 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    8.025e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000417009 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00156776 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.4663e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00451109 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00468055 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00047039 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00223281 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000967607 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0153425 seconds.
[PassRunner] (final validation)
