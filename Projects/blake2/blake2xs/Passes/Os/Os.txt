[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.0022892 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 6.7828e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.0134345 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0039505 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00438075 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000466735 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0021579 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000895416 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00217493 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00119768 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0096053 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00443504 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00321384 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00182395 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00196486 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00499007 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00273435 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000561709 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00158498 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000557636 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00265563 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.000721714 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00181881 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00307288 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000306809 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00167999 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00136322 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00149102 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00110446 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0025604 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.00469474 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0210201 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000558659 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   7.095e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000789985 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00031568 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      1.945e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.000262342 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.00688556 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.113798 seconds.
[PassRunner] (final validation)
