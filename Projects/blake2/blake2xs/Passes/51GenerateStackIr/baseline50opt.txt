[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    4.985e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000547611 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    8.099e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000385481 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00144451 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 6.751e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00400433 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0043754 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000430896 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00212927 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000893163 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00219215 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000429827 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0011592 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.00948843 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00440933 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00333904 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00184698 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00165683 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00490787 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00265884 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000556877 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00159449 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00075273 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00265179 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00178437 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00305275 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000294934 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00165428 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00136278 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00146898 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00115181 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00253667 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0041507 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.017705 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000622661 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   6.824e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000844738 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.000337161 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      1.862e-06 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0889111 seconds.
[PassRunner] (final validation)
