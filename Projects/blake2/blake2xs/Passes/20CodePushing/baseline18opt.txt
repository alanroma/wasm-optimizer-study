[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    5.527e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000348145 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.2e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000382891 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00136436 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 6.8306e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00399427 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00434559 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000437809 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00209595 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000889952 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0021847 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000455956 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0165807 seconds.
[PassRunner] (final validation)
