[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        5.37e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000403243 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        7.022e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.000401284 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.00127444 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     7.0254e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.0135327 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.00388559 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.00440759 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.0004339 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.00213992 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.000877666 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.0342845 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.0182783 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.00112927 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.194079 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.00428172 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00326342 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.00188149 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.00224061 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.00207997 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.00484467 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.00291496 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000563578 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.00162303 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000545859 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.00269526 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.302145 seconds.
[PassRunner] (final validation)
