; ModuleID = 'blake2b-ref.c'
source_filename = "blake2b-ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.blake2b_state__ = type { [8 x i64], [2 x i64], [2 x i64], [128 x i8], i32, i32, i8 }
%struct.blake2b_param__ = type { i8, i8, i8, i8, i32, i32, i32, i8, i8, [14 x i8], [16 x i8], [16 x i8] }

@blake2b_IV = internal constant [8 x i64] [i64 7640891576956012808, i64 -4942790177534073029, i64 4354685564936845355, i64 -6534734903238641935, i64 5840696475078001361, i64 -7276294671716946913, i64 2270897969802886507, i64 6620516959819538809], align 16
@secure_zero_memory.memset_v = internal constant i8* (i8*, i32, i32)* @memset, align 4
@blake2b_sigma = internal constant [12 x [16 x i8]] [[16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F", [16 x i8] c"\0E\0A\04\08\09\0F\0D\06\01\0C\00\02\0B\07\05\03", [16 x i8] c"\0B\08\0C\00\05\02\0F\0D\0A\0E\03\06\07\01\09\04", [16 x i8] c"\07\09\03\01\0D\0C\0B\0E\02\06\05\0A\04\00\0F\08", [16 x i8] c"\09\00\05\07\02\04\0A\0F\0E\01\0B\0C\06\08\03\0D", [16 x i8] c"\02\0C\06\0A\00\0B\08\03\04\0D\07\05\0F\0E\01\09", [16 x i8] c"\0C\05\01\0F\0E\0D\04\0A\00\07\06\03\09\02\08\0B", [16 x i8] c"\0D\0B\07\0E\0C\01\03\09\05\00\0F\04\08\06\02\0A", [16 x i8] c"\06\0F\0E\09\0B\03\00\08\0C\02\0D\07\01\04\0A\05", [16 x i8] c"\0A\02\08\04\07\06\01\05\0F\0B\09\0E\03\0C\0D\00", [16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F", [16 x i8] c"\0E\0A\04\08\09\0F\0D\06\01\0C\00\02\0B\07\05\03"], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init_param(%struct.blake2b_state__* %S, %struct.blake2b_param__* %P) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %P.addr = alloca %struct.blake2b_param__*, align 4
  %p = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  store %struct.blake2b_param__* %P, %struct.blake2b_param__** %P.addr, align 4
  %0 = load %struct.blake2b_param__*, %struct.blake2b_param__** %P.addr, align 4
  %1 = bitcast %struct.blake2b_param__* %0 to i8*
  store i8* %1, i8** %p, align 4
  %2 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  call void @blake2b_init0(%struct.blake2b_state__* %2)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %3, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %p, align 4
  %5 = load i32, i32* %i, align 4
  %mul = mul i32 8, %5
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %mul
  %call = call i64 @load64(i8* %add.ptr)
  %6 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %6, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 %7
  %8 = load i64, i64* %arrayidx, align 8
  %xor = xor i64 %8, %call
  store i64 %xor, i64* %arrayidx, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load %struct.blake2b_param__*, %struct.blake2b_param__** %P.addr, align 4
  %digest_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %10, i32 0, i32 0
  %11 = load i8, i8* %digest_length, align 1
  %conv = zext i8 %11 to i32
  %12 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %outlen = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %12, i32 0, i32 5
  store i32 %conv, i32* %outlen, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2b_init0(%struct.blake2b_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %i = alloca i32, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  %0 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %1 = bitcast %struct.blake2b_state__* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 0, i32 240, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 %3
  %4 = load i64, i64* %arrayidx, align 8
  %5 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 %6
  store i64 %4, i64* %arrayidx1, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i64 @load64(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %p = alloca i8*, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = load i8*, i8** %src.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i8*, i8** %p, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 0
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i64
  %shl = shl i64 %conv, 0
  %3 = load i8*, i8** %p, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i64
  %shl3 = shl i64 %conv2, 8
  %or = or i64 %shl, %shl3
  %5 = load i8*, i8** %p, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %5, i32 2
  %6 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %6 to i64
  %shl6 = shl i64 %conv5, 16
  %or7 = or i64 %or, %shl6
  %7 = load i8*, i8** %p, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 3
  %8 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %8 to i64
  %shl10 = shl i64 %conv9, 24
  %or11 = or i64 %or7, %shl10
  %9 = load i8*, i8** %p, align 4
  %arrayidx12 = getelementptr inbounds i8, i8* %9, i32 4
  %10 = load i8, i8* %arrayidx12, align 1
  %conv13 = zext i8 %10 to i64
  %shl14 = shl i64 %conv13, 32
  %or15 = or i64 %or11, %shl14
  %11 = load i8*, i8** %p, align 4
  %arrayidx16 = getelementptr inbounds i8, i8* %11, i32 5
  %12 = load i8, i8* %arrayidx16, align 1
  %conv17 = zext i8 %12 to i64
  %shl18 = shl i64 %conv17, 40
  %or19 = or i64 %or15, %shl18
  %13 = load i8*, i8** %p, align 4
  %arrayidx20 = getelementptr inbounds i8, i8* %13, i32 6
  %14 = load i8, i8* %arrayidx20, align 1
  %conv21 = zext i8 %14 to i64
  %shl22 = shl i64 %conv21, 48
  %or23 = or i64 %or19, %shl22
  %15 = load i8*, i8** %p, align 4
  %arrayidx24 = getelementptr inbounds i8, i8* %15, i32 7
  %16 = load i8, i8* %arrayidx24, align 1
  %conv25 = zext i8 %16 to i64
  %shl26 = shl i64 %conv25, 56
  %or27 = or i64 %or23, %shl26
  ret i64 %or27
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init(%struct.blake2b_state__* %S, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %P = alloca [1 x %struct.blake2b_param__], align 16
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ugt i32 %1, 64
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %2 to i8
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 16
  %arraydecay1 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay1, i32 0, i32 1
  store i8 0, i8* %key_length, align 1
  %arraydecay2 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay2, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay3 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay3, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay4 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay4, i32 0, i32 4
  %3 = bitcast i32* %leaf_length to i8*
  call void @store32(i8* %3, i32 0)
  %arraydecay5 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay5, i32 0, i32 5
  %4 = bitcast i32* %node_offset to i8*
  call void @store32(i8* %4, i32 0)
  %arraydecay6 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %xof_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay6, i32 0, i32 6
  %5 = bitcast i32* %xof_length to i8*
  call void @store32(i8* %5, i32 0)
  %arraydecay7 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay7, i32 0, i32 7
  store i8 0, i8* %node_depth, align 16
  %arraydecay8 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay8, i32 0, i32 8
  store i8 0, i8* %inner_length, align 1
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %reserved = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay9, i32 0, i32 9
  %arraydecay10 = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 2 %arraydecay10, i8 0, i32 14, i1 false)
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay11, i32 0, i32 10
  %arraydecay12 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay12, i8 0, i32 16, i1 false)
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay13, i32 0, i32 11
  %arraydecay14 = getelementptr inbounds [16 x i8], [16 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay14, i8 0, i32 16, i1 false)
  %6 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %arraydecay15 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %call = call i32 @blake2b_init_param(%struct.blake2b_state__* %6, %struct.blake2b_param__* %arraydecay15)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define internal void @store32(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i32, i32* %w.addr, align 4
  %shr = lshr i32 %1, 0
  %conv = trunc i32 %shr to i8
  %2 = load i8*, i8** %p, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 0
  store i8 %conv, i8* %arrayidx, align 1
  %3 = load i32, i32* %w.addr, align 4
  %shr1 = lshr i32 %3, 8
  %conv2 = trunc i32 %shr1 to i8
  %4 = load i8*, i8** %p, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %4, i32 1
  store i8 %conv2, i8* %arrayidx3, align 1
  %5 = load i32, i32* %w.addr, align 4
  %shr4 = lshr i32 %5, 16
  %conv5 = trunc i32 %shr4 to i8
  %6 = load i8*, i8** %p, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %6, i32 2
  store i8 %conv5, i8* %arrayidx6, align 1
  %7 = load i32, i32* %w.addr, align 4
  %shr7 = lshr i32 %7, 24
  %conv8 = trunc i32 %shr7 to i8
  %8 = load i8*, i8** %p, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %8, i32 3
  store i8 %conv8, i8* %arrayidx9, align 1
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init_key(%struct.blake2b_state__* %S, i32 %outlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %P = alloca [1 x %struct.blake2b_param__], align 16
  %block = alloca [128 x i8], align 16
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ugt i32 %1, 64
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8*, i8** %key.addr, align 4
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then6

lor.lhs.false2:                                   ; preds = %if.end
  %3 = load i32, i32* %keylen.addr, align 4
  %tobool3 = icmp ne i32 %3, 0
  br i1 %tobool3, label %lor.lhs.false4, label %if.then6

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %4 = load i32, i32* %keylen.addr, align 4
  %cmp5 = icmp ugt i32 %4, 64
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %lor.lhs.false4, %lor.lhs.false2, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %lor.lhs.false4
  %5 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %5 to i8
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 16
  %6 = load i32, i32* %keylen.addr, align 4
  %conv8 = trunc i32 %6 to i8
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay9, i32 0, i32 1
  store i8 %conv8, i8* %key_length, align 1
  %arraydecay10 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay10, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay11, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay12 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay12, i32 0, i32 4
  %7 = bitcast i32* %leaf_length to i8*
  call void @store32(i8* %7, i32 0)
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay13, i32 0, i32 5
  %8 = bitcast i32* %node_offset to i8*
  call void @store32(i8* %8, i32 0)
  %arraydecay14 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %xof_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay14, i32 0, i32 6
  %9 = bitcast i32* %xof_length to i8*
  call void @store32(i8* %9, i32 0)
  %arraydecay15 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay15, i32 0, i32 7
  store i8 0, i8* %node_depth, align 16
  %arraydecay16 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay16, i32 0, i32 8
  store i8 0, i8* %inner_length, align 1
  %arraydecay17 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %reserved = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay17, i32 0, i32 9
  %arraydecay18 = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 2 %arraydecay18, i8 0, i32 14, i1 false)
  %arraydecay19 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay19, i32 0, i32 10
  %arraydecay20 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay20, i8 0, i32 16, i1 false)
  %arraydecay21 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2b_param__, %struct.blake2b_param__* %arraydecay21, i32 0, i32 11
  %arraydecay22 = getelementptr inbounds [16 x i8], [16 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay22, i8 0, i32 16, i1 false)
  %10 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %arraydecay23 = getelementptr inbounds [1 x %struct.blake2b_param__], [1 x %struct.blake2b_param__]* %P, i32 0, i32 0
  %call = call i32 @blake2b_init_param(%struct.blake2b_state__* %10, %struct.blake2b_param__* %arraydecay23)
  %cmp24 = icmp slt i32 %call, 0
  br i1 %cmp24, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.end7
  store i32 -1, i32* %retval, align 4
  br label %return

if.end27:                                         ; preds = %if.end7
  %arraydecay28 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay28, i8 0, i32 128, i1 false)
  %arraydecay29 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %11 = load i8*, i8** %key.addr, align 4
  %12 = load i32, i32* %keylen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay29, i8* align 1 %11, i32 %12, i1 false)
  %13 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %arraydecay30 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %call31 = call i32 @blake2b_update(%struct.blake2b_state__* %13, i8* %arraydecay30, i32 128)
  %arraydecay32 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @secure_zero_memory(i8* %arraydecay32, i32 128)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end27, %if.then26, %if.then6, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_update(%struct.blake2b_state__* %S, i8* %pin, i32 %inlen) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %pin.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %in = alloca i8*, align 4
  %left = alloca i32, align 4
  %fill = alloca i32, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  store i8* %pin, i8** %pin.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  %0 = load i8*, i8** %pin.addr, align 4
  store i8* %0, i8** %in, align 4
  %1 = load i32, i32* %inlen.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %2 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %2, i32 0, i32 4
  %3 = load i32, i32* %buflen, align 8
  store i32 %3, i32* %left, align 4
  %4 = load i32, i32* %left, align 4
  %sub = sub i32 128, %4
  store i32 %sub, i32* %fill, align 4
  %5 = load i32, i32* %inlen.addr, align 4
  %6 = load i32, i32* %fill, align 4
  %cmp1 = icmp ugt i32 %5, %6
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %7 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buflen3 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %7, i32 0, i32 4
  store i32 0, i32* %buflen3, align 8
  %8 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %8, i32 0, i32 3
  %arraydecay = getelementptr inbounds [128 x i8], [128 x i8]* %buf, i32 0, i32 0
  %9 = load i32, i32* %left, align 4
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 %9
  %10 = load i8*, i8** %in, align 4
  %11 = load i32, i32* %fill, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %10, i32 %11, i1 false)
  %12 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  call void @blake2b_increment_counter(%struct.blake2b_state__* %12, i64 128)
  %13 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %14 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buf4 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %14, i32 0, i32 3
  %arraydecay5 = getelementptr inbounds [128 x i8], [128 x i8]* %buf4, i32 0, i32 0
  call void @blake2b_compress(%struct.blake2b_state__* %13, i8* %arraydecay5)
  %15 = load i32, i32* %fill, align 4
  %16 = load i8*, i8** %in, align 4
  %add.ptr6 = getelementptr inbounds i8, i8* %16, i32 %15
  store i8* %add.ptr6, i8** %in, align 4
  %17 = load i32, i32* %fill, align 4
  %18 = load i32, i32* %inlen.addr, align 4
  %sub7 = sub i32 %18, %17
  store i32 %sub7, i32* %inlen.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then2
  %19 = load i32, i32* %inlen.addr, align 4
  %cmp8 = icmp ugt i32 %19, 128
  br i1 %cmp8, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %20 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  call void @blake2b_increment_counter(%struct.blake2b_state__* %20, i64 128)
  %21 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %22 = load i8*, i8** %in, align 4
  call void @blake2b_compress(%struct.blake2b_state__* %21, i8* %22)
  %23 = load i8*, i8** %in, align 4
  %add.ptr9 = getelementptr inbounds i8, i8* %23, i32 128
  store i8* %add.ptr9, i8** %in, align 4
  %24 = load i32, i32* %inlen.addr, align 4
  %sub10 = sub i32 %24, 128
  store i32 %sub10, i32* %inlen.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end

if.end:                                           ; preds = %while.end, %if.then
  %25 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buf11 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %25, i32 0, i32 3
  %arraydecay12 = getelementptr inbounds [128 x i8], [128 x i8]* %buf11, i32 0, i32 0
  %26 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buflen13 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %26, i32 0, i32 4
  %27 = load i32, i32* %buflen13, align 8
  %add.ptr14 = getelementptr inbounds i8, i8* %arraydecay12, i32 %27
  %28 = load i8*, i8** %in, align 4
  %29 = load i32, i32* %inlen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr14, i8* align 1 %28, i32 %29, i1 false)
  %30 = load i32, i32* %inlen.addr, align 4
  %31 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buflen15 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %31, i32 0, i32 4
  %32 = load i32, i32* %buflen15, align 8
  %add = add i32 %32, %30
  store i32 %add, i32* %buflen15, align 8
  br label %if.end16

if.end16:                                         ; preds = %if.end, %entry
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @secure_zero_memory(i8* %v, i32 %n) #0 {
entry:
  %v.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  store i8* %v, i8** %v.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load volatile i8* (i8*, i32, i32)*, i8* (i8*, i32, i32)** @secure_zero_memory.memset_v, align 4
  %1 = load i8*, i8** %v.addr, align 4
  %2 = load i32, i32* %n.addr, align 4
  %call = call i8* %0(i8* %1, i32 0, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2b_increment_counter(%struct.blake2b_state__* %S, i64 %inc) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %inc.addr = alloca i64, align 8
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  store i64 %inc, i64* %inc.addr, align 8
  %0 = load i64, i64* %inc.addr, align 8
  %1 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %t = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %t, i32 0, i32 0
  %2 = load i64, i64* %arrayidx, align 8
  %add = add i64 %2, %0
  store i64 %add, i64* %arrayidx, align 8
  %3 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %t1 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %3, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [2 x i64], [2 x i64]* %t1, i32 0, i32 0
  %4 = load i64, i64* %arrayidx2, align 8
  %5 = load i64, i64* %inc.addr, align 8
  %cmp = icmp ult i64 %4, %5
  %conv = zext i1 %cmp to i32
  %conv3 = sext i32 %conv to i64
  %6 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %t4 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %6, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [2 x i64], [2 x i64]* %t4, i32 0, i32 1
  %7 = load i64, i64* %arrayidx5, align 8
  %add6 = add i64 %7, %conv3
  store i64 %add6, i64* %arrayidx5, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2b_compress(%struct.blake2b_state__* %S, i8* %block) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %block.addr = alloca i8*, align 4
  %m = alloca [16 x i64], align 16
  %v = alloca [16 x i64], align 16
  %i = alloca i32, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  store i8* %block, i8** %block.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %block.addr, align 4
  %2 = load i32, i32* %i, align 4
  %mul = mul i32 %2, 8
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  %call = call i64 @load64(i8* %add.ptr)
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %3
  store i64 %call, i64* %arrayidx, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc6, %for.end
  %5 = load i32, i32* %i, align 4
  %cmp2 = icmp ult i32 %5, 8
  br i1 %cmp2, label %for.body3, label %for.end8

for.body3:                                        ; preds = %for.cond1
  %6 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %6, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 %7
  %8 = load i64, i64* %arrayidx4, align 8
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 %9
  store i64 %8, i64* %arrayidx5, align 8
  br label %for.inc6

for.inc6:                                         ; preds = %for.body3
  %10 = load i32, i32* %i, align 4
  %inc7 = add i32 %10, 1
  store i32 %inc7, i32* %i, align 4
  br label %for.cond1

for.end8:                                         ; preds = %for.cond1
  %11 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 0), align 16
  %arrayidx9 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %11, i64* %arrayidx9, align 16
  %12 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 1), align 8
  %arrayidx10 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %12, i64* %arrayidx10, align 8
  %13 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 2), align 16
  %arrayidx11 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %13, i64* %arrayidx11, align 16
  %14 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 3), align 8
  %arrayidx12 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %14, i64* %arrayidx12, align 8
  %15 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 4), align 16
  %16 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %t = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %16, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x i64], [2 x i64]* %t, i32 0, i32 0
  %17 = load i64, i64* %arrayidx13, align 8
  %xor = xor i64 %15, %17
  %arrayidx14 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %xor, i64* %arrayidx14, align 16
  %18 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 5), align 8
  %19 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %t15 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %19, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [2 x i64], [2 x i64]* %t15, i32 0, i32 1
  %20 = load i64, i64* %arrayidx16, align 8
  %xor17 = xor i64 %18, %20
  %arrayidx18 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %xor17, i64* %arrayidx18, align 8
  %21 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 6), align 16
  %22 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %22, i32 0, i32 2
  %arrayidx19 = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 0
  %23 = load i64, i64* %arrayidx19, align 8
  %xor20 = xor i64 %21, %23
  %arrayidx21 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %xor20, i64* %arrayidx21, align 16
  %24 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 7), align 8
  %25 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %f22 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %25, i32 0, i32 2
  %arrayidx23 = getelementptr inbounds [2 x i64], [2 x i64]* %f22, i32 0, i32 1
  %26 = load i64, i64* %arrayidx23, align 8
  %xor24 = xor i64 %24, %26
  %arrayidx25 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %xor24, i64* %arrayidx25, align 8
  br label %do.body

do.body:                                          ; preds = %for.end8
  br label %do.body26

do.body26:                                        ; preds = %do.body
  %arrayidx27 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %27 = load i64, i64* %arrayidx27, align 16
  %arrayidx28 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %28 = load i64, i64* %arrayidx28, align 16
  %add = add i64 %27, %28
  %29 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 0), align 16
  %idxprom = zext i8 %29 to i32
  %arrayidx29 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom
  %30 = load i64, i64* %arrayidx29, align 8
  %add30 = add i64 %add, %30
  %arrayidx31 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add30, i64* %arrayidx31, align 16
  %arrayidx32 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %31 = load i64, i64* %arrayidx32, align 16
  %arrayidx33 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %32 = load i64, i64* %arrayidx33, align 16
  %xor34 = xor i64 %31, %32
  %call35 = call i64 @rotr64(i64 %xor34, i32 32)
  %arrayidx36 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call35, i64* %arrayidx36, align 16
  %arrayidx37 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %33 = load i64, i64* %arrayidx37, align 16
  %arrayidx38 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %34 = load i64, i64* %arrayidx38, align 16
  %add39 = add i64 %33, %34
  %arrayidx40 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add39, i64* %arrayidx40, align 16
  %arrayidx41 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %35 = load i64, i64* %arrayidx41, align 16
  %arrayidx42 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %36 = load i64, i64* %arrayidx42, align 16
  %xor43 = xor i64 %35, %36
  %call44 = call i64 @rotr64(i64 %xor43, i32 24)
  %arrayidx45 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call44, i64* %arrayidx45, align 16
  %arrayidx46 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %37 = load i64, i64* %arrayidx46, align 16
  %arrayidx47 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %38 = load i64, i64* %arrayidx47, align 16
  %add48 = add i64 %37, %38
  %39 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 1), align 1
  %idxprom49 = zext i8 %39 to i32
  %arrayidx50 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom49
  %40 = load i64, i64* %arrayidx50, align 8
  %add51 = add i64 %add48, %40
  %arrayidx52 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add51, i64* %arrayidx52, align 16
  %arrayidx53 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %41 = load i64, i64* %arrayidx53, align 16
  %arrayidx54 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %42 = load i64, i64* %arrayidx54, align 16
  %xor55 = xor i64 %41, %42
  %call56 = call i64 @rotr64(i64 %xor55, i32 16)
  %arrayidx57 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call56, i64* %arrayidx57, align 16
  %arrayidx58 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %43 = load i64, i64* %arrayidx58, align 16
  %arrayidx59 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %44 = load i64, i64* %arrayidx59, align 16
  %add60 = add i64 %43, %44
  %arrayidx61 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add60, i64* %arrayidx61, align 16
  %arrayidx62 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %45 = load i64, i64* %arrayidx62, align 16
  %arrayidx63 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %46 = load i64, i64* %arrayidx63, align 16
  %xor64 = xor i64 %45, %46
  %call65 = call i64 @rotr64(i64 %xor64, i32 63)
  %arrayidx66 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call65, i64* %arrayidx66, align 16
  br label %do.end

do.end:                                           ; preds = %do.body26
  br label %do.body67

do.body67:                                        ; preds = %do.end
  %arrayidx68 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %47 = load i64, i64* %arrayidx68, align 8
  %arrayidx69 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %48 = load i64, i64* %arrayidx69, align 8
  %add70 = add i64 %47, %48
  %49 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 2), align 2
  %idxprom71 = zext i8 %49 to i32
  %arrayidx72 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom71
  %50 = load i64, i64* %arrayidx72, align 8
  %add73 = add i64 %add70, %50
  %arrayidx74 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add73, i64* %arrayidx74, align 8
  %arrayidx75 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %51 = load i64, i64* %arrayidx75, align 8
  %arrayidx76 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %52 = load i64, i64* %arrayidx76, align 8
  %xor77 = xor i64 %51, %52
  %call78 = call i64 @rotr64(i64 %xor77, i32 32)
  %arrayidx79 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call78, i64* %arrayidx79, align 8
  %arrayidx80 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %53 = load i64, i64* %arrayidx80, align 8
  %arrayidx81 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %54 = load i64, i64* %arrayidx81, align 8
  %add82 = add i64 %53, %54
  %arrayidx83 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add82, i64* %arrayidx83, align 8
  %arrayidx84 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %55 = load i64, i64* %arrayidx84, align 8
  %arrayidx85 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %56 = load i64, i64* %arrayidx85, align 8
  %xor86 = xor i64 %55, %56
  %call87 = call i64 @rotr64(i64 %xor86, i32 24)
  %arrayidx88 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call87, i64* %arrayidx88, align 8
  %arrayidx89 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %57 = load i64, i64* %arrayidx89, align 8
  %arrayidx90 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %58 = load i64, i64* %arrayidx90, align 8
  %add91 = add i64 %57, %58
  %59 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 3), align 1
  %idxprom92 = zext i8 %59 to i32
  %arrayidx93 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom92
  %60 = load i64, i64* %arrayidx93, align 8
  %add94 = add i64 %add91, %60
  %arrayidx95 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add94, i64* %arrayidx95, align 8
  %arrayidx96 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %61 = load i64, i64* %arrayidx96, align 8
  %arrayidx97 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %62 = load i64, i64* %arrayidx97, align 8
  %xor98 = xor i64 %61, %62
  %call99 = call i64 @rotr64(i64 %xor98, i32 16)
  %arrayidx100 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call99, i64* %arrayidx100, align 8
  %arrayidx101 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %63 = load i64, i64* %arrayidx101, align 8
  %arrayidx102 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %64 = load i64, i64* %arrayidx102, align 8
  %add103 = add i64 %63, %64
  %arrayidx104 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add103, i64* %arrayidx104, align 8
  %arrayidx105 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %65 = load i64, i64* %arrayidx105, align 8
  %arrayidx106 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %66 = load i64, i64* %arrayidx106, align 8
  %xor107 = xor i64 %65, %66
  %call108 = call i64 @rotr64(i64 %xor107, i32 63)
  %arrayidx109 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call108, i64* %arrayidx109, align 8
  br label %do.end110

do.end110:                                        ; preds = %do.body67
  br label %do.body111

do.body111:                                       ; preds = %do.end110
  %arrayidx112 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %67 = load i64, i64* %arrayidx112, align 16
  %arrayidx113 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %68 = load i64, i64* %arrayidx113, align 16
  %add114 = add i64 %67, %68
  %69 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 4), align 4
  %idxprom115 = zext i8 %69 to i32
  %arrayidx116 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom115
  %70 = load i64, i64* %arrayidx116, align 8
  %add117 = add i64 %add114, %70
  %arrayidx118 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add117, i64* %arrayidx118, align 16
  %arrayidx119 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %71 = load i64, i64* %arrayidx119, align 16
  %arrayidx120 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %72 = load i64, i64* %arrayidx120, align 16
  %xor121 = xor i64 %71, %72
  %call122 = call i64 @rotr64(i64 %xor121, i32 32)
  %arrayidx123 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call122, i64* %arrayidx123, align 16
  %arrayidx124 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %73 = load i64, i64* %arrayidx124, align 16
  %arrayidx125 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %74 = load i64, i64* %arrayidx125, align 16
  %add126 = add i64 %73, %74
  %arrayidx127 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add126, i64* %arrayidx127, align 16
  %arrayidx128 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %75 = load i64, i64* %arrayidx128, align 16
  %arrayidx129 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %76 = load i64, i64* %arrayidx129, align 16
  %xor130 = xor i64 %75, %76
  %call131 = call i64 @rotr64(i64 %xor130, i32 24)
  %arrayidx132 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call131, i64* %arrayidx132, align 16
  %arrayidx133 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %77 = load i64, i64* %arrayidx133, align 16
  %arrayidx134 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %78 = load i64, i64* %arrayidx134, align 16
  %add135 = add i64 %77, %78
  %79 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 5), align 1
  %idxprom136 = zext i8 %79 to i32
  %arrayidx137 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom136
  %80 = load i64, i64* %arrayidx137, align 8
  %add138 = add i64 %add135, %80
  %arrayidx139 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add138, i64* %arrayidx139, align 16
  %arrayidx140 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %81 = load i64, i64* %arrayidx140, align 16
  %arrayidx141 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %82 = load i64, i64* %arrayidx141, align 16
  %xor142 = xor i64 %81, %82
  %call143 = call i64 @rotr64(i64 %xor142, i32 16)
  %arrayidx144 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call143, i64* %arrayidx144, align 16
  %arrayidx145 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %83 = load i64, i64* %arrayidx145, align 16
  %arrayidx146 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %84 = load i64, i64* %arrayidx146, align 16
  %add147 = add i64 %83, %84
  %arrayidx148 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add147, i64* %arrayidx148, align 16
  %arrayidx149 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %85 = load i64, i64* %arrayidx149, align 16
  %arrayidx150 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %86 = load i64, i64* %arrayidx150, align 16
  %xor151 = xor i64 %85, %86
  %call152 = call i64 @rotr64(i64 %xor151, i32 63)
  %arrayidx153 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call152, i64* %arrayidx153, align 16
  br label %do.end154

do.end154:                                        ; preds = %do.body111
  br label %do.body155

do.body155:                                       ; preds = %do.end154
  %arrayidx156 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %87 = load i64, i64* %arrayidx156, align 8
  %arrayidx157 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %88 = load i64, i64* %arrayidx157, align 8
  %add158 = add i64 %87, %88
  %89 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 6), align 2
  %idxprom159 = zext i8 %89 to i32
  %arrayidx160 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom159
  %90 = load i64, i64* %arrayidx160, align 8
  %add161 = add i64 %add158, %90
  %arrayidx162 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add161, i64* %arrayidx162, align 8
  %arrayidx163 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %91 = load i64, i64* %arrayidx163, align 8
  %arrayidx164 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %92 = load i64, i64* %arrayidx164, align 8
  %xor165 = xor i64 %91, %92
  %call166 = call i64 @rotr64(i64 %xor165, i32 32)
  %arrayidx167 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call166, i64* %arrayidx167, align 8
  %arrayidx168 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %93 = load i64, i64* %arrayidx168, align 8
  %arrayidx169 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %94 = load i64, i64* %arrayidx169, align 8
  %add170 = add i64 %93, %94
  %arrayidx171 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add170, i64* %arrayidx171, align 8
  %arrayidx172 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %95 = load i64, i64* %arrayidx172, align 8
  %arrayidx173 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %96 = load i64, i64* %arrayidx173, align 8
  %xor174 = xor i64 %95, %96
  %call175 = call i64 @rotr64(i64 %xor174, i32 24)
  %arrayidx176 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call175, i64* %arrayidx176, align 8
  %arrayidx177 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %97 = load i64, i64* %arrayidx177, align 8
  %arrayidx178 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %98 = load i64, i64* %arrayidx178, align 8
  %add179 = add i64 %97, %98
  %99 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 7), align 1
  %idxprom180 = zext i8 %99 to i32
  %arrayidx181 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom180
  %100 = load i64, i64* %arrayidx181, align 8
  %add182 = add i64 %add179, %100
  %arrayidx183 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add182, i64* %arrayidx183, align 8
  %arrayidx184 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %101 = load i64, i64* %arrayidx184, align 8
  %arrayidx185 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %102 = load i64, i64* %arrayidx185, align 8
  %xor186 = xor i64 %101, %102
  %call187 = call i64 @rotr64(i64 %xor186, i32 16)
  %arrayidx188 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call187, i64* %arrayidx188, align 8
  %arrayidx189 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %103 = load i64, i64* %arrayidx189, align 8
  %arrayidx190 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %104 = load i64, i64* %arrayidx190, align 8
  %add191 = add i64 %103, %104
  %arrayidx192 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add191, i64* %arrayidx192, align 8
  %arrayidx193 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %105 = load i64, i64* %arrayidx193, align 8
  %arrayidx194 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %106 = load i64, i64* %arrayidx194, align 8
  %xor195 = xor i64 %105, %106
  %call196 = call i64 @rotr64(i64 %xor195, i32 63)
  %arrayidx197 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call196, i64* %arrayidx197, align 8
  br label %do.end198

do.end198:                                        ; preds = %do.body155
  br label %do.body199

do.body199:                                       ; preds = %do.end198
  %arrayidx200 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %107 = load i64, i64* %arrayidx200, align 16
  %arrayidx201 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %108 = load i64, i64* %arrayidx201, align 8
  %add202 = add i64 %107, %108
  %109 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 8), align 8
  %idxprom203 = zext i8 %109 to i32
  %arrayidx204 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom203
  %110 = load i64, i64* %arrayidx204, align 8
  %add205 = add i64 %add202, %110
  %arrayidx206 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add205, i64* %arrayidx206, align 16
  %arrayidx207 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %111 = load i64, i64* %arrayidx207, align 8
  %arrayidx208 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %112 = load i64, i64* %arrayidx208, align 16
  %xor209 = xor i64 %111, %112
  %call210 = call i64 @rotr64(i64 %xor209, i32 32)
  %arrayidx211 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call210, i64* %arrayidx211, align 8
  %arrayidx212 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %113 = load i64, i64* %arrayidx212, align 16
  %arrayidx213 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %114 = load i64, i64* %arrayidx213, align 8
  %add214 = add i64 %113, %114
  %arrayidx215 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add214, i64* %arrayidx215, align 16
  %arrayidx216 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %115 = load i64, i64* %arrayidx216, align 8
  %arrayidx217 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %116 = load i64, i64* %arrayidx217, align 16
  %xor218 = xor i64 %115, %116
  %call219 = call i64 @rotr64(i64 %xor218, i32 24)
  %arrayidx220 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call219, i64* %arrayidx220, align 8
  %arrayidx221 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %117 = load i64, i64* %arrayidx221, align 16
  %arrayidx222 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %118 = load i64, i64* %arrayidx222, align 8
  %add223 = add i64 %117, %118
  %119 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 9), align 1
  %idxprom224 = zext i8 %119 to i32
  %arrayidx225 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom224
  %120 = load i64, i64* %arrayidx225, align 8
  %add226 = add i64 %add223, %120
  %arrayidx227 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add226, i64* %arrayidx227, align 16
  %arrayidx228 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %121 = load i64, i64* %arrayidx228, align 8
  %arrayidx229 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %122 = load i64, i64* %arrayidx229, align 16
  %xor230 = xor i64 %121, %122
  %call231 = call i64 @rotr64(i64 %xor230, i32 16)
  %arrayidx232 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call231, i64* %arrayidx232, align 8
  %arrayidx233 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %123 = load i64, i64* %arrayidx233, align 16
  %arrayidx234 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %124 = load i64, i64* %arrayidx234, align 8
  %add235 = add i64 %123, %124
  %arrayidx236 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add235, i64* %arrayidx236, align 16
  %arrayidx237 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %125 = load i64, i64* %arrayidx237, align 8
  %arrayidx238 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %126 = load i64, i64* %arrayidx238, align 16
  %xor239 = xor i64 %125, %126
  %call240 = call i64 @rotr64(i64 %xor239, i32 63)
  %arrayidx241 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call240, i64* %arrayidx241, align 8
  br label %do.end242

do.end242:                                        ; preds = %do.body199
  br label %do.body243

do.body243:                                       ; preds = %do.end242
  %arrayidx244 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %127 = load i64, i64* %arrayidx244, align 8
  %arrayidx245 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %128 = load i64, i64* %arrayidx245, align 16
  %add246 = add i64 %127, %128
  %129 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 10), align 2
  %idxprom247 = zext i8 %129 to i32
  %arrayidx248 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom247
  %130 = load i64, i64* %arrayidx248, align 8
  %add249 = add i64 %add246, %130
  %arrayidx250 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add249, i64* %arrayidx250, align 8
  %arrayidx251 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %131 = load i64, i64* %arrayidx251, align 16
  %arrayidx252 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %132 = load i64, i64* %arrayidx252, align 8
  %xor253 = xor i64 %131, %132
  %call254 = call i64 @rotr64(i64 %xor253, i32 32)
  %arrayidx255 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call254, i64* %arrayidx255, align 16
  %arrayidx256 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %133 = load i64, i64* %arrayidx256, align 8
  %arrayidx257 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %134 = load i64, i64* %arrayidx257, align 16
  %add258 = add i64 %133, %134
  %arrayidx259 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add258, i64* %arrayidx259, align 8
  %arrayidx260 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %135 = load i64, i64* %arrayidx260, align 16
  %arrayidx261 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %136 = load i64, i64* %arrayidx261, align 8
  %xor262 = xor i64 %135, %136
  %call263 = call i64 @rotr64(i64 %xor262, i32 24)
  %arrayidx264 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call263, i64* %arrayidx264, align 16
  %arrayidx265 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %137 = load i64, i64* %arrayidx265, align 8
  %arrayidx266 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %138 = load i64, i64* %arrayidx266, align 16
  %add267 = add i64 %137, %138
  %139 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 11), align 1
  %idxprom268 = zext i8 %139 to i32
  %arrayidx269 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom268
  %140 = load i64, i64* %arrayidx269, align 8
  %add270 = add i64 %add267, %140
  %arrayidx271 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add270, i64* %arrayidx271, align 8
  %arrayidx272 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %141 = load i64, i64* %arrayidx272, align 16
  %arrayidx273 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %142 = load i64, i64* %arrayidx273, align 8
  %xor274 = xor i64 %141, %142
  %call275 = call i64 @rotr64(i64 %xor274, i32 16)
  %arrayidx276 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call275, i64* %arrayidx276, align 16
  %arrayidx277 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %143 = load i64, i64* %arrayidx277, align 8
  %arrayidx278 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %144 = load i64, i64* %arrayidx278, align 16
  %add279 = add i64 %143, %144
  %arrayidx280 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add279, i64* %arrayidx280, align 8
  %arrayidx281 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %145 = load i64, i64* %arrayidx281, align 16
  %arrayidx282 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %146 = load i64, i64* %arrayidx282, align 8
  %xor283 = xor i64 %145, %146
  %call284 = call i64 @rotr64(i64 %xor283, i32 63)
  %arrayidx285 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call284, i64* %arrayidx285, align 16
  br label %do.end286

do.end286:                                        ; preds = %do.body243
  br label %do.body287

do.body287:                                       ; preds = %do.end286
  %arrayidx288 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %147 = load i64, i64* %arrayidx288, align 16
  %arrayidx289 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %148 = load i64, i64* %arrayidx289, align 8
  %add290 = add i64 %147, %148
  %149 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 12), align 4
  %idxprom291 = zext i8 %149 to i32
  %arrayidx292 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom291
  %150 = load i64, i64* %arrayidx292, align 8
  %add293 = add i64 %add290, %150
  %arrayidx294 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add293, i64* %arrayidx294, align 16
  %arrayidx295 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %151 = load i64, i64* %arrayidx295, align 8
  %arrayidx296 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %152 = load i64, i64* %arrayidx296, align 16
  %xor297 = xor i64 %151, %152
  %call298 = call i64 @rotr64(i64 %xor297, i32 32)
  %arrayidx299 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call298, i64* %arrayidx299, align 8
  %arrayidx300 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %153 = load i64, i64* %arrayidx300, align 16
  %arrayidx301 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %154 = load i64, i64* %arrayidx301, align 8
  %add302 = add i64 %153, %154
  %arrayidx303 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add302, i64* %arrayidx303, align 16
  %arrayidx304 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %155 = load i64, i64* %arrayidx304, align 8
  %arrayidx305 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %156 = load i64, i64* %arrayidx305, align 16
  %xor306 = xor i64 %155, %156
  %call307 = call i64 @rotr64(i64 %xor306, i32 24)
  %arrayidx308 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call307, i64* %arrayidx308, align 8
  %arrayidx309 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %157 = load i64, i64* %arrayidx309, align 16
  %arrayidx310 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %158 = load i64, i64* %arrayidx310, align 8
  %add311 = add i64 %157, %158
  %159 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 13), align 1
  %idxprom312 = zext i8 %159 to i32
  %arrayidx313 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom312
  %160 = load i64, i64* %arrayidx313, align 8
  %add314 = add i64 %add311, %160
  %arrayidx315 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add314, i64* %arrayidx315, align 16
  %arrayidx316 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %161 = load i64, i64* %arrayidx316, align 8
  %arrayidx317 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %162 = load i64, i64* %arrayidx317, align 16
  %xor318 = xor i64 %161, %162
  %call319 = call i64 @rotr64(i64 %xor318, i32 16)
  %arrayidx320 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call319, i64* %arrayidx320, align 8
  %arrayidx321 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %163 = load i64, i64* %arrayidx321, align 16
  %arrayidx322 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %164 = load i64, i64* %arrayidx322, align 8
  %add323 = add i64 %163, %164
  %arrayidx324 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add323, i64* %arrayidx324, align 16
  %arrayidx325 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %165 = load i64, i64* %arrayidx325, align 8
  %arrayidx326 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %166 = load i64, i64* %arrayidx326, align 16
  %xor327 = xor i64 %165, %166
  %call328 = call i64 @rotr64(i64 %xor327, i32 63)
  %arrayidx329 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call328, i64* %arrayidx329, align 8
  br label %do.end330

do.end330:                                        ; preds = %do.body287
  br label %do.body331

do.body331:                                       ; preds = %do.end330
  %arrayidx332 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %167 = load i64, i64* %arrayidx332, align 8
  %arrayidx333 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %168 = load i64, i64* %arrayidx333, align 16
  %add334 = add i64 %167, %168
  %169 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 14), align 2
  %idxprom335 = zext i8 %169 to i32
  %arrayidx336 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom335
  %170 = load i64, i64* %arrayidx336, align 8
  %add337 = add i64 %add334, %170
  %arrayidx338 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add337, i64* %arrayidx338, align 8
  %arrayidx339 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %171 = load i64, i64* %arrayidx339, align 16
  %arrayidx340 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %172 = load i64, i64* %arrayidx340, align 8
  %xor341 = xor i64 %171, %172
  %call342 = call i64 @rotr64(i64 %xor341, i32 32)
  %arrayidx343 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call342, i64* %arrayidx343, align 16
  %arrayidx344 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %173 = load i64, i64* %arrayidx344, align 8
  %arrayidx345 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %174 = load i64, i64* %arrayidx345, align 16
  %add346 = add i64 %173, %174
  %arrayidx347 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add346, i64* %arrayidx347, align 8
  %arrayidx348 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %175 = load i64, i64* %arrayidx348, align 16
  %arrayidx349 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %176 = load i64, i64* %arrayidx349, align 8
  %xor350 = xor i64 %175, %176
  %call351 = call i64 @rotr64(i64 %xor350, i32 24)
  %arrayidx352 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call351, i64* %arrayidx352, align 16
  %arrayidx353 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %177 = load i64, i64* %arrayidx353, align 8
  %arrayidx354 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %178 = load i64, i64* %arrayidx354, align 16
  %add355 = add i64 %177, %178
  %179 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 15), align 1
  %idxprom356 = zext i8 %179 to i32
  %arrayidx357 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom356
  %180 = load i64, i64* %arrayidx357, align 8
  %add358 = add i64 %add355, %180
  %arrayidx359 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add358, i64* %arrayidx359, align 8
  %arrayidx360 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %181 = load i64, i64* %arrayidx360, align 16
  %arrayidx361 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %182 = load i64, i64* %arrayidx361, align 8
  %xor362 = xor i64 %181, %182
  %call363 = call i64 @rotr64(i64 %xor362, i32 16)
  %arrayidx364 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call363, i64* %arrayidx364, align 16
  %arrayidx365 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %183 = load i64, i64* %arrayidx365, align 8
  %arrayidx366 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %184 = load i64, i64* %arrayidx366, align 16
  %add367 = add i64 %183, %184
  %arrayidx368 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add367, i64* %arrayidx368, align 8
  %arrayidx369 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %185 = load i64, i64* %arrayidx369, align 16
  %arrayidx370 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %186 = load i64, i64* %arrayidx370, align 8
  %xor371 = xor i64 %185, %186
  %call372 = call i64 @rotr64(i64 %xor371, i32 63)
  %arrayidx373 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call372, i64* %arrayidx373, align 16
  br label %do.end374

do.end374:                                        ; preds = %do.body331
  br label %do.end375

do.end375:                                        ; preds = %do.end374
  br label %do.body376

do.body376:                                       ; preds = %do.end375
  br label %do.body377

do.body377:                                       ; preds = %do.body376
  %arrayidx378 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %187 = load i64, i64* %arrayidx378, align 16
  %arrayidx379 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %188 = load i64, i64* %arrayidx379, align 16
  %add380 = add i64 %187, %188
  %189 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 0), align 16
  %idxprom381 = zext i8 %189 to i32
  %arrayidx382 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom381
  %190 = load i64, i64* %arrayidx382, align 8
  %add383 = add i64 %add380, %190
  %arrayidx384 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add383, i64* %arrayidx384, align 16
  %arrayidx385 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %191 = load i64, i64* %arrayidx385, align 16
  %arrayidx386 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %192 = load i64, i64* %arrayidx386, align 16
  %xor387 = xor i64 %191, %192
  %call388 = call i64 @rotr64(i64 %xor387, i32 32)
  %arrayidx389 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call388, i64* %arrayidx389, align 16
  %arrayidx390 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %193 = load i64, i64* %arrayidx390, align 16
  %arrayidx391 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %194 = load i64, i64* %arrayidx391, align 16
  %add392 = add i64 %193, %194
  %arrayidx393 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add392, i64* %arrayidx393, align 16
  %arrayidx394 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %195 = load i64, i64* %arrayidx394, align 16
  %arrayidx395 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %196 = load i64, i64* %arrayidx395, align 16
  %xor396 = xor i64 %195, %196
  %call397 = call i64 @rotr64(i64 %xor396, i32 24)
  %arrayidx398 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call397, i64* %arrayidx398, align 16
  %arrayidx399 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %197 = load i64, i64* %arrayidx399, align 16
  %arrayidx400 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %198 = load i64, i64* %arrayidx400, align 16
  %add401 = add i64 %197, %198
  %199 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 1), align 1
  %idxprom402 = zext i8 %199 to i32
  %arrayidx403 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom402
  %200 = load i64, i64* %arrayidx403, align 8
  %add404 = add i64 %add401, %200
  %arrayidx405 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add404, i64* %arrayidx405, align 16
  %arrayidx406 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %201 = load i64, i64* %arrayidx406, align 16
  %arrayidx407 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %202 = load i64, i64* %arrayidx407, align 16
  %xor408 = xor i64 %201, %202
  %call409 = call i64 @rotr64(i64 %xor408, i32 16)
  %arrayidx410 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call409, i64* %arrayidx410, align 16
  %arrayidx411 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %203 = load i64, i64* %arrayidx411, align 16
  %arrayidx412 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %204 = load i64, i64* %arrayidx412, align 16
  %add413 = add i64 %203, %204
  %arrayidx414 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add413, i64* %arrayidx414, align 16
  %arrayidx415 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %205 = load i64, i64* %arrayidx415, align 16
  %arrayidx416 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %206 = load i64, i64* %arrayidx416, align 16
  %xor417 = xor i64 %205, %206
  %call418 = call i64 @rotr64(i64 %xor417, i32 63)
  %arrayidx419 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call418, i64* %arrayidx419, align 16
  br label %do.end420

do.end420:                                        ; preds = %do.body377
  br label %do.body421

do.body421:                                       ; preds = %do.end420
  %arrayidx422 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %207 = load i64, i64* %arrayidx422, align 8
  %arrayidx423 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %208 = load i64, i64* %arrayidx423, align 8
  %add424 = add i64 %207, %208
  %209 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 2), align 2
  %idxprom425 = zext i8 %209 to i32
  %arrayidx426 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom425
  %210 = load i64, i64* %arrayidx426, align 8
  %add427 = add i64 %add424, %210
  %arrayidx428 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add427, i64* %arrayidx428, align 8
  %arrayidx429 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %211 = load i64, i64* %arrayidx429, align 8
  %arrayidx430 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %212 = load i64, i64* %arrayidx430, align 8
  %xor431 = xor i64 %211, %212
  %call432 = call i64 @rotr64(i64 %xor431, i32 32)
  %arrayidx433 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call432, i64* %arrayidx433, align 8
  %arrayidx434 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %213 = load i64, i64* %arrayidx434, align 8
  %arrayidx435 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %214 = load i64, i64* %arrayidx435, align 8
  %add436 = add i64 %213, %214
  %arrayidx437 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add436, i64* %arrayidx437, align 8
  %arrayidx438 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %215 = load i64, i64* %arrayidx438, align 8
  %arrayidx439 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %216 = load i64, i64* %arrayidx439, align 8
  %xor440 = xor i64 %215, %216
  %call441 = call i64 @rotr64(i64 %xor440, i32 24)
  %arrayidx442 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call441, i64* %arrayidx442, align 8
  %arrayidx443 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %217 = load i64, i64* %arrayidx443, align 8
  %arrayidx444 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %218 = load i64, i64* %arrayidx444, align 8
  %add445 = add i64 %217, %218
  %219 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 3), align 1
  %idxprom446 = zext i8 %219 to i32
  %arrayidx447 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom446
  %220 = load i64, i64* %arrayidx447, align 8
  %add448 = add i64 %add445, %220
  %arrayidx449 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add448, i64* %arrayidx449, align 8
  %arrayidx450 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %221 = load i64, i64* %arrayidx450, align 8
  %arrayidx451 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %222 = load i64, i64* %arrayidx451, align 8
  %xor452 = xor i64 %221, %222
  %call453 = call i64 @rotr64(i64 %xor452, i32 16)
  %arrayidx454 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call453, i64* %arrayidx454, align 8
  %arrayidx455 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %223 = load i64, i64* %arrayidx455, align 8
  %arrayidx456 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %224 = load i64, i64* %arrayidx456, align 8
  %add457 = add i64 %223, %224
  %arrayidx458 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add457, i64* %arrayidx458, align 8
  %arrayidx459 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %225 = load i64, i64* %arrayidx459, align 8
  %arrayidx460 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %226 = load i64, i64* %arrayidx460, align 8
  %xor461 = xor i64 %225, %226
  %call462 = call i64 @rotr64(i64 %xor461, i32 63)
  %arrayidx463 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call462, i64* %arrayidx463, align 8
  br label %do.end464

do.end464:                                        ; preds = %do.body421
  br label %do.body465

do.body465:                                       ; preds = %do.end464
  %arrayidx466 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %227 = load i64, i64* %arrayidx466, align 16
  %arrayidx467 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %228 = load i64, i64* %arrayidx467, align 16
  %add468 = add i64 %227, %228
  %229 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 4), align 4
  %idxprom469 = zext i8 %229 to i32
  %arrayidx470 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom469
  %230 = load i64, i64* %arrayidx470, align 8
  %add471 = add i64 %add468, %230
  %arrayidx472 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add471, i64* %arrayidx472, align 16
  %arrayidx473 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %231 = load i64, i64* %arrayidx473, align 16
  %arrayidx474 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %232 = load i64, i64* %arrayidx474, align 16
  %xor475 = xor i64 %231, %232
  %call476 = call i64 @rotr64(i64 %xor475, i32 32)
  %arrayidx477 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call476, i64* %arrayidx477, align 16
  %arrayidx478 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %233 = load i64, i64* %arrayidx478, align 16
  %arrayidx479 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %234 = load i64, i64* %arrayidx479, align 16
  %add480 = add i64 %233, %234
  %arrayidx481 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add480, i64* %arrayidx481, align 16
  %arrayidx482 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %235 = load i64, i64* %arrayidx482, align 16
  %arrayidx483 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %236 = load i64, i64* %arrayidx483, align 16
  %xor484 = xor i64 %235, %236
  %call485 = call i64 @rotr64(i64 %xor484, i32 24)
  %arrayidx486 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call485, i64* %arrayidx486, align 16
  %arrayidx487 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %237 = load i64, i64* %arrayidx487, align 16
  %arrayidx488 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %238 = load i64, i64* %arrayidx488, align 16
  %add489 = add i64 %237, %238
  %239 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 5), align 1
  %idxprom490 = zext i8 %239 to i32
  %arrayidx491 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom490
  %240 = load i64, i64* %arrayidx491, align 8
  %add492 = add i64 %add489, %240
  %arrayidx493 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add492, i64* %arrayidx493, align 16
  %arrayidx494 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %241 = load i64, i64* %arrayidx494, align 16
  %arrayidx495 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %242 = load i64, i64* %arrayidx495, align 16
  %xor496 = xor i64 %241, %242
  %call497 = call i64 @rotr64(i64 %xor496, i32 16)
  %arrayidx498 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call497, i64* %arrayidx498, align 16
  %arrayidx499 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %243 = load i64, i64* %arrayidx499, align 16
  %arrayidx500 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %244 = load i64, i64* %arrayidx500, align 16
  %add501 = add i64 %243, %244
  %arrayidx502 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add501, i64* %arrayidx502, align 16
  %arrayidx503 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %245 = load i64, i64* %arrayidx503, align 16
  %arrayidx504 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %246 = load i64, i64* %arrayidx504, align 16
  %xor505 = xor i64 %245, %246
  %call506 = call i64 @rotr64(i64 %xor505, i32 63)
  %arrayidx507 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call506, i64* %arrayidx507, align 16
  br label %do.end508

do.end508:                                        ; preds = %do.body465
  br label %do.body509

do.body509:                                       ; preds = %do.end508
  %arrayidx510 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %247 = load i64, i64* %arrayidx510, align 8
  %arrayidx511 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %248 = load i64, i64* %arrayidx511, align 8
  %add512 = add i64 %247, %248
  %249 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 6), align 2
  %idxprom513 = zext i8 %249 to i32
  %arrayidx514 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom513
  %250 = load i64, i64* %arrayidx514, align 8
  %add515 = add i64 %add512, %250
  %arrayidx516 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add515, i64* %arrayidx516, align 8
  %arrayidx517 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %251 = load i64, i64* %arrayidx517, align 8
  %arrayidx518 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %252 = load i64, i64* %arrayidx518, align 8
  %xor519 = xor i64 %251, %252
  %call520 = call i64 @rotr64(i64 %xor519, i32 32)
  %arrayidx521 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call520, i64* %arrayidx521, align 8
  %arrayidx522 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %253 = load i64, i64* %arrayidx522, align 8
  %arrayidx523 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %254 = load i64, i64* %arrayidx523, align 8
  %add524 = add i64 %253, %254
  %arrayidx525 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add524, i64* %arrayidx525, align 8
  %arrayidx526 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %255 = load i64, i64* %arrayidx526, align 8
  %arrayidx527 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %256 = load i64, i64* %arrayidx527, align 8
  %xor528 = xor i64 %255, %256
  %call529 = call i64 @rotr64(i64 %xor528, i32 24)
  %arrayidx530 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call529, i64* %arrayidx530, align 8
  %arrayidx531 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %257 = load i64, i64* %arrayidx531, align 8
  %arrayidx532 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %258 = load i64, i64* %arrayidx532, align 8
  %add533 = add i64 %257, %258
  %259 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 7), align 1
  %idxprom534 = zext i8 %259 to i32
  %arrayidx535 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom534
  %260 = load i64, i64* %arrayidx535, align 8
  %add536 = add i64 %add533, %260
  %arrayidx537 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add536, i64* %arrayidx537, align 8
  %arrayidx538 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %261 = load i64, i64* %arrayidx538, align 8
  %arrayidx539 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %262 = load i64, i64* %arrayidx539, align 8
  %xor540 = xor i64 %261, %262
  %call541 = call i64 @rotr64(i64 %xor540, i32 16)
  %arrayidx542 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call541, i64* %arrayidx542, align 8
  %arrayidx543 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %263 = load i64, i64* %arrayidx543, align 8
  %arrayidx544 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %264 = load i64, i64* %arrayidx544, align 8
  %add545 = add i64 %263, %264
  %arrayidx546 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add545, i64* %arrayidx546, align 8
  %arrayidx547 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %265 = load i64, i64* %arrayidx547, align 8
  %arrayidx548 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %266 = load i64, i64* %arrayidx548, align 8
  %xor549 = xor i64 %265, %266
  %call550 = call i64 @rotr64(i64 %xor549, i32 63)
  %arrayidx551 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call550, i64* %arrayidx551, align 8
  br label %do.end552

do.end552:                                        ; preds = %do.body509
  br label %do.body553

do.body553:                                       ; preds = %do.end552
  %arrayidx554 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %267 = load i64, i64* %arrayidx554, align 16
  %arrayidx555 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %268 = load i64, i64* %arrayidx555, align 8
  %add556 = add i64 %267, %268
  %269 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 8), align 8
  %idxprom557 = zext i8 %269 to i32
  %arrayidx558 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom557
  %270 = load i64, i64* %arrayidx558, align 8
  %add559 = add i64 %add556, %270
  %arrayidx560 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add559, i64* %arrayidx560, align 16
  %arrayidx561 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %271 = load i64, i64* %arrayidx561, align 8
  %arrayidx562 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %272 = load i64, i64* %arrayidx562, align 16
  %xor563 = xor i64 %271, %272
  %call564 = call i64 @rotr64(i64 %xor563, i32 32)
  %arrayidx565 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call564, i64* %arrayidx565, align 8
  %arrayidx566 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %273 = load i64, i64* %arrayidx566, align 16
  %arrayidx567 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %274 = load i64, i64* %arrayidx567, align 8
  %add568 = add i64 %273, %274
  %arrayidx569 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add568, i64* %arrayidx569, align 16
  %arrayidx570 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %275 = load i64, i64* %arrayidx570, align 8
  %arrayidx571 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %276 = load i64, i64* %arrayidx571, align 16
  %xor572 = xor i64 %275, %276
  %call573 = call i64 @rotr64(i64 %xor572, i32 24)
  %arrayidx574 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call573, i64* %arrayidx574, align 8
  %arrayidx575 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %277 = load i64, i64* %arrayidx575, align 16
  %arrayidx576 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %278 = load i64, i64* %arrayidx576, align 8
  %add577 = add i64 %277, %278
  %279 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 9), align 1
  %idxprom578 = zext i8 %279 to i32
  %arrayidx579 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom578
  %280 = load i64, i64* %arrayidx579, align 8
  %add580 = add i64 %add577, %280
  %arrayidx581 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add580, i64* %arrayidx581, align 16
  %arrayidx582 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %281 = load i64, i64* %arrayidx582, align 8
  %arrayidx583 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %282 = load i64, i64* %arrayidx583, align 16
  %xor584 = xor i64 %281, %282
  %call585 = call i64 @rotr64(i64 %xor584, i32 16)
  %arrayidx586 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call585, i64* %arrayidx586, align 8
  %arrayidx587 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %283 = load i64, i64* %arrayidx587, align 16
  %arrayidx588 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %284 = load i64, i64* %arrayidx588, align 8
  %add589 = add i64 %283, %284
  %arrayidx590 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add589, i64* %arrayidx590, align 16
  %arrayidx591 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %285 = load i64, i64* %arrayidx591, align 8
  %arrayidx592 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %286 = load i64, i64* %arrayidx592, align 16
  %xor593 = xor i64 %285, %286
  %call594 = call i64 @rotr64(i64 %xor593, i32 63)
  %arrayidx595 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call594, i64* %arrayidx595, align 8
  br label %do.end596

do.end596:                                        ; preds = %do.body553
  br label %do.body597

do.body597:                                       ; preds = %do.end596
  %arrayidx598 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %287 = load i64, i64* %arrayidx598, align 8
  %arrayidx599 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %288 = load i64, i64* %arrayidx599, align 16
  %add600 = add i64 %287, %288
  %289 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 10), align 2
  %idxprom601 = zext i8 %289 to i32
  %arrayidx602 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom601
  %290 = load i64, i64* %arrayidx602, align 8
  %add603 = add i64 %add600, %290
  %arrayidx604 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add603, i64* %arrayidx604, align 8
  %arrayidx605 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %291 = load i64, i64* %arrayidx605, align 16
  %arrayidx606 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %292 = load i64, i64* %arrayidx606, align 8
  %xor607 = xor i64 %291, %292
  %call608 = call i64 @rotr64(i64 %xor607, i32 32)
  %arrayidx609 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call608, i64* %arrayidx609, align 16
  %arrayidx610 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %293 = load i64, i64* %arrayidx610, align 8
  %arrayidx611 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %294 = load i64, i64* %arrayidx611, align 16
  %add612 = add i64 %293, %294
  %arrayidx613 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add612, i64* %arrayidx613, align 8
  %arrayidx614 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %295 = load i64, i64* %arrayidx614, align 16
  %arrayidx615 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %296 = load i64, i64* %arrayidx615, align 8
  %xor616 = xor i64 %295, %296
  %call617 = call i64 @rotr64(i64 %xor616, i32 24)
  %arrayidx618 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call617, i64* %arrayidx618, align 16
  %arrayidx619 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %297 = load i64, i64* %arrayidx619, align 8
  %arrayidx620 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %298 = load i64, i64* %arrayidx620, align 16
  %add621 = add i64 %297, %298
  %299 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 11), align 1
  %idxprom622 = zext i8 %299 to i32
  %arrayidx623 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom622
  %300 = load i64, i64* %arrayidx623, align 8
  %add624 = add i64 %add621, %300
  %arrayidx625 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add624, i64* %arrayidx625, align 8
  %arrayidx626 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %301 = load i64, i64* %arrayidx626, align 16
  %arrayidx627 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %302 = load i64, i64* %arrayidx627, align 8
  %xor628 = xor i64 %301, %302
  %call629 = call i64 @rotr64(i64 %xor628, i32 16)
  %arrayidx630 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call629, i64* %arrayidx630, align 16
  %arrayidx631 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %303 = load i64, i64* %arrayidx631, align 8
  %arrayidx632 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %304 = load i64, i64* %arrayidx632, align 16
  %add633 = add i64 %303, %304
  %arrayidx634 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add633, i64* %arrayidx634, align 8
  %arrayidx635 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %305 = load i64, i64* %arrayidx635, align 16
  %arrayidx636 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %306 = load i64, i64* %arrayidx636, align 8
  %xor637 = xor i64 %305, %306
  %call638 = call i64 @rotr64(i64 %xor637, i32 63)
  %arrayidx639 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call638, i64* %arrayidx639, align 16
  br label %do.end640

do.end640:                                        ; preds = %do.body597
  br label %do.body641

do.body641:                                       ; preds = %do.end640
  %arrayidx642 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %307 = load i64, i64* %arrayidx642, align 16
  %arrayidx643 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %308 = load i64, i64* %arrayidx643, align 8
  %add644 = add i64 %307, %308
  %309 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 12), align 4
  %idxprom645 = zext i8 %309 to i32
  %arrayidx646 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom645
  %310 = load i64, i64* %arrayidx646, align 8
  %add647 = add i64 %add644, %310
  %arrayidx648 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add647, i64* %arrayidx648, align 16
  %arrayidx649 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %311 = load i64, i64* %arrayidx649, align 8
  %arrayidx650 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %312 = load i64, i64* %arrayidx650, align 16
  %xor651 = xor i64 %311, %312
  %call652 = call i64 @rotr64(i64 %xor651, i32 32)
  %arrayidx653 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call652, i64* %arrayidx653, align 8
  %arrayidx654 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %313 = load i64, i64* %arrayidx654, align 16
  %arrayidx655 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %314 = load i64, i64* %arrayidx655, align 8
  %add656 = add i64 %313, %314
  %arrayidx657 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add656, i64* %arrayidx657, align 16
  %arrayidx658 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %315 = load i64, i64* %arrayidx658, align 8
  %arrayidx659 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %316 = load i64, i64* %arrayidx659, align 16
  %xor660 = xor i64 %315, %316
  %call661 = call i64 @rotr64(i64 %xor660, i32 24)
  %arrayidx662 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call661, i64* %arrayidx662, align 8
  %arrayidx663 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %317 = load i64, i64* %arrayidx663, align 16
  %arrayidx664 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %318 = load i64, i64* %arrayidx664, align 8
  %add665 = add i64 %317, %318
  %319 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 13), align 1
  %idxprom666 = zext i8 %319 to i32
  %arrayidx667 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom666
  %320 = load i64, i64* %arrayidx667, align 8
  %add668 = add i64 %add665, %320
  %arrayidx669 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add668, i64* %arrayidx669, align 16
  %arrayidx670 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %321 = load i64, i64* %arrayidx670, align 8
  %arrayidx671 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %322 = load i64, i64* %arrayidx671, align 16
  %xor672 = xor i64 %321, %322
  %call673 = call i64 @rotr64(i64 %xor672, i32 16)
  %arrayidx674 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call673, i64* %arrayidx674, align 8
  %arrayidx675 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %323 = load i64, i64* %arrayidx675, align 16
  %arrayidx676 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %324 = load i64, i64* %arrayidx676, align 8
  %add677 = add i64 %323, %324
  %arrayidx678 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add677, i64* %arrayidx678, align 16
  %arrayidx679 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %325 = load i64, i64* %arrayidx679, align 8
  %arrayidx680 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %326 = load i64, i64* %arrayidx680, align 16
  %xor681 = xor i64 %325, %326
  %call682 = call i64 @rotr64(i64 %xor681, i32 63)
  %arrayidx683 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call682, i64* %arrayidx683, align 8
  br label %do.end684

do.end684:                                        ; preds = %do.body641
  br label %do.body685

do.body685:                                       ; preds = %do.end684
  %arrayidx686 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %327 = load i64, i64* %arrayidx686, align 8
  %arrayidx687 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %328 = load i64, i64* %arrayidx687, align 16
  %add688 = add i64 %327, %328
  %329 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 14), align 2
  %idxprom689 = zext i8 %329 to i32
  %arrayidx690 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom689
  %330 = load i64, i64* %arrayidx690, align 8
  %add691 = add i64 %add688, %330
  %arrayidx692 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add691, i64* %arrayidx692, align 8
  %arrayidx693 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %331 = load i64, i64* %arrayidx693, align 16
  %arrayidx694 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %332 = load i64, i64* %arrayidx694, align 8
  %xor695 = xor i64 %331, %332
  %call696 = call i64 @rotr64(i64 %xor695, i32 32)
  %arrayidx697 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call696, i64* %arrayidx697, align 16
  %arrayidx698 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %333 = load i64, i64* %arrayidx698, align 8
  %arrayidx699 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %334 = load i64, i64* %arrayidx699, align 16
  %add700 = add i64 %333, %334
  %arrayidx701 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add700, i64* %arrayidx701, align 8
  %arrayidx702 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %335 = load i64, i64* %arrayidx702, align 16
  %arrayidx703 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %336 = load i64, i64* %arrayidx703, align 8
  %xor704 = xor i64 %335, %336
  %call705 = call i64 @rotr64(i64 %xor704, i32 24)
  %arrayidx706 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call705, i64* %arrayidx706, align 16
  %arrayidx707 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %337 = load i64, i64* %arrayidx707, align 8
  %arrayidx708 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %338 = load i64, i64* %arrayidx708, align 16
  %add709 = add i64 %337, %338
  %339 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 15), align 1
  %idxprom710 = zext i8 %339 to i32
  %arrayidx711 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom710
  %340 = load i64, i64* %arrayidx711, align 8
  %add712 = add i64 %add709, %340
  %arrayidx713 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add712, i64* %arrayidx713, align 8
  %arrayidx714 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %341 = load i64, i64* %arrayidx714, align 16
  %arrayidx715 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %342 = load i64, i64* %arrayidx715, align 8
  %xor716 = xor i64 %341, %342
  %call717 = call i64 @rotr64(i64 %xor716, i32 16)
  %arrayidx718 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call717, i64* %arrayidx718, align 16
  %arrayidx719 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %343 = load i64, i64* %arrayidx719, align 8
  %arrayidx720 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %344 = load i64, i64* %arrayidx720, align 16
  %add721 = add i64 %343, %344
  %arrayidx722 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add721, i64* %arrayidx722, align 8
  %arrayidx723 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %345 = load i64, i64* %arrayidx723, align 16
  %arrayidx724 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %346 = load i64, i64* %arrayidx724, align 8
  %xor725 = xor i64 %345, %346
  %call726 = call i64 @rotr64(i64 %xor725, i32 63)
  %arrayidx727 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call726, i64* %arrayidx727, align 16
  br label %do.end728

do.end728:                                        ; preds = %do.body685
  br label %do.end729

do.end729:                                        ; preds = %do.end728
  br label %do.body730

do.body730:                                       ; preds = %do.end729
  br label %do.body731

do.body731:                                       ; preds = %do.body730
  %arrayidx732 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %347 = load i64, i64* %arrayidx732, align 16
  %arrayidx733 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %348 = load i64, i64* %arrayidx733, align 16
  %add734 = add i64 %347, %348
  %349 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 0), align 16
  %idxprom735 = zext i8 %349 to i32
  %arrayidx736 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom735
  %350 = load i64, i64* %arrayidx736, align 8
  %add737 = add i64 %add734, %350
  %arrayidx738 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add737, i64* %arrayidx738, align 16
  %arrayidx739 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %351 = load i64, i64* %arrayidx739, align 16
  %arrayidx740 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %352 = load i64, i64* %arrayidx740, align 16
  %xor741 = xor i64 %351, %352
  %call742 = call i64 @rotr64(i64 %xor741, i32 32)
  %arrayidx743 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call742, i64* %arrayidx743, align 16
  %arrayidx744 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %353 = load i64, i64* %arrayidx744, align 16
  %arrayidx745 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %354 = load i64, i64* %arrayidx745, align 16
  %add746 = add i64 %353, %354
  %arrayidx747 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add746, i64* %arrayidx747, align 16
  %arrayidx748 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %355 = load i64, i64* %arrayidx748, align 16
  %arrayidx749 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %356 = load i64, i64* %arrayidx749, align 16
  %xor750 = xor i64 %355, %356
  %call751 = call i64 @rotr64(i64 %xor750, i32 24)
  %arrayidx752 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call751, i64* %arrayidx752, align 16
  %arrayidx753 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %357 = load i64, i64* %arrayidx753, align 16
  %arrayidx754 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %358 = load i64, i64* %arrayidx754, align 16
  %add755 = add i64 %357, %358
  %359 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 1), align 1
  %idxprom756 = zext i8 %359 to i32
  %arrayidx757 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom756
  %360 = load i64, i64* %arrayidx757, align 8
  %add758 = add i64 %add755, %360
  %arrayidx759 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add758, i64* %arrayidx759, align 16
  %arrayidx760 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %361 = load i64, i64* %arrayidx760, align 16
  %arrayidx761 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %362 = load i64, i64* %arrayidx761, align 16
  %xor762 = xor i64 %361, %362
  %call763 = call i64 @rotr64(i64 %xor762, i32 16)
  %arrayidx764 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call763, i64* %arrayidx764, align 16
  %arrayidx765 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %363 = load i64, i64* %arrayidx765, align 16
  %arrayidx766 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %364 = load i64, i64* %arrayidx766, align 16
  %add767 = add i64 %363, %364
  %arrayidx768 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add767, i64* %arrayidx768, align 16
  %arrayidx769 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %365 = load i64, i64* %arrayidx769, align 16
  %arrayidx770 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %366 = load i64, i64* %arrayidx770, align 16
  %xor771 = xor i64 %365, %366
  %call772 = call i64 @rotr64(i64 %xor771, i32 63)
  %arrayidx773 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call772, i64* %arrayidx773, align 16
  br label %do.end774

do.end774:                                        ; preds = %do.body731
  br label %do.body775

do.body775:                                       ; preds = %do.end774
  %arrayidx776 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %367 = load i64, i64* %arrayidx776, align 8
  %arrayidx777 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %368 = load i64, i64* %arrayidx777, align 8
  %add778 = add i64 %367, %368
  %369 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 2), align 2
  %idxprom779 = zext i8 %369 to i32
  %arrayidx780 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom779
  %370 = load i64, i64* %arrayidx780, align 8
  %add781 = add i64 %add778, %370
  %arrayidx782 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add781, i64* %arrayidx782, align 8
  %arrayidx783 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %371 = load i64, i64* %arrayidx783, align 8
  %arrayidx784 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %372 = load i64, i64* %arrayidx784, align 8
  %xor785 = xor i64 %371, %372
  %call786 = call i64 @rotr64(i64 %xor785, i32 32)
  %arrayidx787 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call786, i64* %arrayidx787, align 8
  %arrayidx788 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %373 = load i64, i64* %arrayidx788, align 8
  %arrayidx789 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %374 = load i64, i64* %arrayidx789, align 8
  %add790 = add i64 %373, %374
  %arrayidx791 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add790, i64* %arrayidx791, align 8
  %arrayidx792 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %375 = load i64, i64* %arrayidx792, align 8
  %arrayidx793 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %376 = load i64, i64* %arrayidx793, align 8
  %xor794 = xor i64 %375, %376
  %call795 = call i64 @rotr64(i64 %xor794, i32 24)
  %arrayidx796 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call795, i64* %arrayidx796, align 8
  %arrayidx797 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %377 = load i64, i64* %arrayidx797, align 8
  %arrayidx798 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %378 = load i64, i64* %arrayidx798, align 8
  %add799 = add i64 %377, %378
  %379 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 3), align 1
  %idxprom800 = zext i8 %379 to i32
  %arrayidx801 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom800
  %380 = load i64, i64* %arrayidx801, align 8
  %add802 = add i64 %add799, %380
  %arrayidx803 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add802, i64* %arrayidx803, align 8
  %arrayidx804 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %381 = load i64, i64* %arrayidx804, align 8
  %arrayidx805 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %382 = load i64, i64* %arrayidx805, align 8
  %xor806 = xor i64 %381, %382
  %call807 = call i64 @rotr64(i64 %xor806, i32 16)
  %arrayidx808 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call807, i64* %arrayidx808, align 8
  %arrayidx809 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %383 = load i64, i64* %arrayidx809, align 8
  %arrayidx810 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %384 = load i64, i64* %arrayidx810, align 8
  %add811 = add i64 %383, %384
  %arrayidx812 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add811, i64* %arrayidx812, align 8
  %arrayidx813 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %385 = load i64, i64* %arrayidx813, align 8
  %arrayidx814 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %386 = load i64, i64* %arrayidx814, align 8
  %xor815 = xor i64 %385, %386
  %call816 = call i64 @rotr64(i64 %xor815, i32 63)
  %arrayidx817 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call816, i64* %arrayidx817, align 8
  br label %do.end818

do.end818:                                        ; preds = %do.body775
  br label %do.body819

do.body819:                                       ; preds = %do.end818
  %arrayidx820 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %387 = load i64, i64* %arrayidx820, align 16
  %arrayidx821 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %388 = load i64, i64* %arrayidx821, align 16
  %add822 = add i64 %387, %388
  %389 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 4), align 4
  %idxprom823 = zext i8 %389 to i32
  %arrayidx824 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom823
  %390 = load i64, i64* %arrayidx824, align 8
  %add825 = add i64 %add822, %390
  %arrayidx826 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add825, i64* %arrayidx826, align 16
  %arrayidx827 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %391 = load i64, i64* %arrayidx827, align 16
  %arrayidx828 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %392 = load i64, i64* %arrayidx828, align 16
  %xor829 = xor i64 %391, %392
  %call830 = call i64 @rotr64(i64 %xor829, i32 32)
  %arrayidx831 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call830, i64* %arrayidx831, align 16
  %arrayidx832 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %393 = load i64, i64* %arrayidx832, align 16
  %arrayidx833 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %394 = load i64, i64* %arrayidx833, align 16
  %add834 = add i64 %393, %394
  %arrayidx835 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add834, i64* %arrayidx835, align 16
  %arrayidx836 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %395 = load i64, i64* %arrayidx836, align 16
  %arrayidx837 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %396 = load i64, i64* %arrayidx837, align 16
  %xor838 = xor i64 %395, %396
  %call839 = call i64 @rotr64(i64 %xor838, i32 24)
  %arrayidx840 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call839, i64* %arrayidx840, align 16
  %arrayidx841 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %397 = load i64, i64* %arrayidx841, align 16
  %arrayidx842 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %398 = load i64, i64* %arrayidx842, align 16
  %add843 = add i64 %397, %398
  %399 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 5), align 1
  %idxprom844 = zext i8 %399 to i32
  %arrayidx845 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom844
  %400 = load i64, i64* %arrayidx845, align 8
  %add846 = add i64 %add843, %400
  %arrayidx847 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add846, i64* %arrayidx847, align 16
  %arrayidx848 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %401 = load i64, i64* %arrayidx848, align 16
  %arrayidx849 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %402 = load i64, i64* %arrayidx849, align 16
  %xor850 = xor i64 %401, %402
  %call851 = call i64 @rotr64(i64 %xor850, i32 16)
  %arrayidx852 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call851, i64* %arrayidx852, align 16
  %arrayidx853 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %403 = load i64, i64* %arrayidx853, align 16
  %arrayidx854 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %404 = load i64, i64* %arrayidx854, align 16
  %add855 = add i64 %403, %404
  %arrayidx856 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add855, i64* %arrayidx856, align 16
  %arrayidx857 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %405 = load i64, i64* %arrayidx857, align 16
  %arrayidx858 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %406 = load i64, i64* %arrayidx858, align 16
  %xor859 = xor i64 %405, %406
  %call860 = call i64 @rotr64(i64 %xor859, i32 63)
  %arrayidx861 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call860, i64* %arrayidx861, align 16
  br label %do.end862

do.end862:                                        ; preds = %do.body819
  br label %do.body863

do.body863:                                       ; preds = %do.end862
  %arrayidx864 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %407 = load i64, i64* %arrayidx864, align 8
  %arrayidx865 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %408 = load i64, i64* %arrayidx865, align 8
  %add866 = add i64 %407, %408
  %409 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 6), align 2
  %idxprom867 = zext i8 %409 to i32
  %arrayidx868 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom867
  %410 = load i64, i64* %arrayidx868, align 8
  %add869 = add i64 %add866, %410
  %arrayidx870 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add869, i64* %arrayidx870, align 8
  %arrayidx871 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %411 = load i64, i64* %arrayidx871, align 8
  %arrayidx872 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %412 = load i64, i64* %arrayidx872, align 8
  %xor873 = xor i64 %411, %412
  %call874 = call i64 @rotr64(i64 %xor873, i32 32)
  %arrayidx875 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call874, i64* %arrayidx875, align 8
  %arrayidx876 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %413 = load i64, i64* %arrayidx876, align 8
  %arrayidx877 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %414 = load i64, i64* %arrayidx877, align 8
  %add878 = add i64 %413, %414
  %arrayidx879 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add878, i64* %arrayidx879, align 8
  %arrayidx880 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %415 = load i64, i64* %arrayidx880, align 8
  %arrayidx881 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %416 = load i64, i64* %arrayidx881, align 8
  %xor882 = xor i64 %415, %416
  %call883 = call i64 @rotr64(i64 %xor882, i32 24)
  %arrayidx884 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call883, i64* %arrayidx884, align 8
  %arrayidx885 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %417 = load i64, i64* %arrayidx885, align 8
  %arrayidx886 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %418 = load i64, i64* %arrayidx886, align 8
  %add887 = add i64 %417, %418
  %419 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 7), align 1
  %idxprom888 = zext i8 %419 to i32
  %arrayidx889 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom888
  %420 = load i64, i64* %arrayidx889, align 8
  %add890 = add i64 %add887, %420
  %arrayidx891 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add890, i64* %arrayidx891, align 8
  %arrayidx892 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %421 = load i64, i64* %arrayidx892, align 8
  %arrayidx893 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %422 = load i64, i64* %arrayidx893, align 8
  %xor894 = xor i64 %421, %422
  %call895 = call i64 @rotr64(i64 %xor894, i32 16)
  %arrayidx896 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call895, i64* %arrayidx896, align 8
  %arrayidx897 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %423 = load i64, i64* %arrayidx897, align 8
  %arrayidx898 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %424 = load i64, i64* %arrayidx898, align 8
  %add899 = add i64 %423, %424
  %arrayidx900 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add899, i64* %arrayidx900, align 8
  %arrayidx901 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %425 = load i64, i64* %arrayidx901, align 8
  %arrayidx902 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %426 = load i64, i64* %arrayidx902, align 8
  %xor903 = xor i64 %425, %426
  %call904 = call i64 @rotr64(i64 %xor903, i32 63)
  %arrayidx905 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call904, i64* %arrayidx905, align 8
  br label %do.end906

do.end906:                                        ; preds = %do.body863
  br label %do.body907

do.body907:                                       ; preds = %do.end906
  %arrayidx908 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %427 = load i64, i64* %arrayidx908, align 16
  %arrayidx909 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %428 = load i64, i64* %arrayidx909, align 8
  %add910 = add i64 %427, %428
  %429 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 8), align 8
  %idxprom911 = zext i8 %429 to i32
  %arrayidx912 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom911
  %430 = load i64, i64* %arrayidx912, align 8
  %add913 = add i64 %add910, %430
  %arrayidx914 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add913, i64* %arrayidx914, align 16
  %arrayidx915 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %431 = load i64, i64* %arrayidx915, align 8
  %arrayidx916 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %432 = load i64, i64* %arrayidx916, align 16
  %xor917 = xor i64 %431, %432
  %call918 = call i64 @rotr64(i64 %xor917, i32 32)
  %arrayidx919 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call918, i64* %arrayidx919, align 8
  %arrayidx920 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %433 = load i64, i64* %arrayidx920, align 16
  %arrayidx921 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %434 = load i64, i64* %arrayidx921, align 8
  %add922 = add i64 %433, %434
  %arrayidx923 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add922, i64* %arrayidx923, align 16
  %arrayidx924 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %435 = load i64, i64* %arrayidx924, align 8
  %arrayidx925 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %436 = load i64, i64* %arrayidx925, align 16
  %xor926 = xor i64 %435, %436
  %call927 = call i64 @rotr64(i64 %xor926, i32 24)
  %arrayidx928 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call927, i64* %arrayidx928, align 8
  %arrayidx929 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %437 = load i64, i64* %arrayidx929, align 16
  %arrayidx930 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %438 = load i64, i64* %arrayidx930, align 8
  %add931 = add i64 %437, %438
  %439 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 9), align 1
  %idxprom932 = zext i8 %439 to i32
  %arrayidx933 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom932
  %440 = load i64, i64* %arrayidx933, align 8
  %add934 = add i64 %add931, %440
  %arrayidx935 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add934, i64* %arrayidx935, align 16
  %arrayidx936 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %441 = load i64, i64* %arrayidx936, align 8
  %arrayidx937 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %442 = load i64, i64* %arrayidx937, align 16
  %xor938 = xor i64 %441, %442
  %call939 = call i64 @rotr64(i64 %xor938, i32 16)
  %arrayidx940 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call939, i64* %arrayidx940, align 8
  %arrayidx941 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %443 = load i64, i64* %arrayidx941, align 16
  %arrayidx942 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %444 = load i64, i64* %arrayidx942, align 8
  %add943 = add i64 %443, %444
  %arrayidx944 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add943, i64* %arrayidx944, align 16
  %arrayidx945 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %445 = load i64, i64* %arrayidx945, align 8
  %arrayidx946 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %446 = load i64, i64* %arrayidx946, align 16
  %xor947 = xor i64 %445, %446
  %call948 = call i64 @rotr64(i64 %xor947, i32 63)
  %arrayidx949 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call948, i64* %arrayidx949, align 8
  br label %do.end950

do.end950:                                        ; preds = %do.body907
  br label %do.body951

do.body951:                                       ; preds = %do.end950
  %arrayidx952 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %447 = load i64, i64* %arrayidx952, align 8
  %arrayidx953 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %448 = load i64, i64* %arrayidx953, align 16
  %add954 = add i64 %447, %448
  %449 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 10), align 2
  %idxprom955 = zext i8 %449 to i32
  %arrayidx956 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom955
  %450 = load i64, i64* %arrayidx956, align 8
  %add957 = add i64 %add954, %450
  %arrayidx958 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add957, i64* %arrayidx958, align 8
  %arrayidx959 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %451 = load i64, i64* %arrayidx959, align 16
  %arrayidx960 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %452 = load i64, i64* %arrayidx960, align 8
  %xor961 = xor i64 %451, %452
  %call962 = call i64 @rotr64(i64 %xor961, i32 32)
  %arrayidx963 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call962, i64* %arrayidx963, align 16
  %arrayidx964 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %453 = load i64, i64* %arrayidx964, align 8
  %arrayidx965 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %454 = load i64, i64* %arrayidx965, align 16
  %add966 = add i64 %453, %454
  %arrayidx967 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add966, i64* %arrayidx967, align 8
  %arrayidx968 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %455 = load i64, i64* %arrayidx968, align 16
  %arrayidx969 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %456 = load i64, i64* %arrayidx969, align 8
  %xor970 = xor i64 %455, %456
  %call971 = call i64 @rotr64(i64 %xor970, i32 24)
  %arrayidx972 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call971, i64* %arrayidx972, align 16
  %arrayidx973 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %457 = load i64, i64* %arrayidx973, align 8
  %arrayidx974 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %458 = load i64, i64* %arrayidx974, align 16
  %add975 = add i64 %457, %458
  %459 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 11), align 1
  %idxprom976 = zext i8 %459 to i32
  %arrayidx977 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom976
  %460 = load i64, i64* %arrayidx977, align 8
  %add978 = add i64 %add975, %460
  %arrayidx979 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add978, i64* %arrayidx979, align 8
  %arrayidx980 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %461 = load i64, i64* %arrayidx980, align 16
  %arrayidx981 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %462 = load i64, i64* %arrayidx981, align 8
  %xor982 = xor i64 %461, %462
  %call983 = call i64 @rotr64(i64 %xor982, i32 16)
  %arrayidx984 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call983, i64* %arrayidx984, align 16
  %arrayidx985 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %463 = load i64, i64* %arrayidx985, align 8
  %arrayidx986 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %464 = load i64, i64* %arrayidx986, align 16
  %add987 = add i64 %463, %464
  %arrayidx988 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add987, i64* %arrayidx988, align 8
  %arrayidx989 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %465 = load i64, i64* %arrayidx989, align 16
  %arrayidx990 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %466 = load i64, i64* %arrayidx990, align 8
  %xor991 = xor i64 %465, %466
  %call992 = call i64 @rotr64(i64 %xor991, i32 63)
  %arrayidx993 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call992, i64* %arrayidx993, align 16
  br label %do.end994

do.end994:                                        ; preds = %do.body951
  br label %do.body995

do.body995:                                       ; preds = %do.end994
  %arrayidx996 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %467 = load i64, i64* %arrayidx996, align 16
  %arrayidx997 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %468 = load i64, i64* %arrayidx997, align 8
  %add998 = add i64 %467, %468
  %469 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 12), align 4
  %idxprom999 = zext i8 %469 to i32
  %arrayidx1000 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom999
  %470 = load i64, i64* %arrayidx1000, align 8
  %add1001 = add i64 %add998, %470
  %arrayidx1002 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1001, i64* %arrayidx1002, align 16
  %arrayidx1003 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %471 = load i64, i64* %arrayidx1003, align 8
  %arrayidx1004 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %472 = load i64, i64* %arrayidx1004, align 16
  %xor1005 = xor i64 %471, %472
  %call1006 = call i64 @rotr64(i64 %xor1005, i32 32)
  %arrayidx1007 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1006, i64* %arrayidx1007, align 8
  %arrayidx1008 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %473 = load i64, i64* %arrayidx1008, align 16
  %arrayidx1009 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %474 = load i64, i64* %arrayidx1009, align 8
  %add1010 = add i64 %473, %474
  %arrayidx1011 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1010, i64* %arrayidx1011, align 16
  %arrayidx1012 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %475 = load i64, i64* %arrayidx1012, align 8
  %arrayidx1013 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %476 = load i64, i64* %arrayidx1013, align 16
  %xor1014 = xor i64 %475, %476
  %call1015 = call i64 @rotr64(i64 %xor1014, i32 24)
  %arrayidx1016 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1015, i64* %arrayidx1016, align 8
  %arrayidx1017 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %477 = load i64, i64* %arrayidx1017, align 16
  %arrayidx1018 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %478 = load i64, i64* %arrayidx1018, align 8
  %add1019 = add i64 %477, %478
  %479 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 13), align 1
  %idxprom1020 = zext i8 %479 to i32
  %arrayidx1021 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1020
  %480 = load i64, i64* %arrayidx1021, align 8
  %add1022 = add i64 %add1019, %480
  %arrayidx1023 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1022, i64* %arrayidx1023, align 16
  %arrayidx1024 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %481 = load i64, i64* %arrayidx1024, align 8
  %arrayidx1025 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %482 = load i64, i64* %arrayidx1025, align 16
  %xor1026 = xor i64 %481, %482
  %call1027 = call i64 @rotr64(i64 %xor1026, i32 16)
  %arrayidx1028 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1027, i64* %arrayidx1028, align 8
  %arrayidx1029 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %483 = load i64, i64* %arrayidx1029, align 16
  %arrayidx1030 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %484 = load i64, i64* %arrayidx1030, align 8
  %add1031 = add i64 %483, %484
  %arrayidx1032 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1031, i64* %arrayidx1032, align 16
  %arrayidx1033 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %485 = load i64, i64* %arrayidx1033, align 8
  %arrayidx1034 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %486 = load i64, i64* %arrayidx1034, align 16
  %xor1035 = xor i64 %485, %486
  %call1036 = call i64 @rotr64(i64 %xor1035, i32 63)
  %arrayidx1037 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1036, i64* %arrayidx1037, align 8
  br label %do.end1038

do.end1038:                                       ; preds = %do.body995
  br label %do.body1039

do.body1039:                                      ; preds = %do.end1038
  %arrayidx1040 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %487 = load i64, i64* %arrayidx1040, align 8
  %arrayidx1041 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %488 = load i64, i64* %arrayidx1041, align 16
  %add1042 = add i64 %487, %488
  %489 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 14), align 2
  %idxprom1043 = zext i8 %489 to i32
  %arrayidx1044 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1043
  %490 = load i64, i64* %arrayidx1044, align 8
  %add1045 = add i64 %add1042, %490
  %arrayidx1046 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1045, i64* %arrayidx1046, align 8
  %arrayidx1047 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %491 = load i64, i64* %arrayidx1047, align 16
  %arrayidx1048 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %492 = load i64, i64* %arrayidx1048, align 8
  %xor1049 = xor i64 %491, %492
  %call1050 = call i64 @rotr64(i64 %xor1049, i32 32)
  %arrayidx1051 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1050, i64* %arrayidx1051, align 16
  %arrayidx1052 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %493 = load i64, i64* %arrayidx1052, align 8
  %arrayidx1053 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %494 = load i64, i64* %arrayidx1053, align 16
  %add1054 = add i64 %493, %494
  %arrayidx1055 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1054, i64* %arrayidx1055, align 8
  %arrayidx1056 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %495 = load i64, i64* %arrayidx1056, align 16
  %arrayidx1057 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %496 = load i64, i64* %arrayidx1057, align 8
  %xor1058 = xor i64 %495, %496
  %call1059 = call i64 @rotr64(i64 %xor1058, i32 24)
  %arrayidx1060 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1059, i64* %arrayidx1060, align 16
  %arrayidx1061 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %497 = load i64, i64* %arrayidx1061, align 8
  %arrayidx1062 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %498 = load i64, i64* %arrayidx1062, align 16
  %add1063 = add i64 %497, %498
  %499 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 15), align 1
  %idxprom1064 = zext i8 %499 to i32
  %arrayidx1065 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1064
  %500 = load i64, i64* %arrayidx1065, align 8
  %add1066 = add i64 %add1063, %500
  %arrayidx1067 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1066, i64* %arrayidx1067, align 8
  %arrayidx1068 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %501 = load i64, i64* %arrayidx1068, align 16
  %arrayidx1069 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %502 = load i64, i64* %arrayidx1069, align 8
  %xor1070 = xor i64 %501, %502
  %call1071 = call i64 @rotr64(i64 %xor1070, i32 16)
  %arrayidx1072 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1071, i64* %arrayidx1072, align 16
  %arrayidx1073 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %503 = load i64, i64* %arrayidx1073, align 8
  %arrayidx1074 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %504 = load i64, i64* %arrayidx1074, align 16
  %add1075 = add i64 %503, %504
  %arrayidx1076 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1075, i64* %arrayidx1076, align 8
  %arrayidx1077 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %505 = load i64, i64* %arrayidx1077, align 16
  %arrayidx1078 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %506 = load i64, i64* %arrayidx1078, align 8
  %xor1079 = xor i64 %505, %506
  %call1080 = call i64 @rotr64(i64 %xor1079, i32 63)
  %arrayidx1081 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1080, i64* %arrayidx1081, align 16
  br label %do.end1082

do.end1082:                                       ; preds = %do.body1039
  br label %do.end1083

do.end1083:                                       ; preds = %do.end1082
  br label %do.body1084

do.body1084:                                      ; preds = %do.end1083
  br label %do.body1085

do.body1085:                                      ; preds = %do.body1084
  %arrayidx1086 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %507 = load i64, i64* %arrayidx1086, align 16
  %arrayidx1087 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %508 = load i64, i64* %arrayidx1087, align 16
  %add1088 = add i64 %507, %508
  %509 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 0), align 16
  %idxprom1089 = zext i8 %509 to i32
  %arrayidx1090 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1089
  %510 = load i64, i64* %arrayidx1090, align 8
  %add1091 = add i64 %add1088, %510
  %arrayidx1092 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1091, i64* %arrayidx1092, align 16
  %arrayidx1093 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %511 = load i64, i64* %arrayidx1093, align 16
  %arrayidx1094 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %512 = load i64, i64* %arrayidx1094, align 16
  %xor1095 = xor i64 %511, %512
  %call1096 = call i64 @rotr64(i64 %xor1095, i32 32)
  %arrayidx1097 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1096, i64* %arrayidx1097, align 16
  %arrayidx1098 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %513 = load i64, i64* %arrayidx1098, align 16
  %arrayidx1099 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %514 = load i64, i64* %arrayidx1099, align 16
  %add1100 = add i64 %513, %514
  %arrayidx1101 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1100, i64* %arrayidx1101, align 16
  %arrayidx1102 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %515 = load i64, i64* %arrayidx1102, align 16
  %arrayidx1103 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %516 = load i64, i64* %arrayidx1103, align 16
  %xor1104 = xor i64 %515, %516
  %call1105 = call i64 @rotr64(i64 %xor1104, i32 24)
  %arrayidx1106 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1105, i64* %arrayidx1106, align 16
  %arrayidx1107 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %517 = load i64, i64* %arrayidx1107, align 16
  %arrayidx1108 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %518 = load i64, i64* %arrayidx1108, align 16
  %add1109 = add i64 %517, %518
  %519 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 1), align 1
  %idxprom1110 = zext i8 %519 to i32
  %arrayidx1111 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1110
  %520 = load i64, i64* %arrayidx1111, align 8
  %add1112 = add i64 %add1109, %520
  %arrayidx1113 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1112, i64* %arrayidx1113, align 16
  %arrayidx1114 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %521 = load i64, i64* %arrayidx1114, align 16
  %arrayidx1115 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %522 = load i64, i64* %arrayidx1115, align 16
  %xor1116 = xor i64 %521, %522
  %call1117 = call i64 @rotr64(i64 %xor1116, i32 16)
  %arrayidx1118 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1117, i64* %arrayidx1118, align 16
  %arrayidx1119 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %523 = load i64, i64* %arrayidx1119, align 16
  %arrayidx1120 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %524 = load i64, i64* %arrayidx1120, align 16
  %add1121 = add i64 %523, %524
  %arrayidx1122 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1121, i64* %arrayidx1122, align 16
  %arrayidx1123 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %525 = load i64, i64* %arrayidx1123, align 16
  %arrayidx1124 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %526 = load i64, i64* %arrayidx1124, align 16
  %xor1125 = xor i64 %525, %526
  %call1126 = call i64 @rotr64(i64 %xor1125, i32 63)
  %arrayidx1127 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1126, i64* %arrayidx1127, align 16
  br label %do.end1128

do.end1128:                                       ; preds = %do.body1085
  br label %do.body1129

do.body1129:                                      ; preds = %do.end1128
  %arrayidx1130 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %527 = load i64, i64* %arrayidx1130, align 8
  %arrayidx1131 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %528 = load i64, i64* %arrayidx1131, align 8
  %add1132 = add i64 %527, %528
  %529 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 2), align 2
  %idxprom1133 = zext i8 %529 to i32
  %arrayidx1134 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1133
  %530 = load i64, i64* %arrayidx1134, align 8
  %add1135 = add i64 %add1132, %530
  %arrayidx1136 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1135, i64* %arrayidx1136, align 8
  %arrayidx1137 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %531 = load i64, i64* %arrayidx1137, align 8
  %arrayidx1138 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %532 = load i64, i64* %arrayidx1138, align 8
  %xor1139 = xor i64 %531, %532
  %call1140 = call i64 @rotr64(i64 %xor1139, i32 32)
  %arrayidx1141 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1140, i64* %arrayidx1141, align 8
  %arrayidx1142 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %533 = load i64, i64* %arrayidx1142, align 8
  %arrayidx1143 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %534 = load i64, i64* %arrayidx1143, align 8
  %add1144 = add i64 %533, %534
  %arrayidx1145 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1144, i64* %arrayidx1145, align 8
  %arrayidx1146 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %535 = load i64, i64* %arrayidx1146, align 8
  %arrayidx1147 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %536 = load i64, i64* %arrayidx1147, align 8
  %xor1148 = xor i64 %535, %536
  %call1149 = call i64 @rotr64(i64 %xor1148, i32 24)
  %arrayidx1150 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1149, i64* %arrayidx1150, align 8
  %arrayidx1151 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %537 = load i64, i64* %arrayidx1151, align 8
  %arrayidx1152 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %538 = load i64, i64* %arrayidx1152, align 8
  %add1153 = add i64 %537, %538
  %539 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 3), align 1
  %idxprom1154 = zext i8 %539 to i32
  %arrayidx1155 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1154
  %540 = load i64, i64* %arrayidx1155, align 8
  %add1156 = add i64 %add1153, %540
  %arrayidx1157 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1156, i64* %arrayidx1157, align 8
  %arrayidx1158 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %541 = load i64, i64* %arrayidx1158, align 8
  %arrayidx1159 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %542 = load i64, i64* %arrayidx1159, align 8
  %xor1160 = xor i64 %541, %542
  %call1161 = call i64 @rotr64(i64 %xor1160, i32 16)
  %arrayidx1162 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1161, i64* %arrayidx1162, align 8
  %arrayidx1163 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %543 = load i64, i64* %arrayidx1163, align 8
  %arrayidx1164 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %544 = load i64, i64* %arrayidx1164, align 8
  %add1165 = add i64 %543, %544
  %arrayidx1166 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1165, i64* %arrayidx1166, align 8
  %arrayidx1167 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %545 = load i64, i64* %arrayidx1167, align 8
  %arrayidx1168 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %546 = load i64, i64* %arrayidx1168, align 8
  %xor1169 = xor i64 %545, %546
  %call1170 = call i64 @rotr64(i64 %xor1169, i32 63)
  %arrayidx1171 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1170, i64* %arrayidx1171, align 8
  br label %do.end1172

do.end1172:                                       ; preds = %do.body1129
  br label %do.body1173

do.body1173:                                      ; preds = %do.end1172
  %arrayidx1174 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %547 = load i64, i64* %arrayidx1174, align 16
  %arrayidx1175 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %548 = load i64, i64* %arrayidx1175, align 16
  %add1176 = add i64 %547, %548
  %549 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 4), align 4
  %idxprom1177 = zext i8 %549 to i32
  %arrayidx1178 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1177
  %550 = load i64, i64* %arrayidx1178, align 8
  %add1179 = add i64 %add1176, %550
  %arrayidx1180 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1179, i64* %arrayidx1180, align 16
  %arrayidx1181 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %551 = load i64, i64* %arrayidx1181, align 16
  %arrayidx1182 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %552 = load i64, i64* %arrayidx1182, align 16
  %xor1183 = xor i64 %551, %552
  %call1184 = call i64 @rotr64(i64 %xor1183, i32 32)
  %arrayidx1185 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1184, i64* %arrayidx1185, align 16
  %arrayidx1186 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %553 = load i64, i64* %arrayidx1186, align 16
  %arrayidx1187 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %554 = load i64, i64* %arrayidx1187, align 16
  %add1188 = add i64 %553, %554
  %arrayidx1189 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1188, i64* %arrayidx1189, align 16
  %arrayidx1190 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %555 = load i64, i64* %arrayidx1190, align 16
  %arrayidx1191 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %556 = load i64, i64* %arrayidx1191, align 16
  %xor1192 = xor i64 %555, %556
  %call1193 = call i64 @rotr64(i64 %xor1192, i32 24)
  %arrayidx1194 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1193, i64* %arrayidx1194, align 16
  %arrayidx1195 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %557 = load i64, i64* %arrayidx1195, align 16
  %arrayidx1196 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %558 = load i64, i64* %arrayidx1196, align 16
  %add1197 = add i64 %557, %558
  %559 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 5), align 1
  %idxprom1198 = zext i8 %559 to i32
  %arrayidx1199 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1198
  %560 = load i64, i64* %arrayidx1199, align 8
  %add1200 = add i64 %add1197, %560
  %arrayidx1201 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1200, i64* %arrayidx1201, align 16
  %arrayidx1202 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %561 = load i64, i64* %arrayidx1202, align 16
  %arrayidx1203 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %562 = load i64, i64* %arrayidx1203, align 16
  %xor1204 = xor i64 %561, %562
  %call1205 = call i64 @rotr64(i64 %xor1204, i32 16)
  %arrayidx1206 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1205, i64* %arrayidx1206, align 16
  %arrayidx1207 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %563 = load i64, i64* %arrayidx1207, align 16
  %arrayidx1208 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %564 = load i64, i64* %arrayidx1208, align 16
  %add1209 = add i64 %563, %564
  %arrayidx1210 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1209, i64* %arrayidx1210, align 16
  %arrayidx1211 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %565 = load i64, i64* %arrayidx1211, align 16
  %arrayidx1212 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %566 = load i64, i64* %arrayidx1212, align 16
  %xor1213 = xor i64 %565, %566
  %call1214 = call i64 @rotr64(i64 %xor1213, i32 63)
  %arrayidx1215 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1214, i64* %arrayidx1215, align 16
  br label %do.end1216

do.end1216:                                       ; preds = %do.body1173
  br label %do.body1217

do.body1217:                                      ; preds = %do.end1216
  %arrayidx1218 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %567 = load i64, i64* %arrayidx1218, align 8
  %arrayidx1219 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %568 = load i64, i64* %arrayidx1219, align 8
  %add1220 = add i64 %567, %568
  %569 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 6), align 2
  %idxprom1221 = zext i8 %569 to i32
  %arrayidx1222 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1221
  %570 = load i64, i64* %arrayidx1222, align 8
  %add1223 = add i64 %add1220, %570
  %arrayidx1224 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1223, i64* %arrayidx1224, align 8
  %arrayidx1225 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %571 = load i64, i64* %arrayidx1225, align 8
  %arrayidx1226 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %572 = load i64, i64* %arrayidx1226, align 8
  %xor1227 = xor i64 %571, %572
  %call1228 = call i64 @rotr64(i64 %xor1227, i32 32)
  %arrayidx1229 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1228, i64* %arrayidx1229, align 8
  %arrayidx1230 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %573 = load i64, i64* %arrayidx1230, align 8
  %arrayidx1231 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %574 = load i64, i64* %arrayidx1231, align 8
  %add1232 = add i64 %573, %574
  %arrayidx1233 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1232, i64* %arrayidx1233, align 8
  %arrayidx1234 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %575 = load i64, i64* %arrayidx1234, align 8
  %arrayidx1235 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %576 = load i64, i64* %arrayidx1235, align 8
  %xor1236 = xor i64 %575, %576
  %call1237 = call i64 @rotr64(i64 %xor1236, i32 24)
  %arrayidx1238 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1237, i64* %arrayidx1238, align 8
  %arrayidx1239 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %577 = load i64, i64* %arrayidx1239, align 8
  %arrayidx1240 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %578 = load i64, i64* %arrayidx1240, align 8
  %add1241 = add i64 %577, %578
  %579 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 7), align 1
  %idxprom1242 = zext i8 %579 to i32
  %arrayidx1243 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1242
  %580 = load i64, i64* %arrayidx1243, align 8
  %add1244 = add i64 %add1241, %580
  %arrayidx1245 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1244, i64* %arrayidx1245, align 8
  %arrayidx1246 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %581 = load i64, i64* %arrayidx1246, align 8
  %arrayidx1247 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %582 = load i64, i64* %arrayidx1247, align 8
  %xor1248 = xor i64 %581, %582
  %call1249 = call i64 @rotr64(i64 %xor1248, i32 16)
  %arrayidx1250 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1249, i64* %arrayidx1250, align 8
  %arrayidx1251 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %583 = load i64, i64* %arrayidx1251, align 8
  %arrayidx1252 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %584 = load i64, i64* %arrayidx1252, align 8
  %add1253 = add i64 %583, %584
  %arrayidx1254 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1253, i64* %arrayidx1254, align 8
  %arrayidx1255 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %585 = load i64, i64* %arrayidx1255, align 8
  %arrayidx1256 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %586 = load i64, i64* %arrayidx1256, align 8
  %xor1257 = xor i64 %585, %586
  %call1258 = call i64 @rotr64(i64 %xor1257, i32 63)
  %arrayidx1259 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1258, i64* %arrayidx1259, align 8
  br label %do.end1260

do.end1260:                                       ; preds = %do.body1217
  br label %do.body1261

do.body1261:                                      ; preds = %do.end1260
  %arrayidx1262 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %587 = load i64, i64* %arrayidx1262, align 16
  %arrayidx1263 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %588 = load i64, i64* %arrayidx1263, align 8
  %add1264 = add i64 %587, %588
  %589 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 8), align 8
  %idxprom1265 = zext i8 %589 to i32
  %arrayidx1266 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1265
  %590 = load i64, i64* %arrayidx1266, align 8
  %add1267 = add i64 %add1264, %590
  %arrayidx1268 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1267, i64* %arrayidx1268, align 16
  %arrayidx1269 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %591 = load i64, i64* %arrayidx1269, align 8
  %arrayidx1270 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %592 = load i64, i64* %arrayidx1270, align 16
  %xor1271 = xor i64 %591, %592
  %call1272 = call i64 @rotr64(i64 %xor1271, i32 32)
  %arrayidx1273 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1272, i64* %arrayidx1273, align 8
  %arrayidx1274 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %593 = load i64, i64* %arrayidx1274, align 16
  %arrayidx1275 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %594 = load i64, i64* %arrayidx1275, align 8
  %add1276 = add i64 %593, %594
  %arrayidx1277 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1276, i64* %arrayidx1277, align 16
  %arrayidx1278 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %595 = load i64, i64* %arrayidx1278, align 8
  %arrayidx1279 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %596 = load i64, i64* %arrayidx1279, align 16
  %xor1280 = xor i64 %595, %596
  %call1281 = call i64 @rotr64(i64 %xor1280, i32 24)
  %arrayidx1282 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1281, i64* %arrayidx1282, align 8
  %arrayidx1283 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %597 = load i64, i64* %arrayidx1283, align 16
  %arrayidx1284 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %598 = load i64, i64* %arrayidx1284, align 8
  %add1285 = add i64 %597, %598
  %599 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 9), align 1
  %idxprom1286 = zext i8 %599 to i32
  %arrayidx1287 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1286
  %600 = load i64, i64* %arrayidx1287, align 8
  %add1288 = add i64 %add1285, %600
  %arrayidx1289 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1288, i64* %arrayidx1289, align 16
  %arrayidx1290 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %601 = load i64, i64* %arrayidx1290, align 8
  %arrayidx1291 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %602 = load i64, i64* %arrayidx1291, align 16
  %xor1292 = xor i64 %601, %602
  %call1293 = call i64 @rotr64(i64 %xor1292, i32 16)
  %arrayidx1294 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1293, i64* %arrayidx1294, align 8
  %arrayidx1295 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %603 = load i64, i64* %arrayidx1295, align 16
  %arrayidx1296 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %604 = load i64, i64* %arrayidx1296, align 8
  %add1297 = add i64 %603, %604
  %arrayidx1298 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1297, i64* %arrayidx1298, align 16
  %arrayidx1299 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %605 = load i64, i64* %arrayidx1299, align 8
  %arrayidx1300 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %606 = load i64, i64* %arrayidx1300, align 16
  %xor1301 = xor i64 %605, %606
  %call1302 = call i64 @rotr64(i64 %xor1301, i32 63)
  %arrayidx1303 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1302, i64* %arrayidx1303, align 8
  br label %do.end1304

do.end1304:                                       ; preds = %do.body1261
  br label %do.body1305

do.body1305:                                      ; preds = %do.end1304
  %arrayidx1306 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %607 = load i64, i64* %arrayidx1306, align 8
  %arrayidx1307 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %608 = load i64, i64* %arrayidx1307, align 16
  %add1308 = add i64 %607, %608
  %609 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 10), align 2
  %idxprom1309 = zext i8 %609 to i32
  %arrayidx1310 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1309
  %610 = load i64, i64* %arrayidx1310, align 8
  %add1311 = add i64 %add1308, %610
  %arrayidx1312 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1311, i64* %arrayidx1312, align 8
  %arrayidx1313 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %611 = load i64, i64* %arrayidx1313, align 16
  %arrayidx1314 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %612 = load i64, i64* %arrayidx1314, align 8
  %xor1315 = xor i64 %611, %612
  %call1316 = call i64 @rotr64(i64 %xor1315, i32 32)
  %arrayidx1317 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1316, i64* %arrayidx1317, align 16
  %arrayidx1318 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %613 = load i64, i64* %arrayidx1318, align 8
  %arrayidx1319 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %614 = load i64, i64* %arrayidx1319, align 16
  %add1320 = add i64 %613, %614
  %arrayidx1321 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1320, i64* %arrayidx1321, align 8
  %arrayidx1322 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %615 = load i64, i64* %arrayidx1322, align 16
  %arrayidx1323 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %616 = load i64, i64* %arrayidx1323, align 8
  %xor1324 = xor i64 %615, %616
  %call1325 = call i64 @rotr64(i64 %xor1324, i32 24)
  %arrayidx1326 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1325, i64* %arrayidx1326, align 16
  %arrayidx1327 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %617 = load i64, i64* %arrayidx1327, align 8
  %arrayidx1328 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %618 = load i64, i64* %arrayidx1328, align 16
  %add1329 = add i64 %617, %618
  %619 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 11), align 1
  %idxprom1330 = zext i8 %619 to i32
  %arrayidx1331 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1330
  %620 = load i64, i64* %arrayidx1331, align 8
  %add1332 = add i64 %add1329, %620
  %arrayidx1333 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1332, i64* %arrayidx1333, align 8
  %arrayidx1334 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %621 = load i64, i64* %arrayidx1334, align 16
  %arrayidx1335 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %622 = load i64, i64* %arrayidx1335, align 8
  %xor1336 = xor i64 %621, %622
  %call1337 = call i64 @rotr64(i64 %xor1336, i32 16)
  %arrayidx1338 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1337, i64* %arrayidx1338, align 16
  %arrayidx1339 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %623 = load i64, i64* %arrayidx1339, align 8
  %arrayidx1340 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %624 = load i64, i64* %arrayidx1340, align 16
  %add1341 = add i64 %623, %624
  %arrayidx1342 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1341, i64* %arrayidx1342, align 8
  %arrayidx1343 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %625 = load i64, i64* %arrayidx1343, align 16
  %arrayidx1344 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %626 = load i64, i64* %arrayidx1344, align 8
  %xor1345 = xor i64 %625, %626
  %call1346 = call i64 @rotr64(i64 %xor1345, i32 63)
  %arrayidx1347 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1346, i64* %arrayidx1347, align 16
  br label %do.end1348

do.end1348:                                       ; preds = %do.body1305
  br label %do.body1349

do.body1349:                                      ; preds = %do.end1348
  %arrayidx1350 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %627 = load i64, i64* %arrayidx1350, align 16
  %arrayidx1351 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %628 = load i64, i64* %arrayidx1351, align 8
  %add1352 = add i64 %627, %628
  %629 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 12), align 4
  %idxprom1353 = zext i8 %629 to i32
  %arrayidx1354 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1353
  %630 = load i64, i64* %arrayidx1354, align 8
  %add1355 = add i64 %add1352, %630
  %arrayidx1356 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1355, i64* %arrayidx1356, align 16
  %arrayidx1357 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %631 = load i64, i64* %arrayidx1357, align 8
  %arrayidx1358 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %632 = load i64, i64* %arrayidx1358, align 16
  %xor1359 = xor i64 %631, %632
  %call1360 = call i64 @rotr64(i64 %xor1359, i32 32)
  %arrayidx1361 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1360, i64* %arrayidx1361, align 8
  %arrayidx1362 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %633 = load i64, i64* %arrayidx1362, align 16
  %arrayidx1363 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %634 = load i64, i64* %arrayidx1363, align 8
  %add1364 = add i64 %633, %634
  %arrayidx1365 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1364, i64* %arrayidx1365, align 16
  %arrayidx1366 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %635 = load i64, i64* %arrayidx1366, align 8
  %arrayidx1367 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %636 = load i64, i64* %arrayidx1367, align 16
  %xor1368 = xor i64 %635, %636
  %call1369 = call i64 @rotr64(i64 %xor1368, i32 24)
  %arrayidx1370 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1369, i64* %arrayidx1370, align 8
  %arrayidx1371 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %637 = load i64, i64* %arrayidx1371, align 16
  %arrayidx1372 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %638 = load i64, i64* %arrayidx1372, align 8
  %add1373 = add i64 %637, %638
  %639 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 13), align 1
  %idxprom1374 = zext i8 %639 to i32
  %arrayidx1375 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1374
  %640 = load i64, i64* %arrayidx1375, align 8
  %add1376 = add i64 %add1373, %640
  %arrayidx1377 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1376, i64* %arrayidx1377, align 16
  %arrayidx1378 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %641 = load i64, i64* %arrayidx1378, align 8
  %arrayidx1379 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %642 = load i64, i64* %arrayidx1379, align 16
  %xor1380 = xor i64 %641, %642
  %call1381 = call i64 @rotr64(i64 %xor1380, i32 16)
  %arrayidx1382 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1381, i64* %arrayidx1382, align 8
  %arrayidx1383 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %643 = load i64, i64* %arrayidx1383, align 16
  %arrayidx1384 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %644 = load i64, i64* %arrayidx1384, align 8
  %add1385 = add i64 %643, %644
  %arrayidx1386 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1385, i64* %arrayidx1386, align 16
  %arrayidx1387 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %645 = load i64, i64* %arrayidx1387, align 8
  %arrayidx1388 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %646 = load i64, i64* %arrayidx1388, align 16
  %xor1389 = xor i64 %645, %646
  %call1390 = call i64 @rotr64(i64 %xor1389, i32 63)
  %arrayidx1391 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1390, i64* %arrayidx1391, align 8
  br label %do.end1392

do.end1392:                                       ; preds = %do.body1349
  br label %do.body1393

do.body1393:                                      ; preds = %do.end1392
  %arrayidx1394 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %647 = load i64, i64* %arrayidx1394, align 8
  %arrayidx1395 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %648 = load i64, i64* %arrayidx1395, align 16
  %add1396 = add i64 %647, %648
  %649 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 14), align 2
  %idxprom1397 = zext i8 %649 to i32
  %arrayidx1398 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1397
  %650 = load i64, i64* %arrayidx1398, align 8
  %add1399 = add i64 %add1396, %650
  %arrayidx1400 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1399, i64* %arrayidx1400, align 8
  %arrayidx1401 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %651 = load i64, i64* %arrayidx1401, align 16
  %arrayidx1402 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %652 = load i64, i64* %arrayidx1402, align 8
  %xor1403 = xor i64 %651, %652
  %call1404 = call i64 @rotr64(i64 %xor1403, i32 32)
  %arrayidx1405 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1404, i64* %arrayidx1405, align 16
  %arrayidx1406 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %653 = load i64, i64* %arrayidx1406, align 8
  %arrayidx1407 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %654 = load i64, i64* %arrayidx1407, align 16
  %add1408 = add i64 %653, %654
  %arrayidx1409 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1408, i64* %arrayidx1409, align 8
  %arrayidx1410 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %655 = load i64, i64* %arrayidx1410, align 16
  %arrayidx1411 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %656 = load i64, i64* %arrayidx1411, align 8
  %xor1412 = xor i64 %655, %656
  %call1413 = call i64 @rotr64(i64 %xor1412, i32 24)
  %arrayidx1414 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1413, i64* %arrayidx1414, align 16
  %arrayidx1415 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %657 = load i64, i64* %arrayidx1415, align 8
  %arrayidx1416 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %658 = load i64, i64* %arrayidx1416, align 16
  %add1417 = add i64 %657, %658
  %659 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 15), align 1
  %idxprom1418 = zext i8 %659 to i32
  %arrayidx1419 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1418
  %660 = load i64, i64* %arrayidx1419, align 8
  %add1420 = add i64 %add1417, %660
  %arrayidx1421 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1420, i64* %arrayidx1421, align 8
  %arrayidx1422 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %661 = load i64, i64* %arrayidx1422, align 16
  %arrayidx1423 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %662 = load i64, i64* %arrayidx1423, align 8
  %xor1424 = xor i64 %661, %662
  %call1425 = call i64 @rotr64(i64 %xor1424, i32 16)
  %arrayidx1426 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1425, i64* %arrayidx1426, align 16
  %arrayidx1427 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %663 = load i64, i64* %arrayidx1427, align 8
  %arrayidx1428 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %664 = load i64, i64* %arrayidx1428, align 16
  %add1429 = add i64 %663, %664
  %arrayidx1430 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1429, i64* %arrayidx1430, align 8
  %arrayidx1431 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %665 = load i64, i64* %arrayidx1431, align 16
  %arrayidx1432 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %666 = load i64, i64* %arrayidx1432, align 8
  %xor1433 = xor i64 %665, %666
  %call1434 = call i64 @rotr64(i64 %xor1433, i32 63)
  %arrayidx1435 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1434, i64* %arrayidx1435, align 16
  br label %do.end1436

do.end1436:                                       ; preds = %do.body1393
  br label %do.end1437

do.end1437:                                       ; preds = %do.end1436
  br label %do.body1438

do.body1438:                                      ; preds = %do.end1437
  br label %do.body1439

do.body1439:                                      ; preds = %do.body1438
  %arrayidx1440 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %667 = load i64, i64* %arrayidx1440, align 16
  %arrayidx1441 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %668 = load i64, i64* %arrayidx1441, align 16
  %add1442 = add i64 %667, %668
  %669 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 0), align 16
  %idxprom1443 = zext i8 %669 to i32
  %arrayidx1444 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1443
  %670 = load i64, i64* %arrayidx1444, align 8
  %add1445 = add i64 %add1442, %670
  %arrayidx1446 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1445, i64* %arrayidx1446, align 16
  %arrayidx1447 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %671 = load i64, i64* %arrayidx1447, align 16
  %arrayidx1448 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %672 = load i64, i64* %arrayidx1448, align 16
  %xor1449 = xor i64 %671, %672
  %call1450 = call i64 @rotr64(i64 %xor1449, i32 32)
  %arrayidx1451 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1450, i64* %arrayidx1451, align 16
  %arrayidx1452 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %673 = load i64, i64* %arrayidx1452, align 16
  %arrayidx1453 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %674 = load i64, i64* %arrayidx1453, align 16
  %add1454 = add i64 %673, %674
  %arrayidx1455 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1454, i64* %arrayidx1455, align 16
  %arrayidx1456 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %675 = load i64, i64* %arrayidx1456, align 16
  %arrayidx1457 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %676 = load i64, i64* %arrayidx1457, align 16
  %xor1458 = xor i64 %675, %676
  %call1459 = call i64 @rotr64(i64 %xor1458, i32 24)
  %arrayidx1460 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1459, i64* %arrayidx1460, align 16
  %arrayidx1461 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %677 = load i64, i64* %arrayidx1461, align 16
  %arrayidx1462 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %678 = load i64, i64* %arrayidx1462, align 16
  %add1463 = add i64 %677, %678
  %679 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 1), align 1
  %idxprom1464 = zext i8 %679 to i32
  %arrayidx1465 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1464
  %680 = load i64, i64* %arrayidx1465, align 8
  %add1466 = add i64 %add1463, %680
  %arrayidx1467 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1466, i64* %arrayidx1467, align 16
  %arrayidx1468 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %681 = load i64, i64* %arrayidx1468, align 16
  %arrayidx1469 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %682 = load i64, i64* %arrayidx1469, align 16
  %xor1470 = xor i64 %681, %682
  %call1471 = call i64 @rotr64(i64 %xor1470, i32 16)
  %arrayidx1472 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1471, i64* %arrayidx1472, align 16
  %arrayidx1473 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %683 = load i64, i64* %arrayidx1473, align 16
  %arrayidx1474 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %684 = load i64, i64* %arrayidx1474, align 16
  %add1475 = add i64 %683, %684
  %arrayidx1476 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1475, i64* %arrayidx1476, align 16
  %arrayidx1477 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %685 = load i64, i64* %arrayidx1477, align 16
  %arrayidx1478 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %686 = load i64, i64* %arrayidx1478, align 16
  %xor1479 = xor i64 %685, %686
  %call1480 = call i64 @rotr64(i64 %xor1479, i32 63)
  %arrayidx1481 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1480, i64* %arrayidx1481, align 16
  br label %do.end1482

do.end1482:                                       ; preds = %do.body1439
  br label %do.body1483

do.body1483:                                      ; preds = %do.end1482
  %arrayidx1484 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %687 = load i64, i64* %arrayidx1484, align 8
  %arrayidx1485 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %688 = load i64, i64* %arrayidx1485, align 8
  %add1486 = add i64 %687, %688
  %689 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 2), align 2
  %idxprom1487 = zext i8 %689 to i32
  %arrayidx1488 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1487
  %690 = load i64, i64* %arrayidx1488, align 8
  %add1489 = add i64 %add1486, %690
  %arrayidx1490 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1489, i64* %arrayidx1490, align 8
  %arrayidx1491 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %691 = load i64, i64* %arrayidx1491, align 8
  %arrayidx1492 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %692 = load i64, i64* %arrayidx1492, align 8
  %xor1493 = xor i64 %691, %692
  %call1494 = call i64 @rotr64(i64 %xor1493, i32 32)
  %arrayidx1495 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1494, i64* %arrayidx1495, align 8
  %arrayidx1496 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %693 = load i64, i64* %arrayidx1496, align 8
  %arrayidx1497 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %694 = load i64, i64* %arrayidx1497, align 8
  %add1498 = add i64 %693, %694
  %arrayidx1499 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1498, i64* %arrayidx1499, align 8
  %arrayidx1500 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %695 = load i64, i64* %arrayidx1500, align 8
  %arrayidx1501 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %696 = load i64, i64* %arrayidx1501, align 8
  %xor1502 = xor i64 %695, %696
  %call1503 = call i64 @rotr64(i64 %xor1502, i32 24)
  %arrayidx1504 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1503, i64* %arrayidx1504, align 8
  %arrayidx1505 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %697 = load i64, i64* %arrayidx1505, align 8
  %arrayidx1506 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %698 = load i64, i64* %arrayidx1506, align 8
  %add1507 = add i64 %697, %698
  %699 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 3), align 1
  %idxprom1508 = zext i8 %699 to i32
  %arrayidx1509 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1508
  %700 = load i64, i64* %arrayidx1509, align 8
  %add1510 = add i64 %add1507, %700
  %arrayidx1511 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1510, i64* %arrayidx1511, align 8
  %arrayidx1512 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %701 = load i64, i64* %arrayidx1512, align 8
  %arrayidx1513 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %702 = load i64, i64* %arrayidx1513, align 8
  %xor1514 = xor i64 %701, %702
  %call1515 = call i64 @rotr64(i64 %xor1514, i32 16)
  %arrayidx1516 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1515, i64* %arrayidx1516, align 8
  %arrayidx1517 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %703 = load i64, i64* %arrayidx1517, align 8
  %arrayidx1518 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %704 = load i64, i64* %arrayidx1518, align 8
  %add1519 = add i64 %703, %704
  %arrayidx1520 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1519, i64* %arrayidx1520, align 8
  %arrayidx1521 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %705 = load i64, i64* %arrayidx1521, align 8
  %arrayidx1522 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %706 = load i64, i64* %arrayidx1522, align 8
  %xor1523 = xor i64 %705, %706
  %call1524 = call i64 @rotr64(i64 %xor1523, i32 63)
  %arrayidx1525 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1524, i64* %arrayidx1525, align 8
  br label %do.end1526

do.end1526:                                       ; preds = %do.body1483
  br label %do.body1527

do.body1527:                                      ; preds = %do.end1526
  %arrayidx1528 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %707 = load i64, i64* %arrayidx1528, align 16
  %arrayidx1529 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %708 = load i64, i64* %arrayidx1529, align 16
  %add1530 = add i64 %707, %708
  %709 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 4), align 4
  %idxprom1531 = zext i8 %709 to i32
  %arrayidx1532 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1531
  %710 = load i64, i64* %arrayidx1532, align 8
  %add1533 = add i64 %add1530, %710
  %arrayidx1534 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1533, i64* %arrayidx1534, align 16
  %arrayidx1535 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %711 = load i64, i64* %arrayidx1535, align 16
  %arrayidx1536 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %712 = load i64, i64* %arrayidx1536, align 16
  %xor1537 = xor i64 %711, %712
  %call1538 = call i64 @rotr64(i64 %xor1537, i32 32)
  %arrayidx1539 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1538, i64* %arrayidx1539, align 16
  %arrayidx1540 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %713 = load i64, i64* %arrayidx1540, align 16
  %arrayidx1541 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %714 = load i64, i64* %arrayidx1541, align 16
  %add1542 = add i64 %713, %714
  %arrayidx1543 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1542, i64* %arrayidx1543, align 16
  %arrayidx1544 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %715 = load i64, i64* %arrayidx1544, align 16
  %arrayidx1545 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %716 = load i64, i64* %arrayidx1545, align 16
  %xor1546 = xor i64 %715, %716
  %call1547 = call i64 @rotr64(i64 %xor1546, i32 24)
  %arrayidx1548 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1547, i64* %arrayidx1548, align 16
  %arrayidx1549 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %717 = load i64, i64* %arrayidx1549, align 16
  %arrayidx1550 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %718 = load i64, i64* %arrayidx1550, align 16
  %add1551 = add i64 %717, %718
  %719 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 5), align 1
  %idxprom1552 = zext i8 %719 to i32
  %arrayidx1553 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1552
  %720 = load i64, i64* %arrayidx1553, align 8
  %add1554 = add i64 %add1551, %720
  %arrayidx1555 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1554, i64* %arrayidx1555, align 16
  %arrayidx1556 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %721 = load i64, i64* %arrayidx1556, align 16
  %arrayidx1557 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %722 = load i64, i64* %arrayidx1557, align 16
  %xor1558 = xor i64 %721, %722
  %call1559 = call i64 @rotr64(i64 %xor1558, i32 16)
  %arrayidx1560 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1559, i64* %arrayidx1560, align 16
  %arrayidx1561 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %723 = load i64, i64* %arrayidx1561, align 16
  %arrayidx1562 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %724 = load i64, i64* %arrayidx1562, align 16
  %add1563 = add i64 %723, %724
  %arrayidx1564 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1563, i64* %arrayidx1564, align 16
  %arrayidx1565 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %725 = load i64, i64* %arrayidx1565, align 16
  %arrayidx1566 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %726 = load i64, i64* %arrayidx1566, align 16
  %xor1567 = xor i64 %725, %726
  %call1568 = call i64 @rotr64(i64 %xor1567, i32 63)
  %arrayidx1569 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1568, i64* %arrayidx1569, align 16
  br label %do.end1570

do.end1570:                                       ; preds = %do.body1527
  br label %do.body1571

do.body1571:                                      ; preds = %do.end1570
  %arrayidx1572 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %727 = load i64, i64* %arrayidx1572, align 8
  %arrayidx1573 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %728 = load i64, i64* %arrayidx1573, align 8
  %add1574 = add i64 %727, %728
  %729 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 6), align 2
  %idxprom1575 = zext i8 %729 to i32
  %arrayidx1576 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1575
  %730 = load i64, i64* %arrayidx1576, align 8
  %add1577 = add i64 %add1574, %730
  %arrayidx1578 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1577, i64* %arrayidx1578, align 8
  %arrayidx1579 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %731 = load i64, i64* %arrayidx1579, align 8
  %arrayidx1580 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %732 = load i64, i64* %arrayidx1580, align 8
  %xor1581 = xor i64 %731, %732
  %call1582 = call i64 @rotr64(i64 %xor1581, i32 32)
  %arrayidx1583 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1582, i64* %arrayidx1583, align 8
  %arrayidx1584 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %733 = load i64, i64* %arrayidx1584, align 8
  %arrayidx1585 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %734 = load i64, i64* %arrayidx1585, align 8
  %add1586 = add i64 %733, %734
  %arrayidx1587 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1586, i64* %arrayidx1587, align 8
  %arrayidx1588 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %735 = load i64, i64* %arrayidx1588, align 8
  %arrayidx1589 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %736 = load i64, i64* %arrayidx1589, align 8
  %xor1590 = xor i64 %735, %736
  %call1591 = call i64 @rotr64(i64 %xor1590, i32 24)
  %arrayidx1592 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1591, i64* %arrayidx1592, align 8
  %arrayidx1593 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %737 = load i64, i64* %arrayidx1593, align 8
  %arrayidx1594 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %738 = load i64, i64* %arrayidx1594, align 8
  %add1595 = add i64 %737, %738
  %739 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 7), align 1
  %idxprom1596 = zext i8 %739 to i32
  %arrayidx1597 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1596
  %740 = load i64, i64* %arrayidx1597, align 8
  %add1598 = add i64 %add1595, %740
  %arrayidx1599 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1598, i64* %arrayidx1599, align 8
  %arrayidx1600 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %741 = load i64, i64* %arrayidx1600, align 8
  %arrayidx1601 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %742 = load i64, i64* %arrayidx1601, align 8
  %xor1602 = xor i64 %741, %742
  %call1603 = call i64 @rotr64(i64 %xor1602, i32 16)
  %arrayidx1604 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1603, i64* %arrayidx1604, align 8
  %arrayidx1605 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %743 = load i64, i64* %arrayidx1605, align 8
  %arrayidx1606 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %744 = load i64, i64* %arrayidx1606, align 8
  %add1607 = add i64 %743, %744
  %arrayidx1608 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1607, i64* %arrayidx1608, align 8
  %arrayidx1609 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %745 = load i64, i64* %arrayidx1609, align 8
  %arrayidx1610 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %746 = load i64, i64* %arrayidx1610, align 8
  %xor1611 = xor i64 %745, %746
  %call1612 = call i64 @rotr64(i64 %xor1611, i32 63)
  %arrayidx1613 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1612, i64* %arrayidx1613, align 8
  br label %do.end1614

do.end1614:                                       ; preds = %do.body1571
  br label %do.body1615

do.body1615:                                      ; preds = %do.end1614
  %arrayidx1616 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %747 = load i64, i64* %arrayidx1616, align 16
  %arrayidx1617 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %748 = load i64, i64* %arrayidx1617, align 8
  %add1618 = add i64 %747, %748
  %749 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 8), align 8
  %idxprom1619 = zext i8 %749 to i32
  %arrayidx1620 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1619
  %750 = load i64, i64* %arrayidx1620, align 8
  %add1621 = add i64 %add1618, %750
  %arrayidx1622 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1621, i64* %arrayidx1622, align 16
  %arrayidx1623 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %751 = load i64, i64* %arrayidx1623, align 8
  %arrayidx1624 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %752 = load i64, i64* %arrayidx1624, align 16
  %xor1625 = xor i64 %751, %752
  %call1626 = call i64 @rotr64(i64 %xor1625, i32 32)
  %arrayidx1627 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1626, i64* %arrayidx1627, align 8
  %arrayidx1628 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %753 = load i64, i64* %arrayidx1628, align 16
  %arrayidx1629 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %754 = load i64, i64* %arrayidx1629, align 8
  %add1630 = add i64 %753, %754
  %arrayidx1631 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1630, i64* %arrayidx1631, align 16
  %arrayidx1632 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %755 = load i64, i64* %arrayidx1632, align 8
  %arrayidx1633 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %756 = load i64, i64* %arrayidx1633, align 16
  %xor1634 = xor i64 %755, %756
  %call1635 = call i64 @rotr64(i64 %xor1634, i32 24)
  %arrayidx1636 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1635, i64* %arrayidx1636, align 8
  %arrayidx1637 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %757 = load i64, i64* %arrayidx1637, align 16
  %arrayidx1638 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %758 = load i64, i64* %arrayidx1638, align 8
  %add1639 = add i64 %757, %758
  %759 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 9), align 1
  %idxprom1640 = zext i8 %759 to i32
  %arrayidx1641 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1640
  %760 = load i64, i64* %arrayidx1641, align 8
  %add1642 = add i64 %add1639, %760
  %arrayidx1643 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1642, i64* %arrayidx1643, align 16
  %arrayidx1644 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %761 = load i64, i64* %arrayidx1644, align 8
  %arrayidx1645 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %762 = load i64, i64* %arrayidx1645, align 16
  %xor1646 = xor i64 %761, %762
  %call1647 = call i64 @rotr64(i64 %xor1646, i32 16)
  %arrayidx1648 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1647, i64* %arrayidx1648, align 8
  %arrayidx1649 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %763 = load i64, i64* %arrayidx1649, align 16
  %arrayidx1650 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %764 = load i64, i64* %arrayidx1650, align 8
  %add1651 = add i64 %763, %764
  %arrayidx1652 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1651, i64* %arrayidx1652, align 16
  %arrayidx1653 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %765 = load i64, i64* %arrayidx1653, align 8
  %arrayidx1654 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %766 = load i64, i64* %arrayidx1654, align 16
  %xor1655 = xor i64 %765, %766
  %call1656 = call i64 @rotr64(i64 %xor1655, i32 63)
  %arrayidx1657 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1656, i64* %arrayidx1657, align 8
  br label %do.end1658

do.end1658:                                       ; preds = %do.body1615
  br label %do.body1659

do.body1659:                                      ; preds = %do.end1658
  %arrayidx1660 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %767 = load i64, i64* %arrayidx1660, align 8
  %arrayidx1661 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %768 = load i64, i64* %arrayidx1661, align 16
  %add1662 = add i64 %767, %768
  %769 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 10), align 2
  %idxprom1663 = zext i8 %769 to i32
  %arrayidx1664 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1663
  %770 = load i64, i64* %arrayidx1664, align 8
  %add1665 = add i64 %add1662, %770
  %arrayidx1666 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1665, i64* %arrayidx1666, align 8
  %arrayidx1667 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %771 = load i64, i64* %arrayidx1667, align 16
  %arrayidx1668 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %772 = load i64, i64* %arrayidx1668, align 8
  %xor1669 = xor i64 %771, %772
  %call1670 = call i64 @rotr64(i64 %xor1669, i32 32)
  %arrayidx1671 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1670, i64* %arrayidx1671, align 16
  %arrayidx1672 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %773 = load i64, i64* %arrayidx1672, align 8
  %arrayidx1673 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %774 = load i64, i64* %arrayidx1673, align 16
  %add1674 = add i64 %773, %774
  %arrayidx1675 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1674, i64* %arrayidx1675, align 8
  %arrayidx1676 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %775 = load i64, i64* %arrayidx1676, align 16
  %arrayidx1677 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %776 = load i64, i64* %arrayidx1677, align 8
  %xor1678 = xor i64 %775, %776
  %call1679 = call i64 @rotr64(i64 %xor1678, i32 24)
  %arrayidx1680 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1679, i64* %arrayidx1680, align 16
  %arrayidx1681 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %777 = load i64, i64* %arrayidx1681, align 8
  %arrayidx1682 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %778 = load i64, i64* %arrayidx1682, align 16
  %add1683 = add i64 %777, %778
  %779 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 11), align 1
  %idxprom1684 = zext i8 %779 to i32
  %arrayidx1685 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1684
  %780 = load i64, i64* %arrayidx1685, align 8
  %add1686 = add i64 %add1683, %780
  %arrayidx1687 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1686, i64* %arrayidx1687, align 8
  %arrayidx1688 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %781 = load i64, i64* %arrayidx1688, align 16
  %arrayidx1689 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %782 = load i64, i64* %arrayidx1689, align 8
  %xor1690 = xor i64 %781, %782
  %call1691 = call i64 @rotr64(i64 %xor1690, i32 16)
  %arrayidx1692 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1691, i64* %arrayidx1692, align 16
  %arrayidx1693 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %783 = load i64, i64* %arrayidx1693, align 8
  %arrayidx1694 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %784 = load i64, i64* %arrayidx1694, align 16
  %add1695 = add i64 %783, %784
  %arrayidx1696 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1695, i64* %arrayidx1696, align 8
  %arrayidx1697 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %785 = load i64, i64* %arrayidx1697, align 16
  %arrayidx1698 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %786 = load i64, i64* %arrayidx1698, align 8
  %xor1699 = xor i64 %785, %786
  %call1700 = call i64 @rotr64(i64 %xor1699, i32 63)
  %arrayidx1701 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1700, i64* %arrayidx1701, align 16
  br label %do.end1702

do.end1702:                                       ; preds = %do.body1659
  br label %do.body1703

do.body1703:                                      ; preds = %do.end1702
  %arrayidx1704 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %787 = load i64, i64* %arrayidx1704, align 16
  %arrayidx1705 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %788 = load i64, i64* %arrayidx1705, align 8
  %add1706 = add i64 %787, %788
  %789 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 12), align 4
  %idxprom1707 = zext i8 %789 to i32
  %arrayidx1708 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1707
  %790 = load i64, i64* %arrayidx1708, align 8
  %add1709 = add i64 %add1706, %790
  %arrayidx1710 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1709, i64* %arrayidx1710, align 16
  %arrayidx1711 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %791 = load i64, i64* %arrayidx1711, align 8
  %arrayidx1712 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %792 = load i64, i64* %arrayidx1712, align 16
  %xor1713 = xor i64 %791, %792
  %call1714 = call i64 @rotr64(i64 %xor1713, i32 32)
  %arrayidx1715 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1714, i64* %arrayidx1715, align 8
  %arrayidx1716 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %793 = load i64, i64* %arrayidx1716, align 16
  %arrayidx1717 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %794 = load i64, i64* %arrayidx1717, align 8
  %add1718 = add i64 %793, %794
  %arrayidx1719 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1718, i64* %arrayidx1719, align 16
  %arrayidx1720 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %795 = load i64, i64* %arrayidx1720, align 8
  %arrayidx1721 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %796 = load i64, i64* %arrayidx1721, align 16
  %xor1722 = xor i64 %795, %796
  %call1723 = call i64 @rotr64(i64 %xor1722, i32 24)
  %arrayidx1724 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1723, i64* %arrayidx1724, align 8
  %arrayidx1725 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %797 = load i64, i64* %arrayidx1725, align 16
  %arrayidx1726 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %798 = load i64, i64* %arrayidx1726, align 8
  %add1727 = add i64 %797, %798
  %799 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 13), align 1
  %idxprom1728 = zext i8 %799 to i32
  %arrayidx1729 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1728
  %800 = load i64, i64* %arrayidx1729, align 8
  %add1730 = add i64 %add1727, %800
  %arrayidx1731 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1730, i64* %arrayidx1731, align 16
  %arrayidx1732 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %801 = load i64, i64* %arrayidx1732, align 8
  %arrayidx1733 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %802 = load i64, i64* %arrayidx1733, align 16
  %xor1734 = xor i64 %801, %802
  %call1735 = call i64 @rotr64(i64 %xor1734, i32 16)
  %arrayidx1736 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1735, i64* %arrayidx1736, align 8
  %arrayidx1737 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %803 = load i64, i64* %arrayidx1737, align 16
  %arrayidx1738 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %804 = load i64, i64* %arrayidx1738, align 8
  %add1739 = add i64 %803, %804
  %arrayidx1740 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1739, i64* %arrayidx1740, align 16
  %arrayidx1741 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %805 = load i64, i64* %arrayidx1741, align 8
  %arrayidx1742 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %806 = load i64, i64* %arrayidx1742, align 16
  %xor1743 = xor i64 %805, %806
  %call1744 = call i64 @rotr64(i64 %xor1743, i32 63)
  %arrayidx1745 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1744, i64* %arrayidx1745, align 8
  br label %do.end1746

do.end1746:                                       ; preds = %do.body1703
  br label %do.body1747

do.body1747:                                      ; preds = %do.end1746
  %arrayidx1748 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %807 = load i64, i64* %arrayidx1748, align 8
  %arrayidx1749 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %808 = load i64, i64* %arrayidx1749, align 16
  %add1750 = add i64 %807, %808
  %809 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 14), align 2
  %idxprom1751 = zext i8 %809 to i32
  %arrayidx1752 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1751
  %810 = load i64, i64* %arrayidx1752, align 8
  %add1753 = add i64 %add1750, %810
  %arrayidx1754 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1753, i64* %arrayidx1754, align 8
  %arrayidx1755 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %811 = load i64, i64* %arrayidx1755, align 16
  %arrayidx1756 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %812 = load i64, i64* %arrayidx1756, align 8
  %xor1757 = xor i64 %811, %812
  %call1758 = call i64 @rotr64(i64 %xor1757, i32 32)
  %arrayidx1759 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1758, i64* %arrayidx1759, align 16
  %arrayidx1760 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %813 = load i64, i64* %arrayidx1760, align 8
  %arrayidx1761 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %814 = load i64, i64* %arrayidx1761, align 16
  %add1762 = add i64 %813, %814
  %arrayidx1763 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1762, i64* %arrayidx1763, align 8
  %arrayidx1764 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %815 = load i64, i64* %arrayidx1764, align 16
  %arrayidx1765 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %816 = load i64, i64* %arrayidx1765, align 8
  %xor1766 = xor i64 %815, %816
  %call1767 = call i64 @rotr64(i64 %xor1766, i32 24)
  %arrayidx1768 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1767, i64* %arrayidx1768, align 16
  %arrayidx1769 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %817 = load i64, i64* %arrayidx1769, align 8
  %arrayidx1770 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %818 = load i64, i64* %arrayidx1770, align 16
  %add1771 = add i64 %817, %818
  %819 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 15), align 1
  %idxprom1772 = zext i8 %819 to i32
  %arrayidx1773 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1772
  %820 = load i64, i64* %arrayidx1773, align 8
  %add1774 = add i64 %add1771, %820
  %arrayidx1775 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1774, i64* %arrayidx1775, align 8
  %arrayidx1776 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %821 = load i64, i64* %arrayidx1776, align 16
  %arrayidx1777 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %822 = load i64, i64* %arrayidx1777, align 8
  %xor1778 = xor i64 %821, %822
  %call1779 = call i64 @rotr64(i64 %xor1778, i32 16)
  %arrayidx1780 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1779, i64* %arrayidx1780, align 16
  %arrayidx1781 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %823 = load i64, i64* %arrayidx1781, align 8
  %arrayidx1782 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %824 = load i64, i64* %arrayidx1782, align 16
  %add1783 = add i64 %823, %824
  %arrayidx1784 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1783, i64* %arrayidx1784, align 8
  %arrayidx1785 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %825 = load i64, i64* %arrayidx1785, align 16
  %arrayidx1786 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %826 = load i64, i64* %arrayidx1786, align 8
  %xor1787 = xor i64 %825, %826
  %call1788 = call i64 @rotr64(i64 %xor1787, i32 63)
  %arrayidx1789 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1788, i64* %arrayidx1789, align 16
  br label %do.end1790

do.end1790:                                       ; preds = %do.body1747
  br label %do.end1791

do.end1791:                                       ; preds = %do.end1790
  br label %do.body1792

do.body1792:                                      ; preds = %do.end1791
  br label %do.body1793

do.body1793:                                      ; preds = %do.body1792
  %arrayidx1794 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %827 = load i64, i64* %arrayidx1794, align 16
  %arrayidx1795 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %828 = load i64, i64* %arrayidx1795, align 16
  %add1796 = add i64 %827, %828
  %829 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 0), align 16
  %idxprom1797 = zext i8 %829 to i32
  %arrayidx1798 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1797
  %830 = load i64, i64* %arrayidx1798, align 8
  %add1799 = add i64 %add1796, %830
  %arrayidx1800 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1799, i64* %arrayidx1800, align 16
  %arrayidx1801 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %831 = load i64, i64* %arrayidx1801, align 16
  %arrayidx1802 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %832 = load i64, i64* %arrayidx1802, align 16
  %xor1803 = xor i64 %831, %832
  %call1804 = call i64 @rotr64(i64 %xor1803, i32 32)
  %arrayidx1805 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1804, i64* %arrayidx1805, align 16
  %arrayidx1806 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %833 = load i64, i64* %arrayidx1806, align 16
  %arrayidx1807 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %834 = load i64, i64* %arrayidx1807, align 16
  %add1808 = add i64 %833, %834
  %arrayidx1809 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1808, i64* %arrayidx1809, align 16
  %arrayidx1810 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %835 = load i64, i64* %arrayidx1810, align 16
  %arrayidx1811 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %836 = load i64, i64* %arrayidx1811, align 16
  %xor1812 = xor i64 %835, %836
  %call1813 = call i64 @rotr64(i64 %xor1812, i32 24)
  %arrayidx1814 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1813, i64* %arrayidx1814, align 16
  %arrayidx1815 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %837 = load i64, i64* %arrayidx1815, align 16
  %arrayidx1816 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %838 = load i64, i64* %arrayidx1816, align 16
  %add1817 = add i64 %837, %838
  %839 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 1), align 1
  %idxprom1818 = zext i8 %839 to i32
  %arrayidx1819 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1818
  %840 = load i64, i64* %arrayidx1819, align 8
  %add1820 = add i64 %add1817, %840
  %arrayidx1821 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1820, i64* %arrayidx1821, align 16
  %arrayidx1822 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %841 = load i64, i64* %arrayidx1822, align 16
  %arrayidx1823 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %842 = load i64, i64* %arrayidx1823, align 16
  %xor1824 = xor i64 %841, %842
  %call1825 = call i64 @rotr64(i64 %xor1824, i32 16)
  %arrayidx1826 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1825, i64* %arrayidx1826, align 16
  %arrayidx1827 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %843 = load i64, i64* %arrayidx1827, align 16
  %arrayidx1828 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %844 = load i64, i64* %arrayidx1828, align 16
  %add1829 = add i64 %843, %844
  %arrayidx1830 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add1829, i64* %arrayidx1830, align 16
  %arrayidx1831 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %845 = load i64, i64* %arrayidx1831, align 16
  %arrayidx1832 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %846 = load i64, i64* %arrayidx1832, align 16
  %xor1833 = xor i64 %845, %846
  %call1834 = call i64 @rotr64(i64 %xor1833, i32 63)
  %arrayidx1835 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1834, i64* %arrayidx1835, align 16
  br label %do.end1836

do.end1836:                                       ; preds = %do.body1793
  br label %do.body1837

do.body1837:                                      ; preds = %do.end1836
  %arrayidx1838 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %847 = load i64, i64* %arrayidx1838, align 8
  %arrayidx1839 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %848 = load i64, i64* %arrayidx1839, align 8
  %add1840 = add i64 %847, %848
  %849 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 2), align 2
  %idxprom1841 = zext i8 %849 to i32
  %arrayidx1842 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1841
  %850 = load i64, i64* %arrayidx1842, align 8
  %add1843 = add i64 %add1840, %850
  %arrayidx1844 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1843, i64* %arrayidx1844, align 8
  %arrayidx1845 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %851 = load i64, i64* %arrayidx1845, align 8
  %arrayidx1846 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %852 = load i64, i64* %arrayidx1846, align 8
  %xor1847 = xor i64 %851, %852
  %call1848 = call i64 @rotr64(i64 %xor1847, i32 32)
  %arrayidx1849 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1848, i64* %arrayidx1849, align 8
  %arrayidx1850 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %853 = load i64, i64* %arrayidx1850, align 8
  %arrayidx1851 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %854 = load i64, i64* %arrayidx1851, align 8
  %add1852 = add i64 %853, %854
  %arrayidx1853 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1852, i64* %arrayidx1853, align 8
  %arrayidx1854 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %855 = load i64, i64* %arrayidx1854, align 8
  %arrayidx1855 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %856 = load i64, i64* %arrayidx1855, align 8
  %xor1856 = xor i64 %855, %856
  %call1857 = call i64 @rotr64(i64 %xor1856, i32 24)
  %arrayidx1858 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1857, i64* %arrayidx1858, align 8
  %arrayidx1859 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %857 = load i64, i64* %arrayidx1859, align 8
  %arrayidx1860 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %858 = load i64, i64* %arrayidx1860, align 8
  %add1861 = add i64 %857, %858
  %859 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 3), align 1
  %idxprom1862 = zext i8 %859 to i32
  %arrayidx1863 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1862
  %860 = load i64, i64* %arrayidx1863, align 8
  %add1864 = add i64 %add1861, %860
  %arrayidx1865 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add1864, i64* %arrayidx1865, align 8
  %arrayidx1866 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %861 = load i64, i64* %arrayidx1866, align 8
  %arrayidx1867 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %862 = load i64, i64* %arrayidx1867, align 8
  %xor1868 = xor i64 %861, %862
  %call1869 = call i64 @rotr64(i64 %xor1868, i32 16)
  %arrayidx1870 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1869, i64* %arrayidx1870, align 8
  %arrayidx1871 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %863 = load i64, i64* %arrayidx1871, align 8
  %arrayidx1872 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %864 = load i64, i64* %arrayidx1872, align 8
  %add1873 = add i64 %863, %864
  %arrayidx1874 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add1873, i64* %arrayidx1874, align 8
  %arrayidx1875 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %865 = load i64, i64* %arrayidx1875, align 8
  %arrayidx1876 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %866 = load i64, i64* %arrayidx1876, align 8
  %xor1877 = xor i64 %865, %866
  %call1878 = call i64 @rotr64(i64 %xor1877, i32 63)
  %arrayidx1879 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1878, i64* %arrayidx1879, align 8
  br label %do.end1880

do.end1880:                                       ; preds = %do.body1837
  br label %do.body1881

do.body1881:                                      ; preds = %do.end1880
  %arrayidx1882 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %867 = load i64, i64* %arrayidx1882, align 16
  %arrayidx1883 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %868 = load i64, i64* %arrayidx1883, align 16
  %add1884 = add i64 %867, %868
  %869 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 4), align 4
  %idxprom1885 = zext i8 %869 to i32
  %arrayidx1886 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1885
  %870 = load i64, i64* %arrayidx1886, align 8
  %add1887 = add i64 %add1884, %870
  %arrayidx1888 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1887, i64* %arrayidx1888, align 16
  %arrayidx1889 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %871 = load i64, i64* %arrayidx1889, align 16
  %arrayidx1890 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %872 = load i64, i64* %arrayidx1890, align 16
  %xor1891 = xor i64 %871, %872
  %call1892 = call i64 @rotr64(i64 %xor1891, i32 32)
  %arrayidx1893 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1892, i64* %arrayidx1893, align 16
  %arrayidx1894 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %873 = load i64, i64* %arrayidx1894, align 16
  %arrayidx1895 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %874 = load i64, i64* %arrayidx1895, align 16
  %add1896 = add i64 %873, %874
  %arrayidx1897 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1896, i64* %arrayidx1897, align 16
  %arrayidx1898 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %875 = load i64, i64* %arrayidx1898, align 16
  %arrayidx1899 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %876 = load i64, i64* %arrayidx1899, align 16
  %xor1900 = xor i64 %875, %876
  %call1901 = call i64 @rotr64(i64 %xor1900, i32 24)
  %arrayidx1902 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1901, i64* %arrayidx1902, align 16
  %arrayidx1903 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %877 = load i64, i64* %arrayidx1903, align 16
  %arrayidx1904 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %878 = load i64, i64* %arrayidx1904, align 16
  %add1905 = add i64 %877, %878
  %879 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 5), align 1
  %idxprom1906 = zext i8 %879 to i32
  %arrayidx1907 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1906
  %880 = load i64, i64* %arrayidx1907, align 8
  %add1908 = add i64 %add1905, %880
  %arrayidx1909 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add1908, i64* %arrayidx1909, align 16
  %arrayidx1910 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %881 = load i64, i64* %arrayidx1910, align 16
  %arrayidx1911 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %882 = load i64, i64* %arrayidx1911, align 16
  %xor1912 = xor i64 %881, %882
  %call1913 = call i64 @rotr64(i64 %xor1912, i32 16)
  %arrayidx1914 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1913, i64* %arrayidx1914, align 16
  %arrayidx1915 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %883 = load i64, i64* %arrayidx1915, align 16
  %arrayidx1916 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %884 = load i64, i64* %arrayidx1916, align 16
  %add1917 = add i64 %883, %884
  %arrayidx1918 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1917, i64* %arrayidx1918, align 16
  %arrayidx1919 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %885 = load i64, i64* %arrayidx1919, align 16
  %arrayidx1920 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %886 = load i64, i64* %arrayidx1920, align 16
  %xor1921 = xor i64 %885, %886
  %call1922 = call i64 @rotr64(i64 %xor1921, i32 63)
  %arrayidx1923 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1922, i64* %arrayidx1923, align 16
  br label %do.end1924

do.end1924:                                       ; preds = %do.body1881
  br label %do.body1925

do.body1925:                                      ; preds = %do.end1924
  %arrayidx1926 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %887 = load i64, i64* %arrayidx1926, align 8
  %arrayidx1927 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %888 = load i64, i64* %arrayidx1927, align 8
  %add1928 = add i64 %887, %888
  %889 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 6), align 2
  %idxprom1929 = zext i8 %889 to i32
  %arrayidx1930 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1929
  %890 = load i64, i64* %arrayidx1930, align 8
  %add1931 = add i64 %add1928, %890
  %arrayidx1932 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1931, i64* %arrayidx1932, align 8
  %arrayidx1933 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %891 = load i64, i64* %arrayidx1933, align 8
  %arrayidx1934 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %892 = load i64, i64* %arrayidx1934, align 8
  %xor1935 = xor i64 %891, %892
  %call1936 = call i64 @rotr64(i64 %xor1935, i32 32)
  %arrayidx1937 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1936, i64* %arrayidx1937, align 8
  %arrayidx1938 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %893 = load i64, i64* %arrayidx1938, align 8
  %arrayidx1939 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %894 = load i64, i64* %arrayidx1939, align 8
  %add1940 = add i64 %893, %894
  %arrayidx1941 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1940, i64* %arrayidx1941, align 8
  %arrayidx1942 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %895 = load i64, i64* %arrayidx1942, align 8
  %arrayidx1943 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %896 = load i64, i64* %arrayidx1943, align 8
  %xor1944 = xor i64 %895, %896
  %call1945 = call i64 @rotr64(i64 %xor1944, i32 24)
  %arrayidx1946 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1945, i64* %arrayidx1946, align 8
  %arrayidx1947 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %897 = load i64, i64* %arrayidx1947, align 8
  %arrayidx1948 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %898 = load i64, i64* %arrayidx1948, align 8
  %add1949 = add i64 %897, %898
  %899 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 7), align 1
  %idxprom1950 = zext i8 %899 to i32
  %arrayidx1951 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1950
  %900 = load i64, i64* %arrayidx1951, align 8
  %add1952 = add i64 %add1949, %900
  %arrayidx1953 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add1952, i64* %arrayidx1953, align 8
  %arrayidx1954 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %901 = load i64, i64* %arrayidx1954, align 8
  %arrayidx1955 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %902 = load i64, i64* %arrayidx1955, align 8
  %xor1956 = xor i64 %901, %902
  %call1957 = call i64 @rotr64(i64 %xor1956, i32 16)
  %arrayidx1958 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1957, i64* %arrayidx1958, align 8
  %arrayidx1959 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %903 = load i64, i64* %arrayidx1959, align 8
  %arrayidx1960 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %904 = load i64, i64* %arrayidx1960, align 8
  %add1961 = add i64 %903, %904
  %arrayidx1962 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add1961, i64* %arrayidx1962, align 8
  %arrayidx1963 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %905 = load i64, i64* %arrayidx1963, align 8
  %arrayidx1964 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %906 = load i64, i64* %arrayidx1964, align 8
  %xor1965 = xor i64 %905, %906
  %call1966 = call i64 @rotr64(i64 %xor1965, i32 63)
  %arrayidx1967 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1966, i64* %arrayidx1967, align 8
  br label %do.end1968

do.end1968:                                       ; preds = %do.body1925
  br label %do.body1969

do.body1969:                                      ; preds = %do.end1968
  %arrayidx1970 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %907 = load i64, i64* %arrayidx1970, align 16
  %arrayidx1971 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %908 = load i64, i64* %arrayidx1971, align 8
  %add1972 = add i64 %907, %908
  %909 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 8), align 8
  %idxprom1973 = zext i8 %909 to i32
  %arrayidx1974 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1973
  %910 = load i64, i64* %arrayidx1974, align 8
  %add1975 = add i64 %add1972, %910
  %arrayidx1976 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1975, i64* %arrayidx1976, align 16
  %arrayidx1977 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %911 = load i64, i64* %arrayidx1977, align 8
  %arrayidx1978 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %912 = load i64, i64* %arrayidx1978, align 16
  %xor1979 = xor i64 %911, %912
  %call1980 = call i64 @rotr64(i64 %xor1979, i32 32)
  %arrayidx1981 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1980, i64* %arrayidx1981, align 8
  %arrayidx1982 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %913 = load i64, i64* %arrayidx1982, align 16
  %arrayidx1983 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %914 = load i64, i64* %arrayidx1983, align 8
  %add1984 = add i64 %913, %914
  %arrayidx1985 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add1984, i64* %arrayidx1985, align 16
  %arrayidx1986 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %915 = load i64, i64* %arrayidx1986, align 8
  %arrayidx1987 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %916 = load i64, i64* %arrayidx1987, align 16
  %xor1988 = xor i64 %915, %916
  %call1989 = call i64 @rotr64(i64 %xor1988, i32 24)
  %arrayidx1990 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1989, i64* %arrayidx1990, align 8
  %arrayidx1991 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %917 = load i64, i64* %arrayidx1991, align 16
  %arrayidx1992 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %918 = load i64, i64* %arrayidx1992, align 8
  %add1993 = add i64 %917, %918
  %919 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 9), align 1
  %idxprom1994 = zext i8 %919 to i32
  %arrayidx1995 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1994
  %920 = load i64, i64* %arrayidx1995, align 8
  %add1996 = add i64 %add1993, %920
  %arrayidx1997 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add1996, i64* %arrayidx1997, align 16
  %arrayidx1998 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %921 = load i64, i64* %arrayidx1998, align 8
  %arrayidx1999 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %922 = load i64, i64* %arrayidx1999, align 16
  %xor2000 = xor i64 %921, %922
  %call2001 = call i64 @rotr64(i64 %xor2000, i32 16)
  %arrayidx2002 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2001, i64* %arrayidx2002, align 8
  %arrayidx2003 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %923 = load i64, i64* %arrayidx2003, align 16
  %arrayidx2004 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %924 = load i64, i64* %arrayidx2004, align 8
  %add2005 = add i64 %923, %924
  %arrayidx2006 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2005, i64* %arrayidx2006, align 16
  %arrayidx2007 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %925 = load i64, i64* %arrayidx2007, align 8
  %arrayidx2008 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %926 = load i64, i64* %arrayidx2008, align 16
  %xor2009 = xor i64 %925, %926
  %call2010 = call i64 @rotr64(i64 %xor2009, i32 63)
  %arrayidx2011 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2010, i64* %arrayidx2011, align 8
  br label %do.end2012

do.end2012:                                       ; preds = %do.body1969
  br label %do.body2013

do.body2013:                                      ; preds = %do.end2012
  %arrayidx2014 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %927 = load i64, i64* %arrayidx2014, align 8
  %arrayidx2015 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %928 = load i64, i64* %arrayidx2015, align 16
  %add2016 = add i64 %927, %928
  %929 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 10), align 2
  %idxprom2017 = zext i8 %929 to i32
  %arrayidx2018 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2017
  %930 = load i64, i64* %arrayidx2018, align 8
  %add2019 = add i64 %add2016, %930
  %arrayidx2020 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2019, i64* %arrayidx2020, align 8
  %arrayidx2021 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %931 = load i64, i64* %arrayidx2021, align 16
  %arrayidx2022 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %932 = load i64, i64* %arrayidx2022, align 8
  %xor2023 = xor i64 %931, %932
  %call2024 = call i64 @rotr64(i64 %xor2023, i32 32)
  %arrayidx2025 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2024, i64* %arrayidx2025, align 16
  %arrayidx2026 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %933 = load i64, i64* %arrayidx2026, align 8
  %arrayidx2027 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %934 = load i64, i64* %arrayidx2027, align 16
  %add2028 = add i64 %933, %934
  %arrayidx2029 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2028, i64* %arrayidx2029, align 8
  %arrayidx2030 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %935 = load i64, i64* %arrayidx2030, align 16
  %arrayidx2031 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %936 = load i64, i64* %arrayidx2031, align 8
  %xor2032 = xor i64 %935, %936
  %call2033 = call i64 @rotr64(i64 %xor2032, i32 24)
  %arrayidx2034 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2033, i64* %arrayidx2034, align 16
  %arrayidx2035 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %937 = load i64, i64* %arrayidx2035, align 8
  %arrayidx2036 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %938 = load i64, i64* %arrayidx2036, align 16
  %add2037 = add i64 %937, %938
  %939 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 11), align 1
  %idxprom2038 = zext i8 %939 to i32
  %arrayidx2039 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2038
  %940 = load i64, i64* %arrayidx2039, align 8
  %add2040 = add i64 %add2037, %940
  %arrayidx2041 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2040, i64* %arrayidx2041, align 8
  %arrayidx2042 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %941 = load i64, i64* %arrayidx2042, align 16
  %arrayidx2043 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %942 = load i64, i64* %arrayidx2043, align 8
  %xor2044 = xor i64 %941, %942
  %call2045 = call i64 @rotr64(i64 %xor2044, i32 16)
  %arrayidx2046 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2045, i64* %arrayidx2046, align 16
  %arrayidx2047 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %943 = load i64, i64* %arrayidx2047, align 8
  %arrayidx2048 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %944 = load i64, i64* %arrayidx2048, align 16
  %add2049 = add i64 %943, %944
  %arrayidx2050 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2049, i64* %arrayidx2050, align 8
  %arrayidx2051 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %945 = load i64, i64* %arrayidx2051, align 16
  %arrayidx2052 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %946 = load i64, i64* %arrayidx2052, align 8
  %xor2053 = xor i64 %945, %946
  %call2054 = call i64 @rotr64(i64 %xor2053, i32 63)
  %arrayidx2055 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2054, i64* %arrayidx2055, align 16
  br label %do.end2056

do.end2056:                                       ; preds = %do.body2013
  br label %do.body2057

do.body2057:                                      ; preds = %do.end2056
  %arrayidx2058 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %947 = load i64, i64* %arrayidx2058, align 16
  %arrayidx2059 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %948 = load i64, i64* %arrayidx2059, align 8
  %add2060 = add i64 %947, %948
  %949 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 12), align 4
  %idxprom2061 = zext i8 %949 to i32
  %arrayidx2062 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2061
  %950 = load i64, i64* %arrayidx2062, align 8
  %add2063 = add i64 %add2060, %950
  %arrayidx2064 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2063, i64* %arrayidx2064, align 16
  %arrayidx2065 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %951 = load i64, i64* %arrayidx2065, align 8
  %arrayidx2066 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %952 = load i64, i64* %arrayidx2066, align 16
  %xor2067 = xor i64 %951, %952
  %call2068 = call i64 @rotr64(i64 %xor2067, i32 32)
  %arrayidx2069 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2068, i64* %arrayidx2069, align 8
  %arrayidx2070 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %953 = load i64, i64* %arrayidx2070, align 16
  %arrayidx2071 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %954 = load i64, i64* %arrayidx2071, align 8
  %add2072 = add i64 %953, %954
  %arrayidx2073 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2072, i64* %arrayidx2073, align 16
  %arrayidx2074 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %955 = load i64, i64* %arrayidx2074, align 8
  %arrayidx2075 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %956 = load i64, i64* %arrayidx2075, align 16
  %xor2076 = xor i64 %955, %956
  %call2077 = call i64 @rotr64(i64 %xor2076, i32 24)
  %arrayidx2078 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2077, i64* %arrayidx2078, align 8
  %arrayidx2079 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %957 = load i64, i64* %arrayidx2079, align 16
  %arrayidx2080 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %958 = load i64, i64* %arrayidx2080, align 8
  %add2081 = add i64 %957, %958
  %959 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 13), align 1
  %idxprom2082 = zext i8 %959 to i32
  %arrayidx2083 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2082
  %960 = load i64, i64* %arrayidx2083, align 8
  %add2084 = add i64 %add2081, %960
  %arrayidx2085 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2084, i64* %arrayidx2085, align 16
  %arrayidx2086 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %961 = load i64, i64* %arrayidx2086, align 8
  %arrayidx2087 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %962 = load i64, i64* %arrayidx2087, align 16
  %xor2088 = xor i64 %961, %962
  %call2089 = call i64 @rotr64(i64 %xor2088, i32 16)
  %arrayidx2090 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2089, i64* %arrayidx2090, align 8
  %arrayidx2091 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %963 = load i64, i64* %arrayidx2091, align 16
  %arrayidx2092 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %964 = load i64, i64* %arrayidx2092, align 8
  %add2093 = add i64 %963, %964
  %arrayidx2094 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2093, i64* %arrayidx2094, align 16
  %arrayidx2095 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %965 = load i64, i64* %arrayidx2095, align 8
  %arrayidx2096 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %966 = load i64, i64* %arrayidx2096, align 16
  %xor2097 = xor i64 %965, %966
  %call2098 = call i64 @rotr64(i64 %xor2097, i32 63)
  %arrayidx2099 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2098, i64* %arrayidx2099, align 8
  br label %do.end2100

do.end2100:                                       ; preds = %do.body2057
  br label %do.body2101

do.body2101:                                      ; preds = %do.end2100
  %arrayidx2102 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %967 = load i64, i64* %arrayidx2102, align 8
  %arrayidx2103 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %968 = load i64, i64* %arrayidx2103, align 16
  %add2104 = add i64 %967, %968
  %969 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 14), align 2
  %idxprom2105 = zext i8 %969 to i32
  %arrayidx2106 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2105
  %970 = load i64, i64* %arrayidx2106, align 8
  %add2107 = add i64 %add2104, %970
  %arrayidx2108 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2107, i64* %arrayidx2108, align 8
  %arrayidx2109 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %971 = load i64, i64* %arrayidx2109, align 16
  %arrayidx2110 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %972 = load i64, i64* %arrayidx2110, align 8
  %xor2111 = xor i64 %971, %972
  %call2112 = call i64 @rotr64(i64 %xor2111, i32 32)
  %arrayidx2113 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2112, i64* %arrayidx2113, align 16
  %arrayidx2114 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %973 = load i64, i64* %arrayidx2114, align 8
  %arrayidx2115 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %974 = load i64, i64* %arrayidx2115, align 16
  %add2116 = add i64 %973, %974
  %arrayidx2117 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2116, i64* %arrayidx2117, align 8
  %arrayidx2118 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %975 = load i64, i64* %arrayidx2118, align 16
  %arrayidx2119 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %976 = load i64, i64* %arrayidx2119, align 8
  %xor2120 = xor i64 %975, %976
  %call2121 = call i64 @rotr64(i64 %xor2120, i32 24)
  %arrayidx2122 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2121, i64* %arrayidx2122, align 16
  %arrayidx2123 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %977 = load i64, i64* %arrayidx2123, align 8
  %arrayidx2124 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %978 = load i64, i64* %arrayidx2124, align 16
  %add2125 = add i64 %977, %978
  %979 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 15), align 1
  %idxprom2126 = zext i8 %979 to i32
  %arrayidx2127 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2126
  %980 = load i64, i64* %arrayidx2127, align 8
  %add2128 = add i64 %add2125, %980
  %arrayidx2129 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2128, i64* %arrayidx2129, align 8
  %arrayidx2130 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %981 = load i64, i64* %arrayidx2130, align 16
  %arrayidx2131 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %982 = load i64, i64* %arrayidx2131, align 8
  %xor2132 = xor i64 %981, %982
  %call2133 = call i64 @rotr64(i64 %xor2132, i32 16)
  %arrayidx2134 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2133, i64* %arrayidx2134, align 16
  %arrayidx2135 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %983 = load i64, i64* %arrayidx2135, align 8
  %arrayidx2136 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %984 = load i64, i64* %arrayidx2136, align 16
  %add2137 = add i64 %983, %984
  %arrayidx2138 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2137, i64* %arrayidx2138, align 8
  %arrayidx2139 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %985 = load i64, i64* %arrayidx2139, align 16
  %arrayidx2140 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %986 = load i64, i64* %arrayidx2140, align 8
  %xor2141 = xor i64 %985, %986
  %call2142 = call i64 @rotr64(i64 %xor2141, i32 63)
  %arrayidx2143 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2142, i64* %arrayidx2143, align 16
  br label %do.end2144

do.end2144:                                       ; preds = %do.body2101
  br label %do.end2145

do.end2145:                                       ; preds = %do.end2144
  br label %do.body2146

do.body2146:                                      ; preds = %do.end2145
  br label %do.body2147

do.body2147:                                      ; preds = %do.body2146
  %arrayidx2148 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %987 = load i64, i64* %arrayidx2148, align 16
  %arrayidx2149 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %988 = load i64, i64* %arrayidx2149, align 16
  %add2150 = add i64 %987, %988
  %989 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 0), align 16
  %idxprom2151 = zext i8 %989 to i32
  %arrayidx2152 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2151
  %990 = load i64, i64* %arrayidx2152, align 8
  %add2153 = add i64 %add2150, %990
  %arrayidx2154 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2153, i64* %arrayidx2154, align 16
  %arrayidx2155 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %991 = load i64, i64* %arrayidx2155, align 16
  %arrayidx2156 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %992 = load i64, i64* %arrayidx2156, align 16
  %xor2157 = xor i64 %991, %992
  %call2158 = call i64 @rotr64(i64 %xor2157, i32 32)
  %arrayidx2159 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2158, i64* %arrayidx2159, align 16
  %arrayidx2160 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %993 = load i64, i64* %arrayidx2160, align 16
  %arrayidx2161 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %994 = load i64, i64* %arrayidx2161, align 16
  %add2162 = add i64 %993, %994
  %arrayidx2163 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2162, i64* %arrayidx2163, align 16
  %arrayidx2164 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %995 = load i64, i64* %arrayidx2164, align 16
  %arrayidx2165 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %996 = load i64, i64* %arrayidx2165, align 16
  %xor2166 = xor i64 %995, %996
  %call2167 = call i64 @rotr64(i64 %xor2166, i32 24)
  %arrayidx2168 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2167, i64* %arrayidx2168, align 16
  %arrayidx2169 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %997 = load i64, i64* %arrayidx2169, align 16
  %arrayidx2170 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %998 = load i64, i64* %arrayidx2170, align 16
  %add2171 = add i64 %997, %998
  %999 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 1), align 1
  %idxprom2172 = zext i8 %999 to i32
  %arrayidx2173 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2172
  %1000 = load i64, i64* %arrayidx2173, align 8
  %add2174 = add i64 %add2171, %1000
  %arrayidx2175 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2174, i64* %arrayidx2175, align 16
  %arrayidx2176 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1001 = load i64, i64* %arrayidx2176, align 16
  %arrayidx2177 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1002 = load i64, i64* %arrayidx2177, align 16
  %xor2178 = xor i64 %1001, %1002
  %call2179 = call i64 @rotr64(i64 %xor2178, i32 16)
  %arrayidx2180 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2179, i64* %arrayidx2180, align 16
  %arrayidx2181 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1003 = load i64, i64* %arrayidx2181, align 16
  %arrayidx2182 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1004 = load i64, i64* %arrayidx2182, align 16
  %add2183 = add i64 %1003, %1004
  %arrayidx2184 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2183, i64* %arrayidx2184, align 16
  %arrayidx2185 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1005 = load i64, i64* %arrayidx2185, align 16
  %arrayidx2186 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1006 = load i64, i64* %arrayidx2186, align 16
  %xor2187 = xor i64 %1005, %1006
  %call2188 = call i64 @rotr64(i64 %xor2187, i32 63)
  %arrayidx2189 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2188, i64* %arrayidx2189, align 16
  br label %do.end2190

do.end2190:                                       ; preds = %do.body2147
  br label %do.body2191

do.body2191:                                      ; preds = %do.end2190
  %arrayidx2192 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1007 = load i64, i64* %arrayidx2192, align 8
  %arrayidx2193 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1008 = load i64, i64* %arrayidx2193, align 8
  %add2194 = add i64 %1007, %1008
  %1009 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 2), align 2
  %idxprom2195 = zext i8 %1009 to i32
  %arrayidx2196 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2195
  %1010 = load i64, i64* %arrayidx2196, align 8
  %add2197 = add i64 %add2194, %1010
  %arrayidx2198 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2197, i64* %arrayidx2198, align 8
  %arrayidx2199 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1011 = load i64, i64* %arrayidx2199, align 8
  %arrayidx2200 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1012 = load i64, i64* %arrayidx2200, align 8
  %xor2201 = xor i64 %1011, %1012
  %call2202 = call i64 @rotr64(i64 %xor2201, i32 32)
  %arrayidx2203 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2202, i64* %arrayidx2203, align 8
  %arrayidx2204 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1013 = load i64, i64* %arrayidx2204, align 8
  %arrayidx2205 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1014 = load i64, i64* %arrayidx2205, align 8
  %add2206 = add i64 %1013, %1014
  %arrayidx2207 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2206, i64* %arrayidx2207, align 8
  %arrayidx2208 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1015 = load i64, i64* %arrayidx2208, align 8
  %arrayidx2209 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1016 = load i64, i64* %arrayidx2209, align 8
  %xor2210 = xor i64 %1015, %1016
  %call2211 = call i64 @rotr64(i64 %xor2210, i32 24)
  %arrayidx2212 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2211, i64* %arrayidx2212, align 8
  %arrayidx2213 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1017 = load i64, i64* %arrayidx2213, align 8
  %arrayidx2214 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1018 = load i64, i64* %arrayidx2214, align 8
  %add2215 = add i64 %1017, %1018
  %1019 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 3), align 1
  %idxprom2216 = zext i8 %1019 to i32
  %arrayidx2217 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2216
  %1020 = load i64, i64* %arrayidx2217, align 8
  %add2218 = add i64 %add2215, %1020
  %arrayidx2219 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2218, i64* %arrayidx2219, align 8
  %arrayidx2220 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1021 = load i64, i64* %arrayidx2220, align 8
  %arrayidx2221 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1022 = load i64, i64* %arrayidx2221, align 8
  %xor2222 = xor i64 %1021, %1022
  %call2223 = call i64 @rotr64(i64 %xor2222, i32 16)
  %arrayidx2224 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2223, i64* %arrayidx2224, align 8
  %arrayidx2225 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1023 = load i64, i64* %arrayidx2225, align 8
  %arrayidx2226 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1024 = load i64, i64* %arrayidx2226, align 8
  %add2227 = add i64 %1023, %1024
  %arrayidx2228 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2227, i64* %arrayidx2228, align 8
  %arrayidx2229 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1025 = load i64, i64* %arrayidx2229, align 8
  %arrayidx2230 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1026 = load i64, i64* %arrayidx2230, align 8
  %xor2231 = xor i64 %1025, %1026
  %call2232 = call i64 @rotr64(i64 %xor2231, i32 63)
  %arrayidx2233 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2232, i64* %arrayidx2233, align 8
  br label %do.end2234

do.end2234:                                       ; preds = %do.body2191
  br label %do.body2235

do.body2235:                                      ; preds = %do.end2234
  %arrayidx2236 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1027 = load i64, i64* %arrayidx2236, align 16
  %arrayidx2237 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1028 = load i64, i64* %arrayidx2237, align 16
  %add2238 = add i64 %1027, %1028
  %1029 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 4), align 4
  %idxprom2239 = zext i8 %1029 to i32
  %arrayidx2240 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2239
  %1030 = load i64, i64* %arrayidx2240, align 8
  %add2241 = add i64 %add2238, %1030
  %arrayidx2242 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2241, i64* %arrayidx2242, align 16
  %arrayidx2243 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1031 = load i64, i64* %arrayidx2243, align 16
  %arrayidx2244 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1032 = load i64, i64* %arrayidx2244, align 16
  %xor2245 = xor i64 %1031, %1032
  %call2246 = call i64 @rotr64(i64 %xor2245, i32 32)
  %arrayidx2247 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2246, i64* %arrayidx2247, align 16
  %arrayidx2248 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1033 = load i64, i64* %arrayidx2248, align 16
  %arrayidx2249 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1034 = load i64, i64* %arrayidx2249, align 16
  %add2250 = add i64 %1033, %1034
  %arrayidx2251 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2250, i64* %arrayidx2251, align 16
  %arrayidx2252 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1035 = load i64, i64* %arrayidx2252, align 16
  %arrayidx2253 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1036 = load i64, i64* %arrayidx2253, align 16
  %xor2254 = xor i64 %1035, %1036
  %call2255 = call i64 @rotr64(i64 %xor2254, i32 24)
  %arrayidx2256 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2255, i64* %arrayidx2256, align 16
  %arrayidx2257 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1037 = load i64, i64* %arrayidx2257, align 16
  %arrayidx2258 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1038 = load i64, i64* %arrayidx2258, align 16
  %add2259 = add i64 %1037, %1038
  %1039 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 5), align 1
  %idxprom2260 = zext i8 %1039 to i32
  %arrayidx2261 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2260
  %1040 = load i64, i64* %arrayidx2261, align 8
  %add2262 = add i64 %add2259, %1040
  %arrayidx2263 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2262, i64* %arrayidx2263, align 16
  %arrayidx2264 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1041 = load i64, i64* %arrayidx2264, align 16
  %arrayidx2265 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1042 = load i64, i64* %arrayidx2265, align 16
  %xor2266 = xor i64 %1041, %1042
  %call2267 = call i64 @rotr64(i64 %xor2266, i32 16)
  %arrayidx2268 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2267, i64* %arrayidx2268, align 16
  %arrayidx2269 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1043 = load i64, i64* %arrayidx2269, align 16
  %arrayidx2270 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1044 = load i64, i64* %arrayidx2270, align 16
  %add2271 = add i64 %1043, %1044
  %arrayidx2272 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2271, i64* %arrayidx2272, align 16
  %arrayidx2273 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1045 = load i64, i64* %arrayidx2273, align 16
  %arrayidx2274 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1046 = load i64, i64* %arrayidx2274, align 16
  %xor2275 = xor i64 %1045, %1046
  %call2276 = call i64 @rotr64(i64 %xor2275, i32 63)
  %arrayidx2277 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2276, i64* %arrayidx2277, align 16
  br label %do.end2278

do.end2278:                                       ; preds = %do.body2235
  br label %do.body2279

do.body2279:                                      ; preds = %do.end2278
  %arrayidx2280 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1047 = load i64, i64* %arrayidx2280, align 8
  %arrayidx2281 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1048 = load i64, i64* %arrayidx2281, align 8
  %add2282 = add i64 %1047, %1048
  %1049 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 6), align 2
  %idxprom2283 = zext i8 %1049 to i32
  %arrayidx2284 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2283
  %1050 = load i64, i64* %arrayidx2284, align 8
  %add2285 = add i64 %add2282, %1050
  %arrayidx2286 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2285, i64* %arrayidx2286, align 8
  %arrayidx2287 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1051 = load i64, i64* %arrayidx2287, align 8
  %arrayidx2288 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1052 = load i64, i64* %arrayidx2288, align 8
  %xor2289 = xor i64 %1051, %1052
  %call2290 = call i64 @rotr64(i64 %xor2289, i32 32)
  %arrayidx2291 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2290, i64* %arrayidx2291, align 8
  %arrayidx2292 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1053 = load i64, i64* %arrayidx2292, align 8
  %arrayidx2293 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1054 = load i64, i64* %arrayidx2293, align 8
  %add2294 = add i64 %1053, %1054
  %arrayidx2295 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2294, i64* %arrayidx2295, align 8
  %arrayidx2296 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1055 = load i64, i64* %arrayidx2296, align 8
  %arrayidx2297 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1056 = load i64, i64* %arrayidx2297, align 8
  %xor2298 = xor i64 %1055, %1056
  %call2299 = call i64 @rotr64(i64 %xor2298, i32 24)
  %arrayidx2300 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2299, i64* %arrayidx2300, align 8
  %arrayidx2301 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1057 = load i64, i64* %arrayidx2301, align 8
  %arrayidx2302 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1058 = load i64, i64* %arrayidx2302, align 8
  %add2303 = add i64 %1057, %1058
  %1059 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 7), align 1
  %idxprom2304 = zext i8 %1059 to i32
  %arrayidx2305 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2304
  %1060 = load i64, i64* %arrayidx2305, align 8
  %add2306 = add i64 %add2303, %1060
  %arrayidx2307 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2306, i64* %arrayidx2307, align 8
  %arrayidx2308 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1061 = load i64, i64* %arrayidx2308, align 8
  %arrayidx2309 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1062 = load i64, i64* %arrayidx2309, align 8
  %xor2310 = xor i64 %1061, %1062
  %call2311 = call i64 @rotr64(i64 %xor2310, i32 16)
  %arrayidx2312 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2311, i64* %arrayidx2312, align 8
  %arrayidx2313 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1063 = load i64, i64* %arrayidx2313, align 8
  %arrayidx2314 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1064 = load i64, i64* %arrayidx2314, align 8
  %add2315 = add i64 %1063, %1064
  %arrayidx2316 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2315, i64* %arrayidx2316, align 8
  %arrayidx2317 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1065 = load i64, i64* %arrayidx2317, align 8
  %arrayidx2318 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1066 = load i64, i64* %arrayidx2318, align 8
  %xor2319 = xor i64 %1065, %1066
  %call2320 = call i64 @rotr64(i64 %xor2319, i32 63)
  %arrayidx2321 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2320, i64* %arrayidx2321, align 8
  br label %do.end2322

do.end2322:                                       ; preds = %do.body2279
  br label %do.body2323

do.body2323:                                      ; preds = %do.end2322
  %arrayidx2324 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1067 = load i64, i64* %arrayidx2324, align 16
  %arrayidx2325 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1068 = load i64, i64* %arrayidx2325, align 8
  %add2326 = add i64 %1067, %1068
  %1069 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 8), align 8
  %idxprom2327 = zext i8 %1069 to i32
  %arrayidx2328 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2327
  %1070 = load i64, i64* %arrayidx2328, align 8
  %add2329 = add i64 %add2326, %1070
  %arrayidx2330 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2329, i64* %arrayidx2330, align 16
  %arrayidx2331 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1071 = load i64, i64* %arrayidx2331, align 8
  %arrayidx2332 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1072 = load i64, i64* %arrayidx2332, align 16
  %xor2333 = xor i64 %1071, %1072
  %call2334 = call i64 @rotr64(i64 %xor2333, i32 32)
  %arrayidx2335 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2334, i64* %arrayidx2335, align 8
  %arrayidx2336 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1073 = load i64, i64* %arrayidx2336, align 16
  %arrayidx2337 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1074 = load i64, i64* %arrayidx2337, align 8
  %add2338 = add i64 %1073, %1074
  %arrayidx2339 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2338, i64* %arrayidx2339, align 16
  %arrayidx2340 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1075 = load i64, i64* %arrayidx2340, align 8
  %arrayidx2341 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1076 = load i64, i64* %arrayidx2341, align 16
  %xor2342 = xor i64 %1075, %1076
  %call2343 = call i64 @rotr64(i64 %xor2342, i32 24)
  %arrayidx2344 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2343, i64* %arrayidx2344, align 8
  %arrayidx2345 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1077 = load i64, i64* %arrayidx2345, align 16
  %arrayidx2346 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1078 = load i64, i64* %arrayidx2346, align 8
  %add2347 = add i64 %1077, %1078
  %1079 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 9), align 1
  %idxprom2348 = zext i8 %1079 to i32
  %arrayidx2349 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2348
  %1080 = load i64, i64* %arrayidx2349, align 8
  %add2350 = add i64 %add2347, %1080
  %arrayidx2351 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2350, i64* %arrayidx2351, align 16
  %arrayidx2352 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1081 = load i64, i64* %arrayidx2352, align 8
  %arrayidx2353 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1082 = load i64, i64* %arrayidx2353, align 16
  %xor2354 = xor i64 %1081, %1082
  %call2355 = call i64 @rotr64(i64 %xor2354, i32 16)
  %arrayidx2356 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2355, i64* %arrayidx2356, align 8
  %arrayidx2357 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1083 = load i64, i64* %arrayidx2357, align 16
  %arrayidx2358 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1084 = load i64, i64* %arrayidx2358, align 8
  %add2359 = add i64 %1083, %1084
  %arrayidx2360 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2359, i64* %arrayidx2360, align 16
  %arrayidx2361 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1085 = load i64, i64* %arrayidx2361, align 8
  %arrayidx2362 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1086 = load i64, i64* %arrayidx2362, align 16
  %xor2363 = xor i64 %1085, %1086
  %call2364 = call i64 @rotr64(i64 %xor2363, i32 63)
  %arrayidx2365 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2364, i64* %arrayidx2365, align 8
  br label %do.end2366

do.end2366:                                       ; preds = %do.body2323
  br label %do.body2367

do.body2367:                                      ; preds = %do.end2366
  %arrayidx2368 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1087 = load i64, i64* %arrayidx2368, align 8
  %arrayidx2369 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1088 = load i64, i64* %arrayidx2369, align 16
  %add2370 = add i64 %1087, %1088
  %1089 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 10), align 2
  %idxprom2371 = zext i8 %1089 to i32
  %arrayidx2372 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2371
  %1090 = load i64, i64* %arrayidx2372, align 8
  %add2373 = add i64 %add2370, %1090
  %arrayidx2374 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2373, i64* %arrayidx2374, align 8
  %arrayidx2375 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1091 = load i64, i64* %arrayidx2375, align 16
  %arrayidx2376 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1092 = load i64, i64* %arrayidx2376, align 8
  %xor2377 = xor i64 %1091, %1092
  %call2378 = call i64 @rotr64(i64 %xor2377, i32 32)
  %arrayidx2379 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2378, i64* %arrayidx2379, align 16
  %arrayidx2380 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1093 = load i64, i64* %arrayidx2380, align 8
  %arrayidx2381 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1094 = load i64, i64* %arrayidx2381, align 16
  %add2382 = add i64 %1093, %1094
  %arrayidx2383 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2382, i64* %arrayidx2383, align 8
  %arrayidx2384 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1095 = load i64, i64* %arrayidx2384, align 16
  %arrayidx2385 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1096 = load i64, i64* %arrayidx2385, align 8
  %xor2386 = xor i64 %1095, %1096
  %call2387 = call i64 @rotr64(i64 %xor2386, i32 24)
  %arrayidx2388 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2387, i64* %arrayidx2388, align 16
  %arrayidx2389 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1097 = load i64, i64* %arrayidx2389, align 8
  %arrayidx2390 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1098 = load i64, i64* %arrayidx2390, align 16
  %add2391 = add i64 %1097, %1098
  %1099 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 11), align 1
  %idxprom2392 = zext i8 %1099 to i32
  %arrayidx2393 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2392
  %1100 = load i64, i64* %arrayidx2393, align 8
  %add2394 = add i64 %add2391, %1100
  %arrayidx2395 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2394, i64* %arrayidx2395, align 8
  %arrayidx2396 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1101 = load i64, i64* %arrayidx2396, align 16
  %arrayidx2397 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1102 = load i64, i64* %arrayidx2397, align 8
  %xor2398 = xor i64 %1101, %1102
  %call2399 = call i64 @rotr64(i64 %xor2398, i32 16)
  %arrayidx2400 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2399, i64* %arrayidx2400, align 16
  %arrayidx2401 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1103 = load i64, i64* %arrayidx2401, align 8
  %arrayidx2402 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1104 = load i64, i64* %arrayidx2402, align 16
  %add2403 = add i64 %1103, %1104
  %arrayidx2404 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2403, i64* %arrayidx2404, align 8
  %arrayidx2405 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1105 = load i64, i64* %arrayidx2405, align 16
  %arrayidx2406 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1106 = load i64, i64* %arrayidx2406, align 8
  %xor2407 = xor i64 %1105, %1106
  %call2408 = call i64 @rotr64(i64 %xor2407, i32 63)
  %arrayidx2409 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2408, i64* %arrayidx2409, align 16
  br label %do.end2410

do.end2410:                                       ; preds = %do.body2367
  br label %do.body2411

do.body2411:                                      ; preds = %do.end2410
  %arrayidx2412 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1107 = load i64, i64* %arrayidx2412, align 16
  %arrayidx2413 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1108 = load i64, i64* %arrayidx2413, align 8
  %add2414 = add i64 %1107, %1108
  %1109 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 12), align 4
  %idxprom2415 = zext i8 %1109 to i32
  %arrayidx2416 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2415
  %1110 = load i64, i64* %arrayidx2416, align 8
  %add2417 = add i64 %add2414, %1110
  %arrayidx2418 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2417, i64* %arrayidx2418, align 16
  %arrayidx2419 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1111 = load i64, i64* %arrayidx2419, align 8
  %arrayidx2420 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1112 = load i64, i64* %arrayidx2420, align 16
  %xor2421 = xor i64 %1111, %1112
  %call2422 = call i64 @rotr64(i64 %xor2421, i32 32)
  %arrayidx2423 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2422, i64* %arrayidx2423, align 8
  %arrayidx2424 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1113 = load i64, i64* %arrayidx2424, align 16
  %arrayidx2425 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1114 = load i64, i64* %arrayidx2425, align 8
  %add2426 = add i64 %1113, %1114
  %arrayidx2427 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2426, i64* %arrayidx2427, align 16
  %arrayidx2428 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1115 = load i64, i64* %arrayidx2428, align 8
  %arrayidx2429 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1116 = load i64, i64* %arrayidx2429, align 16
  %xor2430 = xor i64 %1115, %1116
  %call2431 = call i64 @rotr64(i64 %xor2430, i32 24)
  %arrayidx2432 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2431, i64* %arrayidx2432, align 8
  %arrayidx2433 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1117 = load i64, i64* %arrayidx2433, align 16
  %arrayidx2434 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1118 = load i64, i64* %arrayidx2434, align 8
  %add2435 = add i64 %1117, %1118
  %1119 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 13), align 1
  %idxprom2436 = zext i8 %1119 to i32
  %arrayidx2437 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2436
  %1120 = load i64, i64* %arrayidx2437, align 8
  %add2438 = add i64 %add2435, %1120
  %arrayidx2439 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2438, i64* %arrayidx2439, align 16
  %arrayidx2440 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1121 = load i64, i64* %arrayidx2440, align 8
  %arrayidx2441 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1122 = load i64, i64* %arrayidx2441, align 16
  %xor2442 = xor i64 %1121, %1122
  %call2443 = call i64 @rotr64(i64 %xor2442, i32 16)
  %arrayidx2444 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2443, i64* %arrayidx2444, align 8
  %arrayidx2445 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1123 = load i64, i64* %arrayidx2445, align 16
  %arrayidx2446 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1124 = load i64, i64* %arrayidx2446, align 8
  %add2447 = add i64 %1123, %1124
  %arrayidx2448 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2447, i64* %arrayidx2448, align 16
  %arrayidx2449 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1125 = load i64, i64* %arrayidx2449, align 8
  %arrayidx2450 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1126 = load i64, i64* %arrayidx2450, align 16
  %xor2451 = xor i64 %1125, %1126
  %call2452 = call i64 @rotr64(i64 %xor2451, i32 63)
  %arrayidx2453 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2452, i64* %arrayidx2453, align 8
  br label %do.end2454

do.end2454:                                       ; preds = %do.body2411
  br label %do.body2455

do.body2455:                                      ; preds = %do.end2454
  %arrayidx2456 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1127 = load i64, i64* %arrayidx2456, align 8
  %arrayidx2457 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1128 = load i64, i64* %arrayidx2457, align 16
  %add2458 = add i64 %1127, %1128
  %1129 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 14), align 2
  %idxprom2459 = zext i8 %1129 to i32
  %arrayidx2460 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2459
  %1130 = load i64, i64* %arrayidx2460, align 8
  %add2461 = add i64 %add2458, %1130
  %arrayidx2462 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2461, i64* %arrayidx2462, align 8
  %arrayidx2463 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1131 = load i64, i64* %arrayidx2463, align 16
  %arrayidx2464 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1132 = load i64, i64* %arrayidx2464, align 8
  %xor2465 = xor i64 %1131, %1132
  %call2466 = call i64 @rotr64(i64 %xor2465, i32 32)
  %arrayidx2467 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2466, i64* %arrayidx2467, align 16
  %arrayidx2468 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1133 = load i64, i64* %arrayidx2468, align 8
  %arrayidx2469 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1134 = load i64, i64* %arrayidx2469, align 16
  %add2470 = add i64 %1133, %1134
  %arrayidx2471 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2470, i64* %arrayidx2471, align 8
  %arrayidx2472 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1135 = load i64, i64* %arrayidx2472, align 16
  %arrayidx2473 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1136 = load i64, i64* %arrayidx2473, align 8
  %xor2474 = xor i64 %1135, %1136
  %call2475 = call i64 @rotr64(i64 %xor2474, i32 24)
  %arrayidx2476 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2475, i64* %arrayidx2476, align 16
  %arrayidx2477 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1137 = load i64, i64* %arrayidx2477, align 8
  %arrayidx2478 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1138 = load i64, i64* %arrayidx2478, align 16
  %add2479 = add i64 %1137, %1138
  %1139 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 15), align 1
  %idxprom2480 = zext i8 %1139 to i32
  %arrayidx2481 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2480
  %1140 = load i64, i64* %arrayidx2481, align 8
  %add2482 = add i64 %add2479, %1140
  %arrayidx2483 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2482, i64* %arrayidx2483, align 8
  %arrayidx2484 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1141 = load i64, i64* %arrayidx2484, align 16
  %arrayidx2485 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1142 = load i64, i64* %arrayidx2485, align 8
  %xor2486 = xor i64 %1141, %1142
  %call2487 = call i64 @rotr64(i64 %xor2486, i32 16)
  %arrayidx2488 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2487, i64* %arrayidx2488, align 16
  %arrayidx2489 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1143 = load i64, i64* %arrayidx2489, align 8
  %arrayidx2490 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1144 = load i64, i64* %arrayidx2490, align 16
  %add2491 = add i64 %1143, %1144
  %arrayidx2492 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2491, i64* %arrayidx2492, align 8
  %arrayidx2493 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1145 = load i64, i64* %arrayidx2493, align 16
  %arrayidx2494 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1146 = load i64, i64* %arrayidx2494, align 8
  %xor2495 = xor i64 %1145, %1146
  %call2496 = call i64 @rotr64(i64 %xor2495, i32 63)
  %arrayidx2497 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2496, i64* %arrayidx2497, align 16
  br label %do.end2498

do.end2498:                                       ; preds = %do.body2455
  br label %do.end2499

do.end2499:                                       ; preds = %do.end2498
  br label %do.body2500

do.body2500:                                      ; preds = %do.end2499
  br label %do.body2501

do.body2501:                                      ; preds = %do.body2500
  %arrayidx2502 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1147 = load i64, i64* %arrayidx2502, align 16
  %arrayidx2503 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1148 = load i64, i64* %arrayidx2503, align 16
  %add2504 = add i64 %1147, %1148
  %1149 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 0), align 16
  %idxprom2505 = zext i8 %1149 to i32
  %arrayidx2506 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2505
  %1150 = load i64, i64* %arrayidx2506, align 8
  %add2507 = add i64 %add2504, %1150
  %arrayidx2508 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2507, i64* %arrayidx2508, align 16
  %arrayidx2509 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1151 = load i64, i64* %arrayidx2509, align 16
  %arrayidx2510 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1152 = load i64, i64* %arrayidx2510, align 16
  %xor2511 = xor i64 %1151, %1152
  %call2512 = call i64 @rotr64(i64 %xor2511, i32 32)
  %arrayidx2513 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2512, i64* %arrayidx2513, align 16
  %arrayidx2514 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1153 = load i64, i64* %arrayidx2514, align 16
  %arrayidx2515 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1154 = load i64, i64* %arrayidx2515, align 16
  %add2516 = add i64 %1153, %1154
  %arrayidx2517 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2516, i64* %arrayidx2517, align 16
  %arrayidx2518 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1155 = load i64, i64* %arrayidx2518, align 16
  %arrayidx2519 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1156 = load i64, i64* %arrayidx2519, align 16
  %xor2520 = xor i64 %1155, %1156
  %call2521 = call i64 @rotr64(i64 %xor2520, i32 24)
  %arrayidx2522 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2521, i64* %arrayidx2522, align 16
  %arrayidx2523 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1157 = load i64, i64* %arrayidx2523, align 16
  %arrayidx2524 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1158 = load i64, i64* %arrayidx2524, align 16
  %add2525 = add i64 %1157, %1158
  %1159 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 1), align 1
  %idxprom2526 = zext i8 %1159 to i32
  %arrayidx2527 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2526
  %1160 = load i64, i64* %arrayidx2527, align 8
  %add2528 = add i64 %add2525, %1160
  %arrayidx2529 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2528, i64* %arrayidx2529, align 16
  %arrayidx2530 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1161 = load i64, i64* %arrayidx2530, align 16
  %arrayidx2531 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1162 = load i64, i64* %arrayidx2531, align 16
  %xor2532 = xor i64 %1161, %1162
  %call2533 = call i64 @rotr64(i64 %xor2532, i32 16)
  %arrayidx2534 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2533, i64* %arrayidx2534, align 16
  %arrayidx2535 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1163 = load i64, i64* %arrayidx2535, align 16
  %arrayidx2536 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1164 = load i64, i64* %arrayidx2536, align 16
  %add2537 = add i64 %1163, %1164
  %arrayidx2538 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2537, i64* %arrayidx2538, align 16
  %arrayidx2539 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1165 = load i64, i64* %arrayidx2539, align 16
  %arrayidx2540 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1166 = load i64, i64* %arrayidx2540, align 16
  %xor2541 = xor i64 %1165, %1166
  %call2542 = call i64 @rotr64(i64 %xor2541, i32 63)
  %arrayidx2543 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2542, i64* %arrayidx2543, align 16
  br label %do.end2544

do.end2544:                                       ; preds = %do.body2501
  br label %do.body2545

do.body2545:                                      ; preds = %do.end2544
  %arrayidx2546 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1167 = load i64, i64* %arrayidx2546, align 8
  %arrayidx2547 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1168 = load i64, i64* %arrayidx2547, align 8
  %add2548 = add i64 %1167, %1168
  %1169 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 2), align 2
  %idxprom2549 = zext i8 %1169 to i32
  %arrayidx2550 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2549
  %1170 = load i64, i64* %arrayidx2550, align 8
  %add2551 = add i64 %add2548, %1170
  %arrayidx2552 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2551, i64* %arrayidx2552, align 8
  %arrayidx2553 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1171 = load i64, i64* %arrayidx2553, align 8
  %arrayidx2554 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1172 = load i64, i64* %arrayidx2554, align 8
  %xor2555 = xor i64 %1171, %1172
  %call2556 = call i64 @rotr64(i64 %xor2555, i32 32)
  %arrayidx2557 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2556, i64* %arrayidx2557, align 8
  %arrayidx2558 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1173 = load i64, i64* %arrayidx2558, align 8
  %arrayidx2559 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1174 = load i64, i64* %arrayidx2559, align 8
  %add2560 = add i64 %1173, %1174
  %arrayidx2561 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2560, i64* %arrayidx2561, align 8
  %arrayidx2562 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1175 = load i64, i64* %arrayidx2562, align 8
  %arrayidx2563 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1176 = load i64, i64* %arrayidx2563, align 8
  %xor2564 = xor i64 %1175, %1176
  %call2565 = call i64 @rotr64(i64 %xor2564, i32 24)
  %arrayidx2566 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2565, i64* %arrayidx2566, align 8
  %arrayidx2567 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1177 = load i64, i64* %arrayidx2567, align 8
  %arrayidx2568 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1178 = load i64, i64* %arrayidx2568, align 8
  %add2569 = add i64 %1177, %1178
  %1179 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 3), align 1
  %idxprom2570 = zext i8 %1179 to i32
  %arrayidx2571 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2570
  %1180 = load i64, i64* %arrayidx2571, align 8
  %add2572 = add i64 %add2569, %1180
  %arrayidx2573 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2572, i64* %arrayidx2573, align 8
  %arrayidx2574 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1181 = load i64, i64* %arrayidx2574, align 8
  %arrayidx2575 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1182 = load i64, i64* %arrayidx2575, align 8
  %xor2576 = xor i64 %1181, %1182
  %call2577 = call i64 @rotr64(i64 %xor2576, i32 16)
  %arrayidx2578 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2577, i64* %arrayidx2578, align 8
  %arrayidx2579 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1183 = load i64, i64* %arrayidx2579, align 8
  %arrayidx2580 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1184 = load i64, i64* %arrayidx2580, align 8
  %add2581 = add i64 %1183, %1184
  %arrayidx2582 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2581, i64* %arrayidx2582, align 8
  %arrayidx2583 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1185 = load i64, i64* %arrayidx2583, align 8
  %arrayidx2584 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1186 = load i64, i64* %arrayidx2584, align 8
  %xor2585 = xor i64 %1185, %1186
  %call2586 = call i64 @rotr64(i64 %xor2585, i32 63)
  %arrayidx2587 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2586, i64* %arrayidx2587, align 8
  br label %do.end2588

do.end2588:                                       ; preds = %do.body2545
  br label %do.body2589

do.body2589:                                      ; preds = %do.end2588
  %arrayidx2590 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1187 = load i64, i64* %arrayidx2590, align 16
  %arrayidx2591 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1188 = load i64, i64* %arrayidx2591, align 16
  %add2592 = add i64 %1187, %1188
  %1189 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 4), align 4
  %idxprom2593 = zext i8 %1189 to i32
  %arrayidx2594 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2593
  %1190 = load i64, i64* %arrayidx2594, align 8
  %add2595 = add i64 %add2592, %1190
  %arrayidx2596 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2595, i64* %arrayidx2596, align 16
  %arrayidx2597 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1191 = load i64, i64* %arrayidx2597, align 16
  %arrayidx2598 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1192 = load i64, i64* %arrayidx2598, align 16
  %xor2599 = xor i64 %1191, %1192
  %call2600 = call i64 @rotr64(i64 %xor2599, i32 32)
  %arrayidx2601 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2600, i64* %arrayidx2601, align 16
  %arrayidx2602 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1193 = load i64, i64* %arrayidx2602, align 16
  %arrayidx2603 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1194 = load i64, i64* %arrayidx2603, align 16
  %add2604 = add i64 %1193, %1194
  %arrayidx2605 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2604, i64* %arrayidx2605, align 16
  %arrayidx2606 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1195 = load i64, i64* %arrayidx2606, align 16
  %arrayidx2607 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1196 = load i64, i64* %arrayidx2607, align 16
  %xor2608 = xor i64 %1195, %1196
  %call2609 = call i64 @rotr64(i64 %xor2608, i32 24)
  %arrayidx2610 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2609, i64* %arrayidx2610, align 16
  %arrayidx2611 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1197 = load i64, i64* %arrayidx2611, align 16
  %arrayidx2612 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1198 = load i64, i64* %arrayidx2612, align 16
  %add2613 = add i64 %1197, %1198
  %1199 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 5), align 1
  %idxprom2614 = zext i8 %1199 to i32
  %arrayidx2615 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2614
  %1200 = load i64, i64* %arrayidx2615, align 8
  %add2616 = add i64 %add2613, %1200
  %arrayidx2617 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2616, i64* %arrayidx2617, align 16
  %arrayidx2618 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1201 = load i64, i64* %arrayidx2618, align 16
  %arrayidx2619 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1202 = load i64, i64* %arrayidx2619, align 16
  %xor2620 = xor i64 %1201, %1202
  %call2621 = call i64 @rotr64(i64 %xor2620, i32 16)
  %arrayidx2622 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2621, i64* %arrayidx2622, align 16
  %arrayidx2623 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1203 = load i64, i64* %arrayidx2623, align 16
  %arrayidx2624 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1204 = load i64, i64* %arrayidx2624, align 16
  %add2625 = add i64 %1203, %1204
  %arrayidx2626 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2625, i64* %arrayidx2626, align 16
  %arrayidx2627 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1205 = load i64, i64* %arrayidx2627, align 16
  %arrayidx2628 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1206 = load i64, i64* %arrayidx2628, align 16
  %xor2629 = xor i64 %1205, %1206
  %call2630 = call i64 @rotr64(i64 %xor2629, i32 63)
  %arrayidx2631 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2630, i64* %arrayidx2631, align 16
  br label %do.end2632

do.end2632:                                       ; preds = %do.body2589
  br label %do.body2633

do.body2633:                                      ; preds = %do.end2632
  %arrayidx2634 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1207 = load i64, i64* %arrayidx2634, align 8
  %arrayidx2635 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1208 = load i64, i64* %arrayidx2635, align 8
  %add2636 = add i64 %1207, %1208
  %1209 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 6), align 2
  %idxprom2637 = zext i8 %1209 to i32
  %arrayidx2638 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2637
  %1210 = load i64, i64* %arrayidx2638, align 8
  %add2639 = add i64 %add2636, %1210
  %arrayidx2640 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2639, i64* %arrayidx2640, align 8
  %arrayidx2641 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1211 = load i64, i64* %arrayidx2641, align 8
  %arrayidx2642 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1212 = load i64, i64* %arrayidx2642, align 8
  %xor2643 = xor i64 %1211, %1212
  %call2644 = call i64 @rotr64(i64 %xor2643, i32 32)
  %arrayidx2645 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2644, i64* %arrayidx2645, align 8
  %arrayidx2646 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1213 = load i64, i64* %arrayidx2646, align 8
  %arrayidx2647 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1214 = load i64, i64* %arrayidx2647, align 8
  %add2648 = add i64 %1213, %1214
  %arrayidx2649 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2648, i64* %arrayidx2649, align 8
  %arrayidx2650 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1215 = load i64, i64* %arrayidx2650, align 8
  %arrayidx2651 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1216 = load i64, i64* %arrayidx2651, align 8
  %xor2652 = xor i64 %1215, %1216
  %call2653 = call i64 @rotr64(i64 %xor2652, i32 24)
  %arrayidx2654 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2653, i64* %arrayidx2654, align 8
  %arrayidx2655 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1217 = load i64, i64* %arrayidx2655, align 8
  %arrayidx2656 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1218 = load i64, i64* %arrayidx2656, align 8
  %add2657 = add i64 %1217, %1218
  %1219 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 7), align 1
  %idxprom2658 = zext i8 %1219 to i32
  %arrayidx2659 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2658
  %1220 = load i64, i64* %arrayidx2659, align 8
  %add2660 = add i64 %add2657, %1220
  %arrayidx2661 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2660, i64* %arrayidx2661, align 8
  %arrayidx2662 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1221 = load i64, i64* %arrayidx2662, align 8
  %arrayidx2663 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1222 = load i64, i64* %arrayidx2663, align 8
  %xor2664 = xor i64 %1221, %1222
  %call2665 = call i64 @rotr64(i64 %xor2664, i32 16)
  %arrayidx2666 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2665, i64* %arrayidx2666, align 8
  %arrayidx2667 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1223 = load i64, i64* %arrayidx2667, align 8
  %arrayidx2668 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1224 = load i64, i64* %arrayidx2668, align 8
  %add2669 = add i64 %1223, %1224
  %arrayidx2670 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2669, i64* %arrayidx2670, align 8
  %arrayidx2671 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1225 = load i64, i64* %arrayidx2671, align 8
  %arrayidx2672 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1226 = load i64, i64* %arrayidx2672, align 8
  %xor2673 = xor i64 %1225, %1226
  %call2674 = call i64 @rotr64(i64 %xor2673, i32 63)
  %arrayidx2675 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2674, i64* %arrayidx2675, align 8
  br label %do.end2676

do.end2676:                                       ; preds = %do.body2633
  br label %do.body2677

do.body2677:                                      ; preds = %do.end2676
  %arrayidx2678 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1227 = load i64, i64* %arrayidx2678, align 16
  %arrayidx2679 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1228 = load i64, i64* %arrayidx2679, align 8
  %add2680 = add i64 %1227, %1228
  %1229 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 8), align 8
  %idxprom2681 = zext i8 %1229 to i32
  %arrayidx2682 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2681
  %1230 = load i64, i64* %arrayidx2682, align 8
  %add2683 = add i64 %add2680, %1230
  %arrayidx2684 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2683, i64* %arrayidx2684, align 16
  %arrayidx2685 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1231 = load i64, i64* %arrayidx2685, align 8
  %arrayidx2686 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1232 = load i64, i64* %arrayidx2686, align 16
  %xor2687 = xor i64 %1231, %1232
  %call2688 = call i64 @rotr64(i64 %xor2687, i32 32)
  %arrayidx2689 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2688, i64* %arrayidx2689, align 8
  %arrayidx2690 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1233 = load i64, i64* %arrayidx2690, align 16
  %arrayidx2691 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1234 = load i64, i64* %arrayidx2691, align 8
  %add2692 = add i64 %1233, %1234
  %arrayidx2693 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2692, i64* %arrayidx2693, align 16
  %arrayidx2694 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1235 = load i64, i64* %arrayidx2694, align 8
  %arrayidx2695 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1236 = load i64, i64* %arrayidx2695, align 16
  %xor2696 = xor i64 %1235, %1236
  %call2697 = call i64 @rotr64(i64 %xor2696, i32 24)
  %arrayidx2698 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2697, i64* %arrayidx2698, align 8
  %arrayidx2699 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1237 = load i64, i64* %arrayidx2699, align 16
  %arrayidx2700 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1238 = load i64, i64* %arrayidx2700, align 8
  %add2701 = add i64 %1237, %1238
  %1239 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 9), align 1
  %idxprom2702 = zext i8 %1239 to i32
  %arrayidx2703 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2702
  %1240 = load i64, i64* %arrayidx2703, align 8
  %add2704 = add i64 %add2701, %1240
  %arrayidx2705 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2704, i64* %arrayidx2705, align 16
  %arrayidx2706 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1241 = load i64, i64* %arrayidx2706, align 8
  %arrayidx2707 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1242 = load i64, i64* %arrayidx2707, align 16
  %xor2708 = xor i64 %1241, %1242
  %call2709 = call i64 @rotr64(i64 %xor2708, i32 16)
  %arrayidx2710 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2709, i64* %arrayidx2710, align 8
  %arrayidx2711 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1243 = load i64, i64* %arrayidx2711, align 16
  %arrayidx2712 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1244 = load i64, i64* %arrayidx2712, align 8
  %add2713 = add i64 %1243, %1244
  %arrayidx2714 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2713, i64* %arrayidx2714, align 16
  %arrayidx2715 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1245 = load i64, i64* %arrayidx2715, align 8
  %arrayidx2716 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1246 = load i64, i64* %arrayidx2716, align 16
  %xor2717 = xor i64 %1245, %1246
  %call2718 = call i64 @rotr64(i64 %xor2717, i32 63)
  %arrayidx2719 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2718, i64* %arrayidx2719, align 8
  br label %do.end2720

do.end2720:                                       ; preds = %do.body2677
  br label %do.body2721

do.body2721:                                      ; preds = %do.end2720
  %arrayidx2722 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1247 = load i64, i64* %arrayidx2722, align 8
  %arrayidx2723 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1248 = load i64, i64* %arrayidx2723, align 16
  %add2724 = add i64 %1247, %1248
  %1249 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 10), align 2
  %idxprom2725 = zext i8 %1249 to i32
  %arrayidx2726 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2725
  %1250 = load i64, i64* %arrayidx2726, align 8
  %add2727 = add i64 %add2724, %1250
  %arrayidx2728 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2727, i64* %arrayidx2728, align 8
  %arrayidx2729 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1251 = load i64, i64* %arrayidx2729, align 16
  %arrayidx2730 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1252 = load i64, i64* %arrayidx2730, align 8
  %xor2731 = xor i64 %1251, %1252
  %call2732 = call i64 @rotr64(i64 %xor2731, i32 32)
  %arrayidx2733 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2732, i64* %arrayidx2733, align 16
  %arrayidx2734 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1253 = load i64, i64* %arrayidx2734, align 8
  %arrayidx2735 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1254 = load i64, i64* %arrayidx2735, align 16
  %add2736 = add i64 %1253, %1254
  %arrayidx2737 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2736, i64* %arrayidx2737, align 8
  %arrayidx2738 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1255 = load i64, i64* %arrayidx2738, align 16
  %arrayidx2739 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1256 = load i64, i64* %arrayidx2739, align 8
  %xor2740 = xor i64 %1255, %1256
  %call2741 = call i64 @rotr64(i64 %xor2740, i32 24)
  %arrayidx2742 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2741, i64* %arrayidx2742, align 16
  %arrayidx2743 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1257 = load i64, i64* %arrayidx2743, align 8
  %arrayidx2744 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1258 = load i64, i64* %arrayidx2744, align 16
  %add2745 = add i64 %1257, %1258
  %1259 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 11), align 1
  %idxprom2746 = zext i8 %1259 to i32
  %arrayidx2747 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2746
  %1260 = load i64, i64* %arrayidx2747, align 8
  %add2748 = add i64 %add2745, %1260
  %arrayidx2749 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2748, i64* %arrayidx2749, align 8
  %arrayidx2750 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1261 = load i64, i64* %arrayidx2750, align 16
  %arrayidx2751 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1262 = load i64, i64* %arrayidx2751, align 8
  %xor2752 = xor i64 %1261, %1262
  %call2753 = call i64 @rotr64(i64 %xor2752, i32 16)
  %arrayidx2754 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2753, i64* %arrayidx2754, align 16
  %arrayidx2755 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1263 = load i64, i64* %arrayidx2755, align 8
  %arrayidx2756 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1264 = load i64, i64* %arrayidx2756, align 16
  %add2757 = add i64 %1263, %1264
  %arrayidx2758 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add2757, i64* %arrayidx2758, align 8
  %arrayidx2759 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1265 = load i64, i64* %arrayidx2759, align 16
  %arrayidx2760 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1266 = load i64, i64* %arrayidx2760, align 8
  %xor2761 = xor i64 %1265, %1266
  %call2762 = call i64 @rotr64(i64 %xor2761, i32 63)
  %arrayidx2763 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2762, i64* %arrayidx2763, align 16
  br label %do.end2764

do.end2764:                                       ; preds = %do.body2721
  br label %do.body2765

do.body2765:                                      ; preds = %do.end2764
  %arrayidx2766 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1267 = load i64, i64* %arrayidx2766, align 16
  %arrayidx2767 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1268 = load i64, i64* %arrayidx2767, align 8
  %add2768 = add i64 %1267, %1268
  %1269 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 12), align 4
  %idxprom2769 = zext i8 %1269 to i32
  %arrayidx2770 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2769
  %1270 = load i64, i64* %arrayidx2770, align 8
  %add2771 = add i64 %add2768, %1270
  %arrayidx2772 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2771, i64* %arrayidx2772, align 16
  %arrayidx2773 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1271 = load i64, i64* %arrayidx2773, align 8
  %arrayidx2774 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1272 = load i64, i64* %arrayidx2774, align 16
  %xor2775 = xor i64 %1271, %1272
  %call2776 = call i64 @rotr64(i64 %xor2775, i32 32)
  %arrayidx2777 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2776, i64* %arrayidx2777, align 8
  %arrayidx2778 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1273 = load i64, i64* %arrayidx2778, align 16
  %arrayidx2779 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1274 = load i64, i64* %arrayidx2779, align 8
  %add2780 = add i64 %1273, %1274
  %arrayidx2781 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2780, i64* %arrayidx2781, align 16
  %arrayidx2782 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1275 = load i64, i64* %arrayidx2782, align 8
  %arrayidx2783 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1276 = load i64, i64* %arrayidx2783, align 16
  %xor2784 = xor i64 %1275, %1276
  %call2785 = call i64 @rotr64(i64 %xor2784, i32 24)
  %arrayidx2786 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2785, i64* %arrayidx2786, align 8
  %arrayidx2787 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1277 = load i64, i64* %arrayidx2787, align 16
  %arrayidx2788 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1278 = load i64, i64* %arrayidx2788, align 8
  %add2789 = add i64 %1277, %1278
  %1279 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 13), align 1
  %idxprom2790 = zext i8 %1279 to i32
  %arrayidx2791 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2790
  %1280 = load i64, i64* %arrayidx2791, align 8
  %add2792 = add i64 %add2789, %1280
  %arrayidx2793 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2792, i64* %arrayidx2793, align 16
  %arrayidx2794 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1281 = load i64, i64* %arrayidx2794, align 8
  %arrayidx2795 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1282 = load i64, i64* %arrayidx2795, align 16
  %xor2796 = xor i64 %1281, %1282
  %call2797 = call i64 @rotr64(i64 %xor2796, i32 16)
  %arrayidx2798 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2797, i64* %arrayidx2798, align 8
  %arrayidx2799 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1283 = load i64, i64* %arrayidx2799, align 16
  %arrayidx2800 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1284 = load i64, i64* %arrayidx2800, align 8
  %add2801 = add i64 %1283, %1284
  %arrayidx2802 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2801, i64* %arrayidx2802, align 16
  %arrayidx2803 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1285 = load i64, i64* %arrayidx2803, align 8
  %arrayidx2804 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1286 = load i64, i64* %arrayidx2804, align 16
  %xor2805 = xor i64 %1285, %1286
  %call2806 = call i64 @rotr64(i64 %xor2805, i32 63)
  %arrayidx2807 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2806, i64* %arrayidx2807, align 8
  br label %do.end2808

do.end2808:                                       ; preds = %do.body2765
  br label %do.body2809

do.body2809:                                      ; preds = %do.end2808
  %arrayidx2810 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1287 = load i64, i64* %arrayidx2810, align 8
  %arrayidx2811 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1288 = load i64, i64* %arrayidx2811, align 16
  %add2812 = add i64 %1287, %1288
  %1289 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 14), align 2
  %idxprom2813 = zext i8 %1289 to i32
  %arrayidx2814 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2813
  %1290 = load i64, i64* %arrayidx2814, align 8
  %add2815 = add i64 %add2812, %1290
  %arrayidx2816 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2815, i64* %arrayidx2816, align 8
  %arrayidx2817 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1291 = load i64, i64* %arrayidx2817, align 16
  %arrayidx2818 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1292 = load i64, i64* %arrayidx2818, align 8
  %xor2819 = xor i64 %1291, %1292
  %call2820 = call i64 @rotr64(i64 %xor2819, i32 32)
  %arrayidx2821 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2820, i64* %arrayidx2821, align 16
  %arrayidx2822 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1293 = load i64, i64* %arrayidx2822, align 8
  %arrayidx2823 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1294 = load i64, i64* %arrayidx2823, align 16
  %add2824 = add i64 %1293, %1294
  %arrayidx2825 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2824, i64* %arrayidx2825, align 8
  %arrayidx2826 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1295 = load i64, i64* %arrayidx2826, align 16
  %arrayidx2827 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1296 = load i64, i64* %arrayidx2827, align 8
  %xor2828 = xor i64 %1295, %1296
  %call2829 = call i64 @rotr64(i64 %xor2828, i32 24)
  %arrayidx2830 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2829, i64* %arrayidx2830, align 16
  %arrayidx2831 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1297 = load i64, i64* %arrayidx2831, align 8
  %arrayidx2832 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1298 = load i64, i64* %arrayidx2832, align 16
  %add2833 = add i64 %1297, %1298
  %1299 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 15), align 1
  %idxprom2834 = zext i8 %1299 to i32
  %arrayidx2835 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2834
  %1300 = load i64, i64* %arrayidx2835, align 8
  %add2836 = add i64 %add2833, %1300
  %arrayidx2837 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2836, i64* %arrayidx2837, align 8
  %arrayidx2838 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1301 = load i64, i64* %arrayidx2838, align 16
  %arrayidx2839 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1302 = load i64, i64* %arrayidx2839, align 8
  %xor2840 = xor i64 %1301, %1302
  %call2841 = call i64 @rotr64(i64 %xor2840, i32 16)
  %arrayidx2842 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2841, i64* %arrayidx2842, align 16
  %arrayidx2843 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1303 = load i64, i64* %arrayidx2843, align 8
  %arrayidx2844 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1304 = load i64, i64* %arrayidx2844, align 16
  %add2845 = add i64 %1303, %1304
  %arrayidx2846 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2845, i64* %arrayidx2846, align 8
  %arrayidx2847 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1305 = load i64, i64* %arrayidx2847, align 16
  %arrayidx2848 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1306 = load i64, i64* %arrayidx2848, align 8
  %xor2849 = xor i64 %1305, %1306
  %call2850 = call i64 @rotr64(i64 %xor2849, i32 63)
  %arrayidx2851 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2850, i64* %arrayidx2851, align 16
  br label %do.end2852

do.end2852:                                       ; preds = %do.body2809
  br label %do.end2853

do.end2853:                                       ; preds = %do.end2852
  br label %do.body2854

do.body2854:                                      ; preds = %do.end2853
  br label %do.body2855

do.body2855:                                      ; preds = %do.body2854
  %arrayidx2856 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1307 = load i64, i64* %arrayidx2856, align 16
  %arrayidx2857 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1308 = load i64, i64* %arrayidx2857, align 16
  %add2858 = add i64 %1307, %1308
  %1309 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 0), align 16
  %idxprom2859 = zext i8 %1309 to i32
  %arrayidx2860 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2859
  %1310 = load i64, i64* %arrayidx2860, align 8
  %add2861 = add i64 %add2858, %1310
  %arrayidx2862 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2861, i64* %arrayidx2862, align 16
  %arrayidx2863 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1311 = load i64, i64* %arrayidx2863, align 16
  %arrayidx2864 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1312 = load i64, i64* %arrayidx2864, align 16
  %xor2865 = xor i64 %1311, %1312
  %call2866 = call i64 @rotr64(i64 %xor2865, i32 32)
  %arrayidx2867 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2866, i64* %arrayidx2867, align 16
  %arrayidx2868 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1313 = load i64, i64* %arrayidx2868, align 16
  %arrayidx2869 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1314 = load i64, i64* %arrayidx2869, align 16
  %add2870 = add i64 %1313, %1314
  %arrayidx2871 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2870, i64* %arrayidx2871, align 16
  %arrayidx2872 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1315 = load i64, i64* %arrayidx2872, align 16
  %arrayidx2873 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1316 = load i64, i64* %arrayidx2873, align 16
  %xor2874 = xor i64 %1315, %1316
  %call2875 = call i64 @rotr64(i64 %xor2874, i32 24)
  %arrayidx2876 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2875, i64* %arrayidx2876, align 16
  %arrayidx2877 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1317 = load i64, i64* %arrayidx2877, align 16
  %arrayidx2878 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1318 = load i64, i64* %arrayidx2878, align 16
  %add2879 = add i64 %1317, %1318
  %1319 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 1), align 1
  %idxprom2880 = zext i8 %1319 to i32
  %arrayidx2881 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2880
  %1320 = load i64, i64* %arrayidx2881, align 8
  %add2882 = add i64 %add2879, %1320
  %arrayidx2883 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add2882, i64* %arrayidx2883, align 16
  %arrayidx2884 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1321 = load i64, i64* %arrayidx2884, align 16
  %arrayidx2885 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1322 = load i64, i64* %arrayidx2885, align 16
  %xor2886 = xor i64 %1321, %1322
  %call2887 = call i64 @rotr64(i64 %xor2886, i32 16)
  %arrayidx2888 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2887, i64* %arrayidx2888, align 16
  %arrayidx2889 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1323 = load i64, i64* %arrayidx2889, align 16
  %arrayidx2890 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1324 = load i64, i64* %arrayidx2890, align 16
  %add2891 = add i64 %1323, %1324
  %arrayidx2892 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add2891, i64* %arrayidx2892, align 16
  %arrayidx2893 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1325 = load i64, i64* %arrayidx2893, align 16
  %arrayidx2894 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1326 = load i64, i64* %arrayidx2894, align 16
  %xor2895 = xor i64 %1325, %1326
  %call2896 = call i64 @rotr64(i64 %xor2895, i32 63)
  %arrayidx2897 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2896, i64* %arrayidx2897, align 16
  br label %do.end2898

do.end2898:                                       ; preds = %do.body2855
  br label %do.body2899

do.body2899:                                      ; preds = %do.end2898
  %arrayidx2900 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1327 = load i64, i64* %arrayidx2900, align 8
  %arrayidx2901 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1328 = load i64, i64* %arrayidx2901, align 8
  %add2902 = add i64 %1327, %1328
  %1329 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 2), align 2
  %idxprom2903 = zext i8 %1329 to i32
  %arrayidx2904 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2903
  %1330 = load i64, i64* %arrayidx2904, align 8
  %add2905 = add i64 %add2902, %1330
  %arrayidx2906 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2905, i64* %arrayidx2906, align 8
  %arrayidx2907 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1331 = load i64, i64* %arrayidx2907, align 8
  %arrayidx2908 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1332 = load i64, i64* %arrayidx2908, align 8
  %xor2909 = xor i64 %1331, %1332
  %call2910 = call i64 @rotr64(i64 %xor2909, i32 32)
  %arrayidx2911 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2910, i64* %arrayidx2911, align 8
  %arrayidx2912 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1333 = load i64, i64* %arrayidx2912, align 8
  %arrayidx2913 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1334 = load i64, i64* %arrayidx2913, align 8
  %add2914 = add i64 %1333, %1334
  %arrayidx2915 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2914, i64* %arrayidx2915, align 8
  %arrayidx2916 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1335 = load i64, i64* %arrayidx2916, align 8
  %arrayidx2917 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1336 = load i64, i64* %arrayidx2917, align 8
  %xor2918 = xor i64 %1335, %1336
  %call2919 = call i64 @rotr64(i64 %xor2918, i32 24)
  %arrayidx2920 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2919, i64* %arrayidx2920, align 8
  %arrayidx2921 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1337 = load i64, i64* %arrayidx2921, align 8
  %arrayidx2922 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1338 = load i64, i64* %arrayidx2922, align 8
  %add2923 = add i64 %1337, %1338
  %1339 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 3), align 1
  %idxprom2924 = zext i8 %1339 to i32
  %arrayidx2925 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2924
  %1340 = load i64, i64* %arrayidx2925, align 8
  %add2926 = add i64 %add2923, %1340
  %arrayidx2927 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add2926, i64* %arrayidx2927, align 8
  %arrayidx2928 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1341 = load i64, i64* %arrayidx2928, align 8
  %arrayidx2929 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1342 = load i64, i64* %arrayidx2929, align 8
  %xor2930 = xor i64 %1341, %1342
  %call2931 = call i64 @rotr64(i64 %xor2930, i32 16)
  %arrayidx2932 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2931, i64* %arrayidx2932, align 8
  %arrayidx2933 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1343 = load i64, i64* %arrayidx2933, align 8
  %arrayidx2934 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1344 = load i64, i64* %arrayidx2934, align 8
  %add2935 = add i64 %1343, %1344
  %arrayidx2936 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add2935, i64* %arrayidx2936, align 8
  %arrayidx2937 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1345 = load i64, i64* %arrayidx2937, align 8
  %arrayidx2938 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1346 = load i64, i64* %arrayidx2938, align 8
  %xor2939 = xor i64 %1345, %1346
  %call2940 = call i64 @rotr64(i64 %xor2939, i32 63)
  %arrayidx2941 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2940, i64* %arrayidx2941, align 8
  br label %do.end2942

do.end2942:                                       ; preds = %do.body2899
  br label %do.body2943

do.body2943:                                      ; preds = %do.end2942
  %arrayidx2944 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1347 = load i64, i64* %arrayidx2944, align 16
  %arrayidx2945 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1348 = load i64, i64* %arrayidx2945, align 16
  %add2946 = add i64 %1347, %1348
  %1349 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 4), align 4
  %idxprom2947 = zext i8 %1349 to i32
  %arrayidx2948 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2947
  %1350 = load i64, i64* %arrayidx2948, align 8
  %add2949 = add i64 %add2946, %1350
  %arrayidx2950 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2949, i64* %arrayidx2950, align 16
  %arrayidx2951 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1351 = load i64, i64* %arrayidx2951, align 16
  %arrayidx2952 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1352 = load i64, i64* %arrayidx2952, align 16
  %xor2953 = xor i64 %1351, %1352
  %call2954 = call i64 @rotr64(i64 %xor2953, i32 32)
  %arrayidx2955 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2954, i64* %arrayidx2955, align 16
  %arrayidx2956 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1353 = load i64, i64* %arrayidx2956, align 16
  %arrayidx2957 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1354 = load i64, i64* %arrayidx2957, align 16
  %add2958 = add i64 %1353, %1354
  %arrayidx2959 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2958, i64* %arrayidx2959, align 16
  %arrayidx2960 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1355 = load i64, i64* %arrayidx2960, align 16
  %arrayidx2961 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1356 = load i64, i64* %arrayidx2961, align 16
  %xor2962 = xor i64 %1355, %1356
  %call2963 = call i64 @rotr64(i64 %xor2962, i32 24)
  %arrayidx2964 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2963, i64* %arrayidx2964, align 16
  %arrayidx2965 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1357 = load i64, i64* %arrayidx2965, align 16
  %arrayidx2966 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1358 = load i64, i64* %arrayidx2966, align 16
  %add2967 = add i64 %1357, %1358
  %1359 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 5), align 1
  %idxprom2968 = zext i8 %1359 to i32
  %arrayidx2969 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2968
  %1360 = load i64, i64* %arrayidx2969, align 8
  %add2970 = add i64 %add2967, %1360
  %arrayidx2971 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add2970, i64* %arrayidx2971, align 16
  %arrayidx2972 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1361 = load i64, i64* %arrayidx2972, align 16
  %arrayidx2973 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1362 = load i64, i64* %arrayidx2973, align 16
  %xor2974 = xor i64 %1361, %1362
  %call2975 = call i64 @rotr64(i64 %xor2974, i32 16)
  %arrayidx2976 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2975, i64* %arrayidx2976, align 16
  %arrayidx2977 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1363 = load i64, i64* %arrayidx2977, align 16
  %arrayidx2978 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1364 = load i64, i64* %arrayidx2978, align 16
  %add2979 = add i64 %1363, %1364
  %arrayidx2980 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add2979, i64* %arrayidx2980, align 16
  %arrayidx2981 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1365 = load i64, i64* %arrayidx2981, align 16
  %arrayidx2982 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1366 = load i64, i64* %arrayidx2982, align 16
  %xor2983 = xor i64 %1365, %1366
  %call2984 = call i64 @rotr64(i64 %xor2983, i32 63)
  %arrayidx2985 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2984, i64* %arrayidx2985, align 16
  br label %do.end2986

do.end2986:                                       ; preds = %do.body2943
  br label %do.body2987

do.body2987:                                      ; preds = %do.end2986
  %arrayidx2988 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1367 = load i64, i64* %arrayidx2988, align 8
  %arrayidx2989 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1368 = load i64, i64* %arrayidx2989, align 8
  %add2990 = add i64 %1367, %1368
  %1369 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 6), align 2
  %idxprom2991 = zext i8 %1369 to i32
  %arrayidx2992 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2991
  %1370 = load i64, i64* %arrayidx2992, align 8
  %add2993 = add i64 %add2990, %1370
  %arrayidx2994 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add2993, i64* %arrayidx2994, align 8
  %arrayidx2995 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1371 = load i64, i64* %arrayidx2995, align 8
  %arrayidx2996 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1372 = load i64, i64* %arrayidx2996, align 8
  %xor2997 = xor i64 %1371, %1372
  %call2998 = call i64 @rotr64(i64 %xor2997, i32 32)
  %arrayidx2999 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2998, i64* %arrayidx2999, align 8
  %arrayidx3000 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1373 = load i64, i64* %arrayidx3000, align 8
  %arrayidx3001 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1374 = load i64, i64* %arrayidx3001, align 8
  %add3002 = add i64 %1373, %1374
  %arrayidx3003 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3002, i64* %arrayidx3003, align 8
  %arrayidx3004 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1375 = load i64, i64* %arrayidx3004, align 8
  %arrayidx3005 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1376 = load i64, i64* %arrayidx3005, align 8
  %xor3006 = xor i64 %1375, %1376
  %call3007 = call i64 @rotr64(i64 %xor3006, i32 24)
  %arrayidx3008 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3007, i64* %arrayidx3008, align 8
  %arrayidx3009 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1377 = load i64, i64* %arrayidx3009, align 8
  %arrayidx3010 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1378 = load i64, i64* %arrayidx3010, align 8
  %add3011 = add i64 %1377, %1378
  %1379 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 7), align 1
  %idxprom3012 = zext i8 %1379 to i32
  %arrayidx3013 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3012
  %1380 = load i64, i64* %arrayidx3013, align 8
  %add3014 = add i64 %add3011, %1380
  %arrayidx3015 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3014, i64* %arrayidx3015, align 8
  %arrayidx3016 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1381 = load i64, i64* %arrayidx3016, align 8
  %arrayidx3017 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1382 = load i64, i64* %arrayidx3017, align 8
  %xor3018 = xor i64 %1381, %1382
  %call3019 = call i64 @rotr64(i64 %xor3018, i32 16)
  %arrayidx3020 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3019, i64* %arrayidx3020, align 8
  %arrayidx3021 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1383 = load i64, i64* %arrayidx3021, align 8
  %arrayidx3022 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1384 = load i64, i64* %arrayidx3022, align 8
  %add3023 = add i64 %1383, %1384
  %arrayidx3024 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3023, i64* %arrayidx3024, align 8
  %arrayidx3025 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1385 = load i64, i64* %arrayidx3025, align 8
  %arrayidx3026 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1386 = load i64, i64* %arrayidx3026, align 8
  %xor3027 = xor i64 %1385, %1386
  %call3028 = call i64 @rotr64(i64 %xor3027, i32 63)
  %arrayidx3029 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3028, i64* %arrayidx3029, align 8
  br label %do.end3030

do.end3030:                                       ; preds = %do.body2987
  br label %do.body3031

do.body3031:                                      ; preds = %do.end3030
  %arrayidx3032 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1387 = load i64, i64* %arrayidx3032, align 16
  %arrayidx3033 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1388 = load i64, i64* %arrayidx3033, align 8
  %add3034 = add i64 %1387, %1388
  %1389 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 8), align 8
  %idxprom3035 = zext i8 %1389 to i32
  %arrayidx3036 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3035
  %1390 = load i64, i64* %arrayidx3036, align 8
  %add3037 = add i64 %add3034, %1390
  %arrayidx3038 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3037, i64* %arrayidx3038, align 16
  %arrayidx3039 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1391 = load i64, i64* %arrayidx3039, align 8
  %arrayidx3040 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1392 = load i64, i64* %arrayidx3040, align 16
  %xor3041 = xor i64 %1391, %1392
  %call3042 = call i64 @rotr64(i64 %xor3041, i32 32)
  %arrayidx3043 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3042, i64* %arrayidx3043, align 8
  %arrayidx3044 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1393 = load i64, i64* %arrayidx3044, align 16
  %arrayidx3045 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1394 = load i64, i64* %arrayidx3045, align 8
  %add3046 = add i64 %1393, %1394
  %arrayidx3047 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3046, i64* %arrayidx3047, align 16
  %arrayidx3048 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1395 = load i64, i64* %arrayidx3048, align 8
  %arrayidx3049 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1396 = load i64, i64* %arrayidx3049, align 16
  %xor3050 = xor i64 %1395, %1396
  %call3051 = call i64 @rotr64(i64 %xor3050, i32 24)
  %arrayidx3052 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3051, i64* %arrayidx3052, align 8
  %arrayidx3053 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1397 = load i64, i64* %arrayidx3053, align 16
  %arrayidx3054 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1398 = load i64, i64* %arrayidx3054, align 8
  %add3055 = add i64 %1397, %1398
  %1399 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 9), align 1
  %idxprom3056 = zext i8 %1399 to i32
  %arrayidx3057 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3056
  %1400 = load i64, i64* %arrayidx3057, align 8
  %add3058 = add i64 %add3055, %1400
  %arrayidx3059 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3058, i64* %arrayidx3059, align 16
  %arrayidx3060 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1401 = load i64, i64* %arrayidx3060, align 8
  %arrayidx3061 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1402 = load i64, i64* %arrayidx3061, align 16
  %xor3062 = xor i64 %1401, %1402
  %call3063 = call i64 @rotr64(i64 %xor3062, i32 16)
  %arrayidx3064 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3063, i64* %arrayidx3064, align 8
  %arrayidx3065 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1403 = load i64, i64* %arrayidx3065, align 16
  %arrayidx3066 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1404 = load i64, i64* %arrayidx3066, align 8
  %add3067 = add i64 %1403, %1404
  %arrayidx3068 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3067, i64* %arrayidx3068, align 16
  %arrayidx3069 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1405 = load i64, i64* %arrayidx3069, align 8
  %arrayidx3070 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1406 = load i64, i64* %arrayidx3070, align 16
  %xor3071 = xor i64 %1405, %1406
  %call3072 = call i64 @rotr64(i64 %xor3071, i32 63)
  %arrayidx3073 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3072, i64* %arrayidx3073, align 8
  br label %do.end3074

do.end3074:                                       ; preds = %do.body3031
  br label %do.body3075

do.body3075:                                      ; preds = %do.end3074
  %arrayidx3076 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1407 = load i64, i64* %arrayidx3076, align 8
  %arrayidx3077 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1408 = load i64, i64* %arrayidx3077, align 16
  %add3078 = add i64 %1407, %1408
  %1409 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 10), align 2
  %idxprom3079 = zext i8 %1409 to i32
  %arrayidx3080 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3079
  %1410 = load i64, i64* %arrayidx3080, align 8
  %add3081 = add i64 %add3078, %1410
  %arrayidx3082 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3081, i64* %arrayidx3082, align 8
  %arrayidx3083 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1411 = load i64, i64* %arrayidx3083, align 16
  %arrayidx3084 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1412 = load i64, i64* %arrayidx3084, align 8
  %xor3085 = xor i64 %1411, %1412
  %call3086 = call i64 @rotr64(i64 %xor3085, i32 32)
  %arrayidx3087 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3086, i64* %arrayidx3087, align 16
  %arrayidx3088 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1413 = load i64, i64* %arrayidx3088, align 8
  %arrayidx3089 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1414 = load i64, i64* %arrayidx3089, align 16
  %add3090 = add i64 %1413, %1414
  %arrayidx3091 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3090, i64* %arrayidx3091, align 8
  %arrayidx3092 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1415 = load i64, i64* %arrayidx3092, align 16
  %arrayidx3093 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1416 = load i64, i64* %arrayidx3093, align 8
  %xor3094 = xor i64 %1415, %1416
  %call3095 = call i64 @rotr64(i64 %xor3094, i32 24)
  %arrayidx3096 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3095, i64* %arrayidx3096, align 16
  %arrayidx3097 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1417 = load i64, i64* %arrayidx3097, align 8
  %arrayidx3098 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1418 = load i64, i64* %arrayidx3098, align 16
  %add3099 = add i64 %1417, %1418
  %1419 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 11), align 1
  %idxprom3100 = zext i8 %1419 to i32
  %arrayidx3101 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3100
  %1420 = load i64, i64* %arrayidx3101, align 8
  %add3102 = add i64 %add3099, %1420
  %arrayidx3103 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3102, i64* %arrayidx3103, align 8
  %arrayidx3104 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1421 = load i64, i64* %arrayidx3104, align 16
  %arrayidx3105 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1422 = load i64, i64* %arrayidx3105, align 8
  %xor3106 = xor i64 %1421, %1422
  %call3107 = call i64 @rotr64(i64 %xor3106, i32 16)
  %arrayidx3108 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3107, i64* %arrayidx3108, align 16
  %arrayidx3109 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1423 = load i64, i64* %arrayidx3109, align 8
  %arrayidx3110 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1424 = load i64, i64* %arrayidx3110, align 16
  %add3111 = add i64 %1423, %1424
  %arrayidx3112 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3111, i64* %arrayidx3112, align 8
  %arrayidx3113 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1425 = load i64, i64* %arrayidx3113, align 16
  %arrayidx3114 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1426 = load i64, i64* %arrayidx3114, align 8
  %xor3115 = xor i64 %1425, %1426
  %call3116 = call i64 @rotr64(i64 %xor3115, i32 63)
  %arrayidx3117 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3116, i64* %arrayidx3117, align 16
  br label %do.end3118

do.end3118:                                       ; preds = %do.body3075
  br label %do.body3119

do.body3119:                                      ; preds = %do.end3118
  %arrayidx3120 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1427 = load i64, i64* %arrayidx3120, align 16
  %arrayidx3121 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1428 = load i64, i64* %arrayidx3121, align 8
  %add3122 = add i64 %1427, %1428
  %1429 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 12), align 4
  %idxprom3123 = zext i8 %1429 to i32
  %arrayidx3124 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3123
  %1430 = load i64, i64* %arrayidx3124, align 8
  %add3125 = add i64 %add3122, %1430
  %arrayidx3126 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3125, i64* %arrayidx3126, align 16
  %arrayidx3127 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1431 = load i64, i64* %arrayidx3127, align 8
  %arrayidx3128 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1432 = load i64, i64* %arrayidx3128, align 16
  %xor3129 = xor i64 %1431, %1432
  %call3130 = call i64 @rotr64(i64 %xor3129, i32 32)
  %arrayidx3131 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3130, i64* %arrayidx3131, align 8
  %arrayidx3132 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1433 = load i64, i64* %arrayidx3132, align 16
  %arrayidx3133 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1434 = load i64, i64* %arrayidx3133, align 8
  %add3134 = add i64 %1433, %1434
  %arrayidx3135 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3134, i64* %arrayidx3135, align 16
  %arrayidx3136 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1435 = load i64, i64* %arrayidx3136, align 8
  %arrayidx3137 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1436 = load i64, i64* %arrayidx3137, align 16
  %xor3138 = xor i64 %1435, %1436
  %call3139 = call i64 @rotr64(i64 %xor3138, i32 24)
  %arrayidx3140 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3139, i64* %arrayidx3140, align 8
  %arrayidx3141 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1437 = load i64, i64* %arrayidx3141, align 16
  %arrayidx3142 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1438 = load i64, i64* %arrayidx3142, align 8
  %add3143 = add i64 %1437, %1438
  %1439 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 13), align 1
  %idxprom3144 = zext i8 %1439 to i32
  %arrayidx3145 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3144
  %1440 = load i64, i64* %arrayidx3145, align 8
  %add3146 = add i64 %add3143, %1440
  %arrayidx3147 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3146, i64* %arrayidx3147, align 16
  %arrayidx3148 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1441 = load i64, i64* %arrayidx3148, align 8
  %arrayidx3149 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1442 = load i64, i64* %arrayidx3149, align 16
  %xor3150 = xor i64 %1441, %1442
  %call3151 = call i64 @rotr64(i64 %xor3150, i32 16)
  %arrayidx3152 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3151, i64* %arrayidx3152, align 8
  %arrayidx3153 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1443 = load i64, i64* %arrayidx3153, align 16
  %arrayidx3154 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1444 = load i64, i64* %arrayidx3154, align 8
  %add3155 = add i64 %1443, %1444
  %arrayidx3156 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3155, i64* %arrayidx3156, align 16
  %arrayidx3157 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1445 = load i64, i64* %arrayidx3157, align 8
  %arrayidx3158 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1446 = load i64, i64* %arrayidx3158, align 16
  %xor3159 = xor i64 %1445, %1446
  %call3160 = call i64 @rotr64(i64 %xor3159, i32 63)
  %arrayidx3161 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3160, i64* %arrayidx3161, align 8
  br label %do.end3162

do.end3162:                                       ; preds = %do.body3119
  br label %do.body3163

do.body3163:                                      ; preds = %do.end3162
  %arrayidx3164 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1447 = load i64, i64* %arrayidx3164, align 8
  %arrayidx3165 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1448 = load i64, i64* %arrayidx3165, align 16
  %add3166 = add i64 %1447, %1448
  %1449 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 14), align 2
  %idxprom3167 = zext i8 %1449 to i32
  %arrayidx3168 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3167
  %1450 = load i64, i64* %arrayidx3168, align 8
  %add3169 = add i64 %add3166, %1450
  %arrayidx3170 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3169, i64* %arrayidx3170, align 8
  %arrayidx3171 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1451 = load i64, i64* %arrayidx3171, align 16
  %arrayidx3172 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1452 = load i64, i64* %arrayidx3172, align 8
  %xor3173 = xor i64 %1451, %1452
  %call3174 = call i64 @rotr64(i64 %xor3173, i32 32)
  %arrayidx3175 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3174, i64* %arrayidx3175, align 16
  %arrayidx3176 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1453 = load i64, i64* %arrayidx3176, align 8
  %arrayidx3177 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1454 = load i64, i64* %arrayidx3177, align 16
  %add3178 = add i64 %1453, %1454
  %arrayidx3179 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3178, i64* %arrayidx3179, align 8
  %arrayidx3180 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1455 = load i64, i64* %arrayidx3180, align 16
  %arrayidx3181 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1456 = load i64, i64* %arrayidx3181, align 8
  %xor3182 = xor i64 %1455, %1456
  %call3183 = call i64 @rotr64(i64 %xor3182, i32 24)
  %arrayidx3184 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3183, i64* %arrayidx3184, align 16
  %arrayidx3185 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1457 = load i64, i64* %arrayidx3185, align 8
  %arrayidx3186 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1458 = load i64, i64* %arrayidx3186, align 16
  %add3187 = add i64 %1457, %1458
  %1459 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 15), align 1
  %idxprom3188 = zext i8 %1459 to i32
  %arrayidx3189 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3188
  %1460 = load i64, i64* %arrayidx3189, align 8
  %add3190 = add i64 %add3187, %1460
  %arrayidx3191 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3190, i64* %arrayidx3191, align 8
  %arrayidx3192 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1461 = load i64, i64* %arrayidx3192, align 16
  %arrayidx3193 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1462 = load i64, i64* %arrayidx3193, align 8
  %xor3194 = xor i64 %1461, %1462
  %call3195 = call i64 @rotr64(i64 %xor3194, i32 16)
  %arrayidx3196 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3195, i64* %arrayidx3196, align 16
  %arrayidx3197 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1463 = load i64, i64* %arrayidx3197, align 8
  %arrayidx3198 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1464 = load i64, i64* %arrayidx3198, align 16
  %add3199 = add i64 %1463, %1464
  %arrayidx3200 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3199, i64* %arrayidx3200, align 8
  %arrayidx3201 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1465 = load i64, i64* %arrayidx3201, align 16
  %arrayidx3202 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1466 = load i64, i64* %arrayidx3202, align 8
  %xor3203 = xor i64 %1465, %1466
  %call3204 = call i64 @rotr64(i64 %xor3203, i32 63)
  %arrayidx3205 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3204, i64* %arrayidx3205, align 16
  br label %do.end3206

do.end3206:                                       ; preds = %do.body3163
  br label %do.end3207

do.end3207:                                       ; preds = %do.end3206
  br label %do.body3208

do.body3208:                                      ; preds = %do.end3207
  br label %do.body3209

do.body3209:                                      ; preds = %do.body3208
  %arrayidx3210 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1467 = load i64, i64* %arrayidx3210, align 16
  %arrayidx3211 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1468 = load i64, i64* %arrayidx3211, align 16
  %add3212 = add i64 %1467, %1468
  %1469 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 0), align 16
  %idxprom3213 = zext i8 %1469 to i32
  %arrayidx3214 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3213
  %1470 = load i64, i64* %arrayidx3214, align 8
  %add3215 = add i64 %add3212, %1470
  %arrayidx3216 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3215, i64* %arrayidx3216, align 16
  %arrayidx3217 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1471 = load i64, i64* %arrayidx3217, align 16
  %arrayidx3218 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1472 = load i64, i64* %arrayidx3218, align 16
  %xor3219 = xor i64 %1471, %1472
  %call3220 = call i64 @rotr64(i64 %xor3219, i32 32)
  %arrayidx3221 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3220, i64* %arrayidx3221, align 16
  %arrayidx3222 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1473 = load i64, i64* %arrayidx3222, align 16
  %arrayidx3223 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1474 = load i64, i64* %arrayidx3223, align 16
  %add3224 = add i64 %1473, %1474
  %arrayidx3225 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3224, i64* %arrayidx3225, align 16
  %arrayidx3226 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1475 = load i64, i64* %arrayidx3226, align 16
  %arrayidx3227 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1476 = load i64, i64* %arrayidx3227, align 16
  %xor3228 = xor i64 %1475, %1476
  %call3229 = call i64 @rotr64(i64 %xor3228, i32 24)
  %arrayidx3230 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3229, i64* %arrayidx3230, align 16
  %arrayidx3231 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1477 = load i64, i64* %arrayidx3231, align 16
  %arrayidx3232 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1478 = load i64, i64* %arrayidx3232, align 16
  %add3233 = add i64 %1477, %1478
  %1479 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 1), align 1
  %idxprom3234 = zext i8 %1479 to i32
  %arrayidx3235 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3234
  %1480 = load i64, i64* %arrayidx3235, align 8
  %add3236 = add i64 %add3233, %1480
  %arrayidx3237 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3236, i64* %arrayidx3237, align 16
  %arrayidx3238 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1481 = load i64, i64* %arrayidx3238, align 16
  %arrayidx3239 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1482 = load i64, i64* %arrayidx3239, align 16
  %xor3240 = xor i64 %1481, %1482
  %call3241 = call i64 @rotr64(i64 %xor3240, i32 16)
  %arrayidx3242 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3241, i64* %arrayidx3242, align 16
  %arrayidx3243 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1483 = load i64, i64* %arrayidx3243, align 16
  %arrayidx3244 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1484 = load i64, i64* %arrayidx3244, align 16
  %add3245 = add i64 %1483, %1484
  %arrayidx3246 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3245, i64* %arrayidx3246, align 16
  %arrayidx3247 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1485 = load i64, i64* %arrayidx3247, align 16
  %arrayidx3248 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1486 = load i64, i64* %arrayidx3248, align 16
  %xor3249 = xor i64 %1485, %1486
  %call3250 = call i64 @rotr64(i64 %xor3249, i32 63)
  %arrayidx3251 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3250, i64* %arrayidx3251, align 16
  br label %do.end3252

do.end3252:                                       ; preds = %do.body3209
  br label %do.body3253

do.body3253:                                      ; preds = %do.end3252
  %arrayidx3254 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1487 = load i64, i64* %arrayidx3254, align 8
  %arrayidx3255 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1488 = load i64, i64* %arrayidx3255, align 8
  %add3256 = add i64 %1487, %1488
  %1489 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 2), align 2
  %idxprom3257 = zext i8 %1489 to i32
  %arrayidx3258 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3257
  %1490 = load i64, i64* %arrayidx3258, align 8
  %add3259 = add i64 %add3256, %1490
  %arrayidx3260 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3259, i64* %arrayidx3260, align 8
  %arrayidx3261 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1491 = load i64, i64* %arrayidx3261, align 8
  %arrayidx3262 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1492 = load i64, i64* %arrayidx3262, align 8
  %xor3263 = xor i64 %1491, %1492
  %call3264 = call i64 @rotr64(i64 %xor3263, i32 32)
  %arrayidx3265 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3264, i64* %arrayidx3265, align 8
  %arrayidx3266 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1493 = load i64, i64* %arrayidx3266, align 8
  %arrayidx3267 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1494 = load i64, i64* %arrayidx3267, align 8
  %add3268 = add i64 %1493, %1494
  %arrayidx3269 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3268, i64* %arrayidx3269, align 8
  %arrayidx3270 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1495 = load i64, i64* %arrayidx3270, align 8
  %arrayidx3271 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1496 = load i64, i64* %arrayidx3271, align 8
  %xor3272 = xor i64 %1495, %1496
  %call3273 = call i64 @rotr64(i64 %xor3272, i32 24)
  %arrayidx3274 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3273, i64* %arrayidx3274, align 8
  %arrayidx3275 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1497 = load i64, i64* %arrayidx3275, align 8
  %arrayidx3276 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1498 = load i64, i64* %arrayidx3276, align 8
  %add3277 = add i64 %1497, %1498
  %1499 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 3), align 1
  %idxprom3278 = zext i8 %1499 to i32
  %arrayidx3279 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3278
  %1500 = load i64, i64* %arrayidx3279, align 8
  %add3280 = add i64 %add3277, %1500
  %arrayidx3281 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3280, i64* %arrayidx3281, align 8
  %arrayidx3282 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1501 = load i64, i64* %arrayidx3282, align 8
  %arrayidx3283 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1502 = load i64, i64* %arrayidx3283, align 8
  %xor3284 = xor i64 %1501, %1502
  %call3285 = call i64 @rotr64(i64 %xor3284, i32 16)
  %arrayidx3286 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3285, i64* %arrayidx3286, align 8
  %arrayidx3287 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1503 = load i64, i64* %arrayidx3287, align 8
  %arrayidx3288 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1504 = load i64, i64* %arrayidx3288, align 8
  %add3289 = add i64 %1503, %1504
  %arrayidx3290 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3289, i64* %arrayidx3290, align 8
  %arrayidx3291 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1505 = load i64, i64* %arrayidx3291, align 8
  %arrayidx3292 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1506 = load i64, i64* %arrayidx3292, align 8
  %xor3293 = xor i64 %1505, %1506
  %call3294 = call i64 @rotr64(i64 %xor3293, i32 63)
  %arrayidx3295 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3294, i64* %arrayidx3295, align 8
  br label %do.end3296

do.end3296:                                       ; preds = %do.body3253
  br label %do.body3297

do.body3297:                                      ; preds = %do.end3296
  %arrayidx3298 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1507 = load i64, i64* %arrayidx3298, align 16
  %arrayidx3299 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1508 = load i64, i64* %arrayidx3299, align 16
  %add3300 = add i64 %1507, %1508
  %1509 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 4), align 4
  %idxprom3301 = zext i8 %1509 to i32
  %arrayidx3302 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3301
  %1510 = load i64, i64* %arrayidx3302, align 8
  %add3303 = add i64 %add3300, %1510
  %arrayidx3304 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3303, i64* %arrayidx3304, align 16
  %arrayidx3305 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1511 = load i64, i64* %arrayidx3305, align 16
  %arrayidx3306 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1512 = load i64, i64* %arrayidx3306, align 16
  %xor3307 = xor i64 %1511, %1512
  %call3308 = call i64 @rotr64(i64 %xor3307, i32 32)
  %arrayidx3309 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3308, i64* %arrayidx3309, align 16
  %arrayidx3310 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1513 = load i64, i64* %arrayidx3310, align 16
  %arrayidx3311 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1514 = load i64, i64* %arrayidx3311, align 16
  %add3312 = add i64 %1513, %1514
  %arrayidx3313 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3312, i64* %arrayidx3313, align 16
  %arrayidx3314 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1515 = load i64, i64* %arrayidx3314, align 16
  %arrayidx3315 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1516 = load i64, i64* %arrayidx3315, align 16
  %xor3316 = xor i64 %1515, %1516
  %call3317 = call i64 @rotr64(i64 %xor3316, i32 24)
  %arrayidx3318 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3317, i64* %arrayidx3318, align 16
  %arrayidx3319 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1517 = load i64, i64* %arrayidx3319, align 16
  %arrayidx3320 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1518 = load i64, i64* %arrayidx3320, align 16
  %add3321 = add i64 %1517, %1518
  %1519 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 5), align 1
  %idxprom3322 = zext i8 %1519 to i32
  %arrayidx3323 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3322
  %1520 = load i64, i64* %arrayidx3323, align 8
  %add3324 = add i64 %add3321, %1520
  %arrayidx3325 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3324, i64* %arrayidx3325, align 16
  %arrayidx3326 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1521 = load i64, i64* %arrayidx3326, align 16
  %arrayidx3327 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1522 = load i64, i64* %arrayidx3327, align 16
  %xor3328 = xor i64 %1521, %1522
  %call3329 = call i64 @rotr64(i64 %xor3328, i32 16)
  %arrayidx3330 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3329, i64* %arrayidx3330, align 16
  %arrayidx3331 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1523 = load i64, i64* %arrayidx3331, align 16
  %arrayidx3332 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1524 = load i64, i64* %arrayidx3332, align 16
  %add3333 = add i64 %1523, %1524
  %arrayidx3334 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3333, i64* %arrayidx3334, align 16
  %arrayidx3335 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1525 = load i64, i64* %arrayidx3335, align 16
  %arrayidx3336 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1526 = load i64, i64* %arrayidx3336, align 16
  %xor3337 = xor i64 %1525, %1526
  %call3338 = call i64 @rotr64(i64 %xor3337, i32 63)
  %arrayidx3339 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3338, i64* %arrayidx3339, align 16
  br label %do.end3340

do.end3340:                                       ; preds = %do.body3297
  br label %do.body3341

do.body3341:                                      ; preds = %do.end3340
  %arrayidx3342 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1527 = load i64, i64* %arrayidx3342, align 8
  %arrayidx3343 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1528 = load i64, i64* %arrayidx3343, align 8
  %add3344 = add i64 %1527, %1528
  %1529 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 6), align 2
  %idxprom3345 = zext i8 %1529 to i32
  %arrayidx3346 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3345
  %1530 = load i64, i64* %arrayidx3346, align 8
  %add3347 = add i64 %add3344, %1530
  %arrayidx3348 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3347, i64* %arrayidx3348, align 8
  %arrayidx3349 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1531 = load i64, i64* %arrayidx3349, align 8
  %arrayidx3350 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1532 = load i64, i64* %arrayidx3350, align 8
  %xor3351 = xor i64 %1531, %1532
  %call3352 = call i64 @rotr64(i64 %xor3351, i32 32)
  %arrayidx3353 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3352, i64* %arrayidx3353, align 8
  %arrayidx3354 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1533 = load i64, i64* %arrayidx3354, align 8
  %arrayidx3355 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1534 = load i64, i64* %arrayidx3355, align 8
  %add3356 = add i64 %1533, %1534
  %arrayidx3357 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3356, i64* %arrayidx3357, align 8
  %arrayidx3358 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1535 = load i64, i64* %arrayidx3358, align 8
  %arrayidx3359 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1536 = load i64, i64* %arrayidx3359, align 8
  %xor3360 = xor i64 %1535, %1536
  %call3361 = call i64 @rotr64(i64 %xor3360, i32 24)
  %arrayidx3362 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3361, i64* %arrayidx3362, align 8
  %arrayidx3363 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1537 = load i64, i64* %arrayidx3363, align 8
  %arrayidx3364 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1538 = load i64, i64* %arrayidx3364, align 8
  %add3365 = add i64 %1537, %1538
  %1539 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 7), align 1
  %idxprom3366 = zext i8 %1539 to i32
  %arrayidx3367 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3366
  %1540 = load i64, i64* %arrayidx3367, align 8
  %add3368 = add i64 %add3365, %1540
  %arrayidx3369 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3368, i64* %arrayidx3369, align 8
  %arrayidx3370 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1541 = load i64, i64* %arrayidx3370, align 8
  %arrayidx3371 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1542 = load i64, i64* %arrayidx3371, align 8
  %xor3372 = xor i64 %1541, %1542
  %call3373 = call i64 @rotr64(i64 %xor3372, i32 16)
  %arrayidx3374 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3373, i64* %arrayidx3374, align 8
  %arrayidx3375 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1543 = load i64, i64* %arrayidx3375, align 8
  %arrayidx3376 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1544 = load i64, i64* %arrayidx3376, align 8
  %add3377 = add i64 %1543, %1544
  %arrayidx3378 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3377, i64* %arrayidx3378, align 8
  %arrayidx3379 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1545 = load i64, i64* %arrayidx3379, align 8
  %arrayidx3380 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1546 = load i64, i64* %arrayidx3380, align 8
  %xor3381 = xor i64 %1545, %1546
  %call3382 = call i64 @rotr64(i64 %xor3381, i32 63)
  %arrayidx3383 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3382, i64* %arrayidx3383, align 8
  br label %do.end3384

do.end3384:                                       ; preds = %do.body3341
  br label %do.body3385

do.body3385:                                      ; preds = %do.end3384
  %arrayidx3386 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1547 = load i64, i64* %arrayidx3386, align 16
  %arrayidx3387 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1548 = load i64, i64* %arrayidx3387, align 8
  %add3388 = add i64 %1547, %1548
  %1549 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 8), align 8
  %idxprom3389 = zext i8 %1549 to i32
  %arrayidx3390 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3389
  %1550 = load i64, i64* %arrayidx3390, align 8
  %add3391 = add i64 %add3388, %1550
  %arrayidx3392 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3391, i64* %arrayidx3392, align 16
  %arrayidx3393 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1551 = load i64, i64* %arrayidx3393, align 8
  %arrayidx3394 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1552 = load i64, i64* %arrayidx3394, align 16
  %xor3395 = xor i64 %1551, %1552
  %call3396 = call i64 @rotr64(i64 %xor3395, i32 32)
  %arrayidx3397 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3396, i64* %arrayidx3397, align 8
  %arrayidx3398 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1553 = load i64, i64* %arrayidx3398, align 16
  %arrayidx3399 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1554 = load i64, i64* %arrayidx3399, align 8
  %add3400 = add i64 %1553, %1554
  %arrayidx3401 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3400, i64* %arrayidx3401, align 16
  %arrayidx3402 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1555 = load i64, i64* %arrayidx3402, align 8
  %arrayidx3403 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1556 = load i64, i64* %arrayidx3403, align 16
  %xor3404 = xor i64 %1555, %1556
  %call3405 = call i64 @rotr64(i64 %xor3404, i32 24)
  %arrayidx3406 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3405, i64* %arrayidx3406, align 8
  %arrayidx3407 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1557 = load i64, i64* %arrayidx3407, align 16
  %arrayidx3408 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1558 = load i64, i64* %arrayidx3408, align 8
  %add3409 = add i64 %1557, %1558
  %1559 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 9), align 1
  %idxprom3410 = zext i8 %1559 to i32
  %arrayidx3411 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3410
  %1560 = load i64, i64* %arrayidx3411, align 8
  %add3412 = add i64 %add3409, %1560
  %arrayidx3413 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3412, i64* %arrayidx3413, align 16
  %arrayidx3414 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1561 = load i64, i64* %arrayidx3414, align 8
  %arrayidx3415 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1562 = load i64, i64* %arrayidx3415, align 16
  %xor3416 = xor i64 %1561, %1562
  %call3417 = call i64 @rotr64(i64 %xor3416, i32 16)
  %arrayidx3418 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3417, i64* %arrayidx3418, align 8
  %arrayidx3419 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1563 = load i64, i64* %arrayidx3419, align 16
  %arrayidx3420 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1564 = load i64, i64* %arrayidx3420, align 8
  %add3421 = add i64 %1563, %1564
  %arrayidx3422 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3421, i64* %arrayidx3422, align 16
  %arrayidx3423 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1565 = load i64, i64* %arrayidx3423, align 8
  %arrayidx3424 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1566 = load i64, i64* %arrayidx3424, align 16
  %xor3425 = xor i64 %1565, %1566
  %call3426 = call i64 @rotr64(i64 %xor3425, i32 63)
  %arrayidx3427 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3426, i64* %arrayidx3427, align 8
  br label %do.end3428

do.end3428:                                       ; preds = %do.body3385
  br label %do.body3429

do.body3429:                                      ; preds = %do.end3428
  %arrayidx3430 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1567 = load i64, i64* %arrayidx3430, align 8
  %arrayidx3431 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1568 = load i64, i64* %arrayidx3431, align 16
  %add3432 = add i64 %1567, %1568
  %1569 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 10), align 2
  %idxprom3433 = zext i8 %1569 to i32
  %arrayidx3434 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3433
  %1570 = load i64, i64* %arrayidx3434, align 8
  %add3435 = add i64 %add3432, %1570
  %arrayidx3436 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3435, i64* %arrayidx3436, align 8
  %arrayidx3437 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1571 = load i64, i64* %arrayidx3437, align 16
  %arrayidx3438 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1572 = load i64, i64* %arrayidx3438, align 8
  %xor3439 = xor i64 %1571, %1572
  %call3440 = call i64 @rotr64(i64 %xor3439, i32 32)
  %arrayidx3441 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3440, i64* %arrayidx3441, align 16
  %arrayidx3442 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1573 = load i64, i64* %arrayidx3442, align 8
  %arrayidx3443 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1574 = load i64, i64* %arrayidx3443, align 16
  %add3444 = add i64 %1573, %1574
  %arrayidx3445 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3444, i64* %arrayidx3445, align 8
  %arrayidx3446 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1575 = load i64, i64* %arrayidx3446, align 16
  %arrayidx3447 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1576 = load i64, i64* %arrayidx3447, align 8
  %xor3448 = xor i64 %1575, %1576
  %call3449 = call i64 @rotr64(i64 %xor3448, i32 24)
  %arrayidx3450 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3449, i64* %arrayidx3450, align 16
  %arrayidx3451 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1577 = load i64, i64* %arrayidx3451, align 8
  %arrayidx3452 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1578 = load i64, i64* %arrayidx3452, align 16
  %add3453 = add i64 %1577, %1578
  %1579 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 11), align 1
  %idxprom3454 = zext i8 %1579 to i32
  %arrayidx3455 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3454
  %1580 = load i64, i64* %arrayidx3455, align 8
  %add3456 = add i64 %add3453, %1580
  %arrayidx3457 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3456, i64* %arrayidx3457, align 8
  %arrayidx3458 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1581 = load i64, i64* %arrayidx3458, align 16
  %arrayidx3459 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1582 = load i64, i64* %arrayidx3459, align 8
  %xor3460 = xor i64 %1581, %1582
  %call3461 = call i64 @rotr64(i64 %xor3460, i32 16)
  %arrayidx3462 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3461, i64* %arrayidx3462, align 16
  %arrayidx3463 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1583 = load i64, i64* %arrayidx3463, align 8
  %arrayidx3464 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1584 = load i64, i64* %arrayidx3464, align 16
  %add3465 = add i64 %1583, %1584
  %arrayidx3466 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3465, i64* %arrayidx3466, align 8
  %arrayidx3467 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1585 = load i64, i64* %arrayidx3467, align 16
  %arrayidx3468 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1586 = load i64, i64* %arrayidx3468, align 8
  %xor3469 = xor i64 %1585, %1586
  %call3470 = call i64 @rotr64(i64 %xor3469, i32 63)
  %arrayidx3471 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3470, i64* %arrayidx3471, align 16
  br label %do.end3472

do.end3472:                                       ; preds = %do.body3429
  br label %do.body3473

do.body3473:                                      ; preds = %do.end3472
  %arrayidx3474 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1587 = load i64, i64* %arrayidx3474, align 16
  %arrayidx3475 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1588 = load i64, i64* %arrayidx3475, align 8
  %add3476 = add i64 %1587, %1588
  %1589 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 12), align 4
  %idxprom3477 = zext i8 %1589 to i32
  %arrayidx3478 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3477
  %1590 = load i64, i64* %arrayidx3478, align 8
  %add3479 = add i64 %add3476, %1590
  %arrayidx3480 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3479, i64* %arrayidx3480, align 16
  %arrayidx3481 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1591 = load i64, i64* %arrayidx3481, align 8
  %arrayidx3482 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1592 = load i64, i64* %arrayidx3482, align 16
  %xor3483 = xor i64 %1591, %1592
  %call3484 = call i64 @rotr64(i64 %xor3483, i32 32)
  %arrayidx3485 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3484, i64* %arrayidx3485, align 8
  %arrayidx3486 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1593 = load i64, i64* %arrayidx3486, align 16
  %arrayidx3487 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1594 = load i64, i64* %arrayidx3487, align 8
  %add3488 = add i64 %1593, %1594
  %arrayidx3489 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3488, i64* %arrayidx3489, align 16
  %arrayidx3490 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1595 = load i64, i64* %arrayidx3490, align 8
  %arrayidx3491 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1596 = load i64, i64* %arrayidx3491, align 16
  %xor3492 = xor i64 %1595, %1596
  %call3493 = call i64 @rotr64(i64 %xor3492, i32 24)
  %arrayidx3494 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3493, i64* %arrayidx3494, align 8
  %arrayidx3495 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1597 = load i64, i64* %arrayidx3495, align 16
  %arrayidx3496 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1598 = load i64, i64* %arrayidx3496, align 8
  %add3497 = add i64 %1597, %1598
  %1599 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 13), align 1
  %idxprom3498 = zext i8 %1599 to i32
  %arrayidx3499 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3498
  %1600 = load i64, i64* %arrayidx3499, align 8
  %add3500 = add i64 %add3497, %1600
  %arrayidx3501 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3500, i64* %arrayidx3501, align 16
  %arrayidx3502 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1601 = load i64, i64* %arrayidx3502, align 8
  %arrayidx3503 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1602 = load i64, i64* %arrayidx3503, align 16
  %xor3504 = xor i64 %1601, %1602
  %call3505 = call i64 @rotr64(i64 %xor3504, i32 16)
  %arrayidx3506 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3505, i64* %arrayidx3506, align 8
  %arrayidx3507 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1603 = load i64, i64* %arrayidx3507, align 16
  %arrayidx3508 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1604 = load i64, i64* %arrayidx3508, align 8
  %add3509 = add i64 %1603, %1604
  %arrayidx3510 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3509, i64* %arrayidx3510, align 16
  %arrayidx3511 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1605 = load i64, i64* %arrayidx3511, align 8
  %arrayidx3512 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1606 = load i64, i64* %arrayidx3512, align 16
  %xor3513 = xor i64 %1605, %1606
  %call3514 = call i64 @rotr64(i64 %xor3513, i32 63)
  %arrayidx3515 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3514, i64* %arrayidx3515, align 8
  br label %do.end3516

do.end3516:                                       ; preds = %do.body3473
  br label %do.body3517

do.body3517:                                      ; preds = %do.end3516
  %arrayidx3518 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1607 = load i64, i64* %arrayidx3518, align 8
  %arrayidx3519 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1608 = load i64, i64* %arrayidx3519, align 16
  %add3520 = add i64 %1607, %1608
  %1609 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 14), align 2
  %idxprom3521 = zext i8 %1609 to i32
  %arrayidx3522 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3521
  %1610 = load i64, i64* %arrayidx3522, align 8
  %add3523 = add i64 %add3520, %1610
  %arrayidx3524 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3523, i64* %arrayidx3524, align 8
  %arrayidx3525 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1611 = load i64, i64* %arrayidx3525, align 16
  %arrayidx3526 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1612 = load i64, i64* %arrayidx3526, align 8
  %xor3527 = xor i64 %1611, %1612
  %call3528 = call i64 @rotr64(i64 %xor3527, i32 32)
  %arrayidx3529 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3528, i64* %arrayidx3529, align 16
  %arrayidx3530 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1613 = load i64, i64* %arrayidx3530, align 8
  %arrayidx3531 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1614 = load i64, i64* %arrayidx3531, align 16
  %add3532 = add i64 %1613, %1614
  %arrayidx3533 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3532, i64* %arrayidx3533, align 8
  %arrayidx3534 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1615 = load i64, i64* %arrayidx3534, align 16
  %arrayidx3535 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1616 = load i64, i64* %arrayidx3535, align 8
  %xor3536 = xor i64 %1615, %1616
  %call3537 = call i64 @rotr64(i64 %xor3536, i32 24)
  %arrayidx3538 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3537, i64* %arrayidx3538, align 16
  %arrayidx3539 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1617 = load i64, i64* %arrayidx3539, align 8
  %arrayidx3540 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1618 = load i64, i64* %arrayidx3540, align 16
  %add3541 = add i64 %1617, %1618
  %1619 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 15), align 1
  %idxprom3542 = zext i8 %1619 to i32
  %arrayidx3543 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3542
  %1620 = load i64, i64* %arrayidx3543, align 8
  %add3544 = add i64 %add3541, %1620
  %arrayidx3545 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3544, i64* %arrayidx3545, align 8
  %arrayidx3546 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1621 = load i64, i64* %arrayidx3546, align 16
  %arrayidx3547 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1622 = load i64, i64* %arrayidx3547, align 8
  %xor3548 = xor i64 %1621, %1622
  %call3549 = call i64 @rotr64(i64 %xor3548, i32 16)
  %arrayidx3550 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3549, i64* %arrayidx3550, align 16
  %arrayidx3551 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1623 = load i64, i64* %arrayidx3551, align 8
  %arrayidx3552 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1624 = load i64, i64* %arrayidx3552, align 16
  %add3553 = add i64 %1623, %1624
  %arrayidx3554 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3553, i64* %arrayidx3554, align 8
  %arrayidx3555 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1625 = load i64, i64* %arrayidx3555, align 16
  %arrayidx3556 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1626 = load i64, i64* %arrayidx3556, align 8
  %xor3557 = xor i64 %1625, %1626
  %call3558 = call i64 @rotr64(i64 %xor3557, i32 63)
  %arrayidx3559 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3558, i64* %arrayidx3559, align 16
  br label %do.end3560

do.end3560:                                       ; preds = %do.body3517
  br label %do.end3561

do.end3561:                                       ; preds = %do.end3560
  br label %do.body3562

do.body3562:                                      ; preds = %do.end3561
  br label %do.body3563

do.body3563:                                      ; preds = %do.body3562
  %arrayidx3564 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1627 = load i64, i64* %arrayidx3564, align 16
  %arrayidx3565 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1628 = load i64, i64* %arrayidx3565, align 16
  %add3566 = add i64 %1627, %1628
  %1629 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 0), align 16
  %idxprom3567 = zext i8 %1629 to i32
  %arrayidx3568 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3567
  %1630 = load i64, i64* %arrayidx3568, align 8
  %add3569 = add i64 %add3566, %1630
  %arrayidx3570 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3569, i64* %arrayidx3570, align 16
  %arrayidx3571 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1631 = load i64, i64* %arrayidx3571, align 16
  %arrayidx3572 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1632 = load i64, i64* %arrayidx3572, align 16
  %xor3573 = xor i64 %1631, %1632
  %call3574 = call i64 @rotr64(i64 %xor3573, i32 32)
  %arrayidx3575 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3574, i64* %arrayidx3575, align 16
  %arrayidx3576 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1633 = load i64, i64* %arrayidx3576, align 16
  %arrayidx3577 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1634 = load i64, i64* %arrayidx3577, align 16
  %add3578 = add i64 %1633, %1634
  %arrayidx3579 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3578, i64* %arrayidx3579, align 16
  %arrayidx3580 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1635 = load i64, i64* %arrayidx3580, align 16
  %arrayidx3581 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1636 = load i64, i64* %arrayidx3581, align 16
  %xor3582 = xor i64 %1635, %1636
  %call3583 = call i64 @rotr64(i64 %xor3582, i32 24)
  %arrayidx3584 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3583, i64* %arrayidx3584, align 16
  %arrayidx3585 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1637 = load i64, i64* %arrayidx3585, align 16
  %arrayidx3586 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1638 = load i64, i64* %arrayidx3586, align 16
  %add3587 = add i64 %1637, %1638
  %1639 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 1), align 1
  %idxprom3588 = zext i8 %1639 to i32
  %arrayidx3589 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3588
  %1640 = load i64, i64* %arrayidx3589, align 8
  %add3590 = add i64 %add3587, %1640
  %arrayidx3591 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3590, i64* %arrayidx3591, align 16
  %arrayidx3592 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1641 = load i64, i64* %arrayidx3592, align 16
  %arrayidx3593 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1642 = load i64, i64* %arrayidx3593, align 16
  %xor3594 = xor i64 %1641, %1642
  %call3595 = call i64 @rotr64(i64 %xor3594, i32 16)
  %arrayidx3596 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3595, i64* %arrayidx3596, align 16
  %arrayidx3597 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1643 = load i64, i64* %arrayidx3597, align 16
  %arrayidx3598 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1644 = load i64, i64* %arrayidx3598, align 16
  %add3599 = add i64 %1643, %1644
  %arrayidx3600 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3599, i64* %arrayidx3600, align 16
  %arrayidx3601 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1645 = load i64, i64* %arrayidx3601, align 16
  %arrayidx3602 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1646 = load i64, i64* %arrayidx3602, align 16
  %xor3603 = xor i64 %1645, %1646
  %call3604 = call i64 @rotr64(i64 %xor3603, i32 63)
  %arrayidx3605 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3604, i64* %arrayidx3605, align 16
  br label %do.end3606

do.end3606:                                       ; preds = %do.body3563
  br label %do.body3607

do.body3607:                                      ; preds = %do.end3606
  %arrayidx3608 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1647 = load i64, i64* %arrayidx3608, align 8
  %arrayidx3609 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1648 = load i64, i64* %arrayidx3609, align 8
  %add3610 = add i64 %1647, %1648
  %1649 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 2), align 2
  %idxprom3611 = zext i8 %1649 to i32
  %arrayidx3612 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3611
  %1650 = load i64, i64* %arrayidx3612, align 8
  %add3613 = add i64 %add3610, %1650
  %arrayidx3614 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3613, i64* %arrayidx3614, align 8
  %arrayidx3615 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1651 = load i64, i64* %arrayidx3615, align 8
  %arrayidx3616 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1652 = load i64, i64* %arrayidx3616, align 8
  %xor3617 = xor i64 %1651, %1652
  %call3618 = call i64 @rotr64(i64 %xor3617, i32 32)
  %arrayidx3619 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3618, i64* %arrayidx3619, align 8
  %arrayidx3620 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1653 = load i64, i64* %arrayidx3620, align 8
  %arrayidx3621 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1654 = load i64, i64* %arrayidx3621, align 8
  %add3622 = add i64 %1653, %1654
  %arrayidx3623 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3622, i64* %arrayidx3623, align 8
  %arrayidx3624 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1655 = load i64, i64* %arrayidx3624, align 8
  %arrayidx3625 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1656 = load i64, i64* %arrayidx3625, align 8
  %xor3626 = xor i64 %1655, %1656
  %call3627 = call i64 @rotr64(i64 %xor3626, i32 24)
  %arrayidx3628 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3627, i64* %arrayidx3628, align 8
  %arrayidx3629 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1657 = load i64, i64* %arrayidx3629, align 8
  %arrayidx3630 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1658 = load i64, i64* %arrayidx3630, align 8
  %add3631 = add i64 %1657, %1658
  %1659 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 3), align 1
  %idxprom3632 = zext i8 %1659 to i32
  %arrayidx3633 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3632
  %1660 = load i64, i64* %arrayidx3633, align 8
  %add3634 = add i64 %add3631, %1660
  %arrayidx3635 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3634, i64* %arrayidx3635, align 8
  %arrayidx3636 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1661 = load i64, i64* %arrayidx3636, align 8
  %arrayidx3637 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1662 = load i64, i64* %arrayidx3637, align 8
  %xor3638 = xor i64 %1661, %1662
  %call3639 = call i64 @rotr64(i64 %xor3638, i32 16)
  %arrayidx3640 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3639, i64* %arrayidx3640, align 8
  %arrayidx3641 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1663 = load i64, i64* %arrayidx3641, align 8
  %arrayidx3642 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1664 = load i64, i64* %arrayidx3642, align 8
  %add3643 = add i64 %1663, %1664
  %arrayidx3644 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3643, i64* %arrayidx3644, align 8
  %arrayidx3645 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1665 = load i64, i64* %arrayidx3645, align 8
  %arrayidx3646 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1666 = load i64, i64* %arrayidx3646, align 8
  %xor3647 = xor i64 %1665, %1666
  %call3648 = call i64 @rotr64(i64 %xor3647, i32 63)
  %arrayidx3649 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3648, i64* %arrayidx3649, align 8
  br label %do.end3650

do.end3650:                                       ; preds = %do.body3607
  br label %do.body3651

do.body3651:                                      ; preds = %do.end3650
  %arrayidx3652 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1667 = load i64, i64* %arrayidx3652, align 16
  %arrayidx3653 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1668 = load i64, i64* %arrayidx3653, align 16
  %add3654 = add i64 %1667, %1668
  %1669 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 4), align 4
  %idxprom3655 = zext i8 %1669 to i32
  %arrayidx3656 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3655
  %1670 = load i64, i64* %arrayidx3656, align 8
  %add3657 = add i64 %add3654, %1670
  %arrayidx3658 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3657, i64* %arrayidx3658, align 16
  %arrayidx3659 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1671 = load i64, i64* %arrayidx3659, align 16
  %arrayidx3660 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1672 = load i64, i64* %arrayidx3660, align 16
  %xor3661 = xor i64 %1671, %1672
  %call3662 = call i64 @rotr64(i64 %xor3661, i32 32)
  %arrayidx3663 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3662, i64* %arrayidx3663, align 16
  %arrayidx3664 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1673 = load i64, i64* %arrayidx3664, align 16
  %arrayidx3665 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1674 = load i64, i64* %arrayidx3665, align 16
  %add3666 = add i64 %1673, %1674
  %arrayidx3667 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3666, i64* %arrayidx3667, align 16
  %arrayidx3668 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1675 = load i64, i64* %arrayidx3668, align 16
  %arrayidx3669 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1676 = load i64, i64* %arrayidx3669, align 16
  %xor3670 = xor i64 %1675, %1676
  %call3671 = call i64 @rotr64(i64 %xor3670, i32 24)
  %arrayidx3672 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3671, i64* %arrayidx3672, align 16
  %arrayidx3673 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1677 = load i64, i64* %arrayidx3673, align 16
  %arrayidx3674 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1678 = load i64, i64* %arrayidx3674, align 16
  %add3675 = add i64 %1677, %1678
  %1679 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 5), align 1
  %idxprom3676 = zext i8 %1679 to i32
  %arrayidx3677 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3676
  %1680 = load i64, i64* %arrayidx3677, align 8
  %add3678 = add i64 %add3675, %1680
  %arrayidx3679 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3678, i64* %arrayidx3679, align 16
  %arrayidx3680 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1681 = load i64, i64* %arrayidx3680, align 16
  %arrayidx3681 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1682 = load i64, i64* %arrayidx3681, align 16
  %xor3682 = xor i64 %1681, %1682
  %call3683 = call i64 @rotr64(i64 %xor3682, i32 16)
  %arrayidx3684 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3683, i64* %arrayidx3684, align 16
  %arrayidx3685 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1683 = load i64, i64* %arrayidx3685, align 16
  %arrayidx3686 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1684 = load i64, i64* %arrayidx3686, align 16
  %add3687 = add i64 %1683, %1684
  %arrayidx3688 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3687, i64* %arrayidx3688, align 16
  %arrayidx3689 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1685 = load i64, i64* %arrayidx3689, align 16
  %arrayidx3690 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1686 = load i64, i64* %arrayidx3690, align 16
  %xor3691 = xor i64 %1685, %1686
  %call3692 = call i64 @rotr64(i64 %xor3691, i32 63)
  %arrayidx3693 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3692, i64* %arrayidx3693, align 16
  br label %do.end3694

do.end3694:                                       ; preds = %do.body3651
  br label %do.body3695

do.body3695:                                      ; preds = %do.end3694
  %arrayidx3696 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1687 = load i64, i64* %arrayidx3696, align 8
  %arrayidx3697 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1688 = load i64, i64* %arrayidx3697, align 8
  %add3698 = add i64 %1687, %1688
  %1689 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 6), align 2
  %idxprom3699 = zext i8 %1689 to i32
  %arrayidx3700 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3699
  %1690 = load i64, i64* %arrayidx3700, align 8
  %add3701 = add i64 %add3698, %1690
  %arrayidx3702 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3701, i64* %arrayidx3702, align 8
  %arrayidx3703 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1691 = load i64, i64* %arrayidx3703, align 8
  %arrayidx3704 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1692 = load i64, i64* %arrayidx3704, align 8
  %xor3705 = xor i64 %1691, %1692
  %call3706 = call i64 @rotr64(i64 %xor3705, i32 32)
  %arrayidx3707 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3706, i64* %arrayidx3707, align 8
  %arrayidx3708 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1693 = load i64, i64* %arrayidx3708, align 8
  %arrayidx3709 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1694 = load i64, i64* %arrayidx3709, align 8
  %add3710 = add i64 %1693, %1694
  %arrayidx3711 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3710, i64* %arrayidx3711, align 8
  %arrayidx3712 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1695 = load i64, i64* %arrayidx3712, align 8
  %arrayidx3713 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1696 = load i64, i64* %arrayidx3713, align 8
  %xor3714 = xor i64 %1695, %1696
  %call3715 = call i64 @rotr64(i64 %xor3714, i32 24)
  %arrayidx3716 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3715, i64* %arrayidx3716, align 8
  %arrayidx3717 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1697 = load i64, i64* %arrayidx3717, align 8
  %arrayidx3718 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1698 = load i64, i64* %arrayidx3718, align 8
  %add3719 = add i64 %1697, %1698
  %1699 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 7), align 1
  %idxprom3720 = zext i8 %1699 to i32
  %arrayidx3721 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3720
  %1700 = load i64, i64* %arrayidx3721, align 8
  %add3722 = add i64 %add3719, %1700
  %arrayidx3723 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3722, i64* %arrayidx3723, align 8
  %arrayidx3724 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1701 = load i64, i64* %arrayidx3724, align 8
  %arrayidx3725 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1702 = load i64, i64* %arrayidx3725, align 8
  %xor3726 = xor i64 %1701, %1702
  %call3727 = call i64 @rotr64(i64 %xor3726, i32 16)
  %arrayidx3728 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3727, i64* %arrayidx3728, align 8
  %arrayidx3729 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1703 = load i64, i64* %arrayidx3729, align 8
  %arrayidx3730 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1704 = load i64, i64* %arrayidx3730, align 8
  %add3731 = add i64 %1703, %1704
  %arrayidx3732 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3731, i64* %arrayidx3732, align 8
  %arrayidx3733 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1705 = load i64, i64* %arrayidx3733, align 8
  %arrayidx3734 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1706 = load i64, i64* %arrayidx3734, align 8
  %xor3735 = xor i64 %1705, %1706
  %call3736 = call i64 @rotr64(i64 %xor3735, i32 63)
  %arrayidx3737 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3736, i64* %arrayidx3737, align 8
  br label %do.end3738

do.end3738:                                       ; preds = %do.body3695
  br label %do.body3739

do.body3739:                                      ; preds = %do.end3738
  %arrayidx3740 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1707 = load i64, i64* %arrayidx3740, align 16
  %arrayidx3741 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1708 = load i64, i64* %arrayidx3741, align 8
  %add3742 = add i64 %1707, %1708
  %1709 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 8), align 8
  %idxprom3743 = zext i8 %1709 to i32
  %arrayidx3744 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3743
  %1710 = load i64, i64* %arrayidx3744, align 8
  %add3745 = add i64 %add3742, %1710
  %arrayidx3746 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3745, i64* %arrayidx3746, align 16
  %arrayidx3747 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1711 = load i64, i64* %arrayidx3747, align 8
  %arrayidx3748 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1712 = load i64, i64* %arrayidx3748, align 16
  %xor3749 = xor i64 %1711, %1712
  %call3750 = call i64 @rotr64(i64 %xor3749, i32 32)
  %arrayidx3751 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3750, i64* %arrayidx3751, align 8
  %arrayidx3752 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1713 = load i64, i64* %arrayidx3752, align 16
  %arrayidx3753 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1714 = load i64, i64* %arrayidx3753, align 8
  %add3754 = add i64 %1713, %1714
  %arrayidx3755 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3754, i64* %arrayidx3755, align 16
  %arrayidx3756 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1715 = load i64, i64* %arrayidx3756, align 8
  %arrayidx3757 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1716 = load i64, i64* %arrayidx3757, align 16
  %xor3758 = xor i64 %1715, %1716
  %call3759 = call i64 @rotr64(i64 %xor3758, i32 24)
  %arrayidx3760 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3759, i64* %arrayidx3760, align 8
  %arrayidx3761 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1717 = load i64, i64* %arrayidx3761, align 16
  %arrayidx3762 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1718 = load i64, i64* %arrayidx3762, align 8
  %add3763 = add i64 %1717, %1718
  %1719 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 9), align 1
  %idxprom3764 = zext i8 %1719 to i32
  %arrayidx3765 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3764
  %1720 = load i64, i64* %arrayidx3765, align 8
  %add3766 = add i64 %add3763, %1720
  %arrayidx3767 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3766, i64* %arrayidx3767, align 16
  %arrayidx3768 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1721 = load i64, i64* %arrayidx3768, align 8
  %arrayidx3769 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1722 = load i64, i64* %arrayidx3769, align 16
  %xor3770 = xor i64 %1721, %1722
  %call3771 = call i64 @rotr64(i64 %xor3770, i32 16)
  %arrayidx3772 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3771, i64* %arrayidx3772, align 8
  %arrayidx3773 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1723 = load i64, i64* %arrayidx3773, align 16
  %arrayidx3774 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1724 = load i64, i64* %arrayidx3774, align 8
  %add3775 = add i64 %1723, %1724
  %arrayidx3776 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add3775, i64* %arrayidx3776, align 16
  %arrayidx3777 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1725 = load i64, i64* %arrayidx3777, align 8
  %arrayidx3778 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1726 = load i64, i64* %arrayidx3778, align 16
  %xor3779 = xor i64 %1725, %1726
  %call3780 = call i64 @rotr64(i64 %xor3779, i32 63)
  %arrayidx3781 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3780, i64* %arrayidx3781, align 8
  br label %do.end3782

do.end3782:                                       ; preds = %do.body3739
  br label %do.body3783

do.body3783:                                      ; preds = %do.end3782
  %arrayidx3784 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1727 = load i64, i64* %arrayidx3784, align 8
  %arrayidx3785 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1728 = load i64, i64* %arrayidx3785, align 16
  %add3786 = add i64 %1727, %1728
  %1729 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 10), align 2
  %idxprom3787 = zext i8 %1729 to i32
  %arrayidx3788 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3787
  %1730 = load i64, i64* %arrayidx3788, align 8
  %add3789 = add i64 %add3786, %1730
  %arrayidx3790 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3789, i64* %arrayidx3790, align 8
  %arrayidx3791 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1731 = load i64, i64* %arrayidx3791, align 16
  %arrayidx3792 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1732 = load i64, i64* %arrayidx3792, align 8
  %xor3793 = xor i64 %1731, %1732
  %call3794 = call i64 @rotr64(i64 %xor3793, i32 32)
  %arrayidx3795 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3794, i64* %arrayidx3795, align 16
  %arrayidx3796 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1733 = load i64, i64* %arrayidx3796, align 8
  %arrayidx3797 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1734 = load i64, i64* %arrayidx3797, align 16
  %add3798 = add i64 %1733, %1734
  %arrayidx3799 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3798, i64* %arrayidx3799, align 8
  %arrayidx3800 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1735 = load i64, i64* %arrayidx3800, align 16
  %arrayidx3801 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1736 = load i64, i64* %arrayidx3801, align 8
  %xor3802 = xor i64 %1735, %1736
  %call3803 = call i64 @rotr64(i64 %xor3802, i32 24)
  %arrayidx3804 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3803, i64* %arrayidx3804, align 16
  %arrayidx3805 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1737 = load i64, i64* %arrayidx3805, align 8
  %arrayidx3806 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1738 = load i64, i64* %arrayidx3806, align 16
  %add3807 = add i64 %1737, %1738
  %1739 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 11), align 1
  %idxprom3808 = zext i8 %1739 to i32
  %arrayidx3809 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3808
  %1740 = load i64, i64* %arrayidx3809, align 8
  %add3810 = add i64 %add3807, %1740
  %arrayidx3811 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3810, i64* %arrayidx3811, align 8
  %arrayidx3812 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1741 = load i64, i64* %arrayidx3812, align 16
  %arrayidx3813 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1742 = load i64, i64* %arrayidx3813, align 8
  %xor3814 = xor i64 %1741, %1742
  %call3815 = call i64 @rotr64(i64 %xor3814, i32 16)
  %arrayidx3816 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3815, i64* %arrayidx3816, align 16
  %arrayidx3817 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1743 = load i64, i64* %arrayidx3817, align 8
  %arrayidx3818 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1744 = load i64, i64* %arrayidx3818, align 16
  %add3819 = add i64 %1743, %1744
  %arrayidx3820 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add3819, i64* %arrayidx3820, align 8
  %arrayidx3821 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1745 = load i64, i64* %arrayidx3821, align 16
  %arrayidx3822 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1746 = load i64, i64* %arrayidx3822, align 8
  %xor3823 = xor i64 %1745, %1746
  %call3824 = call i64 @rotr64(i64 %xor3823, i32 63)
  %arrayidx3825 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3824, i64* %arrayidx3825, align 16
  br label %do.end3826

do.end3826:                                       ; preds = %do.body3783
  br label %do.body3827

do.body3827:                                      ; preds = %do.end3826
  %arrayidx3828 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1747 = load i64, i64* %arrayidx3828, align 16
  %arrayidx3829 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1748 = load i64, i64* %arrayidx3829, align 8
  %add3830 = add i64 %1747, %1748
  %1749 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 12), align 4
  %idxprom3831 = zext i8 %1749 to i32
  %arrayidx3832 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3831
  %1750 = load i64, i64* %arrayidx3832, align 8
  %add3833 = add i64 %add3830, %1750
  %arrayidx3834 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3833, i64* %arrayidx3834, align 16
  %arrayidx3835 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1751 = load i64, i64* %arrayidx3835, align 8
  %arrayidx3836 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1752 = load i64, i64* %arrayidx3836, align 16
  %xor3837 = xor i64 %1751, %1752
  %call3838 = call i64 @rotr64(i64 %xor3837, i32 32)
  %arrayidx3839 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3838, i64* %arrayidx3839, align 8
  %arrayidx3840 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1753 = load i64, i64* %arrayidx3840, align 16
  %arrayidx3841 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1754 = load i64, i64* %arrayidx3841, align 8
  %add3842 = add i64 %1753, %1754
  %arrayidx3843 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3842, i64* %arrayidx3843, align 16
  %arrayidx3844 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1755 = load i64, i64* %arrayidx3844, align 8
  %arrayidx3845 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1756 = load i64, i64* %arrayidx3845, align 16
  %xor3846 = xor i64 %1755, %1756
  %call3847 = call i64 @rotr64(i64 %xor3846, i32 24)
  %arrayidx3848 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3847, i64* %arrayidx3848, align 8
  %arrayidx3849 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1757 = load i64, i64* %arrayidx3849, align 16
  %arrayidx3850 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1758 = load i64, i64* %arrayidx3850, align 8
  %add3851 = add i64 %1757, %1758
  %1759 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 13), align 1
  %idxprom3852 = zext i8 %1759 to i32
  %arrayidx3853 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3852
  %1760 = load i64, i64* %arrayidx3853, align 8
  %add3854 = add i64 %add3851, %1760
  %arrayidx3855 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add3854, i64* %arrayidx3855, align 16
  %arrayidx3856 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1761 = load i64, i64* %arrayidx3856, align 8
  %arrayidx3857 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1762 = load i64, i64* %arrayidx3857, align 16
  %xor3858 = xor i64 %1761, %1762
  %call3859 = call i64 @rotr64(i64 %xor3858, i32 16)
  %arrayidx3860 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3859, i64* %arrayidx3860, align 8
  %arrayidx3861 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1763 = load i64, i64* %arrayidx3861, align 16
  %arrayidx3862 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1764 = load i64, i64* %arrayidx3862, align 8
  %add3863 = add i64 %1763, %1764
  %arrayidx3864 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3863, i64* %arrayidx3864, align 16
  %arrayidx3865 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1765 = load i64, i64* %arrayidx3865, align 8
  %arrayidx3866 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1766 = load i64, i64* %arrayidx3866, align 16
  %xor3867 = xor i64 %1765, %1766
  %call3868 = call i64 @rotr64(i64 %xor3867, i32 63)
  %arrayidx3869 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3868, i64* %arrayidx3869, align 8
  br label %do.end3870

do.end3870:                                       ; preds = %do.body3827
  br label %do.body3871

do.body3871:                                      ; preds = %do.end3870
  %arrayidx3872 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1767 = load i64, i64* %arrayidx3872, align 8
  %arrayidx3873 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1768 = load i64, i64* %arrayidx3873, align 16
  %add3874 = add i64 %1767, %1768
  %1769 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 14), align 2
  %idxprom3875 = zext i8 %1769 to i32
  %arrayidx3876 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3875
  %1770 = load i64, i64* %arrayidx3876, align 8
  %add3877 = add i64 %add3874, %1770
  %arrayidx3878 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3877, i64* %arrayidx3878, align 8
  %arrayidx3879 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1771 = load i64, i64* %arrayidx3879, align 16
  %arrayidx3880 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1772 = load i64, i64* %arrayidx3880, align 8
  %xor3881 = xor i64 %1771, %1772
  %call3882 = call i64 @rotr64(i64 %xor3881, i32 32)
  %arrayidx3883 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3882, i64* %arrayidx3883, align 16
  %arrayidx3884 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1773 = load i64, i64* %arrayidx3884, align 8
  %arrayidx3885 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1774 = load i64, i64* %arrayidx3885, align 16
  %add3886 = add i64 %1773, %1774
  %arrayidx3887 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3886, i64* %arrayidx3887, align 8
  %arrayidx3888 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1775 = load i64, i64* %arrayidx3888, align 16
  %arrayidx3889 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1776 = load i64, i64* %arrayidx3889, align 8
  %xor3890 = xor i64 %1775, %1776
  %call3891 = call i64 @rotr64(i64 %xor3890, i32 24)
  %arrayidx3892 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3891, i64* %arrayidx3892, align 16
  %arrayidx3893 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1777 = load i64, i64* %arrayidx3893, align 8
  %arrayidx3894 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1778 = load i64, i64* %arrayidx3894, align 16
  %add3895 = add i64 %1777, %1778
  %1779 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 15), align 1
  %idxprom3896 = zext i8 %1779 to i32
  %arrayidx3897 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3896
  %1780 = load i64, i64* %arrayidx3897, align 8
  %add3898 = add i64 %add3895, %1780
  %arrayidx3899 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add3898, i64* %arrayidx3899, align 8
  %arrayidx3900 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1781 = load i64, i64* %arrayidx3900, align 16
  %arrayidx3901 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1782 = load i64, i64* %arrayidx3901, align 8
  %xor3902 = xor i64 %1781, %1782
  %call3903 = call i64 @rotr64(i64 %xor3902, i32 16)
  %arrayidx3904 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3903, i64* %arrayidx3904, align 16
  %arrayidx3905 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1783 = load i64, i64* %arrayidx3905, align 8
  %arrayidx3906 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1784 = load i64, i64* %arrayidx3906, align 16
  %add3907 = add i64 %1783, %1784
  %arrayidx3908 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3907, i64* %arrayidx3908, align 8
  %arrayidx3909 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1785 = load i64, i64* %arrayidx3909, align 16
  %arrayidx3910 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1786 = load i64, i64* %arrayidx3910, align 8
  %xor3911 = xor i64 %1785, %1786
  %call3912 = call i64 @rotr64(i64 %xor3911, i32 63)
  %arrayidx3913 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3912, i64* %arrayidx3913, align 16
  br label %do.end3914

do.end3914:                                       ; preds = %do.body3871
  br label %do.end3915

do.end3915:                                       ; preds = %do.end3914
  br label %do.body3916

do.body3916:                                      ; preds = %do.end3915
  br label %do.body3917

do.body3917:                                      ; preds = %do.body3916
  %arrayidx3918 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1787 = load i64, i64* %arrayidx3918, align 16
  %arrayidx3919 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1788 = load i64, i64* %arrayidx3919, align 16
  %add3920 = add i64 %1787, %1788
  %1789 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 0), align 16
  %idxprom3921 = zext i8 %1789 to i32
  %arrayidx3922 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3921
  %1790 = load i64, i64* %arrayidx3922, align 8
  %add3923 = add i64 %add3920, %1790
  %arrayidx3924 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3923, i64* %arrayidx3924, align 16
  %arrayidx3925 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1791 = load i64, i64* %arrayidx3925, align 16
  %arrayidx3926 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1792 = load i64, i64* %arrayidx3926, align 16
  %xor3927 = xor i64 %1791, %1792
  %call3928 = call i64 @rotr64(i64 %xor3927, i32 32)
  %arrayidx3929 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3928, i64* %arrayidx3929, align 16
  %arrayidx3930 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1793 = load i64, i64* %arrayidx3930, align 16
  %arrayidx3931 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1794 = load i64, i64* %arrayidx3931, align 16
  %add3932 = add i64 %1793, %1794
  %arrayidx3933 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3932, i64* %arrayidx3933, align 16
  %arrayidx3934 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1795 = load i64, i64* %arrayidx3934, align 16
  %arrayidx3935 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1796 = load i64, i64* %arrayidx3935, align 16
  %xor3936 = xor i64 %1795, %1796
  %call3937 = call i64 @rotr64(i64 %xor3936, i32 24)
  %arrayidx3938 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3937, i64* %arrayidx3938, align 16
  %arrayidx3939 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1797 = load i64, i64* %arrayidx3939, align 16
  %arrayidx3940 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1798 = load i64, i64* %arrayidx3940, align 16
  %add3941 = add i64 %1797, %1798
  %1799 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 1), align 1
  %idxprom3942 = zext i8 %1799 to i32
  %arrayidx3943 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3942
  %1800 = load i64, i64* %arrayidx3943, align 8
  %add3944 = add i64 %add3941, %1800
  %arrayidx3945 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add3944, i64* %arrayidx3945, align 16
  %arrayidx3946 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1801 = load i64, i64* %arrayidx3946, align 16
  %arrayidx3947 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1802 = load i64, i64* %arrayidx3947, align 16
  %xor3948 = xor i64 %1801, %1802
  %call3949 = call i64 @rotr64(i64 %xor3948, i32 16)
  %arrayidx3950 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3949, i64* %arrayidx3950, align 16
  %arrayidx3951 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1803 = load i64, i64* %arrayidx3951, align 16
  %arrayidx3952 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1804 = load i64, i64* %arrayidx3952, align 16
  %add3953 = add i64 %1803, %1804
  %arrayidx3954 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add3953, i64* %arrayidx3954, align 16
  %arrayidx3955 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1805 = load i64, i64* %arrayidx3955, align 16
  %arrayidx3956 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1806 = load i64, i64* %arrayidx3956, align 16
  %xor3957 = xor i64 %1805, %1806
  %call3958 = call i64 @rotr64(i64 %xor3957, i32 63)
  %arrayidx3959 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3958, i64* %arrayidx3959, align 16
  br label %do.end3960

do.end3960:                                       ; preds = %do.body3917
  br label %do.body3961

do.body3961:                                      ; preds = %do.end3960
  %arrayidx3962 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1807 = load i64, i64* %arrayidx3962, align 8
  %arrayidx3963 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1808 = load i64, i64* %arrayidx3963, align 8
  %add3964 = add i64 %1807, %1808
  %1809 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 2), align 2
  %idxprom3965 = zext i8 %1809 to i32
  %arrayidx3966 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3965
  %1810 = load i64, i64* %arrayidx3966, align 8
  %add3967 = add i64 %add3964, %1810
  %arrayidx3968 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3967, i64* %arrayidx3968, align 8
  %arrayidx3969 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1811 = load i64, i64* %arrayidx3969, align 8
  %arrayidx3970 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1812 = load i64, i64* %arrayidx3970, align 8
  %xor3971 = xor i64 %1811, %1812
  %call3972 = call i64 @rotr64(i64 %xor3971, i32 32)
  %arrayidx3973 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3972, i64* %arrayidx3973, align 8
  %arrayidx3974 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1813 = load i64, i64* %arrayidx3974, align 8
  %arrayidx3975 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1814 = load i64, i64* %arrayidx3975, align 8
  %add3976 = add i64 %1813, %1814
  %arrayidx3977 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3976, i64* %arrayidx3977, align 8
  %arrayidx3978 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1815 = load i64, i64* %arrayidx3978, align 8
  %arrayidx3979 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1816 = load i64, i64* %arrayidx3979, align 8
  %xor3980 = xor i64 %1815, %1816
  %call3981 = call i64 @rotr64(i64 %xor3980, i32 24)
  %arrayidx3982 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3981, i64* %arrayidx3982, align 8
  %arrayidx3983 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1817 = load i64, i64* %arrayidx3983, align 8
  %arrayidx3984 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1818 = load i64, i64* %arrayidx3984, align 8
  %add3985 = add i64 %1817, %1818
  %1819 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 3), align 1
  %idxprom3986 = zext i8 %1819 to i32
  %arrayidx3987 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3986
  %1820 = load i64, i64* %arrayidx3987, align 8
  %add3988 = add i64 %add3985, %1820
  %arrayidx3989 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add3988, i64* %arrayidx3989, align 8
  %arrayidx3990 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1821 = load i64, i64* %arrayidx3990, align 8
  %arrayidx3991 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1822 = load i64, i64* %arrayidx3991, align 8
  %xor3992 = xor i64 %1821, %1822
  %call3993 = call i64 @rotr64(i64 %xor3992, i32 16)
  %arrayidx3994 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3993, i64* %arrayidx3994, align 8
  %arrayidx3995 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1823 = load i64, i64* %arrayidx3995, align 8
  %arrayidx3996 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1824 = load i64, i64* %arrayidx3996, align 8
  %add3997 = add i64 %1823, %1824
  %arrayidx3998 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add3997, i64* %arrayidx3998, align 8
  %arrayidx3999 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1825 = load i64, i64* %arrayidx3999, align 8
  %arrayidx4000 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1826 = load i64, i64* %arrayidx4000, align 8
  %xor4001 = xor i64 %1825, %1826
  %call4002 = call i64 @rotr64(i64 %xor4001, i32 63)
  %arrayidx4003 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call4002, i64* %arrayidx4003, align 8
  br label %do.end4004

do.end4004:                                       ; preds = %do.body3961
  br label %do.body4005

do.body4005:                                      ; preds = %do.end4004
  %arrayidx4006 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1827 = load i64, i64* %arrayidx4006, align 16
  %arrayidx4007 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1828 = load i64, i64* %arrayidx4007, align 16
  %add4008 = add i64 %1827, %1828
  %1829 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 4), align 4
  %idxprom4009 = zext i8 %1829 to i32
  %arrayidx4010 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4009
  %1830 = load i64, i64* %arrayidx4010, align 8
  %add4011 = add i64 %add4008, %1830
  %arrayidx4012 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add4011, i64* %arrayidx4012, align 16
  %arrayidx4013 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1831 = load i64, i64* %arrayidx4013, align 16
  %arrayidx4014 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1832 = load i64, i64* %arrayidx4014, align 16
  %xor4015 = xor i64 %1831, %1832
  %call4016 = call i64 @rotr64(i64 %xor4015, i32 32)
  %arrayidx4017 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call4016, i64* %arrayidx4017, align 16
  %arrayidx4018 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1833 = load i64, i64* %arrayidx4018, align 16
  %arrayidx4019 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1834 = load i64, i64* %arrayidx4019, align 16
  %add4020 = add i64 %1833, %1834
  %arrayidx4021 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add4020, i64* %arrayidx4021, align 16
  %arrayidx4022 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1835 = load i64, i64* %arrayidx4022, align 16
  %arrayidx4023 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1836 = load i64, i64* %arrayidx4023, align 16
  %xor4024 = xor i64 %1835, %1836
  %call4025 = call i64 @rotr64(i64 %xor4024, i32 24)
  %arrayidx4026 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call4025, i64* %arrayidx4026, align 16
  %arrayidx4027 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1837 = load i64, i64* %arrayidx4027, align 16
  %arrayidx4028 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1838 = load i64, i64* %arrayidx4028, align 16
  %add4029 = add i64 %1837, %1838
  %1839 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 5), align 1
  %idxprom4030 = zext i8 %1839 to i32
  %arrayidx4031 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4030
  %1840 = load i64, i64* %arrayidx4031, align 8
  %add4032 = add i64 %add4029, %1840
  %arrayidx4033 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add4032, i64* %arrayidx4033, align 16
  %arrayidx4034 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1841 = load i64, i64* %arrayidx4034, align 16
  %arrayidx4035 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1842 = load i64, i64* %arrayidx4035, align 16
  %xor4036 = xor i64 %1841, %1842
  %call4037 = call i64 @rotr64(i64 %xor4036, i32 16)
  %arrayidx4038 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call4037, i64* %arrayidx4038, align 16
  %arrayidx4039 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1843 = load i64, i64* %arrayidx4039, align 16
  %arrayidx4040 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1844 = load i64, i64* %arrayidx4040, align 16
  %add4041 = add i64 %1843, %1844
  %arrayidx4042 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add4041, i64* %arrayidx4042, align 16
  %arrayidx4043 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1845 = load i64, i64* %arrayidx4043, align 16
  %arrayidx4044 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1846 = load i64, i64* %arrayidx4044, align 16
  %xor4045 = xor i64 %1845, %1846
  %call4046 = call i64 @rotr64(i64 %xor4045, i32 63)
  %arrayidx4047 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call4046, i64* %arrayidx4047, align 16
  br label %do.end4048

do.end4048:                                       ; preds = %do.body4005
  br label %do.body4049

do.body4049:                                      ; preds = %do.end4048
  %arrayidx4050 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1847 = load i64, i64* %arrayidx4050, align 8
  %arrayidx4051 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1848 = load i64, i64* %arrayidx4051, align 8
  %add4052 = add i64 %1847, %1848
  %1849 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 6), align 2
  %idxprom4053 = zext i8 %1849 to i32
  %arrayidx4054 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4053
  %1850 = load i64, i64* %arrayidx4054, align 8
  %add4055 = add i64 %add4052, %1850
  %arrayidx4056 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add4055, i64* %arrayidx4056, align 8
  %arrayidx4057 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1851 = load i64, i64* %arrayidx4057, align 8
  %arrayidx4058 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1852 = load i64, i64* %arrayidx4058, align 8
  %xor4059 = xor i64 %1851, %1852
  %call4060 = call i64 @rotr64(i64 %xor4059, i32 32)
  %arrayidx4061 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call4060, i64* %arrayidx4061, align 8
  %arrayidx4062 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1853 = load i64, i64* %arrayidx4062, align 8
  %arrayidx4063 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1854 = load i64, i64* %arrayidx4063, align 8
  %add4064 = add i64 %1853, %1854
  %arrayidx4065 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add4064, i64* %arrayidx4065, align 8
  %arrayidx4066 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1855 = load i64, i64* %arrayidx4066, align 8
  %arrayidx4067 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1856 = load i64, i64* %arrayidx4067, align 8
  %xor4068 = xor i64 %1855, %1856
  %call4069 = call i64 @rotr64(i64 %xor4068, i32 24)
  %arrayidx4070 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call4069, i64* %arrayidx4070, align 8
  %arrayidx4071 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1857 = load i64, i64* %arrayidx4071, align 8
  %arrayidx4072 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1858 = load i64, i64* %arrayidx4072, align 8
  %add4073 = add i64 %1857, %1858
  %1859 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 7), align 1
  %idxprom4074 = zext i8 %1859 to i32
  %arrayidx4075 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4074
  %1860 = load i64, i64* %arrayidx4075, align 8
  %add4076 = add i64 %add4073, %1860
  %arrayidx4077 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add4076, i64* %arrayidx4077, align 8
  %arrayidx4078 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1861 = load i64, i64* %arrayidx4078, align 8
  %arrayidx4079 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1862 = load i64, i64* %arrayidx4079, align 8
  %xor4080 = xor i64 %1861, %1862
  %call4081 = call i64 @rotr64(i64 %xor4080, i32 16)
  %arrayidx4082 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call4081, i64* %arrayidx4082, align 8
  %arrayidx4083 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1863 = load i64, i64* %arrayidx4083, align 8
  %arrayidx4084 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1864 = load i64, i64* %arrayidx4084, align 8
  %add4085 = add i64 %1863, %1864
  %arrayidx4086 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add4085, i64* %arrayidx4086, align 8
  %arrayidx4087 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1865 = load i64, i64* %arrayidx4087, align 8
  %arrayidx4088 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1866 = load i64, i64* %arrayidx4088, align 8
  %xor4089 = xor i64 %1865, %1866
  %call4090 = call i64 @rotr64(i64 %xor4089, i32 63)
  %arrayidx4091 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call4090, i64* %arrayidx4091, align 8
  br label %do.end4092

do.end4092:                                       ; preds = %do.body4049
  br label %do.body4093

do.body4093:                                      ; preds = %do.end4092
  %arrayidx4094 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1867 = load i64, i64* %arrayidx4094, align 16
  %arrayidx4095 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1868 = load i64, i64* %arrayidx4095, align 8
  %add4096 = add i64 %1867, %1868
  %1869 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 8), align 8
  %idxprom4097 = zext i8 %1869 to i32
  %arrayidx4098 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4097
  %1870 = load i64, i64* %arrayidx4098, align 8
  %add4099 = add i64 %add4096, %1870
  %arrayidx4100 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add4099, i64* %arrayidx4100, align 16
  %arrayidx4101 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1871 = load i64, i64* %arrayidx4101, align 8
  %arrayidx4102 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1872 = load i64, i64* %arrayidx4102, align 16
  %xor4103 = xor i64 %1871, %1872
  %call4104 = call i64 @rotr64(i64 %xor4103, i32 32)
  %arrayidx4105 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call4104, i64* %arrayidx4105, align 8
  %arrayidx4106 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1873 = load i64, i64* %arrayidx4106, align 16
  %arrayidx4107 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1874 = load i64, i64* %arrayidx4107, align 8
  %add4108 = add i64 %1873, %1874
  %arrayidx4109 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add4108, i64* %arrayidx4109, align 16
  %arrayidx4110 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1875 = load i64, i64* %arrayidx4110, align 8
  %arrayidx4111 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1876 = load i64, i64* %arrayidx4111, align 16
  %xor4112 = xor i64 %1875, %1876
  %call4113 = call i64 @rotr64(i64 %xor4112, i32 24)
  %arrayidx4114 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call4113, i64* %arrayidx4114, align 8
  %arrayidx4115 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1877 = load i64, i64* %arrayidx4115, align 16
  %arrayidx4116 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1878 = load i64, i64* %arrayidx4116, align 8
  %add4117 = add i64 %1877, %1878
  %1879 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 9), align 1
  %idxprom4118 = zext i8 %1879 to i32
  %arrayidx4119 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4118
  %1880 = load i64, i64* %arrayidx4119, align 8
  %add4120 = add i64 %add4117, %1880
  %arrayidx4121 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add4120, i64* %arrayidx4121, align 16
  %arrayidx4122 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1881 = load i64, i64* %arrayidx4122, align 8
  %arrayidx4123 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1882 = load i64, i64* %arrayidx4123, align 16
  %xor4124 = xor i64 %1881, %1882
  %call4125 = call i64 @rotr64(i64 %xor4124, i32 16)
  %arrayidx4126 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call4125, i64* %arrayidx4126, align 8
  %arrayidx4127 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1883 = load i64, i64* %arrayidx4127, align 16
  %arrayidx4128 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1884 = load i64, i64* %arrayidx4128, align 8
  %add4129 = add i64 %1883, %1884
  %arrayidx4130 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add4129, i64* %arrayidx4130, align 16
  %arrayidx4131 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1885 = load i64, i64* %arrayidx4131, align 8
  %arrayidx4132 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1886 = load i64, i64* %arrayidx4132, align 16
  %xor4133 = xor i64 %1885, %1886
  %call4134 = call i64 @rotr64(i64 %xor4133, i32 63)
  %arrayidx4135 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call4134, i64* %arrayidx4135, align 8
  br label %do.end4136

do.end4136:                                       ; preds = %do.body4093
  br label %do.body4137

do.body4137:                                      ; preds = %do.end4136
  %arrayidx4138 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1887 = load i64, i64* %arrayidx4138, align 8
  %arrayidx4139 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1888 = load i64, i64* %arrayidx4139, align 16
  %add4140 = add i64 %1887, %1888
  %1889 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 10), align 2
  %idxprom4141 = zext i8 %1889 to i32
  %arrayidx4142 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4141
  %1890 = load i64, i64* %arrayidx4142, align 8
  %add4143 = add i64 %add4140, %1890
  %arrayidx4144 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add4143, i64* %arrayidx4144, align 8
  %arrayidx4145 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1891 = load i64, i64* %arrayidx4145, align 16
  %arrayidx4146 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1892 = load i64, i64* %arrayidx4146, align 8
  %xor4147 = xor i64 %1891, %1892
  %call4148 = call i64 @rotr64(i64 %xor4147, i32 32)
  %arrayidx4149 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call4148, i64* %arrayidx4149, align 16
  %arrayidx4150 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1893 = load i64, i64* %arrayidx4150, align 8
  %arrayidx4151 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1894 = load i64, i64* %arrayidx4151, align 16
  %add4152 = add i64 %1893, %1894
  %arrayidx4153 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add4152, i64* %arrayidx4153, align 8
  %arrayidx4154 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1895 = load i64, i64* %arrayidx4154, align 16
  %arrayidx4155 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1896 = load i64, i64* %arrayidx4155, align 8
  %xor4156 = xor i64 %1895, %1896
  %call4157 = call i64 @rotr64(i64 %xor4156, i32 24)
  %arrayidx4158 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call4157, i64* %arrayidx4158, align 16
  %arrayidx4159 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1897 = load i64, i64* %arrayidx4159, align 8
  %arrayidx4160 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1898 = load i64, i64* %arrayidx4160, align 16
  %add4161 = add i64 %1897, %1898
  %1899 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 11), align 1
  %idxprom4162 = zext i8 %1899 to i32
  %arrayidx4163 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4162
  %1900 = load i64, i64* %arrayidx4163, align 8
  %add4164 = add i64 %add4161, %1900
  %arrayidx4165 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add4164, i64* %arrayidx4165, align 8
  %arrayidx4166 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1901 = load i64, i64* %arrayidx4166, align 16
  %arrayidx4167 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1902 = load i64, i64* %arrayidx4167, align 8
  %xor4168 = xor i64 %1901, %1902
  %call4169 = call i64 @rotr64(i64 %xor4168, i32 16)
  %arrayidx4170 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call4169, i64* %arrayidx4170, align 16
  %arrayidx4171 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1903 = load i64, i64* %arrayidx4171, align 8
  %arrayidx4172 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1904 = load i64, i64* %arrayidx4172, align 16
  %add4173 = add i64 %1903, %1904
  %arrayidx4174 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add4173, i64* %arrayidx4174, align 8
  %arrayidx4175 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1905 = load i64, i64* %arrayidx4175, align 16
  %arrayidx4176 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1906 = load i64, i64* %arrayidx4176, align 8
  %xor4177 = xor i64 %1905, %1906
  %call4178 = call i64 @rotr64(i64 %xor4177, i32 63)
  %arrayidx4179 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call4178, i64* %arrayidx4179, align 16
  br label %do.end4180

do.end4180:                                       ; preds = %do.body4137
  br label %do.body4181

do.body4181:                                      ; preds = %do.end4180
  %arrayidx4182 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1907 = load i64, i64* %arrayidx4182, align 16
  %arrayidx4183 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1908 = load i64, i64* %arrayidx4183, align 8
  %add4184 = add i64 %1907, %1908
  %1909 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 12), align 4
  %idxprom4185 = zext i8 %1909 to i32
  %arrayidx4186 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4185
  %1910 = load i64, i64* %arrayidx4186, align 8
  %add4187 = add i64 %add4184, %1910
  %arrayidx4188 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add4187, i64* %arrayidx4188, align 16
  %arrayidx4189 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1911 = load i64, i64* %arrayidx4189, align 8
  %arrayidx4190 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1912 = load i64, i64* %arrayidx4190, align 16
  %xor4191 = xor i64 %1911, %1912
  %call4192 = call i64 @rotr64(i64 %xor4191, i32 32)
  %arrayidx4193 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call4192, i64* %arrayidx4193, align 8
  %arrayidx4194 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1913 = load i64, i64* %arrayidx4194, align 16
  %arrayidx4195 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1914 = load i64, i64* %arrayidx4195, align 8
  %add4196 = add i64 %1913, %1914
  %arrayidx4197 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add4196, i64* %arrayidx4197, align 16
  %arrayidx4198 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1915 = load i64, i64* %arrayidx4198, align 8
  %arrayidx4199 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1916 = load i64, i64* %arrayidx4199, align 16
  %xor4200 = xor i64 %1915, %1916
  %call4201 = call i64 @rotr64(i64 %xor4200, i32 24)
  %arrayidx4202 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call4201, i64* %arrayidx4202, align 8
  %arrayidx4203 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1917 = load i64, i64* %arrayidx4203, align 16
  %arrayidx4204 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1918 = load i64, i64* %arrayidx4204, align 8
  %add4205 = add i64 %1917, %1918
  %1919 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 13), align 1
  %idxprom4206 = zext i8 %1919 to i32
  %arrayidx4207 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4206
  %1920 = load i64, i64* %arrayidx4207, align 8
  %add4208 = add i64 %add4205, %1920
  %arrayidx4209 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add4208, i64* %arrayidx4209, align 16
  %arrayidx4210 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1921 = load i64, i64* %arrayidx4210, align 8
  %arrayidx4211 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1922 = load i64, i64* %arrayidx4211, align 16
  %xor4212 = xor i64 %1921, %1922
  %call4213 = call i64 @rotr64(i64 %xor4212, i32 16)
  %arrayidx4214 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call4213, i64* %arrayidx4214, align 8
  %arrayidx4215 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1923 = load i64, i64* %arrayidx4215, align 16
  %arrayidx4216 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1924 = load i64, i64* %arrayidx4216, align 8
  %add4217 = add i64 %1923, %1924
  %arrayidx4218 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add4217, i64* %arrayidx4218, align 16
  %arrayidx4219 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1925 = load i64, i64* %arrayidx4219, align 8
  %arrayidx4220 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1926 = load i64, i64* %arrayidx4220, align 16
  %xor4221 = xor i64 %1925, %1926
  %call4222 = call i64 @rotr64(i64 %xor4221, i32 63)
  %arrayidx4223 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call4222, i64* %arrayidx4223, align 8
  br label %do.end4224

do.end4224:                                       ; preds = %do.body4181
  br label %do.body4225

do.body4225:                                      ; preds = %do.end4224
  %arrayidx4226 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1927 = load i64, i64* %arrayidx4226, align 8
  %arrayidx4227 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1928 = load i64, i64* %arrayidx4227, align 16
  %add4228 = add i64 %1927, %1928
  %1929 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 14), align 2
  %idxprom4229 = zext i8 %1929 to i32
  %arrayidx4230 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4229
  %1930 = load i64, i64* %arrayidx4230, align 8
  %add4231 = add i64 %add4228, %1930
  %arrayidx4232 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add4231, i64* %arrayidx4232, align 8
  %arrayidx4233 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1931 = load i64, i64* %arrayidx4233, align 16
  %arrayidx4234 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1932 = load i64, i64* %arrayidx4234, align 8
  %xor4235 = xor i64 %1931, %1932
  %call4236 = call i64 @rotr64(i64 %xor4235, i32 32)
  %arrayidx4237 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call4236, i64* %arrayidx4237, align 16
  %arrayidx4238 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1933 = load i64, i64* %arrayidx4238, align 8
  %arrayidx4239 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1934 = load i64, i64* %arrayidx4239, align 16
  %add4240 = add i64 %1933, %1934
  %arrayidx4241 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add4240, i64* %arrayidx4241, align 8
  %arrayidx4242 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1935 = load i64, i64* %arrayidx4242, align 16
  %arrayidx4243 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1936 = load i64, i64* %arrayidx4243, align 8
  %xor4244 = xor i64 %1935, %1936
  %call4245 = call i64 @rotr64(i64 %xor4244, i32 24)
  %arrayidx4246 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call4245, i64* %arrayidx4246, align 16
  %arrayidx4247 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1937 = load i64, i64* %arrayidx4247, align 8
  %arrayidx4248 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1938 = load i64, i64* %arrayidx4248, align 16
  %add4249 = add i64 %1937, %1938
  %1939 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 15), align 1
  %idxprom4250 = zext i8 %1939 to i32
  %arrayidx4251 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom4250
  %1940 = load i64, i64* %arrayidx4251, align 8
  %add4252 = add i64 %add4249, %1940
  %arrayidx4253 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add4252, i64* %arrayidx4253, align 8
  %arrayidx4254 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1941 = load i64, i64* %arrayidx4254, align 16
  %arrayidx4255 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1942 = load i64, i64* %arrayidx4255, align 8
  %xor4256 = xor i64 %1941, %1942
  %call4257 = call i64 @rotr64(i64 %xor4256, i32 16)
  %arrayidx4258 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call4257, i64* %arrayidx4258, align 16
  %arrayidx4259 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1943 = load i64, i64* %arrayidx4259, align 8
  %arrayidx4260 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1944 = load i64, i64* %arrayidx4260, align 16
  %add4261 = add i64 %1943, %1944
  %arrayidx4262 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add4261, i64* %arrayidx4262, align 8
  %arrayidx4263 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1945 = load i64, i64* %arrayidx4263, align 16
  %arrayidx4264 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1946 = load i64, i64* %arrayidx4264, align 8
  %xor4265 = xor i64 %1945, %1946
  %call4266 = call i64 @rotr64(i64 %xor4265, i32 63)
  %arrayidx4267 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call4266, i64* %arrayidx4267, align 16
  br label %do.end4268

do.end4268:                                       ; preds = %do.body4225
  br label %do.end4269

do.end4269:                                       ; preds = %do.end4268
  store i32 0, i32* %i, align 4
  br label %for.cond4270

for.cond4270:                                     ; preds = %for.inc4282, %do.end4269
  %1947 = load i32, i32* %i, align 4
  %cmp4271 = icmp ult i32 %1947, 8
  br i1 %cmp4271, label %for.body4272, label %for.end4284

for.body4272:                                     ; preds = %for.cond4270
  %1948 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %h4273 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %1948, i32 0, i32 0
  %1949 = load i32, i32* %i, align 4
  %arrayidx4274 = getelementptr inbounds [8 x i64], [8 x i64]* %h4273, i32 0, i32 %1949
  %1950 = load i64, i64* %arrayidx4274, align 8
  %1951 = load i32, i32* %i, align 4
  %arrayidx4275 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 %1951
  %1952 = load i64, i64* %arrayidx4275, align 8
  %xor4276 = xor i64 %1950, %1952
  %1953 = load i32, i32* %i, align 4
  %add4277 = add i32 %1953, 8
  %arrayidx4278 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 %add4277
  %1954 = load i64, i64* %arrayidx4278, align 8
  %xor4279 = xor i64 %xor4276, %1954
  %1955 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %h4280 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %1955, i32 0, i32 0
  %1956 = load i32, i32* %i, align 4
  %arrayidx4281 = getelementptr inbounds [8 x i64], [8 x i64]* %h4280, i32 0, i32 %1956
  store i64 %xor4279, i64* %arrayidx4281, align 8
  br label %for.inc4282

for.inc4282:                                      ; preds = %for.body4272
  %1957 = load i32, i32* %i, align 4
  %inc4283 = add i32 %1957, 1
  store i32 %inc4283, i32* %i, align 4
  br label %for.cond4270

for.end4284:                                      ; preds = %for.cond4270
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_final(%struct.blake2b_state__* %S, i8* %out, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2b_state__*, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %buffer = alloca [64 x i8], align 16
  %i = alloca i32, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = bitcast [64 x i8]* %buffer to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %0, i8 0, i32 64, i1 false)
  %1 = load i8*, i8** %out.addr, align 4
  %cmp = icmp eq i8* %1, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32, i32* %outlen.addr, align 4
  %3 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %outlen1 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %3, i32 0, i32 5
  %4 = load i32, i32* %outlen1, align 4
  %cmp2 = icmp ult i32 %2, %4
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %5 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %call = call i32 @blake2b_is_lastblock(%struct.blake2b_state__* %5)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %6 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %7 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %7, i32 0, i32 4
  %8 = load i32, i32* %buflen, align 8
  %conv = zext i32 %8 to i64
  call void @blake2b_increment_counter(%struct.blake2b_state__* %6, i64 %conv)
  %9 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  call void @blake2b_set_lastblock(%struct.blake2b_state__* %9)
  %10 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %10, i32 0, i32 3
  %arraydecay = getelementptr inbounds [128 x i8], [128 x i8]* %buf, i32 0, i32 0
  %11 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buflen5 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %11, i32 0, i32 4
  %12 = load i32, i32* %buflen5, align 8
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 %12
  %13 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buflen6 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %13, i32 0, i32 4
  %14 = load i32, i32* %buflen6, align 8
  %sub = sub i32 128, %14
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr, i8 0, i32 %sub, i1 false)
  %15 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %16 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %buf7 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %16, i32 0, i32 3
  %arraydecay8 = getelementptr inbounds [128 x i8], [128 x i8]* %buf7, i32 0, i32 0
  call void @blake2b_compress(%struct.blake2b_state__* %15, i8* %arraydecay8)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end4
  %17 = load i32, i32* %i, align 4
  %cmp9 = icmp ult i32 %17, 8
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %arraydecay11 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %mul = mul i32 8, %18
  %add.ptr12 = getelementptr inbounds i8, i8* %arraydecay11, i32 %mul
  %19 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %19, i32 0, i32 0
  %20 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 %20
  %21 = load i64, i64* %arrayidx, align 8
  call void @store64(i8* %add.ptr12, i64 %21)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %i, align 4
  %inc = add i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load i8*, i8** %out.addr, align 4
  %arraydecay13 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %24 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %outlen14 = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %24, i32 0, i32 5
  %25 = load i32, i32* %outlen14, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %23, i8* align 16 %arraydecay13, i32 %25, i1 false)
  %arraydecay15 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  call void @secure_zero_memory(i8* %arraydecay15, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then3, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_is_lastblock(%struct.blake2b_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  %0 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %0, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 0
  %1 = load i64, i64* %arrayidx, align 8
  %cmp = icmp ne i64 %1, 0
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2b_set_lastblock(%struct.blake2b_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  %0 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %last_node = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %0, i32 0, i32 6
  %1 = load i8, i8* %last_node, align 8
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  call void @blake2b_set_lastnode(%struct.blake2b_state__* %2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %3, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 0
  store i64 -1, i64* %arrayidx, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store64(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  %p = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i8*, i8** %dst.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i64, i64* %w.addr, align 8
  %shr = lshr i64 %1, 0
  %conv = trunc i64 %shr to i8
  %2 = load i8*, i8** %p, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 0
  store i8 %conv, i8* %arrayidx, align 1
  %3 = load i64, i64* %w.addr, align 8
  %shr1 = lshr i64 %3, 8
  %conv2 = trunc i64 %shr1 to i8
  %4 = load i8*, i8** %p, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %4, i32 1
  store i8 %conv2, i8* %arrayidx3, align 1
  %5 = load i64, i64* %w.addr, align 8
  %shr4 = lshr i64 %5, 16
  %conv5 = trunc i64 %shr4 to i8
  %6 = load i8*, i8** %p, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %6, i32 2
  store i8 %conv5, i8* %arrayidx6, align 1
  %7 = load i64, i64* %w.addr, align 8
  %shr7 = lshr i64 %7, 24
  %conv8 = trunc i64 %shr7 to i8
  %8 = load i8*, i8** %p, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %8, i32 3
  store i8 %conv8, i8* %arrayidx9, align 1
  %9 = load i64, i64* %w.addr, align 8
  %shr10 = lshr i64 %9, 32
  %conv11 = trunc i64 %shr10 to i8
  %10 = load i8*, i8** %p, align 4
  %arrayidx12 = getelementptr inbounds i8, i8* %10, i32 4
  store i8 %conv11, i8* %arrayidx12, align 1
  %11 = load i64, i64* %w.addr, align 8
  %shr13 = lshr i64 %11, 40
  %conv14 = trunc i64 %shr13 to i8
  %12 = load i8*, i8** %p, align 4
  %arrayidx15 = getelementptr inbounds i8, i8* %12, i32 5
  store i8 %conv14, i8* %arrayidx15, align 1
  %13 = load i64, i64* %w.addr, align 8
  %shr16 = lshr i64 %13, 48
  %conv17 = trunc i64 %shr16 to i8
  %14 = load i8*, i8** %p, align 4
  %arrayidx18 = getelementptr inbounds i8, i8* %14, i32 6
  store i8 %conv17, i8* %arrayidx18, align 1
  %15 = load i64, i64* %w.addr, align 8
  %shr19 = lshr i64 %15, 56
  %conv20 = trunc i64 %shr19 to i8
  %16 = load i8*, i8** %p, align 4
  %arrayidx21 = getelementptr inbounds i8, i8* %16, i32 7
  store i8 %conv20, i8* %arrayidx21, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b(i8* %out, i32 %outlen, i8* %in, i32 %inlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %S = alloca [1 x %struct.blake2b_state__], align 16
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i8*, i8** %in.addr, align 4
  %cmp = icmp eq i8* null, %0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %inlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i8*, i8** %out.addr, align 4
  %cmp2 = icmp eq i8* null, %2
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %3 = load i8*, i8** %key.addr, align 4
  %cmp5 = icmp eq i8* null, %3
  br i1 %cmp5, label %land.lhs.true6, label %if.end9

land.lhs.true6:                                   ; preds = %if.end4
  %4 = load i32, i32* %keylen.addr, align 4
  %cmp7 = icmp ugt i32 %4, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %land.lhs.true6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %land.lhs.true6, %if.end4
  %5 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then11

lor.lhs.false:                                    ; preds = %if.end9
  %6 = load i32, i32* %outlen.addr, align 4
  %cmp10 = icmp ugt i32 %6, 64
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %lor.lhs.false, %if.end9
  store i32 -1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %lor.lhs.false
  %7 = load i32, i32* %keylen.addr, align 4
  %cmp13 = icmp ugt i32 %7, 64
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end12
  store i32 -1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end12
  %8 = load i32, i32* %keylen.addr, align 4
  %cmp16 = icmp ugt i32 %8, 0
  br i1 %cmp16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.end15
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_state__], [1 x %struct.blake2b_state__]* %S, i32 0, i32 0
  %9 = load i32, i32* %outlen.addr, align 4
  %10 = load i8*, i8** %key.addr, align 4
  %11 = load i32, i32* %keylen.addr, align 4
  %call = call i32 @blake2b_init_key(%struct.blake2b_state__* %arraydecay, i32 %9, i8* %10, i32 %11)
  %cmp18 = icmp slt i32 %call, 0
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.then17
  store i32 -1, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.then17
  br label %if.end26

if.else:                                          ; preds = %if.end15
  %arraydecay21 = getelementptr inbounds [1 x %struct.blake2b_state__], [1 x %struct.blake2b_state__]* %S, i32 0, i32 0
  %12 = load i32, i32* %outlen.addr, align 4
  %call22 = call i32 @blake2b_init(%struct.blake2b_state__* %arraydecay21, i32 %12)
  %cmp23 = icmp slt i32 %call22, 0
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.else
  store i32 -1, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.else
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.end20
  %arraydecay27 = getelementptr inbounds [1 x %struct.blake2b_state__], [1 x %struct.blake2b_state__]* %S, i32 0, i32 0
  %13 = load i8*, i8** %in.addr, align 4
  %14 = load i32, i32* %inlen.addr, align 4
  %call28 = call i32 @blake2b_update(%struct.blake2b_state__* %arraydecay27, i8* %13, i32 %14)
  %arraydecay29 = getelementptr inbounds [1 x %struct.blake2b_state__], [1 x %struct.blake2b_state__]* %S, i32 0, i32 0
  %15 = load i8*, i8** %out.addr, align 4
  %16 = load i32, i32* %outlen.addr, align 4
  %call30 = call i32 @blake2b_final(%struct.blake2b_state__* %arraydecay29, i8* %15, i32 %16)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end26, %if.then24, %if.then19, %if.then14, %if.then11, %if.then8, %if.then3, %if.then
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2(i8* %out, i32 %outlen, i8* %in, i32 %inlen, i8* %key, i32 %keylen) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %1 = load i32, i32* %outlen.addr, align 4
  %2 = load i8*, i8** %in.addr, align 4
  %3 = load i32, i32* %inlen.addr, align 4
  %4 = load i8*, i8** %key.addr, align 4
  %5 = load i32, i32* %keylen.addr, align 4
  %call = call i32 @blake2b(i8* %0, i32 %1, i8* %2, i32 %3, i8* %4, i32 %5)
  ret i32 %call
}

declare i8* @memset(i8*, i32, i32) #3

; Function Attrs: noinline nounwind optnone
define internal i64 @rotr64(i64 %w, i32 %c) #0 {
entry:
  %w.addr = alloca i64, align 8
  %c.addr = alloca i32, align 4
  store i64 %w, i64* %w.addr, align 8
  store i32 %c, i32* %c.addr, align 4
  %0 = load i64, i64* %w.addr, align 8
  %1 = load i32, i32* %c.addr, align 4
  %sh_prom = zext i32 %1 to i64
  %shr = lshr i64 %0, %sh_prom
  %2 = load i64, i64* %w.addr, align 8
  %3 = load i32, i32* %c.addr, align 4
  %sub = sub i32 64, %3
  %sh_prom1 = zext i32 %sub to i64
  %shl = shl i64 %2, %sh_prom1
  %or = or i64 %shr, %shl
  ret i64 %or
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2b_set_lastnode(%struct.blake2b_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state__*, align 4
  store %struct.blake2b_state__* %S, %struct.blake2b_state__** %S.addr, align 4
  %0 = load %struct.blake2b_state__*, %struct.blake2b_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state__, %struct.blake2b_state__* %0, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 1
  store i64 -1, i64* %arrayidx, align 8
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
