; ModuleID = 'blake2sp-ref.c'
source_filename = "blake2sp-ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.blake2sp_state__ = type { [8 x [1 x %struct.blake2s_state__]], [1 x %struct.blake2s_state__], [512 x i8], i32, i32 }
%struct.blake2s_state__ = type { [8 x i32], [2 x i32], [2 x i32], [64 x i8], i32, i32, i8 }
%struct.blake2s_param__ = type { i8, i8, i8, i8, i32, i32, i16, i8, i8, [8 x i8], [8 x i8] }

@blake2sp_keyed_kat = internal constant [256 x [32 x i8]] [[32 x i8] c"q\\\B18\95\AE\B6x\F6\12A`\BF\F2\14e\B3\0FOht\19?\C8Q\B4b\10C\F0\9C\C6", [32 x i8] c"@W\8F\FAR\BFQ\AE\18f\F4(M:\15\7F\C1\BC\D3j\C1<\BD\CB\03w\E4\D0\CD\0Bf\03", [32 x i8] c"g\E3\09uE\BA\D7\E8R\D7MN\B5H\EC\A7\C2\19\C2\02\A7\D0\88\DB\0E\FE\AC\0E\AC0BI", [32 x i8] c"\8D\BC\C0X\9A=\17)jzX\E2\F1\EF\F0\E2\AAB\10\B5\8D\1F\88\B8m{\A5\F2\9D\D3\B5\83", [32 x i8] c"\A9\A9e,\8Cgu\94\C8r\12\D8\9DZu\FB1\EFOG\C6X,\DE_\1E\F6k\D4\94S:", [32 x i8] c"\05\A7\18\0EYPTs\99H\C5\E38\C9_\E0\B7\FCa\ACX\A75ttV3\BB\C1\F7p1", [32 x i8] c"\81M\E81S\B8\D7]\FA\DE)\FD9\ACr\DD\09\CA\0F\9B\C8\B7\ABj\06\BA\EE}\D0\F9\F0\83", [32 x i8] c"\DF\D4\19D\91)\FF`O\0A\14\8BL}h\F1\17O}\0F\8C\8D,\E7\7FD\8F\D3A\9Co\B0", [32 x i8] c"\B9\ED\22\E7\DD\8D\D1N\E8\C9[ \E7c.\85S\A2h\D9\FF\863\ED<!\D1\B8\C9\A7\0B\E1", [32 x i8] c"\95\F01g\1AN<TD\1C\EE\9D\BE\F4\B7\AC\A4F\18\A3\A33\ADt\06\D1\97\AC[\A0y\1A", [32 x i8] c"\E2\92[\9D\\\A0\FFb\88\C5\EA\1A\F2\D2+\0Aky\E2\DA\E0\8B\FD6\C3\BE\10\BB\8Dq\D89", [32 x i8] c"\16$\9CtNIQE\1DL\89O\B5\9A>\CB?\BF\B7\A4_\96\F8]\15\80\AC\0B\84-\96\DA", [32 x i8] c"C+\C9\1CR\AC\EB\9D\AE\D8\83(\81d\86P\C1\B8\1D\11z\BDh\E0\84QP\8Ac\BE\00\81", [32 x i8] c"\CD\E8 +\CF\A3\F3\E9]y\BA\CC\16]Rp\0E\F7\1D\87J<c~cOdDsr\0Dk", [32 x i8] c"\16!b\1F\\>\E4F\89\9D<\8A\AEI\17\B1\E6\DBJ\0E\D0B1_\B2\C1t\82^\0A\18\19", [32 x i8] c"3n\8E\BCq\E2\09\\'\F8d\A3\12\1E\FD\0F\AAzA(W%\A5\92\F6\1B\ED\ED\9D\DE\86\ED", [32 x i8] c"\07\9B\E0A\0Ex\9B6\EE\7FU\C1\9F\AA\C6\91en\B0R\1FB\94\9B\84\EE)\FE*\0E\7F6", [32 x i8] c"\17'\0CO4\88\08-\9F\F9\93~\AB<\A9\9C\97\C5\B4YaG7-\D4\E9\8A\CF\13\DB(\10", [32 x i8] c"\18<8uM\03A\CE\07\C1zl\B6\C2\FD\8B\BC\C1@O\DD\01A\99\C7\8B\E1\A9uY\A9(", [32 x i8] c"nR\D7(\A4\05\A6\E1\F8u\87\BB\C2\AC\91\C5\C0\9B-\82\8A\C8\1E\\J\81\D0=\D4\AA\8D\\", [32 x i8] c"\F4\E0\8E\05\9Bt\14K\F9H\14m\14\A2\C8\1EF\DC\15\FF&\EBR4L\DDGJ\BE\A1K\C0", [32 x i8] c"\0F.\0A\10\0E\D8\A1\17\85\96*\D4Yj\F9U\E3\0B\9A\EF\93\0A$\8D\A92+p-Khr", [32 x i8] c"Q\90\FC\C72\F4\04\AA\D46J\C7\96\0C\FD[N4\86)\C3r\EE\B3%\B5\C6\C7\CB\CEY\AB", [32 x i8] c"\C0\C4\CB\86\EA%\EA\95~\EC[\22\D2U\0A\16I\E6\DF\FA1k\B8\F4\C9\1B\8F\F7\A2K%1", [32 x i8] c",\9E\DA\13Z0\AE\CA\F3\AC\B3\D2:05\FB\AB\BA\9831e\D8\7F\CB\F8\FE\103n\CF ", [32 x i8] c"<\D6i\E8\D5bb\A27\13g\22M\AEmu\9E\E1R\C3\153\B2c\FA.d\92\08w\B2\A7", [32 x i8] c"\18\A9\A0\C2\D0\EAl;\B32\83\0F\89\18\B0hO]9\94\DFHgF-\D0n\F0\86$$\CC", [32 x i8] c"s\90\EAA\04\A9\F4\EE\A9\0F\81\E2j\12\9D\CF\9FJ\F3\83R\D9\CBj\81,\C8\05i\09\05\0E", [32 x i8] c"\E4\9E\01\14\C6)\B4\94\B1\1E\A9\8E\CD@2s\1F\15;FP\AC\AC\D7\E0\F6\E7\DE=\F0\19w", [32 x i8] c"'\C5p+\E1\04\B3\A9O\C44#\AE\EE\83\AC<\A7;\7F\87\83\9Ak.)`y\03\B7\F2\87", [32 x i8] c"\81\D2\E1.\B2\F4'`\C6\E3\BA\A7\8F\84\07:\E6\F5a`p\FE%\BE\DE||\82H\AB\1F\BA", [32 x i8] c"\FA\B25\D5\93H\AB\8C\E4\9B\ECw\C0\F1\93(\FD\04]\FD`\8AS\036\DFO\94\E1r\A5\C8", [32 x i8] c"\8A\AA\8D\80\\X\88\1F\F3y\FB\D4,k\F6\F1Lls\DF\80q\B3\B2(\98\11\09\CC\C0\15\F9", [32 x i8] c"\91\FD\D2b 9\169G@\95+\CEr\B6K\AB\B6\F7!4M\EE\82P\BF\0EF\F1\BA\18\8F", [32 x i8] c"\F7\E5{\8F\85\F4}Y\03\ADL\CB\8A\F6*>\85\8A\AB+\8C\C2&IO{\00\BE\DB\F5\B0\D0", [32 x i8] c"\F7o!\AD\DA\E9j\96F\FC\06\F9\BFR\AE\08H\F1\8C5&\B1)\E1[,5^.y\E5\DA", [32 x i8] c"\8A\EB\1Cy_4\90\01^\F4\CDa\A2\80{#\0E\FD\C8F\01s\DA\D0&\A4\A0\FC\C2\FB\F2*", [32 x i8] c"\C5d\FF\C6#\07we\BB\97\87XVT\CEt]\BD\10\8C\EF$\8A\B0\0A\D1\A2d}\99\03\87", [32 x i8] c"\FE\89B\A3\E5\F5\E8\CDpQ\04\F8\82\10rnS\DD~\B3\F9\A2\02\BF\93\14\B3\B9\06^\B7\12", [32 x i8] c"\DC)SY\D46\EE\A7\80\84\E7\B0w\FE\09\B1\9C[\F3\D2\A7\96\DA\B0\19\E4 \05\99\FD\82\02", [32 x i8] c"p\B3\F7/t\902\E2^8;\96Cx\EA\1CT>\9C\15\DE:'\D8m*\9D\221\EF\F4\8A", [32 x i8] c"y\82\B5L\08\DB+\FBoE\F3[\C3#\BC\097y\B6\BB\0E>\EA>\8C\98\B1\DE\99\D3\C5^", [32 x i8] c"u\E4\16\22W\01K\ED\CC\05\C2\94M\CE\0D\F0\C3^\BA\13\19T\06OnN\09_\D0\84E\EE", [32 x i8] c"J\12\9E\A6\CD\BA\BC-9$y7/\97[\9C\F5\A1\B7\DE\B6\9A2f\F0>\BCm\11\13\93\C4", [32 x i8] c"\8F\EDp\F2yU\DC\8A\D9\F1\B7\B3\F6\F5\DF\BD\96*3Y+B\DE\85mB\1E)\12\BA\B8k", [32 x i8] c"\E2\F2\06`7o+\189f|\BF\E5\E1n\F0u\AC9CdO52(/\8B\B0r;\99\86", [32 x i8] c"\AB\F8L\91:\83\DF\98\C7\00)\81\9C\06_mm\E4\F6\D4:\BF`\0D\AD\E05\B2;\ED{\AA", [32 x i8] c"E\9C\15\D4\85l~\CF\82b\03Q\C3\C1\C7l@?>\97\07t\13\87\E2\99\07?\B1pK+", [32 x i8] c"\9A\B9\12\ED\A0v\8A\BD\F8&\B6\E0]\0DsX9\E6\A5\F0.\04\C4\CCue\0B,\8C\ABgI", [32 x i8] c"G@\EB\EC\AC\90\03\1B\B7\E6\8EQ\C5S\91\AF\B1\89\B3\17\F2\DEU\87f\F7\8F\\\B7\1F\81\B6", [32 x i8] c"<\C4\7F\0E\F6H!X|\93|\DD\BA\85\C9\93\D3\CE-\D0\CE\D4\0D;\E3<\B7\DC~\DA\BC\F1", [32 x i8] c"\9FGj\22\DBT\D6\BB\9B\EF\DB&\0CfW\8A\E1\D8\A5\F8}=\8C\01\7F\DBtu\08\0F\A8\E1", [32 x i8] c"\8Bh\C6\FB\07\06\A7\95\F3\A89\D6\FE%\FDJ\A7\F9.fOv-aS\81\BC\85\9A\FA),", [32 x i8] c"\F6@\D2%\A6\BC\D2\FC\8A\CC\AF\BE\D5\A8K[\BB]\8A\E5\DB\06\A1\0Bm\9D\93\16\0B9.\E0", [32 x i8] c"pH`\A7\F5\BAh\DB'\03\1C\15\F2%P\0Di*\B2GSB\81\C4\F6\84\F6\C6\C8\CD\88\C7", [32 x i8] c"\C1\A7[\DD\A1+\8B*\B1\B9$\848X\18:\09\D2\02B\1F\DB\CD\F0\E6>\AEF\F3}\91\ED", [32 x i8] c"\9A\8C\ABz_.Wb!\A6\A8^_\DD\EEug\8E\06S$\A6\1D\B0:9&\1D\DFu\E3\F4", [32 x i8] c"\05\C2\B2k\03\CEl\A5\87\1B\E0\DE\84\EE'\86\A7\9B\CD\9F0\03>\81\9BJ\87\CC\A2z\FCj", [32 x i8] c"\B0\B0\99<m\0Cn\D5\C3Y\04\80\F8e\F4g\F43\1AX\DD\8EG\BD\98\EB\BC\DB\8E\B4\F9M", [32 x i8] c"\E5|\10<\F7\B6\BB\EB\8A\0D\C8\F0Hb\\?L\E4\F1\A5\ADM\07\9C\11\87\BF\E9\EE;\8A_", [32 x i8] c"\F1\00#\E1_;r\B78\ADa\AEe\AB\9A\07\E7wN-z\B0-\BAN\0C\AFV\02\C8\01x", [32 x i8] c"\9A\8F\B3\B58\C1\D6\C4PQ\FA\9E\D9\B0}>\89\B4C\030\01J\1E\FA(#\C0\82<\F27", [32 x i8] c"0u\C5\BC|:\D7\E3\92\01\01\BCh\99\C5\8E\A7\01g\A7w,\A2\8E8\E2\C1\B0\D3%\E5\A0", [32 x i8] c"\E8U\94p\0E9\22\A1\E8\E4\1E\B8\B0d\E7\ACm\94\9D\13\B5\A3E#\E5\A6\BE\AC\03\C8\AB)", [32 x i8] c"\1D7\01\A5f\1B\D3\1A\B2\05b\BD\07\B7M\D1\9A\C8\F3RKs\CE{\C9\96\B7\88\AF\D2\F3\17", [32 x i8] c"\87N\198\03=}85\97\A2\A6_X\B5T\E4\11\06\F6\D1\D5\0E\9B\A0\EBh_km\A0q", [32 x i8] c"\93\F2\F3\D6\9B-6R\95V\EC\CA\F9\F9\9A\DB\E8\95\E1W\221\E6I\B5\05\84\B5\D7\D0\8A\F8", [32 x i8] c"\06\E0ma\0F.\EB\BA6v\82>wD\D7Q\AF\F70v\EDe\F3\CF\F5\E7/\D2'\99\9Cw", [32 x i8] c"\8D\F7W\B3\A1\E0\F4\80\FAv\C7\F3X\ED\03\98\BE?*\8F{\90\EA\8C\80u\99\DE\DA\1D\054", [32 x i8] c"\EE\C9\C5\C6<\C5\16\9D\96{\B1bN\9E\E5\CE\D9(\97sn\FB\D1WT\8D\82\E8|\C7/%", [32 x i8] c"\CC+X2\AD',\C5\\\10\D4\F8\C7\F8\BB8\E6\E4\EB\92/\93\86\83\0F\90\B1\E3\DA97\D5", [32 x i8] c"6\89\85\D58|\0B\FC\92\8A\C2T\FAm\16g>p\94uf\96\1B_\B32ZX\8A\B3\17:", [32 x i8] c"\F1\E4B\AF\B8r\15\1F\814\95lT\8A\E3$\0D\07\E6\E38\D4\A7\A6\AF\8D\A4\11\9A\B0\E2\B0", [32 x i8] c"\B0\12\C7Tj9\C4\0C\AD\EC\E4\E0N\7F3\C5\93\AD\18.\BCZF\D2\DB\F4\AD\1A\92\F5\9E{", [32 x i8] c"l`\97\CD 3\09kM\F3\17\DE\8A\90\8B}\0Cr\949\0CZ9\9C0\1B\F2\A2e.\82b", [32 x i8] c"\BA\83\FE\B5\10\B4\9A\DEO\AE\FB\E9Bx\1E\AF\D4\1A\D5\D46\88\851\B6\88Y\F2,-\16J", [32 x i8] c"Z\06\9EC\92\19Z\C9\D2\84\A4\7F;\D8T\AF\8F\D0\D7\FD\C3H=,_4$\CC\FD\A1\\\8E", [32 x i8] c"~\88\D6K\BB\E2\02ODT\BA\13\98\B3\D8e-\CE\C8 \B1L;\0A\BF\BF\0FO3\06\BB^", [32 x i8] c"\F8t/\F4m\FD\F3\EC\82d\F9\94[ A\94b\F0i\E83\C5\94\EC\80\FF\AC^~Q4\F9", [32 x i8] c"\D3\E0\B78\D2\E9/<G\C7\94ff\09\C0\F5POg\ECNv\0E\EE\CC\F8dNh34\11", [32 x i8] c"\0C\90\CE\10\ED\F0\CE\1DG\EE\B5\0B[z\FF\8E\E8\A4;d\A8\89\C1\C6\C6\B8\E3\1A<\FCE\EE", [32 x i8] c"\83\91z\C1\CD\AD\E8\F0\E3\BFBo\EA\C18\8B?\CB\E3\E1\BF\98y\8C\81X\BFu\8E\8D]N", [32 x i8] c"\DC\8E\B0\C0\13\FA\9D\06N\E3v#6\9F\B3\94\AF\97K\1A\AC\82@[\88\97l\D8\FC\A1%0", [32 x i8] c"\9A\F4\FC\92\EA\8Dk_\E7\99\0E:\02p\1E\C2+-\FDq\00\B9\0D\05Q\86\94\17\95^D\C8", [32 x i8] c"\C7\22\CE\C11\BA\A1c\F4~K3\9E\1F\B9\B4\AC\A2H\C4u\93E\EA\DB\D6\C6\A7\DD\B5\04w", [32 x i8] c"\187\B1 \D4\E4\04lm\E8\CC\AF\09\F1\CA\F3\02\ADV#NkB,\E9\0Aa\BF\06\AE\E4=", [32 x i8] c"\87\AC\9D\0F\8A\0B\11\BF\ED\D6\99\1Am\AF4\C8\AA]~\8A\E1\B9\DFJ\F78\00_\E7\8C\E9<", [32 x i8] c"\E2\1F\B6h\EB\B8\BF-\82\08m\ED\CB:Sq\C2\C4o\A1\AC\11\D2\E2\C5f\D1J\D3\C3e?", [32 x i8] c"Z\9Ai\81^M>\B7r\ED\90\8F\E6X\CEP\871\0E\C1\D5\0C\B9OV(3\9Aa\DC\D9\EE", [32 x i8] c"\AA\C2\85\F1 \8Fp\A6G\97\D0\A9@\0D\A6FS0\188\FE\F6i\0B\87\CD\A9\15\9E\E0~\F4", [32 x i8] c"\05d<\1Co&Y%\A6P\93\F9\DE\8A\19\1COo\D1A\8F\BFf\BE\80Y\A9\1B\A8\DC\DAa", [32 x i8] c"\1Cl\DE[x\10<\9Eo\04m\FE0\F5\12\1C\F9\D4\03\9E\FE\22%@\A4\1B\BC\06\E4i\FE\B6", [32 x i8] c"\B4\9B\B4m\1B\19;\04^t\12\05\9F\E7-U%R\A8\FBl6A\07#\DC}\05\FC\CE\DE\D3", [32 x i8] c"\B6\12\D3\D2\1F\C4\DE<y\1A\F75\E5\9F\B7\17\D89r;BP\8E\9E\BFx\06\D9>\9C\83\7F", [32 x i8] c"|3\90\A3\E5\CB'\D1\86\8B\A4U\CF\EB2\22\FD\E2{\CD\A4\BF$\8E=)\CF\1F42\9F%", [32 x i8] c"\BDB\EE\A7\B3T\86\CD\D0\90|\B4q.\DE/M\EE\CC\BC\A1\91`8e\A1\CC\80\9F\12\B4F", [32 x i8] c"\D1\DDb\01t\0C\FA\ADS\CE\CC\B7V\B1\10\F3\D5\0F\81{C\D7U\95W\E5z\AD\14:\85\D9", [32 x i8] c"X)d<\1B\10\E1\C8\CC\F2\0C\9BJ\F8!\EA\05-\7F\0F|\22\F78\0B\BB\CF\AF\B9w\E2\1F", [32 x i8] c"\FCL\F2\A7\FB\E0\B1\E8\AE\FB\E4\B4\B7\9E\D8N\C9{\03OQ\B4\E9\7Fv\0B c\97e\B93", [32 x i8] c"M|;48\A0\BD\A2\8Ez\96\E4 '\D8\13\E8\8A\E6(\85I\983\D3\C5\F65\9E\F7\ED\BC", [32 x i8] c"4\CB\D3 h\EF~\82\09\9EX\0B\F9\E2d#\E9\81\E3\1B\1B\BC\E6\1A\EA\B1L2\A2s\E4\CB", [32 x i8] c"\A0]\DA}\0D\A9\E0\94\AE\22S?y\E7\DC\CD&\B1u|\EF\B9[\CFb\C4\FF\9C&\92\E1\C0", [32 x i8] c"\22L\CF\FA|\CAL\E3J\FDG\F6*\DES\C5\E8H\9B\04\AC\9CA\F7\FA\D0\C8\ED\EB\89\E9A", [32 x i8] c"k\C6\07d\83\AA\11\C0\7F\BAU\C0\F9\A1\B5\DA\87\EC\BF\FE\A7U\98\CC1\8AQL\EC{;j", [32 x i8] c"\9A\03`\E2:\22\F4\F7l\0E\95(\DA\FD\12\9B\B4g_\B8\8DD\EA\F8Ww0\0C\EC\9B\CCy", [32 x i8] c"y\01\99\B4\CA\90\DE\DC\CF\E3$t\E8[\17O\06\9E5B\BE1\04\C1\12\\/\DB\D6\9D2\C7", [32 x i8] c"U\83\99%\83L\A3\E8%\E9\92A\87M\16\D6\C2b6)\C4\C2\AD\DD\F0\DB\A0\1El\E8\A0\DC", [32 x i8] c"a_\F8F\D9\93\00}8\DE\1A\EC\B3\17\82\89\DE\D0\9Ek\B5\CB\D6\0Fi\C6\AA680 \F7", [32 x i8] c"\F0\E4\0BN\D4\0D4\85\1Er\B4\EEM\00\EAj@\EA\1C\1B\F9\E5\C2iq\0C\9DQ\CB\B8\A3\C9", [32 x i8] c"\0B\07\B23;\08\D0\8C\11\CA4\ABD\9Bq\D2\9A\0FC\E1\F7x\E0s\E7\90\06\CC\B70\EDb", [32 x i8] c"\D1\F4\C2\9D\9F#\EA5\EC@5\B3w\D5\06S\8Er\8B\C79\C1E\96\80\CF\1C\C6\94$\92M", [32 x i8] c"\12y\CFof\9F\92\F6\BF\C2]`[\94@\C7\DC\CB\D2]\F2\8D\C75:\BC\1C\050@]\C4", [32 x i8] c"\1F\A0\AF\00w]\C2\CEvPm2\80\F4r\D2\F6\FF\97\A2\15\1F\AA\82yB\FE\A4J\D0\BA\1F", [32 x i8] c">\1A\D5J_\83[\98;\D2\AA\B0\ED*L\0B\DDr\16 \9C6\A7\9E\9E*\AB\B9\9F\AF5\12", [32 x i8] c"\C6\ED9\E2\D8\B66\EC\CB\A2E\EFN\88d\F4\CD\94k\E2\16\B9\BEH0>\08\B9-\D0\944", [32 x i8] c"\E2G6\C1>\CB\9F6\A0\D8)\D4y\8Dv\99\C1L\C6[m\C4N\D6\F1\0C\D4\85=n\07W", [32 x i8] c"8\9B\E8\80R\A3\81',m\F7A\A8\8A\D3I\B7\12q\845H\0A\81\90\B7\04w\1D-\E67", [32 x i8] c"\88\9F-W\8A]\AE\FD4\1C!\09\84\E1&\D1\D9m\A2\DE\E3\C8\1Fz`\80\BF\84V\9B1\14", [32 x i8] c"\E96\09[\9B\98/\FC\85m/Rv\A4\E5)\ECs\95\DA1mb\87\02\FB(\1A\DAo8\99", [32 x i8] c"\EF\89\CE\1Do\8BH\EA\\\D6\AE\ABj\83\D0\CC\98\C9\A3\A2\07\A1\08W2\F0G\D9@8\C2\88", [32 x i8] c"\F9%\01my\F2\AC\A8\C4\9E\DF\CDf!\D5\BE<\8C\ECa\BDXq\D8\C1\D3\A5e\F3^\0C\9F", [32 x i8] c"c\E8cKuz8\F9+\92\FD#\89;\A2\99\85:\86\13g\9F\DF~\05\11\09\\\0F\04{\CA", [32 x i8] c"\CF,\CA\07r\B7\05\EBW\D2\89C\F8=5?\E2\91\E5\B3wx\0B7L\8B\A4fX0\BE\87", [32 x i8] c"F\DF[\87\C8\0E~@t\AE\E6\85YBGB\84[\9B5\0FQ\BAU\B0t\BB\AELbj\AB", [32 x i8] c"e\8A\A4\F9\D2\BC\BDO\7F\8E\B6>h\F56~\DB\C5\00\A0\B1\FB\B4\1E\9D\F1A\BC\BA\8F\CDS", [32 x i8] c"\EE\80UP\08\A7\16U\E0\81\09+\BAog\0E\D9\8A\F9\A0\9F\B5\AF\B9L\BC\\uH\14\DBO", [32 x i8] c",_\9D\04\82 \B0A\B6\D4RKD\90\CF\8Cf\FC\B8\E1K\0Dd\88z\A1\E4v\1A`+9", [32 x i8] c"D\CBc\11\D0u\0B~3\F73:\A7\8A\AC\A9\C3J\D5\F7\9C\1B\15\91\EC3\95\1Ei\C4\C4a", [32 x i8] c"\0Cl\E3*>\A0V\12\C5\F8\09\0Fj~\87\F5\AB0\E4\1Bp}\CB\E5AUb\0A\D7p\A3@", [32 x i8] c"\C6Y8\DD:\05<r\9C\F5\B7\C8\9F9\0B\FE\BBQ\12vk\B0\0A\A5\FA1d\DF\DF;VG", [32 x i8] c"}\E7\F0\D5\9A\909\AF\F3\AA\F3,>\E5.y\17SW)\06!h\D2I\0Bkl\E2D\B3\80", [32 x i8] c"\89X\98\F5:\8F9\E4$\10\DAw\B6\C4\81[\0B\B29^9\22\F5\BE\D0\E1\FB\F2\A4\C6\DF\EB", [32 x i8] c"\C9\05\A8I\844\8Ad\DB\1FT \83t\8A\D9\0AK\AD\983\CBm\A3\87)41\F1\9E|\9C", [32 x i8] c"\ED7\D1\A4\D0l\90\D1\95xHf~\95H\FE\BB]B>\ABOVx\\\C4\B5Akx\00\08", [32 x i8] c"\0B\C6]\99\97\FBsJV\1F\B1\E9\F8\C0\95\8A\02\C7\A4\DB\D0\96\EB\EF\1A\17Q\AE\D9Y\EE\D7", [32 x i8] c"|_C.\B8\B75*\94\94\DE\A4\D5<!8p1\CEp\E8]\94\08\FCo\8C\D9\8Aj\AA\1E", [32 x i8] c"\B8\BF\8E,4\E03\9869\90\9E\AA7d\0D\87{\04\8F\E2\99\B4p\AF-\0B\A8*_\14\C0", [32 x i8] c"\88\A9\DD\13\D5\DA\DB\DE\E6\BF\F7\EE\1E\F8\C7\1C\C1\93\AAK\F3\E8O\8F\E8\0C\B0uh<\07y", [32 x i8] c"\9A\ED\B8\87m\D2\1C\8C\84\D2\E7\02\A16%\98\04b\F6\8B\F0\A1\B7%J\D8\06\C3\84\03\C9\DE", [32 x i8] c"\D0\97W=\F2\D6\B2H\9AG\94\84\86\98\00\A1\F83\EA\16\9E\FF2\AE<\E6: yT\8Dx", [32 x i8] c"\D1\8F'\A3\E5U\D7\F9\1A\00|g\AC\EE\DE9\1Fu\A6\1F\A4*\0BEf\EBX,\A0^\BC\E7", [32 x i8] c"\DF\1D\AA\90\B1p#\13\E6\A5\90\1Cz\FC^\D9ew\17\A7\15\FAS\A4\18\9E\C1\E5\DF):h", [32 x i8] c"\04\E3\A4\96\B6i\96\C6n2\91\9E\D1\F9L6\EE\BB\F2@c:/s\98E\F0)]4\AF\BA", [32 x i8] c"\8CE\D8\8CN\9C\9D\0C\8Cg\7F\E4\8F\A5D\9B\A3\01x\D4\0A\F0\F0!y!\C6.K`\CD\D3", [32 x i8] c"\E1I\A6\B1;\DE\DE\A2\EE\EE\00\9C\E9D^\8D\CFv\B7nU\A5\01\D8\F5\B4?\F8\96yj\D1", [32 x i8] c"\A87\C4\C7\C6\F5\CF\B9\9E\10\85\FDC(zA\05\CB(\B7o\C3\8B`U\C5\DC\FFx\B8%e", [32 x i8] c"BA\1F(x\0BO\168T\0B\87\05!\ECE\BC\EB\1E\0Cq1\F7\E1\C4g.Cl\88\C8\E9", [32 x i8] c"4\B4\E8vv\94q\DFU.U\22\CE\A7\84\FAS\ACa\BE\DE\8C\FE)\14\09\E6\8Bi\E8wo", [32 x i8] c"\8F1\D67\A9\1D\BD\0E\CB\0B\A0\E6\94\BE\C1DvX\CEl'\EA\9B\95\FF6p\1C\AF6\F0\01", [32 x i8] c"\B5\C8\95\EB\07\1E=8R\8DG];\B0\BA\88\B7\17\95\E4\0A\98.*\C2\D8D\22\A0\F2h]", [32 x i8] c"\E9\06%|A\9D\94\1E\D2\B8\A9\C1'\81\DB\97Y\A3\FC\F3\DC|\DB\03\15\99\E1\08kg/\10", [32 x i8] c"\98\AD$9|n\AEL\F7>\A8\BB\EFZ\0Bt\D2\1A\D1_3\92\0FD\07\0A\98\BD\F5=\0B:", [32 x i8] c"\DDQ\0C\A5[\11p\F9\CE\FD\BB\16\FC\14Rb\AA6:\87\0A\01\E1\BCO\BE@#KKo/", [32 x i8] c"\F2\D8\D91\B9.\1C\B6\98\E5n\D0(\19\EA\11\D2f\19\B8:b\09\ADg\22Sh\FE\11\95q", [32 x i8] c"\E4cpU\DB\91\F9C|\F4`\EF@\B5\14_i\98&j^t\E9j\00x,b\CF0\CF\1C", [32 x i8] c"5cS\0A\89\D3+u\F7\8D\83\E9\87*\D4\C5u\F5 9\9De\03]\ED\99\E5\EE\C5\80qP", [32 x i8] c"\8Ey\F9,\86[\EB>\1C\DB\F0\8FuJ&\06\E8SI\05=f\D6\16\02J\81?\CAT\1AM", [32 x i8] c"\86B&\F2\83\9Cv\B1\D5\F7\C1=\98\C2\A5\15\8C*\BBq\D9\D8\F0\FA\1F|?th\00\16\03", [32 x i8] c"\D3\E3\F5\B8\CE\EB\B1\11\84\8055\90\0Bn\ED\DA`n\EB6\97Q\A7\CD\A3l\A3\02)\FB\02", [32 x i8] c"\8C}k\98ri\16\901\F7\1F\D7\E4\C4E\01->j<\88\09\F6G\9B\D6g\CF1\1E'n", [32 x i8] c"\B9\04\B5q\1B\F1\9E\852\F7\ADd'A\0Ab\A1\F7\7Fw\B9\B6\D7\1D/\C4;\C9\0Fs#Z", [32 x i8] c"E6cC\15\C8g(\F5\ABtI\EB-\04\02\0E\9E\AE\8D\D6yU\00\E9\EC\9A\00f8ni", [32 x i8] c"\FD^I\FE\D4\9D\C4K\DE\89\F4`\A9P\19\1E\BB\06|i\8A?!\EA\140\8Ct\13\B9\16\81", [32 x i8] c"1\F0\1D\03\0B\9B\22\D0\0A\0Fq\ED,\EB]-\C8\1A\F2\C2K\F5g\0F\DE\19\A6\85\E8\D19.", [32 x i8] c"_\84\D9\DE(K\1EOg\8E1\ABjv\F5f\1BZ\EA\A7hS\93\84\AA8\F9\E4\9C\CEnn", [32 x i8] c"\B2\07\9EY\97\A4\EA\D3\A7\1F\EF\C0/\90\A7H:\10\FD.o1\BD\A9\D2\08D\85\CC\01k\BD", [32 x i8] c"\E0\F8M\7FR[o\EDy\1Fw(\9A\E5\8F}P\A2\942\D4,%\C1\E89)\B88\89\1Dy", [32 x i8] c"pF\96\90\95my\18\AC\E7\BA_A0-\A18\C9\B5n\CDAUD\FA\CE\8D\99\8C!\AB\EB", [32 x i8] c"E\C9\1Ab$\9B9\CD\A9NP\82\95\BE\C7fq\19Dwe\EF\80\EF\A8-\1E\92\D5pg\D8", [32 x i8] c"\1D\9E\00s\EE\D0s\15T\C3\BE\AAGF\0DQ\1A\D2a\DDMJ;\ED\9D\8D /\22\F2\15\89", [32 x i8] c"@\82bsm\8A\EC\0B\84}\BA%\02X`\8ACE\A6:\1E\B1\95\E5\C7\AE.\E8t\C3M\A8", [32 x i8] c"#\D2\B7\049F\99I\98#\90S\8D~Z\DE\9F\18\C8\E3\BB\F6`Z\FC\F4\9B\00\C0a\E87", [32 x i8] c"#/\B1\87\D2q\BE\A9\12\EF\D4\07\FF\E0\80V\D6\A4.S!\ECy-\F3\D5\84\A9Oc\0A\B2", [32 x i8] c"\13\8E\19D\E4\B5M\E8h\1D~H\C4\F0\81H\E4\0AV~\\\AD\94jj\F4\E8\D5\D2ou\C7", [32 x i8] c"\80\C1Q2_\BF\C6x\B7\BEN@\B3\0F)\FE1\CD\BE\1C\84\12n\00m\F3\C1\85$\BD-l", [32 x i8] c"\A6B&s\01f\9D\F2a\B89\F8sev)\05\FF2\0A\0A/\C4\BD\C4\8EZ\8E\15\D123", [32 x i8] c"\0F\8B\10\998`\93zt\CC-\E4\0A'1\DD\99T\B6T\BB\94\C3N\87fR\E9\8DK\BD\16", [32 x i8] c"\E64\A5\85\12I2s&\0F\10\D4IS\CD\99\8E4\CB\82\81\C4\1B\F4.\0A\E2\F2\\\BD\1Fu", [32 x i8] c"\BD\E6\AF\9B\AF<\07\E9T#\CA\B5\04\DE\E7\0E\DC\C31\8B\22\DD\1E\B6\FD\85\BEDz\C9\F2\09", [32 x i8] c"\91K7\AB[\8C\FD\E6\A4\80Fj\0D\82C,}v2\8E\9A\88\EF[ORB\9Fz?\FC}", [32 x i8] c"U\BEf\E9\A5\AAg\1A#\88.\F3\E7\D9\D3n\A9T\87\DCq\B7%\A5\ADKy\8A\87\91C\D0", [32 x i8] c"?\D0E\89K\83nD\E9\CAu\FB\E3\EA\DCHl\BB\D0\D8\CE\E1\B3\CF\14\F7n\7F\1Ew\AE\F3", [32 x i8] c"\CE`4=\C4\87Kf\04\E1\FB#\1E7\EC\1E\EC?\06VnB\8A\E7d\EF\FF\A20\AD\D4\85", [32 x i8] c"\E3\8C\9D\F0$\DE!S\D2&s\8A\0E[\A9\B8\C6xM\AC\A6\\\22\A7b\8E\B5\8E\A0\D4\95\A7", [32 x i8] c"\8D\FE\C0\D4\F3e\8A \A0\BA\D6o!`\83+\16Np\0A!\ECZ\01e\C3gr\B2\08a\11", [32 x i8] c"D\01\B5\0E\09\86_B8$;\82%\CA@\A0\8D\BBF\85\F5\F8b\FB\DDr\98\041\A8]?", [32 x i8] c"\86h\94'\88\C4\CE\8A3\19\0F\FC\FA\D1\C6x\C4\FAA\E9\94\17\09N$\0FJC\F3\87\A3\B6", [32 x i8] c"\A7(\8D^\09\80\9Bii\84\EC\D52l\DD\84\FB\E3_\CFg#]\81\1C\82\00%6\A3\C5\E1", [32 x i8] c"\8E\92\\<\14k\AC\F35\1E\C52A\AC\E5\F7>\8F\C9\BD\8Ca\CA\D9\7F\D7r\B0~\1B\83s", [32 x i8] c"\C7\EB\9Em\ED/\99=H\B0\17\0D\A2|[u;\12\17k\E1&\C7\BA-j\F8_\85\93\B7R", [32 x i8] c"\CA'\F1o\94\E4\EC\0Eb\8E\7F\8A\EF\C6e{\ED\C97B\96Y@\AExjs\B5\FDY;\97", [32 x i8] c"\8C!\E6V\8B\C6\DC\00\E3\D6\EB\C0\9E\A9\C2\CE\00l\D3\11\D3\B3\E9\CC\9D\8D\DB\FB<Zwv", [32 x i8] c"RVf\96\8B;}\00{\B9&\B6\EF\DC~!*1\15L\9A\E1\8DC\EE\0E\B7\E6\B1\A98\D3", [32 x i8] c"\E0\9AO\A5\C2\8B\DC\D7\C89\84\0E\0A8>Oz\10-\0B\1B\C8I\C9Ib|A\00\C1}\D3", [32 x i8] c"\C1\9F>)]\B2\FC\0Et\81\C4\F1j\F0\11U\DD\B0\D7\D18=J\1F\F1i\9D\B7\11w4\0C", [32 x i8] c"v\9Eg\8C\0A\09\09\A2\02\1CM\C2k\1A<\9B\C5W\AD\B2\1AP\83L\DC\\\92\93\F7Se\F8", [32 x i8] c"\B6Ht\AD\ABk\CB\85\B9K\D9\A6\C5e\D0\D2\BC5D]u(\BC\85\B4\1F\DCy\DCv\E3O", [32 x i8] c"\FA\F2P\DE\15\82\0F\7F\C6\10\DDS\EE\AED`\1C>\FF\A3\AC\CD\08\8E\B6i\05\BB&S\BE\8C", [32 x i8] c"\1E 8s\9B,\01\8B\0E\9E\0E\1ER/\D9e\12\87\EEn6e\91\9B$\C2\12O\0C\1A?:", [32 x i8] c"_\EC:\A0\08a\DE\1A\C5\DA\B3\C17\06]\1E\01\BB\03\F6\9D\CC}\1C\F7\CAOCV\AE\C9\A3", [32 x i8] c"DQ\FEk\BE\F3\93C\91\92D\C5\1D\AE\1E\A9\A9T\CF,\09f\AB\04[\15R\1E\CF5\00\81", [32 x i8] c"\8Cb/\A2\16\0E\8E\99\18\13\F1\80\BF\EC\0BC\1Cm\BF\A2\95m\91u\81j#\C3\82\C4\F2\00", [32 x i8] c"\81}\\\8F\92\E7\B5\CAW\F5\E1c\90\16\ADW`\E4F\D6\E9\CA\A7I\84\14\AC\E8\22\80\B5\CD", [32 x i8] c"\A6\A1\ADX\CE\E5Ni\CB\BC\AA\87\DF\07\A6p~\B2$s\9C!v\13F\0A\B4T\B4Y\CA\9C", [32 x i8] c"c\B8G'R&`[\E6v\81%\8F}\00\BB\B3\07\C6o\19Y\BF.FzA\AE\E7\14\E5\\", [32 x i8] c"\FER\EB\E5\CF\CF\E6\A2){S\9F\A3\DA\DB\D6\EB\D2\01\AA,\A15c\E3\D7\F1M\15\AB\FFc", [32 x i8] c"\B7\BE\F9\FAZ=\10BbF\B5\F6X\C0\8F\DF\80f\EA\A3\E5Z/}\A1Y\1E\05\C8}\F8\C7", [32 x i8] c"\DE\D1\D6\CA\A9\F8\F3\BD\A9,\EA\7FeI\B1\FB\86\A2!\14x\C4\EC(\9B\83~\FC+\\'\D7", [32 x i8] c"\9F0\00\8A.\B0P\F1\8EV\A7k\E9 \91\B2\FD\C1d\D5n2\C8}\D6L\9E:a\10A\B1", [32 x i8] c"\01\0Bj;\11\86\00\88\F0\AB\C8\0A\89r\CB\BC2\9DRu4)P\EB\9A\04Z\FD\C8\BB\ED$", [32 x i8] c"\0C\D2\10\AA\C1\1F\1C\1C\EDI\7Fg>S\DBh\C3\EC6\07\F0\C5x}\DC`\A3U\DF\E5l%", [32 x i8] c"\0EV\FD\01\DA;O\8B\E2\C9\90U*\AC\8D\1E\8D\A2\09\BC\F4\AA\D4\FF\B5B\7F\D61rF>", [32 x i8] c"\D6\D5\CD\B1\14@\E3J\CA:/\CF0\F5\9E\08\B1\1A*=\E59\E3\E6Q>\D7\8AO\EEQ;", [32 x i8] c"\AA5\AC\90h\06p\C72\ED\1E\F3~\8C\BA\AEI\A4\D8\8E\CFM\F2\B6\89\A0\F1\01\B7V\AEG", [32 x i8] c"'\8EV\12\88r&0\E2j_\C9T\BF-\CDje\81g9\AB\EE{\E1C\07\A9at\E5\B0", [32 x i8] c"\ABK,\A1\A2\B3I\98\15$\B6\15Tb\F0\FF\10`\BF\9B\FA\07\FB\9E\C6\9C\A4qd[j\18", [32 x i8] c"\18\A9\BB\EC<\8E\1F\8E\E9W\12\97\A946\DEB|\D2p\ECi\DF\E8\88\DB}\BF\10\B6I\93", [32 x i8] c"\BA\FC~C\D2e\A1s\02\1A\9D\9EX=`\EDB\A8\03\FA\CDk\83`\DE\1F\91h58\9B\F0", [32 x i8] c"\A5\B6{\E9P\FB\C2\F0\DD2:y\A1\9E>\D1\F4\AEK\A7\89O\93\0E\A5\EFsM\E7\DB\83\AE", [32 x i8] c"\BF\1Ee\F3\CD\84\98\88M\9D\\\19\EB\F7\B9\16\06v7`N&\DB\E2\B7(\8E\CB\11B`h", [32 x i8] c"\C34,\F9\CB\BF)\D4\06\D7\89]\D4\D9T\8DJ\C7\8BM\00\E9\B6> >^\19\E9\97F ", [32 x i8] c"\1C\0B\E6\02wCK\0E\00K{8\8A7U\9F\84\B3\0Cl\F8`\0FR\8B\FC\D3<\AFR\CB\1E", [32 x i8] c"s\95E0\D0?\10\BE\F5*\D5\BC\7F\B4\C0v\F8?c1\C8\BD\1E\EE\C3\88\7FJ\A2\06\92@", [32 x i8] c"i\C1\1E\E0ID\DE\A9\85\AC\9F\13\96\0Es\98\0E\1B\B0\E3\09\F48J\16v\F8\EF\AB8B\88", [32 x i8] c"6\FB\8F\DE\0E\C2\8C\E8S\FBqu\C1\B7\9D\A3\B5\E8\C3\91\86\E7\8A\AE\CETd\DB\D9\FE*\A2", [32 x i8] c"k\B2\A0\9D\FC\AF\96\96-\E0\0C\8A\08-m\F92+If\AE\8D.\CFs$\11\A7j\1A\0E\E6", [32 x i8] c"t\12\E7\DD\1B\F1\AA\93\97A\1B\BAM>\02v\D2\E7\A1\A2\9A$w\15z\D6\03`\D3=Nv", [32 x i8] c"\DD\DE\AF\CF\C7#!\C8I\FB%\94z\B4,\1A\F2\A5\E4?\EFh\1B\E4,~\AF6`\08\0A\D3", [32 x i8] c"\9D\EF\EB\AD\BD\CB\0A\0E\7F\F9\92\F9G\CE\D3\D0\A4\C8\99\E6O\E7s`\E8\1E\1F\0E\97\F8\C1\A2", [32 x i8] c"\84LY\FB\E6Go\D1\89#\99T\F1~6\E1\F6\9E$\AA\ED]\\\8B\84\05\EF*\83\0C\C2\A0", [32 x i8] c"\FF?\AF\B6w\86\E0\1A\0C8\EA\DF\99\C4\CA\E8\02\9D\A8\CF)\87_\C4\19\BFh\00\09\B3\BD\B3", [32 x i8] c"\CAg`\F3Eg\8F0\A2\8Db\82\94'*\19\E3\07.\BCa\B1\9F\F1;1\89s\E9|'8", [32 x i8] c"\C0\8E\1A\90G\C5\05&J\16D|\9E\D9\81\A7\19\D3\81\F2\8E`_\D7\CA\A9\E8\BD\BBB\99j", [32 x i8] c"\F1s\BA\9DE\84\CD\12`P\C6\9F\C2\19\A9\19\0A\0B\F0\AE\CE\CB\E6\11\BE\ED\19=\A6\CAM\E7", [32 x i8] c"\B1\84\87e \DE\D8\BD}\E2^\AE\FB\D3\E06\88\C3\BE9\C1\9F\B7>\1F\0E\CC\AC|\C0\F0\14", [32 x i8] c"\90%\DB\07X\BD\FBH\F0f~\BD~\12\02FY\8F\ED\01\C2XvO\A0\FA\E34\A2\A0\0A\97", [32 x i8] c"\E8=\80\86\FA\BCF\0D^\FCE\9F\95\A2h\F5\DCJ\C2\84\09<$|\A6\EC\84\1A\D6\18?\E1", [32 x i8] c"\CC\9D\F4\1D5\AAu\92\8C\18_s\93fa\10\B8\0F\09\86\A2!\C3p\F4\\.\B9\01l\9A;", [32 x i8] c"\92\F9\A5\94\95E\90\FA\81\98\17\E5\D1\C2\8A\AB+\1C\C5\04\D8m\BAD6v\BD\F8fyh\11", [32 x i8] c"r\95b\A1\E0{\0E&\05IH\09\BDH\0F\157\CE\A1\0D\CA\D4>\F9\F6\8Cf\E8%\DCF\B1", [32 x i8] c"&\F1`\AB\96\F5X E\14n\AF\F2\E2\A8\D4\DA\B2\98\B4\C5~\11|\DF\C5\D0%\C9*\22h", [32 x i8] c"\87\EB\E7!88s\D2G\F8a\82\E3\F5\99\A7cO\CA\EC^\07\B1\E8>\BByb[\A3T\E6", [32 x i8] c"\E0\8D8\9FuiJ\DC\99l\22\F5]O\85\9F\FD\0C\13\19\FF\9C\ED\F7\8C1\BE\84\B6\F2\1A\BC", [32 x i8] c"\13c\E2)\13\C6\E1\8Ez\A6[\83\E7Q\C8\A2\C6\1B\0F0qU\86ZW\DB\A5i\A9\9C{\0E", [32 x i8] c"\88x\08\8E\B2\D1\F6\D0\BBH\1BK\B1\87\DA\04\BC\D8\C2\C69\F0\05\B0\80T\CCAu9\05\FB", [32 x i8] c"\04\18\D6\0D\05\B4\E1$dn\E5\0EwI\A1\D2\09E{\C5C\E3\CC\110'J\EA\0F{\F3\C1", [32 x i8] c"z9~P?);\C4-_~\F5\EC7\87$`\A4\F5\B5\CC\DEw\FBMG\AC\06\81\E5\A0I", [32 x i8] c"\\\0D)\83\E7*m\D4\E6R\D7#\C1\DF\C1+AL\87=J\B4\A0\A1P@\8E\B3CG\E9\95", [32 x i8] c"V#6TS\C0I\89\C7\CF3c^\0F\C4\CD\DDho\C9Z3\DF\ED\CF35yL}\C3D", [32 x i8] c"\11\F6\DA\D1\88\02\8F\DF\13x\A2V\E4W\0E\90c\10{\8Fy\DCf?\A5UoV\FDD\A0\F0", [32 x i8] c"\0E\D8\16\17\97\EC\EE\88\1E}\0E?L_\B89\C8N\B7\A9$&W\CCH0h\07\B3+\EF\DE", [32 x i8] c"sfg\C96L\E1-\B8\F6\B1C\C6\C1x\CD\EF\1E\14E\BCZ/&4\F0\8E\992'<\AA", [32 x i8] c"\E1_6\8BD\06\C1\F6UW\C85\\\BEiKc>&\F1U\F5+}\A9L\FB#\FDJ]\96", [32 x i8] c"Cz\B2\D7OP\CA\86\CC=\E9\BEp\E4UH%\E3=\82K:I#b\E2\E9\D6\11\BCW\9D", [32 x i8] c"+\91X\C7\22\89\8ERm,\DD?\C0\88\E9\FF\A7\9A\9Bs\B7\D2\D2K\C4x\E2\1C\DB;gc", [32 x i8] c"\0C\8A6Y}ta\C6:\94s(!\C9A\85lf\83v`l\86\A5-\E0\EEA\04\C6\15\DB"], align 16
@.str = private unnamed_addr constant [3 x i8] c"ok\00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"error\00", align 1
@secure_zero_memory.memset_v = internal constant i8* (i8*, i32, i32)* @memset, align 4

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2sp_init(%struct.blake2sp_state__* %S, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2sp_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.blake2sp_state__* %S, %struct.blake2sp_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ugt i32 %1, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %2, i32 0, i32 2
  %arraydecay = getelementptr inbounds [512 x i8], [512 x i8]* %buf, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 4 %arraydecay, i8 0, i32 512, i1 false)
  %3 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %3, i32 0, i32 3
  store i32 0, i32* %buflen, align 4
  %4 = load i32, i32* %outlen.addr, align 4
  %5 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %outlen1 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %5, i32 0, i32 4
  store i32 %4, i32* %outlen1, align 4
  %6 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %R = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %6, i32 0, i32 1
  %arraydecay2 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %R, i32 0, i32 0
  %7 = load i32, i32* %outlen.addr, align 4
  %call = call i32 @blake2sp_init_root(%struct.blake2s_state__* %arraydecay2, i32 %7, i32 0)
  %cmp3 = icmp slt i32 %call, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end5
  %8 = load i32, i32* %i, align 4
  %cmp6 = icmp ult i32 %8, 8
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S7 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %9, i32 0, i32 0
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S7, i32 0, i32 %10
  %arraydecay8 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx, i32 0, i32 0
  %11 = load i32, i32* %outlen.addr, align 4
  %12 = load i32, i32* %i, align 4
  %conv = zext i32 %12 to i64
  %call9 = call i32 @blake2sp_init_leaf(%struct.blake2s_state__* %arraydecay8, i32 %11, i32 0, i64 %conv)
  %cmp10 = icmp slt i32 %call9, 0
  br i1 %cmp10, label %if.then12, label %if.end13

if.then12:                                        ; preds = %for.body
  store i32 -1, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %13 = load i32, i32* %i, align 4
  %inc = add i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %R14 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %14, i32 0, i32 1
  %arraydecay15 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %R14, i32 0, i32 0
  %last_node = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %arraydecay15, i32 0, i32 6
  store i8 1, i8* %last_node, align 4
  %15 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S16 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %15, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S16, i32 0, i32 7
  %arraydecay18 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx17, i32 0, i32 0
  %last_node19 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %arraydecay18, i32 0, i32 6
  store i8 1, i8* %last_node19, align 4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then12, %if.then4, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2sp_init_root(%struct.blake2s_state__* %S, i32 %outlen, i32 %keylen) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %keylen.addr = alloca i32, align 4
  %P = alloca [1 x %struct.blake2s_param__], align 16
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %0 to i8
  %arraydecay = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 16
  %1 = load i32, i32* %keylen.addr, align 4
  %conv1 = trunc i32 %1 to i8
  %arraydecay2 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay2, i32 0, i32 1
  store i8 %conv1, i8* %key_length, align 1
  %arraydecay3 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay3, i32 0, i32 2
  store i8 8, i8* %fanout, align 2
  %arraydecay4 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay4, i32 0, i32 3
  store i8 2, i8* %depth, align 1
  %arraydecay5 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay5, i32 0, i32 4
  %2 = bitcast i32* %leaf_length to i8*
  call void @store32(i8* %2, i32 0)
  %arraydecay6 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay6, i32 0, i32 5
  %3 = bitcast i32* %node_offset to i8*
  call void @store32(i8* %3, i32 0)
  %arraydecay7 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %xof_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay7, i32 0, i32 6
  %4 = bitcast i16* %xof_length to i8*
  call void @store16(i8* %4, i16 zeroext 0)
  %arraydecay8 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay8, i32 0, i32 7
  store i8 1, i8* %node_depth, align 2
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay9, i32 0, i32 8
  store i8 32, i8* %inner_length, align 1
  %arraydecay10 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay10, i32 0, i32 9
  %arraydecay11 = getelementptr inbounds [8 x i8], [8 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay11, i8 0, i32 8, i1 false)
  %arraydecay12 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay12, i32 0, i32 10
  %arraydecay13 = getelementptr inbounds [8 x i8], [8 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 8 %arraydecay13, i8 0, i32 8, i1 false)
  %5 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %arraydecay14 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %call = call i32 @blake2s_init_param(%struct.blake2s_state__* %5, %struct.blake2s_param__* %arraydecay14)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2sp_init_leaf(%struct.blake2s_state__* %S, i32 %outlen, i32 %keylen, i64 %offset) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %keylen.addr = alloca i32, align 4
  %offset.addr = alloca i64, align 8
  %P = alloca [1 x %struct.blake2s_param__], align 16
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  store i64 %offset, i64* %offset.addr, align 8
  %0 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %0 to i8
  %arraydecay = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 16
  %1 = load i32, i32* %keylen.addr, align 4
  %conv1 = trunc i32 %1 to i8
  %arraydecay2 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay2, i32 0, i32 1
  store i8 %conv1, i8* %key_length, align 1
  %arraydecay3 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay3, i32 0, i32 2
  store i8 8, i8* %fanout, align 2
  %arraydecay4 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay4, i32 0, i32 3
  store i8 2, i8* %depth, align 1
  %arraydecay5 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay5, i32 0, i32 4
  %2 = bitcast i32* %leaf_length to i8*
  call void @store32(i8* %2, i32 0)
  %arraydecay6 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay6, i32 0, i32 5
  %3 = bitcast i32* %node_offset to i8*
  %4 = load i64, i64* %offset.addr, align 8
  %conv7 = trunc i64 %4 to i32
  call void @store32(i8* %3, i32 %conv7)
  %arraydecay8 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %xof_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay8, i32 0, i32 6
  %5 = bitcast i16* %xof_length to i8*
  call void @store16(i8* %5, i16 zeroext 0)
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay9, i32 0, i32 7
  store i8 0, i8* %node_depth, align 2
  %arraydecay10 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay10, i32 0, i32 8
  store i8 32, i8* %inner_length, align 1
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay11, i32 0, i32 9
  %arraydecay12 = getelementptr inbounds [8 x i8], [8 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay12, i8 0, i32 8, i1 false)
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay13, i32 0, i32 10
  %arraydecay14 = getelementptr inbounds [8 x i8], [8 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 8 %arraydecay14, i8 0, i32 8, i1 false)
  %6 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %arraydecay15 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %call = call i32 @blake2sp_init_leaf_param(%struct.blake2s_state__* %6, %struct.blake2s_param__* %arraydecay15)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2sp_init_key(%struct.blake2sp_state__* %S, i32 %outlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2sp_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %block = alloca [64 x i8], align 16
  store %struct.blake2sp_state__* %S, %struct.blake2sp_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ugt i32 %1, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8*, i8** %key.addr, align 4
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then6

lor.lhs.false2:                                   ; preds = %if.end
  %3 = load i32, i32* %keylen.addr, align 4
  %tobool3 = icmp ne i32 %3, 0
  br i1 %tobool3, label %lor.lhs.false4, label %if.then6

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %4 = load i32, i32* %keylen.addr, align 4
  %cmp5 = icmp ugt i32 %4, 32
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %lor.lhs.false4, %lor.lhs.false2, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %lor.lhs.false4
  %5 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %5, i32 0, i32 2
  %arraydecay = getelementptr inbounds [512 x i8], [512 x i8]* %buf, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 4 %arraydecay, i8 0, i32 512, i1 false)
  %6 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %6, i32 0, i32 3
  store i32 0, i32* %buflen, align 4
  %7 = load i32, i32* %outlen.addr, align 4
  %8 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %outlen8 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %8, i32 0, i32 4
  store i32 %7, i32* %outlen8, align 4
  %9 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %R = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %9, i32 0, i32 1
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %R, i32 0, i32 0
  %10 = load i32, i32* %outlen.addr, align 4
  %11 = load i32, i32* %keylen.addr, align 4
  %call = call i32 @blake2sp_init_root(%struct.blake2s_state__* %arraydecay9, i32 %10, i32 %11)
  %cmp10 = icmp slt i32 %call, 0
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end7
  store i32 -1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end7
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end12
  %12 = load i32, i32* %i, align 4
  %cmp13 = icmp ult i32 %12, 8
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S14 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %13, i32 0, i32 0
  %14 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S14, i32 0, i32 %14
  %arraydecay15 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx, i32 0, i32 0
  %15 = load i32, i32* %outlen.addr, align 4
  %16 = load i32, i32* %keylen.addr, align 4
  %17 = load i32, i32* %i, align 4
  %conv = zext i32 %17 to i64
  %call16 = call i32 @blake2sp_init_leaf(%struct.blake2s_state__* %arraydecay15, i32 %15, i32 %16, i64 %conv)
  %cmp17 = icmp slt i32 %call16, 0
  br i1 %cmp17, label %if.then19, label %if.end20

if.then19:                                        ; preds = %for.body
  store i32 -1, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end20
  %18 = load i32, i32* %i, align 4
  %inc = add i32 %18, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %R21 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %19, i32 0, i32 1
  %arraydecay22 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %R21, i32 0, i32 0
  %last_node = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %arraydecay22, i32 0, i32 6
  store i8 1, i8* %last_node, align 4
  %20 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S23 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %20, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S23, i32 0, i32 7
  %arraydecay25 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx24, i32 0, i32 0
  %last_node26 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %arraydecay25, i32 0, i32 6
  store i8 1, i8* %last_node26, align 4
  %arraydecay27 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay27, i8 0, i32 64, i1 false)
  %arraydecay28 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %21 = load i8*, i8** %key.addr, align 4
  %22 = load i32, i32* %keylen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay28, i8* align 1 %21, i32 %22, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc38, %for.end
  %23 = load i32, i32* %i, align 4
  %cmp30 = icmp ult i32 %23, 8
  br i1 %cmp30, label %for.body32, label %for.end40

for.body32:                                       ; preds = %for.cond29
  %24 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S33 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %24, i32 0, i32 0
  %25 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S33, i32 0, i32 %25
  %arraydecay35 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx34, i32 0, i32 0
  %arraydecay36 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %call37 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay35, i8* %arraydecay36, i32 64)
  br label %for.inc38

for.inc38:                                        ; preds = %for.body32
  %26 = load i32, i32* %i, align 4
  %inc39 = add i32 %26, 1
  store i32 %inc39, i32* %i, align 4
  br label %for.cond29

for.end40:                                        ; preds = %for.cond29
  %arraydecay41 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @secure_zero_memory(i8* %arraydecay41, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end40, %if.then19, %if.then11, %if.then6, %if.then
  %27 = load i32, i32* %retval, align 4
  ret i32 %27
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare i32 @blake2s_update(%struct.blake2s_state__*, i8*, i32) #3

; Function Attrs: noinline nounwind optnone
define internal void @secure_zero_memory(i8* %v, i32 %n) #0 {
entry:
  %v.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  store i8* %v, i8** %v.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load volatile i8* (i8*, i32, i32)*, i8* (i8*, i32, i32)** @secure_zero_memory.memset_v, align 4
  %1 = load i8*, i8** %v.addr, align 4
  %2 = load i32, i32* %n.addr, align 4
  %call = call i8* %0(i8* %1, i32 0, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2sp_update(%struct.blake2sp_state__* %S, i8* %pin, i32 %inlen) #0 {
entry:
  %S.addr = alloca %struct.blake2sp_state__*, align 4
  %pin.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %in = alloca i8*, align 4
  %left = alloca i32, align 4
  %fill = alloca i32, align 4
  %i = alloca i32, align 4
  %inlen__ = alloca i32, align 4
  %in__ = alloca i8*, align 4
  store %struct.blake2sp_state__* %S, %struct.blake2sp_state__** %S.addr, align 4
  store i8* %pin, i8** %pin.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  %0 = load i8*, i8** %pin.addr, align 4
  store i8* %0, i8** %in, align 4
  %1 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %1, i32 0, i32 3
  %2 = load i32, i32* %buflen, align 4
  store i32 %2, i32* %left, align 4
  %3 = load i32, i32* %left, align 4
  %sub = sub i32 512, %3
  store i32 %sub, i32* %fill, align 4
  %4 = load i32, i32* %left, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %5 = load i32, i32* %inlen.addr, align 4
  %6 = load i32, i32* %fill, align 4
  %cmp = icmp uge i32 %5, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %7 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %7, i32 0, i32 2
  %arraydecay = getelementptr inbounds [512 x i8], [512 x i8]* %buf, i32 0, i32 0
  %8 = load i32, i32* %left, align 4
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 %8
  %9 = load i8*, i8** %in, align 4
  %10 = load i32, i32* %fill, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %9, i32 %10, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4
  %cmp1 = icmp ult i32 %11, 8
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S2 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %12, i32 0, i32 0
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S2, i32 0, i32 %13
  %arraydecay3 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx, i32 0, i32 0
  %14 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buf4 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %14, i32 0, i32 2
  %arraydecay5 = getelementptr inbounds [512 x i8], [512 x i8]* %buf4, i32 0, i32 0
  %15 = load i32, i32* %i, align 4
  %mul = mul i32 %15, 64
  %add.ptr6 = getelementptr inbounds i8, i8* %arraydecay5, i32 %mul
  %call = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay3, i8* %add.ptr6, i32 64)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4
  %inc = add i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %17 = load i32, i32* %fill, align 4
  %18 = load i8*, i8** %in, align 4
  %add.ptr7 = getelementptr inbounds i8, i8* %18, i32 %17
  store i8* %add.ptr7, i8** %in, align 4
  %19 = load i32, i32* %fill, align 4
  %20 = load i32, i32* %inlen.addr, align 4
  %sub8 = sub i32 %20, %19
  store i32 %sub8, i32* %inlen.addr, align 4
  store i32 0, i32* %left, align 4
  br label %if.end

if.end:                                           ; preds = %for.end, %land.lhs.true, %entry
  store i32 0, i32* %i, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc21, %if.end
  %21 = load i32, i32* %i, align 4
  %cmp10 = icmp ult i32 %21, 8
  br i1 %cmp10, label %for.body11, label %for.end23

for.body11:                                       ; preds = %for.cond9
  %22 = load i32, i32* %inlen.addr, align 4
  store i32 %22, i32* %inlen__, align 4
  %23 = load i8*, i8** %in, align 4
  store i8* %23, i8** %in__, align 4
  %24 = load i32, i32* %i, align 4
  %mul12 = mul i32 %24, 64
  %25 = load i8*, i8** %in__, align 4
  %add.ptr13 = getelementptr inbounds i8, i8* %25, i32 %mul12
  store i8* %add.ptr13, i8** %in__, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body11
  %26 = load i32, i32* %inlen__, align 4
  %cmp14 = icmp uge i32 %26, 512
  br i1 %cmp14, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %27 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S15 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %27, i32 0, i32 0
  %28 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S15, i32 0, i32 %28
  %arraydecay17 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx16, i32 0, i32 0
  %29 = load i8*, i8** %in__, align 4
  %call18 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay17, i8* %29, i32 64)
  %30 = load i8*, i8** %in__, align 4
  %add.ptr19 = getelementptr inbounds i8, i8* %30, i32 512
  store i8* %add.ptr19, i8** %in__, align 4
  %31 = load i32, i32* %inlen__, align 4
  %sub20 = sub i32 %31, 512
  store i32 %sub20, i32* %inlen__, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.inc21

for.inc21:                                        ; preds = %while.end
  %32 = load i32, i32* %i, align 4
  %inc22 = add i32 %32, 1
  store i32 %inc22, i32* %i, align 4
  br label %for.cond9

for.end23:                                        ; preds = %for.cond9
  %33 = load i32, i32* %inlen.addr, align 4
  %34 = load i32, i32* %inlen.addr, align 4
  %rem = urem i32 %34, 512
  %sub24 = sub i32 %33, %rem
  %35 = load i8*, i8** %in, align 4
  %add.ptr25 = getelementptr inbounds i8, i8* %35, i32 %sub24
  store i8* %add.ptr25, i8** %in, align 4
  %36 = load i32, i32* %inlen.addr, align 4
  %rem26 = urem i32 %36, 512
  store i32 %rem26, i32* %inlen.addr, align 4
  %37 = load i32, i32* %inlen.addr, align 4
  %cmp27 = icmp ugt i32 %37, 0
  br i1 %cmp27, label %if.then28, label %if.end32

if.then28:                                        ; preds = %for.end23
  %38 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buf29 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %38, i32 0, i32 2
  %arraydecay30 = getelementptr inbounds [512 x i8], [512 x i8]* %buf29, i32 0, i32 0
  %39 = load i32, i32* %left, align 4
  %add.ptr31 = getelementptr inbounds i8, i8* %arraydecay30, i32 %39
  %40 = load i8*, i8** %in, align 4
  %41 = load i32, i32* %inlen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr31, i8* align 1 %40, i32 %41, i1 false)
  br label %if.end32

if.end32:                                         ; preds = %if.then28, %for.end23
  %42 = load i32, i32* %left, align 4
  %43 = load i32, i32* %inlen.addr, align 4
  %add = add i32 %42, %43
  %44 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buflen33 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %44, i32 0, i32 3
  store i32 %add, i32* %buflen33, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2sp_final(%struct.blake2sp_state__* %S, i8* %out, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2sp_state__*, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %hash = alloca [8 x [32 x i8]], align 16
  %i = alloca i32, align 4
  %left = alloca i32, align 4
  store %struct.blake2sp_state__* %S, %struct.blake2sp_state__** %S.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %2 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %outlen1 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %2, i32 0, i32 4
  %3 = load i32, i32* %outlen1, align 4
  %cmp2 = icmp ult i32 %1, %3
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %4, 8
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %5, i32 0, i32 3
  %6 = load i32, i32* %buflen, align 4
  %7 = load i32, i32* %i, align 4
  %mul = mul i32 %7, 64
  %cmp4 = icmp ugt i32 %6, %mul
  br i1 %cmp4, label %if.then5, label %if.end14

if.then5:                                         ; preds = %for.body
  %8 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buflen6 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %8, i32 0, i32 3
  %9 = load i32, i32* %buflen6, align 4
  %10 = load i32, i32* %i, align 4
  %mul7 = mul i32 %10, 64
  %sub = sub i32 %9, %mul7
  store i32 %sub, i32* %left, align 4
  %11 = load i32, i32* %left, align 4
  %cmp8 = icmp ugt i32 %11, 64
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.then5
  store i32 64, i32* %left, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %if.then5
  %12 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S11 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %12, i32 0, i32 0
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S11, i32 0, i32 %13
  %arraydecay = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx, i32 0, i32 0
  %14 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %14, i32 0, i32 2
  %arraydecay12 = getelementptr inbounds [512 x i8], [512 x i8]* %buf, i32 0, i32 0
  %15 = load i32, i32* %i, align 4
  %mul13 = mul i32 %15, 64
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay12, i32 %mul13
  %16 = load i32, i32* %left, align 4
  %call = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay, i8* %add.ptr, i32 %16)
  br label %if.end14

if.end14:                                         ; preds = %if.end10, %for.body
  %17 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %S15 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %17, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S15, i32 0, i32 %18
  %arraydecay17 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx16, i32 0, i32 0
  %19 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [8 x [32 x i8]], [8 x [32 x i8]]* %hash, i32 0, i32 %19
  %arraydecay19 = getelementptr inbounds [32 x i8], [32 x i8]* %arrayidx18, i32 0, i32 0
  %call20 = call i32 @blake2s_final(%struct.blake2s_state__* %arraydecay17, i8* %arraydecay19, i32 32)
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %20 = load i32, i32* %i, align 4
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc28, %for.end
  %21 = load i32, i32* %i, align 4
  %cmp22 = icmp ult i32 %21, 8
  br i1 %cmp22, label %for.body23, label %for.end30

for.body23:                                       ; preds = %for.cond21
  %22 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %R = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %22, i32 0, i32 1
  %arraydecay24 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %R, i32 0, i32 0
  %23 = load i32, i32* %i, align 4
  %arrayidx25 = getelementptr inbounds [8 x [32 x i8]], [8 x [32 x i8]]* %hash, i32 0, i32 %23
  %arraydecay26 = getelementptr inbounds [32 x i8], [32 x i8]* %arrayidx25, i32 0, i32 0
  %call27 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay24, i8* %arraydecay26, i32 32)
  br label %for.inc28

for.inc28:                                        ; preds = %for.body23
  %24 = load i32, i32* %i, align 4
  %inc29 = add i32 %24, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond21

for.end30:                                        ; preds = %for.cond21
  %25 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %R31 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %25, i32 0, i32 1
  %arraydecay32 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %R31, i32 0, i32 0
  %26 = load i8*, i8** %out.addr, align 4
  %27 = load %struct.blake2sp_state__*, %struct.blake2sp_state__** %S.addr, align 4
  %outlen33 = getelementptr inbounds %struct.blake2sp_state__, %struct.blake2sp_state__* %27, i32 0, i32 4
  %28 = load i32, i32* %outlen33, align 4
  %call34 = call i32 @blake2s_final(%struct.blake2s_state__* %arraydecay32, i8* %26, i32 %28)
  store i32 %call34, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end30, %if.then
  %29 = load i32, i32* %retval, align 4
  ret i32 %29
}

declare i32 @blake2s_final(%struct.blake2s_state__*, i8*, i32) #3

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2sp(i8* %out, i32 %outlen, i8* %in, i32 %inlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %hash = alloca [8 x [32 x i8]], align 16
  %S = alloca [8 x [1 x %struct.blake2s_state__]], align 16
  %FS = alloca [1 x %struct.blake2s_state__], align 16
  %i = alloca i32, align 4
  %block = alloca [64 x i8], align 16
  %inlen__ = alloca i32, align 4
  %in__ = alloca i8*, align 4
  %left = alloca i32, align 4
  %len = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i8*, i8** %in.addr, align 4
  %cmp = icmp eq i8* null, %0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %inlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i8*, i8** %out.addr, align 4
  %cmp2 = icmp eq i8* null, %2
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %3 = load i8*, i8** %key.addr, align 4
  %cmp5 = icmp eq i8* null, %3
  br i1 %cmp5, label %land.lhs.true6, label %if.end9

land.lhs.true6:                                   ; preds = %if.end4
  %4 = load i32, i32* %keylen.addr, align 4
  %cmp7 = icmp ugt i32 %4, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %land.lhs.true6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %land.lhs.true6, %if.end4
  %5 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then11

lor.lhs.false:                                    ; preds = %if.end9
  %6 = load i32, i32* %outlen.addr, align 4
  %cmp10 = icmp ugt i32 %6, 32
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %lor.lhs.false, %if.end9
  store i32 -1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %lor.lhs.false
  %7 = load i32, i32* %keylen.addr, align 4
  %cmp13 = icmp ugt i32 %7, 32
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end12
  store i32 -1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end12
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end15
  %8 = load i32, i32* %i, align 4
  %cmp16 = icmp ult i32 %8, 8
  br i1 %cmp16, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S, i32 0, i32 %9
  %arraydecay = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx, i32 0, i32 0
  %10 = load i32, i32* %outlen.addr, align 4
  %11 = load i32, i32* %keylen.addr, align 4
  %12 = load i32, i32* %i, align 4
  %conv = zext i32 %12 to i64
  %call = call i32 @blake2sp_init_leaf(%struct.blake2s_state__* %arraydecay, i32 %10, i32 %11, i64 %conv)
  %cmp17 = icmp slt i32 %call, 0
  br i1 %cmp17, label %if.then19, label %if.end20

if.then19:                                        ; preds = %for.body
  store i32 -1, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end20
  %13 = load i32, i32* %i, align 4
  %inc = add i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arrayidx21 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S, i32 0, i32 7
  %arraydecay22 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx21, i32 0, i32 0
  %last_node = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %arraydecay22, i32 0, i32 6
  store i8 1, i8* %last_node, align 4
  %14 = load i32, i32* %keylen.addr, align 4
  %cmp23 = icmp ugt i32 %14, 0
  br i1 %cmp23, label %if.then25, label %if.end40

if.then25:                                        ; preds = %for.end
  %arraydecay26 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay26, i8 0, i32 64, i1 false)
  %arraydecay27 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %15 = load i8*, i8** %key.addr, align 4
  %16 = load i32, i32* %keylen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay27, i8* align 1 %15, i32 %16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc36, %if.then25
  %17 = load i32, i32* %i, align 4
  %cmp29 = icmp ult i32 %17, 8
  br i1 %cmp29, label %for.body31, label %for.end38

for.body31:                                       ; preds = %for.cond28
  %18 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S, i32 0, i32 %18
  %arraydecay33 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx32, i32 0, i32 0
  %arraydecay34 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %call35 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay33, i8* %arraydecay34, i32 64)
  br label %for.inc36

for.inc36:                                        ; preds = %for.body31
  %19 = load i32, i32* %i, align 4
  %inc37 = add i32 %19, 1
  store i32 %inc37, i32* %i, align 4
  br label %for.cond28

for.end38:                                        ; preds = %for.cond28
  %arraydecay39 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @secure_zero_memory(i8* %arraydecay39, i32 64)
  br label %if.end40

if.end40:                                         ; preds = %for.end38, %for.end
  store i32 0, i32* %i, align 4
  br label %for.cond41

for.cond41:                                       ; preds = %for.inc68, %if.end40
  %20 = load i32, i32* %i, align 4
  %cmp42 = icmp ult i32 %20, 8
  br i1 %cmp42, label %for.body44, label %for.end70

for.body44:                                       ; preds = %for.cond41
  %21 = load i32, i32* %inlen.addr, align 4
  store i32 %21, i32* %inlen__, align 4
  %22 = load i8*, i8** %in.addr, align 4
  store i8* %22, i8** %in__, align 4
  %23 = load i32, i32* %i, align 4
  %mul = mul i32 %23, 64
  %24 = load i8*, i8** %in__, align 4
  %add.ptr = getelementptr inbounds i8, i8* %24, i32 %mul
  store i8* %add.ptr, i8** %in__, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body44
  %25 = load i32, i32* %inlen__, align 4
  %cmp45 = icmp uge i32 %25, 512
  br i1 %cmp45, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %26 = load i32, i32* %i, align 4
  %arrayidx47 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S, i32 0, i32 %26
  %arraydecay48 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx47, i32 0, i32 0
  %27 = load i8*, i8** %in__, align 4
  %call49 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay48, i8* %27, i32 64)
  %28 = load i8*, i8** %in__, align 4
  %add.ptr50 = getelementptr inbounds i8, i8* %28, i32 512
  store i8* %add.ptr50, i8** %in__, align 4
  %29 = load i32, i32* %inlen__, align 4
  %sub = sub i32 %29, 512
  store i32 %sub, i32* %inlen__, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %30 = load i32, i32* %inlen__, align 4
  %31 = load i32, i32* %i, align 4
  %mul51 = mul i32 %31, 64
  %cmp52 = icmp ugt i32 %30, %mul51
  br i1 %cmp52, label %if.then54, label %if.end62

if.then54:                                        ; preds = %while.end
  %32 = load i32, i32* %inlen__, align 4
  %33 = load i32, i32* %i, align 4
  %mul55 = mul i32 %33, 64
  %sub56 = sub i32 %32, %mul55
  store i32 %sub56, i32* %left, align 4
  %34 = load i32, i32* %left, align 4
  %cmp57 = icmp ule i32 %34, 64
  br i1 %cmp57, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then54
  %35 = load i32, i32* %left, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then54
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %35, %cond.true ], [ 64, %cond.false ]
  store i32 %cond, i32* %len, align 4
  %36 = load i32, i32* %i, align 4
  %arrayidx59 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S, i32 0, i32 %36
  %arraydecay60 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx59, i32 0, i32 0
  %37 = load i8*, i8** %in__, align 4
  %38 = load i32, i32* %len, align 4
  %call61 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay60, i8* %37, i32 %38)
  br label %if.end62

if.end62:                                         ; preds = %cond.end, %while.end
  %39 = load i32, i32* %i, align 4
  %arrayidx63 = getelementptr inbounds [8 x [1 x %struct.blake2s_state__]], [8 x [1 x %struct.blake2s_state__]]* %S, i32 0, i32 %39
  %arraydecay64 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %arrayidx63, i32 0, i32 0
  %40 = load i32, i32* %i, align 4
  %arrayidx65 = getelementptr inbounds [8 x [32 x i8]], [8 x [32 x i8]]* %hash, i32 0, i32 %40
  %arraydecay66 = getelementptr inbounds [32 x i8], [32 x i8]* %arrayidx65, i32 0, i32 0
  %call67 = call i32 @blake2s_final(%struct.blake2s_state__* %arraydecay64, i8* %arraydecay66, i32 32)
  br label %for.inc68

for.inc68:                                        ; preds = %if.end62
  %41 = load i32, i32* %i, align 4
  %inc69 = add i32 %41, 1
  store i32 %inc69, i32* %i, align 4
  br label %for.cond41

for.end70:                                        ; preds = %for.cond41
  %arraydecay71 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %FS, i32 0, i32 0
  %42 = load i32, i32* %outlen.addr, align 4
  %43 = load i32, i32* %keylen.addr, align 4
  %call72 = call i32 @blake2sp_init_root(%struct.blake2s_state__* %arraydecay71, i32 %42, i32 %43)
  %cmp73 = icmp slt i32 %call72, 0
  br i1 %cmp73, label %if.then75, label %if.end76

if.then75:                                        ; preds = %for.end70
  store i32 -1, i32* %retval, align 4
  br label %return

if.end76:                                         ; preds = %for.end70
  %arraydecay77 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %FS, i32 0, i32 0
  %last_node78 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %arraydecay77, i32 0, i32 6
  store i8 1, i8* %last_node78, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc87, %if.end76
  %44 = load i32, i32* %i, align 4
  %cmp80 = icmp ult i32 %44, 8
  br i1 %cmp80, label %for.body82, label %for.end89

for.body82:                                       ; preds = %for.cond79
  %arraydecay83 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %FS, i32 0, i32 0
  %45 = load i32, i32* %i, align 4
  %arrayidx84 = getelementptr inbounds [8 x [32 x i8]], [8 x [32 x i8]]* %hash, i32 0, i32 %45
  %arraydecay85 = getelementptr inbounds [32 x i8], [32 x i8]* %arrayidx84, i32 0, i32 0
  %call86 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay83, i8* %arraydecay85, i32 32)
  br label %for.inc87

for.inc87:                                        ; preds = %for.body82
  %46 = load i32, i32* %i, align 4
  %inc88 = add i32 %46, 1
  store i32 %inc88, i32* %i, align 4
  br label %for.cond79

for.end89:                                        ; preds = %for.cond79
  %arraydecay90 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %FS, i32 0, i32 0
  %47 = load i8*, i8** %out.addr, align 4
  %48 = load i32, i32* %outlen.addr, align 4
  %call91 = call i32 @blake2s_final(%struct.blake2s_state__* %arraydecay90, i8* %47, i32 %48)
  store i32 %call91, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end89, %if.then75, %if.then19, %if.then14, %if.then11, %if.then8, %if.then3, %if.then
  %49 = load i32, i32* %retval, align 4
  ret i32 %49
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %key = alloca [32 x i8], align 16
  %buf = alloca [256 x i8], align 16
  %i = alloca i32, align 4
  %step = alloca i32, align 4
  %hash = alloca [32 x i8], align 16
  %hash33 = alloca [32 x i8], align 16
  %S = alloca %struct.blake2sp_state__, align 4
  %p = alloca i8*, align 4
  %mlen = alloca i32, align 4
  %err = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %conv = trunc i32 %1 to i8
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [32 x i8], [32 x i8]* %key, i32 0, i32 %2
  store i8 %conv, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %3 = load i32, i32* %i, align 4
  %inc = add i32 %3, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc7, %for.end
  %4 = load i32, i32* %i, align 4
  %cmp2 = icmp ult i32 %4, 256
  br i1 %cmp2, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond1
  %5 = load i32, i32* %i, align 4
  %conv5 = trunc i32 %5 to i8
  %6 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [256 x i8], [256 x i8]* %buf, i32 0, i32 %6
  store i8 %conv5, i8* %arrayidx6, align 1
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %7 = load i32, i32* %i, align 4
  %inc8 = add i32 %7, 1
  store i32 %inc8, i32* %i, align 4
  br label %for.cond1

for.end9:                                         ; preds = %for.cond1
  store i32 0, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc22, %for.end9
  %8 = load i32, i32* %i, align 4
  %cmp11 = icmp ult i32 %8, 256
  br i1 %cmp11, label %for.body13, label %for.end24

for.body13:                                       ; preds = %for.cond10
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %hash, i32 0, i32 0
  %arraydecay14 = getelementptr inbounds [256 x i8], [256 x i8]* %buf, i32 0, i32 0
  %9 = load i32, i32* %i, align 4
  %arraydecay15 = getelementptr inbounds [32 x i8], [32 x i8]* %key, i32 0, i32 0
  %call = call i32 @blake2sp(i8* %arraydecay, i32 32, i8* %arraydecay14, i32 %9, i8* %arraydecay15, i32 32)
  %arraydecay16 = getelementptr inbounds [32 x i8], [32 x i8]* %hash, i32 0, i32 0
  %10 = load i32, i32* %i, align 4
  %arrayidx17 = getelementptr inbounds [256 x [32 x i8]], [256 x [32 x i8]]* @blake2sp_keyed_kat, i32 0, i32 %10
  %arraydecay18 = getelementptr inbounds [32 x i8], [32 x i8]* %arrayidx17, i32 0, i32 0
  %call19 = call i32 @memcmp(i8* %arraydecay16, i8* %arraydecay18, i32 32)
  %cmp20 = icmp ne i32 0, %call19
  br i1 %cmp20, label %if.then, label %if.end

if.then:                                          ; preds = %for.body13
  br label %fail

if.end:                                           ; preds = %for.body13
  br label %for.inc22

for.inc22:                                        ; preds = %if.end
  %11 = load i32, i32* %i, align 4
  %inc23 = add i32 %11, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond10

for.end24:                                        ; preds = %for.cond10
  store i32 1, i32* %step, align 4
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc70, %for.end24
  %12 = load i32, i32* %step, align 4
  %cmp26 = icmp ult i32 %12, 64
  br i1 %cmp26, label %for.body28, label %for.end72

for.body28:                                       ; preds = %for.cond25
  store i32 0, i32* %i, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc67, %for.body28
  %13 = load i32, i32* %i, align 4
  %cmp30 = icmp ult i32 %13, 256
  br i1 %cmp30, label %for.body32, label %for.end69

for.body32:                                       ; preds = %for.cond29
  %arraydecay34 = getelementptr inbounds [256 x i8], [256 x i8]* %buf, i32 0, i32 0
  store i8* %arraydecay34, i8** %p, align 4
  %14 = load i32, i32* %i, align 4
  store i32 %14, i32* %mlen, align 4
  store i32 0, i32* %err, align 4
  %arraydecay35 = getelementptr inbounds [32 x i8], [32 x i8]* %key, i32 0, i32 0
  %call36 = call i32 @blake2sp_init_key(%struct.blake2sp_state__* %S, i32 32, i8* %arraydecay35, i32 32)
  store i32 %call36, i32* %err, align 4
  %cmp37 = icmp slt i32 %call36, 0
  br i1 %cmp37, label %if.then39, label %if.end40

if.then39:                                        ; preds = %for.body32
  br label %fail

if.end40:                                         ; preds = %for.body32
  br label %while.cond

while.cond:                                       ; preds = %if.end47, %if.end40
  %15 = load i32, i32* %mlen, align 4
  %16 = load i32, i32* %step, align 4
  %cmp41 = icmp uge i32 %15, %16
  br i1 %cmp41, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8*, i8** %p, align 4
  %18 = load i32, i32* %step, align 4
  %call43 = call i32 @blake2sp_update(%struct.blake2sp_state__* %S, i8* %17, i32 %18)
  store i32 %call43, i32* %err, align 4
  %cmp44 = icmp slt i32 %call43, 0
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %while.body
  br label %fail

if.end47:                                         ; preds = %while.body
  %19 = load i32, i32* %step, align 4
  %20 = load i32, i32* %mlen, align 4
  %sub = sub i32 %20, %19
  store i32 %sub, i32* %mlen, align 4
  %21 = load i32, i32* %step, align 4
  %22 = load i8*, i8** %p, align 4
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 %21
  store i8* %add.ptr, i8** %p, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = load i8*, i8** %p, align 4
  %24 = load i32, i32* %mlen, align 4
  %call48 = call i32 @blake2sp_update(%struct.blake2sp_state__* %S, i8* %23, i32 %24)
  store i32 %call48, i32* %err, align 4
  %cmp49 = icmp slt i32 %call48, 0
  br i1 %cmp49, label %if.then51, label %if.end52

if.then51:                                        ; preds = %while.end
  br label %fail

if.end52:                                         ; preds = %while.end
  %arraydecay53 = getelementptr inbounds [32 x i8], [32 x i8]* %hash33, i32 0, i32 0
  %call54 = call i32 @blake2sp_final(%struct.blake2sp_state__* %S, i8* %arraydecay53, i32 32)
  store i32 %call54, i32* %err, align 4
  %cmp55 = icmp slt i32 %call54, 0
  br i1 %cmp55, label %if.then57, label %if.end58

if.then57:                                        ; preds = %if.end52
  br label %fail

if.end58:                                         ; preds = %if.end52
  %arraydecay59 = getelementptr inbounds [32 x i8], [32 x i8]* %hash33, i32 0, i32 0
  %25 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr inbounds [256 x [32 x i8]], [256 x [32 x i8]]* @blake2sp_keyed_kat, i32 0, i32 %25
  %arraydecay61 = getelementptr inbounds [32 x i8], [32 x i8]* %arrayidx60, i32 0, i32 0
  %call62 = call i32 @memcmp(i8* %arraydecay59, i8* %arraydecay61, i32 32)
  %cmp63 = icmp ne i32 0, %call62
  br i1 %cmp63, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end58
  br label %fail

if.end66:                                         ; preds = %if.end58
  br label %for.inc67

for.inc67:                                        ; preds = %if.end66
  %26 = load i32, i32* %i, align 4
  %inc68 = add i32 %26, 1
  store i32 %inc68, i32* %i, align 4
  br label %for.cond29

for.end69:                                        ; preds = %for.cond29
  br label %for.inc70

for.inc70:                                        ; preds = %for.end69
  %27 = load i32, i32* %step, align 4
  %inc71 = add i32 %27, 1
  store i32 %inc71, i32* %step, align 4
  br label %for.cond25

for.end72:                                        ; preds = %for.cond25
  %call73 = call i32 @puts(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  br label %return

fail:                                             ; preds = %if.then65, %if.then57, %if.then51, %if.then46, %if.then39, %if.then
  %call74 = call i32 @puts(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0))
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %fail, %for.end72
  %28 = load i32, i32* %retval, align 4
  ret i32 %28
}

declare i32 @memcmp(i8*, i8*, i32) #3

declare i32 @puts(i8*) #3

; Function Attrs: noinline nounwind optnone
define internal void @store32(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i32, i32* %w.addr, align 4
  %shr = lshr i32 %1, 0
  %conv = trunc i32 %shr to i8
  %2 = load i8*, i8** %p, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 0
  store i8 %conv, i8* %arrayidx, align 1
  %3 = load i32, i32* %w.addr, align 4
  %shr1 = lshr i32 %3, 8
  %conv2 = trunc i32 %shr1 to i8
  %4 = load i8*, i8** %p, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %4, i32 1
  store i8 %conv2, i8* %arrayidx3, align 1
  %5 = load i32, i32* %w.addr, align 4
  %shr4 = lshr i32 %5, 16
  %conv5 = trunc i32 %shr4 to i8
  %6 = load i8*, i8** %p, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %6, i32 2
  store i8 %conv5, i8* %arrayidx6, align 1
  %7 = load i32, i32* %w.addr, align 4
  %shr7 = lshr i32 %7, 24
  %conv8 = trunc i32 %shr7 to i8
  %8 = load i8*, i8** %p, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %8, i32 3
  store i8 %conv8, i8* %arrayidx9, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store16(i8* %dst, i16 zeroext %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i16, align 2
  %p = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i16 %w, i16* %w.addr, align 2
  %0 = load i8*, i8** %dst.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i16, i16* %w.addr, align 2
  %conv = trunc i16 %1 to i8
  %2 = load i8*, i8** %p, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %2, i32 1
  store i8* %incdec.ptr, i8** %p, align 4
  store i8 %conv, i8* %2, align 1
  %3 = load i16, i16* %w.addr, align 2
  %conv1 = zext i16 %3 to i32
  %shr = ashr i32 %conv1, 8
  %conv2 = trunc i32 %shr to i16
  store i16 %conv2, i16* %w.addr, align 2
  %4 = load i16, i16* %w.addr, align 2
  %conv3 = trunc i16 %4 to i8
  %5 = load i8*, i8** %p, align 4
  %incdec.ptr4 = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr4, i8** %p, align 4
  store i8 %conv3, i8* %5, align 1
  ret void
}

declare i32 @blake2s_init_param(%struct.blake2s_state__*, %struct.blake2s_param__*) #3

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2sp_init_leaf_param(%struct.blake2s_state__* %S, %struct.blake2s_param__* %P) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %P.addr = alloca %struct.blake2s_param__*, align 4
  %err = alloca i32, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store %struct.blake2s_param__* %P, %struct.blake2s_param__** %P.addr, align 4
  %0 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %1 = load %struct.blake2s_param__*, %struct.blake2s_param__** %P.addr, align 4
  %call = call i32 @blake2s_init_param(%struct.blake2s_state__* %0, %struct.blake2s_param__* %1)
  store i32 %call, i32* %err, align 4
  %2 = load %struct.blake2s_param__*, %struct.blake2s_param__** %P.addr, align 4
  %inner_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %2, i32 0, i32 8
  %3 = load i8, i8* %inner_length, align 1
  %conv = zext i8 %3 to i32
  %4 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %outlen = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %4, i32 0, i32 5
  store i32 %conv, i32* %outlen, align 4
  %5 = load i32, i32* %err, align 4
  ret i32 %5
}

declare i8* @memset(i8*, i32, i32) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
