; ModuleID = 'blake2s-ref.c'
source_filename = "blake2s-ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.blake2s_state__ = type { [8 x i32], [2 x i32], [2 x i32], [64 x i8], i32, i32, i8 }
%struct.blake2s_param__ = type { i8, i8, i8, i8, i32, i32, i16, i8, i8, [8 x i8], [8 x i8] }

@blake2s_IV = internal constant [8 x i32] [i32 1779033703, i32 -1150833019, i32 1013904242, i32 -1521486534, i32 1359893119, i32 -1694144372, i32 528734635, i32 1541459225], align 16
@secure_zero_memory.memset_v = internal constant i8* (i8*, i32, i32)* @memset, align 4
@blake2s_sigma = internal constant [10 x [16 x i8]] [[16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F", [16 x i8] c"\0E\0A\04\08\09\0F\0D\06\01\0C\00\02\0B\07\05\03", [16 x i8] c"\0B\08\0C\00\05\02\0F\0D\0A\0E\03\06\07\01\09\04", [16 x i8] c"\07\09\03\01\0D\0C\0B\0E\02\06\05\0A\04\00\0F\08", [16 x i8] c"\09\00\05\07\02\04\0A\0F\0E\01\0B\0C\06\08\03\0D", [16 x i8] c"\02\0C\06\0A\00\0B\08\03\04\0D\07\05\0F\0E\01\09", [16 x i8] c"\0C\05\01\0F\0E\0D\04\0A\00\07\06\03\09\02\08\0B", [16 x i8] c"\0D\0B\07\0E\0C\01\03\09\05\00\0F\04\08\06\02\0A", [16 x i8] c"\06\0F\0E\09\0B\03\00\08\0C\02\0D\07\01\04\0A\05", [16 x i8] c"\0A\02\08\04\07\06\01\05\0F\0B\09\0E\03\0C\0D\00"], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2s_init_param(%struct.blake2s_state__* %S, %struct.blake2s_param__* %P) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %P.addr = alloca %struct.blake2s_param__*, align 4
  %p = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store %struct.blake2s_param__* %P, %struct.blake2s_param__** %P.addr, align 4
  %0 = load %struct.blake2s_param__*, %struct.blake2s_param__** %P.addr, align 4
  %1 = bitcast %struct.blake2s_param__* %0 to i8*
  store i8* %1, i8** %p, align 4
  %2 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  call void @blake2s_init0(%struct.blake2s_state__* %2)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %3, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %p, align 4
  %5 = load i32, i32* %i, align 4
  %mul = mul i32 %5, 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %mul
  %call = call i32 @load32(i8* %arrayidx)
  %6 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %6, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [8 x i32], [8 x i32]* %h, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx1, align 4
  %xor = xor i32 %8, %call
  store i32 %xor, i32* %arrayidx1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load %struct.blake2s_param__*, %struct.blake2s_param__** %P.addr, align 4
  %digest_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %10, i32 0, i32 0
  %11 = load i8, i8* %digest_length, align 1
  %conv = zext i8 %11 to i32
  %12 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %outlen = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %12, i32 0, i32 5
  store i32 %conv, i32* %outlen, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2s_init0(%struct.blake2s_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %i = alloca i32, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  %0 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %1 = bitcast %struct.blake2s_state__* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 124, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  %5 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [8 x i32], [8 x i32]* %h, i32 0, i32 %6
  store i32 %4, i32* %arrayidx1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @load32(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %p = alloca i8*, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = load i8*, i8** %src.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i8*, i8** %p, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 0
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 0
  %3 = load i8*, i8** %p, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %shl3 = shl i32 %conv2, 8
  %or = or i32 %shl, %shl3
  %5 = load i8*, i8** %p, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %5, i32 2
  %6 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %6 to i32
  %shl6 = shl i32 %conv5, 16
  %or7 = or i32 %or, %shl6
  %7 = load i8*, i8** %p, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %7, i32 3
  %8 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %8 to i32
  %shl10 = shl i32 %conv9, 24
  %or11 = or i32 %or7, %shl10
  ret i32 %or11
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2s_init(%struct.blake2s_state__* %S, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %P = alloca [1 x %struct.blake2s_param__], align 16
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ugt i32 %1, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %2 to i8
  %arraydecay = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 16
  %arraydecay1 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay1, i32 0, i32 1
  store i8 0, i8* %key_length, align 1
  %arraydecay2 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay2, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay3 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay3, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay4 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay4, i32 0, i32 4
  %3 = bitcast i32* %leaf_length to i8*
  call void @store32(i8* %3, i32 0)
  %arraydecay5 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay5, i32 0, i32 5
  %4 = bitcast i32* %node_offset to i8*
  call void @store32(i8* %4, i32 0)
  %arraydecay6 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %xof_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay6, i32 0, i32 6
  %5 = bitcast i16* %xof_length to i8*
  call void @store16(i8* %5, i16 zeroext 0)
  %arraydecay7 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay7, i32 0, i32 7
  store i8 0, i8* %node_depth, align 2
  %arraydecay8 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay8, i32 0, i32 8
  store i8 0, i8* %inner_length, align 1
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay9, i32 0, i32 9
  %arraydecay10 = getelementptr inbounds [8 x i8], [8 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay10, i8 0, i32 8, i1 false)
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay11, i32 0, i32 10
  %arraydecay12 = getelementptr inbounds [8 x i8], [8 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 8 %arraydecay12, i8 0, i32 8, i1 false)
  %6 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %call = call i32 @blake2s_init_param(%struct.blake2s_state__* %6, %struct.blake2s_param__* %arraydecay13)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define internal void @store32(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i32, i32* %w.addr, align 4
  %shr = lshr i32 %1, 0
  %conv = trunc i32 %shr to i8
  %2 = load i8*, i8** %p, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 0
  store i8 %conv, i8* %arrayidx, align 1
  %3 = load i32, i32* %w.addr, align 4
  %shr1 = lshr i32 %3, 8
  %conv2 = trunc i32 %shr1 to i8
  %4 = load i8*, i8** %p, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %4, i32 1
  store i8 %conv2, i8* %arrayidx3, align 1
  %5 = load i32, i32* %w.addr, align 4
  %shr4 = lshr i32 %5, 16
  %conv5 = trunc i32 %shr4 to i8
  %6 = load i8*, i8** %p, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %6, i32 2
  store i8 %conv5, i8* %arrayidx6, align 1
  %7 = load i32, i32* %w.addr, align 4
  %shr7 = lshr i32 %7, 24
  %conv8 = trunc i32 %shr7 to i8
  %8 = load i8*, i8** %p, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %8, i32 3
  store i8 %conv8, i8* %arrayidx9, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store16(i8* %dst, i16 zeroext %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i16, align 2
  %p = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i16 %w, i16* %w.addr, align 2
  %0 = load i8*, i8** %dst.addr, align 4
  store i8* %0, i8** %p, align 4
  %1 = load i16, i16* %w.addr, align 2
  %conv = trunc i16 %1 to i8
  %2 = load i8*, i8** %p, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %2, i32 1
  store i8* %incdec.ptr, i8** %p, align 4
  store i8 %conv, i8* %2, align 1
  %3 = load i16, i16* %w.addr, align 2
  %conv1 = zext i16 %3 to i32
  %shr = ashr i32 %conv1, 8
  %conv2 = trunc i32 %shr to i16
  store i16 %conv2, i16* %w.addr, align 2
  %4 = load i16, i16* %w.addr, align 2
  %conv3 = trunc i16 %4 to i8
  %5 = load i8*, i8** %p, align 4
  %incdec.ptr4 = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr4, i8** %p, align 4
  store i8 %conv3, i8* %5, align 1
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2s_init_key(%struct.blake2s_state__* %S, i32 %outlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %outlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %P = alloca [1 x %struct.blake2s_param__], align 16
  %block = alloca [64 x i8], align 16
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ugt i32 %1, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8*, i8** %key.addr, align 4
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then6

lor.lhs.false2:                                   ; preds = %if.end
  %3 = load i32, i32* %keylen.addr, align 4
  %tobool3 = icmp ne i32 %3, 0
  br i1 %tobool3, label %lor.lhs.false4, label %if.then6

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %4 = load i32, i32* %keylen.addr, align 4
  %cmp5 = icmp ugt i32 %4, 32
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %lor.lhs.false4, %lor.lhs.false2, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %lor.lhs.false4
  %5 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %5 to i8
  %arraydecay = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 16
  %6 = load i32, i32* %keylen.addr, align 4
  %conv8 = trunc i32 %6 to i8
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay9, i32 0, i32 1
  store i8 %conv8, i8* %key_length, align 1
  %arraydecay10 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay10, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay11, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay12 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay12, i32 0, i32 4
  %7 = bitcast i32* %leaf_length to i8*
  call void @store32(i8* %7, i32 0)
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay13, i32 0, i32 5
  %8 = bitcast i32* %node_offset to i8*
  call void @store32(i8* %8, i32 0)
  %arraydecay14 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %xof_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay14, i32 0, i32 6
  %9 = bitcast i16* %xof_length to i8*
  call void @store16(i8* %9, i16 zeroext 0)
  %arraydecay15 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay15, i32 0, i32 7
  store i8 0, i8* %node_depth, align 2
  %arraydecay16 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay16, i32 0, i32 8
  store i8 0, i8* %inner_length, align 1
  %arraydecay17 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay17, i32 0, i32 9
  %arraydecay18 = getelementptr inbounds [8 x i8], [8 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay18, i8 0, i32 8, i1 false)
  %arraydecay19 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2s_param__, %struct.blake2s_param__* %arraydecay19, i32 0, i32 10
  %arraydecay20 = getelementptr inbounds [8 x i8], [8 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 8 %arraydecay20, i8 0, i32 8, i1 false)
  %10 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %arraydecay21 = getelementptr inbounds [1 x %struct.blake2s_param__], [1 x %struct.blake2s_param__]* %P, i32 0, i32 0
  %call = call i32 @blake2s_init_param(%struct.blake2s_state__* %10, %struct.blake2s_param__* %arraydecay21)
  %cmp22 = icmp slt i32 %call, 0
  br i1 %cmp22, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end7
  store i32 -1, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.end7
  %arraydecay26 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay26, i8 0, i32 64, i1 false)
  %arraydecay27 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %11 = load i8*, i8** %key.addr, align 4
  %12 = load i32, i32* %keylen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay27, i8* align 1 %11, i32 %12, i1 false)
  %13 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %arraydecay28 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %call29 = call i32 @blake2s_update(%struct.blake2s_state__* %13, i8* %arraydecay28, i32 64)
  %arraydecay30 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @secure_zero_memory(i8* %arraydecay30, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end25, %if.then24, %if.then6, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2s_update(%struct.blake2s_state__* %S, i8* %pin, i32 %inlen) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %pin.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %in = alloca i8*, align 4
  %left = alloca i32, align 4
  %fill = alloca i32, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i8* %pin, i8** %pin.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  %0 = load i8*, i8** %pin.addr, align 4
  store i8* %0, i8** %in, align 4
  %1 = load i32, i32* %inlen.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %2 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %2, i32 0, i32 4
  %3 = load i32, i32* %buflen, align 4
  store i32 %3, i32* %left, align 4
  %4 = load i32, i32* %left, align 4
  %sub = sub i32 64, %4
  store i32 %sub, i32* %fill, align 4
  %5 = load i32, i32* %inlen.addr, align 4
  %6 = load i32, i32* %fill, align 4
  %cmp1 = icmp ugt i32 %5, %6
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %7 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buflen3 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %7, i32 0, i32 4
  store i32 0, i32* %buflen3, align 4
  %8 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %8, i32 0, i32 3
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %buf, i32 0, i32 0
  %9 = load i32, i32* %left, align 4
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 %9
  %10 = load i8*, i8** %in, align 4
  %11 = load i32, i32* %fill, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %10, i32 %11, i1 false)
  %12 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  call void @blake2s_increment_counter(%struct.blake2s_state__* %12, i32 64)
  %13 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %14 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buf4 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %14, i32 0, i32 3
  %arraydecay5 = getelementptr inbounds [64 x i8], [64 x i8]* %buf4, i32 0, i32 0
  call void @blake2s_compress(%struct.blake2s_state__* %13, i8* %arraydecay5)
  %15 = load i32, i32* %fill, align 4
  %16 = load i8*, i8** %in, align 4
  %add.ptr6 = getelementptr inbounds i8, i8* %16, i32 %15
  store i8* %add.ptr6, i8** %in, align 4
  %17 = load i32, i32* %fill, align 4
  %18 = load i32, i32* %inlen.addr, align 4
  %sub7 = sub i32 %18, %17
  store i32 %sub7, i32* %inlen.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then2
  %19 = load i32, i32* %inlen.addr, align 4
  %cmp8 = icmp ugt i32 %19, 64
  br i1 %cmp8, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %20 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  call void @blake2s_increment_counter(%struct.blake2s_state__* %20, i32 64)
  %21 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %22 = load i8*, i8** %in, align 4
  call void @blake2s_compress(%struct.blake2s_state__* %21, i8* %22)
  %23 = load i8*, i8** %in, align 4
  %add.ptr9 = getelementptr inbounds i8, i8* %23, i32 64
  store i8* %add.ptr9, i8** %in, align 4
  %24 = load i32, i32* %inlen.addr, align 4
  %sub10 = sub i32 %24, 64
  store i32 %sub10, i32* %inlen.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end

if.end:                                           ; preds = %while.end, %if.then
  %25 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buf11 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %25, i32 0, i32 3
  %arraydecay12 = getelementptr inbounds [64 x i8], [64 x i8]* %buf11, i32 0, i32 0
  %26 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buflen13 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %26, i32 0, i32 4
  %27 = load i32, i32* %buflen13, align 4
  %add.ptr14 = getelementptr inbounds i8, i8* %arraydecay12, i32 %27
  %28 = load i8*, i8** %in, align 4
  %29 = load i32, i32* %inlen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr14, i8* align 1 %28, i32 %29, i1 false)
  %30 = load i32, i32* %inlen.addr, align 4
  %31 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buflen15 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %31, i32 0, i32 4
  %32 = load i32, i32* %buflen15, align 4
  %add = add i32 %32, %30
  store i32 %add, i32* %buflen15, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.end, %entry
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @secure_zero_memory(i8* %v, i32 %n) #0 {
entry:
  %v.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  store i8* %v, i8** %v.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load volatile i8* (i8*, i32, i32)*, i8* (i8*, i32, i32)** @secure_zero_memory.memset_v, align 4
  %1 = load i8*, i8** %v.addr, align 4
  %2 = load i32, i32* %n.addr, align 4
  %call = call i8* %0(i8* %1, i32 0, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2s_increment_counter(%struct.blake2s_state__* %S, i32 %inc) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %inc.addr = alloca i32, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i32 %inc, i32* %inc.addr, align 4
  %0 = load i32, i32* %inc.addr, align 4
  %1 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %t = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %t, i32 0, i32 0
  %2 = load i32, i32* %arrayidx, align 4
  %add = add i32 %2, %0
  store i32 %add, i32* %arrayidx, align 4
  %3 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %t1 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %3, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %t1, i32 0, i32 0
  %4 = load i32, i32* %arrayidx2, align 4
  %5 = load i32, i32* %inc.addr, align 4
  %cmp = icmp ult i32 %4, %5
  %conv = zext i1 %cmp to i32
  %6 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %t3 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %6, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %t3, i32 0, i32 1
  %7 = load i32, i32* %arrayidx4, align 4
  %add5 = add i32 %7, %conv
  store i32 %add5, i32* %arrayidx4, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2s_compress(%struct.blake2s_state__* %S, i8* %in) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %in.addr = alloca i8*, align 4
  %m = alloca [16 x i32], align 16
  %v = alloca [16 x i32], align 16
  %i = alloca i32, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %in.addr, align 4
  %2 = load i32, i32* %i, align 4
  %mul = mul i32 %2, 4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  %call = call i32 @load32(i8* %add.ptr)
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %3
  store i32 %call, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc6, %for.end
  %5 = load i32, i32* %i, align 4
  %cmp2 = icmp ult i32 %5, 8
  br i1 %cmp2, label %for.body3, label %for.end8

for.body3:                                        ; preds = %for.cond1
  %6 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %6, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [8 x i32], [8 x i32]* %h, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx4, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 %9
  store i32 %8, i32* %arrayidx5, align 4
  br label %for.inc6

for.inc6:                                         ; preds = %for.body3
  %10 = load i32, i32* %i, align 4
  %inc7 = add i32 %10, 1
  store i32 %inc7, i32* %i, align 4
  br label %for.cond1

for.end8:                                         ; preds = %for.cond1
  %11 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 0), align 16
  %arrayidx9 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %11, i32* %arrayidx9, align 16
  %12 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 1), align 4
  %arrayidx10 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %12, i32* %arrayidx10, align 4
  %13 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 2), align 8
  %arrayidx11 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %13, i32* %arrayidx11, align 8
  %14 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 3), align 4
  %arrayidx12 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %14, i32* %arrayidx12, align 4
  %15 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %t = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %15, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x i32], [2 x i32]* %t, i32 0, i32 0
  %16 = load i32, i32* %arrayidx13, align 4
  %17 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 4), align 16
  %xor = xor i32 %16, %17
  %arrayidx14 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %xor, i32* %arrayidx14, align 16
  %18 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %t15 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %18, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %t15, i32 0, i32 1
  %19 = load i32, i32* %arrayidx16, align 4
  %20 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 5), align 4
  %xor17 = xor i32 %19, %20
  %arrayidx18 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %xor17, i32* %arrayidx18, align 4
  %21 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %21, i32 0, i32 2
  %arrayidx19 = getelementptr inbounds [2 x i32], [2 x i32]* %f, i32 0, i32 0
  %22 = load i32, i32* %arrayidx19, align 4
  %23 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 6), align 8
  %xor20 = xor i32 %22, %23
  %arrayidx21 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %xor20, i32* %arrayidx21, align 8
  %24 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %f22 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %24, i32 0, i32 2
  %arrayidx23 = getelementptr inbounds [2 x i32], [2 x i32]* %f22, i32 0, i32 1
  %25 = load i32, i32* %arrayidx23, align 4
  %26 = load i32, i32* getelementptr inbounds ([8 x i32], [8 x i32]* @blake2s_IV, i32 0, i32 7), align 4
  %xor24 = xor i32 %25, %26
  %arrayidx25 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %xor24, i32* %arrayidx25, align 4
  br label %do.body

do.body:                                          ; preds = %for.end8
  br label %do.body26

do.body26:                                        ; preds = %do.body
  %arrayidx27 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %27 = load i32, i32* %arrayidx27, align 16
  %arrayidx28 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %28 = load i32, i32* %arrayidx28, align 16
  %add = add i32 %27, %28
  %29 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 0), align 16
  %idxprom = zext i8 %29 to i32
  %arrayidx29 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom
  %30 = load i32, i32* %arrayidx29, align 4
  %add30 = add i32 %add, %30
  %arrayidx31 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add30, i32* %arrayidx31, align 16
  %arrayidx32 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %31 = load i32, i32* %arrayidx32, align 16
  %arrayidx33 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %32 = load i32, i32* %arrayidx33, align 16
  %xor34 = xor i32 %31, %32
  %call35 = call i32 @rotr32(i32 %xor34, i32 16)
  %arrayidx36 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call35, i32* %arrayidx36, align 16
  %arrayidx37 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %33 = load i32, i32* %arrayidx37, align 16
  %arrayidx38 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %34 = load i32, i32* %arrayidx38, align 16
  %add39 = add i32 %33, %34
  %arrayidx40 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add39, i32* %arrayidx40, align 16
  %arrayidx41 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %35 = load i32, i32* %arrayidx41, align 16
  %arrayidx42 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %36 = load i32, i32* %arrayidx42, align 16
  %xor43 = xor i32 %35, %36
  %call44 = call i32 @rotr32(i32 %xor43, i32 12)
  %arrayidx45 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call44, i32* %arrayidx45, align 16
  %arrayidx46 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %37 = load i32, i32* %arrayidx46, align 16
  %arrayidx47 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %38 = load i32, i32* %arrayidx47, align 16
  %add48 = add i32 %37, %38
  %39 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 1), align 1
  %idxprom49 = zext i8 %39 to i32
  %arrayidx50 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom49
  %40 = load i32, i32* %arrayidx50, align 4
  %add51 = add i32 %add48, %40
  %arrayidx52 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add51, i32* %arrayidx52, align 16
  %arrayidx53 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %41 = load i32, i32* %arrayidx53, align 16
  %arrayidx54 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %42 = load i32, i32* %arrayidx54, align 16
  %xor55 = xor i32 %41, %42
  %call56 = call i32 @rotr32(i32 %xor55, i32 8)
  %arrayidx57 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call56, i32* %arrayidx57, align 16
  %arrayidx58 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %43 = load i32, i32* %arrayidx58, align 16
  %arrayidx59 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %44 = load i32, i32* %arrayidx59, align 16
  %add60 = add i32 %43, %44
  %arrayidx61 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add60, i32* %arrayidx61, align 16
  %arrayidx62 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %45 = load i32, i32* %arrayidx62, align 16
  %arrayidx63 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %46 = load i32, i32* %arrayidx63, align 16
  %xor64 = xor i32 %45, %46
  %call65 = call i32 @rotr32(i32 %xor64, i32 7)
  %arrayidx66 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call65, i32* %arrayidx66, align 16
  br label %do.end

do.end:                                           ; preds = %do.body26
  br label %do.body67

do.body67:                                        ; preds = %do.end
  %arrayidx68 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %47 = load i32, i32* %arrayidx68, align 4
  %arrayidx69 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %48 = load i32, i32* %arrayidx69, align 4
  %add70 = add i32 %47, %48
  %49 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 2), align 2
  %idxprom71 = zext i8 %49 to i32
  %arrayidx72 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom71
  %50 = load i32, i32* %arrayidx72, align 4
  %add73 = add i32 %add70, %50
  %arrayidx74 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add73, i32* %arrayidx74, align 4
  %arrayidx75 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %51 = load i32, i32* %arrayidx75, align 4
  %arrayidx76 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %52 = load i32, i32* %arrayidx76, align 4
  %xor77 = xor i32 %51, %52
  %call78 = call i32 @rotr32(i32 %xor77, i32 16)
  %arrayidx79 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call78, i32* %arrayidx79, align 4
  %arrayidx80 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %53 = load i32, i32* %arrayidx80, align 4
  %arrayidx81 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %54 = load i32, i32* %arrayidx81, align 4
  %add82 = add i32 %53, %54
  %arrayidx83 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add82, i32* %arrayidx83, align 4
  %arrayidx84 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %55 = load i32, i32* %arrayidx84, align 4
  %arrayidx85 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %56 = load i32, i32* %arrayidx85, align 4
  %xor86 = xor i32 %55, %56
  %call87 = call i32 @rotr32(i32 %xor86, i32 12)
  %arrayidx88 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call87, i32* %arrayidx88, align 4
  %arrayidx89 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %57 = load i32, i32* %arrayidx89, align 4
  %arrayidx90 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %58 = load i32, i32* %arrayidx90, align 4
  %add91 = add i32 %57, %58
  %59 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 3), align 1
  %idxprom92 = zext i8 %59 to i32
  %arrayidx93 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom92
  %60 = load i32, i32* %arrayidx93, align 4
  %add94 = add i32 %add91, %60
  %arrayidx95 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add94, i32* %arrayidx95, align 4
  %arrayidx96 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %61 = load i32, i32* %arrayidx96, align 4
  %arrayidx97 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %62 = load i32, i32* %arrayidx97, align 4
  %xor98 = xor i32 %61, %62
  %call99 = call i32 @rotr32(i32 %xor98, i32 8)
  %arrayidx100 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call99, i32* %arrayidx100, align 4
  %arrayidx101 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %63 = load i32, i32* %arrayidx101, align 4
  %arrayidx102 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %64 = load i32, i32* %arrayidx102, align 4
  %add103 = add i32 %63, %64
  %arrayidx104 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add103, i32* %arrayidx104, align 4
  %arrayidx105 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %65 = load i32, i32* %arrayidx105, align 4
  %arrayidx106 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %66 = load i32, i32* %arrayidx106, align 4
  %xor107 = xor i32 %65, %66
  %call108 = call i32 @rotr32(i32 %xor107, i32 7)
  %arrayidx109 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call108, i32* %arrayidx109, align 4
  br label %do.end110

do.end110:                                        ; preds = %do.body67
  br label %do.body111

do.body111:                                       ; preds = %do.end110
  %arrayidx112 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %67 = load i32, i32* %arrayidx112, align 8
  %arrayidx113 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %68 = load i32, i32* %arrayidx113, align 8
  %add114 = add i32 %67, %68
  %69 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 4), align 4
  %idxprom115 = zext i8 %69 to i32
  %arrayidx116 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom115
  %70 = load i32, i32* %arrayidx116, align 4
  %add117 = add i32 %add114, %70
  %arrayidx118 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add117, i32* %arrayidx118, align 8
  %arrayidx119 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %71 = load i32, i32* %arrayidx119, align 8
  %arrayidx120 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %72 = load i32, i32* %arrayidx120, align 8
  %xor121 = xor i32 %71, %72
  %call122 = call i32 @rotr32(i32 %xor121, i32 16)
  %arrayidx123 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call122, i32* %arrayidx123, align 8
  %arrayidx124 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %73 = load i32, i32* %arrayidx124, align 8
  %arrayidx125 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %74 = load i32, i32* %arrayidx125, align 8
  %add126 = add i32 %73, %74
  %arrayidx127 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add126, i32* %arrayidx127, align 8
  %arrayidx128 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %75 = load i32, i32* %arrayidx128, align 8
  %arrayidx129 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %76 = load i32, i32* %arrayidx129, align 8
  %xor130 = xor i32 %75, %76
  %call131 = call i32 @rotr32(i32 %xor130, i32 12)
  %arrayidx132 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call131, i32* %arrayidx132, align 8
  %arrayidx133 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %77 = load i32, i32* %arrayidx133, align 8
  %arrayidx134 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %78 = load i32, i32* %arrayidx134, align 8
  %add135 = add i32 %77, %78
  %79 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 5), align 1
  %idxprom136 = zext i8 %79 to i32
  %arrayidx137 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom136
  %80 = load i32, i32* %arrayidx137, align 4
  %add138 = add i32 %add135, %80
  %arrayidx139 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add138, i32* %arrayidx139, align 8
  %arrayidx140 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %81 = load i32, i32* %arrayidx140, align 8
  %arrayidx141 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %82 = load i32, i32* %arrayidx141, align 8
  %xor142 = xor i32 %81, %82
  %call143 = call i32 @rotr32(i32 %xor142, i32 8)
  %arrayidx144 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call143, i32* %arrayidx144, align 8
  %arrayidx145 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %83 = load i32, i32* %arrayidx145, align 8
  %arrayidx146 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %84 = load i32, i32* %arrayidx146, align 8
  %add147 = add i32 %83, %84
  %arrayidx148 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add147, i32* %arrayidx148, align 8
  %arrayidx149 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %85 = load i32, i32* %arrayidx149, align 8
  %arrayidx150 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %86 = load i32, i32* %arrayidx150, align 8
  %xor151 = xor i32 %85, %86
  %call152 = call i32 @rotr32(i32 %xor151, i32 7)
  %arrayidx153 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call152, i32* %arrayidx153, align 8
  br label %do.end154

do.end154:                                        ; preds = %do.body111
  br label %do.body155

do.body155:                                       ; preds = %do.end154
  %arrayidx156 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %87 = load i32, i32* %arrayidx156, align 4
  %arrayidx157 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %88 = load i32, i32* %arrayidx157, align 4
  %add158 = add i32 %87, %88
  %89 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 6), align 2
  %idxprom159 = zext i8 %89 to i32
  %arrayidx160 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom159
  %90 = load i32, i32* %arrayidx160, align 4
  %add161 = add i32 %add158, %90
  %arrayidx162 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add161, i32* %arrayidx162, align 4
  %arrayidx163 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %91 = load i32, i32* %arrayidx163, align 4
  %arrayidx164 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %92 = load i32, i32* %arrayidx164, align 4
  %xor165 = xor i32 %91, %92
  %call166 = call i32 @rotr32(i32 %xor165, i32 16)
  %arrayidx167 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call166, i32* %arrayidx167, align 4
  %arrayidx168 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %93 = load i32, i32* %arrayidx168, align 4
  %arrayidx169 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %94 = load i32, i32* %arrayidx169, align 4
  %add170 = add i32 %93, %94
  %arrayidx171 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add170, i32* %arrayidx171, align 4
  %arrayidx172 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %95 = load i32, i32* %arrayidx172, align 4
  %arrayidx173 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %96 = load i32, i32* %arrayidx173, align 4
  %xor174 = xor i32 %95, %96
  %call175 = call i32 @rotr32(i32 %xor174, i32 12)
  %arrayidx176 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call175, i32* %arrayidx176, align 4
  %arrayidx177 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %97 = load i32, i32* %arrayidx177, align 4
  %arrayidx178 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %98 = load i32, i32* %arrayidx178, align 4
  %add179 = add i32 %97, %98
  %99 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 7), align 1
  %idxprom180 = zext i8 %99 to i32
  %arrayidx181 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom180
  %100 = load i32, i32* %arrayidx181, align 4
  %add182 = add i32 %add179, %100
  %arrayidx183 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add182, i32* %arrayidx183, align 4
  %arrayidx184 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %101 = load i32, i32* %arrayidx184, align 4
  %arrayidx185 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %102 = load i32, i32* %arrayidx185, align 4
  %xor186 = xor i32 %101, %102
  %call187 = call i32 @rotr32(i32 %xor186, i32 8)
  %arrayidx188 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call187, i32* %arrayidx188, align 4
  %arrayidx189 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %103 = load i32, i32* %arrayidx189, align 4
  %arrayidx190 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %104 = load i32, i32* %arrayidx190, align 4
  %add191 = add i32 %103, %104
  %arrayidx192 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add191, i32* %arrayidx192, align 4
  %arrayidx193 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %105 = load i32, i32* %arrayidx193, align 4
  %arrayidx194 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %106 = load i32, i32* %arrayidx194, align 4
  %xor195 = xor i32 %105, %106
  %call196 = call i32 @rotr32(i32 %xor195, i32 7)
  %arrayidx197 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call196, i32* %arrayidx197, align 4
  br label %do.end198

do.end198:                                        ; preds = %do.body155
  br label %do.body199

do.body199:                                       ; preds = %do.end198
  %arrayidx200 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %107 = load i32, i32* %arrayidx200, align 16
  %arrayidx201 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %108 = load i32, i32* %arrayidx201, align 4
  %add202 = add i32 %107, %108
  %109 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 8), align 8
  %idxprom203 = zext i8 %109 to i32
  %arrayidx204 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom203
  %110 = load i32, i32* %arrayidx204, align 4
  %add205 = add i32 %add202, %110
  %arrayidx206 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add205, i32* %arrayidx206, align 16
  %arrayidx207 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %111 = load i32, i32* %arrayidx207, align 4
  %arrayidx208 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %112 = load i32, i32* %arrayidx208, align 16
  %xor209 = xor i32 %111, %112
  %call210 = call i32 @rotr32(i32 %xor209, i32 16)
  %arrayidx211 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call210, i32* %arrayidx211, align 4
  %arrayidx212 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %113 = load i32, i32* %arrayidx212, align 8
  %arrayidx213 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %114 = load i32, i32* %arrayidx213, align 4
  %add214 = add i32 %113, %114
  %arrayidx215 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add214, i32* %arrayidx215, align 8
  %arrayidx216 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %115 = load i32, i32* %arrayidx216, align 4
  %arrayidx217 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %116 = load i32, i32* %arrayidx217, align 8
  %xor218 = xor i32 %115, %116
  %call219 = call i32 @rotr32(i32 %xor218, i32 12)
  %arrayidx220 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call219, i32* %arrayidx220, align 4
  %arrayidx221 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %117 = load i32, i32* %arrayidx221, align 16
  %arrayidx222 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %118 = load i32, i32* %arrayidx222, align 4
  %add223 = add i32 %117, %118
  %119 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 9), align 1
  %idxprom224 = zext i8 %119 to i32
  %arrayidx225 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom224
  %120 = load i32, i32* %arrayidx225, align 4
  %add226 = add i32 %add223, %120
  %arrayidx227 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add226, i32* %arrayidx227, align 16
  %arrayidx228 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %121 = load i32, i32* %arrayidx228, align 4
  %arrayidx229 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %122 = load i32, i32* %arrayidx229, align 16
  %xor230 = xor i32 %121, %122
  %call231 = call i32 @rotr32(i32 %xor230, i32 8)
  %arrayidx232 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call231, i32* %arrayidx232, align 4
  %arrayidx233 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %123 = load i32, i32* %arrayidx233, align 8
  %arrayidx234 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %124 = load i32, i32* %arrayidx234, align 4
  %add235 = add i32 %123, %124
  %arrayidx236 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add235, i32* %arrayidx236, align 8
  %arrayidx237 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %125 = load i32, i32* %arrayidx237, align 4
  %arrayidx238 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %126 = load i32, i32* %arrayidx238, align 8
  %xor239 = xor i32 %125, %126
  %call240 = call i32 @rotr32(i32 %xor239, i32 7)
  %arrayidx241 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call240, i32* %arrayidx241, align 4
  br label %do.end242

do.end242:                                        ; preds = %do.body199
  br label %do.body243

do.body243:                                       ; preds = %do.end242
  %arrayidx244 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %127 = load i32, i32* %arrayidx244, align 4
  %arrayidx245 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %128 = load i32, i32* %arrayidx245, align 8
  %add246 = add i32 %127, %128
  %129 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 10), align 2
  %idxprom247 = zext i8 %129 to i32
  %arrayidx248 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom247
  %130 = load i32, i32* %arrayidx248, align 4
  %add249 = add i32 %add246, %130
  %arrayidx250 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add249, i32* %arrayidx250, align 4
  %arrayidx251 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %131 = load i32, i32* %arrayidx251, align 16
  %arrayidx252 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %132 = load i32, i32* %arrayidx252, align 4
  %xor253 = xor i32 %131, %132
  %call254 = call i32 @rotr32(i32 %xor253, i32 16)
  %arrayidx255 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call254, i32* %arrayidx255, align 16
  %arrayidx256 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %133 = load i32, i32* %arrayidx256, align 4
  %arrayidx257 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %134 = load i32, i32* %arrayidx257, align 16
  %add258 = add i32 %133, %134
  %arrayidx259 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add258, i32* %arrayidx259, align 4
  %arrayidx260 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %135 = load i32, i32* %arrayidx260, align 8
  %arrayidx261 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %136 = load i32, i32* %arrayidx261, align 4
  %xor262 = xor i32 %135, %136
  %call263 = call i32 @rotr32(i32 %xor262, i32 12)
  %arrayidx264 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call263, i32* %arrayidx264, align 8
  %arrayidx265 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %137 = load i32, i32* %arrayidx265, align 4
  %arrayidx266 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %138 = load i32, i32* %arrayidx266, align 8
  %add267 = add i32 %137, %138
  %139 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 11), align 1
  %idxprom268 = zext i8 %139 to i32
  %arrayidx269 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom268
  %140 = load i32, i32* %arrayidx269, align 4
  %add270 = add i32 %add267, %140
  %arrayidx271 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add270, i32* %arrayidx271, align 4
  %arrayidx272 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %141 = load i32, i32* %arrayidx272, align 16
  %arrayidx273 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %142 = load i32, i32* %arrayidx273, align 4
  %xor274 = xor i32 %141, %142
  %call275 = call i32 @rotr32(i32 %xor274, i32 8)
  %arrayidx276 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call275, i32* %arrayidx276, align 16
  %arrayidx277 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %143 = load i32, i32* %arrayidx277, align 4
  %arrayidx278 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %144 = load i32, i32* %arrayidx278, align 16
  %add279 = add i32 %143, %144
  %arrayidx280 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add279, i32* %arrayidx280, align 4
  %arrayidx281 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %145 = load i32, i32* %arrayidx281, align 8
  %arrayidx282 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %146 = load i32, i32* %arrayidx282, align 4
  %xor283 = xor i32 %145, %146
  %call284 = call i32 @rotr32(i32 %xor283, i32 7)
  %arrayidx285 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call284, i32* %arrayidx285, align 8
  br label %do.end286

do.end286:                                        ; preds = %do.body243
  br label %do.body287

do.body287:                                       ; preds = %do.end286
  %arrayidx288 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %147 = load i32, i32* %arrayidx288, align 8
  %arrayidx289 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %148 = load i32, i32* %arrayidx289, align 4
  %add290 = add i32 %147, %148
  %149 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 12), align 4
  %idxprom291 = zext i8 %149 to i32
  %arrayidx292 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom291
  %150 = load i32, i32* %arrayidx292, align 4
  %add293 = add i32 %add290, %150
  %arrayidx294 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add293, i32* %arrayidx294, align 8
  %arrayidx295 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %151 = load i32, i32* %arrayidx295, align 4
  %arrayidx296 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %152 = load i32, i32* %arrayidx296, align 8
  %xor297 = xor i32 %151, %152
  %call298 = call i32 @rotr32(i32 %xor297, i32 16)
  %arrayidx299 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call298, i32* %arrayidx299, align 4
  %arrayidx300 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %153 = load i32, i32* %arrayidx300, align 16
  %arrayidx301 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %154 = load i32, i32* %arrayidx301, align 4
  %add302 = add i32 %153, %154
  %arrayidx303 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add302, i32* %arrayidx303, align 16
  %arrayidx304 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %155 = load i32, i32* %arrayidx304, align 4
  %arrayidx305 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %156 = load i32, i32* %arrayidx305, align 16
  %xor306 = xor i32 %155, %156
  %call307 = call i32 @rotr32(i32 %xor306, i32 12)
  %arrayidx308 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call307, i32* %arrayidx308, align 4
  %arrayidx309 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %157 = load i32, i32* %arrayidx309, align 8
  %arrayidx310 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %158 = load i32, i32* %arrayidx310, align 4
  %add311 = add i32 %157, %158
  %159 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 13), align 1
  %idxprom312 = zext i8 %159 to i32
  %arrayidx313 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom312
  %160 = load i32, i32* %arrayidx313, align 4
  %add314 = add i32 %add311, %160
  %arrayidx315 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add314, i32* %arrayidx315, align 8
  %arrayidx316 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %161 = load i32, i32* %arrayidx316, align 4
  %arrayidx317 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %162 = load i32, i32* %arrayidx317, align 8
  %xor318 = xor i32 %161, %162
  %call319 = call i32 @rotr32(i32 %xor318, i32 8)
  %arrayidx320 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call319, i32* %arrayidx320, align 4
  %arrayidx321 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %163 = load i32, i32* %arrayidx321, align 16
  %arrayidx322 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %164 = load i32, i32* %arrayidx322, align 4
  %add323 = add i32 %163, %164
  %arrayidx324 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add323, i32* %arrayidx324, align 16
  %arrayidx325 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %165 = load i32, i32* %arrayidx325, align 4
  %arrayidx326 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %166 = load i32, i32* %arrayidx326, align 16
  %xor327 = xor i32 %165, %166
  %call328 = call i32 @rotr32(i32 %xor327, i32 7)
  %arrayidx329 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call328, i32* %arrayidx329, align 4
  br label %do.end330

do.end330:                                        ; preds = %do.body287
  br label %do.body331

do.body331:                                       ; preds = %do.end330
  %arrayidx332 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %167 = load i32, i32* %arrayidx332, align 4
  %arrayidx333 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %168 = load i32, i32* %arrayidx333, align 16
  %add334 = add i32 %167, %168
  %169 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 14), align 2
  %idxprom335 = zext i8 %169 to i32
  %arrayidx336 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom335
  %170 = load i32, i32* %arrayidx336, align 4
  %add337 = add i32 %add334, %170
  %arrayidx338 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add337, i32* %arrayidx338, align 4
  %arrayidx339 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %171 = load i32, i32* %arrayidx339, align 8
  %arrayidx340 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %172 = load i32, i32* %arrayidx340, align 4
  %xor341 = xor i32 %171, %172
  %call342 = call i32 @rotr32(i32 %xor341, i32 16)
  %arrayidx343 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call342, i32* %arrayidx343, align 8
  %arrayidx344 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %173 = load i32, i32* %arrayidx344, align 4
  %arrayidx345 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %174 = load i32, i32* %arrayidx345, align 8
  %add346 = add i32 %173, %174
  %arrayidx347 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add346, i32* %arrayidx347, align 4
  %arrayidx348 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %175 = load i32, i32* %arrayidx348, align 16
  %arrayidx349 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %176 = load i32, i32* %arrayidx349, align 4
  %xor350 = xor i32 %175, %176
  %call351 = call i32 @rotr32(i32 %xor350, i32 12)
  %arrayidx352 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call351, i32* %arrayidx352, align 16
  %arrayidx353 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %177 = load i32, i32* %arrayidx353, align 4
  %arrayidx354 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %178 = load i32, i32* %arrayidx354, align 16
  %add355 = add i32 %177, %178
  %179 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 0, i32 15), align 1
  %idxprom356 = zext i8 %179 to i32
  %arrayidx357 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom356
  %180 = load i32, i32* %arrayidx357, align 4
  %add358 = add i32 %add355, %180
  %arrayidx359 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add358, i32* %arrayidx359, align 4
  %arrayidx360 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %181 = load i32, i32* %arrayidx360, align 8
  %arrayidx361 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %182 = load i32, i32* %arrayidx361, align 4
  %xor362 = xor i32 %181, %182
  %call363 = call i32 @rotr32(i32 %xor362, i32 8)
  %arrayidx364 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call363, i32* %arrayidx364, align 8
  %arrayidx365 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %183 = load i32, i32* %arrayidx365, align 4
  %arrayidx366 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %184 = load i32, i32* %arrayidx366, align 8
  %add367 = add i32 %183, %184
  %arrayidx368 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add367, i32* %arrayidx368, align 4
  %arrayidx369 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %185 = load i32, i32* %arrayidx369, align 16
  %arrayidx370 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %186 = load i32, i32* %arrayidx370, align 4
  %xor371 = xor i32 %185, %186
  %call372 = call i32 @rotr32(i32 %xor371, i32 7)
  %arrayidx373 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call372, i32* %arrayidx373, align 16
  br label %do.end374

do.end374:                                        ; preds = %do.body331
  br label %do.end375

do.end375:                                        ; preds = %do.end374
  br label %do.body376

do.body376:                                       ; preds = %do.end375
  br label %do.body377

do.body377:                                       ; preds = %do.body376
  %arrayidx378 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %187 = load i32, i32* %arrayidx378, align 16
  %arrayidx379 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %188 = load i32, i32* %arrayidx379, align 16
  %add380 = add i32 %187, %188
  %189 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 0), align 16
  %idxprom381 = zext i8 %189 to i32
  %arrayidx382 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom381
  %190 = load i32, i32* %arrayidx382, align 4
  %add383 = add i32 %add380, %190
  %arrayidx384 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add383, i32* %arrayidx384, align 16
  %arrayidx385 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %191 = load i32, i32* %arrayidx385, align 16
  %arrayidx386 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %192 = load i32, i32* %arrayidx386, align 16
  %xor387 = xor i32 %191, %192
  %call388 = call i32 @rotr32(i32 %xor387, i32 16)
  %arrayidx389 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call388, i32* %arrayidx389, align 16
  %arrayidx390 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %193 = load i32, i32* %arrayidx390, align 16
  %arrayidx391 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %194 = load i32, i32* %arrayidx391, align 16
  %add392 = add i32 %193, %194
  %arrayidx393 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add392, i32* %arrayidx393, align 16
  %arrayidx394 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %195 = load i32, i32* %arrayidx394, align 16
  %arrayidx395 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %196 = load i32, i32* %arrayidx395, align 16
  %xor396 = xor i32 %195, %196
  %call397 = call i32 @rotr32(i32 %xor396, i32 12)
  %arrayidx398 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call397, i32* %arrayidx398, align 16
  %arrayidx399 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %197 = load i32, i32* %arrayidx399, align 16
  %arrayidx400 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %198 = load i32, i32* %arrayidx400, align 16
  %add401 = add i32 %197, %198
  %199 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 1), align 1
  %idxprom402 = zext i8 %199 to i32
  %arrayidx403 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom402
  %200 = load i32, i32* %arrayidx403, align 4
  %add404 = add i32 %add401, %200
  %arrayidx405 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add404, i32* %arrayidx405, align 16
  %arrayidx406 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %201 = load i32, i32* %arrayidx406, align 16
  %arrayidx407 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %202 = load i32, i32* %arrayidx407, align 16
  %xor408 = xor i32 %201, %202
  %call409 = call i32 @rotr32(i32 %xor408, i32 8)
  %arrayidx410 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call409, i32* %arrayidx410, align 16
  %arrayidx411 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %203 = load i32, i32* %arrayidx411, align 16
  %arrayidx412 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %204 = load i32, i32* %arrayidx412, align 16
  %add413 = add i32 %203, %204
  %arrayidx414 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add413, i32* %arrayidx414, align 16
  %arrayidx415 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %205 = load i32, i32* %arrayidx415, align 16
  %arrayidx416 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %206 = load i32, i32* %arrayidx416, align 16
  %xor417 = xor i32 %205, %206
  %call418 = call i32 @rotr32(i32 %xor417, i32 7)
  %arrayidx419 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call418, i32* %arrayidx419, align 16
  br label %do.end420

do.end420:                                        ; preds = %do.body377
  br label %do.body421

do.body421:                                       ; preds = %do.end420
  %arrayidx422 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %207 = load i32, i32* %arrayidx422, align 4
  %arrayidx423 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %208 = load i32, i32* %arrayidx423, align 4
  %add424 = add i32 %207, %208
  %209 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 2), align 2
  %idxprom425 = zext i8 %209 to i32
  %arrayidx426 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom425
  %210 = load i32, i32* %arrayidx426, align 4
  %add427 = add i32 %add424, %210
  %arrayidx428 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add427, i32* %arrayidx428, align 4
  %arrayidx429 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %211 = load i32, i32* %arrayidx429, align 4
  %arrayidx430 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %212 = load i32, i32* %arrayidx430, align 4
  %xor431 = xor i32 %211, %212
  %call432 = call i32 @rotr32(i32 %xor431, i32 16)
  %arrayidx433 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call432, i32* %arrayidx433, align 4
  %arrayidx434 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %213 = load i32, i32* %arrayidx434, align 4
  %arrayidx435 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %214 = load i32, i32* %arrayidx435, align 4
  %add436 = add i32 %213, %214
  %arrayidx437 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add436, i32* %arrayidx437, align 4
  %arrayidx438 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %215 = load i32, i32* %arrayidx438, align 4
  %arrayidx439 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %216 = load i32, i32* %arrayidx439, align 4
  %xor440 = xor i32 %215, %216
  %call441 = call i32 @rotr32(i32 %xor440, i32 12)
  %arrayidx442 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call441, i32* %arrayidx442, align 4
  %arrayidx443 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %217 = load i32, i32* %arrayidx443, align 4
  %arrayidx444 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %218 = load i32, i32* %arrayidx444, align 4
  %add445 = add i32 %217, %218
  %219 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 3), align 1
  %idxprom446 = zext i8 %219 to i32
  %arrayidx447 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom446
  %220 = load i32, i32* %arrayidx447, align 4
  %add448 = add i32 %add445, %220
  %arrayidx449 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add448, i32* %arrayidx449, align 4
  %arrayidx450 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %221 = load i32, i32* %arrayidx450, align 4
  %arrayidx451 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %222 = load i32, i32* %arrayidx451, align 4
  %xor452 = xor i32 %221, %222
  %call453 = call i32 @rotr32(i32 %xor452, i32 8)
  %arrayidx454 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call453, i32* %arrayidx454, align 4
  %arrayidx455 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %223 = load i32, i32* %arrayidx455, align 4
  %arrayidx456 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %224 = load i32, i32* %arrayidx456, align 4
  %add457 = add i32 %223, %224
  %arrayidx458 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add457, i32* %arrayidx458, align 4
  %arrayidx459 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %225 = load i32, i32* %arrayidx459, align 4
  %arrayidx460 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %226 = load i32, i32* %arrayidx460, align 4
  %xor461 = xor i32 %225, %226
  %call462 = call i32 @rotr32(i32 %xor461, i32 7)
  %arrayidx463 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call462, i32* %arrayidx463, align 4
  br label %do.end464

do.end464:                                        ; preds = %do.body421
  br label %do.body465

do.body465:                                       ; preds = %do.end464
  %arrayidx466 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %227 = load i32, i32* %arrayidx466, align 8
  %arrayidx467 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %228 = load i32, i32* %arrayidx467, align 8
  %add468 = add i32 %227, %228
  %229 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 4), align 4
  %idxprom469 = zext i8 %229 to i32
  %arrayidx470 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom469
  %230 = load i32, i32* %arrayidx470, align 4
  %add471 = add i32 %add468, %230
  %arrayidx472 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add471, i32* %arrayidx472, align 8
  %arrayidx473 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %231 = load i32, i32* %arrayidx473, align 8
  %arrayidx474 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %232 = load i32, i32* %arrayidx474, align 8
  %xor475 = xor i32 %231, %232
  %call476 = call i32 @rotr32(i32 %xor475, i32 16)
  %arrayidx477 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call476, i32* %arrayidx477, align 8
  %arrayidx478 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %233 = load i32, i32* %arrayidx478, align 8
  %arrayidx479 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %234 = load i32, i32* %arrayidx479, align 8
  %add480 = add i32 %233, %234
  %arrayidx481 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add480, i32* %arrayidx481, align 8
  %arrayidx482 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %235 = load i32, i32* %arrayidx482, align 8
  %arrayidx483 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %236 = load i32, i32* %arrayidx483, align 8
  %xor484 = xor i32 %235, %236
  %call485 = call i32 @rotr32(i32 %xor484, i32 12)
  %arrayidx486 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call485, i32* %arrayidx486, align 8
  %arrayidx487 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %237 = load i32, i32* %arrayidx487, align 8
  %arrayidx488 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %238 = load i32, i32* %arrayidx488, align 8
  %add489 = add i32 %237, %238
  %239 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 5), align 1
  %idxprom490 = zext i8 %239 to i32
  %arrayidx491 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom490
  %240 = load i32, i32* %arrayidx491, align 4
  %add492 = add i32 %add489, %240
  %arrayidx493 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add492, i32* %arrayidx493, align 8
  %arrayidx494 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %241 = load i32, i32* %arrayidx494, align 8
  %arrayidx495 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %242 = load i32, i32* %arrayidx495, align 8
  %xor496 = xor i32 %241, %242
  %call497 = call i32 @rotr32(i32 %xor496, i32 8)
  %arrayidx498 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call497, i32* %arrayidx498, align 8
  %arrayidx499 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %243 = load i32, i32* %arrayidx499, align 8
  %arrayidx500 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %244 = load i32, i32* %arrayidx500, align 8
  %add501 = add i32 %243, %244
  %arrayidx502 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add501, i32* %arrayidx502, align 8
  %arrayidx503 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %245 = load i32, i32* %arrayidx503, align 8
  %arrayidx504 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %246 = load i32, i32* %arrayidx504, align 8
  %xor505 = xor i32 %245, %246
  %call506 = call i32 @rotr32(i32 %xor505, i32 7)
  %arrayidx507 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call506, i32* %arrayidx507, align 8
  br label %do.end508

do.end508:                                        ; preds = %do.body465
  br label %do.body509

do.body509:                                       ; preds = %do.end508
  %arrayidx510 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %247 = load i32, i32* %arrayidx510, align 4
  %arrayidx511 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %248 = load i32, i32* %arrayidx511, align 4
  %add512 = add i32 %247, %248
  %249 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 6), align 2
  %idxprom513 = zext i8 %249 to i32
  %arrayidx514 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom513
  %250 = load i32, i32* %arrayidx514, align 4
  %add515 = add i32 %add512, %250
  %arrayidx516 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add515, i32* %arrayidx516, align 4
  %arrayidx517 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %251 = load i32, i32* %arrayidx517, align 4
  %arrayidx518 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %252 = load i32, i32* %arrayidx518, align 4
  %xor519 = xor i32 %251, %252
  %call520 = call i32 @rotr32(i32 %xor519, i32 16)
  %arrayidx521 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call520, i32* %arrayidx521, align 4
  %arrayidx522 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %253 = load i32, i32* %arrayidx522, align 4
  %arrayidx523 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %254 = load i32, i32* %arrayidx523, align 4
  %add524 = add i32 %253, %254
  %arrayidx525 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add524, i32* %arrayidx525, align 4
  %arrayidx526 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %255 = load i32, i32* %arrayidx526, align 4
  %arrayidx527 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %256 = load i32, i32* %arrayidx527, align 4
  %xor528 = xor i32 %255, %256
  %call529 = call i32 @rotr32(i32 %xor528, i32 12)
  %arrayidx530 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call529, i32* %arrayidx530, align 4
  %arrayidx531 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %257 = load i32, i32* %arrayidx531, align 4
  %arrayidx532 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %258 = load i32, i32* %arrayidx532, align 4
  %add533 = add i32 %257, %258
  %259 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 7), align 1
  %idxprom534 = zext i8 %259 to i32
  %arrayidx535 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom534
  %260 = load i32, i32* %arrayidx535, align 4
  %add536 = add i32 %add533, %260
  %arrayidx537 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add536, i32* %arrayidx537, align 4
  %arrayidx538 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %261 = load i32, i32* %arrayidx538, align 4
  %arrayidx539 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %262 = load i32, i32* %arrayidx539, align 4
  %xor540 = xor i32 %261, %262
  %call541 = call i32 @rotr32(i32 %xor540, i32 8)
  %arrayidx542 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call541, i32* %arrayidx542, align 4
  %arrayidx543 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %263 = load i32, i32* %arrayidx543, align 4
  %arrayidx544 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %264 = load i32, i32* %arrayidx544, align 4
  %add545 = add i32 %263, %264
  %arrayidx546 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add545, i32* %arrayidx546, align 4
  %arrayidx547 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %265 = load i32, i32* %arrayidx547, align 4
  %arrayidx548 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %266 = load i32, i32* %arrayidx548, align 4
  %xor549 = xor i32 %265, %266
  %call550 = call i32 @rotr32(i32 %xor549, i32 7)
  %arrayidx551 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call550, i32* %arrayidx551, align 4
  br label %do.end552

do.end552:                                        ; preds = %do.body509
  br label %do.body553

do.body553:                                       ; preds = %do.end552
  %arrayidx554 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %267 = load i32, i32* %arrayidx554, align 16
  %arrayidx555 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %268 = load i32, i32* %arrayidx555, align 4
  %add556 = add i32 %267, %268
  %269 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 8), align 8
  %idxprom557 = zext i8 %269 to i32
  %arrayidx558 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom557
  %270 = load i32, i32* %arrayidx558, align 4
  %add559 = add i32 %add556, %270
  %arrayidx560 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add559, i32* %arrayidx560, align 16
  %arrayidx561 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %271 = load i32, i32* %arrayidx561, align 4
  %arrayidx562 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %272 = load i32, i32* %arrayidx562, align 16
  %xor563 = xor i32 %271, %272
  %call564 = call i32 @rotr32(i32 %xor563, i32 16)
  %arrayidx565 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call564, i32* %arrayidx565, align 4
  %arrayidx566 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %273 = load i32, i32* %arrayidx566, align 8
  %arrayidx567 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %274 = load i32, i32* %arrayidx567, align 4
  %add568 = add i32 %273, %274
  %arrayidx569 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add568, i32* %arrayidx569, align 8
  %arrayidx570 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %275 = load i32, i32* %arrayidx570, align 4
  %arrayidx571 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %276 = load i32, i32* %arrayidx571, align 8
  %xor572 = xor i32 %275, %276
  %call573 = call i32 @rotr32(i32 %xor572, i32 12)
  %arrayidx574 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call573, i32* %arrayidx574, align 4
  %arrayidx575 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %277 = load i32, i32* %arrayidx575, align 16
  %arrayidx576 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %278 = load i32, i32* %arrayidx576, align 4
  %add577 = add i32 %277, %278
  %279 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 9), align 1
  %idxprom578 = zext i8 %279 to i32
  %arrayidx579 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom578
  %280 = load i32, i32* %arrayidx579, align 4
  %add580 = add i32 %add577, %280
  %arrayidx581 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add580, i32* %arrayidx581, align 16
  %arrayidx582 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %281 = load i32, i32* %arrayidx582, align 4
  %arrayidx583 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %282 = load i32, i32* %arrayidx583, align 16
  %xor584 = xor i32 %281, %282
  %call585 = call i32 @rotr32(i32 %xor584, i32 8)
  %arrayidx586 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call585, i32* %arrayidx586, align 4
  %arrayidx587 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %283 = load i32, i32* %arrayidx587, align 8
  %arrayidx588 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %284 = load i32, i32* %arrayidx588, align 4
  %add589 = add i32 %283, %284
  %arrayidx590 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add589, i32* %arrayidx590, align 8
  %arrayidx591 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %285 = load i32, i32* %arrayidx591, align 4
  %arrayidx592 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %286 = load i32, i32* %arrayidx592, align 8
  %xor593 = xor i32 %285, %286
  %call594 = call i32 @rotr32(i32 %xor593, i32 7)
  %arrayidx595 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call594, i32* %arrayidx595, align 4
  br label %do.end596

do.end596:                                        ; preds = %do.body553
  br label %do.body597

do.body597:                                       ; preds = %do.end596
  %arrayidx598 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %287 = load i32, i32* %arrayidx598, align 4
  %arrayidx599 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %288 = load i32, i32* %arrayidx599, align 8
  %add600 = add i32 %287, %288
  %289 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 10), align 2
  %idxprom601 = zext i8 %289 to i32
  %arrayidx602 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom601
  %290 = load i32, i32* %arrayidx602, align 4
  %add603 = add i32 %add600, %290
  %arrayidx604 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add603, i32* %arrayidx604, align 4
  %arrayidx605 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %291 = load i32, i32* %arrayidx605, align 16
  %arrayidx606 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %292 = load i32, i32* %arrayidx606, align 4
  %xor607 = xor i32 %291, %292
  %call608 = call i32 @rotr32(i32 %xor607, i32 16)
  %arrayidx609 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call608, i32* %arrayidx609, align 16
  %arrayidx610 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %293 = load i32, i32* %arrayidx610, align 4
  %arrayidx611 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %294 = load i32, i32* %arrayidx611, align 16
  %add612 = add i32 %293, %294
  %arrayidx613 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add612, i32* %arrayidx613, align 4
  %arrayidx614 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %295 = load i32, i32* %arrayidx614, align 8
  %arrayidx615 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %296 = load i32, i32* %arrayidx615, align 4
  %xor616 = xor i32 %295, %296
  %call617 = call i32 @rotr32(i32 %xor616, i32 12)
  %arrayidx618 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call617, i32* %arrayidx618, align 8
  %arrayidx619 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %297 = load i32, i32* %arrayidx619, align 4
  %arrayidx620 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %298 = load i32, i32* %arrayidx620, align 8
  %add621 = add i32 %297, %298
  %299 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 11), align 1
  %idxprom622 = zext i8 %299 to i32
  %arrayidx623 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom622
  %300 = load i32, i32* %arrayidx623, align 4
  %add624 = add i32 %add621, %300
  %arrayidx625 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add624, i32* %arrayidx625, align 4
  %arrayidx626 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %301 = load i32, i32* %arrayidx626, align 16
  %arrayidx627 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %302 = load i32, i32* %arrayidx627, align 4
  %xor628 = xor i32 %301, %302
  %call629 = call i32 @rotr32(i32 %xor628, i32 8)
  %arrayidx630 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call629, i32* %arrayidx630, align 16
  %arrayidx631 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %303 = load i32, i32* %arrayidx631, align 4
  %arrayidx632 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %304 = load i32, i32* %arrayidx632, align 16
  %add633 = add i32 %303, %304
  %arrayidx634 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add633, i32* %arrayidx634, align 4
  %arrayidx635 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %305 = load i32, i32* %arrayidx635, align 8
  %arrayidx636 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %306 = load i32, i32* %arrayidx636, align 4
  %xor637 = xor i32 %305, %306
  %call638 = call i32 @rotr32(i32 %xor637, i32 7)
  %arrayidx639 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call638, i32* %arrayidx639, align 8
  br label %do.end640

do.end640:                                        ; preds = %do.body597
  br label %do.body641

do.body641:                                       ; preds = %do.end640
  %arrayidx642 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %307 = load i32, i32* %arrayidx642, align 8
  %arrayidx643 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %308 = load i32, i32* %arrayidx643, align 4
  %add644 = add i32 %307, %308
  %309 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 12), align 4
  %idxprom645 = zext i8 %309 to i32
  %arrayidx646 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom645
  %310 = load i32, i32* %arrayidx646, align 4
  %add647 = add i32 %add644, %310
  %arrayidx648 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add647, i32* %arrayidx648, align 8
  %arrayidx649 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %311 = load i32, i32* %arrayidx649, align 4
  %arrayidx650 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %312 = load i32, i32* %arrayidx650, align 8
  %xor651 = xor i32 %311, %312
  %call652 = call i32 @rotr32(i32 %xor651, i32 16)
  %arrayidx653 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call652, i32* %arrayidx653, align 4
  %arrayidx654 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %313 = load i32, i32* %arrayidx654, align 16
  %arrayidx655 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %314 = load i32, i32* %arrayidx655, align 4
  %add656 = add i32 %313, %314
  %arrayidx657 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add656, i32* %arrayidx657, align 16
  %arrayidx658 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %315 = load i32, i32* %arrayidx658, align 4
  %arrayidx659 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %316 = load i32, i32* %arrayidx659, align 16
  %xor660 = xor i32 %315, %316
  %call661 = call i32 @rotr32(i32 %xor660, i32 12)
  %arrayidx662 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call661, i32* %arrayidx662, align 4
  %arrayidx663 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %317 = load i32, i32* %arrayidx663, align 8
  %arrayidx664 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %318 = load i32, i32* %arrayidx664, align 4
  %add665 = add i32 %317, %318
  %319 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 13), align 1
  %idxprom666 = zext i8 %319 to i32
  %arrayidx667 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom666
  %320 = load i32, i32* %arrayidx667, align 4
  %add668 = add i32 %add665, %320
  %arrayidx669 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add668, i32* %arrayidx669, align 8
  %arrayidx670 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %321 = load i32, i32* %arrayidx670, align 4
  %arrayidx671 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %322 = load i32, i32* %arrayidx671, align 8
  %xor672 = xor i32 %321, %322
  %call673 = call i32 @rotr32(i32 %xor672, i32 8)
  %arrayidx674 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call673, i32* %arrayidx674, align 4
  %arrayidx675 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %323 = load i32, i32* %arrayidx675, align 16
  %arrayidx676 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %324 = load i32, i32* %arrayidx676, align 4
  %add677 = add i32 %323, %324
  %arrayidx678 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add677, i32* %arrayidx678, align 16
  %arrayidx679 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %325 = load i32, i32* %arrayidx679, align 4
  %arrayidx680 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %326 = load i32, i32* %arrayidx680, align 16
  %xor681 = xor i32 %325, %326
  %call682 = call i32 @rotr32(i32 %xor681, i32 7)
  %arrayidx683 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call682, i32* %arrayidx683, align 4
  br label %do.end684

do.end684:                                        ; preds = %do.body641
  br label %do.body685

do.body685:                                       ; preds = %do.end684
  %arrayidx686 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %327 = load i32, i32* %arrayidx686, align 4
  %arrayidx687 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %328 = load i32, i32* %arrayidx687, align 16
  %add688 = add i32 %327, %328
  %329 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 14), align 2
  %idxprom689 = zext i8 %329 to i32
  %arrayidx690 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom689
  %330 = load i32, i32* %arrayidx690, align 4
  %add691 = add i32 %add688, %330
  %arrayidx692 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add691, i32* %arrayidx692, align 4
  %arrayidx693 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %331 = load i32, i32* %arrayidx693, align 8
  %arrayidx694 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %332 = load i32, i32* %arrayidx694, align 4
  %xor695 = xor i32 %331, %332
  %call696 = call i32 @rotr32(i32 %xor695, i32 16)
  %arrayidx697 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call696, i32* %arrayidx697, align 8
  %arrayidx698 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %333 = load i32, i32* %arrayidx698, align 4
  %arrayidx699 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %334 = load i32, i32* %arrayidx699, align 8
  %add700 = add i32 %333, %334
  %arrayidx701 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add700, i32* %arrayidx701, align 4
  %arrayidx702 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %335 = load i32, i32* %arrayidx702, align 16
  %arrayidx703 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %336 = load i32, i32* %arrayidx703, align 4
  %xor704 = xor i32 %335, %336
  %call705 = call i32 @rotr32(i32 %xor704, i32 12)
  %arrayidx706 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call705, i32* %arrayidx706, align 16
  %arrayidx707 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %337 = load i32, i32* %arrayidx707, align 4
  %arrayidx708 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %338 = load i32, i32* %arrayidx708, align 16
  %add709 = add i32 %337, %338
  %339 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 1, i32 15), align 1
  %idxprom710 = zext i8 %339 to i32
  %arrayidx711 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom710
  %340 = load i32, i32* %arrayidx711, align 4
  %add712 = add i32 %add709, %340
  %arrayidx713 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add712, i32* %arrayidx713, align 4
  %arrayidx714 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %341 = load i32, i32* %arrayidx714, align 8
  %arrayidx715 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %342 = load i32, i32* %arrayidx715, align 4
  %xor716 = xor i32 %341, %342
  %call717 = call i32 @rotr32(i32 %xor716, i32 8)
  %arrayidx718 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call717, i32* %arrayidx718, align 8
  %arrayidx719 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %343 = load i32, i32* %arrayidx719, align 4
  %arrayidx720 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %344 = load i32, i32* %arrayidx720, align 8
  %add721 = add i32 %343, %344
  %arrayidx722 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add721, i32* %arrayidx722, align 4
  %arrayidx723 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %345 = load i32, i32* %arrayidx723, align 16
  %arrayidx724 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %346 = load i32, i32* %arrayidx724, align 4
  %xor725 = xor i32 %345, %346
  %call726 = call i32 @rotr32(i32 %xor725, i32 7)
  %arrayidx727 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call726, i32* %arrayidx727, align 16
  br label %do.end728

do.end728:                                        ; preds = %do.body685
  br label %do.end729

do.end729:                                        ; preds = %do.end728
  br label %do.body730

do.body730:                                       ; preds = %do.end729
  br label %do.body731

do.body731:                                       ; preds = %do.body730
  %arrayidx732 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %347 = load i32, i32* %arrayidx732, align 16
  %arrayidx733 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %348 = load i32, i32* %arrayidx733, align 16
  %add734 = add i32 %347, %348
  %349 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 0), align 16
  %idxprom735 = zext i8 %349 to i32
  %arrayidx736 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom735
  %350 = load i32, i32* %arrayidx736, align 4
  %add737 = add i32 %add734, %350
  %arrayidx738 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add737, i32* %arrayidx738, align 16
  %arrayidx739 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %351 = load i32, i32* %arrayidx739, align 16
  %arrayidx740 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %352 = load i32, i32* %arrayidx740, align 16
  %xor741 = xor i32 %351, %352
  %call742 = call i32 @rotr32(i32 %xor741, i32 16)
  %arrayidx743 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call742, i32* %arrayidx743, align 16
  %arrayidx744 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %353 = load i32, i32* %arrayidx744, align 16
  %arrayidx745 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %354 = load i32, i32* %arrayidx745, align 16
  %add746 = add i32 %353, %354
  %arrayidx747 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add746, i32* %arrayidx747, align 16
  %arrayidx748 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %355 = load i32, i32* %arrayidx748, align 16
  %arrayidx749 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %356 = load i32, i32* %arrayidx749, align 16
  %xor750 = xor i32 %355, %356
  %call751 = call i32 @rotr32(i32 %xor750, i32 12)
  %arrayidx752 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call751, i32* %arrayidx752, align 16
  %arrayidx753 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %357 = load i32, i32* %arrayidx753, align 16
  %arrayidx754 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %358 = load i32, i32* %arrayidx754, align 16
  %add755 = add i32 %357, %358
  %359 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 1), align 1
  %idxprom756 = zext i8 %359 to i32
  %arrayidx757 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom756
  %360 = load i32, i32* %arrayidx757, align 4
  %add758 = add i32 %add755, %360
  %arrayidx759 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add758, i32* %arrayidx759, align 16
  %arrayidx760 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %361 = load i32, i32* %arrayidx760, align 16
  %arrayidx761 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %362 = load i32, i32* %arrayidx761, align 16
  %xor762 = xor i32 %361, %362
  %call763 = call i32 @rotr32(i32 %xor762, i32 8)
  %arrayidx764 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call763, i32* %arrayidx764, align 16
  %arrayidx765 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %363 = load i32, i32* %arrayidx765, align 16
  %arrayidx766 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %364 = load i32, i32* %arrayidx766, align 16
  %add767 = add i32 %363, %364
  %arrayidx768 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add767, i32* %arrayidx768, align 16
  %arrayidx769 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %365 = load i32, i32* %arrayidx769, align 16
  %arrayidx770 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %366 = load i32, i32* %arrayidx770, align 16
  %xor771 = xor i32 %365, %366
  %call772 = call i32 @rotr32(i32 %xor771, i32 7)
  %arrayidx773 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call772, i32* %arrayidx773, align 16
  br label %do.end774

do.end774:                                        ; preds = %do.body731
  br label %do.body775

do.body775:                                       ; preds = %do.end774
  %arrayidx776 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %367 = load i32, i32* %arrayidx776, align 4
  %arrayidx777 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %368 = load i32, i32* %arrayidx777, align 4
  %add778 = add i32 %367, %368
  %369 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 2), align 2
  %idxprom779 = zext i8 %369 to i32
  %arrayidx780 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom779
  %370 = load i32, i32* %arrayidx780, align 4
  %add781 = add i32 %add778, %370
  %arrayidx782 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add781, i32* %arrayidx782, align 4
  %arrayidx783 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %371 = load i32, i32* %arrayidx783, align 4
  %arrayidx784 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %372 = load i32, i32* %arrayidx784, align 4
  %xor785 = xor i32 %371, %372
  %call786 = call i32 @rotr32(i32 %xor785, i32 16)
  %arrayidx787 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call786, i32* %arrayidx787, align 4
  %arrayidx788 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %373 = load i32, i32* %arrayidx788, align 4
  %arrayidx789 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %374 = load i32, i32* %arrayidx789, align 4
  %add790 = add i32 %373, %374
  %arrayidx791 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add790, i32* %arrayidx791, align 4
  %arrayidx792 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %375 = load i32, i32* %arrayidx792, align 4
  %arrayidx793 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %376 = load i32, i32* %arrayidx793, align 4
  %xor794 = xor i32 %375, %376
  %call795 = call i32 @rotr32(i32 %xor794, i32 12)
  %arrayidx796 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call795, i32* %arrayidx796, align 4
  %arrayidx797 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %377 = load i32, i32* %arrayidx797, align 4
  %arrayidx798 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %378 = load i32, i32* %arrayidx798, align 4
  %add799 = add i32 %377, %378
  %379 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 3), align 1
  %idxprom800 = zext i8 %379 to i32
  %arrayidx801 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom800
  %380 = load i32, i32* %arrayidx801, align 4
  %add802 = add i32 %add799, %380
  %arrayidx803 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add802, i32* %arrayidx803, align 4
  %arrayidx804 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %381 = load i32, i32* %arrayidx804, align 4
  %arrayidx805 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %382 = load i32, i32* %arrayidx805, align 4
  %xor806 = xor i32 %381, %382
  %call807 = call i32 @rotr32(i32 %xor806, i32 8)
  %arrayidx808 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call807, i32* %arrayidx808, align 4
  %arrayidx809 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %383 = load i32, i32* %arrayidx809, align 4
  %arrayidx810 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %384 = load i32, i32* %arrayidx810, align 4
  %add811 = add i32 %383, %384
  %arrayidx812 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add811, i32* %arrayidx812, align 4
  %arrayidx813 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %385 = load i32, i32* %arrayidx813, align 4
  %arrayidx814 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %386 = load i32, i32* %arrayidx814, align 4
  %xor815 = xor i32 %385, %386
  %call816 = call i32 @rotr32(i32 %xor815, i32 7)
  %arrayidx817 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call816, i32* %arrayidx817, align 4
  br label %do.end818

do.end818:                                        ; preds = %do.body775
  br label %do.body819

do.body819:                                       ; preds = %do.end818
  %arrayidx820 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %387 = load i32, i32* %arrayidx820, align 8
  %arrayidx821 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %388 = load i32, i32* %arrayidx821, align 8
  %add822 = add i32 %387, %388
  %389 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 4), align 4
  %idxprom823 = zext i8 %389 to i32
  %arrayidx824 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom823
  %390 = load i32, i32* %arrayidx824, align 4
  %add825 = add i32 %add822, %390
  %arrayidx826 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add825, i32* %arrayidx826, align 8
  %arrayidx827 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %391 = load i32, i32* %arrayidx827, align 8
  %arrayidx828 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %392 = load i32, i32* %arrayidx828, align 8
  %xor829 = xor i32 %391, %392
  %call830 = call i32 @rotr32(i32 %xor829, i32 16)
  %arrayidx831 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call830, i32* %arrayidx831, align 8
  %arrayidx832 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %393 = load i32, i32* %arrayidx832, align 8
  %arrayidx833 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %394 = load i32, i32* %arrayidx833, align 8
  %add834 = add i32 %393, %394
  %arrayidx835 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add834, i32* %arrayidx835, align 8
  %arrayidx836 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %395 = load i32, i32* %arrayidx836, align 8
  %arrayidx837 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %396 = load i32, i32* %arrayidx837, align 8
  %xor838 = xor i32 %395, %396
  %call839 = call i32 @rotr32(i32 %xor838, i32 12)
  %arrayidx840 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call839, i32* %arrayidx840, align 8
  %arrayidx841 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %397 = load i32, i32* %arrayidx841, align 8
  %arrayidx842 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %398 = load i32, i32* %arrayidx842, align 8
  %add843 = add i32 %397, %398
  %399 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 5), align 1
  %idxprom844 = zext i8 %399 to i32
  %arrayidx845 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom844
  %400 = load i32, i32* %arrayidx845, align 4
  %add846 = add i32 %add843, %400
  %arrayidx847 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add846, i32* %arrayidx847, align 8
  %arrayidx848 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %401 = load i32, i32* %arrayidx848, align 8
  %arrayidx849 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %402 = load i32, i32* %arrayidx849, align 8
  %xor850 = xor i32 %401, %402
  %call851 = call i32 @rotr32(i32 %xor850, i32 8)
  %arrayidx852 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call851, i32* %arrayidx852, align 8
  %arrayidx853 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %403 = load i32, i32* %arrayidx853, align 8
  %arrayidx854 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %404 = load i32, i32* %arrayidx854, align 8
  %add855 = add i32 %403, %404
  %arrayidx856 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add855, i32* %arrayidx856, align 8
  %arrayidx857 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %405 = load i32, i32* %arrayidx857, align 8
  %arrayidx858 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %406 = load i32, i32* %arrayidx858, align 8
  %xor859 = xor i32 %405, %406
  %call860 = call i32 @rotr32(i32 %xor859, i32 7)
  %arrayidx861 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call860, i32* %arrayidx861, align 8
  br label %do.end862

do.end862:                                        ; preds = %do.body819
  br label %do.body863

do.body863:                                       ; preds = %do.end862
  %arrayidx864 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %407 = load i32, i32* %arrayidx864, align 4
  %arrayidx865 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %408 = load i32, i32* %arrayidx865, align 4
  %add866 = add i32 %407, %408
  %409 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 6), align 2
  %idxprom867 = zext i8 %409 to i32
  %arrayidx868 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom867
  %410 = load i32, i32* %arrayidx868, align 4
  %add869 = add i32 %add866, %410
  %arrayidx870 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add869, i32* %arrayidx870, align 4
  %arrayidx871 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %411 = load i32, i32* %arrayidx871, align 4
  %arrayidx872 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %412 = load i32, i32* %arrayidx872, align 4
  %xor873 = xor i32 %411, %412
  %call874 = call i32 @rotr32(i32 %xor873, i32 16)
  %arrayidx875 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call874, i32* %arrayidx875, align 4
  %arrayidx876 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %413 = load i32, i32* %arrayidx876, align 4
  %arrayidx877 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %414 = load i32, i32* %arrayidx877, align 4
  %add878 = add i32 %413, %414
  %arrayidx879 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add878, i32* %arrayidx879, align 4
  %arrayidx880 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %415 = load i32, i32* %arrayidx880, align 4
  %arrayidx881 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %416 = load i32, i32* %arrayidx881, align 4
  %xor882 = xor i32 %415, %416
  %call883 = call i32 @rotr32(i32 %xor882, i32 12)
  %arrayidx884 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call883, i32* %arrayidx884, align 4
  %arrayidx885 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %417 = load i32, i32* %arrayidx885, align 4
  %arrayidx886 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %418 = load i32, i32* %arrayidx886, align 4
  %add887 = add i32 %417, %418
  %419 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 7), align 1
  %idxprom888 = zext i8 %419 to i32
  %arrayidx889 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom888
  %420 = load i32, i32* %arrayidx889, align 4
  %add890 = add i32 %add887, %420
  %arrayidx891 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add890, i32* %arrayidx891, align 4
  %arrayidx892 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %421 = load i32, i32* %arrayidx892, align 4
  %arrayidx893 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %422 = load i32, i32* %arrayidx893, align 4
  %xor894 = xor i32 %421, %422
  %call895 = call i32 @rotr32(i32 %xor894, i32 8)
  %arrayidx896 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call895, i32* %arrayidx896, align 4
  %arrayidx897 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %423 = load i32, i32* %arrayidx897, align 4
  %arrayidx898 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %424 = load i32, i32* %arrayidx898, align 4
  %add899 = add i32 %423, %424
  %arrayidx900 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add899, i32* %arrayidx900, align 4
  %arrayidx901 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %425 = load i32, i32* %arrayidx901, align 4
  %arrayidx902 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %426 = load i32, i32* %arrayidx902, align 4
  %xor903 = xor i32 %425, %426
  %call904 = call i32 @rotr32(i32 %xor903, i32 7)
  %arrayidx905 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call904, i32* %arrayidx905, align 4
  br label %do.end906

do.end906:                                        ; preds = %do.body863
  br label %do.body907

do.body907:                                       ; preds = %do.end906
  %arrayidx908 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %427 = load i32, i32* %arrayidx908, align 16
  %arrayidx909 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %428 = load i32, i32* %arrayidx909, align 4
  %add910 = add i32 %427, %428
  %429 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 8), align 8
  %idxprom911 = zext i8 %429 to i32
  %arrayidx912 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom911
  %430 = load i32, i32* %arrayidx912, align 4
  %add913 = add i32 %add910, %430
  %arrayidx914 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add913, i32* %arrayidx914, align 16
  %arrayidx915 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %431 = load i32, i32* %arrayidx915, align 4
  %arrayidx916 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %432 = load i32, i32* %arrayidx916, align 16
  %xor917 = xor i32 %431, %432
  %call918 = call i32 @rotr32(i32 %xor917, i32 16)
  %arrayidx919 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call918, i32* %arrayidx919, align 4
  %arrayidx920 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %433 = load i32, i32* %arrayidx920, align 8
  %arrayidx921 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %434 = load i32, i32* %arrayidx921, align 4
  %add922 = add i32 %433, %434
  %arrayidx923 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add922, i32* %arrayidx923, align 8
  %arrayidx924 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %435 = load i32, i32* %arrayidx924, align 4
  %arrayidx925 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %436 = load i32, i32* %arrayidx925, align 8
  %xor926 = xor i32 %435, %436
  %call927 = call i32 @rotr32(i32 %xor926, i32 12)
  %arrayidx928 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call927, i32* %arrayidx928, align 4
  %arrayidx929 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %437 = load i32, i32* %arrayidx929, align 16
  %arrayidx930 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %438 = load i32, i32* %arrayidx930, align 4
  %add931 = add i32 %437, %438
  %439 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 9), align 1
  %idxprom932 = zext i8 %439 to i32
  %arrayidx933 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom932
  %440 = load i32, i32* %arrayidx933, align 4
  %add934 = add i32 %add931, %440
  %arrayidx935 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add934, i32* %arrayidx935, align 16
  %arrayidx936 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %441 = load i32, i32* %arrayidx936, align 4
  %arrayidx937 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %442 = load i32, i32* %arrayidx937, align 16
  %xor938 = xor i32 %441, %442
  %call939 = call i32 @rotr32(i32 %xor938, i32 8)
  %arrayidx940 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call939, i32* %arrayidx940, align 4
  %arrayidx941 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %443 = load i32, i32* %arrayidx941, align 8
  %arrayidx942 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %444 = load i32, i32* %arrayidx942, align 4
  %add943 = add i32 %443, %444
  %arrayidx944 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add943, i32* %arrayidx944, align 8
  %arrayidx945 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %445 = load i32, i32* %arrayidx945, align 4
  %arrayidx946 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %446 = load i32, i32* %arrayidx946, align 8
  %xor947 = xor i32 %445, %446
  %call948 = call i32 @rotr32(i32 %xor947, i32 7)
  %arrayidx949 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call948, i32* %arrayidx949, align 4
  br label %do.end950

do.end950:                                        ; preds = %do.body907
  br label %do.body951

do.body951:                                       ; preds = %do.end950
  %arrayidx952 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %447 = load i32, i32* %arrayidx952, align 4
  %arrayidx953 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %448 = load i32, i32* %arrayidx953, align 8
  %add954 = add i32 %447, %448
  %449 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 10), align 2
  %idxprom955 = zext i8 %449 to i32
  %arrayidx956 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom955
  %450 = load i32, i32* %arrayidx956, align 4
  %add957 = add i32 %add954, %450
  %arrayidx958 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add957, i32* %arrayidx958, align 4
  %arrayidx959 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %451 = load i32, i32* %arrayidx959, align 16
  %arrayidx960 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %452 = load i32, i32* %arrayidx960, align 4
  %xor961 = xor i32 %451, %452
  %call962 = call i32 @rotr32(i32 %xor961, i32 16)
  %arrayidx963 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call962, i32* %arrayidx963, align 16
  %arrayidx964 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %453 = load i32, i32* %arrayidx964, align 4
  %arrayidx965 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %454 = load i32, i32* %arrayidx965, align 16
  %add966 = add i32 %453, %454
  %arrayidx967 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add966, i32* %arrayidx967, align 4
  %arrayidx968 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %455 = load i32, i32* %arrayidx968, align 8
  %arrayidx969 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %456 = load i32, i32* %arrayidx969, align 4
  %xor970 = xor i32 %455, %456
  %call971 = call i32 @rotr32(i32 %xor970, i32 12)
  %arrayidx972 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call971, i32* %arrayidx972, align 8
  %arrayidx973 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %457 = load i32, i32* %arrayidx973, align 4
  %arrayidx974 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %458 = load i32, i32* %arrayidx974, align 8
  %add975 = add i32 %457, %458
  %459 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 11), align 1
  %idxprom976 = zext i8 %459 to i32
  %arrayidx977 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom976
  %460 = load i32, i32* %arrayidx977, align 4
  %add978 = add i32 %add975, %460
  %arrayidx979 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add978, i32* %arrayidx979, align 4
  %arrayidx980 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %461 = load i32, i32* %arrayidx980, align 16
  %arrayidx981 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %462 = load i32, i32* %arrayidx981, align 4
  %xor982 = xor i32 %461, %462
  %call983 = call i32 @rotr32(i32 %xor982, i32 8)
  %arrayidx984 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call983, i32* %arrayidx984, align 16
  %arrayidx985 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %463 = load i32, i32* %arrayidx985, align 4
  %arrayidx986 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %464 = load i32, i32* %arrayidx986, align 16
  %add987 = add i32 %463, %464
  %arrayidx988 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add987, i32* %arrayidx988, align 4
  %arrayidx989 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %465 = load i32, i32* %arrayidx989, align 8
  %arrayidx990 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %466 = load i32, i32* %arrayidx990, align 4
  %xor991 = xor i32 %465, %466
  %call992 = call i32 @rotr32(i32 %xor991, i32 7)
  %arrayidx993 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call992, i32* %arrayidx993, align 8
  br label %do.end994

do.end994:                                        ; preds = %do.body951
  br label %do.body995

do.body995:                                       ; preds = %do.end994
  %arrayidx996 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %467 = load i32, i32* %arrayidx996, align 8
  %arrayidx997 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %468 = load i32, i32* %arrayidx997, align 4
  %add998 = add i32 %467, %468
  %469 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 12), align 4
  %idxprom999 = zext i8 %469 to i32
  %arrayidx1000 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom999
  %470 = load i32, i32* %arrayidx1000, align 4
  %add1001 = add i32 %add998, %470
  %arrayidx1002 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1001, i32* %arrayidx1002, align 8
  %arrayidx1003 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %471 = load i32, i32* %arrayidx1003, align 4
  %arrayidx1004 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %472 = load i32, i32* %arrayidx1004, align 8
  %xor1005 = xor i32 %471, %472
  %call1006 = call i32 @rotr32(i32 %xor1005, i32 16)
  %arrayidx1007 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1006, i32* %arrayidx1007, align 4
  %arrayidx1008 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %473 = load i32, i32* %arrayidx1008, align 16
  %arrayidx1009 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %474 = load i32, i32* %arrayidx1009, align 4
  %add1010 = add i32 %473, %474
  %arrayidx1011 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1010, i32* %arrayidx1011, align 16
  %arrayidx1012 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %475 = load i32, i32* %arrayidx1012, align 4
  %arrayidx1013 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %476 = load i32, i32* %arrayidx1013, align 16
  %xor1014 = xor i32 %475, %476
  %call1015 = call i32 @rotr32(i32 %xor1014, i32 12)
  %arrayidx1016 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1015, i32* %arrayidx1016, align 4
  %arrayidx1017 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %477 = load i32, i32* %arrayidx1017, align 8
  %arrayidx1018 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %478 = load i32, i32* %arrayidx1018, align 4
  %add1019 = add i32 %477, %478
  %479 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 13), align 1
  %idxprom1020 = zext i8 %479 to i32
  %arrayidx1021 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1020
  %480 = load i32, i32* %arrayidx1021, align 4
  %add1022 = add i32 %add1019, %480
  %arrayidx1023 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1022, i32* %arrayidx1023, align 8
  %arrayidx1024 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %481 = load i32, i32* %arrayidx1024, align 4
  %arrayidx1025 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %482 = load i32, i32* %arrayidx1025, align 8
  %xor1026 = xor i32 %481, %482
  %call1027 = call i32 @rotr32(i32 %xor1026, i32 8)
  %arrayidx1028 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1027, i32* %arrayidx1028, align 4
  %arrayidx1029 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %483 = load i32, i32* %arrayidx1029, align 16
  %arrayidx1030 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %484 = load i32, i32* %arrayidx1030, align 4
  %add1031 = add i32 %483, %484
  %arrayidx1032 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1031, i32* %arrayidx1032, align 16
  %arrayidx1033 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %485 = load i32, i32* %arrayidx1033, align 4
  %arrayidx1034 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %486 = load i32, i32* %arrayidx1034, align 16
  %xor1035 = xor i32 %485, %486
  %call1036 = call i32 @rotr32(i32 %xor1035, i32 7)
  %arrayidx1037 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1036, i32* %arrayidx1037, align 4
  br label %do.end1038

do.end1038:                                       ; preds = %do.body995
  br label %do.body1039

do.body1039:                                      ; preds = %do.end1038
  %arrayidx1040 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %487 = load i32, i32* %arrayidx1040, align 4
  %arrayidx1041 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %488 = load i32, i32* %arrayidx1041, align 16
  %add1042 = add i32 %487, %488
  %489 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 14), align 2
  %idxprom1043 = zext i8 %489 to i32
  %arrayidx1044 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1043
  %490 = load i32, i32* %arrayidx1044, align 4
  %add1045 = add i32 %add1042, %490
  %arrayidx1046 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1045, i32* %arrayidx1046, align 4
  %arrayidx1047 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %491 = load i32, i32* %arrayidx1047, align 8
  %arrayidx1048 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %492 = load i32, i32* %arrayidx1048, align 4
  %xor1049 = xor i32 %491, %492
  %call1050 = call i32 @rotr32(i32 %xor1049, i32 16)
  %arrayidx1051 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1050, i32* %arrayidx1051, align 8
  %arrayidx1052 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %493 = load i32, i32* %arrayidx1052, align 4
  %arrayidx1053 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %494 = load i32, i32* %arrayidx1053, align 8
  %add1054 = add i32 %493, %494
  %arrayidx1055 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1054, i32* %arrayidx1055, align 4
  %arrayidx1056 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %495 = load i32, i32* %arrayidx1056, align 16
  %arrayidx1057 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %496 = load i32, i32* %arrayidx1057, align 4
  %xor1058 = xor i32 %495, %496
  %call1059 = call i32 @rotr32(i32 %xor1058, i32 12)
  %arrayidx1060 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1059, i32* %arrayidx1060, align 16
  %arrayidx1061 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %497 = load i32, i32* %arrayidx1061, align 4
  %arrayidx1062 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %498 = load i32, i32* %arrayidx1062, align 16
  %add1063 = add i32 %497, %498
  %499 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 2, i32 15), align 1
  %idxprom1064 = zext i8 %499 to i32
  %arrayidx1065 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1064
  %500 = load i32, i32* %arrayidx1065, align 4
  %add1066 = add i32 %add1063, %500
  %arrayidx1067 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1066, i32* %arrayidx1067, align 4
  %arrayidx1068 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %501 = load i32, i32* %arrayidx1068, align 8
  %arrayidx1069 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %502 = load i32, i32* %arrayidx1069, align 4
  %xor1070 = xor i32 %501, %502
  %call1071 = call i32 @rotr32(i32 %xor1070, i32 8)
  %arrayidx1072 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1071, i32* %arrayidx1072, align 8
  %arrayidx1073 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %503 = load i32, i32* %arrayidx1073, align 4
  %arrayidx1074 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %504 = load i32, i32* %arrayidx1074, align 8
  %add1075 = add i32 %503, %504
  %arrayidx1076 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1075, i32* %arrayidx1076, align 4
  %arrayidx1077 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %505 = load i32, i32* %arrayidx1077, align 16
  %arrayidx1078 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %506 = load i32, i32* %arrayidx1078, align 4
  %xor1079 = xor i32 %505, %506
  %call1080 = call i32 @rotr32(i32 %xor1079, i32 7)
  %arrayidx1081 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1080, i32* %arrayidx1081, align 16
  br label %do.end1082

do.end1082:                                       ; preds = %do.body1039
  br label %do.end1083

do.end1083:                                       ; preds = %do.end1082
  br label %do.body1084

do.body1084:                                      ; preds = %do.end1083
  br label %do.body1085

do.body1085:                                      ; preds = %do.body1084
  %arrayidx1086 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %507 = load i32, i32* %arrayidx1086, align 16
  %arrayidx1087 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %508 = load i32, i32* %arrayidx1087, align 16
  %add1088 = add i32 %507, %508
  %509 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 0), align 16
  %idxprom1089 = zext i8 %509 to i32
  %arrayidx1090 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1089
  %510 = load i32, i32* %arrayidx1090, align 4
  %add1091 = add i32 %add1088, %510
  %arrayidx1092 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1091, i32* %arrayidx1092, align 16
  %arrayidx1093 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %511 = load i32, i32* %arrayidx1093, align 16
  %arrayidx1094 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %512 = load i32, i32* %arrayidx1094, align 16
  %xor1095 = xor i32 %511, %512
  %call1096 = call i32 @rotr32(i32 %xor1095, i32 16)
  %arrayidx1097 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1096, i32* %arrayidx1097, align 16
  %arrayidx1098 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %513 = load i32, i32* %arrayidx1098, align 16
  %arrayidx1099 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %514 = load i32, i32* %arrayidx1099, align 16
  %add1100 = add i32 %513, %514
  %arrayidx1101 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1100, i32* %arrayidx1101, align 16
  %arrayidx1102 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %515 = load i32, i32* %arrayidx1102, align 16
  %arrayidx1103 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %516 = load i32, i32* %arrayidx1103, align 16
  %xor1104 = xor i32 %515, %516
  %call1105 = call i32 @rotr32(i32 %xor1104, i32 12)
  %arrayidx1106 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1105, i32* %arrayidx1106, align 16
  %arrayidx1107 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %517 = load i32, i32* %arrayidx1107, align 16
  %arrayidx1108 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %518 = load i32, i32* %arrayidx1108, align 16
  %add1109 = add i32 %517, %518
  %519 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 1), align 1
  %idxprom1110 = zext i8 %519 to i32
  %arrayidx1111 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1110
  %520 = load i32, i32* %arrayidx1111, align 4
  %add1112 = add i32 %add1109, %520
  %arrayidx1113 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1112, i32* %arrayidx1113, align 16
  %arrayidx1114 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %521 = load i32, i32* %arrayidx1114, align 16
  %arrayidx1115 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %522 = load i32, i32* %arrayidx1115, align 16
  %xor1116 = xor i32 %521, %522
  %call1117 = call i32 @rotr32(i32 %xor1116, i32 8)
  %arrayidx1118 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1117, i32* %arrayidx1118, align 16
  %arrayidx1119 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %523 = load i32, i32* %arrayidx1119, align 16
  %arrayidx1120 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %524 = load i32, i32* %arrayidx1120, align 16
  %add1121 = add i32 %523, %524
  %arrayidx1122 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1121, i32* %arrayidx1122, align 16
  %arrayidx1123 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %525 = load i32, i32* %arrayidx1123, align 16
  %arrayidx1124 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %526 = load i32, i32* %arrayidx1124, align 16
  %xor1125 = xor i32 %525, %526
  %call1126 = call i32 @rotr32(i32 %xor1125, i32 7)
  %arrayidx1127 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1126, i32* %arrayidx1127, align 16
  br label %do.end1128

do.end1128:                                       ; preds = %do.body1085
  br label %do.body1129

do.body1129:                                      ; preds = %do.end1128
  %arrayidx1130 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %527 = load i32, i32* %arrayidx1130, align 4
  %arrayidx1131 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %528 = load i32, i32* %arrayidx1131, align 4
  %add1132 = add i32 %527, %528
  %529 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 2), align 2
  %idxprom1133 = zext i8 %529 to i32
  %arrayidx1134 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1133
  %530 = load i32, i32* %arrayidx1134, align 4
  %add1135 = add i32 %add1132, %530
  %arrayidx1136 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1135, i32* %arrayidx1136, align 4
  %arrayidx1137 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %531 = load i32, i32* %arrayidx1137, align 4
  %arrayidx1138 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %532 = load i32, i32* %arrayidx1138, align 4
  %xor1139 = xor i32 %531, %532
  %call1140 = call i32 @rotr32(i32 %xor1139, i32 16)
  %arrayidx1141 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1140, i32* %arrayidx1141, align 4
  %arrayidx1142 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %533 = load i32, i32* %arrayidx1142, align 4
  %arrayidx1143 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %534 = load i32, i32* %arrayidx1143, align 4
  %add1144 = add i32 %533, %534
  %arrayidx1145 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1144, i32* %arrayidx1145, align 4
  %arrayidx1146 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %535 = load i32, i32* %arrayidx1146, align 4
  %arrayidx1147 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %536 = load i32, i32* %arrayidx1147, align 4
  %xor1148 = xor i32 %535, %536
  %call1149 = call i32 @rotr32(i32 %xor1148, i32 12)
  %arrayidx1150 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1149, i32* %arrayidx1150, align 4
  %arrayidx1151 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %537 = load i32, i32* %arrayidx1151, align 4
  %arrayidx1152 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %538 = load i32, i32* %arrayidx1152, align 4
  %add1153 = add i32 %537, %538
  %539 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 3), align 1
  %idxprom1154 = zext i8 %539 to i32
  %arrayidx1155 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1154
  %540 = load i32, i32* %arrayidx1155, align 4
  %add1156 = add i32 %add1153, %540
  %arrayidx1157 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1156, i32* %arrayidx1157, align 4
  %arrayidx1158 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %541 = load i32, i32* %arrayidx1158, align 4
  %arrayidx1159 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %542 = load i32, i32* %arrayidx1159, align 4
  %xor1160 = xor i32 %541, %542
  %call1161 = call i32 @rotr32(i32 %xor1160, i32 8)
  %arrayidx1162 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1161, i32* %arrayidx1162, align 4
  %arrayidx1163 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %543 = load i32, i32* %arrayidx1163, align 4
  %arrayidx1164 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %544 = load i32, i32* %arrayidx1164, align 4
  %add1165 = add i32 %543, %544
  %arrayidx1166 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1165, i32* %arrayidx1166, align 4
  %arrayidx1167 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %545 = load i32, i32* %arrayidx1167, align 4
  %arrayidx1168 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %546 = load i32, i32* %arrayidx1168, align 4
  %xor1169 = xor i32 %545, %546
  %call1170 = call i32 @rotr32(i32 %xor1169, i32 7)
  %arrayidx1171 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1170, i32* %arrayidx1171, align 4
  br label %do.end1172

do.end1172:                                       ; preds = %do.body1129
  br label %do.body1173

do.body1173:                                      ; preds = %do.end1172
  %arrayidx1174 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %547 = load i32, i32* %arrayidx1174, align 8
  %arrayidx1175 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %548 = load i32, i32* %arrayidx1175, align 8
  %add1176 = add i32 %547, %548
  %549 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 4), align 4
  %idxprom1177 = zext i8 %549 to i32
  %arrayidx1178 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1177
  %550 = load i32, i32* %arrayidx1178, align 4
  %add1179 = add i32 %add1176, %550
  %arrayidx1180 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1179, i32* %arrayidx1180, align 8
  %arrayidx1181 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %551 = load i32, i32* %arrayidx1181, align 8
  %arrayidx1182 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %552 = load i32, i32* %arrayidx1182, align 8
  %xor1183 = xor i32 %551, %552
  %call1184 = call i32 @rotr32(i32 %xor1183, i32 16)
  %arrayidx1185 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1184, i32* %arrayidx1185, align 8
  %arrayidx1186 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %553 = load i32, i32* %arrayidx1186, align 8
  %arrayidx1187 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %554 = load i32, i32* %arrayidx1187, align 8
  %add1188 = add i32 %553, %554
  %arrayidx1189 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1188, i32* %arrayidx1189, align 8
  %arrayidx1190 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %555 = load i32, i32* %arrayidx1190, align 8
  %arrayidx1191 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %556 = load i32, i32* %arrayidx1191, align 8
  %xor1192 = xor i32 %555, %556
  %call1193 = call i32 @rotr32(i32 %xor1192, i32 12)
  %arrayidx1194 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1193, i32* %arrayidx1194, align 8
  %arrayidx1195 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %557 = load i32, i32* %arrayidx1195, align 8
  %arrayidx1196 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %558 = load i32, i32* %arrayidx1196, align 8
  %add1197 = add i32 %557, %558
  %559 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 5), align 1
  %idxprom1198 = zext i8 %559 to i32
  %arrayidx1199 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1198
  %560 = load i32, i32* %arrayidx1199, align 4
  %add1200 = add i32 %add1197, %560
  %arrayidx1201 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1200, i32* %arrayidx1201, align 8
  %arrayidx1202 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %561 = load i32, i32* %arrayidx1202, align 8
  %arrayidx1203 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %562 = load i32, i32* %arrayidx1203, align 8
  %xor1204 = xor i32 %561, %562
  %call1205 = call i32 @rotr32(i32 %xor1204, i32 8)
  %arrayidx1206 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1205, i32* %arrayidx1206, align 8
  %arrayidx1207 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %563 = load i32, i32* %arrayidx1207, align 8
  %arrayidx1208 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %564 = load i32, i32* %arrayidx1208, align 8
  %add1209 = add i32 %563, %564
  %arrayidx1210 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1209, i32* %arrayidx1210, align 8
  %arrayidx1211 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %565 = load i32, i32* %arrayidx1211, align 8
  %arrayidx1212 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %566 = load i32, i32* %arrayidx1212, align 8
  %xor1213 = xor i32 %565, %566
  %call1214 = call i32 @rotr32(i32 %xor1213, i32 7)
  %arrayidx1215 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1214, i32* %arrayidx1215, align 8
  br label %do.end1216

do.end1216:                                       ; preds = %do.body1173
  br label %do.body1217

do.body1217:                                      ; preds = %do.end1216
  %arrayidx1218 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %567 = load i32, i32* %arrayidx1218, align 4
  %arrayidx1219 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %568 = load i32, i32* %arrayidx1219, align 4
  %add1220 = add i32 %567, %568
  %569 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 6), align 2
  %idxprom1221 = zext i8 %569 to i32
  %arrayidx1222 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1221
  %570 = load i32, i32* %arrayidx1222, align 4
  %add1223 = add i32 %add1220, %570
  %arrayidx1224 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1223, i32* %arrayidx1224, align 4
  %arrayidx1225 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %571 = load i32, i32* %arrayidx1225, align 4
  %arrayidx1226 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %572 = load i32, i32* %arrayidx1226, align 4
  %xor1227 = xor i32 %571, %572
  %call1228 = call i32 @rotr32(i32 %xor1227, i32 16)
  %arrayidx1229 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1228, i32* %arrayidx1229, align 4
  %arrayidx1230 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %573 = load i32, i32* %arrayidx1230, align 4
  %arrayidx1231 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %574 = load i32, i32* %arrayidx1231, align 4
  %add1232 = add i32 %573, %574
  %arrayidx1233 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1232, i32* %arrayidx1233, align 4
  %arrayidx1234 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %575 = load i32, i32* %arrayidx1234, align 4
  %arrayidx1235 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %576 = load i32, i32* %arrayidx1235, align 4
  %xor1236 = xor i32 %575, %576
  %call1237 = call i32 @rotr32(i32 %xor1236, i32 12)
  %arrayidx1238 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1237, i32* %arrayidx1238, align 4
  %arrayidx1239 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %577 = load i32, i32* %arrayidx1239, align 4
  %arrayidx1240 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %578 = load i32, i32* %arrayidx1240, align 4
  %add1241 = add i32 %577, %578
  %579 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 7), align 1
  %idxprom1242 = zext i8 %579 to i32
  %arrayidx1243 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1242
  %580 = load i32, i32* %arrayidx1243, align 4
  %add1244 = add i32 %add1241, %580
  %arrayidx1245 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1244, i32* %arrayidx1245, align 4
  %arrayidx1246 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %581 = load i32, i32* %arrayidx1246, align 4
  %arrayidx1247 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %582 = load i32, i32* %arrayidx1247, align 4
  %xor1248 = xor i32 %581, %582
  %call1249 = call i32 @rotr32(i32 %xor1248, i32 8)
  %arrayidx1250 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1249, i32* %arrayidx1250, align 4
  %arrayidx1251 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %583 = load i32, i32* %arrayidx1251, align 4
  %arrayidx1252 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %584 = load i32, i32* %arrayidx1252, align 4
  %add1253 = add i32 %583, %584
  %arrayidx1254 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1253, i32* %arrayidx1254, align 4
  %arrayidx1255 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %585 = load i32, i32* %arrayidx1255, align 4
  %arrayidx1256 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %586 = load i32, i32* %arrayidx1256, align 4
  %xor1257 = xor i32 %585, %586
  %call1258 = call i32 @rotr32(i32 %xor1257, i32 7)
  %arrayidx1259 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1258, i32* %arrayidx1259, align 4
  br label %do.end1260

do.end1260:                                       ; preds = %do.body1217
  br label %do.body1261

do.body1261:                                      ; preds = %do.end1260
  %arrayidx1262 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %587 = load i32, i32* %arrayidx1262, align 16
  %arrayidx1263 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %588 = load i32, i32* %arrayidx1263, align 4
  %add1264 = add i32 %587, %588
  %589 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 8), align 8
  %idxprom1265 = zext i8 %589 to i32
  %arrayidx1266 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1265
  %590 = load i32, i32* %arrayidx1266, align 4
  %add1267 = add i32 %add1264, %590
  %arrayidx1268 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1267, i32* %arrayidx1268, align 16
  %arrayidx1269 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %591 = load i32, i32* %arrayidx1269, align 4
  %arrayidx1270 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %592 = load i32, i32* %arrayidx1270, align 16
  %xor1271 = xor i32 %591, %592
  %call1272 = call i32 @rotr32(i32 %xor1271, i32 16)
  %arrayidx1273 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1272, i32* %arrayidx1273, align 4
  %arrayidx1274 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %593 = load i32, i32* %arrayidx1274, align 8
  %arrayidx1275 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %594 = load i32, i32* %arrayidx1275, align 4
  %add1276 = add i32 %593, %594
  %arrayidx1277 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1276, i32* %arrayidx1277, align 8
  %arrayidx1278 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %595 = load i32, i32* %arrayidx1278, align 4
  %arrayidx1279 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %596 = load i32, i32* %arrayidx1279, align 8
  %xor1280 = xor i32 %595, %596
  %call1281 = call i32 @rotr32(i32 %xor1280, i32 12)
  %arrayidx1282 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1281, i32* %arrayidx1282, align 4
  %arrayidx1283 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %597 = load i32, i32* %arrayidx1283, align 16
  %arrayidx1284 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %598 = load i32, i32* %arrayidx1284, align 4
  %add1285 = add i32 %597, %598
  %599 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 9), align 1
  %idxprom1286 = zext i8 %599 to i32
  %arrayidx1287 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1286
  %600 = load i32, i32* %arrayidx1287, align 4
  %add1288 = add i32 %add1285, %600
  %arrayidx1289 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1288, i32* %arrayidx1289, align 16
  %arrayidx1290 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %601 = load i32, i32* %arrayidx1290, align 4
  %arrayidx1291 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %602 = load i32, i32* %arrayidx1291, align 16
  %xor1292 = xor i32 %601, %602
  %call1293 = call i32 @rotr32(i32 %xor1292, i32 8)
  %arrayidx1294 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1293, i32* %arrayidx1294, align 4
  %arrayidx1295 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %603 = load i32, i32* %arrayidx1295, align 8
  %arrayidx1296 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %604 = load i32, i32* %arrayidx1296, align 4
  %add1297 = add i32 %603, %604
  %arrayidx1298 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1297, i32* %arrayidx1298, align 8
  %arrayidx1299 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %605 = load i32, i32* %arrayidx1299, align 4
  %arrayidx1300 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %606 = load i32, i32* %arrayidx1300, align 8
  %xor1301 = xor i32 %605, %606
  %call1302 = call i32 @rotr32(i32 %xor1301, i32 7)
  %arrayidx1303 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1302, i32* %arrayidx1303, align 4
  br label %do.end1304

do.end1304:                                       ; preds = %do.body1261
  br label %do.body1305

do.body1305:                                      ; preds = %do.end1304
  %arrayidx1306 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %607 = load i32, i32* %arrayidx1306, align 4
  %arrayidx1307 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %608 = load i32, i32* %arrayidx1307, align 8
  %add1308 = add i32 %607, %608
  %609 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 10), align 2
  %idxprom1309 = zext i8 %609 to i32
  %arrayidx1310 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1309
  %610 = load i32, i32* %arrayidx1310, align 4
  %add1311 = add i32 %add1308, %610
  %arrayidx1312 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1311, i32* %arrayidx1312, align 4
  %arrayidx1313 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %611 = load i32, i32* %arrayidx1313, align 16
  %arrayidx1314 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %612 = load i32, i32* %arrayidx1314, align 4
  %xor1315 = xor i32 %611, %612
  %call1316 = call i32 @rotr32(i32 %xor1315, i32 16)
  %arrayidx1317 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1316, i32* %arrayidx1317, align 16
  %arrayidx1318 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %613 = load i32, i32* %arrayidx1318, align 4
  %arrayidx1319 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %614 = load i32, i32* %arrayidx1319, align 16
  %add1320 = add i32 %613, %614
  %arrayidx1321 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1320, i32* %arrayidx1321, align 4
  %arrayidx1322 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %615 = load i32, i32* %arrayidx1322, align 8
  %arrayidx1323 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %616 = load i32, i32* %arrayidx1323, align 4
  %xor1324 = xor i32 %615, %616
  %call1325 = call i32 @rotr32(i32 %xor1324, i32 12)
  %arrayidx1326 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1325, i32* %arrayidx1326, align 8
  %arrayidx1327 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %617 = load i32, i32* %arrayidx1327, align 4
  %arrayidx1328 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %618 = load i32, i32* %arrayidx1328, align 8
  %add1329 = add i32 %617, %618
  %619 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 11), align 1
  %idxprom1330 = zext i8 %619 to i32
  %arrayidx1331 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1330
  %620 = load i32, i32* %arrayidx1331, align 4
  %add1332 = add i32 %add1329, %620
  %arrayidx1333 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1332, i32* %arrayidx1333, align 4
  %arrayidx1334 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %621 = load i32, i32* %arrayidx1334, align 16
  %arrayidx1335 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %622 = load i32, i32* %arrayidx1335, align 4
  %xor1336 = xor i32 %621, %622
  %call1337 = call i32 @rotr32(i32 %xor1336, i32 8)
  %arrayidx1338 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1337, i32* %arrayidx1338, align 16
  %arrayidx1339 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %623 = load i32, i32* %arrayidx1339, align 4
  %arrayidx1340 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %624 = load i32, i32* %arrayidx1340, align 16
  %add1341 = add i32 %623, %624
  %arrayidx1342 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1341, i32* %arrayidx1342, align 4
  %arrayidx1343 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %625 = load i32, i32* %arrayidx1343, align 8
  %arrayidx1344 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %626 = load i32, i32* %arrayidx1344, align 4
  %xor1345 = xor i32 %625, %626
  %call1346 = call i32 @rotr32(i32 %xor1345, i32 7)
  %arrayidx1347 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1346, i32* %arrayidx1347, align 8
  br label %do.end1348

do.end1348:                                       ; preds = %do.body1305
  br label %do.body1349

do.body1349:                                      ; preds = %do.end1348
  %arrayidx1350 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %627 = load i32, i32* %arrayidx1350, align 8
  %arrayidx1351 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %628 = load i32, i32* %arrayidx1351, align 4
  %add1352 = add i32 %627, %628
  %629 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 12), align 4
  %idxprom1353 = zext i8 %629 to i32
  %arrayidx1354 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1353
  %630 = load i32, i32* %arrayidx1354, align 4
  %add1355 = add i32 %add1352, %630
  %arrayidx1356 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1355, i32* %arrayidx1356, align 8
  %arrayidx1357 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %631 = load i32, i32* %arrayidx1357, align 4
  %arrayidx1358 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %632 = load i32, i32* %arrayidx1358, align 8
  %xor1359 = xor i32 %631, %632
  %call1360 = call i32 @rotr32(i32 %xor1359, i32 16)
  %arrayidx1361 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1360, i32* %arrayidx1361, align 4
  %arrayidx1362 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %633 = load i32, i32* %arrayidx1362, align 16
  %arrayidx1363 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %634 = load i32, i32* %arrayidx1363, align 4
  %add1364 = add i32 %633, %634
  %arrayidx1365 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1364, i32* %arrayidx1365, align 16
  %arrayidx1366 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %635 = load i32, i32* %arrayidx1366, align 4
  %arrayidx1367 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %636 = load i32, i32* %arrayidx1367, align 16
  %xor1368 = xor i32 %635, %636
  %call1369 = call i32 @rotr32(i32 %xor1368, i32 12)
  %arrayidx1370 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1369, i32* %arrayidx1370, align 4
  %arrayidx1371 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %637 = load i32, i32* %arrayidx1371, align 8
  %arrayidx1372 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %638 = load i32, i32* %arrayidx1372, align 4
  %add1373 = add i32 %637, %638
  %639 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 13), align 1
  %idxprom1374 = zext i8 %639 to i32
  %arrayidx1375 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1374
  %640 = load i32, i32* %arrayidx1375, align 4
  %add1376 = add i32 %add1373, %640
  %arrayidx1377 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1376, i32* %arrayidx1377, align 8
  %arrayidx1378 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %641 = load i32, i32* %arrayidx1378, align 4
  %arrayidx1379 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %642 = load i32, i32* %arrayidx1379, align 8
  %xor1380 = xor i32 %641, %642
  %call1381 = call i32 @rotr32(i32 %xor1380, i32 8)
  %arrayidx1382 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1381, i32* %arrayidx1382, align 4
  %arrayidx1383 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %643 = load i32, i32* %arrayidx1383, align 16
  %arrayidx1384 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %644 = load i32, i32* %arrayidx1384, align 4
  %add1385 = add i32 %643, %644
  %arrayidx1386 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1385, i32* %arrayidx1386, align 16
  %arrayidx1387 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %645 = load i32, i32* %arrayidx1387, align 4
  %arrayidx1388 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %646 = load i32, i32* %arrayidx1388, align 16
  %xor1389 = xor i32 %645, %646
  %call1390 = call i32 @rotr32(i32 %xor1389, i32 7)
  %arrayidx1391 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1390, i32* %arrayidx1391, align 4
  br label %do.end1392

do.end1392:                                       ; preds = %do.body1349
  br label %do.body1393

do.body1393:                                      ; preds = %do.end1392
  %arrayidx1394 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %647 = load i32, i32* %arrayidx1394, align 4
  %arrayidx1395 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %648 = load i32, i32* %arrayidx1395, align 16
  %add1396 = add i32 %647, %648
  %649 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 14), align 2
  %idxprom1397 = zext i8 %649 to i32
  %arrayidx1398 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1397
  %650 = load i32, i32* %arrayidx1398, align 4
  %add1399 = add i32 %add1396, %650
  %arrayidx1400 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1399, i32* %arrayidx1400, align 4
  %arrayidx1401 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %651 = load i32, i32* %arrayidx1401, align 8
  %arrayidx1402 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %652 = load i32, i32* %arrayidx1402, align 4
  %xor1403 = xor i32 %651, %652
  %call1404 = call i32 @rotr32(i32 %xor1403, i32 16)
  %arrayidx1405 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1404, i32* %arrayidx1405, align 8
  %arrayidx1406 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %653 = load i32, i32* %arrayidx1406, align 4
  %arrayidx1407 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %654 = load i32, i32* %arrayidx1407, align 8
  %add1408 = add i32 %653, %654
  %arrayidx1409 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1408, i32* %arrayidx1409, align 4
  %arrayidx1410 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %655 = load i32, i32* %arrayidx1410, align 16
  %arrayidx1411 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %656 = load i32, i32* %arrayidx1411, align 4
  %xor1412 = xor i32 %655, %656
  %call1413 = call i32 @rotr32(i32 %xor1412, i32 12)
  %arrayidx1414 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1413, i32* %arrayidx1414, align 16
  %arrayidx1415 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %657 = load i32, i32* %arrayidx1415, align 4
  %arrayidx1416 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %658 = load i32, i32* %arrayidx1416, align 16
  %add1417 = add i32 %657, %658
  %659 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 3, i32 15), align 1
  %idxprom1418 = zext i8 %659 to i32
  %arrayidx1419 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1418
  %660 = load i32, i32* %arrayidx1419, align 4
  %add1420 = add i32 %add1417, %660
  %arrayidx1421 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1420, i32* %arrayidx1421, align 4
  %arrayidx1422 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %661 = load i32, i32* %arrayidx1422, align 8
  %arrayidx1423 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %662 = load i32, i32* %arrayidx1423, align 4
  %xor1424 = xor i32 %661, %662
  %call1425 = call i32 @rotr32(i32 %xor1424, i32 8)
  %arrayidx1426 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1425, i32* %arrayidx1426, align 8
  %arrayidx1427 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %663 = load i32, i32* %arrayidx1427, align 4
  %arrayidx1428 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %664 = load i32, i32* %arrayidx1428, align 8
  %add1429 = add i32 %663, %664
  %arrayidx1430 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1429, i32* %arrayidx1430, align 4
  %arrayidx1431 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %665 = load i32, i32* %arrayidx1431, align 16
  %arrayidx1432 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %666 = load i32, i32* %arrayidx1432, align 4
  %xor1433 = xor i32 %665, %666
  %call1434 = call i32 @rotr32(i32 %xor1433, i32 7)
  %arrayidx1435 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1434, i32* %arrayidx1435, align 16
  br label %do.end1436

do.end1436:                                       ; preds = %do.body1393
  br label %do.end1437

do.end1437:                                       ; preds = %do.end1436
  br label %do.body1438

do.body1438:                                      ; preds = %do.end1437
  br label %do.body1439

do.body1439:                                      ; preds = %do.body1438
  %arrayidx1440 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %667 = load i32, i32* %arrayidx1440, align 16
  %arrayidx1441 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %668 = load i32, i32* %arrayidx1441, align 16
  %add1442 = add i32 %667, %668
  %669 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 0), align 16
  %idxprom1443 = zext i8 %669 to i32
  %arrayidx1444 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1443
  %670 = load i32, i32* %arrayidx1444, align 4
  %add1445 = add i32 %add1442, %670
  %arrayidx1446 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1445, i32* %arrayidx1446, align 16
  %arrayidx1447 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %671 = load i32, i32* %arrayidx1447, align 16
  %arrayidx1448 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %672 = load i32, i32* %arrayidx1448, align 16
  %xor1449 = xor i32 %671, %672
  %call1450 = call i32 @rotr32(i32 %xor1449, i32 16)
  %arrayidx1451 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1450, i32* %arrayidx1451, align 16
  %arrayidx1452 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %673 = load i32, i32* %arrayidx1452, align 16
  %arrayidx1453 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %674 = load i32, i32* %arrayidx1453, align 16
  %add1454 = add i32 %673, %674
  %arrayidx1455 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1454, i32* %arrayidx1455, align 16
  %arrayidx1456 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %675 = load i32, i32* %arrayidx1456, align 16
  %arrayidx1457 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %676 = load i32, i32* %arrayidx1457, align 16
  %xor1458 = xor i32 %675, %676
  %call1459 = call i32 @rotr32(i32 %xor1458, i32 12)
  %arrayidx1460 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1459, i32* %arrayidx1460, align 16
  %arrayidx1461 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %677 = load i32, i32* %arrayidx1461, align 16
  %arrayidx1462 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %678 = load i32, i32* %arrayidx1462, align 16
  %add1463 = add i32 %677, %678
  %679 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 1), align 1
  %idxprom1464 = zext i8 %679 to i32
  %arrayidx1465 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1464
  %680 = load i32, i32* %arrayidx1465, align 4
  %add1466 = add i32 %add1463, %680
  %arrayidx1467 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1466, i32* %arrayidx1467, align 16
  %arrayidx1468 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %681 = load i32, i32* %arrayidx1468, align 16
  %arrayidx1469 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %682 = load i32, i32* %arrayidx1469, align 16
  %xor1470 = xor i32 %681, %682
  %call1471 = call i32 @rotr32(i32 %xor1470, i32 8)
  %arrayidx1472 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1471, i32* %arrayidx1472, align 16
  %arrayidx1473 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %683 = load i32, i32* %arrayidx1473, align 16
  %arrayidx1474 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %684 = load i32, i32* %arrayidx1474, align 16
  %add1475 = add i32 %683, %684
  %arrayidx1476 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1475, i32* %arrayidx1476, align 16
  %arrayidx1477 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %685 = load i32, i32* %arrayidx1477, align 16
  %arrayidx1478 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %686 = load i32, i32* %arrayidx1478, align 16
  %xor1479 = xor i32 %685, %686
  %call1480 = call i32 @rotr32(i32 %xor1479, i32 7)
  %arrayidx1481 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1480, i32* %arrayidx1481, align 16
  br label %do.end1482

do.end1482:                                       ; preds = %do.body1439
  br label %do.body1483

do.body1483:                                      ; preds = %do.end1482
  %arrayidx1484 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %687 = load i32, i32* %arrayidx1484, align 4
  %arrayidx1485 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %688 = load i32, i32* %arrayidx1485, align 4
  %add1486 = add i32 %687, %688
  %689 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 2), align 2
  %idxprom1487 = zext i8 %689 to i32
  %arrayidx1488 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1487
  %690 = load i32, i32* %arrayidx1488, align 4
  %add1489 = add i32 %add1486, %690
  %arrayidx1490 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1489, i32* %arrayidx1490, align 4
  %arrayidx1491 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %691 = load i32, i32* %arrayidx1491, align 4
  %arrayidx1492 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %692 = load i32, i32* %arrayidx1492, align 4
  %xor1493 = xor i32 %691, %692
  %call1494 = call i32 @rotr32(i32 %xor1493, i32 16)
  %arrayidx1495 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1494, i32* %arrayidx1495, align 4
  %arrayidx1496 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %693 = load i32, i32* %arrayidx1496, align 4
  %arrayidx1497 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %694 = load i32, i32* %arrayidx1497, align 4
  %add1498 = add i32 %693, %694
  %arrayidx1499 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1498, i32* %arrayidx1499, align 4
  %arrayidx1500 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %695 = load i32, i32* %arrayidx1500, align 4
  %arrayidx1501 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %696 = load i32, i32* %arrayidx1501, align 4
  %xor1502 = xor i32 %695, %696
  %call1503 = call i32 @rotr32(i32 %xor1502, i32 12)
  %arrayidx1504 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1503, i32* %arrayidx1504, align 4
  %arrayidx1505 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %697 = load i32, i32* %arrayidx1505, align 4
  %arrayidx1506 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %698 = load i32, i32* %arrayidx1506, align 4
  %add1507 = add i32 %697, %698
  %699 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 3), align 1
  %idxprom1508 = zext i8 %699 to i32
  %arrayidx1509 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1508
  %700 = load i32, i32* %arrayidx1509, align 4
  %add1510 = add i32 %add1507, %700
  %arrayidx1511 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1510, i32* %arrayidx1511, align 4
  %arrayidx1512 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %701 = load i32, i32* %arrayidx1512, align 4
  %arrayidx1513 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %702 = load i32, i32* %arrayidx1513, align 4
  %xor1514 = xor i32 %701, %702
  %call1515 = call i32 @rotr32(i32 %xor1514, i32 8)
  %arrayidx1516 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1515, i32* %arrayidx1516, align 4
  %arrayidx1517 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %703 = load i32, i32* %arrayidx1517, align 4
  %arrayidx1518 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %704 = load i32, i32* %arrayidx1518, align 4
  %add1519 = add i32 %703, %704
  %arrayidx1520 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1519, i32* %arrayidx1520, align 4
  %arrayidx1521 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %705 = load i32, i32* %arrayidx1521, align 4
  %arrayidx1522 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %706 = load i32, i32* %arrayidx1522, align 4
  %xor1523 = xor i32 %705, %706
  %call1524 = call i32 @rotr32(i32 %xor1523, i32 7)
  %arrayidx1525 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1524, i32* %arrayidx1525, align 4
  br label %do.end1526

do.end1526:                                       ; preds = %do.body1483
  br label %do.body1527

do.body1527:                                      ; preds = %do.end1526
  %arrayidx1528 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %707 = load i32, i32* %arrayidx1528, align 8
  %arrayidx1529 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %708 = load i32, i32* %arrayidx1529, align 8
  %add1530 = add i32 %707, %708
  %709 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 4), align 4
  %idxprom1531 = zext i8 %709 to i32
  %arrayidx1532 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1531
  %710 = load i32, i32* %arrayidx1532, align 4
  %add1533 = add i32 %add1530, %710
  %arrayidx1534 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1533, i32* %arrayidx1534, align 8
  %arrayidx1535 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %711 = load i32, i32* %arrayidx1535, align 8
  %arrayidx1536 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %712 = load i32, i32* %arrayidx1536, align 8
  %xor1537 = xor i32 %711, %712
  %call1538 = call i32 @rotr32(i32 %xor1537, i32 16)
  %arrayidx1539 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1538, i32* %arrayidx1539, align 8
  %arrayidx1540 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %713 = load i32, i32* %arrayidx1540, align 8
  %arrayidx1541 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %714 = load i32, i32* %arrayidx1541, align 8
  %add1542 = add i32 %713, %714
  %arrayidx1543 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1542, i32* %arrayidx1543, align 8
  %arrayidx1544 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %715 = load i32, i32* %arrayidx1544, align 8
  %arrayidx1545 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %716 = load i32, i32* %arrayidx1545, align 8
  %xor1546 = xor i32 %715, %716
  %call1547 = call i32 @rotr32(i32 %xor1546, i32 12)
  %arrayidx1548 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1547, i32* %arrayidx1548, align 8
  %arrayidx1549 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %717 = load i32, i32* %arrayidx1549, align 8
  %arrayidx1550 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %718 = load i32, i32* %arrayidx1550, align 8
  %add1551 = add i32 %717, %718
  %719 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 5), align 1
  %idxprom1552 = zext i8 %719 to i32
  %arrayidx1553 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1552
  %720 = load i32, i32* %arrayidx1553, align 4
  %add1554 = add i32 %add1551, %720
  %arrayidx1555 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1554, i32* %arrayidx1555, align 8
  %arrayidx1556 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %721 = load i32, i32* %arrayidx1556, align 8
  %arrayidx1557 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %722 = load i32, i32* %arrayidx1557, align 8
  %xor1558 = xor i32 %721, %722
  %call1559 = call i32 @rotr32(i32 %xor1558, i32 8)
  %arrayidx1560 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1559, i32* %arrayidx1560, align 8
  %arrayidx1561 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %723 = load i32, i32* %arrayidx1561, align 8
  %arrayidx1562 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %724 = load i32, i32* %arrayidx1562, align 8
  %add1563 = add i32 %723, %724
  %arrayidx1564 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1563, i32* %arrayidx1564, align 8
  %arrayidx1565 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %725 = load i32, i32* %arrayidx1565, align 8
  %arrayidx1566 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %726 = load i32, i32* %arrayidx1566, align 8
  %xor1567 = xor i32 %725, %726
  %call1568 = call i32 @rotr32(i32 %xor1567, i32 7)
  %arrayidx1569 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1568, i32* %arrayidx1569, align 8
  br label %do.end1570

do.end1570:                                       ; preds = %do.body1527
  br label %do.body1571

do.body1571:                                      ; preds = %do.end1570
  %arrayidx1572 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %727 = load i32, i32* %arrayidx1572, align 4
  %arrayidx1573 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %728 = load i32, i32* %arrayidx1573, align 4
  %add1574 = add i32 %727, %728
  %729 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 6), align 2
  %idxprom1575 = zext i8 %729 to i32
  %arrayidx1576 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1575
  %730 = load i32, i32* %arrayidx1576, align 4
  %add1577 = add i32 %add1574, %730
  %arrayidx1578 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1577, i32* %arrayidx1578, align 4
  %arrayidx1579 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %731 = load i32, i32* %arrayidx1579, align 4
  %arrayidx1580 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %732 = load i32, i32* %arrayidx1580, align 4
  %xor1581 = xor i32 %731, %732
  %call1582 = call i32 @rotr32(i32 %xor1581, i32 16)
  %arrayidx1583 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1582, i32* %arrayidx1583, align 4
  %arrayidx1584 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %733 = load i32, i32* %arrayidx1584, align 4
  %arrayidx1585 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %734 = load i32, i32* %arrayidx1585, align 4
  %add1586 = add i32 %733, %734
  %arrayidx1587 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1586, i32* %arrayidx1587, align 4
  %arrayidx1588 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %735 = load i32, i32* %arrayidx1588, align 4
  %arrayidx1589 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %736 = load i32, i32* %arrayidx1589, align 4
  %xor1590 = xor i32 %735, %736
  %call1591 = call i32 @rotr32(i32 %xor1590, i32 12)
  %arrayidx1592 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1591, i32* %arrayidx1592, align 4
  %arrayidx1593 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %737 = load i32, i32* %arrayidx1593, align 4
  %arrayidx1594 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %738 = load i32, i32* %arrayidx1594, align 4
  %add1595 = add i32 %737, %738
  %739 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 7), align 1
  %idxprom1596 = zext i8 %739 to i32
  %arrayidx1597 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1596
  %740 = load i32, i32* %arrayidx1597, align 4
  %add1598 = add i32 %add1595, %740
  %arrayidx1599 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1598, i32* %arrayidx1599, align 4
  %arrayidx1600 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %741 = load i32, i32* %arrayidx1600, align 4
  %arrayidx1601 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %742 = load i32, i32* %arrayidx1601, align 4
  %xor1602 = xor i32 %741, %742
  %call1603 = call i32 @rotr32(i32 %xor1602, i32 8)
  %arrayidx1604 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1603, i32* %arrayidx1604, align 4
  %arrayidx1605 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %743 = load i32, i32* %arrayidx1605, align 4
  %arrayidx1606 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %744 = load i32, i32* %arrayidx1606, align 4
  %add1607 = add i32 %743, %744
  %arrayidx1608 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1607, i32* %arrayidx1608, align 4
  %arrayidx1609 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %745 = load i32, i32* %arrayidx1609, align 4
  %arrayidx1610 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %746 = load i32, i32* %arrayidx1610, align 4
  %xor1611 = xor i32 %745, %746
  %call1612 = call i32 @rotr32(i32 %xor1611, i32 7)
  %arrayidx1613 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1612, i32* %arrayidx1613, align 4
  br label %do.end1614

do.end1614:                                       ; preds = %do.body1571
  br label %do.body1615

do.body1615:                                      ; preds = %do.end1614
  %arrayidx1616 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %747 = load i32, i32* %arrayidx1616, align 16
  %arrayidx1617 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %748 = load i32, i32* %arrayidx1617, align 4
  %add1618 = add i32 %747, %748
  %749 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 8), align 8
  %idxprom1619 = zext i8 %749 to i32
  %arrayidx1620 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1619
  %750 = load i32, i32* %arrayidx1620, align 4
  %add1621 = add i32 %add1618, %750
  %arrayidx1622 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1621, i32* %arrayidx1622, align 16
  %arrayidx1623 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %751 = load i32, i32* %arrayidx1623, align 4
  %arrayidx1624 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %752 = load i32, i32* %arrayidx1624, align 16
  %xor1625 = xor i32 %751, %752
  %call1626 = call i32 @rotr32(i32 %xor1625, i32 16)
  %arrayidx1627 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1626, i32* %arrayidx1627, align 4
  %arrayidx1628 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %753 = load i32, i32* %arrayidx1628, align 8
  %arrayidx1629 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %754 = load i32, i32* %arrayidx1629, align 4
  %add1630 = add i32 %753, %754
  %arrayidx1631 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1630, i32* %arrayidx1631, align 8
  %arrayidx1632 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %755 = load i32, i32* %arrayidx1632, align 4
  %arrayidx1633 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %756 = load i32, i32* %arrayidx1633, align 8
  %xor1634 = xor i32 %755, %756
  %call1635 = call i32 @rotr32(i32 %xor1634, i32 12)
  %arrayidx1636 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1635, i32* %arrayidx1636, align 4
  %arrayidx1637 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %757 = load i32, i32* %arrayidx1637, align 16
  %arrayidx1638 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %758 = load i32, i32* %arrayidx1638, align 4
  %add1639 = add i32 %757, %758
  %759 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 9), align 1
  %idxprom1640 = zext i8 %759 to i32
  %arrayidx1641 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1640
  %760 = load i32, i32* %arrayidx1641, align 4
  %add1642 = add i32 %add1639, %760
  %arrayidx1643 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1642, i32* %arrayidx1643, align 16
  %arrayidx1644 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %761 = load i32, i32* %arrayidx1644, align 4
  %arrayidx1645 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %762 = load i32, i32* %arrayidx1645, align 16
  %xor1646 = xor i32 %761, %762
  %call1647 = call i32 @rotr32(i32 %xor1646, i32 8)
  %arrayidx1648 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1647, i32* %arrayidx1648, align 4
  %arrayidx1649 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %763 = load i32, i32* %arrayidx1649, align 8
  %arrayidx1650 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %764 = load i32, i32* %arrayidx1650, align 4
  %add1651 = add i32 %763, %764
  %arrayidx1652 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1651, i32* %arrayidx1652, align 8
  %arrayidx1653 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %765 = load i32, i32* %arrayidx1653, align 4
  %arrayidx1654 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %766 = load i32, i32* %arrayidx1654, align 8
  %xor1655 = xor i32 %765, %766
  %call1656 = call i32 @rotr32(i32 %xor1655, i32 7)
  %arrayidx1657 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1656, i32* %arrayidx1657, align 4
  br label %do.end1658

do.end1658:                                       ; preds = %do.body1615
  br label %do.body1659

do.body1659:                                      ; preds = %do.end1658
  %arrayidx1660 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %767 = load i32, i32* %arrayidx1660, align 4
  %arrayidx1661 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %768 = load i32, i32* %arrayidx1661, align 8
  %add1662 = add i32 %767, %768
  %769 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 10), align 2
  %idxprom1663 = zext i8 %769 to i32
  %arrayidx1664 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1663
  %770 = load i32, i32* %arrayidx1664, align 4
  %add1665 = add i32 %add1662, %770
  %arrayidx1666 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1665, i32* %arrayidx1666, align 4
  %arrayidx1667 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %771 = load i32, i32* %arrayidx1667, align 16
  %arrayidx1668 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %772 = load i32, i32* %arrayidx1668, align 4
  %xor1669 = xor i32 %771, %772
  %call1670 = call i32 @rotr32(i32 %xor1669, i32 16)
  %arrayidx1671 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1670, i32* %arrayidx1671, align 16
  %arrayidx1672 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %773 = load i32, i32* %arrayidx1672, align 4
  %arrayidx1673 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %774 = load i32, i32* %arrayidx1673, align 16
  %add1674 = add i32 %773, %774
  %arrayidx1675 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1674, i32* %arrayidx1675, align 4
  %arrayidx1676 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %775 = load i32, i32* %arrayidx1676, align 8
  %arrayidx1677 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %776 = load i32, i32* %arrayidx1677, align 4
  %xor1678 = xor i32 %775, %776
  %call1679 = call i32 @rotr32(i32 %xor1678, i32 12)
  %arrayidx1680 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1679, i32* %arrayidx1680, align 8
  %arrayidx1681 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %777 = load i32, i32* %arrayidx1681, align 4
  %arrayidx1682 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %778 = load i32, i32* %arrayidx1682, align 8
  %add1683 = add i32 %777, %778
  %779 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 11), align 1
  %idxprom1684 = zext i8 %779 to i32
  %arrayidx1685 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1684
  %780 = load i32, i32* %arrayidx1685, align 4
  %add1686 = add i32 %add1683, %780
  %arrayidx1687 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1686, i32* %arrayidx1687, align 4
  %arrayidx1688 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %781 = load i32, i32* %arrayidx1688, align 16
  %arrayidx1689 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %782 = load i32, i32* %arrayidx1689, align 4
  %xor1690 = xor i32 %781, %782
  %call1691 = call i32 @rotr32(i32 %xor1690, i32 8)
  %arrayidx1692 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1691, i32* %arrayidx1692, align 16
  %arrayidx1693 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %783 = load i32, i32* %arrayidx1693, align 4
  %arrayidx1694 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %784 = load i32, i32* %arrayidx1694, align 16
  %add1695 = add i32 %783, %784
  %arrayidx1696 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1695, i32* %arrayidx1696, align 4
  %arrayidx1697 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %785 = load i32, i32* %arrayidx1697, align 8
  %arrayidx1698 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %786 = load i32, i32* %arrayidx1698, align 4
  %xor1699 = xor i32 %785, %786
  %call1700 = call i32 @rotr32(i32 %xor1699, i32 7)
  %arrayidx1701 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1700, i32* %arrayidx1701, align 8
  br label %do.end1702

do.end1702:                                       ; preds = %do.body1659
  br label %do.body1703

do.body1703:                                      ; preds = %do.end1702
  %arrayidx1704 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %787 = load i32, i32* %arrayidx1704, align 8
  %arrayidx1705 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %788 = load i32, i32* %arrayidx1705, align 4
  %add1706 = add i32 %787, %788
  %789 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 12), align 4
  %idxprom1707 = zext i8 %789 to i32
  %arrayidx1708 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1707
  %790 = load i32, i32* %arrayidx1708, align 4
  %add1709 = add i32 %add1706, %790
  %arrayidx1710 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1709, i32* %arrayidx1710, align 8
  %arrayidx1711 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %791 = load i32, i32* %arrayidx1711, align 4
  %arrayidx1712 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %792 = load i32, i32* %arrayidx1712, align 8
  %xor1713 = xor i32 %791, %792
  %call1714 = call i32 @rotr32(i32 %xor1713, i32 16)
  %arrayidx1715 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1714, i32* %arrayidx1715, align 4
  %arrayidx1716 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %793 = load i32, i32* %arrayidx1716, align 16
  %arrayidx1717 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %794 = load i32, i32* %arrayidx1717, align 4
  %add1718 = add i32 %793, %794
  %arrayidx1719 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1718, i32* %arrayidx1719, align 16
  %arrayidx1720 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %795 = load i32, i32* %arrayidx1720, align 4
  %arrayidx1721 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %796 = load i32, i32* %arrayidx1721, align 16
  %xor1722 = xor i32 %795, %796
  %call1723 = call i32 @rotr32(i32 %xor1722, i32 12)
  %arrayidx1724 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1723, i32* %arrayidx1724, align 4
  %arrayidx1725 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %797 = load i32, i32* %arrayidx1725, align 8
  %arrayidx1726 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %798 = load i32, i32* %arrayidx1726, align 4
  %add1727 = add i32 %797, %798
  %799 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 13), align 1
  %idxprom1728 = zext i8 %799 to i32
  %arrayidx1729 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1728
  %800 = load i32, i32* %arrayidx1729, align 4
  %add1730 = add i32 %add1727, %800
  %arrayidx1731 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1730, i32* %arrayidx1731, align 8
  %arrayidx1732 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %801 = load i32, i32* %arrayidx1732, align 4
  %arrayidx1733 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %802 = load i32, i32* %arrayidx1733, align 8
  %xor1734 = xor i32 %801, %802
  %call1735 = call i32 @rotr32(i32 %xor1734, i32 8)
  %arrayidx1736 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1735, i32* %arrayidx1736, align 4
  %arrayidx1737 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %803 = load i32, i32* %arrayidx1737, align 16
  %arrayidx1738 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %804 = load i32, i32* %arrayidx1738, align 4
  %add1739 = add i32 %803, %804
  %arrayidx1740 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1739, i32* %arrayidx1740, align 16
  %arrayidx1741 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %805 = load i32, i32* %arrayidx1741, align 4
  %arrayidx1742 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %806 = load i32, i32* %arrayidx1742, align 16
  %xor1743 = xor i32 %805, %806
  %call1744 = call i32 @rotr32(i32 %xor1743, i32 7)
  %arrayidx1745 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1744, i32* %arrayidx1745, align 4
  br label %do.end1746

do.end1746:                                       ; preds = %do.body1703
  br label %do.body1747

do.body1747:                                      ; preds = %do.end1746
  %arrayidx1748 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %807 = load i32, i32* %arrayidx1748, align 4
  %arrayidx1749 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %808 = load i32, i32* %arrayidx1749, align 16
  %add1750 = add i32 %807, %808
  %809 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 14), align 2
  %idxprom1751 = zext i8 %809 to i32
  %arrayidx1752 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1751
  %810 = load i32, i32* %arrayidx1752, align 4
  %add1753 = add i32 %add1750, %810
  %arrayidx1754 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1753, i32* %arrayidx1754, align 4
  %arrayidx1755 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %811 = load i32, i32* %arrayidx1755, align 8
  %arrayidx1756 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %812 = load i32, i32* %arrayidx1756, align 4
  %xor1757 = xor i32 %811, %812
  %call1758 = call i32 @rotr32(i32 %xor1757, i32 16)
  %arrayidx1759 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1758, i32* %arrayidx1759, align 8
  %arrayidx1760 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %813 = load i32, i32* %arrayidx1760, align 4
  %arrayidx1761 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %814 = load i32, i32* %arrayidx1761, align 8
  %add1762 = add i32 %813, %814
  %arrayidx1763 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1762, i32* %arrayidx1763, align 4
  %arrayidx1764 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %815 = load i32, i32* %arrayidx1764, align 16
  %arrayidx1765 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %816 = load i32, i32* %arrayidx1765, align 4
  %xor1766 = xor i32 %815, %816
  %call1767 = call i32 @rotr32(i32 %xor1766, i32 12)
  %arrayidx1768 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1767, i32* %arrayidx1768, align 16
  %arrayidx1769 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %817 = load i32, i32* %arrayidx1769, align 4
  %arrayidx1770 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %818 = load i32, i32* %arrayidx1770, align 16
  %add1771 = add i32 %817, %818
  %819 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 4, i32 15), align 1
  %idxprom1772 = zext i8 %819 to i32
  %arrayidx1773 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1772
  %820 = load i32, i32* %arrayidx1773, align 4
  %add1774 = add i32 %add1771, %820
  %arrayidx1775 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1774, i32* %arrayidx1775, align 4
  %arrayidx1776 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %821 = load i32, i32* %arrayidx1776, align 8
  %arrayidx1777 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %822 = load i32, i32* %arrayidx1777, align 4
  %xor1778 = xor i32 %821, %822
  %call1779 = call i32 @rotr32(i32 %xor1778, i32 8)
  %arrayidx1780 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1779, i32* %arrayidx1780, align 8
  %arrayidx1781 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %823 = load i32, i32* %arrayidx1781, align 4
  %arrayidx1782 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %824 = load i32, i32* %arrayidx1782, align 8
  %add1783 = add i32 %823, %824
  %arrayidx1784 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1783, i32* %arrayidx1784, align 4
  %arrayidx1785 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %825 = load i32, i32* %arrayidx1785, align 16
  %arrayidx1786 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %826 = load i32, i32* %arrayidx1786, align 4
  %xor1787 = xor i32 %825, %826
  %call1788 = call i32 @rotr32(i32 %xor1787, i32 7)
  %arrayidx1789 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1788, i32* %arrayidx1789, align 16
  br label %do.end1790

do.end1790:                                       ; preds = %do.body1747
  br label %do.end1791

do.end1791:                                       ; preds = %do.end1790
  br label %do.body1792

do.body1792:                                      ; preds = %do.end1791
  br label %do.body1793

do.body1793:                                      ; preds = %do.body1792
  %arrayidx1794 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %827 = load i32, i32* %arrayidx1794, align 16
  %arrayidx1795 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %828 = load i32, i32* %arrayidx1795, align 16
  %add1796 = add i32 %827, %828
  %829 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 0), align 16
  %idxprom1797 = zext i8 %829 to i32
  %arrayidx1798 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1797
  %830 = load i32, i32* %arrayidx1798, align 4
  %add1799 = add i32 %add1796, %830
  %arrayidx1800 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1799, i32* %arrayidx1800, align 16
  %arrayidx1801 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %831 = load i32, i32* %arrayidx1801, align 16
  %arrayidx1802 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %832 = load i32, i32* %arrayidx1802, align 16
  %xor1803 = xor i32 %831, %832
  %call1804 = call i32 @rotr32(i32 %xor1803, i32 16)
  %arrayidx1805 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1804, i32* %arrayidx1805, align 16
  %arrayidx1806 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %833 = load i32, i32* %arrayidx1806, align 16
  %arrayidx1807 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %834 = load i32, i32* %arrayidx1807, align 16
  %add1808 = add i32 %833, %834
  %arrayidx1809 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1808, i32* %arrayidx1809, align 16
  %arrayidx1810 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %835 = load i32, i32* %arrayidx1810, align 16
  %arrayidx1811 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %836 = load i32, i32* %arrayidx1811, align 16
  %xor1812 = xor i32 %835, %836
  %call1813 = call i32 @rotr32(i32 %xor1812, i32 12)
  %arrayidx1814 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1813, i32* %arrayidx1814, align 16
  %arrayidx1815 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %837 = load i32, i32* %arrayidx1815, align 16
  %arrayidx1816 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %838 = load i32, i32* %arrayidx1816, align 16
  %add1817 = add i32 %837, %838
  %839 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 1), align 1
  %idxprom1818 = zext i8 %839 to i32
  %arrayidx1819 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1818
  %840 = load i32, i32* %arrayidx1819, align 4
  %add1820 = add i32 %add1817, %840
  %arrayidx1821 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1820, i32* %arrayidx1821, align 16
  %arrayidx1822 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %841 = load i32, i32* %arrayidx1822, align 16
  %arrayidx1823 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %842 = load i32, i32* %arrayidx1823, align 16
  %xor1824 = xor i32 %841, %842
  %call1825 = call i32 @rotr32(i32 %xor1824, i32 8)
  %arrayidx1826 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call1825, i32* %arrayidx1826, align 16
  %arrayidx1827 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %843 = load i32, i32* %arrayidx1827, align 16
  %arrayidx1828 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %844 = load i32, i32* %arrayidx1828, align 16
  %add1829 = add i32 %843, %844
  %arrayidx1830 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add1829, i32* %arrayidx1830, align 16
  %arrayidx1831 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %845 = load i32, i32* %arrayidx1831, align 16
  %arrayidx1832 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %846 = load i32, i32* %arrayidx1832, align 16
  %xor1833 = xor i32 %845, %846
  %call1834 = call i32 @rotr32(i32 %xor1833, i32 7)
  %arrayidx1835 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call1834, i32* %arrayidx1835, align 16
  br label %do.end1836

do.end1836:                                       ; preds = %do.body1793
  br label %do.body1837

do.body1837:                                      ; preds = %do.end1836
  %arrayidx1838 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %847 = load i32, i32* %arrayidx1838, align 4
  %arrayidx1839 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %848 = load i32, i32* %arrayidx1839, align 4
  %add1840 = add i32 %847, %848
  %849 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 2), align 2
  %idxprom1841 = zext i8 %849 to i32
  %arrayidx1842 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1841
  %850 = load i32, i32* %arrayidx1842, align 4
  %add1843 = add i32 %add1840, %850
  %arrayidx1844 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1843, i32* %arrayidx1844, align 4
  %arrayidx1845 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %851 = load i32, i32* %arrayidx1845, align 4
  %arrayidx1846 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %852 = load i32, i32* %arrayidx1846, align 4
  %xor1847 = xor i32 %851, %852
  %call1848 = call i32 @rotr32(i32 %xor1847, i32 16)
  %arrayidx1849 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1848, i32* %arrayidx1849, align 4
  %arrayidx1850 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %853 = load i32, i32* %arrayidx1850, align 4
  %arrayidx1851 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %854 = load i32, i32* %arrayidx1851, align 4
  %add1852 = add i32 %853, %854
  %arrayidx1853 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1852, i32* %arrayidx1853, align 4
  %arrayidx1854 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %855 = load i32, i32* %arrayidx1854, align 4
  %arrayidx1855 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %856 = load i32, i32* %arrayidx1855, align 4
  %xor1856 = xor i32 %855, %856
  %call1857 = call i32 @rotr32(i32 %xor1856, i32 12)
  %arrayidx1858 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1857, i32* %arrayidx1858, align 4
  %arrayidx1859 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %857 = load i32, i32* %arrayidx1859, align 4
  %arrayidx1860 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %858 = load i32, i32* %arrayidx1860, align 4
  %add1861 = add i32 %857, %858
  %859 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 3), align 1
  %idxprom1862 = zext i8 %859 to i32
  %arrayidx1863 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1862
  %860 = load i32, i32* %arrayidx1863, align 4
  %add1864 = add i32 %add1861, %860
  %arrayidx1865 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add1864, i32* %arrayidx1865, align 4
  %arrayidx1866 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %861 = load i32, i32* %arrayidx1866, align 4
  %arrayidx1867 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %862 = load i32, i32* %arrayidx1867, align 4
  %xor1868 = xor i32 %861, %862
  %call1869 = call i32 @rotr32(i32 %xor1868, i32 8)
  %arrayidx1870 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call1869, i32* %arrayidx1870, align 4
  %arrayidx1871 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %863 = load i32, i32* %arrayidx1871, align 4
  %arrayidx1872 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %864 = load i32, i32* %arrayidx1872, align 4
  %add1873 = add i32 %863, %864
  %arrayidx1874 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add1873, i32* %arrayidx1874, align 4
  %arrayidx1875 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %865 = load i32, i32* %arrayidx1875, align 4
  %arrayidx1876 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %866 = load i32, i32* %arrayidx1876, align 4
  %xor1877 = xor i32 %865, %866
  %call1878 = call i32 @rotr32(i32 %xor1877, i32 7)
  %arrayidx1879 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1878, i32* %arrayidx1879, align 4
  br label %do.end1880

do.end1880:                                       ; preds = %do.body1837
  br label %do.body1881

do.body1881:                                      ; preds = %do.end1880
  %arrayidx1882 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %867 = load i32, i32* %arrayidx1882, align 8
  %arrayidx1883 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %868 = load i32, i32* %arrayidx1883, align 8
  %add1884 = add i32 %867, %868
  %869 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 4), align 4
  %idxprom1885 = zext i8 %869 to i32
  %arrayidx1886 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1885
  %870 = load i32, i32* %arrayidx1886, align 4
  %add1887 = add i32 %add1884, %870
  %arrayidx1888 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1887, i32* %arrayidx1888, align 8
  %arrayidx1889 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %871 = load i32, i32* %arrayidx1889, align 8
  %arrayidx1890 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %872 = load i32, i32* %arrayidx1890, align 8
  %xor1891 = xor i32 %871, %872
  %call1892 = call i32 @rotr32(i32 %xor1891, i32 16)
  %arrayidx1893 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1892, i32* %arrayidx1893, align 8
  %arrayidx1894 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %873 = load i32, i32* %arrayidx1894, align 8
  %arrayidx1895 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %874 = load i32, i32* %arrayidx1895, align 8
  %add1896 = add i32 %873, %874
  %arrayidx1897 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1896, i32* %arrayidx1897, align 8
  %arrayidx1898 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %875 = load i32, i32* %arrayidx1898, align 8
  %arrayidx1899 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %876 = load i32, i32* %arrayidx1899, align 8
  %xor1900 = xor i32 %875, %876
  %call1901 = call i32 @rotr32(i32 %xor1900, i32 12)
  %arrayidx1902 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1901, i32* %arrayidx1902, align 8
  %arrayidx1903 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %877 = load i32, i32* %arrayidx1903, align 8
  %arrayidx1904 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %878 = load i32, i32* %arrayidx1904, align 8
  %add1905 = add i32 %877, %878
  %879 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 5), align 1
  %idxprom1906 = zext i8 %879 to i32
  %arrayidx1907 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1906
  %880 = load i32, i32* %arrayidx1907, align 4
  %add1908 = add i32 %add1905, %880
  %arrayidx1909 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add1908, i32* %arrayidx1909, align 8
  %arrayidx1910 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %881 = load i32, i32* %arrayidx1910, align 8
  %arrayidx1911 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %882 = load i32, i32* %arrayidx1911, align 8
  %xor1912 = xor i32 %881, %882
  %call1913 = call i32 @rotr32(i32 %xor1912, i32 8)
  %arrayidx1914 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call1913, i32* %arrayidx1914, align 8
  %arrayidx1915 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %883 = load i32, i32* %arrayidx1915, align 8
  %arrayidx1916 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %884 = load i32, i32* %arrayidx1916, align 8
  %add1917 = add i32 %883, %884
  %arrayidx1918 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1917, i32* %arrayidx1918, align 8
  %arrayidx1919 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %885 = load i32, i32* %arrayidx1919, align 8
  %arrayidx1920 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %886 = load i32, i32* %arrayidx1920, align 8
  %xor1921 = xor i32 %885, %886
  %call1922 = call i32 @rotr32(i32 %xor1921, i32 7)
  %arrayidx1923 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call1922, i32* %arrayidx1923, align 8
  br label %do.end1924

do.end1924:                                       ; preds = %do.body1881
  br label %do.body1925

do.body1925:                                      ; preds = %do.end1924
  %arrayidx1926 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %887 = load i32, i32* %arrayidx1926, align 4
  %arrayidx1927 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %888 = load i32, i32* %arrayidx1927, align 4
  %add1928 = add i32 %887, %888
  %889 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 6), align 2
  %idxprom1929 = zext i8 %889 to i32
  %arrayidx1930 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1929
  %890 = load i32, i32* %arrayidx1930, align 4
  %add1931 = add i32 %add1928, %890
  %arrayidx1932 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1931, i32* %arrayidx1932, align 4
  %arrayidx1933 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %891 = load i32, i32* %arrayidx1933, align 4
  %arrayidx1934 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %892 = load i32, i32* %arrayidx1934, align 4
  %xor1935 = xor i32 %891, %892
  %call1936 = call i32 @rotr32(i32 %xor1935, i32 16)
  %arrayidx1937 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1936, i32* %arrayidx1937, align 4
  %arrayidx1938 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %893 = load i32, i32* %arrayidx1938, align 4
  %arrayidx1939 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %894 = load i32, i32* %arrayidx1939, align 4
  %add1940 = add i32 %893, %894
  %arrayidx1941 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1940, i32* %arrayidx1941, align 4
  %arrayidx1942 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %895 = load i32, i32* %arrayidx1942, align 4
  %arrayidx1943 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %896 = load i32, i32* %arrayidx1943, align 4
  %xor1944 = xor i32 %895, %896
  %call1945 = call i32 @rotr32(i32 %xor1944, i32 12)
  %arrayidx1946 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1945, i32* %arrayidx1946, align 4
  %arrayidx1947 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %897 = load i32, i32* %arrayidx1947, align 4
  %arrayidx1948 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %898 = load i32, i32* %arrayidx1948, align 4
  %add1949 = add i32 %897, %898
  %899 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 7), align 1
  %idxprom1950 = zext i8 %899 to i32
  %arrayidx1951 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1950
  %900 = load i32, i32* %arrayidx1951, align 4
  %add1952 = add i32 %add1949, %900
  %arrayidx1953 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add1952, i32* %arrayidx1953, align 4
  %arrayidx1954 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %901 = load i32, i32* %arrayidx1954, align 4
  %arrayidx1955 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %902 = load i32, i32* %arrayidx1955, align 4
  %xor1956 = xor i32 %901, %902
  %call1957 = call i32 @rotr32(i32 %xor1956, i32 8)
  %arrayidx1958 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1957, i32* %arrayidx1958, align 4
  %arrayidx1959 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %903 = load i32, i32* %arrayidx1959, align 4
  %arrayidx1960 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %904 = load i32, i32* %arrayidx1960, align 4
  %add1961 = add i32 %903, %904
  %arrayidx1962 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add1961, i32* %arrayidx1962, align 4
  %arrayidx1963 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %905 = load i32, i32* %arrayidx1963, align 4
  %arrayidx1964 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %906 = load i32, i32* %arrayidx1964, align 4
  %xor1965 = xor i32 %905, %906
  %call1966 = call i32 @rotr32(i32 %xor1965, i32 7)
  %arrayidx1967 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call1966, i32* %arrayidx1967, align 4
  br label %do.end1968

do.end1968:                                       ; preds = %do.body1925
  br label %do.body1969

do.body1969:                                      ; preds = %do.end1968
  %arrayidx1970 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %907 = load i32, i32* %arrayidx1970, align 16
  %arrayidx1971 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %908 = load i32, i32* %arrayidx1971, align 4
  %add1972 = add i32 %907, %908
  %909 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 8), align 8
  %idxprom1973 = zext i8 %909 to i32
  %arrayidx1974 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1973
  %910 = load i32, i32* %arrayidx1974, align 4
  %add1975 = add i32 %add1972, %910
  %arrayidx1976 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1975, i32* %arrayidx1976, align 16
  %arrayidx1977 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %911 = load i32, i32* %arrayidx1977, align 4
  %arrayidx1978 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %912 = load i32, i32* %arrayidx1978, align 16
  %xor1979 = xor i32 %911, %912
  %call1980 = call i32 @rotr32(i32 %xor1979, i32 16)
  %arrayidx1981 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call1980, i32* %arrayidx1981, align 4
  %arrayidx1982 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %913 = load i32, i32* %arrayidx1982, align 8
  %arrayidx1983 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %914 = load i32, i32* %arrayidx1983, align 4
  %add1984 = add i32 %913, %914
  %arrayidx1985 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add1984, i32* %arrayidx1985, align 8
  %arrayidx1986 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %915 = load i32, i32* %arrayidx1986, align 4
  %arrayidx1987 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %916 = load i32, i32* %arrayidx1987, align 8
  %xor1988 = xor i32 %915, %916
  %call1989 = call i32 @rotr32(i32 %xor1988, i32 12)
  %arrayidx1990 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call1989, i32* %arrayidx1990, align 4
  %arrayidx1991 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %917 = load i32, i32* %arrayidx1991, align 16
  %arrayidx1992 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %918 = load i32, i32* %arrayidx1992, align 4
  %add1993 = add i32 %917, %918
  %919 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 9), align 1
  %idxprom1994 = zext i8 %919 to i32
  %arrayidx1995 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom1994
  %920 = load i32, i32* %arrayidx1995, align 4
  %add1996 = add i32 %add1993, %920
  %arrayidx1997 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add1996, i32* %arrayidx1997, align 16
  %arrayidx1998 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %921 = load i32, i32* %arrayidx1998, align 4
  %arrayidx1999 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %922 = load i32, i32* %arrayidx1999, align 16
  %xor2000 = xor i32 %921, %922
  %call2001 = call i32 @rotr32(i32 %xor2000, i32 8)
  %arrayidx2002 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2001, i32* %arrayidx2002, align 4
  %arrayidx2003 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %923 = load i32, i32* %arrayidx2003, align 8
  %arrayidx2004 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %924 = load i32, i32* %arrayidx2004, align 4
  %add2005 = add i32 %923, %924
  %arrayidx2006 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2005, i32* %arrayidx2006, align 8
  %arrayidx2007 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %925 = load i32, i32* %arrayidx2007, align 4
  %arrayidx2008 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %926 = load i32, i32* %arrayidx2008, align 8
  %xor2009 = xor i32 %925, %926
  %call2010 = call i32 @rotr32(i32 %xor2009, i32 7)
  %arrayidx2011 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2010, i32* %arrayidx2011, align 4
  br label %do.end2012

do.end2012:                                       ; preds = %do.body1969
  br label %do.body2013

do.body2013:                                      ; preds = %do.end2012
  %arrayidx2014 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %927 = load i32, i32* %arrayidx2014, align 4
  %arrayidx2015 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %928 = load i32, i32* %arrayidx2015, align 8
  %add2016 = add i32 %927, %928
  %929 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 10), align 2
  %idxprom2017 = zext i8 %929 to i32
  %arrayidx2018 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2017
  %930 = load i32, i32* %arrayidx2018, align 4
  %add2019 = add i32 %add2016, %930
  %arrayidx2020 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2019, i32* %arrayidx2020, align 4
  %arrayidx2021 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %931 = load i32, i32* %arrayidx2021, align 16
  %arrayidx2022 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %932 = load i32, i32* %arrayidx2022, align 4
  %xor2023 = xor i32 %931, %932
  %call2024 = call i32 @rotr32(i32 %xor2023, i32 16)
  %arrayidx2025 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2024, i32* %arrayidx2025, align 16
  %arrayidx2026 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %933 = load i32, i32* %arrayidx2026, align 4
  %arrayidx2027 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %934 = load i32, i32* %arrayidx2027, align 16
  %add2028 = add i32 %933, %934
  %arrayidx2029 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2028, i32* %arrayidx2029, align 4
  %arrayidx2030 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %935 = load i32, i32* %arrayidx2030, align 8
  %arrayidx2031 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %936 = load i32, i32* %arrayidx2031, align 4
  %xor2032 = xor i32 %935, %936
  %call2033 = call i32 @rotr32(i32 %xor2032, i32 12)
  %arrayidx2034 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2033, i32* %arrayidx2034, align 8
  %arrayidx2035 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %937 = load i32, i32* %arrayidx2035, align 4
  %arrayidx2036 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %938 = load i32, i32* %arrayidx2036, align 8
  %add2037 = add i32 %937, %938
  %939 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 11), align 1
  %idxprom2038 = zext i8 %939 to i32
  %arrayidx2039 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2038
  %940 = load i32, i32* %arrayidx2039, align 4
  %add2040 = add i32 %add2037, %940
  %arrayidx2041 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2040, i32* %arrayidx2041, align 4
  %arrayidx2042 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %941 = load i32, i32* %arrayidx2042, align 16
  %arrayidx2043 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %942 = load i32, i32* %arrayidx2043, align 4
  %xor2044 = xor i32 %941, %942
  %call2045 = call i32 @rotr32(i32 %xor2044, i32 8)
  %arrayidx2046 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2045, i32* %arrayidx2046, align 16
  %arrayidx2047 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %943 = load i32, i32* %arrayidx2047, align 4
  %arrayidx2048 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %944 = load i32, i32* %arrayidx2048, align 16
  %add2049 = add i32 %943, %944
  %arrayidx2050 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2049, i32* %arrayidx2050, align 4
  %arrayidx2051 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %945 = load i32, i32* %arrayidx2051, align 8
  %arrayidx2052 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %946 = load i32, i32* %arrayidx2052, align 4
  %xor2053 = xor i32 %945, %946
  %call2054 = call i32 @rotr32(i32 %xor2053, i32 7)
  %arrayidx2055 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2054, i32* %arrayidx2055, align 8
  br label %do.end2056

do.end2056:                                       ; preds = %do.body2013
  br label %do.body2057

do.body2057:                                      ; preds = %do.end2056
  %arrayidx2058 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %947 = load i32, i32* %arrayidx2058, align 8
  %arrayidx2059 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %948 = load i32, i32* %arrayidx2059, align 4
  %add2060 = add i32 %947, %948
  %949 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 12), align 4
  %idxprom2061 = zext i8 %949 to i32
  %arrayidx2062 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2061
  %950 = load i32, i32* %arrayidx2062, align 4
  %add2063 = add i32 %add2060, %950
  %arrayidx2064 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2063, i32* %arrayidx2064, align 8
  %arrayidx2065 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %951 = load i32, i32* %arrayidx2065, align 4
  %arrayidx2066 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %952 = load i32, i32* %arrayidx2066, align 8
  %xor2067 = xor i32 %951, %952
  %call2068 = call i32 @rotr32(i32 %xor2067, i32 16)
  %arrayidx2069 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2068, i32* %arrayidx2069, align 4
  %arrayidx2070 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %953 = load i32, i32* %arrayidx2070, align 16
  %arrayidx2071 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %954 = load i32, i32* %arrayidx2071, align 4
  %add2072 = add i32 %953, %954
  %arrayidx2073 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2072, i32* %arrayidx2073, align 16
  %arrayidx2074 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %955 = load i32, i32* %arrayidx2074, align 4
  %arrayidx2075 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %956 = load i32, i32* %arrayidx2075, align 16
  %xor2076 = xor i32 %955, %956
  %call2077 = call i32 @rotr32(i32 %xor2076, i32 12)
  %arrayidx2078 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2077, i32* %arrayidx2078, align 4
  %arrayidx2079 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %957 = load i32, i32* %arrayidx2079, align 8
  %arrayidx2080 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %958 = load i32, i32* %arrayidx2080, align 4
  %add2081 = add i32 %957, %958
  %959 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 13), align 1
  %idxprom2082 = zext i8 %959 to i32
  %arrayidx2083 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2082
  %960 = load i32, i32* %arrayidx2083, align 4
  %add2084 = add i32 %add2081, %960
  %arrayidx2085 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2084, i32* %arrayidx2085, align 8
  %arrayidx2086 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %961 = load i32, i32* %arrayidx2086, align 4
  %arrayidx2087 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %962 = load i32, i32* %arrayidx2087, align 8
  %xor2088 = xor i32 %961, %962
  %call2089 = call i32 @rotr32(i32 %xor2088, i32 8)
  %arrayidx2090 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2089, i32* %arrayidx2090, align 4
  %arrayidx2091 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %963 = load i32, i32* %arrayidx2091, align 16
  %arrayidx2092 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %964 = load i32, i32* %arrayidx2092, align 4
  %add2093 = add i32 %963, %964
  %arrayidx2094 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2093, i32* %arrayidx2094, align 16
  %arrayidx2095 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %965 = load i32, i32* %arrayidx2095, align 4
  %arrayidx2096 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %966 = load i32, i32* %arrayidx2096, align 16
  %xor2097 = xor i32 %965, %966
  %call2098 = call i32 @rotr32(i32 %xor2097, i32 7)
  %arrayidx2099 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2098, i32* %arrayidx2099, align 4
  br label %do.end2100

do.end2100:                                       ; preds = %do.body2057
  br label %do.body2101

do.body2101:                                      ; preds = %do.end2100
  %arrayidx2102 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %967 = load i32, i32* %arrayidx2102, align 4
  %arrayidx2103 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %968 = load i32, i32* %arrayidx2103, align 16
  %add2104 = add i32 %967, %968
  %969 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 14), align 2
  %idxprom2105 = zext i8 %969 to i32
  %arrayidx2106 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2105
  %970 = load i32, i32* %arrayidx2106, align 4
  %add2107 = add i32 %add2104, %970
  %arrayidx2108 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2107, i32* %arrayidx2108, align 4
  %arrayidx2109 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %971 = load i32, i32* %arrayidx2109, align 8
  %arrayidx2110 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %972 = load i32, i32* %arrayidx2110, align 4
  %xor2111 = xor i32 %971, %972
  %call2112 = call i32 @rotr32(i32 %xor2111, i32 16)
  %arrayidx2113 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2112, i32* %arrayidx2113, align 8
  %arrayidx2114 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %973 = load i32, i32* %arrayidx2114, align 4
  %arrayidx2115 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %974 = load i32, i32* %arrayidx2115, align 8
  %add2116 = add i32 %973, %974
  %arrayidx2117 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2116, i32* %arrayidx2117, align 4
  %arrayidx2118 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %975 = load i32, i32* %arrayidx2118, align 16
  %arrayidx2119 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %976 = load i32, i32* %arrayidx2119, align 4
  %xor2120 = xor i32 %975, %976
  %call2121 = call i32 @rotr32(i32 %xor2120, i32 12)
  %arrayidx2122 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2121, i32* %arrayidx2122, align 16
  %arrayidx2123 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %977 = load i32, i32* %arrayidx2123, align 4
  %arrayidx2124 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %978 = load i32, i32* %arrayidx2124, align 16
  %add2125 = add i32 %977, %978
  %979 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 5, i32 15), align 1
  %idxprom2126 = zext i8 %979 to i32
  %arrayidx2127 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2126
  %980 = load i32, i32* %arrayidx2127, align 4
  %add2128 = add i32 %add2125, %980
  %arrayidx2129 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2128, i32* %arrayidx2129, align 4
  %arrayidx2130 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %981 = load i32, i32* %arrayidx2130, align 8
  %arrayidx2131 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %982 = load i32, i32* %arrayidx2131, align 4
  %xor2132 = xor i32 %981, %982
  %call2133 = call i32 @rotr32(i32 %xor2132, i32 8)
  %arrayidx2134 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2133, i32* %arrayidx2134, align 8
  %arrayidx2135 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %983 = load i32, i32* %arrayidx2135, align 4
  %arrayidx2136 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %984 = load i32, i32* %arrayidx2136, align 8
  %add2137 = add i32 %983, %984
  %arrayidx2138 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2137, i32* %arrayidx2138, align 4
  %arrayidx2139 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %985 = load i32, i32* %arrayidx2139, align 16
  %arrayidx2140 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %986 = load i32, i32* %arrayidx2140, align 4
  %xor2141 = xor i32 %985, %986
  %call2142 = call i32 @rotr32(i32 %xor2141, i32 7)
  %arrayidx2143 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2142, i32* %arrayidx2143, align 16
  br label %do.end2144

do.end2144:                                       ; preds = %do.body2101
  br label %do.end2145

do.end2145:                                       ; preds = %do.end2144
  br label %do.body2146

do.body2146:                                      ; preds = %do.end2145
  br label %do.body2147

do.body2147:                                      ; preds = %do.body2146
  %arrayidx2148 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %987 = load i32, i32* %arrayidx2148, align 16
  %arrayidx2149 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %988 = load i32, i32* %arrayidx2149, align 16
  %add2150 = add i32 %987, %988
  %989 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 0), align 16
  %idxprom2151 = zext i8 %989 to i32
  %arrayidx2152 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2151
  %990 = load i32, i32* %arrayidx2152, align 4
  %add2153 = add i32 %add2150, %990
  %arrayidx2154 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2153, i32* %arrayidx2154, align 16
  %arrayidx2155 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %991 = load i32, i32* %arrayidx2155, align 16
  %arrayidx2156 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %992 = load i32, i32* %arrayidx2156, align 16
  %xor2157 = xor i32 %991, %992
  %call2158 = call i32 @rotr32(i32 %xor2157, i32 16)
  %arrayidx2159 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2158, i32* %arrayidx2159, align 16
  %arrayidx2160 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %993 = load i32, i32* %arrayidx2160, align 16
  %arrayidx2161 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %994 = load i32, i32* %arrayidx2161, align 16
  %add2162 = add i32 %993, %994
  %arrayidx2163 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2162, i32* %arrayidx2163, align 16
  %arrayidx2164 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %995 = load i32, i32* %arrayidx2164, align 16
  %arrayidx2165 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %996 = load i32, i32* %arrayidx2165, align 16
  %xor2166 = xor i32 %995, %996
  %call2167 = call i32 @rotr32(i32 %xor2166, i32 12)
  %arrayidx2168 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2167, i32* %arrayidx2168, align 16
  %arrayidx2169 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %997 = load i32, i32* %arrayidx2169, align 16
  %arrayidx2170 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %998 = load i32, i32* %arrayidx2170, align 16
  %add2171 = add i32 %997, %998
  %999 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 1), align 1
  %idxprom2172 = zext i8 %999 to i32
  %arrayidx2173 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2172
  %1000 = load i32, i32* %arrayidx2173, align 4
  %add2174 = add i32 %add2171, %1000
  %arrayidx2175 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2174, i32* %arrayidx2175, align 16
  %arrayidx2176 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1001 = load i32, i32* %arrayidx2176, align 16
  %arrayidx2177 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1002 = load i32, i32* %arrayidx2177, align 16
  %xor2178 = xor i32 %1001, %1002
  %call2179 = call i32 @rotr32(i32 %xor2178, i32 8)
  %arrayidx2180 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2179, i32* %arrayidx2180, align 16
  %arrayidx2181 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1003 = load i32, i32* %arrayidx2181, align 16
  %arrayidx2182 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1004 = load i32, i32* %arrayidx2182, align 16
  %add2183 = add i32 %1003, %1004
  %arrayidx2184 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2183, i32* %arrayidx2184, align 16
  %arrayidx2185 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1005 = load i32, i32* %arrayidx2185, align 16
  %arrayidx2186 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1006 = load i32, i32* %arrayidx2186, align 16
  %xor2187 = xor i32 %1005, %1006
  %call2188 = call i32 @rotr32(i32 %xor2187, i32 7)
  %arrayidx2189 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2188, i32* %arrayidx2189, align 16
  br label %do.end2190

do.end2190:                                       ; preds = %do.body2147
  br label %do.body2191

do.body2191:                                      ; preds = %do.end2190
  %arrayidx2192 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1007 = load i32, i32* %arrayidx2192, align 4
  %arrayidx2193 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1008 = load i32, i32* %arrayidx2193, align 4
  %add2194 = add i32 %1007, %1008
  %1009 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 2), align 2
  %idxprom2195 = zext i8 %1009 to i32
  %arrayidx2196 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2195
  %1010 = load i32, i32* %arrayidx2196, align 4
  %add2197 = add i32 %add2194, %1010
  %arrayidx2198 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2197, i32* %arrayidx2198, align 4
  %arrayidx2199 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1011 = load i32, i32* %arrayidx2199, align 4
  %arrayidx2200 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1012 = load i32, i32* %arrayidx2200, align 4
  %xor2201 = xor i32 %1011, %1012
  %call2202 = call i32 @rotr32(i32 %xor2201, i32 16)
  %arrayidx2203 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2202, i32* %arrayidx2203, align 4
  %arrayidx2204 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1013 = load i32, i32* %arrayidx2204, align 4
  %arrayidx2205 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1014 = load i32, i32* %arrayidx2205, align 4
  %add2206 = add i32 %1013, %1014
  %arrayidx2207 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2206, i32* %arrayidx2207, align 4
  %arrayidx2208 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1015 = load i32, i32* %arrayidx2208, align 4
  %arrayidx2209 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1016 = load i32, i32* %arrayidx2209, align 4
  %xor2210 = xor i32 %1015, %1016
  %call2211 = call i32 @rotr32(i32 %xor2210, i32 12)
  %arrayidx2212 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2211, i32* %arrayidx2212, align 4
  %arrayidx2213 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1017 = load i32, i32* %arrayidx2213, align 4
  %arrayidx2214 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1018 = load i32, i32* %arrayidx2214, align 4
  %add2215 = add i32 %1017, %1018
  %1019 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 3), align 1
  %idxprom2216 = zext i8 %1019 to i32
  %arrayidx2217 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2216
  %1020 = load i32, i32* %arrayidx2217, align 4
  %add2218 = add i32 %add2215, %1020
  %arrayidx2219 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2218, i32* %arrayidx2219, align 4
  %arrayidx2220 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1021 = load i32, i32* %arrayidx2220, align 4
  %arrayidx2221 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1022 = load i32, i32* %arrayidx2221, align 4
  %xor2222 = xor i32 %1021, %1022
  %call2223 = call i32 @rotr32(i32 %xor2222, i32 8)
  %arrayidx2224 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2223, i32* %arrayidx2224, align 4
  %arrayidx2225 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1023 = load i32, i32* %arrayidx2225, align 4
  %arrayidx2226 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1024 = load i32, i32* %arrayidx2226, align 4
  %add2227 = add i32 %1023, %1024
  %arrayidx2228 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2227, i32* %arrayidx2228, align 4
  %arrayidx2229 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1025 = load i32, i32* %arrayidx2229, align 4
  %arrayidx2230 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1026 = load i32, i32* %arrayidx2230, align 4
  %xor2231 = xor i32 %1025, %1026
  %call2232 = call i32 @rotr32(i32 %xor2231, i32 7)
  %arrayidx2233 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2232, i32* %arrayidx2233, align 4
  br label %do.end2234

do.end2234:                                       ; preds = %do.body2191
  br label %do.body2235

do.body2235:                                      ; preds = %do.end2234
  %arrayidx2236 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1027 = load i32, i32* %arrayidx2236, align 8
  %arrayidx2237 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1028 = load i32, i32* %arrayidx2237, align 8
  %add2238 = add i32 %1027, %1028
  %1029 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 4), align 4
  %idxprom2239 = zext i8 %1029 to i32
  %arrayidx2240 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2239
  %1030 = load i32, i32* %arrayidx2240, align 4
  %add2241 = add i32 %add2238, %1030
  %arrayidx2242 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2241, i32* %arrayidx2242, align 8
  %arrayidx2243 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1031 = load i32, i32* %arrayidx2243, align 8
  %arrayidx2244 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1032 = load i32, i32* %arrayidx2244, align 8
  %xor2245 = xor i32 %1031, %1032
  %call2246 = call i32 @rotr32(i32 %xor2245, i32 16)
  %arrayidx2247 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2246, i32* %arrayidx2247, align 8
  %arrayidx2248 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1033 = load i32, i32* %arrayidx2248, align 8
  %arrayidx2249 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1034 = load i32, i32* %arrayidx2249, align 8
  %add2250 = add i32 %1033, %1034
  %arrayidx2251 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2250, i32* %arrayidx2251, align 8
  %arrayidx2252 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1035 = load i32, i32* %arrayidx2252, align 8
  %arrayidx2253 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1036 = load i32, i32* %arrayidx2253, align 8
  %xor2254 = xor i32 %1035, %1036
  %call2255 = call i32 @rotr32(i32 %xor2254, i32 12)
  %arrayidx2256 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2255, i32* %arrayidx2256, align 8
  %arrayidx2257 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1037 = load i32, i32* %arrayidx2257, align 8
  %arrayidx2258 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1038 = load i32, i32* %arrayidx2258, align 8
  %add2259 = add i32 %1037, %1038
  %1039 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 5), align 1
  %idxprom2260 = zext i8 %1039 to i32
  %arrayidx2261 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2260
  %1040 = load i32, i32* %arrayidx2261, align 4
  %add2262 = add i32 %add2259, %1040
  %arrayidx2263 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2262, i32* %arrayidx2263, align 8
  %arrayidx2264 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1041 = load i32, i32* %arrayidx2264, align 8
  %arrayidx2265 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1042 = load i32, i32* %arrayidx2265, align 8
  %xor2266 = xor i32 %1041, %1042
  %call2267 = call i32 @rotr32(i32 %xor2266, i32 8)
  %arrayidx2268 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2267, i32* %arrayidx2268, align 8
  %arrayidx2269 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1043 = load i32, i32* %arrayidx2269, align 8
  %arrayidx2270 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1044 = load i32, i32* %arrayidx2270, align 8
  %add2271 = add i32 %1043, %1044
  %arrayidx2272 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2271, i32* %arrayidx2272, align 8
  %arrayidx2273 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1045 = load i32, i32* %arrayidx2273, align 8
  %arrayidx2274 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1046 = load i32, i32* %arrayidx2274, align 8
  %xor2275 = xor i32 %1045, %1046
  %call2276 = call i32 @rotr32(i32 %xor2275, i32 7)
  %arrayidx2277 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2276, i32* %arrayidx2277, align 8
  br label %do.end2278

do.end2278:                                       ; preds = %do.body2235
  br label %do.body2279

do.body2279:                                      ; preds = %do.end2278
  %arrayidx2280 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1047 = load i32, i32* %arrayidx2280, align 4
  %arrayidx2281 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1048 = load i32, i32* %arrayidx2281, align 4
  %add2282 = add i32 %1047, %1048
  %1049 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 6), align 2
  %idxprom2283 = zext i8 %1049 to i32
  %arrayidx2284 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2283
  %1050 = load i32, i32* %arrayidx2284, align 4
  %add2285 = add i32 %add2282, %1050
  %arrayidx2286 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2285, i32* %arrayidx2286, align 4
  %arrayidx2287 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1051 = load i32, i32* %arrayidx2287, align 4
  %arrayidx2288 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1052 = load i32, i32* %arrayidx2288, align 4
  %xor2289 = xor i32 %1051, %1052
  %call2290 = call i32 @rotr32(i32 %xor2289, i32 16)
  %arrayidx2291 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2290, i32* %arrayidx2291, align 4
  %arrayidx2292 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1053 = load i32, i32* %arrayidx2292, align 4
  %arrayidx2293 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1054 = load i32, i32* %arrayidx2293, align 4
  %add2294 = add i32 %1053, %1054
  %arrayidx2295 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2294, i32* %arrayidx2295, align 4
  %arrayidx2296 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1055 = load i32, i32* %arrayidx2296, align 4
  %arrayidx2297 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1056 = load i32, i32* %arrayidx2297, align 4
  %xor2298 = xor i32 %1055, %1056
  %call2299 = call i32 @rotr32(i32 %xor2298, i32 12)
  %arrayidx2300 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2299, i32* %arrayidx2300, align 4
  %arrayidx2301 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1057 = load i32, i32* %arrayidx2301, align 4
  %arrayidx2302 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1058 = load i32, i32* %arrayidx2302, align 4
  %add2303 = add i32 %1057, %1058
  %1059 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 7), align 1
  %idxprom2304 = zext i8 %1059 to i32
  %arrayidx2305 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2304
  %1060 = load i32, i32* %arrayidx2305, align 4
  %add2306 = add i32 %add2303, %1060
  %arrayidx2307 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2306, i32* %arrayidx2307, align 4
  %arrayidx2308 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1061 = load i32, i32* %arrayidx2308, align 4
  %arrayidx2309 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1062 = load i32, i32* %arrayidx2309, align 4
  %xor2310 = xor i32 %1061, %1062
  %call2311 = call i32 @rotr32(i32 %xor2310, i32 8)
  %arrayidx2312 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2311, i32* %arrayidx2312, align 4
  %arrayidx2313 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1063 = load i32, i32* %arrayidx2313, align 4
  %arrayidx2314 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1064 = load i32, i32* %arrayidx2314, align 4
  %add2315 = add i32 %1063, %1064
  %arrayidx2316 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2315, i32* %arrayidx2316, align 4
  %arrayidx2317 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1065 = load i32, i32* %arrayidx2317, align 4
  %arrayidx2318 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1066 = load i32, i32* %arrayidx2318, align 4
  %xor2319 = xor i32 %1065, %1066
  %call2320 = call i32 @rotr32(i32 %xor2319, i32 7)
  %arrayidx2321 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2320, i32* %arrayidx2321, align 4
  br label %do.end2322

do.end2322:                                       ; preds = %do.body2279
  br label %do.body2323

do.body2323:                                      ; preds = %do.end2322
  %arrayidx2324 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1067 = load i32, i32* %arrayidx2324, align 16
  %arrayidx2325 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1068 = load i32, i32* %arrayidx2325, align 4
  %add2326 = add i32 %1067, %1068
  %1069 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 8), align 8
  %idxprom2327 = zext i8 %1069 to i32
  %arrayidx2328 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2327
  %1070 = load i32, i32* %arrayidx2328, align 4
  %add2329 = add i32 %add2326, %1070
  %arrayidx2330 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2329, i32* %arrayidx2330, align 16
  %arrayidx2331 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1071 = load i32, i32* %arrayidx2331, align 4
  %arrayidx2332 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1072 = load i32, i32* %arrayidx2332, align 16
  %xor2333 = xor i32 %1071, %1072
  %call2334 = call i32 @rotr32(i32 %xor2333, i32 16)
  %arrayidx2335 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2334, i32* %arrayidx2335, align 4
  %arrayidx2336 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1073 = load i32, i32* %arrayidx2336, align 8
  %arrayidx2337 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1074 = load i32, i32* %arrayidx2337, align 4
  %add2338 = add i32 %1073, %1074
  %arrayidx2339 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2338, i32* %arrayidx2339, align 8
  %arrayidx2340 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1075 = load i32, i32* %arrayidx2340, align 4
  %arrayidx2341 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1076 = load i32, i32* %arrayidx2341, align 8
  %xor2342 = xor i32 %1075, %1076
  %call2343 = call i32 @rotr32(i32 %xor2342, i32 12)
  %arrayidx2344 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2343, i32* %arrayidx2344, align 4
  %arrayidx2345 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1077 = load i32, i32* %arrayidx2345, align 16
  %arrayidx2346 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1078 = load i32, i32* %arrayidx2346, align 4
  %add2347 = add i32 %1077, %1078
  %1079 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 9), align 1
  %idxprom2348 = zext i8 %1079 to i32
  %arrayidx2349 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2348
  %1080 = load i32, i32* %arrayidx2349, align 4
  %add2350 = add i32 %add2347, %1080
  %arrayidx2351 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2350, i32* %arrayidx2351, align 16
  %arrayidx2352 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1081 = load i32, i32* %arrayidx2352, align 4
  %arrayidx2353 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1082 = load i32, i32* %arrayidx2353, align 16
  %xor2354 = xor i32 %1081, %1082
  %call2355 = call i32 @rotr32(i32 %xor2354, i32 8)
  %arrayidx2356 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2355, i32* %arrayidx2356, align 4
  %arrayidx2357 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1083 = load i32, i32* %arrayidx2357, align 8
  %arrayidx2358 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1084 = load i32, i32* %arrayidx2358, align 4
  %add2359 = add i32 %1083, %1084
  %arrayidx2360 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2359, i32* %arrayidx2360, align 8
  %arrayidx2361 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1085 = load i32, i32* %arrayidx2361, align 4
  %arrayidx2362 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1086 = load i32, i32* %arrayidx2362, align 8
  %xor2363 = xor i32 %1085, %1086
  %call2364 = call i32 @rotr32(i32 %xor2363, i32 7)
  %arrayidx2365 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2364, i32* %arrayidx2365, align 4
  br label %do.end2366

do.end2366:                                       ; preds = %do.body2323
  br label %do.body2367

do.body2367:                                      ; preds = %do.end2366
  %arrayidx2368 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1087 = load i32, i32* %arrayidx2368, align 4
  %arrayidx2369 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1088 = load i32, i32* %arrayidx2369, align 8
  %add2370 = add i32 %1087, %1088
  %1089 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 10), align 2
  %idxprom2371 = zext i8 %1089 to i32
  %arrayidx2372 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2371
  %1090 = load i32, i32* %arrayidx2372, align 4
  %add2373 = add i32 %add2370, %1090
  %arrayidx2374 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2373, i32* %arrayidx2374, align 4
  %arrayidx2375 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1091 = load i32, i32* %arrayidx2375, align 16
  %arrayidx2376 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1092 = load i32, i32* %arrayidx2376, align 4
  %xor2377 = xor i32 %1091, %1092
  %call2378 = call i32 @rotr32(i32 %xor2377, i32 16)
  %arrayidx2379 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2378, i32* %arrayidx2379, align 16
  %arrayidx2380 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1093 = load i32, i32* %arrayidx2380, align 4
  %arrayidx2381 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1094 = load i32, i32* %arrayidx2381, align 16
  %add2382 = add i32 %1093, %1094
  %arrayidx2383 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2382, i32* %arrayidx2383, align 4
  %arrayidx2384 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1095 = load i32, i32* %arrayidx2384, align 8
  %arrayidx2385 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1096 = load i32, i32* %arrayidx2385, align 4
  %xor2386 = xor i32 %1095, %1096
  %call2387 = call i32 @rotr32(i32 %xor2386, i32 12)
  %arrayidx2388 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2387, i32* %arrayidx2388, align 8
  %arrayidx2389 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1097 = load i32, i32* %arrayidx2389, align 4
  %arrayidx2390 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1098 = load i32, i32* %arrayidx2390, align 8
  %add2391 = add i32 %1097, %1098
  %1099 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 11), align 1
  %idxprom2392 = zext i8 %1099 to i32
  %arrayidx2393 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2392
  %1100 = load i32, i32* %arrayidx2393, align 4
  %add2394 = add i32 %add2391, %1100
  %arrayidx2395 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2394, i32* %arrayidx2395, align 4
  %arrayidx2396 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1101 = load i32, i32* %arrayidx2396, align 16
  %arrayidx2397 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1102 = load i32, i32* %arrayidx2397, align 4
  %xor2398 = xor i32 %1101, %1102
  %call2399 = call i32 @rotr32(i32 %xor2398, i32 8)
  %arrayidx2400 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2399, i32* %arrayidx2400, align 16
  %arrayidx2401 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1103 = load i32, i32* %arrayidx2401, align 4
  %arrayidx2402 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1104 = load i32, i32* %arrayidx2402, align 16
  %add2403 = add i32 %1103, %1104
  %arrayidx2404 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2403, i32* %arrayidx2404, align 4
  %arrayidx2405 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1105 = load i32, i32* %arrayidx2405, align 8
  %arrayidx2406 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1106 = load i32, i32* %arrayidx2406, align 4
  %xor2407 = xor i32 %1105, %1106
  %call2408 = call i32 @rotr32(i32 %xor2407, i32 7)
  %arrayidx2409 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2408, i32* %arrayidx2409, align 8
  br label %do.end2410

do.end2410:                                       ; preds = %do.body2367
  br label %do.body2411

do.body2411:                                      ; preds = %do.end2410
  %arrayidx2412 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1107 = load i32, i32* %arrayidx2412, align 8
  %arrayidx2413 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1108 = load i32, i32* %arrayidx2413, align 4
  %add2414 = add i32 %1107, %1108
  %1109 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 12), align 4
  %idxprom2415 = zext i8 %1109 to i32
  %arrayidx2416 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2415
  %1110 = load i32, i32* %arrayidx2416, align 4
  %add2417 = add i32 %add2414, %1110
  %arrayidx2418 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2417, i32* %arrayidx2418, align 8
  %arrayidx2419 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1111 = load i32, i32* %arrayidx2419, align 4
  %arrayidx2420 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1112 = load i32, i32* %arrayidx2420, align 8
  %xor2421 = xor i32 %1111, %1112
  %call2422 = call i32 @rotr32(i32 %xor2421, i32 16)
  %arrayidx2423 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2422, i32* %arrayidx2423, align 4
  %arrayidx2424 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1113 = load i32, i32* %arrayidx2424, align 16
  %arrayidx2425 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1114 = load i32, i32* %arrayidx2425, align 4
  %add2426 = add i32 %1113, %1114
  %arrayidx2427 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2426, i32* %arrayidx2427, align 16
  %arrayidx2428 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1115 = load i32, i32* %arrayidx2428, align 4
  %arrayidx2429 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1116 = load i32, i32* %arrayidx2429, align 16
  %xor2430 = xor i32 %1115, %1116
  %call2431 = call i32 @rotr32(i32 %xor2430, i32 12)
  %arrayidx2432 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2431, i32* %arrayidx2432, align 4
  %arrayidx2433 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1117 = load i32, i32* %arrayidx2433, align 8
  %arrayidx2434 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1118 = load i32, i32* %arrayidx2434, align 4
  %add2435 = add i32 %1117, %1118
  %1119 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 13), align 1
  %idxprom2436 = zext i8 %1119 to i32
  %arrayidx2437 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2436
  %1120 = load i32, i32* %arrayidx2437, align 4
  %add2438 = add i32 %add2435, %1120
  %arrayidx2439 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2438, i32* %arrayidx2439, align 8
  %arrayidx2440 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1121 = load i32, i32* %arrayidx2440, align 4
  %arrayidx2441 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1122 = load i32, i32* %arrayidx2441, align 8
  %xor2442 = xor i32 %1121, %1122
  %call2443 = call i32 @rotr32(i32 %xor2442, i32 8)
  %arrayidx2444 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2443, i32* %arrayidx2444, align 4
  %arrayidx2445 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1123 = load i32, i32* %arrayidx2445, align 16
  %arrayidx2446 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1124 = load i32, i32* %arrayidx2446, align 4
  %add2447 = add i32 %1123, %1124
  %arrayidx2448 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2447, i32* %arrayidx2448, align 16
  %arrayidx2449 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1125 = load i32, i32* %arrayidx2449, align 4
  %arrayidx2450 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1126 = load i32, i32* %arrayidx2450, align 16
  %xor2451 = xor i32 %1125, %1126
  %call2452 = call i32 @rotr32(i32 %xor2451, i32 7)
  %arrayidx2453 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2452, i32* %arrayidx2453, align 4
  br label %do.end2454

do.end2454:                                       ; preds = %do.body2411
  br label %do.body2455

do.body2455:                                      ; preds = %do.end2454
  %arrayidx2456 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1127 = load i32, i32* %arrayidx2456, align 4
  %arrayidx2457 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1128 = load i32, i32* %arrayidx2457, align 16
  %add2458 = add i32 %1127, %1128
  %1129 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 14), align 2
  %idxprom2459 = zext i8 %1129 to i32
  %arrayidx2460 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2459
  %1130 = load i32, i32* %arrayidx2460, align 4
  %add2461 = add i32 %add2458, %1130
  %arrayidx2462 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2461, i32* %arrayidx2462, align 4
  %arrayidx2463 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1131 = load i32, i32* %arrayidx2463, align 8
  %arrayidx2464 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1132 = load i32, i32* %arrayidx2464, align 4
  %xor2465 = xor i32 %1131, %1132
  %call2466 = call i32 @rotr32(i32 %xor2465, i32 16)
  %arrayidx2467 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2466, i32* %arrayidx2467, align 8
  %arrayidx2468 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1133 = load i32, i32* %arrayidx2468, align 4
  %arrayidx2469 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1134 = load i32, i32* %arrayidx2469, align 8
  %add2470 = add i32 %1133, %1134
  %arrayidx2471 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2470, i32* %arrayidx2471, align 4
  %arrayidx2472 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1135 = load i32, i32* %arrayidx2472, align 16
  %arrayidx2473 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1136 = load i32, i32* %arrayidx2473, align 4
  %xor2474 = xor i32 %1135, %1136
  %call2475 = call i32 @rotr32(i32 %xor2474, i32 12)
  %arrayidx2476 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2475, i32* %arrayidx2476, align 16
  %arrayidx2477 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1137 = load i32, i32* %arrayidx2477, align 4
  %arrayidx2478 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1138 = load i32, i32* %arrayidx2478, align 16
  %add2479 = add i32 %1137, %1138
  %1139 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 6, i32 15), align 1
  %idxprom2480 = zext i8 %1139 to i32
  %arrayidx2481 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2480
  %1140 = load i32, i32* %arrayidx2481, align 4
  %add2482 = add i32 %add2479, %1140
  %arrayidx2483 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2482, i32* %arrayidx2483, align 4
  %arrayidx2484 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1141 = load i32, i32* %arrayidx2484, align 8
  %arrayidx2485 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1142 = load i32, i32* %arrayidx2485, align 4
  %xor2486 = xor i32 %1141, %1142
  %call2487 = call i32 @rotr32(i32 %xor2486, i32 8)
  %arrayidx2488 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2487, i32* %arrayidx2488, align 8
  %arrayidx2489 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1143 = load i32, i32* %arrayidx2489, align 4
  %arrayidx2490 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1144 = load i32, i32* %arrayidx2490, align 8
  %add2491 = add i32 %1143, %1144
  %arrayidx2492 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2491, i32* %arrayidx2492, align 4
  %arrayidx2493 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1145 = load i32, i32* %arrayidx2493, align 16
  %arrayidx2494 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1146 = load i32, i32* %arrayidx2494, align 4
  %xor2495 = xor i32 %1145, %1146
  %call2496 = call i32 @rotr32(i32 %xor2495, i32 7)
  %arrayidx2497 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2496, i32* %arrayidx2497, align 16
  br label %do.end2498

do.end2498:                                       ; preds = %do.body2455
  br label %do.end2499

do.end2499:                                       ; preds = %do.end2498
  br label %do.body2500

do.body2500:                                      ; preds = %do.end2499
  br label %do.body2501

do.body2501:                                      ; preds = %do.body2500
  %arrayidx2502 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1147 = load i32, i32* %arrayidx2502, align 16
  %arrayidx2503 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1148 = load i32, i32* %arrayidx2503, align 16
  %add2504 = add i32 %1147, %1148
  %1149 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 0), align 16
  %idxprom2505 = zext i8 %1149 to i32
  %arrayidx2506 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2505
  %1150 = load i32, i32* %arrayidx2506, align 4
  %add2507 = add i32 %add2504, %1150
  %arrayidx2508 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2507, i32* %arrayidx2508, align 16
  %arrayidx2509 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1151 = load i32, i32* %arrayidx2509, align 16
  %arrayidx2510 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1152 = load i32, i32* %arrayidx2510, align 16
  %xor2511 = xor i32 %1151, %1152
  %call2512 = call i32 @rotr32(i32 %xor2511, i32 16)
  %arrayidx2513 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2512, i32* %arrayidx2513, align 16
  %arrayidx2514 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1153 = load i32, i32* %arrayidx2514, align 16
  %arrayidx2515 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1154 = load i32, i32* %arrayidx2515, align 16
  %add2516 = add i32 %1153, %1154
  %arrayidx2517 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2516, i32* %arrayidx2517, align 16
  %arrayidx2518 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1155 = load i32, i32* %arrayidx2518, align 16
  %arrayidx2519 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1156 = load i32, i32* %arrayidx2519, align 16
  %xor2520 = xor i32 %1155, %1156
  %call2521 = call i32 @rotr32(i32 %xor2520, i32 12)
  %arrayidx2522 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2521, i32* %arrayidx2522, align 16
  %arrayidx2523 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1157 = load i32, i32* %arrayidx2523, align 16
  %arrayidx2524 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1158 = load i32, i32* %arrayidx2524, align 16
  %add2525 = add i32 %1157, %1158
  %1159 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 1), align 1
  %idxprom2526 = zext i8 %1159 to i32
  %arrayidx2527 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2526
  %1160 = load i32, i32* %arrayidx2527, align 4
  %add2528 = add i32 %add2525, %1160
  %arrayidx2529 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2528, i32* %arrayidx2529, align 16
  %arrayidx2530 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1161 = load i32, i32* %arrayidx2530, align 16
  %arrayidx2531 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1162 = load i32, i32* %arrayidx2531, align 16
  %xor2532 = xor i32 %1161, %1162
  %call2533 = call i32 @rotr32(i32 %xor2532, i32 8)
  %arrayidx2534 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2533, i32* %arrayidx2534, align 16
  %arrayidx2535 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1163 = load i32, i32* %arrayidx2535, align 16
  %arrayidx2536 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1164 = load i32, i32* %arrayidx2536, align 16
  %add2537 = add i32 %1163, %1164
  %arrayidx2538 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2537, i32* %arrayidx2538, align 16
  %arrayidx2539 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1165 = load i32, i32* %arrayidx2539, align 16
  %arrayidx2540 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1166 = load i32, i32* %arrayidx2540, align 16
  %xor2541 = xor i32 %1165, %1166
  %call2542 = call i32 @rotr32(i32 %xor2541, i32 7)
  %arrayidx2543 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2542, i32* %arrayidx2543, align 16
  br label %do.end2544

do.end2544:                                       ; preds = %do.body2501
  br label %do.body2545

do.body2545:                                      ; preds = %do.end2544
  %arrayidx2546 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1167 = load i32, i32* %arrayidx2546, align 4
  %arrayidx2547 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1168 = load i32, i32* %arrayidx2547, align 4
  %add2548 = add i32 %1167, %1168
  %1169 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 2), align 2
  %idxprom2549 = zext i8 %1169 to i32
  %arrayidx2550 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2549
  %1170 = load i32, i32* %arrayidx2550, align 4
  %add2551 = add i32 %add2548, %1170
  %arrayidx2552 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2551, i32* %arrayidx2552, align 4
  %arrayidx2553 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1171 = load i32, i32* %arrayidx2553, align 4
  %arrayidx2554 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1172 = load i32, i32* %arrayidx2554, align 4
  %xor2555 = xor i32 %1171, %1172
  %call2556 = call i32 @rotr32(i32 %xor2555, i32 16)
  %arrayidx2557 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2556, i32* %arrayidx2557, align 4
  %arrayidx2558 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1173 = load i32, i32* %arrayidx2558, align 4
  %arrayidx2559 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1174 = load i32, i32* %arrayidx2559, align 4
  %add2560 = add i32 %1173, %1174
  %arrayidx2561 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2560, i32* %arrayidx2561, align 4
  %arrayidx2562 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1175 = load i32, i32* %arrayidx2562, align 4
  %arrayidx2563 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1176 = load i32, i32* %arrayidx2563, align 4
  %xor2564 = xor i32 %1175, %1176
  %call2565 = call i32 @rotr32(i32 %xor2564, i32 12)
  %arrayidx2566 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2565, i32* %arrayidx2566, align 4
  %arrayidx2567 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1177 = load i32, i32* %arrayidx2567, align 4
  %arrayidx2568 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1178 = load i32, i32* %arrayidx2568, align 4
  %add2569 = add i32 %1177, %1178
  %1179 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 3), align 1
  %idxprom2570 = zext i8 %1179 to i32
  %arrayidx2571 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2570
  %1180 = load i32, i32* %arrayidx2571, align 4
  %add2572 = add i32 %add2569, %1180
  %arrayidx2573 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2572, i32* %arrayidx2573, align 4
  %arrayidx2574 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1181 = load i32, i32* %arrayidx2574, align 4
  %arrayidx2575 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1182 = load i32, i32* %arrayidx2575, align 4
  %xor2576 = xor i32 %1181, %1182
  %call2577 = call i32 @rotr32(i32 %xor2576, i32 8)
  %arrayidx2578 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2577, i32* %arrayidx2578, align 4
  %arrayidx2579 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1183 = load i32, i32* %arrayidx2579, align 4
  %arrayidx2580 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1184 = load i32, i32* %arrayidx2580, align 4
  %add2581 = add i32 %1183, %1184
  %arrayidx2582 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2581, i32* %arrayidx2582, align 4
  %arrayidx2583 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1185 = load i32, i32* %arrayidx2583, align 4
  %arrayidx2584 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1186 = load i32, i32* %arrayidx2584, align 4
  %xor2585 = xor i32 %1185, %1186
  %call2586 = call i32 @rotr32(i32 %xor2585, i32 7)
  %arrayidx2587 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2586, i32* %arrayidx2587, align 4
  br label %do.end2588

do.end2588:                                       ; preds = %do.body2545
  br label %do.body2589

do.body2589:                                      ; preds = %do.end2588
  %arrayidx2590 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1187 = load i32, i32* %arrayidx2590, align 8
  %arrayidx2591 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1188 = load i32, i32* %arrayidx2591, align 8
  %add2592 = add i32 %1187, %1188
  %1189 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 4), align 4
  %idxprom2593 = zext i8 %1189 to i32
  %arrayidx2594 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2593
  %1190 = load i32, i32* %arrayidx2594, align 4
  %add2595 = add i32 %add2592, %1190
  %arrayidx2596 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2595, i32* %arrayidx2596, align 8
  %arrayidx2597 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1191 = load i32, i32* %arrayidx2597, align 8
  %arrayidx2598 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1192 = load i32, i32* %arrayidx2598, align 8
  %xor2599 = xor i32 %1191, %1192
  %call2600 = call i32 @rotr32(i32 %xor2599, i32 16)
  %arrayidx2601 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2600, i32* %arrayidx2601, align 8
  %arrayidx2602 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1193 = load i32, i32* %arrayidx2602, align 8
  %arrayidx2603 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1194 = load i32, i32* %arrayidx2603, align 8
  %add2604 = add i32 %1193, %1194
  %arrayidx2605 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2604, i32* %arrayidx2605, align 8
  %arrayidx2606 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1195 = load i32, i32* %arrayidx2606, align 8
  %arrayidx2607 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1196 = load i32, i32* %arrayidx2607, align 8
  %xor2608 = xor i32 %1195, %1196
  %call2609 = call i32 @rotr32(i32 %xor2608, i32 12)
  %arrayidx2610 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2609, i32* %arrayidx2610, align 8
  %arrayidx2611 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1197 = load i32, i32* %arrayidx2611, align 8
  %arrayidx2612 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1198 = load i32, i32* %arrayidx2612, align 8
  %add2613 = add i32 %1197, %1198
  %1199 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 5), align 1
  %idxprom2614 = zext i8 %1199 to i32
  %arrayidx2615 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2614
  %1200 = load i32, i32* %arrayidx2615, align 4
  %add2616 = add i32 %add2613, %1200
  %arrayidx2617 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2616, i32* %arrayidx2617, align 8
  %arrayidx2618 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1201 = load i32, i32* %arrayidx2618, align 8
  %arrayidx2619 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1202 = load i32, i32* %arrayidx2619, align 8
  %xor2620 = xor i32 %1201, %1202
  %call2621 = call i32 @rotr32(i32 %xor2620, i32 8)
  %arrayidx2622 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2621, i32* %arrayidx2622, align 8
  %arrayidx2623 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1203 = load i32, i32* %arrayidx2623, align 8
  %arrayidx2624 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1204 = load i32, i32* %arrayidx2624, align 8
  %add2625 = add i32 %1203, %1204
  %arrayidx2626 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2625, i32* %arrayidx2626, align 8
  %arrayidx2627 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1205 = load i32, i32* %arrayidx2627, align 8
  %arrayidx2628 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1206 = load i32, i32* %arrayidx2628, align 8
  %xor2629 = xor i32 %1205, %1206
  %call2630 = call i32 @rotr32(i32 %xor2629, i32 7)
  %arrayidx2631 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2630, i32* %arrayidx2631, align 8
  br label %do.end2632

do.end2632:                                       ; preds = %do.body2589
  br label %do.body2633

do.body2633:                                      ; preds = %do.end2632
  %arrayidx2634 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1207 = load i32, i32* %arrayidx2634, align 4
  %arrayidx2635 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1208 = load i32, i32* %arrayidx2635, align 4
  %add2636 = add i32 %1207, %1208
  %1209 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 6), align 2
  %idxprom2637 = zext i8 %1209 to i32
  %arrayidx2638 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2637
  %1210 = load i32, i32* %arrayidx2638, align 4
  %add2639 = add i32 %add2636, %1210
  %arrayidx2640 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2639, i32* %arrayidx2640, align 4
  %arrayidx2641 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1211 = load i32, i32* %arrayidx2641, align 4
  %arrayidx2642 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1212 = load i32, i32* %arrayidx2642, align 4
  %xor2643 = xor i32 %1211, %1212
  %call2644 = call i32 @rotr32(i32 %xor2643, i32 16)
  %arrayidx2645 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2644, i32* %arrayidx2645, align 4
  %arrayidx2646 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1213 = load i32, i32* %arrayidx2646, align 4
  %arrayidx2647 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1214 = load i32, i32* %arrayidx2647, align 4
  %add2648 = add i32 %1213, %1214
  %arrayidx2649 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2648, i32* %arrayidx2649, align 4
  %arrayidx2650 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1215 = load i32, i32* %arrayidx2650, align 4
  %arrayidx2651 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1216 = load i32, i32* %arrayidx2651, align 4
  %xor2652 = xor i32 %1215, %1216
  %call2653 = call i32 @rotr32(i32 %xor2652, i32 12)
  %arrayidx2654 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2653, i32* %arrayidx2654, align 4
  %arrayidx2655 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1217 = load i32, i32* %arrayidx2655, align 4
  %arrayidx2656 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1218 = load i32, i32* %arrayidx2656, align 4
  %add2657 = add i32 %1217, %1218
  %1219 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 7), align 1
  %idxprom2658 = zext i8 %1219 to i32
  %arrayidx2659 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2658
  %1220 = load i32, i32* %arrayidx2659, align 4
  %add2660 = add i32 %add2657, %1220
  %arrayidx2661 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2660, i32* %arrayidx2661, align 4
  %arrayidx2662 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1221 = load i32, i32* %arrayidx2662, align 4
  %arrayidx2663 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1222 = load i32, i32* %arrayidx2663, align 4
  %xor2664 = xor i32 %1221, %1222
  %call2665 = call i32 @rotr32(i32 %xor2664, i32 8)
  %arrayidx2666 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2665, i32* %arrayidx2666, align 4
  %arrayidx2667 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1223 = load i32, i32* %arrayidx2667, align 4
  %arrayidx2668 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1224 = load i32, i32* %arrayidx2668, align 4
  %add2669 = add i32 %1223, %1224
  %arrayidx2670 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2669, i32* %arrayidx2670, align 4
  %arrayidx2671 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1225 = load i32, i32* %arrayidx2671, align 4
  %arrayidx2672 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1226 = load i32, i32* %arrayidx2672, align 4
  %xor2673 = xor i32 %1225, %1226
  %call2674 = call i32 @rotr32(i32 %xor2673, i32 7)
  %arrayidx2675 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2674, i32* %arrayidx2675, align 4
  br label %do.end2676

do.end2676:                                       ; preds = %do.body2633
  br label %do.body2677

do.body2677:                                      ; preds = %do.end2676
  %arrayidx2678 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1227 = load i32, i32* %arrayidx2678, align 16
  %arrayidx2679 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1228 = load i32, i32* %arrayidx2679, align 4
  %add2680 = add i32 %1227, %1228
  %1229 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 8), align 8
  %idxprom2681 = zext i8 %1229 to i32
  %arrayidx2682 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2681
  %1230 = load i32, i32* %arrayidx2682, align 4
  %add2683 = add i32 %add2680, %1230
  %arrayidx2684 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2683, i32* %arrayidx2684, align 16
  %arrayidx2685 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1231 = load i32, i32* %arrayidx2685, align 4
  %arrayidx2686 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1232 = load i32, i32* %arrayidx2686, align 16
  %xor2687 = xor i32 %1231, %1232
  %call2688 = call i32 @rotr32(i32 %xor2687, i32 16)
  %arrayidx2689 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2688, i32* %arrayidx2689, align 4
  %arrayidx2690 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1233 = load i32, i32* %arrayidx2690, align 8
  %arrayidx2691 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1234 = load i32, i32* %arrayidx2691, align 4
  %add2692 = add i32 %1233, %1234
  %arrayidx2693 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2692, i32* %arrayidx2693, align 8
  %arrayidx2694 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1235 = load i32, i32* %arrayidx2694, align 4
  %arrayidx2695 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1236 = load i32, i32* %arrayidx2695, align 8
  %xor2696 = xor i32 %1235, %1236
  %call2697 = call i32 @rotr32(i32 %xor2696, i32 12)
  %arrayidx2698 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2697, i32* %arrayidx2698, align 4
  %arrayidx2699 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1237 = load i32, i32* %arrayidx2699, align 16
  %arrayidx2700 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1238 = load i32, i32* %arrayidx2700, align 4
  %add2701 = add i32 %1237, %1238
  %1239 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 9), align 1
  %idxprom2702 = zext i8 %1239 to i32
  %arrayidx2703 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2702
  %1240 = load i32, i32* %arrayidx2703, align 4
  %add2704 = add i32 %add2701, %1240
  %arrayidx2705 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2704, i32* %arrayidx2705, align 16
  %arrayidx2706 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1241 = load i32, i32* %arrayidx2706, align 4
  %arrayidx2707 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1242 = load i32, i32* %arrayidx2707, align 16
  %xor2708 = xor i32 %1241, %1242
  %call2709 = call i32 @rotr32(i32 %xor2708, i32 8)
  %arrayidx2710 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2709, i32* %arrayidx2710, align 4
  %arrayidx2711 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1243 = load i32, i32* %arrayidx2711, align 8
  %arrayidx2712 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1244 = load i32, i32* %arrayidx2712, align 4
  %add2713 = add i32 %1243, %1244
  %arrayidx2714 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2713, i32* %arrayidx2714, align 8
  %arrayidx2715 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1245 = load i32, i32* %arrayidx2715, align 4
  %arrayidx2716 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1246 = load i32, i32* %arrayidx2716, align 8
  %xor2717 = xor i32 %1245, %1246
  %call2718 = call i32 @rotr32(i32 %xor2717, i32 7)
  %arrayidx2719 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2718, i32* %arrayidx2719, align 4
  br label %do.end2720

do.end2720:                                       ; preds = %do.body2677
  br label %do.body2721

do.body2721:                                      ; preds = %do.end2720
  %arrayidx2722 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1247 = load i32, i32* %arrayidx2722, align 4
  %arrayidx2723 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1248 = load i32, i32* %arrayidx2723, align 8
  %add2724 = add i32 %1247, %1248
  %1249 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 10), align 2
  %idxprom2725 = zext i8 %1249 to i32
  %arrayidx2726 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2725
  %1250 = load i32, i32* %arrayidx2726, align 4
  %add2727 = add i32 %add2724, %1250
  %arrayidx2728 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2727, i32* %arrayidx2728, align 4
  %arrayidx2729 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1251 = load i32, i32* %arrayidx2729, align 16
  %arrayidx2730 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1252 = load i32, i32* %arrayidx2730, align 4
  %xor2731 = xor i32 %1251, %1252
  %call2732 = call i32 @rotr32(i32 %xor2731, i32 16)
  %arrayidx2733 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2732, i32* %arrayidx2733, align 16
  %arrayidx2734 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1253 = load i32, i32* %arrayidx2734, align 4
  %arrayidx2735 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1254 = load i32, i32* %arrayidx2735, align 16
  %add2736 = add i32 %1253, %1254
  %arrayidx2737 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2736, i32* %arrayidx2737, align 4
  %arrayidx2738 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1255 = load i32, i32* %arrayidx2738, align 8
  %arrayidx2739 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1256 = load i32, i32* %arrayidx2739, align 4
  %xor2740 = xor i32 %1255, %1256
  %call2741 = call i32 @rotr32(i32 %xor2740, i32 12)
  %arrayidx2742 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2741, i32* %arrayidx2742, align 8
  %arrayidx2743 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1257 = load i32, i32* %arrayidx2743, align 4
  %arrayidx2744 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1258 = load i32, i32* %arrayidx2744, align 8
  %add2745 = add i32 %1257, %1258
  %1259 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 11), align 1
  %idxprom2746 = zext i8 %1259 to i32
  %arrayidx2747 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2746
  %1260 = load i32, i32* %arrayidx2747, align 4
  %add2748 = add i32 %add2745, %1260
  %arrayidx2749 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2748, i32* %arrayidx2749, align 4
  %arrayidx2750 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1261 = load i32, i32* %arrayidx2750, align 16
  %arrayidx2751 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1262 = load i32, i32* %arrayidx2751, align 4
  %xor2752 = xor i32 %1261, %1262
  %call2753 = call i32 @rotr32(i32 %xor2752, i32 8)
  %arrayidx2754 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2753, i32* %arrayidx2754, align 16
  %arrayidx2755 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1263 = load i32, i32* %arrayidx2755, align 4
  %arrayidx2756 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1264 = load i32, i32* %arrayidx2756, align 16
  %add2757 = add i32 %1263, %1264
  %arrayidx2758 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add2757, i32* %arrayidx2758, align 4
  %arrayidx2759 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1265 = load i32, i32* %arrayidx2759, align 8
  %arrayidx2760 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1266 = load i32, i32* %arrayidx2760, align 4
  %xor2761 = xor i32 %1265, %1266
  %call2762 = call i32 @rotr32(i32 %xor2761, i32 7)
  %arrayidx2763 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2762, i32* %arrayidx2763, align 8
  br label %do.end2764

do.end2764:                                       ; preds = %do.body2721
  br label %do.body2765

do.body2765:                                      ; preds = %do.end2764
  %arrayidx2766 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1267 = load i32, i32* %arrayidx2766, align 8
  %arrayidx2767 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1268 = load i32, i32* %arrayidx2767, align 4
  %add2768 = add i32 %1267, %1268
  %1269 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 12), align 4
  %idxprom2769 = zext i8 %1269 to i32
  %arrayidx2770 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2769
  %1270 = load i32, i32* %arrayidx2770, align 4
  %add2771 = add i32 %add2768, %1270
  %arrayidx2772 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2771, i32* %arrayidx2772, align 8
  %arrayidx2773 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1271 = load i32, i32* %arrayidx2773, align 4
  %arrayidx2774 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1272 = load i32, i32* %arrayidx2774, align 8
  %xor2775 = xor i32 %1271, %1272
  %call2776 = call i32 @rotr32(i32 %xor2775, i32 16)
  %arrayidx2777 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2776, i32* %arrayidx2777, align 4
  %arrayidx2778 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1273 = load i32, i32* %arrayidx2778, align 16
  %arrayidx2779 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1274 = load i32, i32* %arrayidx2779, align 4
  %add2780 = add i32 %1273, %1274
  %arrayidx2781 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2780, i32* %arrayidx2781, align 16
  %arrayidx2782 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1275 = load i32, i32* %arrayidx2782, align 4
  %arrayidx2783 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1276 = load i32, i32* %arrayidx2783, align 16
  %xor2784 = xor i32 %1275, %1276
  %call2785 = call i32 @rotr32(i32 %xor2784, i32 12)
  %arrayidx2786 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2785, i32* %arrayidx2786, align 4
  %arrayidx2787 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1277 = load i32, i32* %arrayidx2787, align 8
  %arrayidx2788 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1278 = load i32, i32* %arrayidx2788, align 4
  %add2789 = add i32 %1277, %1278
  %1279 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 13), align 1
  %idxprom2790 = zext i8 %1279 to i32
  %arrayidx2791 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2790
  %1280 = load i32, i32* %arrayidx2791, align 4
  %add2792 = add i32 %add2789, %1280
  %arrayidx2793 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2792, i32* %arrayidx2793, align 8
  %arrayidx2794 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1281 = load i32, i32* %arrayidx2794, align 4
  %arrayidx2795 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1282 = load i32, i32* %arrayidx2795, align 8
  %xor2796 = xor i32 %1281, %1282
  %call2797 = call i32 @rotr32(i32 %xor2796, i32 8)
  %arrayidx2798 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2797, i32* %arrayidx2798, align 4
  %arrayidx2799 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1283 = load i32, i32* %arrayidx2799, align 16
  %arrayidx2800 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1284 = load i32, i32* %arrayidx2800, align 4
  %add2801 = add i32 %1283, %1284
  %arrayidx2802 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2801, i32* %arrayidx2802, align 16
  %arrayidx2803 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1285 = load i32, i32* %arrayidx2803, align 4
  %arrayidx2804 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1286 = load i32, i32* %arrayidx2804, align 16
  %xor2805 = xor i32 %1285, %1286
  %call2806 = call i32 @rotr32(i32 %xor2805, i32 7)
  %arrayidx2807 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call2806, i32* %arrayidx2807, align 4
  br label %do.end2808

do.end2808:                                       ; preds = %do.body2765
  br label %do.body2809

do.body2809:                                      ; preds = %do.end2808
  %arrayidx2810 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1287 = load i32, i32* %arrayidx2810, align 4
  %arrayidx2811 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1288 = load i32, i32* %arrayidx2811, align 16
  %add2812 = add i32 %1287, %1288
  %1289 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 14), align 2
  %idxprom2813 = zext i8 %1289 to i32
  %arrayidx2814 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2813
  %1290 = load i32, i32* %arrayidx2814, align 4
  %add2815 = add i32 %add2812, %1290
  %arrayidx2816 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2815, i32* %arrayidx2816, align 4
  %arrayidx2817 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1291 = load i32, i32* %arrayidx2817, align 8
  %arrayidx2818 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1292 = load i32, i32* %arrayidx2818, align 4
  %xor2819 = xor i32 %1291, %1292
  %call2820 = call i32 @rotr32(i32 %xor2819, i32 16)
  %arrayidx2821 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2820, i32* %arrayidx2821, align 8
  %arrayidx2822 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1293 = load i32, i32* %arrayidx2822, align 4
  %arrayidx2823 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1294 = load i32, i32* %arrayidx2823, align 8
  %add2824 = add i32 %1293, %1294
  %arrayidx2825 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2824, i32* %arrayidx2825, align 4
  %arrayidx2826 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1295 = load i32, i32* %arrayidx2826, align 16
  %arrayidx2827 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1296 = load i32, i32* %arrayidx2827, align 4
  %xor2828 = xor i32 %1295, %1296
  %call2829 = call i32 @rotr32(i32 %xor2828, i32 12)
  %arrayidx2830 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2829, i32* %arrayidx2830, align 16
  %arrayidx2831 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1297 = load i32, i32* %arrayidx2831, align 4
  %arrayidx2832 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1298 = load i32, i32* %arrayidx2832, align 16
  %add2833 = add i32 %1297, %1298
  %1299 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 7, i32 15), align 1
  %idxprom2834 = zext i8 %1299 to i32
  %arrayidx2835 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2834
  %1300 = load i32, i32* %arrayidx2835, align 4
  %add2836 = add i32 %add2833, %1300
  %arrayidx2837 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2836, i32* %arrayidx2837, align 4
  %arrayidx2838 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1301 = load i32, i32* %arrayidx2838, align 8
  %arrayidx2839 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1302 = load i32, i32* %arrayidx2839, align 4
  %xor2840 = xor i32 %1301, %1302
  %call2841 = call i32 @rotr32(i32 %xor2840, i32 8)
  %arrayidx2842 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2841, i32* %arrayidx2842, align 8
  %arrayidx2843 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1303 = load i32, i32* %arrayidx2843, align 4
  %arrayidx2844 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1304 = load i32, i32* %arrayidx2844, align 8
  %add2845 = add i32 %1303, %1304
  %arrayidx2846 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2845, i32* %arrayidx2846, align 4
  %arrayidx2847 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1305 = load i32, i32* %arrayidx2847, align 16
  %arrayidx2848 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1306 = load i32, i32* %arrayidx2848, align 4
  %xor2849 = xor i32 %1305, %1306
  %call2850 = call i32 @rotr32(i32 %xor2849, i32 7)
  %arrayidx2851 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2850, i32* %arrayidx2851, align 16
  br label %do.end2852

do.end2852:                                       ; preds = %do.body2809
  br label %do.end2853

do.end2853:                                       ; preds = %do.end2852
  br label %do.body2854

do.body2854:                                      ; preds = %do.end2853
  br label %do.body2855

do.body2855:                                      ; preds = %do.body2854
  %arrayidx2856 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1307 = load i32, i32* %arrayidx2856, align 16
  %arrayidx2857 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1308 = load i32, i32* %arrayidx2857, align 16
  %add2858 = add i32 %1307, %1308
  %1309 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 0), align 16
  %idxprom2859 = zext i8 %1309 to i32
  %arrayidx2860 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2859
  %1310 = load i32, i32* %arrayidx2860, align 4
  %add2861 = add i32 %add2858, %1310
  %arrayidx2862 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2861, i32* %arrayidx2862, align 16
  %arrayidx2863 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1311 = load i32, i32* %arrayidx2863, align 16
  %arrayidx2864 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1312 = load i32, i32* %arrayidx2864, align 16
  %xor2865 = xor i32 %1311, %1312
  %call2866 = call i32 @rotr32(i32 %xor2865, i32 16)
  %arrayidx2867 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2866, i32* %arrayidx2867, align 16
  %arrayidx2868 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1313 = load i32, i32* %arrayidx2868, align 16
  %arrayidx2869 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1314 = load i32, i32* %arrayidx2869, align 16
  %add2870 = add i32 %1313, %1314
  %arrayidx2871 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2870, i32* %arrayidx2871, align 16
  %arrayidx2872 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1315 = load i32, i32* %arrayidx2872, align 16
  %arrayidx2873 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1316 = load i32, i32* %arrayidx2873, align 16
  %xor2874 = xor i32 %1315, %1316
  %call2875 = call i32 @rotr32(i32 %xor2874, i32 12)
  %arrayidx2876 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2875, i32* %arrayidx2876, align 16
  %arrayidx2877 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1317 = load i32, i32* %arrayidx2877, align 16
  %arrayidx2878 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1318 = load i32, i32* %arrayidx2878, align 16
  %add2879 = add i32 %1317, %1318
  %1319 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 1), align 1
  %idxprom2880 = zext i8 %1319 to i32
  %arrayidx2881 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2880
  %1320 = load i32, i32* %arrayidx2881, align 4
  %add2882 = add i32 %add2879, %1320
  %arrayidx2883 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add2882, i32* %arrayidx2883, align 16
  %arrayidx2884 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1321 = load i32, i32* %arrayidx2884, align 16
  %arrayidx2885 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1322 = load i32, i32* %arrayidx2885, align 16
  %xor2886 = xor i32 %1321, %1322
  %call2887 = call i32 @rotr32(i32 %xor2886, i32 8)
  %arrayidx2888 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call2887, i32* %arrayidx2888, align 16
  %arrayidx2889 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1323 = load i32, i32* %arrayidx2889, align 16
  %arrayidx2890 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1324 = load i32, i32* %arrayidx2890, align 16
  %add2891 = add i32 %1323, %1324
  %arrayidx2892 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add2891, i32* %arrayidx2892, align 16
  %arrayidx2893 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1325 = load i32, i32* %arrayidx2893, align 16
  %arrayidx2894 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1326 = load i32, i32* %arrayidx2894, align 16
  %xor2895 = xor i32 %1325, %1326
  %call2896 = call i32 @rotr32(i32 %xor2895, i32 7)
  %arrayidx2897 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call2896, i32* %arrayidx2897, align 16
  br label %do.end2898

do.end2898:                                       ; preds = %do.body2855
  br label %do.body2899

do.body2899:                                      ; preds = %do.end2898
  %arrayidx2900 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1327 = load i32, i32* %arrayidx2900, align 4
  %arrayidx2901 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1328 = load i32, i32* %arrayidx2901, align 4
  %add2902 = add i32 %1327, %1328
  %1329 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 2), align 2
  %idxprom2903 = zext i8 %1329 to i32
  %arrayidx2904 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2903
  %1330 = load i32, i32* %arrayidx2904, align 4
  %add2905 = add i32 %add2902, %1330
  %arrayidx2906 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2905, i32* %arrayidx2906, align 4
  %arrayidx2907 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1331 = load i32, i32* %arrayidx2907, align 4
  %arrayidx2908 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1332 = load i32, i32* %arrayidx2908, align 4
  %xor2909 = xor i32 %1331, %1332
  %call2910 = call i32 @rotr32(i32 %xor2909, i32 16)
  %arrayidx2911 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2910, i32* %arrayidx2911, align 4
  %arrayidx2912 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1333 = load i32, i32* %arrayidx2912, align 4
  %arrayidx2913 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1334 = load i32, i32* %arrayidx2913, align 4
  %add2914 = add i32 %1333, %1334
  %arrayidx2915 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2914, i32* %arrayidx2915, align 4
  %arrayidx2916 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1335 = load i32, i32* %arrayidx2916, align 4
  %arrayidx2917 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1336 = load i32, i32* %arrayidx2917, align 4
  %xor2918 = xor i32 %1335, %1336
  %call2919 = call i32 @rotr32(i32 %xor2918, i32 12)
  %arrayidx2920 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2919, i32* %arrayidx2920, align 4
  %arrayidx2921 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1337 = load i32, i32* %arrayidx2921, align 4
  %arrayidx2922 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1338 = load i32, i32* %arrayidx2922, align 4
  %add2923 = add i32 %1337, %1338
  %1339 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 3), align 1
  %idxprom2924 = zext i8 %1339 to i32
  %arrayidx2925 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2924
  %1340 = load i32, i32* %arrayidx2925, align 4
  %add2926 = add i32 %add2923, %1340
  %arrayidx2927 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add2926, i32* %arrayidx2927, align 4
  %arrayidx2928 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1341 = load i32, i32* %arrayidx2928, align 4
  %arrayidx2929 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1342 = load i32, i32* %arrayidx2929, align 4
  %xor2930 = xor i32 %1341, %1342
  %call2931 = call i32 @rotr32(i32 %xor2930, i32 8)
  %arrayidx2932 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call2931, i32* %arrayidx2932, align 4
  %arrayidx2933 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1343 = load i32, i32* %arrayidx2933, align 4
  %arrayidx2934 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1344 = load i32, i32* %arrayidx2934, align 4
  %add2935 = add i32 %1343, %1344
  %arrayidx2936 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add2935, i32* %arrayidx2936, align 4
  %arrayidx2937 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1345 = load i32, i32* %arrayidx2937, align 4
  %arrayidx2938 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1346 = load i32, i32* %arrayidx2938, align 4
  %xor2939 = xor i32 %1345, %1346
  %call2940 = call i32 @rotr32(i32 %xor2939, i32 7)
  %arrayidx2941 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call2940, i32* %arrayidx2941, align 4
  br label %do.end2942

do.end2942:                                       ; preds = %do.body2899
  br label %do.body2943

do.body2943:                                      ; preds = %do.end2942
  %arrayidx2944 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1347 = load i32, i32* %arrayidx2944, align 8
  %arrayidx2945 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1348 = load i32, i32* %arrayidx2945, align 8
  %add2946 = add i32 %1347, %1348
  %1349 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 4), align 4
  %idxprom2947 = zext i8 %1349 to i32
  %arrayidx2948 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2947
  %1350 = load i32, i32* %arrayidx2948, align 4
  %add2949 = add i32 %add2946, %1350
  %arrayidx2950 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2949, i32* %arrayidx2950, align 8
  %arrayidx2951 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1351 = load i32, i32* %arrayidx2951, align 8
  %arrayidx2952 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1352 = load i32, i32* %arrayidx2952, align 8
  %xor2953 = xor i32 %1351, %1352
  %call2954 = call i32 @rotr32(i32 %xor2953, i32 16)
  %arrayidx2955 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2954, i32* %arrayidx2955, align 8
  %arrayidx2956 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1353 = load i32, i32* %arrayidx2956, align 8
  %arrayidx2957 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1354 = load i32, i32* %arrayidx2957, align 8
  %add2958 = add i32 %1353, %1354
  %arrayidx2959 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2958, i32* %arrayidx2959, align 8
  %arrayidx2960 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1355 = load i32, i32* %arrayidx2960, align 8
  %arrayidx2961 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1356 = load i32, i32* %arrayidx2961, align 8
  %xor2962 = xor i32 %1355, %1356
  %call2963 = call i32 @rotr32(i32 %xor2962, i32 12)
  %arrayidx2964 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2963, i32* %arrayidx2964, align 8
  %arrayidx2965 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1357 = load i32, i32* %arrayidx2965, align 8
  %arrayidx2966 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1358 = load i32, i32* %arrayidx2966, align 8
  %add2967 = add i32 %1357, %1358
  %1359 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 5), align 1
  %idxprom2968 = zext i8 %1359 to i32
  %arrayidx2969 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2968
  %1360 = load i32, i32* %arrayidx2969, align 4
  %add2970 = add i32 %add2967, %1360
  %arrayidx2971 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add2970, i32* %arrayidx2971, align 8
  %arrayidx2972 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1361 = load i32, i32* %arrayidx2972, align 8
  %arrayidx2973 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1362 = load i32, i32* %arrayidx2973, align 8
  %xor2974 = xor i32 %1361, %1362
  %call2975 = call i32 @rotr32(i32 %xor2974, i32 8)
  %arrayidx2976 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call2975, i32* %arrayidx2976, align 8
  %arrayidx2977 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1363 = load i32, i32* %arrayidx2977, align 8
  %arrayidx2978 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1364 = load i32, i32* %arrayidx2978, align 8
  %add2979 = add i32 %1363, %1364
  %arrayidx2980 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add2979, i32* %arrayidx2980, align 8
  %arrayidx2981 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1365 = load i32, i32* %arrayidx2981, align 8
  %arrayidx2982 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1366 = load i32, i32* %arrayidx2982, align 8
  %xor2983 = xor i32 %1365, %1366
  %call2984 = call i32 @rotr32(i32 %xor2983, i32 7)
  %arrayidx2985 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call2984, i32* %arrayidx2985, align 8
  br label %do.end2986

do.end2986:                                       ; preds = %do.body2943
  br label %do.body2987

do.body2987:                                      ; preds = %do.end2986
  %arrayidx2988 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1367 = load i32, i32* %arrayidx2988, align 4
  %arrayidx2989 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1368 = load i32, i32* %arrayidx2989, align 4
  %add2990 = add i32 %1367, %1368
  %1369 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 6), align 2
  %idxprom2991 = zext i8 %1369 to i32
  %arrayidx2992 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom2991
  %1370 = load i32, i32* %arrayidx2992, align 4
  %add2993 = add i32 %add2990, %1370
  %arrayidx2994 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add2993, i32* %arrayidx2994, align 4
  %arrayidx2995 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1371 = load i32, i32* %arrayidx2995, align 4
  %arrayidx2996 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1372 = load i32, i32* %arrayidx2996, align 4
  %xor2997 = xor i32 %1371, %1372
  %call2998 = call i32 @rotr32(i32 %xor2997, i32 16)
  %arrayidx2999 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call2998, i32* %arrayidx2999, align 4
  %arrayidx3000 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1373 = load i32, i32* %arrayidx3000, align 4
  %arrayidx3001 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1374 = load i32, i32* %arrayidx3001, align 4
  %add3002 = add i32 %1373, %1374
  %arrayidx3003 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3002, i32* %arrayidx3003, align 4
  %arrayidx3004 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1375 = load i32, i32* %arrayidx3004, align 4
  %arrayidx3005 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1376 = load i32, i32* %arrayidx3005, align 4
  %xor3006 = xor i32 %1375, %1376
  %call3007 = call i32 @rotr32(i32 %xor3006, i32 12)
  %arrayidx3008 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3007, i32* %arrayidx3008, align 4
  %arrayidx3009 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1377 = load i32, i32* %arrayidx3009, align 4
  %arrayidx3010 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1378 = load i32, i32* %arrayidx3010, align 4
  %add3011 = add i32 %1377, %1378
  %1379 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 7), align 1
  %idxprom3012 = zext i8 %1379 to i32
  %arrayidx3013 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3012
  %1380 = load i32, i32* %arrayidx3013, align 4
  %add3014 = add i32 %add3011, %1380
  %arrayidx3015 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add3014, i32* %arrayidx3015, align 4
  %arrayidx3016 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1381 = load i32, i32* %arrayidx3016, align 4
  %arrayidx3017 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1382 = load i32, i32* %arrayidx3017, align 4
  %xor3018 = xor i32 %1381, %1382
  %call3019 = call i32 @rotr32(i32 %xor3018, i32 8)
  %arrayidx3020 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call3019, i32* %arrayidx3020, align 4
  %arrayidx3021 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1383 = load i32, i32* %arrayidx3021, align 4
  %arrayidx3022 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1384 = load i32, i32* %arrayidx3022, align 4
  %add3023 = add i32 %1383, %1384
  %arrayidx3024 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3023, i32* %arrayidx3024, align 4
  %arrayidx3025 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1385 = load i32, i32* %arrayidx3025, align 4
  %arrayidx3026 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1386 = load i32, i32* %arrayidx3026, align 4
  %xor3027 = xor i32 %1385, %1386
  %call3028 = call i32 @rotr32(i32 %xor3027, i32 7)
  %arrayidx3029 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3028, i32* %arrayidx3029, align 4
  br label %do.end3030

do.end3030:                                       ; preds = %do.body2987
  br label %do.body3031

do.body3031:                                      ; preds = %do.end3030
  %arrayidx3032 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1387 = load i32, i32* %arrayidx3032, align 16
  %arrayidx3033 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1388 = load i32, i32* %arrayidx3033, align 4
  %add3034 = add i32 %1387, %1388
  %1389 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 8), align 8
  %idxprom3035 = zext i8 %1389 to i32
  %arrayidx3036 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3035
  %1390 = load i32, i32* %arrayidx3036, align 4
  %add3037 = add i32 %add3034, %1390
  %arrayidx3038 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add3037, i32* %arrayidx3038, align 16
  %arrayidx3039 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1391 = load i32, i32* %arrayidx3039, align 4
  %arrayidx3040 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1392 = load i32, i32* %arrayidx3040, align 16
  %xor3041 = xor i32 %1391, %1392
  %call3042 = call i32 @rotr32(i32 %xor3041, i32 16)
  %arrayidx3043 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call3042, i32* %arrayidx3043, align 4
  %arrayidx3044 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1393 = load i32, i32* %arrayidx3044, align 8
  %arrayidx3045 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1394 = load i32, i32* %arrayidx3045, align 4
  %add3046 = add i32 %1393, %1394
  %arrayidx3047 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add3046, i32* %arrayidx3047, align 8
  %arrayidx3048 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1395 = load i32, i32* %arrayidx3048, align 4
  %arrayidx3049 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1396 = load i32, i32* %arrayidx3049, align 8
  %xor3050 = xor i32 %1395, %1396
  %call3051 = call i32 @rotr32(i32 %xor3050, i32 12)
  %arrayidx3052 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call3051, i32* %arrayidx3052, align 4
  %arrayidx3053 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1397 = load i32, i32* %arrayidx3053, align 16
  %arrayidx3054 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1398 = load i32, i32* %arrayidx3054, align 4
  %add3055 = add i32 %1397, %1398
  %1399 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 9), align 1
  %idxprom3056 = zext i8 %1399 to i32
  %arrayidx3057 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3056
  %1400 = load i32, i32* %arrayidx3057, align 4
  %add3058 = add i32 %add3055, %1400
  %arrayidx3059 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add3058, i32* %arrayidx3059, align 16
  %arrayidx3060 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1401 = load i32, i32* %arrayidx3060, align 4
  %arrayidx3061 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1402 = load i32, i32* %arrayidx3061, align 16
  %xor3062 = xor i32 %1401, %1402
  %call3063 = call i32 @rotr32(i32 %xor3062, i32 8)
  %arrayidx3064 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call3063, i32* %arrayidx3064, align 4
  %arrayidx3065 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1403 = load i32, i32* %arrayidx3065, align 8
  %arrayidx3066 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1404 = load i32, i32* %arrayidx3066, align 4
  %add3067 = add i32 %1403, %1404
  %arrayidx3068 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add3067, i32* %arrayidx3068, align 8
  %arrayidx3069 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1405 = load i32, i32* %arrayidx3069, align 4
  %arrayidx3070 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1406 = load i32, i32* %arrayidx3070, align 8
  %xor3071 = xor i32 %1405, %1406
  %call3072 = call i32 @rotr32(i32 %xor3071, i32 7)
  %arrayidx3073 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call3072, i32* %arrayidx3073, align 4
  br label %do.end3074

do.end3074:                                       ; preds = %do.body3031
  br label %do.body3075

do.body3075:                                      ; preds = %do.end3074
  %arrayidx3076 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1407 = load i32, i32* %arrayidx3076, align 4
  %arrayidx3077 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1408 = load i32, i32* %arrayidx3077, align 8
  %add3078 = add i32 %1407, %1408
  %1409 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 10), align 2
  %idxprom3079 = zext i8 %1409 to i32
  %arrayidx3080 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3079
  %1410 = load i32, i32* %arrayidx3080, align 4
  %add3081 = add i32 %add3078, %1410
  %arrayidx3082 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add3081, i32* %arrayidx3082, align 4
  %arrayidx3083 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1411 = load i32, i32* %arrayidx3083, align 16
  %arrayidx3084 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1412 = load i32, i32* %arrayidx3084, align 4
  %xor3085 = xor i32 %1411, %1412
  %call3086 = call i32 @rotr32(i32 %xor3085, i32 16)
  %arrayidx3087 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call3086, i32* %arrayidx3087, align 16
  %arrayidx3088 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1413 = load i32, i32* %arrayidx3088, align 4
  %arrayidx3089 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1414 = load i32, i32* %arrayidx3089, align 16
  %add3090 = add i32 %1413, %1414
  %arrayidx3091 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3090, i32* %arrayidx3091, align 4
  %arrayidx3092 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1415 = load i32, i32* %arrayidx3092, align 8
  %arrayidx3093 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1416 = load i32, i32* %arrayidx3093, align 4
  %xor3094 = xor i32 %1415, %1416
  %call3095 = call i32 @rotr32(i32 %xor3094, i32 12)
  %arrayidx3096 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call3095, i32* %arrayidx3096, align 8
  %arrayidx3097 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1417 = load i32, i32* %arrayidx3097, align 4
  %arrayidx3098 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1418 = load i32, i32* %arrayidx3098, align 8
  %add3099 = add i32 %1417, %1418
  %1419 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 11), align 1
  %idxprom3100 = zext i8 %1419 to i32
  %arrayidx3101 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3100
  %1420 = load i32, i32* %arrayidx3101, align 4
  %add3102 = add i32 %add3099, %1420
  %arrayidx3103 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add3102, i32* %arrayidx3103, align 4
  %arrayidx3104 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1421 = load i32, i32* %arrayidx3104, align 16
  %arrayidx3105 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1422 = load i32, i32* %arrayidx3105, align 4
  %xor3106 = xor i32 %1421, %1422
  %call3107 = call i32 @rotr32(i32 %xor3106, i32 8)
  %arrayidx3108 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call3107, i32* %arrayidx3108, align 16
  %arrayidx3109 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1423 = load i32, i32* %arrayidx3109, align 4
  %arrayidx3110 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1424 = load i32, i32* %arrayidx3110, align 16
  %add3111 = add i32 %1423, %1424
  %arrayidx3112 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3111, i32* %arrayidx3112, align 4
  %arrayidx3113 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1425 = load i32, i32* %arrayidx3113, align 8
  %arrayidx3114 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1426 = load i32, i32* %arrayidx3114, align 4
  %xor3115 = xor i32 %1425, %1426
  %call3116 = call i32 @rotr32(i32 %xor3115, i32 7)
  %arrayidx3117 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call3116, i32* %arrayidx3117, align 8
  br label %do.end3118

do.end3118:                                       ; preds = %do.body3075
  br label %do.body3119

do.body3119:                                      ; preds = %do.end3118
  %arrayidx3120 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1427 = load i32, i32* %arrayidx3120, align 8
  %arrayidx3121 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1428 = load i32, i32* %arrayidx3121, align 4
  %add3122 = add i32 %1427, %1428
  %1429 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 12), align 4
  %idxprom3123 = zext i8 %1429 to i32
  %arrayidx3124 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3123
  %1430 = load i32, i32* %arrayidx3124, align 4
  %add3125 = add i32 %add3122, %1430
  %arrayidx3126 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add3125, i32* %arrayidx3126, align 8
  %arrayidx3127 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1431 = load i32, i32* %arrayidx3127, align 4
  %arrayidx3128 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1432 = load i32, i32* %arrayidx3128, align 8
  %xor3129 = xor i32 %1431, %1432
  %call3130 = call i32 @rotr32(i32 %xor3129, i32 16)
  %arrayidx3131 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call3130, i32* %arrayidx3131, align 4
  %arrayidx3132 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1433 = load i32, i32* %arrayidx3132, align 16
  %arrayidx3133 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1434 = load i32, i32* %arrayidx3133, align 4
  %add3134 = add i32 %1433, %1434
  %arrayidx3135 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add3134, i32* %arrayidx3135, align 16
  %arrayidx3136 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1435 = load i32, i32* %arrayidx3136, align 4
  %arrayidx3137 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1436 = load i32, i32* %arrayidx3137, align 16
  %xor3138 = xor i32 %1435, %1436
  %call3139 = call i32 @rotr32(i32 %xor3138, i32 12)
  %arrayidx3140 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3139, i32* %arrayidx3140, align 4
  %arrayidx3141 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1437 = load i32, i32* %arrayidx3141, align 8
  %arrayidx3142 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1438 = load i32, i32* %arrayidx3142, align 4
  %add3143 = add i32 %1437, %1438
  %1439 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 13), align 1
  %idxprom3144 = zext i8 %1439 to i32
  %arrayidx3145 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3144
  %1440 = load i32, i32* %arrayidx3145, align 4
  %add3146 = add i32 %add3143, %1440
  %arrayidx3147 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add3146, i32* %arrayidx3147, align 8
  %arrayidx3148 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1441 = load i32, i32* %arrayidx3148, align 4
  %arrayidx3149 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1442 = load i32, i32* %arrayidx3149, align 8
  %xor3150 = xor i32 %1441, %1442
  %call3151 = call i32 @rotr32(i32 %xor3150, i32 8)
  %arrayidx3152 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call3151, i32* %arrayidx3152, align 4
  %arrayidx3153 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1443 = load i32, i32* %arrayidx3153, align 16
  %arrayidx3154 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1444 = load i32, i32* %arrayidx3154, align 4
  %add3155 = add i32 %1443, %1444
  %arrayidx3156 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add3155, i32* %arrayidx3156, align 16
  %arrayidx3157 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1445 = load i32, i32* %arrayidx3157, align 4
  %arrayidx3158 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1446 = load i32, i32* %arrayidx3158, align 16
  %xor3159 = xor i32 %1445, %1446
  %call3160 = call i32 @rotr32(i32 %xor3159, i32 7)
  %arrayidx3161 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3160, i32* %arrayidx3161, align 4
  br label %do.end3162

do.end3162:                                       ; preds = %do.body3119
  br label %do.body3163

do.body3163:                                      ; preds = %do.end3162
  %arrayidx3164 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1447 = load i32, i32* %arrayidx3164, align 4
  %arrayidx3165 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1448 = load i32, i32* %arrayidx3165, align 16
  %add3166 = add i32 %1447, %1448
  %1449 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 14), align 2
  %idxprom3167 = zext i8 %1449 to i32
  %arrayidx3168 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3167
  %1450 = load i32, i32* %arrayidx3168, align 4
  %add3169 = add i32 %add3166, %1450
  %arrayidx3170 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add3169, i32* %arrayidx3170, align 4
  %arrayidx3171 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1451 = load i32, i32* %arrayidx3171, align 8
  %arrayidx3172 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1452 = load i32, i32* %arrayidx3172, align 4
  %xor3173 = xor i32 %1451, %1452
  %call3174 = call i32 @rotr32(i32 %xor3173, i32 16)
  %arrayidx3175 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call3174, i32* %arrayidx3175, align 8
  %arrayidx3176 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1453 = load i32, i32* %arrayidx3176, align 4
  %arrayidx3177 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1454 = load i32, i32* %arrayidx3177, align 8
  %add3178 = add i32 %1453, %1454
  %arrayidx3179 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add3178, i32* %arrayidx3179, align 4
  %arrayidx3180 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1455 = load i32, i32* %arrayidx3180, align 16
  %arrayidx3181 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1456 = load i32, i32* %arrayidx3181, align 4
  %xor3182 = xor i32 %1455, %1456
  %call3183 = call i32 @rotr32(i32 %xor3182, i32 12)
  %arrayidx3184 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call3183, i32* %arrayidx3184, align 16
  %arrayidx3185 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1457 = load i32, i32* %arrayidx3185, align 4
  %arrayidx3186 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1458 = load i32, i32* %arrayidx3186, align 16
  %add3187 = add i32 %1457, %1458
  %1459 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 8, i32 15), align 1
  %idxprom3188 = zext i8 %1459 to i32
  %arrayidx3189 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3188
  %1460 = load i32, i32* %arrayidx3189, align 4
  %add3190 = add i32 %add3187, %1460
  %arrayidx3191 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add3190, i32* %arrayidx3191, align 4
  %arrayidx3192 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1461 = load i32, i32* %arrayidx3192, align 8
  %arrayidx3193 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1462 = load i32, i32* %arrayidx3193, align 4
  %xor3194 = xor i32 %1461, %1462
  %call3195 = call i32 @rotr32(i32 %xor3194, i32 8)
  %arrayidx3196 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call3195, i32* %arrayidx3196, align 8
  %arrayidx3197 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1463 = load i32, i32* %arrayidx3197, align 4
  %arrayidx3198 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1464 = load i32, i32* %arrayidx3198, align 8
  %add3199 = add i32 %1463, %1464
  %arrayidx3200 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add3199, i32* %arrayidx3200, align 4
  %arrayidx3201 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1465 = load i32, i32* %arrayidx3201, align 16
  %arrayidx3202 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1466 = load i32, i32* %arrayidx3202, align 4
  %xor3203 = xor i32 %1465, %1466
  %call3204 = call i32 @rotr32(i32 %xor3203, i32 7)
  %arrayidx3205 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call3204, i32* %arrayidx3205, align 16
  br label %do.end3206

do.end3206:                                       ; preds = %do.body3163
  br label %do.end3207

do.end3207:                                       ; preds = %do.end3206
  br label %do.body3208

do.body3208:                                      ; preds = %do.end3207
  br label %do.body3209

do.body3209:                                      ; preds = %do.body3208
  %arrayidx3210 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1467 = load i32, i32* %arrayidx3210, align 16
  %arrayidx3211 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1468 = load i32, i32* %arrayidx3211, align 16
  %add3212 = add i32 %1467, %1468
  %1469 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 0), align 16
  %idxprom3213 = zext i8 %1469 to i32
  %arrayidx3214 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3213
  %1470 = load i32, i32* %arrayidx3214, align 4
  %add3215 = add i32 %add3212, %1470
  %arrayidx3216 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add3215, i32* %arrayidx3216, align 16
  %arrayidx3217 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1471 = load i32, i32* %arrayidx3217, align 16
  %arrayidx3218 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1472 = load i32, i32* %arrayidx3218, align 16
  %xor3219 = xor i32 %1471, %1472
  %call3220 = call i32 @rotr32(i32 %xor3219, i32 16)
  %arrayidx3221 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call3220, i32* %arrayidx3221, align 16
  %arrayidx3222 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1473 = load i32, i32* %arrayidx3222, align 16
  %arrayidx3223 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1474 = load i32, i32* %arrayidx3223, align 16
  %add3224 = add i32 %1473, %1474
  %arrayidx3225 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add3224, i32* %arrayidx3225, align 16
  %arrayidx3226 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1475 = load i32, i32* %arrayidx3226, align 16
  %arrayidx3227 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1476 = load i32, i32* %arrayidx3227, align 16
  %xor3228 = xor i32 %1475, %1476
  %call3229 = call i32 @rotr32(i32 %xor3228, i32 12)
  %arrayidx3230 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call3229, i32* %arrayidx3230, align 16
  %arrayidx3231 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1477 = load i32, i32* %arrayidx3231, align 16
  %arrayidx3232 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1478 = load i32, i32* %arrayidx3232, align 16
  %add3233 = add i32 %1477, %1478
  %1479 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 1), align 1
  %idxprom3234 = zext i8 %1479 to i32
  %arrayidx3235 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3234
  %1480 = load i32, i32* %arrayidx3235, align 4
  %add3236 = add i32 %add3233, %1480
  %arrayidx3237 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add3236, i32* %arrayidx3237, align 16
  %arrayidx3238 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1481 = load i32, i32* %arrayidx3238, align 16
  %arrayidx3239 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1482 = load i32, i32* %arrayidx3239, align 16
  %xor3240 = xor i32 %1481, %1482
  %call3241 = call i32 @rotr32(i32 %xor3240, i32 8)
  %arrayidx3242 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call3241, i32* %arrayidx3242, align 16
  %arrayidx3243 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1483 = load i32, i32* %arrayidx3243, align 16
  %arrayidx3244 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1484 = load i32, i32* %arrayidx3244, align 16
  %add3245 = add i32 %1483, %1484
  %arrayidx3246 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add3245, i32* %arrayidx3246, align 16
  %arrayidx3247 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1485 = load i32, i32* %arrayidx3247, align 16
  %arrayidx3248 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1486 = load i32, i32* %arrayidx3248, align 16
  %xor3249 = xor i32 %1485, %1486
  %call3250 = call i32 @rotr32(i32 %xor3249, i32 7)
  %arrayidx3251 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call3250, i32* %arrayidx3251, align 16
  br label %do.end3252

do.end3252:                                       ; preds = %do.body3209
  br label %do.body3253

do.body3253:                                      ; preds = %do.end3252
  %arrayidx3254 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1487 = load i32, i32* %arrayidx3254, align 4
  %arrayidx3255 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1488 = load i32, i32* %arrayidx3255, align 4
  %add3256 = add i32 %1487, %1488
  %1489 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 2), align 2
  %idxprom3257 = zext i8 %1489 to i32
  %arrayidx3258 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3257
  %1490 = load i32, i32* %arrayidx3258, align 4
  %add3259 = add i32 %add3256, %1490
  %arrayidx3260 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add3259, i32* %arrayidx3260, align 4
  %arrayidx3261 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1491 = load i32, i32* %arrayidx3261, align 4
  %arrayidx3262 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1492 = load i32, i32* %arrayidx3262, align 4
  %xor3263 = xor i32 %1491, %1492
  %call3264 = call i32 @rotr32(i32 %xor3263, i32 16)
  %arrayidx3265 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call3264, i32* %arrayidx3265, align 4
  %arrayidx3266 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1493 = load i32, i32* %arrayidx3266, align 4
  %arrayidx3267 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1494 = load i32, i32* %arrayidx3267, align 4
  %add3268 = add i32 %1493, %1494
  %arrayidx3269 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add3268, i32* %arrayidx3269, align 4
  %arrayidx3270 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1495 = load i32, i32* %arrayidx3270, align 4
  %arrayidx3271 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1496 = load i32, i32* %arrayidx3271, align 4
  %xor3272 = xor i32 %1495, %1496
  %call3273 = call i32 @rotr32(i32 %xor3272, i32 12)
  %arrayidx3274 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call3273, i32* %arrayidx3274, align 4
  %arrayidx3275 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1497 = load i32, i32* %arrayidx3275, align 4
  %arrayidx3276 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1498 = load i32, i32* %arrayidx3276, align 4
  %add3277 = add i32 %1497, %1498
  %1499 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 3), align 1
  %idxprom3278 = zext i8 %1499 to i32
  %arrayidx3279 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3278
  %1500 = load i32, i32* %arrayidx3279, align 4
  %add3280 = add i32 %add3277, %1500
  %arrayidx3281 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add3280, i32* %arrayidx3281, align 4
  %arrayidx3282 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1501 = load i32, i32* %arrayidx3282, align 4
  %arrayidx3283 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1502 = load i32, i32* %arrayidx3283, align 4
  %xor3284 = xor i32 %1501, %1502
  %call3285 = call i32 @rotr32(i32 %xor3284, i32 8)
  %arrayidx3286 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call3285, i32* %arrayidx3286, align 4
  %arrayidx3287 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1503 = load i32, i32* %arrayidx3287, align 4
  %arrayidx3288 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1504 = load i32, i32* %arrayidx3288, align 4
  %add3289 = add i32 %1503, %1504
  %arrayidx3290 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add3289, i32* %arrayidx3290, align 4
  %arrayidx3291 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1505 = load i32, i32* %arrayidx3291, align 4
  %arrayidx3292 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1506 = load i32, i32* %arrayidx3292, align 4
  %xor3293 = xor i32 %1505, %1506
  %call3294 = call i32 @rotr32(i32 %xor3293, i32 7)
  %arrayidx3295 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call3294, i32* %arrayidx3295, align 4
  br label %do.end3296

do.end3296:                                       ; preds = %do.body3253
  br label %do.body3297

do.body3297:                                      ; preds = %do.end3296
  %arrayidx3298 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1507 = load i32, i32* %arrayidx3298, align 8
  %arrayidx3299 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1508 = load i32, i32* %arrayidx3299, align 8
  %add3300 = add i32 %1507, %1508
  %1509 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 4), align 4
  %idxprom3301 = zext i8 %1509 to i32
  %arrayidx3302 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3301
  %1510 = load i32, i32* %arrayidx3302, align 4
  %add3303 = add i32 %add3300, %1510
  %arrayidx3304 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add3303, i32* %arrayidx3304, align 8
  %arrayidx3305 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1511 = load i32, i32* %arrayidx3305, align 8
  %arrayidx3306 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1512 = load i32, i32* %arrayidx3306, align 8
  %xor3307 = xor i32 %1511, %1512
  %call3308 = call i32 @rotr32(i32 %xor3307, i32 16)
  %arrayidx3309 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call3308, i32* %arrayidx3309, align 8
  %arrayidx3310 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1513 = load i32, i32* %arrayidx3310, align 8
  %arrayidx3311 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1514 = load i32, i32* %arrayidx3311, align 8
  %add3312 = add i32 %1513, %1514
  %arrayidx3313 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add3312, i32* %arrayidx3313, align 8
  %arrayidx3314 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1515 = load i32, i32* %arrayidx3314, align 8
  %arrayidx3315 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1516 = load i32, i32* %arrayidx3315, align 8
  %xor3316 = xor i32 %1515, %1516
  %call3317 = call i32 @rotr32(i32 %xor3316, i32 12)
  %arrayidx3318 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call3317, i32* %arrayidx3318, align 8
  %arrayidx3319 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1517 = load i32, i32* %arrayidx3319, align 8
  %arrayidx3320 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1518 = load i32, i32* %arrayidx3320, align 8
  %add3321 = add i32 %1517, %1518
  %1519 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 5), align 1
  %idxprom3322 = zext i8 %1519 to i32
  %arrayidx3323 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3322
  %1520 = load i32, i32* %arrayidx3323, align 4
  %add3324 = add i32 %add3321, %1520
  %arrayidx3325 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add3324, i32* %arrayidx3325, align 8
  %arrayidx3326 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1521 = load i32, i32* %arrayidx3326, align 8
  %arrayidx3327 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1522 = load i32, i32* %arrayidx3327, align 8
  %xor3328 = xor i32 %1521, %1522
  %call3329 = call i32 @rotr32(i32 %xor3328, i32 8)
  %arrayidx3330 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call3329, i32* %arrayidx3330, align 8
  %arrayidx3331 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1523 = load i32, i32* %arrayidx3331, align 8
  %arrayidx3332 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1524 = load i32, i32* %arrayidx3332, align 8
  %add3333 = add i32 %1523, %1524
  %arrayidx3334 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add3333, i32* %arrayidx3334, align 8
  %arrayidx3335 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1525 = load i32, i32* %arrayidx3335, align 8
  %arrayidx3336 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1526 = load i32, i32* %arrayidx3336, align 8
  %xor3337 = xor i32 %1525, %1526
  %call3338 = call i32 @rotr32(i32 %xor3337, i32 7)
  %arrayidx3339 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call3338, i32* %arrayidx3339, align 8
  br label %do.end3340

do.end3340:                                       ; preds = %do.body3297
  br label %do.body3341

do.body3341:                                      ; preds = %do.end3340
  %arrayidx3342 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1527 = load i32, i32* %arrayidx3342, align 4
  %arrayidx3343 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1528 = load i32, i32* %arrayidx3343, align 4
  %add3344 = add i32 %1527, %1528
  %1529 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 6), align 2
  %idxprom3345 = zext i8 %1529 to i32
  %arrayidx3346 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3345
  %1530 = load i32, i32* %arrayidx3346, align 4
  %add3347 = add i32 %add3344, %1530
  %arrayidx3348 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add3347, i32* %arrayidx3348, align 4
  %arrayidx3349 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1531 = load i32, i32* %arrayidx3349, align 4
  %arrayidx3350 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1532 = load i32, i32* %arrayidx3350, align 4
  %xor3351 = xor i32 %1531, %1532
  %call3352 = call i32 @rotr32(i32 %xor3351, i32 16)
  %arrayidx3353 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call3352, i32* %arrayidx3353, align 4
  %arrayidx3354 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1533 = load i32, i32* %arrayidx3354, align 4
  %arrayidx3355 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1534 = load i32, i32* %arrayidx3355, align 4
  %add3356 = add i32 %1533, %1534
  %arrayidx3357 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3356, i32* %arrayidx3357, align 4
  %arrayidx3358 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1535 = load i32, i32* %arrayidx3358, align 4
  %arrayidx3359 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1536 = load i32, i32* %arrayidx3359, align 4
  %xor3360 = xor i32 %1535, %1536
  %call3361 = call i32 @rotr32(i32 %xor3360, i32 12)
  %arrayidx3362 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3361, i32* %arrayidx3362, align 4
  %arrayidx3363 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1537 = load i32, i32* %arrayidx3363, align 4
  %arrayidx3364 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1538 = load i32, i32* %arrayidx3364, align 4
  %add3365 = add i32 %1537, %1538
  %1539 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 7), align 1
  %idxprom3366 = zext i8 %1539 to i32
  %arrayidx3367 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3366
  %1540 = load i32, i32* %arrayidx3367, align 4
  %add3368 = add i32 %add3365, %1540
  %arrayidx3369 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add3368, i32* %arrayidx3369, align 4
  %arrayidx3370 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1541 = load i32, i32* %arrayidx3370, align 4
  %arrayidx3371 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1542 = load i32, i32* %arrayidx3371, align 4
  %xor3372 = xor i32 %1541, %1542
  %call3373 = call i32 @rotr32(i32 %xor3372, i32 8)
  %arrayidx3374 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call3373, i32* %arrayidx3374, align 4
  %arrayidx3375 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1543 = load i32, i32* %arrayidx3375, align 4
  %arrayidx3376 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1544 = load i32, i32* %arrayidx3376, align 4
  %add3377 = add i32 %1543, %1544
  %arrayidx3378 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3377, i32* %arrayidx3378, align 4
  %arrayidx3379 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1545 = load i32, i32* %arrayidx3379, align 4
  %arrayidx3380 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1546 = load i32, i32* %arrayidx3380, align 4
  %xor3381 = xor i32 %1545, %1546
  %call3382 = call i32 @rotr32(i32 %xor3381, i32 7)
  %arrayidx3383 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3382, i32* %arrayidx3383, align 4
  br label %do.end3384

do.end3384:                                       ; preds = %do.body3341
  br label %do.body3385

do.body3385:                                      ; preds = %do.end3384
  %arrayidx3386 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1547 = load i32, i32* %arrayidx3386, align 16
  %arrayidx3387 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1548 = load i32, i32* %arrayidx3387, align 4
  %add3388 = add i32 %1547, %1548
  %1549 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 8), align 8
  %idxprom3389 = zext i8 %1549 to i32
  %arrayidx3390 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3389
  %1550 = load i32, i32* %arrayidx3390, align 4
  %add3391 = add i32 %add3388, %1550
  %arrayidx3392 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add3391, i32* %arrayidx3392, align 16
  %arrayidx3393 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1551 = load i32, i32* %arrayidx3393, align 4
  %arrayidx3394 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1552 = load i32, i32* %arrayidx3394, align 16
  %xor3395 = xor i32 %1551, %1552
  %call3396 = call i32 @rotr32(i32 %xor3395, i32 16)
  %arrayidx3397 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call3396, i32* %arrayidx3397, align 4
  %arrayidx3398 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1553 = load i32, i32* %arrayidx3398, align 8
  %arrayidx3399 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1554 = load i32, i32* %arrayidx3399, align 4
  %add3400 = add i32 %1553, %1554
  %arrayidx3401 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add3400, i32* %arrayidx3401, align 8
  %arrayidx3402 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1555 = load i32, i32* %arrayidx3402, align 4
  %arrayidx3403 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1556 = load i32, i32* %arrayidx3403, align 8
  %xor3404 = xor i32 %1555, %1556
  %call3405 = call i32 @rotr32(i32 %xor3404, i32 12)
  %arrayidx3406 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call3405, i32* %arrayidx3406, align 4
  %arrayidx3407 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1557 = load i32, i32* %arrayidx3407, align 16
  %arrayidx3408 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1558 = load i32, i32* %arrayidx3408, align 4
  %add3409 = add i32 %1557, %1558
  %1559 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 9), align 1
  %idxprom3410 = zext i8 %1559 to i32
  %arrayidx3411 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3410
  %1560 = load i32, i32* %arrayidx3411, align 4
  %add3412 = add i32 %add3409, %1560
  %arrayidx3413 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  store i32 %add3412, i32* %arrayidx3413, align 16
  %arrayidx3414 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1561 = load i32, i32* %arrayidx3414, align 4
  %arrayidx3415 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 0
  %1562 = load i32, i32* %arrayidx3415, align 16
  %xor3416 = xor i32 %1561, %1562
  %call3417 = call i32 @rotr32(i32 %xor3416, i32 8)
  %arrayidx3418 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  store i32 %call3417, i32* %arrayidx3418, align 4
  %arrayidx3419 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1563 = load i32, i32* %arrayidx3419, align 8
  %arrayidx3420 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 15
  %1564 = load i32, i32* %arrayidx3420, align 4
  %add3421 = add i32 %1563, %1564
  %arrayidx3422 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  store i32 %add3421, i32* %arrayidx3422, align 8
  %arrayidx3423 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  %1565 = load i32, i32* %arrayidx3423, align 4
  %arrayidx3424 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 10
  %1566 = load i32, i32* %arrayidx3424, align 8
  %xor3425 = xor i32 %1565, %1566
  %call3426 = call i32 @rotr32(i32 %xor3425, i32 7)
  %arrayidx3427 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 5
  store i32 %call3426, i32* %arrayidx3427, align 4
  br label %do.end3428

do.end3428:                                       ; preds = %do.body3385
  br label %do.body3429

do.body3429:                                      ; preds = %do.end3428
  %arrayidx3430 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1567 = load i32, i32* %arrayidx3430, align 4
  %arrayidx3431 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1568 = load i32, i32* %arrayidx3431, align 8
  %add3432 = add i32 %1567, %1568
  %1569 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 10), align 2
  %idxprom3433 = zext i8 %1569 to i32
  %arrayidx3434 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3433
  %1570 = load i32, i32* %arrayidx3434, align 4
  %add3435 = add i32 %add3432, %1570
  %arrayidx3436 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add3435, i32* %arrayidx3436, align 4
  %arrayidx3437 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1571 = load i32, i32* %arrayidx3437, align 16
  %arrayidx3438 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1572 = load i32, i32* %arrayidx3438, align 4
  %xor3439 = xor i32 %1571, %1572
  %call3440 = call i32 @rotr32(i32 %xor3439, i32 16)
  %arrayidx3441 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call3440, i32* %arrayidx3441, align 16
  %arrayidx3442 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1573 = load i32, i32* %arrayidx3442, align 4
  %arrayidx3443 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1574 = load i32, i32* %arrayidx3443, align 16
  %add3444 = add i32 %1573, %1574
  %arrayidx3445 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3444, i32* %arrayidx3445, align 4
  %arrayidx3446 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1575 = load i32, i32* %arrayidx3446, align 8
  %arrayidx3447 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1576 = load i32, i32* %arrayidx3447, align 4
  %xor3448 = xor i32 %1575, %1576
  %call3449 = call i32 @rotr32(i32 %xor3448, i32 12)
  %arrayidx3450 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call3449, i32* %arrayidx3450, align 8
  %arrayidx3451 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1577 = load i32, i32* %arrayidx3451, align 4
  %arrayidx3452 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1578 = load i32, i32* %arrayidx3452, align 8
  %add3453 = add i32 %1577, %1578
  %1579 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 11), align 1
  %idxprom3454 = zext i8 %1579 to i32
  %arrayidx3455 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3454
  %1580 = load i32, i32* %arrayidx3455, align 4
  %add3456 = add i32 %add3453, %1580
  %arrayidx3457 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  store i32 %add3456, i32* %arrayidx3457, align 4
  %arrayidx3458 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1581 = load i32, i32* %arrayidx3458, align 16
  %arrayidx3459 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 1
  %1582 = load i32, i32* %arrayidx3459, align 4
  %xor3460 = xor i32 %1581, %1582
  %call3461 = call i32 @rotr32(i32 %xor3460, i32 8)
  %arrayidx3462 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  store i32 %call3461, i32* %arrayidx3462, align 16
  %arrayidx3463 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1583 = load i32, i32* %arrayidx3463, align 4
  %arrayidx3464 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 12
  %1584 = load i32, i32* %arrayidx3464, align 16
  %add3465 = add i32 %1583, %1584
  %arrayidx3466 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  store i32 %add3465, i32* %arrayidx3466, align 4
  %arrayidx3467 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  %1585 = load i32, i32* %arrayidx3467, align 8
  %arrayidx3468 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 11
  %1586 = load i32, i32* %arrayidx3468, align 4
  %xor3469 = xor i32 %1585, %1586
  %call3470 = call i32 @rotr32(i32 %xor3469, i32 7)
  %arrayidx3471 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 6
  store i32 %call3470, i32* %arrayidx3471, align 8
  br label %do.end3472

do.end3472:                                       ; preds = %do.body3429
  br label %do.body3473

do.body3473:                                      ; preds = %do.end3472
  %arrayidx3474 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1587 = load i32, i32* %arrayidx3474, align 8
  %arrayidx3475 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1588 = load i32, i32* %arrayidx3475, align 4
  %add3476 = add i32 %1587, %1588
  %1589 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 12), align 4
  %idxprom3477 = zext i8 %1589 to i32
  %arrayidx3478 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3477
  %1590 = load i32, i32* %arrayidx3478, align 4
  %add3479 = add i32 %add3476, %1590
  %arrayidx3480 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add3479, i32* %arrayidx3480, align 8
  %arrayidx3481 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1591 = load i32, i32* %arrayidx3481, align 4
  %arrayidx3482 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1592 = load i32, i32* %arrayidx3482, align 8
  %xor3483 = xor i32 %1591, %1592
  %call3484 = call i32 @rotr32(i32 %xor3483, i32 16)
  %arrayidx3485 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call3484, i32* %arrayidx3485, align 4
  %arrayidx3486 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1593 = load i32, i32* %arrayidx3486, align 16
  %arrayidx3487 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1594 = load i32, i32* %arrayidx3487, align 4
  %add3488 = add i32 %1593, %1594
  %arrayidx3489 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add3488, i32* %arrayidx3489, align 16
  %arrayidx3490 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1595 = load i32, i32* %arrayidx3490, align 4
  %arrayidx3491 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1596 = load i32, i32* %arrayidx3491, align 16
  %xor3492 = xor i32 %1595, %1596
  %call3493 = call i32 @rotr32(i32 %xor3492, i32 12)
  %arrayidx3494 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3493, i32* %arrayidx3494, align 4
  %arrayidx3495 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1597 = load i32, i32* %arrayidx3495, align 8
  %arrayidx3496 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1598 = load i32, i32* %arrayidx3496, align 4
  %add3497 = add i32 %1597, %1598
  %1599 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 13), align 1
  %idxprom3498 = zext i8 %1599 to i32
  %arrayidx3499 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3498
  %1600 = load i32, i32* %arrayidx3499, align 4
  %add3500 = add i32 %add3497, %1600
  %arrayidx3501 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  store i32 %add3500, i32* %arrayidx3501, align 8
  %arrayidx3502 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1601 = load i32, i32* %arrayidx3502, align 4
  %arrayidx3503 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 2
  %1602 = load i32, i32* %arrayidx3503, align 8
  %xor3504 = xor i32 %1601, %1602
  %call3505 = call i32 @rotr32(i32 %xor3504, i32 8)
  %arrayidx3506 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  store i32 %call3505, i32* %arrayidx3506, align 4
  %arrayidx3507 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1603 = load i32, i32* %arrayidx3507, align 16
  %arrayidx3508 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 13
  %1604 = load i32, i32* %arrayidx3508, align 4
  %add3509 = add i32 %1603, %1604
  %arrayidx3510 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  store i32 %add3509, i32* %arrayidx3510, align 16
  %arrayidx3511 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  %1605 = load i32, i32* %arrayidx3511, align 4
  %arrayidx3512 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 8
  %1606 = load i32, i32* %arrayidx3512, align 16
  %xor3513 = xor i32 %1605, %1606
  %call3514 = call i32 @rotr32(i32 %xor3513, i32 7)
  %arrayidx3515 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 7
  store i32 %call3514, i32* %arrayidx3515, align 4
  br label %do.end3516

do.end3516:                                       ; preds = %do.body3473
  br label %do.body3517

do.body3517:                                      ; preds = %do.end3516
  %arrayidx3518 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1607 = load i32, i32* %arrayidx3518, align 4
  %arrayidx3519 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1608 = load i32, i32* %arrayidx3519, align 16
  %add3520 = add i32 %1607, %1608
  %1609 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 14), align 2
  %idxprom3521 = zext i8 %1609 to i32
  %arrayidx3522 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3521
  %1610 = load i32, i32* %arrayidx3522, align 4
  %add3523 = add i32 %add3520, %1610
  %arrayidx3524 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add3523, i32* %arrayidx3524, align 4
  %arrayidx3525 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1611 = load i32, i32* %arrayidx3525, align 8
  %arrayidx3526 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1612 = load i32, i32* %arrayidx3526, align 4
  %xor3527 = xor i32 %1611, %1612
  %call3528 = call i32 @rotr32(i32 %xor3527, i32 16)
  %arrayidx3529 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call3528, i32* %arrayidx3529, align 8
  %arrayidx3530 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1613 = load i32, i32* %arrayidx3530, align 4
  %arrayidx3531 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1614 = load i32, i32* %arrayidx3531, align 8
  %add3532 = add i32 %1613, %1614
  %arrayidx3533 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add3532, i32* %arrayidx3533, align 4
  %arrayidx3534 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1615 = load i32, i32* %arrayidx3534, align 16
  %arrayidx3535 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1616 = load i32, i32* %arrayidx3535, align 4
  %xor3536 = xor i32 %1615, %1616
  %call3537 = call i32 @rotr32(i32 %xor3536, i32 12)
  %arrayidx3538 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call3537, i32* %arrayidx3538, align 16
  %arrayidx3539 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1617 = load i32, i32* %arrayidx3539, align 4
  %arrayidx3540 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1618 = load i32, i32* %arrayidx3540, align 16
  %add3541 = add i32 %1617, %1618
  %1619 = load i8, i8* getelementptr inbounds ([10 x [16 x i8]], [10 x [16 x i8]]* @blake2s_sigma, i32 0, i32 9, i32 15), align 1
  %idxprom3542 = zext i8 %1619 to i32
  %arrayidx3543 = getelementptr inbounds [16 x i32], [16 x i32]* %m, i32 0, i32 %idxprom3542
  %1620 = load i32, i32* %arrayidx3543, align 4
  %add3544 = add i32 %add3541, %1620
  %arrayidx3545 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  store i32 %add3544, i32* %arrayidx3545, align 4
  %arrayidx3546 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1621 = load i32, i32* %arrayidx3546, align 8
  %arrayidx3547 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 3
  %1622 = load i32, i32* %arrayidx3547, align 4
  %xor3548 = xor i32 %1621, %1622
  %call3549 = call i32 @rotr32(i32 %xor3548, i32 8)
  %arrayidx3550 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  store i32 %call3549, i32* %arrayidx3550, align 8
  %arrayidx3551 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1623 = load i32, i32* %arrayidx3551, align 4
  %arrayidx3552 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 14
  %1624 = load i32, i32* %arrayidx3552, align 8
  %add3553 = add i32 %1623, %1624
  %arrayidx3554 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  store i32 %add3553, i32* %arrayidx3554, align 4
  %arrayidx3555 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  %1625 = load i32, i32* %arrayidx3555, align 16
  %arrayidx3556 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 9
  %1626 = load i32, i32* %arrayidx3556, align 4
  %xor3557 = xor i32 %1625, %1626
  %call3558 = call i32 @rotr32(i32 %xor3557, i32 7)
  %arrayidx3559 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 4
  store i32 %call3558, i32* %arrayidx3559, align 16
  br label %do.end3560

do.end3560:                                       ; preds = %do.body3517
  br label %do.end3561

do.end3561:                                       ; preds = %do.end3560
  store i32 0, i32* %i, align 4
  br label %for.cond3562

for.cond3562:                                     ; preds = %for.inc3574, %do.end3561
  %1627 = load i32, i32* %i, align 4
  %cmp3563 = icmp ult i32 %1627, 8
  br i1 %cmp3563, label %for.body3564, label %for.end3576

for.body3564:                                     ; preds = %for.cond3562
  %1628 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %h3565 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %1628, i32 0, i32 0
  %1629 = load i32, i32* %i, align 4
  %arrayidx3566 = getelementptr inbounds [8 x i32], [8 x i32]* %h3565, i32 0, i32 %1629
  %1630 = load i32, i32* %arrayidx3566, align 4
  %1631 = load i32, i32* %i, align 4
  %arrayidx3567 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 %1631
  %1632 = load i32, i32* %arrayidx3567, align 4
  %xor3568 = xor i32 %1630, %1632
  %1633 = load i32, i32* %i, align 4
  %add3569 = add i32 %1633, 8
  %arrayidx3570 = getelementptr inbounds [16 x i32], [16 x i32]* %v, i32 0, i32 %add3569
  %1634 = load i32, i32* %arrayidx3570, align 4
  %xor3571 = xor i32 %xor3568, %1634
  %1635 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %h3572 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %1635, i32 0, i32 0
  %1636 = load i32, i32* %i, align 4
  %arrayidx3573 = getelementptr inbounds [8 x i32], [8 x i32]* %h3572, i32 0, i32 %1636
  store i32 %xor3571, i32* %arrayidx3573, align 4
  br label %for.inc3574

for.inc3574:                                      ; preds = %for.body3564
  %1637 = load i32, i32* %i, align 4
  %inc3575 = add i32 %1637, 1
  store i32 %inc3575, i32* %i, align 4
  br label %for.cond3562

for.end3576:                                      ; preds = %for.cond3562
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2s_final(%struct.blake2s_state__* %S, i8* %out, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2s_state__*, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %buffer = alloca [32 x i8], align 16
  %i = alloca i32, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = bitcast [32 x i8]* %buffer to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %0, i8 0, i32 32, i1 false)
  %1 = load i8*, i8** %out.addr, align 4
  %cmp = icmp eq i8* %1, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32, i32* %outlen.addr, align 4
  %3 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %outlen1 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %3, i32 0, i32 5
  %4 = load i32, i32* %outlen1, align 4
  %cmp2 = icmp ult i32 %2, %4
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %5 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %call = call i32 @blake2s_is_lastblock(%struct.blake2s_state__* %5)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %6 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %7 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %7, i32 0, i32 4
  %8 = load i32, i32* %buflen, align 4
  call void @blake2s_increment_counter(%struct.blake2s_state__* %6, i32 %8)
  %9 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  call void @blake2s_set_lastblock(%struct.blake2s_state__* %9)
  %10 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %10, i32 0, i32 3
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %buf, i32 0, i32 0
  %11 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buflen5 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %11, i32 0, i32 4
  %12 = load i32, i32* %buflen5, align 4
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 %12
  %13 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buflen6 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %13, i32 0, i32 4
  %14 = load i32, i32* %buflen6, align 4
  %sub = sub i32 64, %14
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr, i8 0, i32 %sub, i1 false)
  %15 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %16 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %buf7 = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %16, i32 0, i32 3
  %arraydecay8 = getelementptr inbounds [64 x i8], [64 x i8]* %buf7, i32 0, i32 0
  call void @blake2s_compress(%struct.blake2s_state__* %15, i8* %arraydecay8)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end4
  %17 = load i32, i32* %i, align 4
  %cmp9 = icmp ult i32 %17, 8
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %arraydecay10 = getelementptr inbounds [32 x i8], [32 x i8]* %buffer, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %mul = mul i32 4, %18
  %add.ptr11 = getelementptr inbounds i8, i8* %arraydecay10, i32 %mul
  %19 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %19, i32 0, i32 0
  %20 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %h, i32 0, i32 %20
  %21 = load i32, i32* %arrayidx, align 4
  call void @store32(i8* %add.ptr11, i32 %21)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %i, align 4
  %inc = add i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load i8*, i8** %out.addr, align 4
  %arraydecay12 = getelementptr inbounds [32 x i8], [32 x i8]* %buffer, i32 0, i32 0
  %24 = load i32, i32* %outlen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %23, i8* align 16 %arraydecay12, i32 %24, i1 false)
  %arraydecay13 = getelementptr inbounds [32 x i8], [32 x i8]* %buffer, i32 0, i32 0
  call void @secure_zero_memory(i8* %arraydecay13, i32 32)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then3, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2s_is_lastblock(%struct.blake2s_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  %0 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %0, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %f, i32 0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  %cmp = icmp ne i32 %1, 0
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2s_set_lastblock(%struct.blake2s_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  %0 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %last_node = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %0, i32 0, i32 6
  %1 = load i8, i8* %last_node, align 4
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  call void @blake2s_set_lastnode(%struct.blake2s_state__* %2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %3, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %f, i32 0, i32 0
  store i32 -1, i32* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2s(i8* %out, i32 %outlen, i8* %in, i32 %inlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %S = alloca [1 x %struct.blake2s_state__], align 16
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i8*, i8** %in.addr, align 4
  %cmp = icmp eq i8* null, %0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %inlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i8*, i8** %out.addr, align 4
  %cmp2 = icmp eq i8* null, %2
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %3 = load i8*, i8** %key.addr, align 4
  %cmp5 = icmp eq i8* null, %3
  br i1 %cmp5, label %land.lhs.true6, label %if.end9

land.lhs.true6:                                   ; preds = %if.end4
  %4 = load i32, i32* %keylen.addr, align 4
  %cmp7 = icmp ugt i32 %4, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %land.lhs.true6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %land.lhs.true6, %if.end4
  %5 = load i32, i32* %outlen.addr, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then11

lor.lhs.false:                                    ; preds = %if.end9
  %6 = load i32, i32* %outlen.addr, align 4
  %cmp10 = icmp ugt i32 %6, 32
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %lor.lhs.false, %if.end9
  store i32 -1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %lor.lhs.false
  %7 = load i32, i32* %keylen.addr, align 4
  %cmp13 = icmp ugt i32 %7, 32
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end12
  store i32 -1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end12
  %8 = load i32, i32* %keylen.addr, align 4
  %cmp16 = icmp ugt i32 %8, 0
  br i1 %cmp16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.end15
  %arraydecay = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %S, i32 0, i32 0
  %9 = load i32, i32* %outlen.addr, align 4
  %10 = load i8*, i8** %key.addr, align 4
  %11 = load i32, i32* %keylen.addr, align 4
  %call = call i32 @blake2s_init_key(%struct.blake2s_state__* %arraydecay, i32 %9, i8* %10, i32 %11)
  %cmp18 = icmp slt i32 %call, 0
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.then17
  store i32 -1, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.then17
  br label %if.end26

if.else:                                          ; preds = %if.end15
  %arraydecay21 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %S, i32 0, i32 0
  %12 = load i32, i32* %outlen.addr, align 4
  %call22 = call i32 @blake2s_init(%struct.blake2s_state__* %arraydecay21, i32 %12)
  %cmp23 = icmp slt i32 %call22, 0
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.else
  store i32 -1, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.else
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.end20
  %arraydecay27 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %S, i32 0, i32 0
  %13 = load i8*, i8** %in.addr, align 4
  %14 = load i32, i32* %inlen.addr, align 4
  %call28 = call i32 @blake2s_update(%struct.blake2s_state__* %arraydecay27, i8* %13, i32 %14)
  %arraydecay29 = getelementptr inbounds [1 x %struct.blake2s_state__], [1 x %struct.blake2s_state__]* %S, i32 0, i32 0
  %15 = load i8*, i8** %out.addr, align 4
  %16 = load i32, i32* %outlen.addr, align 4
  %call30 = call i32 @blake2s_final(%struct.blake2s_state__* %arraydecay29, i8* %15, i32 %16)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end26, %if.then24, %if.then19, %if.then14, %if.then11, %if.then8, %if.then3, %if.then
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

declare i8* @memset(i8*, i32, i32) #3

; Function Attrs: noinline nounwind optnone
define internal i32 @rotr32(i32 %w, i32 %c) #0 {
entry:
  %w.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  store i32 %w, i32* %w.addr, align 4
  store i32 %c, i32* %c.addr, align 4
  %0 = load i32, i32* %w.addr, align 4
  %1 = load i32, i32* %c.addr, align 4
  %shr = lshr i32 %0, %1
  %2 = load i32, i32* %w.addr, align 4
  %3 = load i32, i32* %c.addr, align 4
  %sub = sub i32 32, %3
  %shl = shl i32 %2, %sub
  %or = or i32 %shr, %shl
  ret i32 %or
}

; Function Attrs: noinline nounwind optnone
define internal void @blake2s_set_lastnode(%struct.blake2s_state__* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2s_state__*, align 4
  store %struct.blake2s_state__* %S, %struct.blake2s_state__** %S.addr, align 4
  %0 = load %struct.blake2s_state__*, %struct.blake2s_state__** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2s_state__, %struct.blake2s_state__* %0, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %f, i32 0, i32 1
  store i32 -1, i32* %arrayidx, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
