(module
  (type (;0;) (func (param i32 i32) (result i32)))
  (type (;1;) (func))
  (type (;2;) (func (param i32)))
  (type (;3;) (func (result i32)))
  (import "wasi_snapshot_preview1" "proc_exit" (func (;0;) (type 2)))
  (import "wasi_snapshot_preview1" "args_get" (func (;1;) (type 0)))
  (import "wasi_snapshot_preview1" "args_sizes_get" (func (;2;) (type 0)))
  (func (;3;) (type 3) (result i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 0
    global.set 0
    block  ;; label = @1
      local.get 0
      i32.const 12
      i32.add
      local.get 0
      i32.const 8
      i32.add
      call 2
      i32.eqz
      if  ;; label = @2
        local.get 0
        i32.load offset=12
        local.tee 1
        if  ;; label = @3
          local.get 0
          local.get 1
          i32.const 2
          i32.shl
          local.tee 3
          i32.const 19
          i32.add
          i32.const -16
          i32.and
          i32.sub
          local.tee 2
          local.tee 1
          global.set 0
          local.get 1
          local.get 0
          i32.load offset=8
          i32.const 15
          i32.add
          i32.const -16
          i32.and
          i32.sub
          local.tee 1
          global.set 0
          local.get 2
          local.get 3
          i32.add
          i32.const 0
          i32.store
          local.get 2
          local.get 1
          call 1
          br_if 2 (;@1;)
          local.get 0
          i32.load offset=12
          drop
        end
        local.get 0
        i32.const 16
        i32.add
        global.set 0
        i32.const 0
        return
      end
      i32.const 71
      call 0
      unreachable
    end
    i32.const 71
    call 0
    unreachable)
  (func (;4;) (type 1)
    call 3
    call 0
    unreachable)
  (memory (;0;) 256 256)
  (global (;0;) (mut i32) (i32.const 5244576))
  (export "memory" (memory 0))
  (export "_start" (func 4))
  (data (;0;) (i32.const 1536) "\a0\06P"))
