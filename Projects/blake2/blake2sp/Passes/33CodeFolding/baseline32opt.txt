[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        3.105e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    3.6195e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        8.013e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    6.438e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     4.0487e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     8.688e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        6.7036e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                3.1214e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  3.3017e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                9.623e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              3.4488e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    8.712e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.000126696 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 4.3866e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       1.409e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        6.1597e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             2.7845e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     1.8034e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  1.4462e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       6.646e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    3.7251e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    4.4796e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             2.0269e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     7.763e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    1.7628e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     7.208e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             2.0079e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.000755246 seconds.
[PassRunner] (final validation)
