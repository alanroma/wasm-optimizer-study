[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    2.964e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                6.1592e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    5.067e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                6.741e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 3.687e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 6.203e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            2.8693e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              3.4019e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            1.0024e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          4.455e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                1.1985e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     8.7339e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       7.893e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   1.3762e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    6.3099e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         2.8526e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 1.5141e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              1.4938e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                3.7652e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                4.1081e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         1.9993e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 7.595e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                1.6875e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 6.96e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         1.9868e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   1.8789e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              1.4727e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            4.062e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   1.0158e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     3.0621e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          1.4629e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            2.8854e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         1.9901e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 4.321e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            4.7757e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 3.3321e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   4.333e-06 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.000899792 seconds.
[PassRunner] (final validation)
