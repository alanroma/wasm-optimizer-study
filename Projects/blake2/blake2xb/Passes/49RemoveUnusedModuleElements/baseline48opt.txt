[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    5.142e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000423182 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.996e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00042363 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.001425 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 6.8113e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00465661 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00483712 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000478554 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00229303 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0009833 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00243584 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000482325 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00124607 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0106865 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00495613 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00217463 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00191829 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00171591 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00526786 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00296286 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000640048 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00161855 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000600029 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00289201 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0019017 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00338947 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000318238 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00178758 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00149078 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00157312 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00114043 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0027451 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.00415049 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0181449 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000621369 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   8.444e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00094759 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0934179 seconds.
[PassRunner] (final validation)
