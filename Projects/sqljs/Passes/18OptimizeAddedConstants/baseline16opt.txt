[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    7.5028e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00324437 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000118494 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.016608 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0142365 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000271441 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.136754 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.203581 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0170532 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.061847 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0336698 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0779223 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.565381 seconds.
[PassRunner] (final validation)
