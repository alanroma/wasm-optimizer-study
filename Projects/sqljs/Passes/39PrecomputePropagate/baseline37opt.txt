[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        9.6825e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.00293578 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        9.6641e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.0167262 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.0137069 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     0.000292923 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.488547 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.138276 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.204465 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.0172553 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.0618545 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.0338994 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               1.34924 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.754716 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.101747 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.398411 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.16982 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.0703338 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0754709 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.0194272 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0645691 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.173916 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0992256 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.0221523 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0647967 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.0226091 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.100412 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                       0.0277681 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.0915363 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.107402 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.0135206 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.0785769 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 4.7838 seconds.
[PassRunner] (final validation)
