; ModuleID = '/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/thread.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/thread.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.__pthread = type opaque
%struct.pthread_attr_t = type { %union.anon }
%union.anon = type { [11 x i32] }

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2_thread_create(%struct.__pthread** %handle, i8* (i8*)* %func, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %handle.addr = alloca %struct.__pthread**, align 4
  %func.addr = alloca i8* (i8*)*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.__pthread** %handle, %struct.__pthread*** %handle.addr, align 4, !tbaa !2
  store i8* (i8*)* %func, i8* (i8*)** %func.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = load %struct.__pthread**, %struct.__pthread*** %handle.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.__pthread** null, %0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8* (i8*)*, i8* (i8*)** %func.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i8* (i8*)* %1, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load %struct.__pthread**, %struct.__pthread*** %handle.addr, align 4, !tbaa !2
  %3 = load i8* (i8*)*, i8* (i8*)** %func.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %args.addr, align 4, !tbaa !2
  %call = call i32 @pthread_create(%struct.__pthread** %2, %struct.pthread_attr_t* null, i8* (i8*)* %3, i8* %4) #3
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: optsize
declare !callback !6 i32 @pthread_create(%struct.__pthread**, %struct.pthread_attr_t*, i8* (i8*)*, i8*) #1

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2_thread_join(%struct.__pthread* %handle) #0 {
entry:
  %handle.addr = alloca %struct.__pthread*, align 4
  store %struct.__pthread* %handle, %struct.__pthread** %handle.addr, align 4, !tbaa !2
  %0 = load %struct.__pthread*, %struct.__pthread** %handle.addr, align 4, !tbaa !2
  %call = call i32 @pthread_join(%struct.__pthread* %0, i8** null) #3
  ret i32 %call
}

; Function Attrs: optsize
declare i32 @pthread_join(%struct.__pthread*, i8**) #1

; Function Attrs: noinline nounwind optsize
define hidden void @argon2_thread_exit() #0 {
entry:
  call void @pthread_exit(i8* null) #4
  unreachable
}

; Function Attrs: noreturn optsize
declare void @pthread_exit(i8*) #2

attributes #0 = { noinline nounwind optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { optsize }
attributes #4 = { noreturn optsize }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7}
!7 = !{i64 2, i64 3, i1 false}
