; ModuleID = '/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/encoding.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/encoding.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Argon2_Context = type { i8*, i32, i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32 (i8**, i32)*, void (i8*, i32)*, i32 }

@.str = private unnamed_addr constant [2 x i8] c"$\00", align 1
@.str.1 = private unnamed_addr constant [4 x i8] c"$v=\00", align 1
@.str.2 = private unnamed_addr constant [4 x i8] c"$m=\00", align 1
@.str.3 = private unnamed_addr constant [4 x i8] c",t=\00", align 1
@.str.4 = private unnamed_addr constant [4 x i8] c",p=\00", align 1
@.str.5 = private unnamed_addr constant [4 x i8] c"%lu\00", align 1

; Function Attrs: noinline nounwind optsize
define hidden i32 @decode_string(%struct.Argon2_Context* %ctx, i8* %str, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.Argon2_Context*, align 4
  %str.addr = alloca i8*, align 4
  %type.addr = alloca i32, align 4
  %maxsaltlen = alloca i32, align 4
  %maxoutlen = alloca i32, align 4
  %validation_result = alloca i32, align 4
  %type_string = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %cc_len = alloca i32, align 4
  %cc_len5 = alloca i32, align 4
  %cc_len18 = alloca i32, align 4
  %dec_x = alloca i32, align 4
  %cc_len42 = alloca i32, align 4
  %dec_x54 = alloca i32, align 4
  %cc_len67 = alloca i32, align 4
  %dec_x79 = alloca i32, align 4
  %cc_len92 = alloca i32, align 4
  %dec_x104 = alloca i32, align 4
  %cc_len118 = alloca i32, align 4
  %bin_len = alloca i32, align 4
  %cc_len143 = alloca i32, align 4
  %bin_len155 = alloca i32, align 4
  store %struct.Argon2_Context* %ctx, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  store i8* %str, i8** %str.addr, align 4, !tbaa !2
  store i32 %type, i32* %type.addr, align 4, !tbaa !6
  %0 = bitcast i32* %maxsaltlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %1, i32 0, i32 5
  %2 = load i32, i32* %saltlen, align 4, !tbaa !7
  store i32 %2, i32* %maxsaltlen, align 4, !tbaa !10
  %3 = bitcast i32* %maxoutlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %4, i32 0, i32 1
  %5 = load i32, i32* %outlen, align 4, !tbaa !12
  store i32 %5, i32* %maxoutlen, align 4, !tbaa !10
  %6 = bitcast i32* %validation_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i8** %type_string to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load i32, i32* %type.addr, align 4, !tbaa !6
  %call = call i8* @argon2_type2string(i32 %8, i32 0) #4
  store i8* %call, i8** %type_string, align 4, !tbaa !2
  %9 = load i8*, i8** %type_string, align 4, !tbaa !2
  %tobool = icmp ne i8* %9, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -26, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup175

if.end:                                           ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %if.end
  %10 = bitcast i32* %cc_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  store i32 1, i32* %cc_len, align 4, !tbaa !10
  %11 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %12 = load i32, i32* %cc_len, align 4, !tbaa !10
  %call1 = call i32 @strncmp(i8* %11, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i32 %12) #4
  %cmp = icmp ne i32 %call1, 0
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %do.body
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %do.body
  %13 = load i32, i32* %cc_len, align 4, !tbaa !10
  %14 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %13
  store i8* %add.ptr, i8** %str.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end3, %if.then2
  %15 = bitcast i32* %cc_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup175 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body4

do.body4:                                         ; preds = %do.end
  %16 = bitcast i32* %cc_len5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load i8*, i8** %type_string, align 4, !tbaa !2
  %call6 = call i32 @strlen(i8* %17) #4
  store i32 %call6, i32* %cc_len5, align 4, !tbaa !10
  %18 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %19 = load i8*, i8** %type_string, align 4, !tbaa !2
  %20 = load i32, i32* %cc_len5, align 4, !tbaa !10
  %call7 = call i32 @strncmp(i8* %18, i8* %19, i32 %20) #4
  %cmp8 = icmp ne i32 %call7, 0
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %do.body4
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

if.end10:                                         ; preds = %do.body4
  %21 = load i32, i32* %cc_len5, align 4, !tbaa !10
  %22 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %22, i32 %21
  store i8* %add.ptr11, i8** %str.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

cleanup12:                                        ; preds = %if.end10, %if.then9
  %23 = bitcast i32* %cc_len5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  %cleanup.dest13 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest13, label %cleanup175 [
    i32 0, label %cleanup.cont14
  ]

cleanup.cont14:                                   ; preds = %cleanup12
  br label %do.cond15

do.cond15:                                        ; preds = %cleanup.cont14
  br label %do.end16

do.end16:                                         ; preds = %do.cond15
  %24 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %version = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %24, i32 0, i32 14
  store i32 16, i32* %version, align 4, !tbaa !13
  br label %do.body17

do.body17:                                        ; preds = %do.end16
  %25 = bitcast i32* %cc_len18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  store i32 3, i32* %cc_len18, align 4, !tbaa !10
  %26 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %27 = load i32, i32* %cc_len18, align 4, !tbaa !10
  %call19 = call i32 @strncmp(i8* %26, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i32 0, i32 0), i32 %27) #4
  %cmp20 = icmp eq i32 %call19, 0
  br i1 %cmp20, label %if.then21, label %if.end35

if.then21:                                        ; preds = %do.body17
  %28 = load i32, i32* %cc_len18, align 4, !tbaa !10
  %29 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %29, i32 %28
  store i8* %add.ptr22, i8** %str.addr, align 4, !tbaa !2
  br label %do.body23

do.body23:                                        ; preds = %if.then21
  %30 = bitcast i32* %dec_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call24 = call i8* @decode_decimal(i8* %31, i32* %dec_x) #4
  store i8* %call24, i8** %str.addr, align 4, !tbaa !2
  %32 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %cmp25 = icmp eq i8* %32, null
  br i1 %cmp25, label %if.then27, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %do.body23
  %33 = load i32, i32* %dec_x, align 4, !tbaa !10
  %cmp26 = icmp ugt i32 %33, -1
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %lor.lhs.false, %do.body23
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

if.end28:                                         ; preds = %lor.lhs.false
  %34 = load i32, i32* %dec_x, align 4, !tbaa !10
  %35 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %version29 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %35, i32 0, i32 14
  store i32 %34, i32* %version29, align 4, !tbaa !13
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

cleanup30:                                        ; preds = %if.end28, %if.then27
  %36 = bitcast i32* %dec_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  %cleanup.dest31 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest31, label %cleanup36 [
    i32 0, label %cleanup.cont32
  ]

cleanup.cont32:                                   ; preds = %cleanup30
  br label %do.cond33

do.cond33:                                        ; preds = %cleanup.cont32
  br label %do.end34

do.end34:                                         ; preds = %do.cond33
  br label %if.end35

if.end35:                                         ; preds = %do.end34, %do.body17
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup36

cleanup36:                                        ; preds = %if.end35, %cleanup30
  %37 = bitcast i32* %cc_len18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #3
  %cleanup.dest37 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest37, label %cleanup175 [
    i32 0, label %cleanup.cont38
  ]

cleanup.cont38:                                   ; preds = %cleanup36
  br label %do.cond39

do.cond39:                                        ; preds = %cleanup.cont38
  br label %do.end40

do.end40:                                         ; preds = %do.cond39
  br label %do.body41

do.body41:                                        ; preds = %do.end40
  %38 = bitcast i32* %cc_len42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #3
  store i32 3, i32* %cc_len42, align 4, !tbaa !10
  %39 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %40 = load i32, i32* %cc_len42, align 4, !tbaa !10
  %call43 = call i32 @strncmp(i8* %39, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0), i32 %40) #4
  %cmp44 = icmp ne i32 %call43, 0
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %do.body41
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup48

if.end46:                                         ; preds = %do.body41
  %41 = load i32, i32* %cc_len42, align 4, !tbaa !10
  %42 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr47 = getelementptr inbounds i8, i8* %42, i32 %41
  store i8* %add.ptr47, i8** %str.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup48

cleanup48:                                        ; preds = %if.end46, %if.then45
  %43 = bitcast i32* %cc_len42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  %cleanup.dest49 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest49, label %cleanup175 [
    i32 0, label %cleanup.cont50
  ]

cleanup.cont50:                                   ; preds = %cleanup48
  br label %do.cond51

do.cond51:                                        ; preds = %cleanup.cont50
  br label %do.end52

do.end52:                                         ; preds = %do.cond51
  br label %do.body53

do.body53:                                        ; preds = %do.end52
  %44 = bitcast i32* %dec_x54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call55 = call i8* @decode_decimal(i8* %45, i32* %dec_x54) #4
  store i8* %call55, i8** %str.addr, align 4, !tbaa !2
  %46 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %cmp56 = icmp eq i8* %46, null
  br i1 %cmp56, label %if.then59, label %lor.lhs.false57

lor.lhs.false57:                                  ; preds = %do.body53
  %47 = load i32, i32* %dec_x54, align 4, !tbaa !10
  %cmp58 = icmp ugt i32 %47, -1
  br i1 %cmp58, label %if.then59, label %if.end60

if.then59:                                        ; preds = %lor.lhs.false57, %do.body53
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end60:                                         ; preds = %lor.lhs.false57
  %48 = load i32, i32* %dec_x54, align 4, !tbaa !10
  %49 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %49, i32 0, i32 11
  store i32 %48, i32* %m_cost, align 4, !tbaa !14
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

cleanup61:                                        ; preds = %if.end60, %if.then59
  %50 = bitcast i32* %dec_x54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %cleanup.dest62 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest62, label %cleanup175 [
    i32 0, label %cleanup.cont63
  ]

cleanup.cont63:                                   ; preds = %cleanup61
  br label %do.cond64

do.cond64:                                        ; preds = %cleanup.cont63
  br label %do.end65

do.end65:                                         ; preds = %do.cond64
  br label %do.body66

do.body66:                                        ; preds = %do.end65
  %51 = bitcast i32* %cc_len67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #3
  store i32 3, i32* %cc_len67, align 4, !tbaa !10
  %52 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %53 = load i32, i32* %cc_len67, align 4, !tbaa !10
  %call68 = call i32 @strncmp(i8* %52, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0), i32 %53) #4
  %cmp69 = icmp ne i32 %call68, 0
  br i1 %cmp69, label %if.then70, label %if.end71

if.then70:                                        ; preds = %do.body66
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup73

if.end71:                                         ; preds = %do.body66
  %54 = load i32, i32* %cc_len67, align 4, !tbaa !10
  %55 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr72 = getelementptr inbounds i8, i8* %55, i32 %54
  store i8* %add.ptr72, i8** %str.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup73

cleanup73:                                        ; preds = %if.end71, %if.then70
  %56 = bitcast i32* %cc_len67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %cleanup.dest74 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest74, label %cleanup175 [
    i32 0, label %cleanup.cont75
  ]

cleanup.cont75:                                   ; preds = %cleanup73
  br label %do.cond76

do.cond76:                                        ; preds = %cleanup.cont75
  br label %do.end77

do.end77:                                         ; preds = %do.cond76
  br label %do.body78

do.body78:                                        ; preds = %do.end77
  %57 = bitcast i32* %dec_x79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #3
  %58 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call80 = call i8* @decode_decimal(i8* %58, i32* %dec_x79) #4
  store i8* %call80, i8** %str.addr, align 4, !tbaa !2
  %59 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %cmp81 = icmp eq i8* %59, null
  br i1 %cmp81, label %if.then84, label %lor.lhs.false82

lor.lhs.false82:                                  ; preds = %do.body78
  %60 = load i32, i32* %dec_x79, align 4, !tbaa !10
  %cmp83 = icmp ugt i32 %60, -1
  br i1 %cmp83, label %if.then84, label %if.end85

if.then84:                                        ; preds = %lor.lhs.false82, %do.body78
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup86

if.end85:                                         ; preds = %lor.lhs.false82
  %61 = load i32, i32* %dec_x79, align 4, !tbaa !10
  %62 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %62, i32 0, i32 10
  store i32 %61, i32* %t_cost, align 4, !tbaa !15
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup86

cleanup86:                                        ; preds = %if.end85, %if.then84
  %63 = bitcast i32* %dec_x79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #3
  %cleanup.dest87 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest87, label %cleanup175 [
    i32 0, label %cleanup.cont88
  ]

cleanup.cont88:                                   ; preds = %cleanup86
  br label %do.cond89

do.cond89:                                        ; preds = %cleanup.cont88
  br label %do.end90

do.end90:                                         ; preds = %do.cond89
  br label %do.body91

do.body91:                                        ; preds = %do.end90
  %64 = bitcast i32* %cc_len92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  store i32 3, i32* %cc_len92, align 4, !tbaa !10
  %65 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %66 = load i32, i32* %cc_len92, align 4, !tbaa !10
  %call93 = call i32 @strncmp(i8* %65, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %66) #4
  %cmp94 = icmp ne i32 %call93, 0
  br i1 %cmp94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %do.body91
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup98

if.end96:                                         ; preds = %do.body91
  %67 = load i32, i32* %cc_len92, align 4, !tbaa !10
  %68 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr97 = getelementptr inbounds i8, i8* %68, i32 %67
  store i8* %add.ptr97, i8** %str.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup98

cleanup98:                                        ; preds = %if.end96, %if.then95
  %69 = bitcast i32* %cc_len92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %cleanup.dest99 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest99, label %cleanup175 [
    i32 0, label %cleanup.cont100
  ]

cleanup.cont100:                                  ; preds = %cleanup98
  br label %do.cond101

do.cond101:                                       ; preds = %cleanup.cont100
  br label %do.end102

do.end102:                                        ; preds = %do.cond101
  br label %do.body103

do.body103:                                       ; preds = %do.end102
  %70 = bitcast i32* %dec_x104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #3
  %71 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call105 = call i8* @decode_decimal(i8* %71, i32* %dec_x104) #4
  store i8* %call105, i8** %str.addr, align 4, !tbaa !2
  %72 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %cmp106 = icmp eq i8* %72, null
  br i1 %cmp106, label %if.then109, label %lor.lhs.false107

lor.lhs.false107:                                 ; preds = %do.body103
  %73 = load i32, i32* %dec_x104, align 4, !tbaa !10
  %cmp108 = icmp ugt i32 %73, -1
  br i1 %cmp108, label %if.then109, label %if.end110

if.then109:                                       ; preds = %lor.lhs.false107, %do.body103
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

if.end110:                                        ; preds = %lor.lhs.false107
  %74 = load i32, i32* %dec_x104, align 4, !tbaa !10
  %75 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %75, i32 0, i32 12
  store i32 %74, i32* %lanes, align 4, !tbaa !16
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

cleanup111:                                       ; preds = %if.end110, %if.then109
  %76 = bitcast i32* %dec_x104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %cleanup.dest112 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest112, label %cleanup175 [
    i32 0, label %cleanup.cont113
  ]

cleanup.cont113:                                  ; preds = %cleanup111
  br label %do.cond114

do.cond114:                                       ; preds = %cleanup.cont113
  br label %do.end115

do.end115:                                        ; preds = %do.cond114
  %77 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %lanes116 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %77, i32 0, i32 12
  %78 = load i32, i32* %lanes116, align 4, !tbaa !16
  %79 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %79, i32 0, i32 13
  store i32 %78, i32* %threads, align 4, !tbaa !17
  br label %do.body117

do.body117:                                       ; preds = %do.end115
  %80 = bitcast i32* %cc_len118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #3
  store i32 1, i32* %cc_len118, align 4, !tbaa !10
  %81 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %82 = load i32, i32* %cc_len118, align 4, !tbaa !10
  %call119 = call i32 @strncmp(i8* %81, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i32 %82) #4
  %cmp120 = icmp ne i32 %call119, 0
  br i1 %cmp120, label %if.then121, label %if.end122

if.then121:                                       ; preds = %do.body117
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup124

if.end122:                                        ; preds = %do.body117
  %83 = load i32, i32* %cc_len118, align 4, !tbaa !10
  %84 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr123 = getelementptr inbounds i8, i8* %84, i32 %83
  store i8* %add.ptr123, i8** %str.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup124

cleanup124:                                       ; preds = %if.end122, %if.then121
  %85 = bitcast i32* %cc_len118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #3
  %cleanup.dest125 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest125, label %cleanup175 [
    i32 0, label %cleanup.cont126
  ]

cleanup.cont126:                                  ; preds = %cleanup124
  br label %do.cond127

do.cond127:                                       ; preds = %cleanup.cont126
  br label %do.end128

do.end128:                                        ; preds = %do.cond127
  br label %do.body129

do.body129:                                       ; preds = %do.end128
  %86 = bitcast i32* %bin_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #3
  %87 = load i32, i32* %maxsaltlen, align 4, !tbaa !10
  store i32 %87, i32* %bin_len, align 4, !tbaa !10
  %88 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %88, i32 0, i32 4
  %89 = load i8*, i8** %salt, align 4, !tbaa !18
  %90 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call130 = call i8* @from_base64(i8* %89, i32* %bin_len, i8* %90) #4
  store i8* %call130, i8** %str.addr, align 4, !tbaa !2
  %91 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %cmp131 = icmp eq i8* %91, null
  br i1 %cmp131, label %if.then134, label %lor.lhs.false132

lor.lhs.false132:                                 ; preds = %do.body129
  %92 = load i32, i32* %bin_len, align 4, !tbaa !10
  %cmp133 = icmp ugt i32 %92, -1
  br i1 %cmp133, label %if.then134, label %if.end135

if.then134:                                       ; preds = %lor.lhs.false132, %do.body129
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup137

if.end135:                                        ; preds = %lor.lhs.false132
  %93 = load i32, i32* %bin_len, align 4, !tbaa !10
  %94 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %saltlen136 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %94, i32 0, i32 5
  store i32 %93, i32* %saltlen136, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup137

cleanup137:                                       ; preds = %if.end135, %if.then134
  %95 = bitcast i32* %bin_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  %cleanup.dest138 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest138, label %cleanup175 [
    i32 0, label %cleanup.cont139
  ]

cleanup.cont139:                                  ; preds = %cleanup137
  br label %do.cond140

do.cond140:                                       ; preds = %cleanup.cont139
  br label %do.end141

do.end141:                                        ; preds = %do.cond140
  br label %do.body142

do.body142:                                       ; preds = %do.end141
  %96 = bitcast i32* %cc_len143 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #3
  store i32 1, i32* %cc_len143, align 4, !tbaa !10
  %97 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %98 = load i32, i32* %cc_len143, align 4, !tbaa !10
  %call144 = call i32 @strncmp(i8* %97, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i32 %98) #4
  %cmp145 = icmp ne i32 %call144, 0
  br i1 %cmp145, label %if.then146, label %if.end147

if.then146:                                       ; preds = %do.body142
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup149

if.end147:                                        ; preds = %do.body142
  %99 = load i32, i32* %cc_len143, align 4, !tbaa !10
  %100 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %add.ptr148 = getelementptr inbounds i8, i8* %100, i32 %99
  store i8* %add.ptr148, i8** %str.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup149

cleanup149:                                       ; preds = %if.end147, %if.then146
  %101 = bitcast i32* %cc_len143 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #3
  %cleanup.dest150 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest150, label %cleanup175 [
    i32 0, label %cleanup.cont151
  ]

cleanup.cont151:                                  ; preds = %cleanup149
  br label %do.cond152

do.cond152:                                       ; preds = %cleanup.cont151
  br label %do.end153

do.end153:                                        ; preds = %do.cond152
  br label %do.body154

do.body154:                                       ; preds = %do.end153
  %102 = bitcast i32* %bin_len155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #3
  %103 = load i32, i32* %maxoutlen, align 4, !tbaa !10
  store i32 %103, i32* %bin_len155, align 4, !tbaa !10
  %104 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %104, i32 0, i32 0
  %105 = load i8*, i8** %out, align 4, !tbaa !19
  %106 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call156 = call i8* @from_base64(i8* %105, i32* %bin_len155, i8* %106) #4
  store i8* %call156, i8** %str.addr, align 4, !tbaa !2
  %107 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %cmp157 = icmp eq i8* %107, null
  br i1 %cmp157, label %if.then160, label %lor.lhs.false158

lor.lhs.false158:                                 ; preds = %do.body154
  %108 = load i32, i32* %bin_len155, align 4, !tbaa !10
  %cmp159 = icmp ugt i32 %108, -1
  br i1 %cmp159, label %if.then160, label %if.end161

if.then160:                                       ; preds = %lor.lhs.false158, %do.body154
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup163

if.end161:                                        ; preds = %lor.lhs.false158
  %109 = load i32, i32* %bin_len155, align 4, !tbaa !10
  %110 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %outlen162 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %110, i32 0, i32 1
  store i32 %109, i32* %outlen162, align 4, !tbaa !12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup163

cleanup163:                                       ; preds = %if.end161, %if.then160
  %111 = bitcast i32* %bin_len155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %cleanup.dest164 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest164, label %cleanup175 [
    i32 0, label %cleanup.cont165
  ]

cleanup.cont165:                                  ; preds = %cleanup163
  br label %do.cond166

do.cond166:                                       ; preds = %cleanup.cont165
  br label %do.end167

do.end167:                                        ; preds = %do.cond166
  %112 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %112, i32 0, i32 6
  store i8* null, i8** %secret, align 4, !tbaa !20
  %113 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %113, i32 0, i32 7
  store i32 0, i32* %secretlen, align 4, !tbaa !21
  %114 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %114, i32 0, i32 8
  store i8* null, i8** %ad, align 4, !tbaa !22
  %115 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %115, i32 0, i32 9
  store i32 0, i32* %adlen, align 4, !tbaa !23
  %116 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %allocate_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %116, i32 0, i32 15
  store i32 (i8**, i32)* null, i32 (i8**, i32)** %allocate_cbk, align 4, !tbaa !24
  %117 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %free_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %117, i32 0, i32 16
  store void (i8*, i32)* null, void (i8*, i32)** %free_cbk, align 4, !tbaa !25
  %118 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %118, i32 0, i32 17
  store i32 0, i32* %flags, align 4, !tbaa !26
  %119 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %call168 = call i32 @validate_inputs(%struct.Argon2_Context* %119) #4
  store i32 %call168, i32* %validation_result, align 4, !tbaa !27
  %120 = load i32, i32* %validation_result, align 4, !tbaa !27
  %cmp169 = icmp ne i32 %120, 0
  br i1 %cmp169, label %if.then170, label %if.end171

if.then170:                                       ; preds = %do.end167
  %121 = load i32, i32* %validation_result, align 4, !tbaa !27
  store i32 %121, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup175

if.end171:                                        ; preds = %do.end167
  %122 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %123 = load i8, i8* %122, align 1, !tbaa !6
  %conv = sext i8 %123 to i32
  %cmp172 = icmp eq i32 %conv, 0
  br i1 %cmp172, label %if.then174, label %if.else

if.then174:                                       ; preds = %if.end171
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup175

if.else:                                          ; preds = %if.end171
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup175

cleanup175:                                       ; preds = %if.else, %if.then174, %if.then170, %cleanup163, %cleanup149, %cleanup137, %cleanup124, %cleanup111, %cleanup98, %cleanup86, %cleanup73, %cleanup61, %cleanup48, %cleanup36, %cleanup12, %cleanup, %if.then
  %124 = bitcast i8** %type_string to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast i32* %validation_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  %126 = bitcast i32* %maxoutlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %maxsaltlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = load i32, i32* %retval, align 4
  ret i32 %128
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: optsize
declare i8* @argon2_type2string(i32, i32) #2

; Function Attrs: optsize
declare i32 @strncmp(i8*, i8*, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: optsize
declare i32 @strlen(i8*) #2

; Function Attrs: noinline nounwind optsize
define internal i8* @decode_decimal(i8* %str, i32* %v) #0 {
entry:
  %retval = alloca i8*, align 4
  %str.addr = alloca i8*, align 4
  %v.addr = alloca i32*, align 4
  %orig = alloca i8*, align 4
  %acc = alloca i32, align 4
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4, !tbaa !2
  store i32* %v, i32** %v.addr, align 4, !tbaa !2
  %0 = bitcast i8** %orig to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %acc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %acc, align 4, !tbaa !10
  %2 = load i8*, i8** %str.addr, align 4, !tbaa !2
  store i8* %2, i8** %orig, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %5 = load i8, i8* %4, align 1, !tbaa !6
  %conv = sext i8 %5 to i32
  store i32 %conv, i32* %c, align 4, !tbaa !27
  %6 = load i32, i32* %c, align 4, !tbaa !27
  %cmp = icmp slt i32 %6, 48
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.cond
  %7 = load i32, i32* %c, align 4, !tbaa !27
  %cmp2 = icmp sgt i32 %7, 57
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %8 = load i32, i32* %c, align 4, !tbaa !27
  %sub = sub nsw i32 %8, 48
  store i32 %sub, i32* %c, align 4, !tbaa !27
  %9 = load i32, i32* %acc, align 4, !tbaa !10
  %cmp4 = icmp ugt i32 %9, 429496729
  br i1 %cmp4, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %10 = load i32, i32* %acc, align 4, !tbaa !10
  %mul = mul i32 %10, 10
  store i32 %mul, i32* %acc, align 4, !tbaa !10
  %11 = load i32, i32* %c, align 4, !tbaa !27
  %12 = load i32, i32* %acc, align 4, !tbaa !10
  %sub8 = sub i32 -1, %12
  %cmp9 = icmp ugt i32 %11, %sub8
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end7
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end7
  %13 = load i32, i32* %c, align 4, !tbaa !27
  %14 = load i32, i32* %acc, align 4, !tbaa !10
  %add = add i32 %14, %13
  store i32 %add, i32* %acc, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end12, %if.then11, %if.then6, %if.then
  %15 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup23 [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %16 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %16, i32 1
  store i8* %incdec.ptr, i8** %str.addr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %17 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %orig, align 4, !tbaa !2
  %cmp13 = icmp eq i8* %17, %18
  br i1 %cmp13, label %if.then21, label %lor.lhs.false15

lor.lhs.false15:                                  ; preds = %for.end
  %19 = load i8*, i8** %orig, align 4, !tbaa !2
  %20 = load i8, i8* %19, align 1, !tbaa !6
  %conv16 = sext i8 %20 to i32
  %cmp17 = icmp eq i32 %conv16, 48
  br i1 %cmp17, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %lor.lhs.false15
  %21 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %orig, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 1
  %cmp19 = icmp ne i8* %21, %add.ptr
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %land.lhs.true, %for.end
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.end22:                                         ; preds = %land.lhs.true, %lor.lhs.false15
  %23 = load i32, i32* %acc, align 4, !tbaa !10
  %24 = load i32*, i32** %v.addr, align 4, !tbaa !2
  store i32 %23, i32* %24, align 4, !tbaa !10
  %25 = load i8*, i8** %str.addr, align 4, !tbaa !2
  store i8* %25, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %if.then21, %cleanup
  %26 = bitcast i32* %acc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  %27 = bitcast i8** %orig to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = load i8*, i8** %retval, align 4
  ret i8* %28
}

; Function Attrs: noinline nounwind optsize
define internal i8* @from_base64(i8* %dst, i32* %dst_len, i8* %src) #0 {
entry:
  %retval = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %dst_len.addr = alloca i32*, align 4
  %src.addr = alloca i8*, align 4
  %len = alloca i32, align 4
  %buf = alloca i8*, align 4
  %acc = alloca i32, align 4
  %acc_len = alloca i32, align 4
  %d = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32* %dst_len, i32** %dst_len.addr, align 4, !tbaa !2
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  %0 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %acc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %acc_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  store i8* %4, i8** %buf, align 4, !tbaa !2
  store i32 0, i32* %len, align 4, !tbaa !10
  store i32 0, i32* %acc, align 4, !tbaa !27
  store i32 0, i32* %acc_len, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %entry
  %5 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %7 = load i8, i8* %6, align 1, !tbaa !6
  %conv = sext i8 %7 to i32
  %call = call i32 @b64_char_to_byte(i32 %conv) #4
  store i32 %call, i32* %d, align 4, !tbaa !27
  %8 = load i32, i32* %d, align 4, !tbaa !27
  %cmp = icmp eq i32 %8, 255
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.cond
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %9, i32 1
  store i8* %incdec.ptr, i8** %src.addr, align 4, !tbaa !2
  %10 = load i32, i32* %acc, align 4, !tbaa !27
  %shl = shl i32 %10, 6
  %11 = load i32, i32* %d, align 4, !tbaa !27
  %add = add i32 %shl, %11
  store i32 %add, i32* %acc, align 4, !tbaa !27
  %12 = load i32, i32* %acc_len, align 4, !tbaa !27
  %add2 = add i32 %12, 6
  store i32 %add2, i32* %acc_len, align 4, !tbaa !27
  %13 = load i32, i32* %acc_len, align 4, !tbaa !27
  %cmp3 = icmp uge i32 %13, 8
  br i1 %cmp3, label %if.then5, label %if.end12

if.then5:                                         ; preds = %if.end
  %14 = load i32, i32* %acc_len, align 4, !tbaa !27
  %sub = sub i32 %14, 8
  store i32 %sub, i32* %acc_len, align 4, !tbaa !27
  %15 = load i32, i32* %len, align 4, !tbaa !10
  %inc = add i32 %15, 1
  store i32 %inc, i32* %len, align 4, !tbaa !10
  %16 = load i32*, i32** %dst_len.addr, align 4, !tbaa !2
  %17 = load i32, i32* %16, align 4, !tbaa !10
  %cmp6 = icmp uge i32 %15, %17
  br i1 %cmp6, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.then5
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.then5
  %18 = load i32, i32* %acc, align 4, !tbaa !27
  %19 = load i32, i32* %acc_len, align 4, !tbaa !27
  %shr = lshr i32 %18, %19
  %and = and i32 %shr, 255
  %conv10 = trunc i32 %and to i8
  %20 = load i8*, i8** %buf, align 4, !tbaa !2
  %incdec.ptr11 = getelementptr inbounds i8, i8* %20, i32 1
  store i8* %incdec.ptr11, i8** %buf, align 4, !tbaa !2
  store i8 %conv10, i8* %20, align 1, !tbaa !6
  br label %if.end12

if.end12:                                         ; preds = %if.end9, %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end12, %if.then8, %if.then
  %21 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup22 [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %22 = load i32, i32* %acc_len, align 4, !tbaa !27
  %cmp13 = icmp ugt i32 %22, 4
  br i1 %cmp13, label %if.then20, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %23 = load i32, i32* %acc, align 4, !tbaa !27
  %24 = load i32, i32* %acc_len, align 4, !tbaa !27
  %shl15 = shl i32 1, %24
  %sub16 = sub i32 %shl15, 1
  %and17 = and i32 %23, %sub16
  %cmp18 = icmp ne i32 %and17, 0
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %lor.lhs.false, %for.end
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup22

if.end21:                                         ; preds = %lor.lhs.false
  %25 = load i32, i32* %len, align 4, !tbaa !10
  %26 = load i32*, i32** %dst_len.addr, align 4, !tbaa !2
  store i32 %25, i32* %26, align 4, !tbaa !10
  %27 = load i8*, i8** %src.addr, align 4, !tbaa !2
  store i8* %27, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup22

cleanup22:                                        ; preds = %if.end21, %if.then20, %cleanup
  %28 = bitcast i32* %acc_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  %29 = bitcast i32* %acc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  %30 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = load i8*, i8** %retval, align 4
  ret i8* %32
}

; Function Attrs: optsize
declare i32 @validate_inputs(%struct.Argon2_Context*) #2

; Function Attrs: noinline nounwind optsize
define hidden i32 @encode_string(i8* %dst, i32 %dst_len, %struct.Argon2_Context* %ctx, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_len.addr = alloca i32, align 4
  %ctx.addr = alloca %struct.Argon2_Context*, align 4
  %type.addr = alloca i32, align 4
  %type_string = alloca i8*, align 4
  %validation_result = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pp_len = alloca i32, align 4
  %pp_len8 = alloca i32, align 4
  %pp_len22 = alloca i32, align 4
  %tmp = alloca [30 x i8], align 16
  %pp_len37 = alloca i32, align 4
  %pp_len58 = alloca i32, align 4
  %tmp71 = alloca [30 x i8], align 16
  %pp_len75 = alloca i32, align 4
  %pp_len96 = alloca i32, align 4
  %tmp109 = alloca [30 x i8], align 16
  %pp_len113 = alloca i32, align 4
  %pp_len134 = alloca i32, align 4
  %tmp147 = alloca [30 x i8], align 16
  %pp_len151 = alloca i32, align 4
  %pp_len172 = alloca i32, align 4
  %sb_len = alloca i32, align 4
  %pp_len197 = alloca i32, align 4
  %sb_len210 = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_len, i32* %dst_len.addr, align 4, !tbaa !10
  store %struct.Argon2_Context* %ctx, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  store i32 %type, i32* %type.addr, align 4, !tbaa !6
  %0 = bitcast i8** %type_string to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %type.addr, align 4, !tbaa !6
  %call = call i8* @argon2_type2string(i32 %1, i32 0) #4
  store i8* %call, i8** %type_string, align 4, !tbaa !2
  %2 = bitcast i32* %validation_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %call1 = call i32 @validate_inputs(%struct.Argon2_Context* %3) #4
  store i32 %call1, i32* %validation_result, align 4, !tbaa !27
  %4 = load i8*, i8** %type_string, align 4, !tbaa !2
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup222

if.end:                                           ; preds = %entry
  %5 = load i32, i32* %validation_result, align 4, !tbaa !27
  %cmp = icmp ne i32 %5, 0
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %6 = load i32, i32* %validation_result, align 4, !tbaa !27
  store i32 %6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup222

if.end3:                                          ; preds = %if.end
  br label %do.body

do.body:                                          ; preds = %if.end3
  %7 = bitcast i32* %pp_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store i32 1, i32* %pp_len, align 4, !tbaa !10
  %8 = load i32, i32* %pp_len, align 4, !tbaa !10
  %9 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp4 = icmp uge i32 %8, %9
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %do.body
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %do.body
  %10 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %11 = load i32, i32* %pp_len, align 4, !tbaa !10
  %add = add i32 %11, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %10, i8* align 1 getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i32 %add, i1 false)
  %12 = load i32, i32* %pp_len, align 4, !tbaa !10
  %13 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %12
  store i8* %add.ptr, i8** %dst.addr, align 4, !tbaa !2
  %14 = load i32, i32* %pp_len, align 4, !tbaa !10
  %15 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub = sub i32 %15, %14
  store i32 %sub, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end6, %if.then5
  %16 = bitcast i32* %pp_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup222 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body7

do.body7:                                         ; preds = %do.end
  %17 = bitcast i32* %pp_len8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load i8*, i8** %type_string, align 4, !tbaa !2
  %call9 = call i32 @strlen(i8* %18) #4
  store i32 %call9, i32* %pp_len8, align 4, !tbaa !10
  %19 = load i32, i32* %pp_len8, align 4, !tbaa !10
  %20 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp10 = icmp uge i32 %19, %20
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %do.body7
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

if.end12:                                         ; preds = %do.body7
  %21 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %type_string, align 4, !tbaa !2
  %23 = load i32, i32* %pp_len8, align 4, !tbaa !10
  %add13 = add i32 %23, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %21, i8* align 1 %22, i32 %add13, i1 false)
  %24 = load i32, i32* %pp_len8, align 4, !tbaa !10
  %25 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr14 = getelementptr inbounds i8, i8* %25, i32 %24
  store i8* %add.ptr14, i8** %dst.addr, align 4, !tbaa !2
  %26 = load i32, i32* %pp_len8, align 4, !tbaa !10
  %27 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub15 = sub i32 %27, %26
  store i32 %sub15, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

cleanup16:                                        ; preds = %if.end12, %if.then11
  %28 = bitcast i32* %pp_len8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  %cleanup.dest17 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest17, label %cleanup222 [
    i32 0, label %cleanup.cont18
  ]

cleanup.cont18:                                   ; preds = %cleanup16
  br label %do.cond19

do.cond19:                                        ; preds = %cleanup.cont18
  br label %do.end20

do.end20:                                         ; preds = %do.cond19
  br label %do.body21

do.body21:                                        ; preds = %do.end20
  %29 = bitcast i32* %pp_len22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #3
  store i32 3, i32* %pp_len22, align 4, !tbaa !10
  %30 = load i32, i32* %pp_len22, align 4, !tbaa !10
  %31 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp23 = icmp uge i32 %30, %31
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %do.body21
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

if.end25:                                         ; preds = %do.body21
  %32 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %33 = load i32, i32* %pp_len22, align 4, !tbaa !10
  %add26 = add i32 %33, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %32, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i32 0, i32 0), i32 %add26, i1 false)
  %34 = load i32, i32* %pp_len22, align 4, !tbaa !10
  %35 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr27 = getelementptr inbounds i8, i8* %35, i32 %34
  store i8* %add.ptr27, i8** %dst.addr, align 4, !tbaa !2
  %36 = load i32, i32* %pp_len22, align 4, !tbaa !10
  %37 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub28 = sub i32 %37, %36
  store i32 %sub28, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

cleanup29:                                        ; preds = %if.end25, %if.then24
  %38 = bitcast i32* %pp_len22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %cleanup.dest30 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest30, label %cleanup222 [
    i32 0, label %cleanup.cont31
  ]

cleanup.cont31:                                   ; preds = %cleanup29
  br label %do.cond32

do.cond32:                                        ; preds = %cleanup.cont31
  br label %do.end33

do.end33:                                         ; preds = %do.cond32
  br label %do.body34

do.body34:                                        ; preds = %do.end33
  %39 = bitcast [30 x i8]* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 30, i8* %39) #3
  %arraydecay = getelementptr inbounds [30 x i8], [30 x i8]* %tmp, i32 0, i32 0
  %40 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %version = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %40, i32 0, i32 14
  %41 = load i32, i32* %version, align 4, !tbaa !13
  %call35 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %41) #4
  br label %do.body36

do.body36:                                        ; preds = %do.body34
  %42 = bitcast i32* %pp_len37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #3
  %arraydecay38 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp, i32 0, i32 0
  %call39 = call i32 @strlen(i8* %arraydecay38) #4
  store i32 %call39, i32* %pp_len37, align 4, !tbaa !10
  %43 = load i32, i32* %pp_len37, align 4, !tbaa !10
  %44 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp40 = icmp uge i32 %43, %44
  br i1 %cmp40, label %if.then41, label %if.end42

if.then41:                                        ; preds = %do.body36
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup47

if.end42:                                         ; preds = %do.body36
  %45 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %arraydecay43 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp, i32 0, i32 0
  %46 = load i32, i32* %pp_len37, align 4, !tbaa !10
  %add44 = add i32 %46, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %45, i8* align 16 %arraydecay43, i32 %add44, i1 false)
  %47 = load i32, i32* %pp_len37, align 4, !tbaa !10
  %48 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr45 = getelementptr inbounds i8, i8* %48, i32 %47
  store i8* %add.ptr45, i8** %dst.addr, align 4, !tbaa !2
  %49 = load i32, i32* %pp_len37, align 4, !tbaa !10
  %50 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub46 = sub i32 %50, %49
  store i32 %sub46, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup47

cleanup47:                                        ; preds = %if.end42, %if.then41
  %51 = bitcast i32* %pp_len37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %cleanup.dest48 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest48, label %cleanup52 [
    i32 0, label %cleanup.cont49
  ]

cleanup.cont49:                                   ; preds = %cleanup47
  br label %do.cond50

do.cond50:                                        ; preds = %cleanup.cont49
  br label %do.end51

do.end51:                                         ; preds = %do.cond50
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

cleanup52:                                        ; preds = %do.end51, %cleanup47
  %52 = bitcast [30 x i8]* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 30, i8* %52) #3
  %cleanup.dest53 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest53, label %cleanup222 [
    i32 0, label %cleanup.cont54
  ]

cleanup.cont54:                                   ; preds = %cleanup52
  br label %do.cond55

do.cond55:                                        ; preds = %cleanup.cont54
  br label %do.end56

do.end56:                                         ; preds = %do.cond55
  br label %do.body57

do.body57:                                        ; preds = %do.end56
  %53 = bitcast i32* %pp_len58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #3
  store i32 3, i32* %pp_len58, align 4, !tbaa !10
  %54 = load i32, i32* %pp_len58, align 4, !tbaa !10
  %55 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp59 = icmp uge i32 %54, %55
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %do.body57
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup65

if.end61:                                         ; preds = %do.body57
  %56 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %57 = load i32, i32* %pp_len58, align 4, !tbaa !10
  %add62 = add i32 %57, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %56, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0), i32 %add62, i1 false)
  %58 = load i32, i32* %pp_len58, align 4, !tbaa !10
  %59 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr63 = getelementptr inbounds i8, i8* %59, i32 %58
  store i8* %add.ptr63, i8** %dst.addr, align 4, !tbaa !2
  %60 = load i32, i32* %pp_len58, align 4, !tbaa !10
  %61 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub64 = sub i32 %61, %60
  store i32 %sub64, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup65

cleanup65:                                        ; preds = %if.end61, %if.then60
  %62 = bitcast i32* %pp_len58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #3
  %cleanup.dest66 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest66, label %cleanup222 [
    i32 0, label %cleanup.cont67
  ]

cleanup.cont67:                                   ; preds = %cleanup65
  br label %do.cond68

do.cond68:                                        ; preds = %cleanup.cont67
  br label %do.end69

do.end69:                                         ; preds = %do.cond68
  br label %do.body70

do.body70:                                        ; preds = %do.end69
  %63 = bitcast [30 x i8]* %tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 30, i8* %63) #3
  %arraydecay72 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp71, i32 0, i32 0
  %64 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %64, i32 0, i32 11
  %65 = load i32, i32* %m_cost, align 4, !tbaa !14
  %call73 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay72, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %65) #4
  br label %do.body74

do.body74:                                        ; preds = %do.body70
  %66 = bitcast i32* %pp_len75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #3
  %arraydecay76 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp71, i32 0, i32 0
  %call77 = call i32 @strlen(i8* %arraydecay76) #4
  store i32 %call77, i32* %pp_len75, align 4, !tbaa !10
  %67 = load i32, i32* %pp_len75, align 4, !tbaa !10
  %68 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp78 = icmp uge i32 %67, %68
  br i1 %cmp78, label %if.then79, label %if.end80

if.then79:                                        ; preds = %do.body74
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup85

if.end80:                                         ; preds = %do.body74
  %69 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %arraydecay81 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp71, i32 0, i32 0
  %70 = load i32, i32* %pp_len75, align 4, !tbaa !10
  %add82 = add i32 %70, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %69, i8* align 16 %arraydecay81, i32 %add82, i1 false)
  %71 = load i32, i32* %pp_len75, align 4, !tbaa !10
  %72 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr83 = getelementptr inbounds i8, i8* %72, i32 %71
  store i8* %add.ptr83, i8** %dst.addr, align 4, !tbaa !2
  %73 = load i32, i32* %pp_len75, align 4, !tbaa !10
  %74 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub84 = sub i32 %74, %73
  store i32 %sub84, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup85

cleanup85:                                        ; preds = %if.end80, %if.then79
  %75 = bitcast i32* %pp_len75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %cleanup.dest86 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest86, label %cleanup90 [
    i32 0, label %cleanup.cont87
  ]

cleanup.cont87:                                   ; preds = %cleanup85
  br label %do.cond88

do.cond88:                                        ; preds = %cleanup.cont87
  br label %do.end89

do.end89:                                         ; preds = %do.cond88
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup90

cleanup90:                                        ; preds = %do.end89, %cleanup85
  %76 = bitcast [30 x i8]* %tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 30, i8* %76) #3
  %cleanup.dest91 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest91, label %cleanup222 [
    i32 0, label %cleanup.cont92
  ]

cleanup.cont92:                                   ; preds = %cleanup90
  br label %do.cond93

do.cond93:                                        ; preds = %cleanup.cont92
  br label %do.end94

do.end94:                                         ; preds = %do.cond93
  br label %do.body95

do.body95:                                        ; preds = %do.end94
  %77 = bitcast i32* %pp_len96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #3
  store i32 3, i32* %pp_len96, align 4, !tbaa !10
  %78 = load i32, i32* %pp_len96, align 4, !tbaa !10
  %79 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp97 = icmp uge i32 %78, %79
  br i1 %cmp97, label %if.then98, label %if.end99

if.then98:                                        ; preds = %do.body95
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup103

if.end99:                                         ; preds = %do.body95
  %80 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %81 = load i32, i32* %pp_len96, align 4, !tbaa !10
  %add100 = add i32 %81, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %80, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0), i32 %add100, i1 false)
  %82 = load i32, i32* %pp_len96, align 4, !tbaa !10
  %83 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr101 = getelementptr inbounds i8, i8* %83, i32 %82
  store i8* %add.ptr101, i8** %dst.addr, align 4, !tbaa !2
  %84 = load i32, i32* %pp_len96, align 4, !tbaa !10
  %85 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub102 = sub i32 %85, %84
  store i32 %sub102, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup103

cleanup103:                                       ; preds = %if.end99, %if.then98
  %86 = bitcast i32* %pp_len96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #3
  %cleanup.dest104 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest104, label %cleanup222 [
    i32 0, label %cleanup.cont105
  ]

cleanup.cont105:                                  ; preds = %cleanup103
  br label %do.cond106

do.cond106:                                       ; preds = %cleanup.cont105
  br label %do.end107

do.end107:                                        ; preds = %do.cond106
  br label %do.body108

do.body108:                                       ; preds = %do.end107
  %87 = bitcast [30 x i8]* %tmp109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 30, i8* %87) #3
  %arraydecay110 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp109, i32 0, i32 0
  %88 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %88, i32 0, i32 10
  %89 = load i32, i32* %t_cost, align 4, !tbaa !15
  %call111 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay110, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %89) #4
  br label %do.body112

do.body112:                                       ; preds = %do.body108
  %90 = bitcast i32* %pp_len113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #3
  %arraydecay114 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp109, i32 0, i32 0
  %call115 = call i32 @strlen(i8* %arraydecay114) #4
  store i32 %call115, i32* %pp_len113, align 4, !tbaa !10
  %91 = load i32, i32* %pp_len113, align 4, !tbaa !10
  %92 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp116 = icmp uge i32 %91, %92
  br i1 %cmp116, label %if.then117, label %if.end118

if.then117:                                       ; preds = %do.body112
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup123

if.end118:                                        ; preds = %do.body112
  %93 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %arraydecay119 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp109, i32 0, i32 0
  %94 = load i32, i32* %pp_len113, align 4, !tbaa !10
  %add120 = add i32 %94, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %93, i8* align 16 %arraydecay119, i32 %add120, i1 false)
  %95 = load i32, i32* %pp_len113, align 4, !tbaa !10
  %96 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr121 = getelementptr inbounds i8, i8* %96, i32 %95
  store i8* %add.ptr121, i8** %dst.addr, align 4, !tbaa !2
  %97 = load i32, i32* %pp_len113, align 4, !tbaa !10
  %98 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub122 = sub i32 %98, %97
  store i32 %sub122, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup123

cleanup123:                                       ; preds = %if.end118, %if.then117
  %99 = bitcast i32* %pp_len113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #3
  %cleanup.dest124 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest124, label %cleanup128 [
    i32 0, label %cleanup.cont125
  ]

cleanup.cont125:                                  ; preds = %cleanup123
  br label %do.cond126

do.cond126:                                       ; preds = %cleanup.cont125
  br label %do.end127

do.end127:                                        ; preds = %do.cond126
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup128

cleanup128:                                       ; preds = %do.end127, %cleanup123
  %100 = bitcast [30 x i8]* %tmp109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 30, i8* %100) #3
  %cleanup.dest129 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest129, label %cleanup222 [
    i32 0, label %cleanup.cont130
  ]

cleanup.cont130:                                  ; preds = %cleanup128
  br label %do.cond131

do.cond131:                                       ; preds = %cleanup.cont130
  br label %do.end132

do.end132:                                        ; preds = %do.cond131
  br label %do.body133

do.body133:                                       ; preds = %do.end132
  %101 = bitcast i32* %pp_len134 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #3
  store i32 3, i32* %pp_len134, align 4, !tbaa !10
  %102 = load i32, i32* %pp_len134, align 4, !tbaa !10
  %103 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp135 = icmp uge i32 %102, %103
  br i1 %cmp135, label %if.then136, label %if.end137

if.then136:                                       ; preds = %do.body133
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup141

if.end137:                                        ; preds = %do.body133
  %104 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %105 = load i32, i32* %pp_len134, align 4, !tbaa !10
  %add138 = add i32 %105, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %104, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %add138, i1 false)
  %106 = load i32, i32* %pp_len134, align 4, !tbaa !10
  %107 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr139 = getelementptr inbounds i8, i8* %107, i32 %106
  store i8* %add.ptr139, i8** %dst.addr, align 4, !tbaa !2
  %108 = load i32, i32* %pp_len134, align 4, !tbaa !10
  %109 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub140 = sub i32 %109, %108
  store i32 %sub140, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup141

cleanup141:                                       ; preds = %if.end137, %if.then136
  %110 = bitcast i32* %pp_len134 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %cleanup.dest142 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest142, label %cleanup222 [
    i32 0, label %cleanup.cont143
  ]

cleanup.cont143:                                  ; preds = %cleanup141
  br label %do.cond144

do.cond144:                                       ; preds = %cleanup.cont143
  br label %do.end145

do.end145:                                        ; preds = %do.cond144
  br label %do.body146

do.body146:                                       ; preds = %do.end145
  %111 = bitcast [30 x i8]* %tmp147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 30, i8* %111) #3
  %arraydecay148 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp147, i32 0, i32 0
  %112 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %112, i32 0, i32 12
  %113 = load i32, i32* %lanes, align 4, !tbaa !16
  %call149 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay148, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %113) #4
  br label %do.body150

do.body150:                                       ; preds = %do.body146
  %114 = bitcast i32* %pp_len151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #3
  %arraydecay152 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp147, i32 0, i32 0
  %call153 = call i32 @strlen(i8* %arraydecay152) #4
  store i32 %call153, i32* %pp_len151, align 4, !tbaa !10
  %115 = load i32, i32* %pp_len151, align 4, !tbaa !10
  %116 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp154 = icmp uge i32 %115, %116
  br i1 %cmp154, label %if.then155, label %if.end156

if.then155:                                       ; preds = %do.body150
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup161

if.end156:                                        ; preds = %do.body150
  %117 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %arraydecay157 = getelementptr inbounds [30 x i8], [30 x i8]* %tmp147, i32 0, i32 0
  %118 = load i32, i32* %pp_len151, align 4, !tbaa !10
  %add158 = add i32 %118, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %117, i8* align 16 %arraydecay157, i32 %add158, i1 false)
  %119 = load i32, i32* %pp_len151, align 4, !tbaa !10
  %120 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr159 = getelementptr inbounds i8, i8* %120, i32 %119
  store i8* %add.ptr159, i8** %dst.addr, align 4, !tbaa !2
  %121 = load i32, i32* %pp_len151, align 4, !tbaa !10
  %122 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub160 = sub i32 %122, %121
  store i32 %sub160, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup161

cleanup161:                                       ; preds = %if.end156, %if.then155
  %123 = bitcast i32* %pp_len151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %cleanup.dest162 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest162, label %cleanup166 [
    i32 0, label %cleanup.cont163
  ]

cleanup.cont163:                                  ; preds = %cleanup161
  br label %do.cond164

do.cond164:                                       ; preds = %cleanup.cont163
  br label %do.end165

do.end165:                                        ; preds = %do.cond164
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup166

cleanup166:                                       ; preds = %do.end165, %cleanup161
  %124 = bitcast [30 x i8]* %tmp147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 30, i8* %124) #3
  %cleanup.dest167 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest167, label %cleanup222 [
    i32 0, label %cleanup.cont168
  ]

cleanup.cont168:                                  ; preds = %cleanup166
  br label %do.cond169

do.cond169:                                       ; preds = %cleanup.cont168
  br label %do.end170

do.end170:                                        ; preds = %do.cond169
  br label %do.body171

do.body171:                                       ; preds = %do.end170
  %125 = bitcast i32* %pp_len172 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #3
  store i32 1, i32* %pp_len172, align 4, !tbaa !10
  %126 = load i32, i32* %pp_len172, align 4, !tbaa !10
  %127 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp173 = icmp uge i32 %126, %127
  br i1 %cmp173, label %if.then174, label %if.end175

if.then174:                                       ; preds = %do.body171
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup179

if.end175:                                        ; preds = %do.body171
  %128 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %129 = load i32, i32* %pp_len172, align 4, !tbaa !10
  %add176 = add i32 %129, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %128, i8* align 1 getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i32 %add176, i1 false)
  %130 = load i32, i32* %pp_len172, align 4, !tbaa !10
  %131 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr177 = getelementptr inbounds i8, i8* %131, i32 %130
  store i8* %add.ptr177, i8** %dst.addr, align 4, !tbaa !2
  %132 = load i32, i32* %pp_len172, align 4, !tbaa !10
  %133 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub178 = sub i32 %133, %132
  store i32 %sub178, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup179

cleanup179:                                       ; preds = %if.end175, %if.then174
  %134 = bitcast i32* %pp_len172 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  %cleanup.dest180 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest180, label %cleanup222 [
    i32 0, label %cleanup.cont181
  ]

cleanup.cont181:                                  ; preds = %cleanup179
  br label %do.cond182

do.cond182:                                       ; preds = %cleanup.cont181
  br label %do.end183

do.end183:                                        ; preds = %do.cond182
  br label %do.body184

do.body184:                                       ; preds = %do.end183
  %135 = bitcast i32* %sb_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #3
  %136 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %137 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %138 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %138, i32 0, i32 4
  %139 = load i8*, i8** %salt, align 4, !tbaa !18
  %140 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %140, i32 0, i32 5
  %141 = load i32, i32* %saltlen, align 4, !tbaa !7
  %call185 = call i32 @to_base64(i8* %136, i32 %137, i8* %139, i32 %141) #4
  store i32 %call185, i32* %sb_len, align 4, !tbaa !10
  %142 = load i32, i32* %sb_len, align 4, !tbaa !10
  %cmp186 = icmp eq i32 %142, -1
  br i1 %cmp186, label %if.then187, label %if.end188

if.then187:                                       ; preds = %do.body184
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup191

if.end188:                                        ; preds = %do.body184
  %143 = load i32, i32* %sb_len, align 4, !tbaa !10
  %144 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr189 = getelementptr inbounds i8, i8* %144, i32 %143
  store i8* %add.ptr189, i8** %dst.addr, align 4, !tbaa !2
  %145 = load i32, i32* %sb_len, align 4, !tbaa !10
  %146 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub190 = sub i32 %146, %145
  store i32 %sub190, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup191

cleanup191:                                       ; preds = %if.end188, %if.then187
  %147 = bitcast i32* %sb_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #3
  %cleanup.dest192 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest192, label %cleanup222 [
    i32 0, label %cleanup.cont193
  ]

cleanup.cont193:                                  ; preds = %cleanup191
  br label %do.cond194

do.cond194:                                       ; preds = %cleanup.cont193
  br label %do.end195

do.end195:                                        ; preds = %do.cond194
  br label %do.body196

do.body196:                                       ; preds = %do.end195
  %148 = bitcast i32* %pp_len197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #3
  store i32 1, i32* %pp_len197, align 4, !tbaa !10
  %149 = load i32, i32* %pp_len197, align 4, !tbaa !10
  %150 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %cmp198 = icmp uge i32 %149, %150
  br i1 %cmp198, label %if.then199, label %if.end200

if.then199:                                       ; preds = %do.body196
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup204

if.end200:                                        ; preds = %do.body196
  %151 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %152 = load i32, i32* %pp_len197, align 4, !tbaa !10
  %add201 = add i32 %152, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %151, i8* align 1 getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i32 %add201, i1 false)
  %153 = load i32, i32* %pp_len197, align 4, !tbaa !10
  %154 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr202 = getelementptr inbounds i8, i8* %154, i32 %153
  store i8* %add.ptr202, i8** %dst.addr, align 4, !tbaa !2
  %155 = load i32, i32* %pp_len197, align 4, !tbaa !10
  %156 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub203 = sub i32 %156, %155
  store i32 %sub203, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup204

cleanup204:                                       ; preds = %if.end200, %if.then199
  %157 = bitcast i32* %pp_len197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #3
  %cleanup.dest205 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest205, label %cleanup222 [
    i32 0, label %cleanup.cont206
  ]

cleanup.cont206:                                  ; preds = %cleanup204
  br label %do.cond207

do.cond207:                                       ; preds = %cleanup.cont206
  br label %do.end208

do.end208:                                        ; preds = %do.cond207
  br label %do.body209

do.body209:                                       ; preds = %do.end208
  %158 = bitcast i32* %sb_len210 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #3
  %159 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %160 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %161 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %161, i32 0, i32 0
  %162 = load i8*, i8** %out, align 4, !tbaa !19
  %163 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4, !tbaa !2
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %163, i32 0, i32 1
  %164 = load i32, i32* %outlen, align 4, !tbaa !12
  %call211 = call i32 @to_base64(i8* %159, i32 %160, i8* %162, i32 %164) #4
  store i32 %call211, i32* %sb_len210, align 4, !tbaa !10
  %165 = load i32, i32* %sb_len210, align 4, !tbaa !10
  %cmp212 = icmp eq i32 %165, -1
  br i1 %cmp212, label %if.then213, label %if.end214

if.then213:                                       ; preds = %do.body209
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup217

if.end214:                                        ; preds = %do.body209
  %166 = load i32, i32* %sb_len210, align 4, !tbaa !10
  %167 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr215 = getelementptr inbounds i8, i8* %167, i32 %166
  store i8* %add.ptr215, i8** %dst.addr, align 4, !tbaa !2
  %168 = load i32, i32* %sb_len210, align 4, !tbaa !10
  %169 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %sub216 = sub i32 %169, %168
  store i32 %sub216, i32* %dst_len.addr, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup217

cleanup217:                                       ; preds = %if.end214, %if.then213
  %170 = bitcast i32* %sb_len210 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #3
  %cleanup.dest218 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest218, label %cleanup222 [
    i32 0, label %cleanup.cont219
  ]

cleanup.cont219:                                  ; preds = %cleanup217
  br label %do.cond220

do.cond220:                                       ; preds = %cleanup.cont219
  br label %do.end221

do.end221:                                        ; preds = %do.cond220
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup222

cleanup222:                                       ; preds = %do.end221, %cleanup217, %cleanup204, %cleanup191, %cleanup179, %cleanup166, %cleanup141, %cleanup128, %cleanup103, %cleanup90, %cleanup65, %cleanup52, %cleanup29, %cleanup16, %cleanup, %if.then2, %if.then
  %171 = bitcast i32* %validation_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #3
  %172 = bitcast i8** %type_string to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #3
  %173 = load i32, i32* %retval, align 4
  ret i32 %173
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: optsize
declare i32 @sprintf(i8*, i8*, ...) #2

; Function Attrs: noinline nounwind optsize
define internal i32 @to_base64(i8* %dst, i32 %dst_len, i8* %src, i32 %src_len) #0 {
entry:
  %retval = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_len.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %src_len.addr = alloca i32, align 4
  %olen = alloca i32, align 4
  %buf = alloca i8*, align 4
  %acc = alloca i32, align 4
  %acc_len = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_len, i32* %dst_len.addr, align 4, !tbaa !10
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_len, i32* %src_len.addr, align 4, !tbaa !10
  %0 = bitcast i32* %olen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %acc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %acc_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i32, i32* %src_len.addr, align 4, !tbaa !10
  %div = udiv i32 %4, 3
  %shl = shl i32 %div, 2
  store i32 %shl, i32* %olen, align 4, !tbaa !10
  %5 = load i32, i32* %src_len.addr, align 4, !tbaa !10
  %rem = urem i32 %5, 3
  switch i32 %rem, label %sw.epilog [
    i32 2, label %sw.bb
    i32 1, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %6 = load i32, i32* %olen, align 4, !tbaa !10
  %inc = add i32 %6, 1
  store i32 %inc, i32* %olen, align 4, !tbaa !10
  br label %sw.bb1

sw.bb1:                                           ; preds = %entry, %sw.bb
  %7 = load i32, i32* %olen, align 4, !tbaa !10
  %add = add i32 %7, 2
  store i32 %add, i32* %olen, align 4, !tbaa !10
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %sw.bb1
  %8 = load i32, i32* %dst_len.addr, align 4, !tbaa !10
  %9 = load i32, i32* %olen, align 4, !tbaa !10
  %cmp = icmp ule i32 %8, %9
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %sw.epilog
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %sw.epilog
  store i32 0, i32* %acc, align 4, !tbaa !27
  store i32 0, i32* %acc_len, align 4, !tbaa !27
  %10 = load i8*, i8** %src.addr, align 4, !tbaa !2
  store i8* %10, i8** %buf, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.end, %if.end
  %11 = load i32, i32* %src_len.addr, align 4, !tbaa !10
  %dec = add i32 %11, -1
  store i32 %dec, i32* %src_len.addr, align 4, !tbaa !10
  %cmp2 = icmp ugt i32 %11, 0
  br i1 %cmp2, label %while.body, label %while.end12

while.body:                                       ; preds = %while.cond
  %12 = load i32, i32* %acc, align 4, !tbaa !27
  %shl3 = shl i32 %12, 8
  %13 = load i8*, i8** %buf, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %13, i32 1
  store i8* %incdec.ptr, i8** %buf, align 4, !tbaa !2
  %14 = load i8, i8* %13, align 1, !tbaa !6
  %conv = zext i8 %14 to i32
  %add4 = add i32 %shl3, %conv
  store i32 %add4, i32* %acc, align 4, !tbaa !27
  %15 = load i32, i32* %acc_len, align 4, !tbaa !27
  %add5 = add i32 %15, 8
  store i32 %add5, i32* %acc_len, align 4, !tbaa !27
  br label %while.cond6

while.cond6:                                      ; preds = %while.body9, %while.body
  %16 = load i32, i32* %acc_len, align 4, !tbaa !27
  %cmp7 = icmp uge i32 %16, 6
  br i1 %cmp7, label %while.body9, label %while.end

while.body9:                                      ; preds = %while.cond6
  %17 = load i32, i32* %acc_len, align 4, !tbaa !27
  %sub = sub i32 %17, 6
  store i32 %sub, i32* %acc_len, align 4, !tbaa !27
  %18 = load i32, i32* %acc, align 4, !tbaa !27
  %19 = load i32, i32* %acc_len, align 4, !tbaa !27
  %shr = lshr i32 %18, %19
  %and = and i32 %shr, 63
  %call = call i32 @b64_byte_to_char(i32 %and) #4
  %conv10 = trunc i32 %call to i8
  %20 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr11 = getelementptr inbounds i8, i8* %20, i32 1
  store i8* %incdec.ptr11, i8** %dst.addr, align 4, !tbaa !2
  store i8 %conv10, i8* %20, align 1, !tbaa !6
  br label %while.cond6

while.end:                                        ; preds = %while.cond6
  br label %while.cond

while.end12:                                      ; preds = %while.cond
  %21 = load i32, i32* %acc_len, align 4, !tbaa !27
  %cmp13 = icmp ugt i32 %21, 0
  br i1 %cmp13, label %if.then15, label %if.end22

if.then15:                                        ; preds = %while.end12
  %22 = load i32, i32* %acc, align 4, !tbaa !27
  %23 = load i32, i32* %acc_len, align 4, !tbaa !27
  %sub16 = sub i32 6, %23
  %shl17 = shl i32 %22, %sub16
  %and18 = and i32 %shl17, 63
  %call19 = call i32 @b64_byte_to_char(i32 %and18) #4
  %conv20 = trunc i32 %call19 to i8
  %24 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr21 = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr21, i8** %dst.addr, align 4, !tbaa !2
  store i8 %conv20, i8* %24, align 1, !tbaa !6
  br label %if.end22

if.end22:                                         ; preds = %if.then15, %while.end12
  %25 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr23 = getelementptr inbounds i8, i8* %25, i32 1
  store i8* %incdec.ptr23, i8** %dst.addr, align 4, !tbaa !2
  store i8 0, i8* %25, align 1, !tbaa !6
  %26 = load i32, i32* %olen, align 4, !tbaa !10
  store i32 %26, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end22, %if.then
  %27 = bitcast i32* %acc_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #3
  %28 = bitcast i32* %acc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  %29 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  %30 = bitcast i32* %olen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @b64len(i32 %len) #0 {
entry:
  %len.addr = alloca i32, align 4
  %olen = alloca i32, align 4
  store i32 %len, i32* %len.addr, align 4, !tbaa !27
  %0 = bitcast i32* %olen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %len.addr, align 4, !tbaa !27
  %div = udiv i32 %1, 3
  %shl = shl i32 %div, 2
  store i32 %shl, i32* %olen, align 4, !tbaa !10
  %2 = load i32, i32* %len.addr, align 4, !tbaa !27
  %rem = urem i32 %2, 3
  switch i32 %rem, label %sw.epilog [
    i32 2, label %sw.bb
    i32 1, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %3 = load i32, i32* %olen, align 4, !tbaa !10
  %inc = add i32 %3, 1
  store i32 %inc, i32* %olen, align 4, !tbaa !10
  br label %sw.bb1

sw.bb1:                                           ; preds = %entry, %sw.bb
  %4 = load i32, i32* %olen, align 4, !tbaa !10
  %add = add i32 %4, 2
  store i32 %add, i32* %olen, align 4, !tbaa !10
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %sw.bb1
  %5 = load i32, i32* %olen, align 4, !tbaa !10
  %6 = bitcast i32* %olen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #3
  ret i32 %5
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @numlen(i32 %num) #0 {
entry:
  %num.addr = alloca i32, align 4
  %len = alloca i32, align 4
  store i32 %num, i32* %num.addr, align 4, !tbaa !27
  %0 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 1, i32* %len, align 4, !tbaa !10
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %num.addr, align 4, !tbaa !27
  %cmp = icmp uge i32 %1, 10
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %len, align 4, !tbaa !10
  %inc = add i32 %2, 1
  store i32 %inc, i32* %len, align 4, !tbaa !10
  %3 = load i32, i32* %num.addr, align 4, !tbaa !27
  %div = udiv i32 %3, 10
  store i32 %div, i32* %num.addr, align 4, !tbaa !27
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32, i32* %len, align 4, !tbaa !10
  %5 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #3
  ret i32 %4
}

; Function Attrs: noinline nounwind optsize
define internal i32 @b64_char_to_byte(i32 %c) #0 {
entry:
  %c.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store i32 %c, i32* %c.addr, align 4, !tbaa !27
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub = sub i32 %1, 65
  %shr = lshr i32 %sub, 8
  %and = and i32 %shr, 255
  %xor = xor i32 %and, 255
  %2 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub1 = sub i32 90, %2
  %shr2 = lshr i32 %sub1, 8
  %and3 = and i32 %shr2, 255
  %xor4 = xor i32 %and3, 255
  %and5 = and i32 %xor, %xor4
  %3 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub6 = sub nsw i32 %3, 65
  %and7 = and i32 %and5, %sub6
  %4 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub8 = sub i32 %4, 97
  %shr9 = lshr i32 %sub8, 8
  %and10 = and i32 %shr9, 255
  %xor11 = xor i32 %and10, 255
  %5 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub12 = sub i32 122, %5
  %shr13 = lshr i32 %sub12, 8
  %and14 = and i32 %shr13, 255
  %xor15 = xor i32 %and14, 255
  %and16 = and i32 %xor11, %xor15
  %6 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub17 = sub nsw i32 %6, 71
  %and18 = and i32 %and16, %sub17
  %or = or i32 %and7, %and18
  %7 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub19 = sub i32 %7, 48
  %shr20 = lshr i32 %sub19, 8
  %and21 = and i32 %shr20, 255
  %xor22 = xor i32 %and21, 255
  %8 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub23 = sub i32 57, %8
  %shr24 = lshr i32 %sub23, 8
  %and25 = and i32 %shr24, 255
  %xor26 = xor i32 %and25, 255
  %and27 = and i32 %xor22, %xor26
  %9 = load i32, i32* %c.addr, align 4, !tbaa !27
  %sub28 = sub nsw i32 %9, -4
  %and29 = and i32 %and27, %sub28
  %or30 = or i32 %or, %and29
  %10 = load i32, i32* %c.addr, align 4, !tbaa !27
  %xor31 = xor i32 %10, 43
  %sub32 = sub i32 0, %xor31
  %shr33 = lshr i32 %sub32, 8
  %and34 = and i32 %shr33, 255
  %xor35 = xor i32 %and34, 255
  %and36 = and i32 %xor35, 62
  %or37 = or i32 %or30, %and36
  %11 = load i32, i32* %c.addr, align 4, !tbaa !27
  %xor38 = xor i32 %11, 47
  %sub39 = sub i32 0, %xor38
  %shr40 = lshr i32 %sub39, 8
  %and41 = and i32 %shr40, 255
  %xor42 = xor i32 %and41, 255
  %and43 = and i32 %xor42, 63
  %or44 = or i32 %or37, %and43
  store i32 %or44, i32* %x, align 4, !tbaa !27
  %12 = load i32, i32* %x, align 4, !tbaa !27
  %13 = load i32, i32* %x, align 4, !tbaa !27
  %xor45 = xor i32 %13, 0
  %sub46 = sub i32 0, %xor45
  %shr47 = lshr i32 %sub46, 8
  %and48 = and i32 %shr47, 255
  %xor49 = xor i32 %and48, 255
  %14 = load i32, i32* %c.addr, align 4, !tbaa !27
  %xor50 = xor i32 %14, 65
  %sub51 = sub i32 0, %xor50
  %shr52 = lshr i32 %sub51, 8
  %and53 = and i32 %shr52, 255
  %xor54 = xor i32 %and53, 255
  %xor55 = xor i32 %xor54, 255
  %and56 = and i32 %xor49, %xor55
  %or57 = or i32 %12, %and56
  %15 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  ret i32 %or57
}

; Function Attrs: noinline nounwind optsize
define internal i32 @b64_byte_to_char(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4, !tbaa !27
  %0 = load i32, i32* %x.addr, align 4, !tbaa !27
  %sub = sub i32 %0, 26
  %shr = lshr i32 %sub, 8
  %and = and i32 %shr, 255
  %1 = load i32, i32* %x.addr, align 4, !tbaa !27
  %add = add i32 %1, 65
  %and1 = and i32 %and, %add
  %2 = load i32, i32* %x.addr, align 4, !tbaa !27
  %sub2 = sub i32 %2, 26
  %shr3 = lshr i32 %sub2, 8
  %and4 = and i32 %shr3, 255
  %xor = xor i32 %and4, 255
  %3 = load i32, i32* %x.addr, align 4, !tbaa !27
  %sub5 = sub i32 %3, 52
  %shr6 = lshr i32 %sub5, 8
  %and7 = and i32 %shr6, 255
  %and8 = and i32 %xor, %and7
  %4 = load i32, i32* %x.addr, align 4, !tbaa !27
  %add9 = add i32 %4, 71
  %and10 = and i32 %and8, %add9
  %or = or i32 %and1, %and10
  %5 = load i32, i32* %x.addr, align 4, !tbaa !27
  %sub11 = sub i32 %5, 52
  %shr12 = lshr i32 %sub11, 8
  %and13 = and i32 %shr12, 255
  %xor14 = xor i32 %and13, 255
  %6 = load i32, i32* %x.addr, align 4, !tbaa !27
  %sub15 = sub i32 %6, 62
  %shr16 = lshr i32 %sub15, 8
  %and17 = and i32 %shr16, 255
  %and18 = and i32 %xor14, %and17
  %7 = load i32, i32* %x.addr, align 4, !tbaa !27
  %add19 = add i32 %7, -4
  %and20 = and i32 %and18, %add19
  %or21 = or i32 %or, %and20
  %8 = load i32, i32* %x.addr, align 4, !tbaa !27
  %xor22 = xor i32 %8, 62
  %sub23 = sub i32 0, %xor22
  %shr24 = lshr i32 %sub23, 8
  %and25 = and i32 %shr24, 255
  %xor26 = xor i32 %and25, 255
  %and27 = and i32 %xor26, 43
  %or28 = or i32 %or21, %and27
  %9 = load i32, i32* %x.addr, align 4, !tbaa !27
  %xor29 = xor i32 %9, 63
  %sub30 = sub i32 0, %xor29
  %shr31 = lshr i32 %sub30, 8
  %and32 = and i32 %shr31, 255
  %xor33 = xor i32 %and32, 255
  %and34 = and i32 %xor33, 47
  %or35 = or i32 %or28, %and34
  ret i32 %or35
}

attributes #0 = { noinline nounwind optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { optsize }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!4, !4, i64 0}
!7 = !{!8, !9, i64 20}
!8 = !{!"Argon2_Context", !3, i64 0, !9, i64 4, !3, i64 8, !9, i64 12, !3, i64 16, !9, i64 20, !3, i64 24, !9, i64 28, !3, i64 32, !9, i64 36, !9, i64 40, !9, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !3, i64 60, !3, i64 64, !9, i64 68}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"long", !4, i64 0}
!12 = !{!8, !9, i64 4}
!13 = !{!8, !9, i64 56}
!14 = !{!8, !9, i64 44}
!15 = !{!8, !9, i64 40}
!16 = !{!8, !9, i64 48}
!17 = !{!8, !9, i64 52}
!18 = !{!8, !3, i64 16}
!19 = !{!8, !3, i64 0}
!20 = !{!8, !3, i64 24}
!21 = !{!8, !9, i64 28}
!22 = !{!8, !3, i64 32}
!23 = !{!8, !9, i64 36}
!24 = !{!8, !3, i64 60}
!25 = !{!8, !3, i64 64}
!26 = !{!8, !9, i64 68}
!27 = !{!9, !9, i64 0}
