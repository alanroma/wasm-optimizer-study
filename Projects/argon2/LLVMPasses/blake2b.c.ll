; ModuleID = '/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/blake2/blake2b.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/blake2/blake2b.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.__blake2b_state = type { [8 x i64], [2 x i64], [2 x i64], [128 x i8], i32, i32, i8 }
%struct.__blake2b_param = type { i8, i8, i8, i8, i32, i64, i8, i8, [14 x i8], [16 x i8], [16 x i8] }

@blake2b_IV = internal constant [8 x i64] [i64 7640891576956012808, i64 -4942790177534073029, i64 4354685564936845355, i64 -6534734903238641935, i64 5840696475078001361, i64 -7276294671716946913, i64 2270897969802886507, i64 6620516959819538809], align 16
@blake2b_sigma = internal constant [12 x [16 x i32]] [[16 x i32] [i32 0, i32 1, i32 2, i32 3, i32 4, i32 5, i32 6, i32 7, i32 8, i32 9, i32 10, i32 11, i32 12, i32 13, i32 14, i32 15], [16 x i32] [i32 14, i32 10, i32 4, i32 8, i32 9, i32 15, i32 13, i32 6, i32 1, i32 12, i32 0, i32 2, i32 11, i32 7, i32 5, i32 3], [16 x i32] [i32 11, i32 8, i32 12, i32 0, i32 5, i32 2, i32 15, i32 13, i32 10, i32 14, i32 3, i32 6, i32 7, i32 1, i32 9, i32 4], [16 x i32] [i32 7, i32 9, i32 3, i32 1, i32 13, i32 12, i32 11, i32 14, i32 2, i32 6, i32 5, i32 10, i32 4, i32 0, i32 15, i32 8], [16 x i32] [i32 9, i32 0, i32 5, i32 7, i32 2, i32 4, i32 10, i32 15, i32 14, i32 1, i32 11, i32 12, i32 6, i32 8, i32 3, i32 13], [16 x i32] [i32 2, i32 12, i32 6, i32 10, i32 0, i32 11, i32 8, i32 3, i32 4, i32 13, i32 7, i32 5, i32 15, i32 14, i32 1, i32 9], [16 x i32] [i32 12, i32 5, i32 1, i32 15, i32 14, i32 13, i32 4, i32 10, i32 0, i32 7, i32 6, i32 3, i32 9, i32 2, i32 8, i32 11], [16 x i32] [i32 13, i32 11, i32 7, i32 14, i32 12, i32 1, i32 3, i32 9, i32 5, i32 0, i32 15, i32 4, i32 8, i32 6, i32 2, i32 10], [16 x i32] [i32 6, i32 15, i32 14, i32 9, i32 11, i32 3, i32 0, i32 8, i32 12, i32 2, i32 13, i32 7, i32 1, i32 4, i32 10, i32 5], [16 x i32] [i32 10, i32 2, i32 8, i32 4, i32 7, i32 6, i32 1, i32 5, i32 15, i32 11, i32 9, i32 14, i32 3, i32 12, i32 13, i32 0], [16 x i32] [i32 0, i32 1, i32 2, i32 3, i32 4, i32 5, i32 6, i32 7, i32 8, i32 9, i32 10, i32 11, i32 12, i32 13, i32 14, i32 15], [16 x i32] [i32 14, i32 10, i32 4, i32 8, i32 9, i32 15, i32 13, i32 6, i32 1, i32 12, i32 0, i32 2, i32 11, i32 7, i32 5, i32 3]], align 16

; Function Attrs: noinline nounwind optsize
define hidden i32 @blake2b_init_param(%struct.__blake2b_state* %S, %struct.__blake2b_param* %P) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.__blake2b_state*, align 4
  %P.addr = alloca %struct.__blake2b_param*, align 4
  %p = alloca i8*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  store %struct.__blake2b_param* %P, %struct.__blake2b_param** %P.addr, align 4, !tbaa !2
  %0 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.__blake2b_param*, %struct.__blake2b_param** %P.addr, align 4, !tbaa !2
  %2 = bitcast %struct.__blake2b_param* %1 to i8*
  store i8* %2, i8** %p, align 4, !tbaa !2
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.__blake2b_param*, %struct.__blake2b_param** %P.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.__blake2b_param* null, %4
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %5 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %cmp1 = icmp eq %struct.__blake2b_state* null, %5
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %6 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_init0(%struct.__blake2b_state* %6) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %cmp2 = icmp ult i32 %7, 8
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load i8*, i8** %p, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul i32 %9, 8
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %mul
  %call = call i64 @load64(i8* %arrayidx) #5
  %10 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %10, i32 0, i32 0
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 %11
  %12 = load i64, i64* %arrayidx3, align 8, !tbaa !8
  %xor = xor i64 %12, %call
  store i64 %xor, i64* %arrayidx3, align 8, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load %struct.__blake2b_param*, %struct.__blake2b_param** %P.addr, align 4, !tbaa !2
  %digest_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %14, i32 0, i32 0
  %15 = load i8, i8* %digest_length, align 1, !tbaa !10
  %conv = zext i8 %15 to i32
  %16 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %outlen = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %16, i32 0, i32 5
  store i32 %conv, i32* %outlen, align 4, !tbaa !12
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %18 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: noinline nounwind optsize
define internal void @blake2b_init0(%struct.__blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.__blake2b_state*, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %0 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %1 = bitcast %struct.__blake2b_state* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 0, i32 240, i1 false)
  %2 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %2, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 0
  %3 = bitcast i64* %arraydecay to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %3, i8* align 16 bitcast ([8 x i64]* @blake2b_IV to i8*), i32 64, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optsize
define internal i64 @load64(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  %0 = bitcast i64* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast i64* %w to i8*
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %1, i8* align 1 %2, i32 8, i1 false)
  %3 = load i64, i64* %w, align 8, !tbaa !8
  %4 = bitcast i64* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %4) #4
  ret i64 %3
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: noinline nounwind optsize
define hidden i32 @blake2b_init(%struct.__blake2b_state* %S, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.__blake2b_state*, align 4
  %outlen.addr = alloca i32, align 4
  %P = alloca %struct.__blake2b_param, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  store i32 %outlen, i32* %outlen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.__blake2b_param* %P to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #4
  %1 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.__blake2b_state* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %3 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp2 = icmp ugt i32 %3, 64
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  %4 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_invalidate_state(%struct.__blake2b_state* %4) #5
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %lor.lhs.false
  %5 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %conv = trunc i32 %5 to i8
  %digest_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 1, !tbaa !10
  %key_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 1
  store i8 0, i8* %key_length, align 1, !tbaa !16
  %fanout = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 2
  store i8 1, i8* %fanout, align 1, !tbaa !17
  %depth = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 3
  store i8 1, i8* %depth, align 1, !tbaa !18
  %leaf_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 4
  store i32 0, i32* %leaf_length, align 1, !tbaa !19
  %node_offset = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 5
  store i64 0, i64* %node_offset, align 1, !tbaa !20
  %node_depth = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 6
  store i8 0, i8* %node_depth, align 1, !tbaa !21
  %inner_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 7
  store i8 0, i8* %inner_length, align 1, !tbaa !22
  %reserved = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 8
  %arraydecay = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay, i8 0, i32 14, i1 false)
  %salt = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 9
  %arraydecay5 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay5, i8 0, i32 16, i1 false)
  %personal = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 10
  %arraydecay6 = getelementptr inbounds [16 x i8], [16 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay6, i8 0, i32 16, i1 false)
  %6 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %call = call i32 @blake2b_init_param(%struct.__blake2b_state* %6, %struct.__blake2b_param* %P) #5
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end4, %if.then3, %if.then
  %7 = bitcast %struct.__blake2b_param* %P to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %7) #4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optsize
define internal void @blake2b_invalidate_state(%struct.__blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.__blake2b_state*, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %0 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %1 = bitcast %struct.__blake2b_state* %0 to i8*
  call void @clear_internal_memory(i8* %1, i32 240) #5
  %2 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_set_lastblock(%struct.__blake2b_state* %2) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optsize
define hidden i32 @blake2b_init_key(%struct.__blake2b_state* %S, i32 %outlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.__blake2b_state*, align 4
  %outlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %P = alloca %struct.__blake2b_param, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %block = alloca [128 x i8], align 16
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  store i32 %outlen, i32* %outlen.addr, align 4, !tbaa !14
  store i8* %key, i8** %key.addr, align 4, !tbaa !2
  store i32 %keylen, i32* %keylen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.__blake2b_param* %P to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #4
  %1 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.__blake2b_state* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %3 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp2 = icmp ugt i32 %3, 64
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  %4 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_invalidate_state(%struct.__blake2b_state* %4) #5
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %lor.lhs.false
  %5 = load i8*, i8** %key.addr, align 4, !tbaa !2
  %cmp5 = icmp eq i8* %5, null
  br i1 %cmp5, label %if.then10, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %if.end4
  %6 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  %cmp7 = icmp eq i32 %6, 0
  br i1 %cmp7, label %if.then10, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %lor.lhs.false6
  %7 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  %cmp9 = icmp ugt i32 %7, 64
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %lor.lhs.false8, %lor.lhs.false6, %if.end4
  %8 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_invalidate_state(%struct.__blake2b_state* %8) #5
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %lor.lhs.false8
  %9 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %conv = trunc i32 %9 to i8
  %digest_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 0
  store i8 %conv, i8* %digest_length, align 1, !tbaa !10
  %10 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  %conv12 = trunc i32 %10 to i8
  %key_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 1
  store i8 %conv12, i8* %key_length, align 1, !tbaa !16
  %fanout = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 2
  store i8 1, i8* %fanout, align 1, !tbaa !17
  %depth = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 3
  store i8 1, i8* %depth, align 1, !tbaa !18
  %leaf_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 4
  store i32 0, i32* %leaf_length, align 1, !tbaa !19
  %node_offset = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 5
  store i64 0, i64* %node_offset, align 1, !tbaa !20
  %node_depth = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 6
  store i8 0, i8* %node_depth, align 1, !tbaa !21
  %inner_length = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 7
  store i8 0, i8* %inner_length, align 1, !tbaa !22
  %reserved = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 8
  %arraydecay = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay, i8 0, i32 14, i1 false)
  %salt = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 9
  %arraydecay13 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay13, i8 0, i32 16, i1 false)
  %personal = getelementptr inbounds %struct.__blake2b_param, %struct.__blake2b_param* %P, i32 0, i32 10
  %arraydecay14 = getelementptr inbounds [16 x i8], [16 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay14, i8 0, i32 16, i1 false)
  %11 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %call = call i32 @blake2b_init_param(%struct.__blake2b_state* %11, %struct.__blake2b_param* %P) #5
  %cmp15 = icmp slt i32 %call, 0
  br i1 %cmp15, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end11
  %12 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_invalidate_state(%struct.__blake2b_state* %12) #5
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %if.end11
  %13 = bitcast [128 x i8]* %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %13) #4
  %arraydecay19 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay19, i8 0, i32 128, i1 false)
  %arraydecay20 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %14 = load i8*, i8** %key.addr, align 4, !tbaa !2
  %15 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay20, i8* align 1 %14, i32 %15, i1 false)
  %16 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %arraydecay21 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %call22 = call i32 @blake2b_update(%struct.__blake2b_state* %16, i8* %arraydecay21, i32 128) #5
  %arraydecay23 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @clear_internal_memory(i8* %arraydecay23, i32 128) #5
  %17 = bitcast [128 x i8]* %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %17) #4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end18, %if.then17, %if.then10, %if.then3, %if.then
  %18 = bitcast %struct.__blake2b_param* %P to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %18) #4
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optsize
define hidden i32 @blake2b_update(%struct.__blake2b_state* %S, i8* %in, i32 %inlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.__blake2b_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %pin = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %left = alloca i32, align 4
  %fill = alloca i32, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  store i8* %in, i8** %in.addr, align 4, !tbaa !2
  store i32 %inlen, i32* %inlen.addr, align 4, !tbaa !14
  %0 = bitcast i8** %pin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %in.addr, align 4, !tbaa !2
  store i8* %1, i8** %pin, align 4, !tbaa !2
  %2 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %cmp1 = icmp eq %struct.__blake2b_state* %3, null
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %4 = load i8*, i8** %in.addr, align 4, !tbaa !2
  %cmp2 = icmp eq i8* %4, null
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %lor.lhs.false
  %5 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %f = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %5, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 0
  %6 = load i64, i64* %arrayidx, align 8, !tbaa !8
  %cmp5 = icmp ne i64 %6, 0
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end4
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end4
  %7 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %7, i32 0, i32 4
  %8 = load i32, i32* %buflen, align 8, !tbaa !23
  %9 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %add = add i32 %8, %9
  %cmp8 = icmp ugt i32 %add, 128
  br i1 %cmp8, label %if.then9, label %if.end18

if.then9:                                         ; preds = %if.end7
  %10 = bitcast i32* %left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen10 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %11, i32 0, i32 4
  %12 = load i32, i32* %buflen10, align 8, !tbaa !23
  store i32 %12, i32* %left, align 4, !tbaa !14
  %13 = bitcast i32* %fill to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i32, i32* %left, align 4, !tbaa !14
  %sub = sub i32 128, %14
  store i32 %sub, i32* %fill, align 4, !tbaa !14
  %15 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %15, i32 0, i32 3
  %16 = load i32, i32* %left, align 4, !tbaa !14
  %arrayidx11 = getelementptr inbounds [128 x i8], [128 x i8]* %buf, i32 0, i32 %16
  %17 = load i8*, i8** %pin, align 4, !tbaa !2
  %18 = load i32, i32* %fill, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx11, i8* align 1 %17, i32 %18, i1 false)
  %19 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_increment_counter(%struct.__blake2b_state* %19, i64 128) #5
  %20 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %21 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buf12 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %21, i32 0, i32 3
  %arraydecay = getelementptr inbounds [128 x i8], [128 x i8]* %buf12, i32 0, i32 0
  call void @blake2b_compress(%struct.__blake2b_state* %20, i8* %arraydecay) #5
  %22 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen13 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %22, i32 0, i32 4
  store i32 0, i32* %buflen13, align 8, !tbaa !23
  %23 = load i32, i32* %fill, align 4, !tbaa !14
  %24 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %sub14 = sub i32 %24, %23
  store i32 %sub14, i32* %inlen.addr, align 4, !tbaa !14
  %25 = load i32, i32* %fill, align 4, !tbaa !14
  %26 = load i8*, i8** %pin, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %26, i32 %25
  store i8* %add.ptr, i8** %pin, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then9
  %27 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %cmp15 = icmp ugt i32 %27, 128
  br i1 %cmp15, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %28 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_increment_counter(%struct.__blake2b_state* %28, i64 128) #5
  %29 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %30 = load i8*, i8** %pin, align 4, !tbaa !2
  call void @blake2b_compress(%struct.__blake2b_state* %29, i8* %30) #5
  %31 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %sub16 = sub i32 %31, 128
  store i32 %sub16, i32* %inlen.addr, align 4, !tbaa !14
  %32 = load i8*, i8** %pin, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i8, i8* %32, i32 128
  store i8* %add.ptr17, i8** %pin, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %33 = bitcast i32* %fill to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  br label %if.end18

if.end18:                                         ; preds = %while.end, %if.end7
  %35 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buf19 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %35, i32 0, i32 3
  %36 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen20 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %36, i32 0, i32 4
  %37 = load i32, i32* %buflen20, align 8, !tbaa !23
  %arrayidx21 = getelementptr inbounds [128 x i8], [128 x i8]* %buf19, i32 0, i32 %37
  %38 = load i8*, i8** %pin, align 4, !tbaa !2
  %39 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx21, i8* align 1 %38, i32 %39, i1 false)
  %40 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %41 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen22 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %41, i32 0, i32 4
  %42 = load i32, i32* %buflen22, align 8, !tbaa !23
  %add23 = add i32 %42, %40
  store i32 %add23, i32* %buflen22, align 8, !tbaa !23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end18, %if.then6, %if.then3, %if.then
  %43 = bitcast i8** %pin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = load i32, i32* %retval, align 4
  ret i32 %44
}

; Function Attrs: optsize
declare void @clear_internal_memory(i8*, i32) #3

; Function Attrs: noinline nounwind optsize
define internal void @blake2b_increment_counter(%struct.__blake2b_state* %S, i64 %inc) #0 {
entry:
  %S.addr = alloca %struct.__blake2b_state*, align 4
  %inc.addr = alloca i64, align 8
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  store i64 %inc, i64* %inc.addr, align 8, !tbaa !8
  %0 = load i64, i64* %inc.addr, align 8, !tbaa !8
  %1 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %t = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %t, i32 0, i32 0
  %2 = load i64, i64* %arrayidx, align 8, !tbaa !8
  %add = add i64 %2, %0
  store i64 %add, i64* %arrayidx, align 8, !tbaa !8
  %3 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %t1 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %3, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [2 x i64], [2 x i64]* %t1, i32 0, i32 0
  %4 = load i64, i64* %arrayidx2, align 8, !tbaa !8
  %5 = load i64, i64* %inc.addr, align 8, !tbaa !8
  %cmp = icmp ult i64 %4, %5
  %conv = zext i1 %cmp to i32
  %conv3 = sext i32 %conv to i64
  %6 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %t4 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %6, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [2 x i64], [2 x i64]* %t4, i32 0, i32 1
  %7 = load i64, i64* %arrayidx5, align 8, !tbaa !8
  %add6 = add i64 %7, %conv3
  store i64 %add6, i64* %arrayidx5, align 8, !tbaa !8
  ret void
}

; Function Attrs: noinline nounwind optsize
define internal void @blake2b_compress(%struct.__blake2b_state* %S, i8* %block) #0 {
entry:
  %S.addr = alloca %struct.__blake2b_state*, align 4
  %block.addr = alloca i8*, align 4
  %m = alloca [16 x i64], align 16
  %v = alloca [16 x i64], align 16
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  store i8* %block, i8** %block.addr, align 4, !tbaa !2
  %0 = bitcast [16 x i64]* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %0) #4
  %1 = bitcast [16 x i64]* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %1) #4
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp ult i32 %4, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i8*, i8** %block.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul i32 %6, 8
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  %call = call i64 @load64(i8* %add.ptr) #5
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %7
  store i64 %call, i64* %arrayidx, align 8, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc6, %for.end
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %cmp2 = icmp ult i32 %9, 8
  br i1 %cmp2, label %for.body3, label %for.end8

for.body3:                                        ; preds = %for.cond1
  %10 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %10, i32 0, i32 0
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 %11
  %12 = load i64, i64* %arrayidx4, align 8, !tbaa !8
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 %13
  store i64 %12, i64* %arrayidx5, align 8, !tbaa !8
  br label %for.inc6

for.inc6:                                         ; preds = %for.body3
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %inc7 = add i32 %14, 1
  store i32 %inc7, i32* %i, align 4, !tbaa !6
  br label %for.cond1

for.end8:                                         ; preds = %for.cond1
  %15 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 0), align 16, !tbaa !8
  %arrayidx9 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %15, i64* %arrayidx9, align 16, !tbaa !8
  %16 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 1), align 8, !tbaa !8
  %arrayidx10 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %16, i64* %arrayidx10, align 8, !tbaa !8
  %17 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 2), align 16, !tbaa !8
  %arrayidx11 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %17, i64* %arrayidx11, align 16, !tbaa !8
  %18 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 3), align 8, !tbaa !8
  %arrayidx12 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %18, i64* %arrayidx12, align 8, !tbaa !8
  %19 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 4), align 16, !tbaa !8
  %20 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %t = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %20, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x i64], [2 x i64]* %t, i32 0, i32 0
  %21 = load i64, i64* %arrayidx13, align 8, !tbaa !8
  %xor = xor i64 %19, %21
  %arrayidx14 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %xor, i64* %arrayidx14, align 16, !tbaa !8
  %22 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 5), align 8, !tbaa !8
  %23 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %t15 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %23, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [2 x i64], [2 x i64]* %t15, i32 0, i32 1
  %24 = load i64, i64* %arrayidx16, align 8, !tbaa !8
  %xor17 = xor i64 %22, %24
  %arrayidx18 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %xor17, i64* %arrayidx18, align 8, !tbaa !8
  %25 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 6), align 16, !tbaa !8
  %26 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %f = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %26, i32 0, i32 2
  %arrayidx19 = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 0
  %27 = load i64, i64* %arrayidx19, align 8, !tbaa !8
  %xor20 = xor i64 %25, %27
  %arrayidx21 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %xor20, i64* %arrayidx21, align 16, !tbaa !8
  %28 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 7), align 8, !tbaa !8
  %29 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %f22 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %29, i32 0, i32 2
  %arrayidx23 = getelementptr inbounds [2 x i64], [2 x i64]* %f22, i32 0, i32 1
  %30 = load i64, i64* %arrayidx23, align 8, !tbaa !8
  %xor24 = xor i64 %28, %30
  %arrayidx25 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %xor24, i64* %arrayidx25, align 8, !tbaa !8
  store i32 0, i32* %r, align 4, !tbaa !6
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc404, %for.end8
  %31 = load i32, i32* %r, align 4, !tbaa !6
  %cmp27 = icmp ult i32 %31, 12
  br i1 %cmp27, label %for.body28, label %for.end406

for.body28:                                       ; preds = %for.cond26
  br label %do.body

do.body:                                          ; preds = %for.body28
  br label %do.body29

do.body29:                                        ; preds = %do.body
  %arrayidx30 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %32 = load i64, i64* %arrayidx30, align 16, !tbaa !8
  %arrayidx31 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %33 = load i64, i64* %arrayidx31, align 16, !tbaa !8
  %add = add i64 %32, %33
  %34 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %34
  %arrayidx33 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx32, i32 0, i32 0
  %35 = load i32, i32* %arrayidx33, align 16, !tbaa !6
  %arrayidx34 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %35
  %36 = load i64, i64* %arrayidx34, align 8, !tbaa !8
  %add35 = add i64 %add, %36
  %arrayidx36 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add35, i64* %arrayidx36, align 16, !tbaa !8
  %arrayidx37 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %37 = load i64, i64* %arrayidx37, align 16, !tbaa !8
  %arrayidx38 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %38 = load i64, i64* %arrayidx38, align 16, !tbaa !8
  %xor39 = xor i64 %37, %38
  %call40 = call i64 @rotr64(i64 %xor39, i32 32) #5
  %arrayidx41 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call40, i64* %arrayidx41, align 16, !tbaa !8
  %arrayidx42 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %39 = load i64, i64* %arrayidx42, align 16, !tbaa !8
  %arrayidx43 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %40 = load i64, i64* %arrayidx43, align 16, !tbaa !8
  %add44 = add i64 %39, %40
  %arrayidx45 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add44, i64* %arrayidx45, align 16, !tbaa !8
  %arrayidx46 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %41 = load i64, i64* %arrayidx46, align 16, !tbaa !8
  %arrayidx47 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %42 = load i64, i64* %arrayidx47, align 16, !tbaa !8
  %xor48 = xor i64 %41, %42
  %call49 = call i64 @rotr64(i64 %xor48, i32 24) #5
  %arrayidx50 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call49, i64* %arrayidx50, align 16, !tbaa !8
  %arrayidx51 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %43 = load i64, i64* %arrayidx51, align 16, !tbaa !8
  %arrayidx52 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %44 = load i64, i64* %arrayidx52, align 16, !tbaa !8
  %add53 = add i64 %43, %44
  %45 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx54 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %45
  %arrayidx55 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx54, i32 0, i32 1
  %46 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %46
  %47 = load i64, i64* %arrayidx56, align 8, !tbaa !8
  %add57 = add i64 %add53, %47
  %arrayidx58 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add57, i64* %arrayidx58, align 16, !tbaa !8
  %arrayidx59 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %48 = load i64, i64* %arrayidx59, align 16, !tbaa !8
  %arrayidx60 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %49 = load i64, i64* %arrayidx60, align 16, !tbaa !8
  %xor61 = xor i64 %48, %49
  %call62 = call i64 @rotr64(i64 %xor61, i32 16) #5
  %arrayidx63 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call62, i64* %arrayidx63, align 16, !tbaa !8
  %arrayidx64 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %50 = load i64, i64* %arrayidx64, align 16, !tbaa !8
  %arrayidx65 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %51 = load i64, i64* %arrayidx65, align 16, !tbaa !8
  %add66 = add i64 %50, %51
  %arrayidx67 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add66, i64* %arrayidx67, align 16, !tbaa !8
  %arrayidx68 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %52 = load i64, i64* %arrayidx68, align 16, !tbaa !8
  %arrayidx69 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %53 = load i64, i64* %arrayidx69, align 16, !tbaa !8
  %xor70 = xor i64 %52, %53
  %call71 = call i64 @rotr64(i64 %xor70, i32 63) #5
  %arrayidx72 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call71, i64* %arrayidx72, align 16, !tbaa !8
  br label %do.cond

do.cond:                                          ; preds = %do.body29
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body73

do.body73:                                        ; preds = %do.end
  %arrayidx74 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %54 = load i64, i64* %arrayidx74, align 8, !tbaa !8
  %arrayidx75 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %55 = load i64, i64* %arrayidx75, align 8, !tbaa !8
  %add76 = add i64 %54, %55
  %56 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx77 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %56
  %arrayidx78 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx77, i32 0, i32 2
  %57 = load i32, i32* %arrayidx78, align 8, !tbaa !6
  %arrayidx79 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %57
  %58 = load i64, i64* %arrayidx79, align 8, !tbaa !8
  %add80 = add i64 %add76, %58
  %arrayidx81 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add80, i64* %arrayidx81, align 8, !tbaa !8
  %arrayidx82 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %59 = load i64, i64* %arrayidx82, align 8, !tbaa !8
  %arrayidx83 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %60 = load i64, i64* %arrayidx83, align 8, !tbaa !8
  %xor84 = xor i64 %59, %60
  %call85 = call i64 @rotr64(i64 %xor84, i32 32) #5
  %arrayidx86 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call85, i64* %arrayidx86, align 8, !tbaa !8
  %arrayidx87 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %61 = load i64, i64* %arrayidx87, align 8, !tbaa !8
  %arrayidx88 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %62 = load i64, i64* %arrayidx88, align 8, !tbaa !8
  %add89 = add i64 %61, %62
  %arrayidx90 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add89, i64* %arrayidx90, align 8, !tbaa !8
  %arrayidx91 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %63 = load i64, i64* %arrayidx91, align 8, !tbaa !8
  %arrayidx92 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %64 = load i64, i64* %arrayidx92, align 8, !tbaa !8
  %xor93 = xor i64 %63, %64
  %call94 = call i64 @rotr64(i64 %xor93, i32 24) #5
  %arrayidx95 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call94, i64* %arrayidx95, align 8, !tbaa !8
  %arrayidx96 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %65 = load i64, i64* %arrayidx96, align 8, !tbaa !8
  %arrayidx97 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %66 = load i64, i64* %arrayidx97, align 8, !tbaa !8
  %add98 = add i64 %65, %66
  %67 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx99 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %67
  %arrayidx100 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx99, i32 0, i32 3
  %68 = load i32, i32* %arrayidx100, align 4, !tbaa !6
  %arrayidx101 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %68
  %69 = load i64, i64* %arrayidx101, align 8, !tbaa !8
  %add102 = add i64 %add98, %69
  %arrayidx103 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add102, i64* %arrayidx103, align 8, !tbaa !8
  %arrayidx104 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %70 = load i64, i64* %arrayidx104, align 8, !tbaa !8
  %arrayidx105 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %71 = load i64, i64* %arrayidx105, align 8, !tbaa !8
  %xor106 = xor i64 %70, %71
  %call107 = call i64 @rotr64(i64 %xor106, i32 16) #5
  %arrayidx108 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call107, i64* %arrayidx108, align 8, !tbaa !8
  %arrayidx109 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %72 = load i64, i64* %arrayidx109, align 8, !tbaa !8
  %arrayidx110 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %73 = load i64, i64* %arrayidx110, align 8, !tbaa !8
  %add111 = add i64 %72, %73
  %arrayidx112 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add111, i64* %arrayidx112, align 8, !tbaa !8
  %arrayidx113 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %74 = load i64, i64* %arrayidx113, align 8, !tbaa !8
  %arrayidx114 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %75 = load i64, i64* %arrayidx114, align 8, !tbaa !8
  %xor115 = xor i64 %74, %75
  %call116 = call i64 @rotr64(i64 %xor115, i32 63) #5
  %arrayidx117 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call116, i64* %arrayidx117, align 8, !tbaa !8
  br label %do.cond118

do.cond118:                                       ; preds = %do.body73
  br label %do.end119

do.end119:                                        ; preds = %do.cond118
  br label %do.body120

do.body120:                                       ; preds = %do.end119
  %arrayidx121 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %76 = load i64, i64* %arrayidx121, align 16, !tbaa !8
  %arrayidx122 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %77 = load i64, i64* %arrayidx122, align 16, !tbaa !8
  %add123 = add i64 %76, %77
  %78 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx124 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %78
  %arrayidx125 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx124, i32 0, i32 4
  %79 = load i32, i32* %arrayidx125, align 16, !tbaa !6
  %arrayidx126 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %79
  %80 = load i64, i64* %arrayidx126, align 8, !tbaa !8
  %add127 = add i64 %add123, %80
  %arrayidx128 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add127, i64* %arrayidx128, align 16, !tbaa !8
  %arrayidx129 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %81 = load i64, i64* %arrayidx129, align 16, !tbaa !8
  %arrayidx130 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %82 = load i64, i64* %arrayidx130, align 16, !tbaa !8
  %xor131 = xor i64 %81, %82
  %call132 = call i64 @rotr64(i64 %xor131, i32 32) #5
  %arrayidx133 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call132, i64* %arrayidx133, align 16, !tbaa !8
  %arrayidx134 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %83 = load i64, i64* %arrayidx134, align 16, !tbaa !8
  %arrayidx135 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %84 = load i64, i64* %arrayidx135, align 16, !tbaa !8
  %add136 = add i64 %83, %84
  %arrayidx137 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add136, i64* %arrayidx137, align 16, !tbaa !8
  %arrayidx138 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %85 = load i64, i64* %arrayidx138, align 16, !tbaa !8
  %arrayidx139 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %86 = load i64, i64* %arrayidx139, align 16, !tbaa !8
  %xor140 = xor i64 %85, %86
  %call141 = call i64 @rotr64(i64 %xor140, i32 24) #5
  %arrayidx142 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call141, i64* %arrayidx142, align 16, !tbaa !8
  %arrayidx143 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %87 = load i64, i64* %arrayidx143, align 16, !tbaa !8
  %arrayidx144 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %88 = load i64, i64* %arrayidx144, align 16, !tbaa !8
  %add145 = add i64 %87, %88
  %89 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx146 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %89
  %arrayidx147 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx146, i32 0, i32 5
  %90 = load i32, i32* %arrayidx147, align 4, !tbaa !6
  %arrayidx148 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %90
  %91 = load i64, i64* %arrayidx148, align 8, !tbaa !8
  %add149 = add i64 %add145, %91
  %arrayidx150 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add149, i64* %arrayidx150, align 16, !tbaa !8
  %arrayidx151 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %92 = load i64, i64* %arrayidx151, align 16, !tbaa !8
  %arrayidx152 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %93 = load i64, i64* %arrayidx152, align 16, !tbaa !8
  %xor153 = xor i64 %92, %93
  %call154 = call i64 @rotr64(i64 %xor153, i32 16) #5
  %arrayidx155 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call154, i64* %arrayidx155, align 16, !tbaa !8
  %arrayidx156 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %94 = load i64, i64* %arrayidx156, align 16, !tbaa !8
  %arrayidx157 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %95 = load i64, i64* %arrayidx157, align 16, !tbaa !8
  %add158 = add i64 %94, %95
  %arrayidx159 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add158, i64* %arrayidx159, align 16, !tbaa !8
  %arrayidx160 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %96 = load i64, i64* %arrayidx160, align 16, !tbaa !8
  %arrayidx161 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %97 = load i64, i64* %arrayidx161, align 16, !tbaa !8
  %xor162 = xor i64 %96, %97
  %call163 = call i64 @rotr64(i64 %xor162, i32 63) #5
  %arrayidx164 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call163, i64* %arrayidx164, align 16, !tbaa !8
  br label %do.cond165

do.cond165:                                       ; preds = %do.body120
  br label %do.end166

do.end166:                                        ; preds = %do.cond165
  br label %do.body167

do.body167:                                       ; preds = %do.end166
  %arrayidx168 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %98 = load i64, i64* %arrayidx168, align 8, !tbaa !8
  %arrayidx169 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %99 = load i64, i64* %arrayidx169, align 8, !tbaa !8
  %add170 = add i64 %98, %99
  %100 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx171 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %100
  %arrayidx172 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx171, i32 0, i32 6
  %101 = load i32, i32* %arrayidx172, align 8, !tbaa !6
  %arrayidx173 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %101
  %102 = load i64, i64* %arrayidx173, align 8, !tbaa !8
  %add174 = add i64 %add170, %102
  %arrayidx175 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add174, i64* %arrayidx175, align 8, !tbaa !8
  %arrayidx176 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %103 = load i64, i64* %arrayidx176, align 8, !tbaa !8
  %arrayidx177 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %104 = load i64, i64* %arrayidx177, align 8, !tbaa !8
  %xor178 = xor i64 %103, %104
  %call179 = call i64 @rotr64(i64 %xor178, i32 32) #5
  %arrayidx180 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call179, i64* %arrayidx180, align 8, !tbaa !8
  %arrayidx181 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %105 = load i64, i64* %arrayidx181, align 8, !tbaa !8
  %arrayidx182 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %106 = load i64, i64* %arrayidx182, align 8, !tbaa !8
  %add183 = add i64 %105, %106
  %arrayidx184 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add183, i64* %arrayidx184, align 8, !tbaa !8
  %arrayidx185 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %107 = load i64, i64* %arrayidx185, align 8, !tbaa !8
  %arrayidx186 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %108 = load i64, i64* %arrayidx186, align 8, !tbaa !8
  %xor187 = xor i64 %107, %108
  %call188 = call i64 @rotr64(i64 %xor187, i32 24) #5
  %arrayidx189 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call188, i64* %arrayidx189, align 8, !tbaa !8
  %arrayidx190 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %109 = load i64, i64* %arrayidx190, align 8, !tbaa !8
  %arrayidx191 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %110 = load i64, i64* %arrayidx191, align 8, !tbaa !8
  %add192 = add i64 %109, %110
  %111 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx193 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %111
  %arrayidx194 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx193, i32 0, i32 7
  %112 = load i32, i32* %arrayidx194, align 4, !tbaa !6
  %arrayidx195 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %112
  %113 = load i64, i64* %arrayidx195, align 8, !tbaa !8
  %add196 = add i64 %add192, %113
  %arrayidx197 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add196, i64* %arrayidx197, align 8, !tbaa !8
  %arrayidx198 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %114 = load i64, i64* %arrayidx198, align 8, !tbaa !8
  %arrayidx199 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %115 = load i64, i64* %arrayidx199, align 8, !tbaa !8
  %xor200 = xor i64 %114, %115
  %call201 = call i64 @rotr64(i64 %xor200, i32 16) #5
  %arrayidx202 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call201, i64* %arrayidx202, align 8, !tbaa !8
  %arrayidx203 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %116 = load i64, i64* %arrayidx203, align 8, !tbaa !8
  %arrayidx204 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %117 = load i64, i64* %arrayidx204, align 8, !tbaa !8
  %add205 = add i64 %116, %117
  %arrayidx206 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add205, i64* %arrayidx206, align 8, !tbaa !8
  %arrayidx207 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %118 = load i64, i64* %arrayidx207, align 8, !tbaa !8
  %arrayidx208 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %119 = load i64, i64* %arrayidx208, align 8, !tbaa !8
  %xor209 = xor i64 %118, %119
  %call210 = call i64 @rotr64(i64 %xor209, i32 63) #5
  %arrayidx211 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call210, i64* %arrayidx211, align 8, !tbaa !8
  br label %do.cond212

do.cond212:                                       ; preds = %do.body167
  br label %do.end213

do.end213:                                        ; preds = %do.cond212
  br label %do.body214

do.body214:                                       ; preds = %do.end213
  %arrayidx215 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %120 = load i64, i64* %arrayidx215, align 16, !tbaa !8
  %arrayidx216 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %121 = load i64, i64* %arrayidx216, align 8, !tbaa !8
  %add217 = add i64 %120, %121
  %122 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx218 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %122
  %arrayidx219 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx218, i32 0, i32 8
  %123 = load i32, i32* %arrayidx219, align 16, !tbaa !6
  %arrayidx220 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %123
  %124 = load i64, i64* %arrayidx220, align 8, !tbaa !8
  %add221 = add i64 %add217, %124
  %arrayidx222 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add221, i64* %arrayidx222, align 16, !tbaa !8
  %arrayidx223 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %125 = load i64, i64* %arrayidx223, align 8, !tbaa !8
  %arrayidx224 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %126 = load i64, i64* %arrayidx224, align 16, !tbaa !8
  %xor225 = xor i64 %125, %126
  %call226 = call i64 @rotr64(i64 %xor225, i32 32) #5
  %arrayidx227 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call226, i64* %arrayidx227, align 8, !tbaa !8
  %arrayidx228 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %127 = load i64, i64* %arrayidx228, align 16, !tbaa !8
  %arrayidx229 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %128 = load i64, i64* %arrayidx229, align 8, !tbaa !8
  %add230 = add i64 %127, %128
  %arrayidx231 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add230, i64* %arrayidx231, align 16, !tbaa !8
  %arrayidx232 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %129 = load i64, i64* %arrayidx232, align 8, !tbaa !8
  %arrayidx233 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %130 = load i64, i64* %arrayidx233, align 16, !tbaa !8
  %xor234 = xor i64 %129, %130
  %call235 = call i64 @rotr64(i64 %xor234, i32 24) #5
  %arrayidx236 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call235, i64* %arrayidx236, align 8, !tbaa !8
  %arrayidx237 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %131 = load i64, i64* %arrayidx237, align 16, !tbaa !8
  %arrayidx238 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %132 = load i64, i64* %arrayidx238, align 8, !tbaa !8
  %add239 = add i64 %131, %132
  %133 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx240 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %133
  %arrayidx241 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx240, i32 0, i32 9
  %134 = load i32, i32* %arrayidx241, align 4, !tbaa !6
  %arrayidx242 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %134
  %135 = load i64, i64* %arrayidx242, align 8, !tbaa !8
  %add243 = add i64 %add239, %135
  %arrayidx244 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  store i64 %add243, i64* %arrayidx244, align 16, !tbaa !8
  %arrayidx245 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %136 = load i64, i64* %arrayidx245, align 8, !tbaa !8
  %arrayidx246 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %137 = load i64, i64* %arrayidx246, align 16, !tbaa !8
  %xor247 = xor i64 %136, %137
  %call248 = call i64 @rotr64(i64 %xor247, i32 16) #5
  %arrayidx249 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call248, i64* %arrayidx249, align 8, !tbaa !8
  %arrayidx250 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %138 = load i64, i64* %arrayidx250, align 16, !tbaa !8
  %arrayidx251 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %139 = load i64, i64* %arrayidx251, align 8, !tbaa !8
  %add252 = add i64 %138, %139
  %arrayidx253 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %add252, i64* %arrayidx253, align 16, !tbaa !8
  %arrayidx254 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %140 = load i64, i64* %arrayidx254, align 8, !tbaa !8
  %arrayidx255 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %141 = load i64, i64* %arrayidx255, align 16, !tbaa !8
  %xor256 = xor i64 %140, %141
  %call257 = call i64 @rotr64(i64 %xor256, i32 63) #5
  %arrayidx258 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call257, i64* %arrayidx258, align 8, !tbaa !8
  br label %do.cond259

do.cond259:                                       ; preds = %do.body214
  br label %do.end260

do.end260:                                        ; preds = %do.cond259
  br label %do.body261

do.body261:                                       ; preds = %do.end260
  %arrayidx262 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %142 = load i64, i64* %arrayidx262, align 8, !tbaa !8
  %arrayidx263 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %143 = load i64, i64* %arrayidx263, align 16, !tbaa !8
  %add264 = add i64 %142, %143
  %144 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx265 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %144
  %arrayidx266 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx265, i32 0, i32 10
  %145 = load i32, i32* %arrayidx266, align 8, !tbaa !6
  %arrayidx267 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %145
  %146 = load i64, i64* %arrayidx267, align 8, !tbaa !8
  %add268 = add i64 %add264, %146
  %arrayidx269 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add268, i64* %arrayidx269, align 8, !tbaa !8
  %arrayidx270 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %147 = load i64, i64* %arrayidx270, align 16, !tbaa !8
  %arrayidx271 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %148 = load i64, i64* %arrayidx271, align 8, !tbaa !8
  %xor272 = xor i64 %147, %148
  %call273 = call i64 @rotr64(i64 %xor272, i32 32) #5
  %arrayidx274 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call273, i64* %arrayidx274, align 16, !tbaa !8
  %arrayidx275 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %149 = load i64, i64* %arrayidx275, align 8, !tbaa !8
  %arrayidx276 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %150 = load i64, i64* %arrayidx276, align 16, !tbaa !8
  %add277 = add i64 %149, %150
  %arrayidx278 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add277, i64* %arrayidx278, align 8, !tbaa !8
  %arrayidx279 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %151 = load i64, i64* %arrayidx279, align 16, !tbaa !8
  %arrayidx280 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %152 = load i64, i64* %arrayidx280, align 8, !tbaa !8
  %xor281 = xor i64 %151, %152
  %call282 = call i64 @rotr64(i64 %xor281, i32 24) #5
  %arrayidx283 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call282, i64* %arrayidx283, align 16, !tbaa !8
  %arrayidx284 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %153 = load i64, i64* %arrayidx284, align 8, !tbaa !8
  %arrayidx285 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %154 = load i64, i64* %arrayidx285, align 16, !tbaa !8
  %add286 = add i64 %153, %154
  %155 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx287 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %155
  %arrayidx288 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx287, i32 0, i32 11
  %156 = load i32, i32* %arrayidx288, align 4, !tbaa !6
  %arrayidx289 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %156
  %157 = load i64, i64* %arrayidx289, align 8, !tbaa !8
  %add290 = add i64 %add286, %157
  %arrayidx291 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  store i64 %add290, i64* %arrayidx291, align 8, !tbaa !8
  %arrayidx292 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %158 = load i64, i64* %arrayidx292, align 16, !tbaa !8
  %arrayidx293 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %159 = load i64, i64* %arrayidx293, align 8, !tbaa !8
  %xor294 = xor i64 %158, %159
  %call295 = call i64 @rotr64(i64 %xor294, i32 16) #5
  %arrayidx296 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call295, i64* %arrayidx296, align 16, !tbaa !8
  %arrayidx297 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %160 = load i64, i64* %arrayidx297, align 8, !tbaa !8
  %arrayidx298 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %161 = load i64, i64* %arrayidx298, align 16, !tbaa !8
  %add299 = add i64 %160, %161
  %arrayidx300 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %add299, i64* %arrayidx300, align 8, !tbaa !8
  %arrayidx301 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %162 = load i64, i64* %arrayidx301, align 16, !tbaa !8
  %arrayidx302 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %163 = load i64, i64* %arrayidx302, align 8, !tbaa !8
  %xor303 = xor i64 %162, %163
  %call304 = call i64 @rotr64(i64 %xor303, i32 63) #5
  %arrayidx305 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call304, i64* %arrayidx305, align 16, !tbaa !8
  br label %do.cond306

do.cond306:                                       ; preds = %do.body261
  br label %do.end307

do.end307:                                        ; preds = %do.cond306
  br label %do.body308

do.body308:                                       ; preds = %do.end307
  %arrayidx309 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %164 = load i64, i64* %arrayidx309, align 16, !tbaa !8
  %arrayidx310 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %165 = load i64, i64* %arrayidx310, align 8, !tbaa !8
  %add311 = add i64 %164, %165
  %166 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx312 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %166
  %arrayidx313 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx312, i32 0, i32 12
  %167 = load i32, i32* %arrayidx313, align 16, !tbaa !6
  %arrayidx314 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %167
  %168 = load i64, i64* %arrayidx314, align 8, !tbaa !8
  %add315 = add i64 %add311, %168
  %arrayidx316 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add315, i64* %arrayidx316, align 16, !tbaa !8
  %arrayidx317 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %169 = load i64, i64* %arrayidx317, align 8, !tbaa !8
  %arrayidx318 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %170 = load i64, i64* %arrayidx318, align 16, !tbaa !8
  %xor319 = xor i64 %169, %170
  %call320 = call i64 @rotr64(i64 %xor319, i32 32) #5
  %arrayidx321 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call320, i64* %arrayidx321, align 8, !tbaa !8
  %arrayidx322 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %171 = load i64, i64* %arrayidx322, align 16, !tbaa !8
  %arrayidx323 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %172 = load i64, i64* %arrayidx323, align 8, !tbaa !8
  %add324 = add i64 %171, %172
  %arrayidx325 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add324, i64* %arrayidx325, align 16, !tbaa !8
  %arrayidx326 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %173 = load i64, i64* %arrayidx326, align 8, !tbaa !8
  %arrayidx327 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %174 = load i64, i64* %arrayidx327, align 16, !tbaa !8
  %xor328 = xor i64 %173, %174
  %call329 = call i64 @rotr64(i64 %xor328, i32 24) #5
  %arrayidx330 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call329, i64* %arrayidx330, align 8, !tbaa !8
  %arrayidx331 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %175 = load i64, i64* %arrayidx331, align 16, !tbaa !8
  %arrayidx332 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %176 = load i64, i64* %arrayidx332, align 8, !tbaa !8
  %add333 = add i64 %175, %176
  %177 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx334 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %177
  %arrayidx335 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx334, i32 0, i32 13
  %178 = load i32, i32* %arrayidx335, align 4, !tbaa !6
  %arrayidx336 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %178
  %179 = load i64, i64* %arrayidx336, align 8, !tbaa !8
  %add337 = add i64 %add333, %179
  %arrayidx338 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  store i64 %add337, i64* %arrayidx338, align 16, !tbaa !8
  %arrayidx339 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %180 = load i64, i64* %arrayidx339, align 8, !tbaa !8
  %arrayidx340 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %181 = load i64, i64* %arrayidx340, align 16, !tbaa !8
  %xor341 = xor i64 %180, %181
  %call342 = call i64 @rotr64(i64 %xor341, i32 16) #5
  %arrayidx343 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call342, i64* %arrayidx343, align 8, !tbaa !8
  %arrayidx344 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %182 = load i64, i64* %arrayidx344, align 16, !tbaa !8
  %arrayidx345 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %183 = load i64, i64* %arrayidx345, align 8, !tbaa !8
  %add346 = add i64 %182, %183
  %arrayidx347 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %add346, i64* %arrayidx347, align 16, !tbaa !8
  %arrayidx348 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %184 = load i64, i64* %arrayidx348, align 8, !tbaa !8
  %arrayidx349 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %185 = load i64, i64* %arrayidx349, align 16, !tbaa !8
  %xor350 = xor i64 %184, %185
  %call351 = call i64 @rotr64(i64 %xor350, i32 63) #5
  %arrayidx352 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call351, i64* %arrayidx352, align 8, !tbaa !8
  br label %do.cond353

do.cond353:                                       ; preds = %do.body308
  br label %do.end354

do.end354:                                        ; preds = %do.cond353
  br label %do.body355

do.body355:                                       ; preds = %do.end354
  %arrayidx356 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %186 = load i64, i64* %arrayidx356, align 8, !tbaa !8
  %arrayidx357 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %187 = load i64, i64* %arrayidx357, align 16, !tbaa !8
  %add358 = add i64 %186, %187
  %188 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx359 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %188
  %arrayidx360 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx359, i32 0, i32 14
  %189 = load i32, i32* %arrayidx360, align 8, !tbaa !6
  %arrayidx361 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %189
  %190 = load i64, i64* %arrayidx361, align 8, !tbaa !8
  %add362 = add i64 %add358, %190
  %arrayidx363 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add362, i64* %arrayidx363, align 8, !tbaa !8
  %arrayidx364 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %191 = load i64, i64* %arrayidx364, align 16, !tbaa !8
  %arrayidx365 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %192 = load i64, i64* %arrayidx365, align 8, !tbaa !8
  %xor366 = xor i64 %191, %192
  %call367 = call i64 @rotr64(i64 %xor366, i32 32) #5
  %arrayidx368 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call367, i64* %arrayidx368, align 16, !tbaa !8
  %arrayidx369 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %193 = load i64, i64* %arrayidx369, align 8, !tbaa !8
  %arrayidx370 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %194 = load i64, i64* %arrayidx370, align 16, !tbaa !8
  %add371 = add i64 %193, %194
  %arrayidx372 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add371, i64* %arrayidx372, align 8, !tbaa !8
  %arrayidx373 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %195 = load i64, i64* %arrayidx373, align 16, !tbaa !8
  %arrayidx374 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %196 = load i64, i64* %arrayidx374, align 8, !tbaa !8
  %xor375 = xor i64 %195, %196
  %call376 = call i64 @rotr64(i64 %xor375, i32 24) #5
  %arrayidx377 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call376, i64* %arrayidx377, align 16, !tbaa !8
  %arrayidx378 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %197 = load i64, i64* %arrayidx378, align 8, !tbaa !8
  %arrayidx379 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %198 = load i64, i64* %arrayidx379, align 16, !tbaa !8
  %add380 = add i64 %197, %198
  %199 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx381 = getelementptr inbounds [12 x [16 x i32]], [12 x [16 x i32]]* @blake2b_sigma, i32 0, i32 %199
  %arrayidx382 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx381, i32 0, i32 15
  %200 = load i32, i32* %arrayidx382, align 4, !tbaa !6
  %arrayidx383 = getelementptr inbounds [16 x i64], [16 x i64]* %m, i32 0, i32 %200
  %201 = load i64, i64* %arrayidx383, align 8, !tbaa !8
  %add384 = add i64 %add380, %201
  %arrayidx385 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  store i64 %add384, i64* %arrayidx385, align 8, !tbaa !8
  %arrayidx386 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %202 = load i64, i64* %arrayidx386, align 16, !tbaa !8
  %arrayidx387 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %203 = load i64, i64* %arrayidx387, align 8, !tbaa !8
  %xor388 = xor i64 %202, %203
  %call389 = call i64 @rotr64(i64 %xor388, i32 16) #5
  %arrayidx390 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call389, i64* %arrayidx390, align 16, !tbaa !8
  %arrayidx391 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %204 = load i64, i64* %arrayidx391, align 8, !tbaa !8
  %arrayidx392 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %205 = load i64, i64* %arrayidx392, align 16, !tbaa !8
  %add393 = add i64 %204, %205
  %arrayidx394 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %add393, i64* %arrayidx394, align 8, !tbaa !8
  %arrayidx395 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %206 = load i64, i64* %arrayidx395, align 16, !tbaa !8
  %arrayidx396 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %207 = load i64, i64* %arrayidx396, align 8, !tbaa !8
  %xor397 = xor i64 %206, %207
  %call398 = call i64 @rotr64(i64 %xor397, i32 63) #5
  %arrayidx399 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call398, i64* %arrayidx399, align 16, !tbaa !8
  br label %do.cond400

do.cond400:                                       ; preds = %do.body355
  br label %do.end401

do.end401:                                        ; preds = %do.cond400
  br label %do.cond402

do.cond402:                                       ; preds = %do.end401
  br label %do.end403

do.end403:                                        ; preds = %do.cond402
  br label %for.inc404

for.inc404:                                       ; preds = %do.end403
  %208 = load i32, i32* %r, align 4, !tbaa !6
  %inc405 = add i32 %208, 1
  store i32 %inc405, i32* %r, align 4, !tbaa !6
  br label %for.cond26

for.end406:                                       ; preds = %for.cond26
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond407

for.cond407:                                      ; preds = %for.inc419, %for.end406
  %209 = load i32, i32* %i, align 4, !tbaa !6
  %cmp408 = icmp ult i32 %209, 8
  br i1 %cmp408, label %for.body409, label %for.end421

for.body409:                                      ; preds = %for.cond407
  %210 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %h410 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %210, i32 0, i32 0
  %211 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx411 = getelementptr inbounds [8 x i64], [8 x i64]* %h410, i32 0, i32 %211
  %212 = load i64, i64* %arrayidx411, align 8, !tbaa !8
  %213 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx412 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 %213
  %214 = load i64, i64* %arrayidx412, align 8, !tbaa !8
  %xor413 = xor i64 %212, %214
  %215 = load i32, i32* %i, align 4, !tbaa !6
  %add414 = add i32 %215, 8
  %arrayidx415 = getelementptr inbounds [16 x i64], [16 x i64]* %v, i32 0, i32 %add414
  %216 = load i64, i64* %arrayidx415, align 8, !tbaa !8
  %xor416 = xor i64 %xor413, %216
  %217 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %h417 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %217, i32 0, i32 0
  %218 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx418 = getelementptr inbounds [8 x i64], [8 x i64]* %h417, i32 0, i32 %218
  store i64 %xor416, i64* %arrayidx418, align 8, !tbaa !8
  br label %for.inc419

for.inc419:                                       ; preds = %for.body409
  %219 = load i32, i32* %i, align 4, !tbaa !6
  %inc420 = add i32 %219, 1
  store i32 %inc420, i32* %i, align 4, !tbaa !6
  br label %for.cond407

for.end421:                                       ; preds = %for.cond407
  %220 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  %221 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #4
  %222 = bitcast [16 x i64]* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %222) #4
  %223 = bitcast [16 x i64]* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %223) #4
  ret void
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @blake2b_final(%struct.__blake2b_state* %S, i8* %out, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.__blake2b_state*, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %buffer = alloca [64 x i8], align 16
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  store i8* %out, i8** %out.addr, align 4, !tbaa !2
  store i32 %outlen, i32* %outlen.addr, align 4, !tbaa !14
  %0 = bitcast [64 x i8]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #4
  %1 = bitcast [64 x i8]* %buffer to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %1, i8 0, i32 64, i1 false)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.__blake2b_state* %3, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load i8*, i8** %out.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i8* %4, null
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %5 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %6 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %outlen3 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %6, i32 0, i32 5
  %7 = load i32, i32* %outlen3, align 4, !tbaa !12
  %cmp4 = icmp ult i32 %5, %7
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false2
  %8 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %f = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %8, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 0
  %9 = load i64, i64* %arrayidx, align 8, !tbaa !8
  %cmp5 = icmp ne i64 %9, 0
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %10 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %11 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %11, i32 0, i32 4
  %12 = load i32, i32* %buflen, align 8, !tbaa !23
  %conv = zext i32 %12 to i64
  call void @blake2b_increment_counter(%struct.__blake2b_state* %10, i64 %conv) #5
  %13 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_set_lastblock(%struct.__blake2b_state* %13) #5
  %14 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %14, i32 0, i32 3
  %15 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen8 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %15, i32 0, i32 4
  %16 = load i32, i32* %buflen8, align 8, !tbaa !23
  %arrayidx9 = getelementptr inbounds [128 x i8], [128 x i8]* %buf, i32 0, i32 %16
  %17 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buflen10 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %17, i32 0, i32 4
  %18 = load i32, i32* %buflen10, align 8, !tbaa !23
  %sub = sub i32 128, %18
  call void @llvm.memset.p0i8.i32(i8* align 1 %arrayidx9, i8 0, i32 %sub, i1 false)
  %19 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %20 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buf11 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %20, i32 0, i32 3
  %arraydecay = getelementptr inbounds [128 x i8], [128 x i8]* %buf11, i32 0, i32 0
  call void @blake2b_compress(%struct.__blake2b_state* %19, i8* %arraydecay) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end7
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %cmp12 = icmp ult i32 %21, 8
  br i1 %cmp12, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %arraydecay14 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul i32 8, %22
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay14, i32 %mul
  %23 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %23, i32 0, i32 0
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [8 x i64], [8 x i64]* %h, i32 0, i32 %24
  %25 = load i64, i64* %arrayidx15, align 8, !tbaa !8
  call void @store64(i8* %add.ptr, i64 %25) #5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %26, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %27 = load i8*, i8** %out.addr, align 4, !tbaa !2
  %arraydecay16 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %28 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %outlen17 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %28, i32 0, i32 5
  %29 = load i32, i32* %outlen17, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %27, i8* align 16 %arraydecay16, i32 %29, i1 false)
  %arraydecay18 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  call void @clear_internal_memory(i8* %arraydecay18, i32 64) #5
  %30 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %buf19 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %30, i32 0, i32 3
  %arraydecay20 = getelementptr inbounds [128 x i8], [128 x i8]* %buf19, i32 0, i32 0
  call void @clear_internal_memory(i8* %arraydecay20, i32 128) #5
  %31 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %h21 = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %31, i32 0, i32 0
  %arraydecay22 = getelementptr inbounds [8 x i64], [8 x i64]* %h21, i32 0, i32 0
  %32 = bitcast i64* %arraydecay22 to i8*
  call void @clear_internal_memory(i8* %32, i32 64) #5
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then6, %if.then
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %34 = bitcast [64 x i8]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %34) #4
  %35 = load i32, i32* %retval, align 4
  ret i32 %35
}

; Function Attrs: noinline nounwind optsize
define internal void @blake2b_set_lastblock(%struct.__blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.__blake2b_state*, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %0 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %last_node = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %0, i32 0, i32 6
  %1 = load i8, i8* %last_node, align 8, !tbaa !24
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  call void @blake2b_set_lastnode(%struct.__blake2b_state* %2) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %f = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %3, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 0
  store i64 -1, i64* %arrayidx, align 8, !tbaa !8
  ret void
}

; Function Attrs: noinline nounwind optsize
define internal void @store64(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i64 %w, i64* %w.addr, align 8, !tbaa !8
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @blake2b(i8* %out, i32 %outlen, i8* %in, i32 %inlen, i8* %key, i32 %keylen) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %S = alloca %struct.__blake2b_state, align 8
  %ret = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4, !tbaa !2
  store i32 %outlen, i32* %outlen.addr, align 4, !tbaa !14
  store i8* %in, i8** %in.addr, align 4, !tbaa !2
  store i32 %inlen, i32* %inlen.addr, align 4, !tbaa !14
  store i8* %key, i8** %key.addr, align 4, !tbaa !2
  store i32 %keylen, i32* %keylen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.__blake2b_state* %S to i8*
  call void @llvm.lifetime.start.p0i8(i64 240, i8* %0) #4
  %1 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 -1, i32* %ret, align 4, !tbaa !6
  %2 = load i8*, i8** %in.addr, align 4, !tbaa !2
  %cmp = icmp eq i8* null, %2
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %cmp1 = icmp ugt i32 %3, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %fail

if.end:                                           ; preds = %land.lhs.true, %entry
  %4 = load i8*, i8** %out.addr, align 4, !tbaa !2
  %cmp2 = icmp eq i8* null, %4
  br i1 %cmp2, label %if.then6, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %5 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp3 = icmp eq i32 %5, 0
  br i1 %cmp3, label %if.then6, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false
  %6 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp5 = icmp ugt i32 %6, 64
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %lor.lhs.false4, %lor.lhs.false, %if.end
  br label %fail

if.end7:                                          ; preds = %lor.lhs.false4
  %7 = load i8*, i8** %key.addr, align 4, !tbaa !2
  %cmp8 = icmp eq i8* null, %7
  br i1 %cmp8, label %land.lhs.true9, label %lor.lhs.false11

land.lhs.true9:                                   ; preds = %if.end7
  %8 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  %cmp10 = icmp ugt i32 %8, 0
  br i1 %cmp10, label %if.then13, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %land.lhs.true9, %if.end7
  %9 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  %cmp12 = icmp ugt i32 %9, 64
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %lor.lhs.false11, %land.lhs.true9
  br label %fail

if.end14:                                         ; preds = %lor.lhs.false11
  %10 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  %cmp15 = icmp ugt i32 %10, 0
  br i1 %cmp15, label %if.then16, label %if.else

if.then16:                                        ; preds = %if.end14
  %11 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %12 = load i8*, i8** %key.addr, align 4, !tbaa !2
  %13 = load i32, i32* %keylen.addr, align 4, !tbaa !14
  %call = call i32 @blake2b_init_key(%struct.__blake2b_state* %S, i32 %11, i8* %12, i32 %13) #5
  %cmp17 = icmp slt i32 %call, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.then16
  br label %fail

if.end19:                                         ; preds = %if.then16
  br label %if.end24

if.else:                                          ; preds = %if.end14
  %14 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %call20 = call i32 @blake2b_init(%struct.__blake2b_state* %S, i32 %14) #5
  %cmp21 = icmp slt i32 %call20, 0
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.else
  br label %fail

if.end23:                                         ; preds = %if.else
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.end19
  %15 = load i8*, i8** %in.addr, align 4, !tbaa !2
  %16 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %call25 = call i32 @blake2b_update(%struct.__blake2b_state* %S, i8* %15, i32 %16) #5
  %cmp26 = icmp slt i32 %call25, 0
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.end24
  br label %fail

if.end28:                                         ; preds = %if.end24
  %17 = load i8*, i8** %out.addr, align 4, !tbaa !2
  %18 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %call29 = call i32 @blake2b_final(%struct.__blake2b_state* %S, i8* %17, i32 %18) #5
  store i32 %call29, i32* %ret, align 4, !tbaa !6
  br label %fail

fail:                                             ; preds = %if.end28, %if.then27, %if.then22, %if.then18, %if.then13, %if.then6, %if.then
  %19 = bitcast %struct.__blake2b_state* %S to i8*
  call void @clear_internal_memory(i8* %19, i32 240) #5
  %20 = load i32, i32* %ret, align 4, !tbaa !6
  %21 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  %22 = bitcast %struct.__blake2b_state* %S to i8*
  call void @llvm.lifetime.end.p0i8(i64 240, i8* %22) #4
  ret i32 %20
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @blake2b_long(i8* %pout, i32 %outlen, i8* %in, i32 %inlen) #0 {
entry:
  %retval = alloca i32, align 4
  %pout.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %out = alloca i8*, align 4
  %blake_state = alloca %struct.__blake2b_state, align 8
  %outlen_bytes = alloca [4 x i8], align 1
  %ret = alloca i32, align 4
  %toproduce = alloca i32, align 4
  %out_buffer = alloca [64 x i8], align 16
  %in_buffer = alloca [64 x i8], align 16
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %pout, i8** %pout.addr, align 4, !tbaa !2
  store i32 %outlen, i32* %outlen.addr, align 4, !tbaa !14
  store i8* %in, i8** %in.addr, align 4, !tbaa !2
  store i32 %inlen, i32* %inlen.addr, align 4, !tbaa !14
  %0 = bitcast i8** %out to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %pout.addr, align 4, !tbaa !2
  store i8* %1, i8** %out, align 4, !tbaa !2
  %2 = bitcast %struct.__blake2b_state* %blake_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 240, i8* %2) #4
  %3 = bitcast [4 x i8]* %outlen_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast [4 x i8]* %outlen_bytes to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %4, i8 0, i32 4, i1 false)
  %5 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 -1, i32* %ret, align 4, !tbaa !6
  %6 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp = icmp ugt i32 %6, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %fail

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %outlen_bytes, i32 0, i32 0
  %7 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  call void @store32(i8* %arraydecay, i32 %7) #5
  %8 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %cmp1 = icmp ule i32 %8, 64
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  br label %do.body

do.body:                                          ; preds = %if.then2
  %9 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %call = call i32 @blake2b_init(%struct.__blake2b_state* %blake_state, i32 %9) #5
  store i32 %call, i32* %ret, align 4, !tbaa !6
  %10 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %10, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %do.body
  br label %fail

if.end5:                                          ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end5
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body6

do.body6:                                         ; preds = %do.end
  %arraydecay7 = getelementptr inbounds [4 x i8], [4 x i8]* %outlen_bytes, i32 0, i32 0
  %call8 = call i32 @blake2b_update(%struct.__blake2b_state* %blake_state, i8* %arraydecay7, i32 4) #5
  store i32 %call8, i32* %ret, align 4, !tbaa !6
  %11 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %11, 0
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %do.body6
  br label %fail

if.end11:                                         ; preds = %do.body6
  br label %do.cond12

do.cond12:                                        ; preds = %if.end11
  br label %do.end13

do.end13:                                         ; preds = %do.cond12
  br label %do.body14

do.body14:                                        ; preds = %do.end13
  %12 = load i8*, i8** %in.addr, align 4, !tbaa !2
  %13 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %call15 = call i32 @blake2b_update(%struct.__blake2b_state* %blake_state, i8* %12, i32 %13) #5
  store i32 %call15, i32* %ret, align 4, !tbaa !6
  %14 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %14, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %do.body14
  br label %fail

if.end18:                                         ; preds = %do.body14
  br label %do.cond19

do.cond19:                                        ; preds = %if.end18
  br label %do.end20

do.end20:                                         ; preds = %do.cond19
  br label %do.body21

do.body21:                                        ; preds = %do.end20
  %15 = load i8*, i8** %out, align 4, !tbaa !2
  %16 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %call22 = call i32 @blake2b_final(%struct.__blake2b_state* %blake_state, i8* %15, i32 %16) #5
  store i32 %call22, i32* %ret, align 4, !tbaa !6
  %17 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %17, 0
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %do.body21
  br label %fail

if.end25:                                         ; preds = %do.body21
  br label %do.cond26

do.cond26:                                        ; preds = %if.end25
  br label %do.end27

do.end27:                                         ; preds = %do.cond26
  br label %if.end88

if.else:                                          ; preds = %if.end
  %18 = bitcast i32* %toproduce to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = bitcast [64 x i8]* %out_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %19) #4
  %20 = bitcast [64 x i8]* %in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %20) #4
  br label %do.body28

do.body28:                                        ; preds = %if.else
  %call29 = call i32 @blake2b_init(%struct.__blake2b_state* %blake_state, i32 64) #5
  store i32 %call29, i32* %ret, align 4, !tbaa !6
  %21 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp30 = icmp slt i32 %21, 0
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %do.body28
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end32:                                         ; preds = %do.body28
  br label %do.cond33

do.cond33:                                        ; preds = %if.end32
  br label %do.end34

do.end34:                                         ; preds = %do.cond33
  br label %do.body35

do.body35:                                        ; preds = %do.end34
  %arraydecay36 = getelementptr inbounds [4 x i8], [4 x i8]* %outlen_bytes, i32 0, i32 0
  %call37 = call i32 @blake2b_update(%struct.__blake2b_state* %blake_state, i8* %arraydecay36, i32 4) #5
  store i32 %call37, i32* %ret, align 4, !tbaa !6
  %22 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %22, 0
  br i1 %cmp38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %do.body35
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end40:                                         ; preds = %do.body35
  br label %do.cond41

do.cond41:                                        ; preds = %if.end40
  br label %do.end42

do.end42:                                         ; preds = %do.cond41
  br label %do.body43

do.body43:                                        ; preds = %do.end42
  %23 = load i8*, i8** %in.addr, align 4, !tbaa !2
  %24 = load i32, i32* %inlen.addr, align 4, !tbaa !14
  %call44 = call i32 @blake2b_update(%struct.__blake2b_state* %blake_state, i8* %23, i32 %24) #5
  store i32 %call44, i32* %ret, align 4, !tbaa !6
  %25 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp45 = icmp slt i32 %25, 0
  br i1 %cmp45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %do.body43
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end47:                                         ; preds = %do.body43
  br label %do.cond48

do.cond48:                                        ; preds = %if.end47
  br label %do.end49

do.end49:                                         ; preds = %do.cond48
  br label %do.body50

do.body50:                                        ; preds = %do.end49
  %arraydecay51 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %call52 = call i32 @blake2b_final(%struct.__blake2b_state* %blake_state, i8* %arraydecay51, i32 64) #5
  store i32 %call52, i32* %ret, align 4, !tbaa !6
  %26 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp53 = icmp slt i32 %26, 0
  br i1 %cmp53, label %if.then54, label %if.end55

if.then54:                                        ; preds = %do.body50
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end55:                                         ; preds = %do.body50
  br label %do.cond56

do.cond56:                                        ; preds = %if.end55
  br label %do.end57

do.end57:                                         ; preds = %do.cond56
  %27 = load i8*, i8** %out, align 4, !tbaa !2
  %arraydecay58 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %27, i8* align 16 %arraydecay58, i32 32, i1 false)
  %28 = load i8*, i8** %out, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %28, i32 32
  store i8* %add.ptr, i8** %out, align 4, !tbaa !2
  %29 = load i32, i32* %outlen.addr, align 4, !tbaa !14
  %sub = sub i32 %29, 32
  store i32 %sub, i32* %toproduce, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %do.end70, %do.end57
  %30 = load i32, i32* %toproduce, align 4, !tbaa !6
  %cmp59 = icmp ugt i32 %30, 64
  br i1 %cmp59, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arraydecay60 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay60, i8* align 16 %arraydecay61, i32 64, i1 false)
  br label %do.body62

do.body62:                                        ; preds = %while.body
  %arraydecay63 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %arraydecay64 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %call65 = call i32 @blake2b(i8* %arraydecay63, i32 64, i8* %arraydecay64, i32 64, i8* null, i32 0) #5
  store i32 %call65, i32* %ret, align 4, !tbaa !6
  %31 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp66 = icmp slt i32 %31, 0
  br i1 %cmp66, label %if.then67, label %if.end68

if.then67:                                        ; preds = %do.body62
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end68:                                         ; preds = %do.body62
  br label %do.cond69

do.cond69:                                        ; preds = %if.end68
  br label %do.end70

do.end70:                                         ; preds = %do.cond69
  %32 = load i8*, i8** %out, align 4, !tbaa !2
  %arraydecay71 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %32, i8* align 16 %arraydecay71, i32 32, i1 false)
  %33 = load i8*, i8** %out, align 4, !tbaa !2
  %add.ptr72 = getelementptr inbounds i8, i8* %33, i32 32
  store i8* %add.ptr72, i8** %out, align 4, !tbaa !2
  %34 = load i32, i32* %toproduce, align 4, !tbaa !6
  %sub73 = sub i32 %34, 32
  store i32 %sub73, i32* %toproduce, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %arraydecay74 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %arraydecay75 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay74, i8* align 16 %arraydecay75, i32 64, i1 false)
  br label %do.body76

do.body76:                                        ; preds = %while.end
  %arraydecay77 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %35 = load i32, i32* %toproduce, align 4, !tbaa !6
  %arraydecay78 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %call79 = call i32 @blake2b(i8* %arraydecay77, i32 %35, i8* %arraydecay78, i32 64, i8* null, i32 0) #5
  store i32 %call79, i32* %ret, align 4, !tbaa !6
  %36 = load i32, i32* %ret, align 4, !tbaa !6
  %cmp80 = icmp slt i32 %36, 0
  br i1 %cmp80, label %if.then81, label %if.end82

if.then81:                                        ; preds = %do.body76
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end82:                                         ; preds = %do.body76
  br label %do.cond83

do.cond83:                                        ; preds = %if.end82
  br label %do.end84

do.end84:                                         ; preds = %do.cond83
  %37 = load i8*, i8** %out, align 4, !tbaa !2
  %arraydecay85 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %38 = load i32, i32* %toproduce, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %37, i8* align 16 %arraydecay85, i32 %38, i1 false)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then81, %if.then67, %if.then54, %if.then46, %if.then39, %if.then31, %do.end84
  %39 = bitcast [64 x i8]* %in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %39) #4
  %40 = bitcast [64 x i8]* %out_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %40) #4
  %41 = bitcast i32* %toproduce to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup89 [
    i32 0, label %cleanup.cont
    i32 2, label %fail
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end88

if.end88:                                         ; preds = %cleanup.cont, %do.end27
  br label %fail

fail:                                             ; preds = %if.end88, %cleanup, %if.then24, %if.then17, %if.then10, %if.then4, %if.then
  %42 = bitcast %struct.__blake2b_state* %blake_state to i8*
  call void @clear_internal_memory(i8* %42, i32 240) #5
  %43 = load i32, i32* %ret, align 4, !tbaa !6
  store i32 %43, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup89

cleanup89:                                        ; preds = %fail, %cleanup
  %44 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast [4 x i8]* %outlen_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast %struct.__blake2b_state* %blake_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 240, i8* %46) #4
  %47 = bitcast i8** %out to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = load i32, i32* %retval, align 4
  ret i32 %48
}

; Function Attrs: noinline nounwind optsize
define internal void @store32(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optsize
define internal i64 @rotr64(i64 %w, i32 %c) #0 {
entry:
  %w.addr = alloca i64, align 8
  %c.addr = alloca i32, align 4
  store i64 %w, i64* %w.addr, align 8, !tbaa !8
  store i32 %c, i32* %c.addr, align 4, !tbaa !6
  %0 = load i64, i64* %w.addr, align 8, !tbaa !8
  %1 = load i32, i32* %c.addr, align 4, !tbaa !6
  %sh_prom = zext i32 %1 to i64
  %shr = lshr i64 %0, %sh_prom
  %2 = load i64, i64* %w.addr, align 8, !tbaa !8
  %3 = load i32, i32* %c.addr, align 4, !tbaa !6
  %sub = sub i32 64, %3
  %sh_prom1 = zext i32 %sub to i64
  %shl = shl i64 %2, %sh_prom1
  %or = or i64 %shr, %shl
  ret i64 %or
}

; Function Attrs: noinline nounwind optsize
define internal void @blake2b_set_lastnode(%struct.__blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.__blake2b_state*, align 4
  store %struct.__blake2b_state* %S, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %0 = load %struct.__blake2b_state*, %struct.__blake2b_state** %S.addr, align 4, !tbaa !2
  %f = getelementptr inbounds %struct.__blake2b_state, %struct.__blake2b_state* %0, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x i64], [2 x i64]* %f, i32 0, i32 1
  store i64 -1, i64* %arrayidx, align 8, !tbaa !8
  ret void
}

attributes #0 = { noinline nounwind optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { optsize }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long long", !4, i64 0}
!10 = !{!11, !4, i64 0}
!11 = !{!"__blake2b_param", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !7, i64 4, !9, i64 8, !4, i64 16, !4, i64 17, !4, i64 18, !4, i64 32, !4, i64 48}
!12 = !{!13, !7, i64 228}
!13 = !{!"__blake2b_state", !4, i64 0, !4, i64 64, !4, i64 80, !4, i64 96, !7, i64 224, !7, i64 228, !4, i64 232}
!14 = !{!15, !15, i64 0}
!15 = !{!"long", !4, i64 0}
!16 = !{!11, !4, i64 1}
!17 = !{!11, !4, i64 2}
!18 = !{!11, !4, i64 3}
!19 = !{!11, !7, i64 4}
!20 = !{!11, !9, i64 8}
!21 = !{!11, !4, i64 16}
!22 = !{!11, !4, i64 17}
!23 = !{!13, !7, i64 224}
!24 = !{!13, !4, i64 232}
