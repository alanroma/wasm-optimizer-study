; ModuleID = '/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/core.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/core.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.block_ = type { [128 x i64] }
%struct.Argon2_Context = type { i8*, i32, i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32 (i8**, i32)*, void (i8*, i32)*, i32 }
%struct.Argon2_instance_t = type { %struct.block_*, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.Argon2_Context* }
%struct.Argon2_position_t = type { i32, i32, i8, i32 }
%struct.__pthread = type opaque
%struct.Argon2_thread_data = type { %struct.Argon2_instance_t*, %struct.Argon2_position_t }
%struct.__blake2b_state = type { [8 x i64], [2 x i64], [2 x i64], [128 x i8], i32, i32, i8 }

@secure_wipe_memory.memset_sec = internal constant i8* (i8*, i32, i32)* @memset, align 4
@FLAG_clear_internal_memory = hidden global i32 1, align 4

; Function Attrs: noinline nounwind optsize
define hidden void @init_block_value(%struct.block_* %b, i8 zeroext %in) #0 {
entry:
  %b.addr = alloca %struct.block_*, align 4
  %in.addr = alloca i8, align 1
  store %struct.block_* %b, %struct.block_** %b.addr, align 4, !tbaa !2
  store i8 %in, i8* %in.addr, align 1, !tbaa !6
  %0 = load %struct.block_*, %struct.block_** %b.addr, align 4, !tbaa !2
  %v = getelementptr inbounds %struct.block_, %struct.block_* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %1 = bitcast i64* %arraydecay to i8*
  %2 = load i8, i8* %in.addr, align 1, !tbaa !6
  %conv = zext i8 %2 to i32
  %3 = trunc i32 %conv to i8
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 %3, i32 1024, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optsize
define hidden void @copy_block(%struct.block_* %dst, %struct.block_* %src) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %src.addr = alloca %struct.block_*, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4, !tbaa !2
  store %struct.block_* %src, %struct.block_** %src.addr, align 4, !tbaa !2
  %0 = load %struct.block_*, %struct.block_** %dst.addr, align 4, !tbaa !2
  %v = getelementptr inbounds %struct.block_, %struct.block_* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %1 = bitcast i64* %arraydecay to i8*
  %2 = load %struct.block_*, %struct.block_** %src.addr, align 4, !tbaa !2
  %v1 = getelementptr inbounds %struct.block_, %struct.block_* %2, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [128 x i64], [128 x i64]* %v1, i32 0, i32 0
  %3 = bitcast i64* %arraydecay2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %1, i8* align 8 %3, i32 1024, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optsize
define hidden void @xor_block(%struct.block_* %dst, %struct.block_* %src) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %src.addr = alloca %struct.block_*, align 4
  %i = alloca i32, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4, !tbaa !2
  store %struct.block_* %src, %struct.block_** %src.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp slt i32 %1, 128
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.block_*, %struct.block_** %src.addr, align 4, !tbaa !2
  %v = getelementptr inbounds %struct.block_, %struct.block_* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 %3
  %4 = load i64, i64* %arrayidx, align 8, !tbaa !9
  %5 = load %struct.block_*, %struct.block_** %dst.addr, align 4, !tbaa !2
  %v1 = getelementptr inbounds %struct.block_, %struct.block_* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx2 = getelementptr inbounds [128 x i64], [128 x i64]* %v1, i32 0, i32 %6
  %7 = load i64, i64* %arrayidx2, align 8, !tbaa !9
  %xor = xor i64 %7, %4
  store i64 %xor, i64* %arrayidx2, align 8, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: noinline nounwind optsize
define hidden i32 @allocate_memory(%struct.Argon2_Context* %context, i8** %memory, i32 %num, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %memory.addr = alloca i8**, align 4
  %num.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  %memory_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  store i8** %memory, i8*** %memory.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !11
  store i32 %size, i32* %size.addr, align 4, !tbaa !11
  %0 = bitcast i32* %memory_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %num.addr, align 4, !tbaa !11
  %2 = load i32, i32* %size.addr, align 4, !tbaa !11
  %mul = mul i32 %1, %2
  store i32 %mul, i32* %memory_size, align 4, !tbaa !11
  %3 = load i8**, i8*** %memory.addr, align 4, !tbaa !2
  %cmp = icmp eq i8** %3, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -22, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %size.addr, align 4, !tbaa !11
  %cmp1 = icmp ne i32 %4, 0
  br i1 %cmp1, label %land.lhs.true, label %if.end4

land.lhs.true:                                    ; preds = %if.end
  %5 = load i32, i32* %memory_size, align 4, !tbaa !11
  %6 = load i32, i32* %size.addr, align 4, !tbaa !11
  %div = udiv i32 %5, %6
  %7 = load i32, i32* %num.addr, align 4, !tbaa !11
  %cmp2 = icmp ne i32 %div, %7
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %land.lhs.true
  store i32 -22, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %land.lhs.true, %if.end
  %8 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %allocate_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %8, i32 0, i32 15
  %9 = load i32 (i8**, i32)*, i32 (i8**, i32)** %allocate_cbk, align 4, !tbaa !13
  %tobool = icmp ne i32 (i8**, i32)* %9, null
  br i1 %tobool, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end4
  %10 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %allocate_cbk6 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %10, i32 0, i32 15
  %11 = load i32 (i8**, i32)*, i32 (i8**, i32)** %allocate_cbk6, align 4, !tbaa !13
  %12 = load i8**, i8*** %memory.addr, align 4, !tbaa !2
  %13 = load i32, i32* %memory_size, align 4, !tbaa !11
  %call = call i32 %11(i8** %12, i32 %13) #6
  br label %if.end8

if.else:                                          ; preds = %if.end4
  %14 = load i32, i32* %memory_size, align 4, !tbaa !11
  %call7 = call i8* @malloc(i32 %14) #6
  %15 = load i8**, i8*** %memory.addr, align 4, !tbaa !2
  store i8* %call7, i8** %15, align 4, !tbaa !2
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.then5
  %16 = load i8**, i8*** %memory.addr, align 4, !tbaa !2
  %17 = load i8*, i8** %16, align 4, !tbaa !2
  %cmp9 = icmp eq i8* %17, null
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end8
  store i32 -22, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %if.end8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end11, %if.then10, %if.then3, %if.then
  %18 = bitcast i32* %memory_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: optsize
declare i8* @malloc(i32) #3

; Function Attrs: noinline nounwind optsize
define hidden void @free_memory(%struct.Argon2_Context* %context, i8* %memory, i32 %num, i32 %size) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %memory.addr = alloca i8*, align 4
  %num.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  %memory_size = alloca i32, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  store i8* %memory, i8** %memory.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !11
  store i32 %size, i32* %size.addr, align 4, !tbaa !11
  %0 = bitcast i32* %memory_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %num.addr, align 4, !tbaa !11
  %2 = load i32, i32* %size.addr, align 4, !tbaa !11
  %mul = mul i32 %1, %2
  store i32 %mul, i32* %memory_size, align 4, !tbaa !11
  %3 = load i8*, i8** %memory.addr, align 4, !tbaa !2
  %4 = load i32, i32* %memory_size, align 4, !tbaa !11
  call void @clear_internal_memory(i8* %3, i32 %4) #6
  %5 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %free_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %5, i32 0, i32 16
  %6 = load void (i8*, i32)*, void (i8*, i32)** %free_cbk, align 4, !tbaa !15
  %tobool = icmp ne void (i8*, i32)* %6, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %free_cbk1 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %7, i32 0, i32 16
  %8 = load void (i8*, i32)*, void (i8*, i32)** %free_cbk1, align 4, !tbaa !15
  %9 = load i8*, i8** %memory.addr, align 4, !tbaa !2
  %10 = load i32, i32* %memory_size, align 4, !tbaa !11
  call void %8(i8* %9, i32 %10) #6
  br label %if.end

if.else:                                          ; preds = %entry
  %11 = load i8*, i8** %memory.addr, align 4, !tbaa !2
  call void @free(i8* %11) #6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = bitcast i32* %memory_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  ret void
}

; Function Attrs: noinline nounwind optsize
define hidden void @clear_internal_memory(i8* %v, i32 %n) #0 {
entry:
  %v.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  store i8* %v, i8** %v.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !11
  %0 = load i32, i32* @FLAG_clear_internal_memory, align 4, !tbaa !7
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i8*, i8** %v.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i8* %1, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load i8*, i8** %v.addr, align 4, !tbaa !2
  %3 = load i32, i32* %n.addr, align 4, !tbaa !11
  call void @secure_wipe_memory(i8* %2, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: optsize
declare void @free(i8*) #3

; Function Attrs: noinline nounwind optnone
define hidden void @secure_wipe_memory(i8* %v, i32 %n) #4 {
entry:
  %v.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  store i8* %v, i8** %v.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !11
  %0 = load volatile i8* (i8*, i32, i32)*, i8* (i8*, i32, i32)** @secure_wipe_memory.memset_sec, align 4, !tbaa !2
  %1 = load i8*, i8** %v.addr, align 4, !tbaa !2
  %2 = load i32, i32* %n.addr, align 4, !tbaa !11
  %call = call i8* %0(i8* %1, i32 0, i32 %2) #6
  ret void
}

; Function Attrs: optsize
declare i8* @memset(i8*, i32, i32) #3

; Function Attrs: noinline nounwind optsize
define hidden void @finalize(%struct.Argon2_Context* %context, %struct.Argon2_instance_t* %instance) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %blockhash = alloca %struct.block_, align 8
  %l = alloca i32, align 4
  %last_block_in_lane = alloca i32, align 4
  %blockhash_bytes = alloca [1024 x i8], align 16
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.Argon2_Context* %0, null
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %cmp1 = icmp ne %struct.Argon2_instance_t* %1, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = bitcast %struct.block_* %blockhash to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %2) #5
  %3 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %4, i32 0, i32 0
  %5 = load %struct.block_*, %struct.block_** %memory, align 4, !tbaa !16
  %6 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %6, i32 0, i32 5
  %7 = load i32, i32* %lane_length, align 4, !tbaa !18
  %add.ptr = getelementptr inbounds %struct.block_, %struct.block_* %5, i32 %7
  %add.ptr2 = getelementptr inbounds %struct.block_, %struct.block_* %add.ptr, i32 -1
  call void @copy_block(%struct.block_* %blockhash, %struct.block_* %add.ptr2) #6
  store i32 1, i32* %l, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %l, align 4, !tbaa !7
  %9 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %9, i32 0, i32 6
  %10 = load i32, i32* %lanes, align 4, !tbaa !19
  %cmp3 = icmp ult i32 %8, %10
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = bitcast i32* %last_block_in_lane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %l, align 4, !tbaa !7
  %13 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length4 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %13, i32 0, i32 5
  %14 = load i32, i32* %lane_length4, align 4, !tbaa !18
  %mul = mul i32 %12, %14
  %15 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length5 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %15, i32 0, i32 5
  %16 = load i32, i32* %lane_length5, align 4, !tbaa !18
  %sub = sub i32 %16, 1
  %add = add i32 %mul, %sub
  store i32 %add, i32* %last_block_in_lane, align 4, !tbaa !7
  %17 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory6 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %17, i32 0, i32 0
  %18 = load %struct.block_*, %struct.block_** %memory6, align 4, !tbaa !16
  %19 = load i32, i32* %last_block_in_lane, align 4, !tbaa !7
  %add.ptr7 = getelementptr inbounds %struct.block_, %struct.block_* %18, i32 %19
  call void @xor_block(%struct.block_* %blockhash, %struct.block_* %add.ptr7) #6
  %20 = bitcast i32* %last_block_in_lane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %l, align 4, !tbaa !7
  %inc = add i32 %21, 1
  store i32 %inc, i32* %l, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = bitcast [1024 x i8]* %blockhash_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %22) #5
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @store_block(i8* %arraydecay, %struct.block_* %blockhash) #6
  %23 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %23, i32 0, i32 0
  %24 = load i8*, i8** %out, align 4, !tbaa !20
  %25 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %25, i32 0, i32 1
  %26 = load i32, i32* %outlen, align 4, !tbaa !21
  %arraydecay8 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  %call = call i32 @blake2b_long(i8* %24, i32 %26, i8* %arraydecay8, i32 1024) #6
  %v = getelementptr inbounds %struct.block_, %struct.block_* %blockhash, i32 0, i32 0
  %arraydecay9 = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %27 = bitcast i64* %arraydecay9 to i8*
  call void @clear_internal_memory(i8* %27, i32 1024) #6
  %arraydecay10 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @clear_internal_memory(i8* %arraydecay10, i32 1024) #6
  %28 = bitcast [1024 x i8]* %blockhash_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %28) #5
  %29 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %30 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory11 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %30, i32 0, i32 0
  %31 = load %struct.block_*, %struct.block_** %memory11, align 4, !tbaa !16
  %32 = bitcast %struct.block_* %31 to i8*
  %33 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory_blocks = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %33, i32 0, i32 3
  %34 = load i32, i32* %memory_blocks, align 4, !tbaa !22
  call void @free_memory(%struct.Argon2_Context* %29, i8* %32, i32 %34, i32 1024) #6
  %35 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  %36 = bitcast %struct.block_* %blockhash to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %36) #5
  br label %if.end

if.end:                                           ; preds = %for.end, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optsize
define internal void @store_block(i8* %output, %struct.block_* %src) #0 {
entry:
  %output.addr = alloca i8*, align 4
  %src.addr = alloca %struct.block_*, align 4
  %i = alloca i32, align 4
  store i8* %output, i8** %output.addr, align 4, !tbaa !2
  store %struct.block_* %src, %struct.block_** %src.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp ult i32 %1, 128
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %output.addr, align 4, !tbaa !2
  %3 = load i32, i32* %i, align 4, !tbaa !7
  %mul = mul i32 %3, 8
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %mul
  %4 = load %struct.block_*, %struct.block_** %src.addr, align 4, !tbaa !2
  %v = getelementptr inbounds %struct.block_, %struct.block_* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 %5
  %6 = load i64, i64* %arrayidx, align 8, !tbaa !9
  call void @store64(i8* %add.ptr, i64 %6) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #5
  ret void
}

; Function Attrs: optsize
declare i32 @blake2b_long(i8*, i32, i8*, i32) #3

; Function Attrs: noinline nounwind optsize
define hidden i32 @index_alpha(%struct.Argon2_instance_t* %instance, %struct.Argon2_position_t* %position, i32 %pseudo_rand, i32 %same_lane) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %position.addr = alloca %struct.Argon2_position_t*, align 4
  %pseudo_rand.addr = alloca i32, align 4
  %same_lane.addr = alloca i32, align 4
  %reference_area_size = alloca i32, align 4
  %relative_position = alloca i64, align 8
  %start_position = alloca i32, align 4
  %absolute_position = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  store %struct.Argon2_position_t* %position, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  store i32 %pseudo_rand, i32* %pseudo_rand.addr, align 4, !tbaa !7
  store i32 %same_lane, i32* %same_lane.addr, align 4, !tbaa !7
  %0 = bitcast i32* %reference_area_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i64* %relative_position to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #5
  %2 = bitcast i32* %start_position to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %absolute_position to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %pass = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %4, i32 0, i32 0
  %5 = load i32, i32* %pass, align 4, !tbaa !23
  %cmp = icmp eq i32 0, %5
  br i1 %cmp, label %if.then, label %if.else19

if.then:                                          ; preds = %entry
  %6 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %6, i32 0, i32 2
  %7 = load i8, i8* %slice, align 4, !tbaa !25
  %conv = zext i8 %7 to i32
  %cmp1 = icmp eq i32 0, %conv
  br i1 %cmp1, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %8 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %index = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %8, i32 0, i32 3
  %9 = load i32, i32* %index, align 4, !tbaa !26
  %sub = sub i32 %9, 1
  store i32 %sub, i32* %reference_area_size, align 4, !tbaa !7
  br label %if.end18

if.else:                                          ; preds = %if.then
  %10 = load i32, i32* %same_lane.addr, align 4, !tbaa !7
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %if.then4, label %if.else9

if.then4:                                         ; preds = %if.else
  %11 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %slice5 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %11, i32 0, i32 2
  %12 = load i8, i8* %slice5, align 4, !tbaa !25
  %conv6 = zext i8 %12 to i32
  %13 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %segment_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %13, i32 0, i32 4
  %14 = load i32, i32* %segment_length, align 4, !tbaa !27
  %mul = mul i32 %conv6, %14
  %15 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %index7 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %15, i32 0, i32 3
  %16 = load i32, i32* %index7, align 4, !tbaa !26
  %add = add i32 %mul, %16
  %sub8 = sub i32 %add, 1
  store i32 %sub8, i32* %reference_area_size, align 4, !tbaa !7
  br label %if.end

if.else9:                                         ; preds = %if.else
  %17 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %slice10 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %17, i32 0, i32 2
  %18 = load i8, i8* %slice10, align 4, !tbaa !25
  %conv11 = zext i8 %18 to i32
  %19 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %segment_length12 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %19, i32 0, i32 4
  %20 = load i32, i32* %segment_length12, align 4, !tbaa !27
  %mul13 = mul i32 %conv11, %20
  %21 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %index14 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %21, i32 0, i32 3
  %22 = load i32, i32* %index14, align 4, !tbaa !26
  %cmp15 = icmp eq i32 %22, 0
  %23 = zext i1 %cmp15 to i64
  %cond = select i1 %cmp15, i32 -1, i32 0
  %add17 = add i32 %mul13, %cond
  store i32 %add17, i32* %reference_area_size, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.else9, %if.then4
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then3
  br label %if.end37

if.else19:                                        ; preds = %entry
  %24 = load i32, i32* %same_lane.addr, align 4, !tbaa !7
  %tobool20 = icmp ne i32 %24, 0
  br i1 %tobool20, label %if.then21, label %if.else27

if.then21:                                        ; preds = %if.else19
  %25 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %25, i32 0, i32 5
  %26 = load i32, i32* %lane_length, align 4, !tbaa !18
  %27 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %segment_length22 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %27, i32 0, i32 4
  %28 = load i32, i32* %segment_length22, align 4, !tbaa !27
  %sub23 = sub i32 %26, %28
  %29 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %index24 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %29, i32 0, i32 3
  %30 = load i32, i32* %index24, align 4, !tbaa !26
  %add25 = add i32 %sub23, %30
  %sub26 = sub i32 %add25, 1
  store i32 %sub26, i32* %reference_area_size, align 4, !tbaa !7
  br label %if.end36

if.else27:                                        ; preds = %if.else19
  %31 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length28 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %31, i32 0, i32 5
  %32 = load i32, i32* %lane_length28, align 4, !tbaa !18
  %33 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %segment_length29 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %33, i32 0, i32 4
  %34 = load i32, i32* %segment_length29, align 4, !tbaa !27
  %sub30 = sub i32 %32, %34
  %35 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %index31 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %35, i32 0, i32 3
  %36 = load i32, i32* %index31, align 4, !tbaa !26
  %cmp32 = icmp eq i32 %36, 0
  %37 = zext i1 %cmp32 to i64
  %cond34 = select i1 %cmp32, i32 -1, i32 0
  %add35 = add i32 %sub30, %cond34
  store i32 %add35, i32* %reference_area_size, align 4, !tbaa !7
  br label %if.end36

if.end36:                                         ; preds = %if.else27, %if.then21
  br label %if.end37

if.end37:                                         ; preds = %if.end36, %if.end18
  %38 = load i32, i32* %pseudo_rand.addr, align 4, !tbaa !7
  %conv38 = zext i32 %38 to i64
  store i64 %conv38, i64* %relative_position, align 8, !tbaa !9
  %39 = load i64, i64* %relative_position, align 8, !tbaa !9
  %40 = load i64, i64* %relative_position, align 8, !tbaa !9
  %mul39 = mul i64 %39, %40
  %shr = lshr i64 %mul39, 32
  store i64 %shr, i64* %relative_position, align 8, !tbaa !9
  %41 = load i32, i32* %reference_area_size, align 4, !tbaa !7
  %sub40 = sub i32 %41, 1
  %conv41 = zext i32 %sub40 to i64
  %42 = load i32, i32* %reference_area_size, align 4, !tbaa !7
  %conv42 = zext i32 %42 to i64
  %43 = load i64, i64* %relative_position, align 8, !tbaa !9
  %mul43 = mul i64 %conv42, %43
  %shr44 = lshr i64 %mul43, 32
  %sub45 = sub i64 %conv41, %shr44
  store i64 %sub45, i64* %relative_position, align 8, !tbaa !9
  store i32 0, i32* %start_position, align 4, !tbaa !7
  %44 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %pass46 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %44, i32 0, i32 0
  %45 = load i32, i32* %pass46, align 4, !tbaa !23
  %cmp47 = icmp ne i32 0, %45
  br i1 %cmp47, label %if.then49, label %if.end60

if.then49:                                        ; preds = %if.end37
  %46 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %slice50 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %46, i32 0, i32 2
  %47 = load i8, i8* %slice50, align 4, !tbaa !25
  %conv51 = zext i8 %47 to i32
  %cmp52 = icmp eq i32 %conv51, 3
  br i1 %cmp52, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then49
  br label %cond.end

cond.false:                                       ; preds = %if.then49
  %48 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4, !tbaa !2
  %slice54 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %48, i32 0, i32 2
  %49 = load i8, i8* %slice54, align 4, !tbaa !25
  %conv55 = zext i8 %49 to i32
  %add56 = add nsw i32 %conv55, 1
  %50 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %segment_length57 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %50, i32 0, i32 4
  %51 = load i32, i32* %segment_length57, align 4, !tbaa !27
  %mul58 = mul i32 %add56, %51
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond59 = phi i32 [ 0, %cond.true ], [ %mul58, %cond.false ]
  store i32 %cond59, i32* %start_position, align 4, !tbaa !7
  br label %if.end60

if.end60:                                         ; preds = %cond.end, %if.end37
  %52 = load i32, i32* %start_position, align 4, !tbaa !7
  %conv61 = zext i32 %52 to i64
  %53 = load i64, i64* %relative_position, align 8, !tbaa !9
  %add62 = add i64 %conv61, %53
  %54 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length63 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %54, i32 0, i32 5
  %55 = load i32, i32* %lane_length63, align 4, !tbaa !18
  %conv64 = zext i32 %55 to i64
  %rem = urem i64 %add62, %conv64
  %conv65 = trunc i64 %rem to i32
  store i32 %conv65, i32* %absolute_position, align 4, !tbaa !7
  %56 = load i32, i32* %absolute_position, align 4, !tbaa !7
  %57 = bitcast i32* %absolute_position to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  %58 = bitcast i32* %start_position to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #5
  %59 = bitcast i64* %relative_position to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %59) #5
  %60 = bitcast i32* %reference_area_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #5
  ret i32 %56
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @fill_memory_blocks(%struct.Argon2_instance_t* %instance) #0 {
entry:
  %retval = alloca i32, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %0 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Argon2_instance_t* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %1, i32 0, i32 6
  %2 = load i32, i32* %lanes, align 4, !tbaa !19
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -25, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %threads = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %3, i32 0, i32 7
  %4 = load i32, i32* %threads, align 4, !tbaa !28
  %cmp2 = icmp eq i32 %4, 1
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %5 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %call = call i32 @fill_memory_blocks_st(%struct.Argon2_instance_t* %5) #6
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %6 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %call3 = call i32 @fill_memory_blocks_mt(%struct.Argon2_instance_t* %6) #6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call, %cond.true ], [ %call3, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optsize
define internal i32 @fill_memory_blocks_st(%struct.Argon2_instance_t* %instance) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %r = alloca i32, align 4
  %s = alloca i32, align 4
  %l = alloca i32, align 4
  %position = alloca %struct.Argon2_position_t, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %r, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %3 = load i32, i32* %r, align 4, !tbaa !7
  %4 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %passes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %4, i32 0, i32 2
  %5 = load i32, i32* %passes, align 4, !tbaa !29
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %s, align 4, !tbaa !7
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc7, %for.body
  %6 = load i32, i32* %s, align 4, !tbaa !7
  %cmp2 = icmp ult i32 %6, 4
  br i1 %cmp2, label %for.body3, label %for.end9

for.body3:                                        ; preds = %for.cond1
  store i32 0, i32* %l, align 4, !tbaa !7
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body3
  %7 = load i32, i32* %l, align 4, !tbaa !7
  %8 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %8, i32 0, i32 6
  %9 = load i32, i32* %lanes, align 4, !tbaa !19
  %cmp5 = icmp ult i32 %7, %9
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %10 = bitcast %struct.Argon2_position_t* %position to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #5
  %pass = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %11 = load i32, i32* %r, align 4, !tbaa !7
  store i32 %11, i32* %pass, align 4, !tbaa !23
  %lane = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %12 = load i32, i32* %l, align 4, !tbaa !7
  store i32 %12, i32* %lane, align 4, !tbaa !30
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %13 = load i32, i32* %s, align 4, !tbaa !7
  %conv = trunc i32 %13 to i8
  store i8 %conv, i8* %slice, align 4, !tbaa !25
  %index = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 3
  store i32 0, i32* %index, align 4, !tbaa !26
  %14 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  call void @fill_segment(%struct.Argon2_instance_t* %14, %struct.Argon2_position_t* byval(%struct.Argon2_position_t) align 4 %position) #6
  %15 = bitcast %struct.Argon2_position_t* %position to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #5
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %16 = load i32, i32* %l, align 4, !tbaa !7
  %inc = add i32 %16, 1
  store i32 %inc, i32* %l, align 4, !tbaa !7
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %for.inc7

for.inc7:                                         ; preds = %for.end
  %17 = load i32, i32* %s, align 4, !tbaa !7
  %inc8 = add i32 %17, 1
  store i32 %inc8, i32* %s, align 4, !tbaa !7
  br label %for.cond1

for.end9:                                         ; preds = %for.cond1
  br label %for.inc10

for.inc10:                                        ; preds = %for.end9
  %18 = load i32, i32* %r, align 4, !tbaa !7
  %inc11 = add i32 %18, 1
  store i32 %inc11, i32* %r, align 4, !tbaa !7
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  %19 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  ret i32 0
}

; Function Attrs: noinline nounwind optsize
define internal i32 @fill_memory_blocks_mt(%struct.Argon2_instance_t* %instance) #0 {
entry:
  %retval = alloca i32, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %r = alloca i32, align 4
  %s = alloca i32, align 4
  %thread = alloca %struct.__pthread**, align 4
  %thr_data = alloca %struct.Argon2_thread_data*, align 4
  %rc = alloca i32, align 4
  %l = alloca i32, align 4
  %ll = alloca i32, align 4
  %position = alloca %struct.Argon2_position_t, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast %struct.__pthread*** %thread to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store %struct.__pthread** null, %struct.__pthread*** %thread, align 4, !tbaa !2
  %3 = bitcast %struct.Argon2_thread_data** %thr_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store %struct.Argon2_thread_data* null, %struct.Argon2_thread_data** %thr_data, align 4, !tbaa !2
  %4 = bitcast i32* %rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 0, i32* %rc, align 4, !tbaa !7
  %5 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %5, i32 0, i32 6
  %6 = load i32, i32* %lanes, align 4, !tbaa !19
  %call = call i8* @calloc(i32 %6, i32 4) #6
  %7 = bitcast i8* %call to %struct.__pthread**
  store %struct.__pthread** %7, %struct.__pthread*** %thread, align 4, !tbaa !2
  %8 = load %struct.__pthread**, %struct.__pthread*** %thread, align 4, !tbaa !2
  %cmp = icmp eq %struct.__pthread** %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -22, i32* %rc, align 4, !tbaa !7
  br label %fail

if.end:                                           ; preds = %entry
  %9 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes1 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %9, i32 0, i32 6
  %10 = load i32, i32* %lanes1, align 4, !tbaa !19
  %call2 = call i8* @calloc(i32 %10, i32 20) #6
  %11 = bitcast i8* %call2 to %struct.Argon2_thread_data*
  store %struct.Argon2_thread_data* %11, %struct.Argon2_thread_data** %thr_data, align 4, !tbaa !2
  %12 = load %struct.Argon2_thread_data*, %struct.Argon2_thread_data** %thr_data, align 4, !tbaa !2
  %cmp3 = icmp eq %struct.Argon2_thread_data* %12, null
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i32 -22, i32* %rc, align 4, !tbaa !7
  br label %fail

if.end5:                                          ; preds = %if.end
  store i32 0, i32* %r, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc46, %if.end5
  %13 = load i32, i32* %r, align 4, !tbaa !7
  %14 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %passes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %14, i32 0, i32 2
  %15 = load i32, i32* %passes, align 4, !tbaa !29
  %cmp6 = icmp ult i32 %13, %15
  br i1 %cmp6, label %for.body, label %for.end48

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %s, align 4, !tbaa !7
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc43, %for.body
  %16 = load i32, i32* %s, align 4, !tbaa !7
  %cmp8 = icmp ult i32 %16, 4
  br i1 %cmp8, label %for.body9, label %for.end45

for.body9:                                        ; preds = %for.cond7
  %17 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %ll to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  store i32 0, i32* %l, align 4, !tbaa !7
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body9
  %19 = load i32, i32* %l, align 4, !tbaa !7
  %20 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes11 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %20, i32 0, i32 6
  %21 = load i32, i32* %lanes11, align 4, !tbaa !19
  %cmp12 = icmp ult i32 %19, %21
  br i1 %cmp12, label %for.body13, label %for.end

for.body13:                                       ; preds = %for.cond10
  %22 = bitcast %struct.Argon2_position_t* %position to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #5
  %23 = load i32, i32* %l, align 4, !tbaa !7
  %24 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %threads = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %24, i32 0, i32 7
  %25 = load i32, i32* %threads, align 4, !tbaa !28
  %cmp14 = icmp uge i32 %23, %25
  br i1 %cmp14, label %if.then15, label %if.end20

if.then15:                                        ; preds = %for.body13
  %26 = load %struct.__pthread**, %struct.__pthread*** %thread, align 4, !tbaa !2
  %27 = load i32, i32* %l, align 4, !tbaa !7
  %28 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %threads16 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %28, i32 0, i32 7
  %29 = load i32, i32* %threads16, align 4, !tbaa !28
  %sub = sub i32 %27, %29
  %arrayidx = getelementptr inbounds %struct.__pthread*, %struct.__pthread** %26, i32 %sub
  %30 = load %struct.__pthread*, %struct.__pthread** %arrayidx, align 4, !tbaa !2
  %call17 = call i32 @argon2_thread_join(%struct.__pthread* %30) #6
  %tobool = icmp ne i32 %call17, 0
  br i1 %tobool, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.then15
  store i32 -33, i32* %rc, align 4, !tbaa !7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.then15
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %for.body13
  %31 = load i32, i32* %r, align 4, !tbaa !7
  %pass = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  store i32 %31, i32* %pass, align 4, !tbaa !23
  %32 = load i32, i32* %l, align 4, !tbaa !7
  %lane = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  store i32 %32, i32* %lane, align 4, !tbaa !30
  %33 = load i32, i32* %s, align 4, !tbaa !7
  %conv = trunc i32 %33 to i8
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  store i8 %conv, i8* %slice, align 4, !tbaa !25
  %index = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 3
  store i32 0, i32* %index, align 4, !tbaa !26
  %34 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %35 = load %struct.Argon2_thread_data*, %struct.Argon2_thread_data** %thr_data, align 4, !tbaa !2
  %36 = load i32, i32* %l, align 4, !tbaa !7
  %arrayidx21 = getelementptr inbounds %struct.Argon2_thread_data, %struct.Argon2_thread_data* %35, i32 %36
  %instance_ptr = getelementptr inbounds %struct.Argon2_thread_data, %struct.Argon2_thread_data* %arrayidx21, i32 0, i32 0
  store %struct.Argon2_instance_t* %34, %struct.Argon2_instance_t** %instance_ptr, align 4, !tbaa !31
  %37 = load %struct.Argon2_thread_data*, %struct.Argon2_thread_data** %thr_data, align 4, !tbaa !2
  %38 = load i32, i32* %l, align 4, !tbaa !7
  %arrayidx22 = getelementptr inbounds %struct.Argon2_thread_data, %struct.Argon2_thread_data* %37, i32 %38
  %pos = getelementptr inbounds %struct.Argon2_thread_data, %struct.Argon2_thread_data* %arrayidx22, i32 0, i32 1
  %39 = bitcast %struct.Argon2_position_t* %pos to i8*
  %40 = bitcast %struct.Argon2_position_t* %position to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 16, i1 false)
  %41 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  call void @fill_segment(%struct.Argon2_instance_t* %41, %struct.Argon2_position_t* byval(%struct.Argon2_position_t) align 4 %position) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then18, %if.end20
  %42 = bitcast %struct.Argon2_position_t* %position to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup39 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %43 = load i32, i32* %l, align 4, !tbaa !7
  %inc = add i32 %43, 1
  store i32 %inc, i32* %l, align 4, !tbaa !7
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  %44 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes23 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %44, i32 0, i32 6
  %45 = load i32, i32* %lanes23, align 4, !tbaa !19
  %46 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %threads24 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %46, i32 0, i32 7
  %47 = load i32, i32* %threads24, align 4, !tbaa !28
  %sub25 = sub i32 %45, %47
  store i32 %sub25, i32* %l, align 4, !tbaa !7
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc36, %for.end
  %48 = load i32, i32* %l, align 4, !tbaa !7
  %49 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes27 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %49, i32 0, i32 6
  %50 = load i32, i32* %lanes27, align 4, !tbaa !19
  %cmp28 = icmp ult i32 %48, %50
  br i1 %cmp28, label %for.body30, label %for.end38

for.body30:                                       ; preds = %for.cond26
  %51 = load %struct.__pthread**, %struct.__pthread*** %thread, align 4, !tbaa !2
  %52 = load i32, i32* %l, align 4, !tbaa !7
  %arrayidx31 = getelementptr inbounds %struct.__pthread*, %struct.__pthread** %51, i32 %52
  %53 = load %struct.__pthread*, %struct.__pthread** %arrayidx31, align 4, !tbaa !2
  %call32 = call i32 @argon2_thread_join(%struct.__pthread* %53) #6
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %for.body30
  store i32 -33, i32* %rc, align 4, !tbaa !7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup39

if.end35:                                         ; preds = %for.body30
  br label %for.inc36

for.inc36:                                        ; preds = %if.end35
  %54 = load i32, i32* %l, align 4, !tbaa !7
  %inc37 = add i32 %54, 1
  store i32 %inc37, i32* %l, align 4, !tbaa !7
  br label %for.cond26

for.end38:                                        ; preds = %for.cond26
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup39

cleanup39:                                        ; preds = %if.then34, %for.end38, %cleanup
  %55 = bitcast i32* %ll to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %56 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  %cleanup.dest41 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest41, label %cleanup57 [
    i32 0, label %cleanup.cont42
    i32 2, label %fail
  ]

cleanup.cont42:                                   ; preds = %cleanup39
  br label %for.inc43

for.inc43:                                        ; preds = %cleanup.cont42
  %57 = load i32, i32* %s, align 4, !tbaa !7
  %inc44 = add i32 %57, 1
  store i32 %inc44, i32* %s, align 4, !tbaa !7
  br label %for.cond7

for.end45:                                        ; preds = %for.cond7
  br label %for.inc46

for.inc46:                                        ; preds = %for.end45
  %58 = load i32, i32* %r, align 4, !tbaa !7
  %inc47 = add i32 %58, 1
  store i32 %inc47, i32* %r, align 4, !tbaa !7
  br label %for.cond

for.end48:                                        ; preds = %for.cond
  br label %fail

fail:                                             ; preds = %for.end48, %cleanup39, %if.then4, %if.then
  %59 = load %struct.__pthread**, %struct.__pthread*** %thread, align 4, !tbaa !2
  %cmp49 = icmp ne %struct.__pthread** %59, null
  br i1 %cmp49, label %if.then51, label %if.end52

if.then51:                                        ; preds = %fail
  %60 = load %struct.__pthread**, %struct.__pthread*** %thread, align 4, !tbaa !2
  %61 = bitcast %struct.__pthread** %60 to i8*
  call void @free(i8* %61) #6
  br label %if.end52

if.end52:                                         ; preds = %if.then51, %fail
  %62 = load %struct.Argon2_thread_data*, %struct.Argon2_thread_data** %thr_data, align 4, !tbaa !2
  %cmp53 = icmp ne %struct.Argon2_thread_data* %62, null
  br i1 %cmp53, label %if.then55, label %if.end56

if.then55:                                        ; preds = %if.end52
  %63 = load %struct.Argon2_thread_data*, %struct.Argon2_thread_data** %thr_data, align 4, !tbaa !2
  %64 = bitcast %struct.Argon2_thread_data* %63 to i8*
  call void @free(i8* %64) #6
  br label %if.end56

if.end56:                                         ; preds = %if.then55, %if.end52
  %65 = load i32, i32* %rc, align 4, !tbaa !7
  store i32 %65, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup57

cleanup57:                                        ; preds = %if.end56, %cleanup39
  %66 = bitcast i32* %rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast %struct.Argon2_thread_data** %thr_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast %struct.__pthread*** %thread to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %69 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  %70 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #5
  %71 = load i32, i32* %retval, align 4
  ret i32 %71
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @validate_inputs(%struct.Argon2_Context* %context) #0 {
entry:
  %retval = alloca i32, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Argon2_Context* null, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -25, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %1, i32 0, i32 0
  %2 = load i8*, i8** %out, align 4, !tbaa !20
  %cmp1 = icmp eq i8* null, %2
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %3, i32 0, i32 1
  %4 = load i32, i32* %outlen, align 4, !tbaa !21
  %cmp4 = icmp ugt i32 4, %4
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -2, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %5 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %outlen7 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %5, i32 0, i32 1
  %6 = load i32, i32* %outlen7, align 4, !tbaa !21
  %cmp8 = icmp ult i32 -1, %6
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end6
  store i32 -3, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.end6
  %7 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwd = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %7, i32 0, i32 2
  %8 = load i8*, i8** %pwd, align 4, !tbaa !33
  %cmp11 = icmp eq i8* null, %8
  br i1 %cmp11, label %if.then12, label %if.end16

if.then12:                                        ; preds = %if.end10
  %9 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwdlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %9, i32 0, i32 3
  %10 = load i32, i32* %pwdlen, align 4, !tbaa !34
  %cmp13 = icmp ne i32 0, %10
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 -18, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.end10
  %11 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwdlen17 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %11, i32 0, i32 3
  %12 = load i32, i32* %pwdlen17, align 4, !tbaa !34
  %cmp18 = icmp ugt i32 0, %12
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end16
  store i32 -4, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.end16
  %13 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwdlen21 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %13, i32 0, i32 3
  %14 = load i32, i32* %pwdlen21, align 4, !tbaa !34
  %cmp22 = icmp ult i32 -1, %14
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end20
  store i32 -5, i32* %retval, align 4
  br label %return

if.end24:                                         ; preds = %if.end20
  %15 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %15, i32 0, i32 4
  %16 = load i8*, i8** %salt, align 4, !tbaa !35
  %cmp25 = icmp eq i8* null, %16
  br i1 %cmp25, label %if.then26, label %if.end30

if.then26:                                        ; preds = %if.end24
  %17 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %17, i32 0, i32 5
  %18 = load i32, i32* %saltlen, align 4, !tbaa !36
  %cmp27 = icmp ne i32 0, %18
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.then26
  store i32 -19, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %if.then26
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.end24
  %19 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %saltlen31 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %19, i32 0, i32 5
  %20 = load i32, i32* %saltlen31, align 4, !tbaa !36
  %cmp32 = icmp ugt i32 8, %20
  br i1 %cmp32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %if.end30
  store i32 -6, i32* %retval, align 4
  br label %return

if.end34:                                         ; preds = %if.end30
  %21 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %saltlen35 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %21, i32 0, i32 5
  %22 = load i32, i32* %saltlen35, align 4, !tbaa !36
  %cmp36 = icmp ult i32 -1, %22
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.end34
  store i32 -7, i32* %retval, align 4
  br label %return

if.end38:                                         ; preds = %if.end34
  %23 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %23, i32 0, i32 6
  %24 = load i8*, i8** %secret, align 4, !tbaa !37
  %cmp39 = icmp eq i8* null, %24
  br i1 %cmp39, label %if.then40, label %if.else

if.then40:                                        ; preds = %if.end38
  %25 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %25, i32 0, i32 7
  %26 = load i32, i32* %secretlen, align 4, !tbaa !38
  %cmp41 = icmp ne i32 0, %26
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.then40
  store i32 -20, i32* %retval, align 4
  br label %return

if.end43:                                         ; preds = %if.then40
  br label %if.end52

if.else:                                          ; preds = %if.end38
  %27 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secretlen44 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %27, i32 0, i32 7
  %28 = load i32, i32* %secretlen44, align 4, !tbaa !38
  %cmp45 = icmp ugt i32 0, %28
  br i1 %cmp45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.else
  store i32 -10, i32* %retval, align 4
  br label %return

if.end47:                                         ; preds = %if.else
  %29 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secretlen48 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %29, i32 0, i32 7
  %30 = load i32, i32* %secretlen48, align 4, !tbaa !38
  %cmp49 = icmp ult i32 -1, %30
  br i1 %cmp49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end47
  store i32 -11, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.end47
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end43
  %31 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %31, i32 0, i32 8
  %32 = load i8*, i8** %ad, align 4, !tbaa !39
  %cmp53 = icmp eq i8* null, %32
  br i1 %cmp53, label %if.then54, label %if.else58

if.then54:                                        ; preds = %if.end52
  %33 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %33, i32 0, i32 9
  %34 = load i32, i32* %adlen, align 4, !tbaa !40
  %cmp55 = icmp ne i32 0, %34
  br i1 %cmp55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %if.then54
  store i32 -21, i32* %retval, align 4
  br label %return

if.end57:                                         ; preds = %if.then54
  br label %if.end67

if.else58:                                        ; preds = %if.end52
  %35 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %adlen59 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %35, i32 0, i32 9
  %36 = load i32, i32* %adlen59, align 4, !tbaa !40
  %cmp60 = icmp ugt i32 0, %36
  br i1 %cmp60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %if.else58
  store i32 -8, i32* %retval, align 4
  br label %return

if.end62:                                         ; preds = %if.else58
  %37 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %adlen63 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %37, i32 0, i32 9
  %38 = load i32, i32* %adlen63, align 4, !tbaa !40
  %cmp64 = icmp ult i32 -1, %38
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end62
  store i32 -9, i32* %retval, align 4
  br label %return

if.end66:                                         ; preds = %if.end62
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %if.end57
  %39 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %39, i32 0, i32 11
  %40 = load i32, i32* %m_cost, align 4, !tbaa !41
  %cmp68 = icmp ugt i32 8, %40
  br i1 %cmp68, label %if.then69, label %if.end70

if.then69:                                        ; preds = %if.end67
  store i32 -14, i32* %retval, align 4
  br label %return

if.end70:                                         ; preds = %if.end67
  %41 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %m_cost71 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %41, i32 0, i32 11
  %42 = load i32, i32* %m_cost71, align 4, !tbaa !41
  %conv = zext i32 %42 to i64
  %cmp72 = icmp ult i64 2097152, %conv
  br i1 %cmp72, label %if.then74, label %if.end75

if.then74:                                        ; preds = %if.end70
  store i32 -15, i32* %retval, align 4
  br label %return

if.end75:                                         ; preds = %if.end70
  %43 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %m_cost76 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %43, i32 0, i32 11
  %44 = load i32, i32* %m_cost76, align 4, !tbaa !41
  %45 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %45, i32 0, i32 12
  %46 = load i32, i32* %lanes, align 4, !tbaa !42
  %mul = mul i32 8, %46
  %cmp77 = icmp ult i32 %44, %mul
  br i1 %cmp77, label %if.then79, label %if.end80

if.then79:                                        ; preds = %if.end75
  store i32 -14, i32* %retval, align 4
  br label %return

if.end80:                                         ; preds = %if.end75
  %47 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %47, i32 0, i32 10
  %48 = load i32, i32* %t_cost, align 4, !tbaa !43
  %cmp81 = icmp ugt i32 1, %48
  br i1 %cmp81, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.end80
  store i32 -12, i32* %retval, align 4
  br label %return

if.end84:                                         ; preds = %if.end80
  %49 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %t_cost85 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %49, i32 0, i32 10
  %50 = load i32, i32* %t_cost85, align 4, !tbaa !43
  %cmp86 = icmp ult i32 -1, %50
  br i1 %cmp86, label %if.then88, label %if.end89

if.then88:                                        ; preds = %if.end84
  store i32 -13, i32* %retval, align 4
  br label %return

if.end89:                                         ; preds = %if.end84
  %51 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %lanes90 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %51, i32 0, i32 12
  %52 = load i32, i32* %lanes90, align 4, !tbaa !42
  %cmp91 = icmp ugt i32 1, %52
  br i1 %cmp91, label %if.then93, label %if.end94

if.then93:                                        ; preds = %if.end89
  store i32 -16, i32* %retval, align 4
  br label %return

if.end94:                                         ; preds = %if.end89
  %53 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %lanes95 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %53, i32 0, i32 12
  %54 = load i32, i32* %lanes95, align 4, !tbaa !42
  %cmp96 = icmp ult i32 16777215, %54
  br i1 %cmp96, label %if.then98, label %if.end99

if.then98:                                        ; preds = %if.end94
  store i32 -17, i32* %retval, align 4
  br label %return

if.end99:                                         ; preds = %if.end94
  %55 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %55, i32 0, i32 13
  %56 = load i32, i32* %threads, align 4, !tbaa !44
  %cmp100 = icmp ugt i32 1, %56
  br i1 %cmp100, label %if.then102, label %if.end103

if.then102:                                       ; preds = %if.end99
  store i32 -28, i32* %retval, align 4
  br label %return

if.end103:                                        ; preds = %if.end99
  %57 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %threads104 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %57, i32 0, i32 13
  %58 = load i32, i32* %threads104, align 4, !tbaa !44
  %cmp105 = icmp ult i32 16777215, %58
  br i1 %cmp105, label %if.then107, label %if.end108

if.then107:                                       ; preds = %if.end103
  store i32 -29, i32* %retval, align 4
  br label %return

if.end108:                                        ; preds = %if.end103
  %59 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %allocate_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %59, i32 0, i32 15
  %60 = load i32 (i8**, i32)*, i32 (i8**, i32)** %allocate_cbk, align 4, !tbaa !13
  %cmp109 = icmp ne i32 (i8**, i32)* null, %60
  br i1 %cmp109, label %land.lhs.true, label %if.end114

land.lhs.true:                                    ; preds = %if.end108
  %61 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %free_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %61, i32 0, i32 16
  %62 = load void (i8*, i32)*, void (i8*, i32)** %free_cbk, align 4, !tbaa !15
  %cmp111 = icmp eq void (i8*, i32)* null, %62
  br i1 %cmp111, label %if.then113, label %if.end114

if.then113:                                       ; preds = %land.lhs.true
  store i32 -23, i32* %retval, align 4
  br label %return

if.end114:                                        ; preds = %land.lhs.true, %if.end108
  %63 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %allocate_cbk115 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %63, i32 0, i32 15
  %64 = load i32 (i8**, i32)*, i32 (i8**, i32)** %allocate_cbk115, align 4, !tbaa !13
  %cmp116 = icmp eq i32 (i8**, i32)* null, %64
  br i1 %cmp116, label %land.lhs.true118, label %if.end123

land.lhs.true118:                                 ; preds = %if.end114
  %65 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %free_cbk119 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %65, i32 0, i32 16
  %66 = load void (i8*, i32)*, void (i8*, i32)** %free_cbk119, align 4, !tbaa !15
  %cmp120 = icmp ne void (i8*, i32)* null, %66
  br i1 %cmp120, label %if.then122, label %if.end123

if.then122:                                       ; preds = %land.lhs.true118
  store i32 -24, i32* %retval, align 4
  br label %return

if.end123:                                        ; preds = %land.lhs.true118, %if.end114
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end123, %if.then122, %if.then113, %if.then107, %if.then102, %if.then98, %if.then93, %if.then88, %if.then83, %if.then79, %if.then74, %if.then69, %if.then65, %if.then61, %if.then56, %if.then50, %if.then46, %if.then42, %if.then37, %if.then33, %if.then28, %if.then23, %if.then19, %if.then14, %if.then9, %if.then5, %if.then2, %if.then
  %67 = load i32, i32* %retval, align 4
  ret i32 %67
}

; Function Attrs: noinline nounwind optsize
define hidden void @fill_first_blocks(i8* %blockhash, %struct.Argon2_instance_t* %instance) #0 {
entry:
  %blockhash.addr = alloca i8*, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %l = alloca i32, align 4
  %blockhash_bytes = alloca [1024 x i8], align 16
  store i8* %blockhash, i8** %blockhash.addr, align 4, !tbaa !2
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast [1024 x i8]* %blockhash_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %1) #5
  store i32 0, i32* %l, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %l, align 4, !tbaa !7
  %3 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %3, i32 0, i32 6
  %4 = load i32, i32* %lanes, align 4, !tbaa !19
  %cmp = icmp ult i32 %2, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i8*, i8** %blockhash.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 64
  call void @store32(i8* %add.ptr, i32 0) #6
  %6 = load i8*, i8** %blockhash.addr, align 4, !tbaa !2
  %add.ptr1 = getelementptr inbounds i8, i8* %6, i32 64
  %add.ptr2 = getelementptr inbounds i8, i8* %add.ptr1, i32 4
  %7 = load i32, i32* %l, align 4, !tbaa !7
  call void @store32(i8* %add.ptr2, i32 %7) #6
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  %8 = load i8*, i8** %blockhash.addr, align 4, !tbaa !2
  %call = call i32 @blake2b_long(i8* %arraydecay, i32 1024, i8* %8, i32 72) #6
  %9 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %9, i32 0, i32 0
  %10 = load %struct.block_*, %struct.block_** %memory, align 4, !tbaa !16
  %11 = load i32, i32* %l, align 4, !tbaa !7
  %12 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %12, i32 0, i32 5
  %13 = load i32, i32* %lane_length, align 4, !tbaa !18
  %mul = mul i32 %11, %13
  %add = add i32 %mul, 0
  %arrayidx = getelementptr inbounds %struct.block_, %struct.block_* %10, i32 %add
  %arraydecay3 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @load_block(%struct.block_* %arrayidx, i8* %arraydecay3) #6
  %14 = load i8*, i8** %blockhash.addr, align 4, !tbaa !2
  %add.ptr4 = getelementptr inbounds i8, i8* %14, i32 64
  call void @store32(i8* %add.ptr4, i32 1) #6
  %arraydecay5 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  %15 = load i8*, i8** %blockhash.addr, align 4, !tbaa !2
  %call6 = call i32 @blake2b_long(i8* %arraydecay5, i32 1024, i8* %15, i32 72) #6
  %16 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory7 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %16, i32 0, i32 0
  %17 = load %struct.block_*, %struct.block_** %memory7, align 4, !tbaa !16
  %18 = load i32, i32* %l, align 4, !tbaa !7
  %19 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length8 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %19, i32 0, i32 5
  %20 = load i32, i32* %lane_length8, align 4, !tbaa !18
  %mul9 = mul i32 %18, %20
  %add10 = add i32 %mul9, 1
  %arrayidx11 = getelementptr inbounds %struct.block_, %struct.block_* %17, i32 %add10
  %arraydecay12 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @load_block(%struct.block_* %arrayidx11, i8* %arraydecay12) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %l, align 4, !tbaa !7
  %inc = add i32 %21, 1
  store i32 %inc, i32* %l, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay13 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @clear_internal_memory(i8* %arraydecay13, i32 1024) #6
  %22 = bitcast [1024 x i8]* %blockhash_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %22) #5
  %23 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  ret void
}

; Function Attrs: noinline nounwind optsize
define internal void @store32(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %w, i32* %w.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optsize
define internal void @load_block(%struct.block_* %dst, i8* %input) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %input.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4, !tbaa !2
  store i8* %input, i8** %input.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp ult i32 %1, 128
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %input.addr, align 4, !tbaa !2
  %3 = load i32, i32* %i, align 4, !tbaa !7
  %mul = mul i32 %3, 8
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %mul
  %call = call i64 @load64(i8* %add.ptr) #6
  %4 = load %struct.block_*, %struct.block_** %dst.addr, align 4, !tbaa !2
  %v = getelementptr inbounds %struct.block_, %struct.block_* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 %5
  store i64 %call, i64* %arrayidx, align 8, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  ret void
}

; Function Attrs: noinline nounwind optsize
define hidden void @initial_hash(i8* %blockhash, %struct.Argon2_Context* %context, i32 %type) #0 {
entry:
  %blockhash.addr = alloca i8*, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %type.addr = alloca i32, align 4
  %BlakeHash = alloca %struct.__blake2b_state, align 8
  %value = alloca [4 x i8], align 1
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %blockhash, i8** %blockhash.addr, align 4, !tbaa !2
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  store i32 %type, i32* %type.addr, align 4, !tbaa !6
  %0 = bitcast %struct.__blake2b_state* %BlakeHash to i8*
  call void @llvm.lifetime.start.p0i8(i64 240, i8* %0) #5
  %1 = bitcast [4 x i8]* %value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Argon2_Context* null, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load i8*, i8** %blockhash.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i8* null, %3
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %call = call i32 @blake2b_init(%struct.__blake2b_state* %BlakeHash, i32 64) #6
  %4 = bitcast [4 x i8]* %value to i8*
  %5 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %5, i32 0, i32 12
  %6 = load i32, i32* %lanes, align 4, !tbaa !42
  call void @store32(i8* %4, i32 %6) #6
  %7 = bitcast [4 x i8]* %value to i8*
  %call2 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %7, i32 4) #6
  %8 = bitcast [4 x i8]* %value to i8*
  %9 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %9, i32 0, i32 1
  %10 = load i32, i32* %outlen, align 4, !tbaa !21
  call void @store32(i8* %8, i32 %10) #6
  %11 = bitcast [4 x i8]* %value to i8*
  %call3 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %11, i32 4) #6
  %12 = bitcast [4 x i8]* %value to i8*
  %13 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %13, i32 0, i32 11
  %14 = load i32, i32* %m_cost, align 4, !tbaa !41
  call void @store32(i8* %12, i32 %14) #6
  %15 = bitcast [4 x i8]* %value to i8*
  %call4 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %15, i32 4) #6
  %16 = bitcast [4 x i8]* %value to i8*
  %17 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %17, i32 0, i32 10
  %18 = load i32, i32* %t_cost, align 4, !tbaa !43
  call void @store32(i8* %16, i32 %18) #6
  %19 = bitcast [4 x i8]* %value to i8*
  %call5 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %19, i32 4) #6
  %20 = bitcast [4 x i8]* %value to i8*
  %21 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %version = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %21, i32 0, i32 14
  %22 = load i32, i32* %version, align 4, !tbaa !45
  call void @store32(i8* %20, i32 %22) #6
  %23 = bitcast [4 x i8]* %value to i8*
  %call6 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %23, i32 4) #6
  %24 = bitcast [4 x i8]* %value to i8*
  %25 = load i32, i32* %type.addr, align 4, !tbaa !6
  call void @store32(i8* %24, i32 %25) #6
  %26 = bitcast [4 x i8]* %value to i8*
  %call7 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %26, i32 4) #6
  %27 = bitcast [4 x i8]* %value to i8*
  %28 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwdlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %28, i32 0, i32 3
  %29 = load i32, i32* %pwdlen, align 4, !tbaa !34
  call void @store32(i8* %27, i32 %29) #6
  %30 = bitcast [4 x i8]* %value to i8*
  %call8 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %30, i32 4) #6
  %31 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwd = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %31, i32 0, i32 2
  %32 = load i8*, i8** %pwd, align 4, !tbaa !33
  %cmp9 = icmp ne i8* %32, null
  br i1 %cmp9, label %if.then10, label %if.end19

if.then10:                                        ; preds = %if.end
  %33 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwd11 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %33, i32 0, i32 2
  %34 = load i8*, i8** %pwd11, align 4, !tbaa !33
  %35 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwdlen12 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %35, i32 0, i32 3
  %36 = load i32, i32* %pwdlen12, align 4, !tbaa !34
  %call13 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %34, i32 %36) #6
  %37 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %37, i32 0, i32 17
  %38 = load i32, i32* %flags, align 4, !tbaa !46
  %and = and i32 %38, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then14, label %if.end18

if.then14:                                        ; preds = %if.then10
  %39 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwd15 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %39, i32 0, i32 2
  %40 = load i8*, i8** %pwd15, align 4, !tbaa !33
  %41 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwdlen16 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %41, i32 0, i32 3
  %42 = load i32, i32* %pwdlen16, align 4, !tbaa !34
  call void @secure_wipe_memory(i8* %40, i32 %42)
  %43 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %pwdlen17 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %43, i32 0, i32 3
  store i32 0, i32* %pwdlen17, align 4, !tbaa !34
  br label %if.end18

if.end18:                                         ; preds = %if.then14, %if.then10
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.end
  %44 = bitcast [4 x i8]* %value to i8*
  %45 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %45, i32 0, i32 5
  %46 = load i32, i32* %saltlen, align 4, !tbaa !36
  call void @store32(i8* %44, i32 %46) #6
  %47 = bitcast [4 x i8]* %value to i8*
  %call20 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %47, i32 4) #6
  %48 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %48, i32 0, i32 4
  %49 = load i8*, i8** %salt, align 4, !tbaa !35
  %cmp21 = icmp ne i8* %49, null
  br i1 %cmp21, label %if.then22, label %if.end26

if.then22:                                        ; preds = %if.end19
  %50 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %salt23 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %50, i32 0, i32 4
  %51 = load i8*, i8** %salt23, align 4, !tbaa !35
  %52 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %saltlen24 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %52, i32 0, i32 5
  %53 = load i32, i32* %saltlen24, align 4, !tbaa !36
  %call25 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %51, i32 %53) #6
  br label %if.end26

if.end26:                                         ; preds = %if.then22, %if.end19
  %54 = bitcast [4 x i8]* %value to i8*
  %55 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %55, i32 0, i32 7
  %56 = load i32, i32* %secretlen, align 4, !tbaa !38
  call void @store32(i8* %54, i32 %56) #6
  %57 = bitcast [4 x i8]* %value to i8*
  %call27 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %57, i32 4) #6
  %58 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %58, i32 0, i32 6
  %59 = load i8*, i8** %secret, align 4, !tbaa !37
  %cmp28 = icmp ne i8* %59, null
  br i1 %cmp28, label %if.then29, label %if.end41

if.then29:                                        ; preds = %if.end26
  %60 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secret30 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %60, i32 0, i32 6
  %61 = load i8*, i8** %secret30, align 4, !tbaa !37
  %62 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secretlen31 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %62, i32 0, i32 7
  %63 = load i32, i32* %secretlen31, align 4, !tbaa !38
  %call32 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %61, i32 %63) #6
  %64 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %flags33 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %64, i32 0, i32 17
  %65 = load i32, i32* %flags33, align 4, !tbaa !46
  %and34 = and i32 %65, 2
  %tobool35 = icmp ne i32 %and34, 0
  br i1 %tobool35, label %if.then36, label %if.end40

if.then36:                                        ; preds = %if.then29
  %66 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secret37 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %66, i32 0, i32 6
  %67 = load i8*, i8** %secret37, align 4, !tbaa !37
  %68 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secretlen38 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %68, i32 0, i32 7
  %69 = load i32, i32* %secretlen38, align 4, !tbaa !38
  call void @secure_wipe_memory(i8* %67, i32 %69)
  %70 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %secretlen39 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %70, i32 0, i32 7
  store i32 0, i32* %secretlen39, align 4, !tbaa !38
  br label %if.end40

if.end40:                                         ; preds = %if.then36, %if.then29
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.end26
  %71 = bitcast [4 x i8]* %value to i8*
  %72 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %72, i32 0, i32 9
  %73 = load i32, i32* %adlen, align 4, !tbaa !40
  call void @store32(i8* %71, i32 %73) #6
  %74 = bitcast [4 x i8]* %value to i8*
  %call42 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %74, i32 4) #6
  %75 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %75, i32 0, i32 8
  %76 = load i8*, i8** %ad, align 4, !tbaa !39
  %cmp43 = icmp ne i8* %76, null
  br i1 %cmp43, label %if.then44, label %if.end48

if.then44:                                        ; preds = %if.end41
  %77 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %ad45 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %77, i32 0, i32 8
  %78 = load i8*, i8** %ad45, align 4, !tbaa !39
  %79 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %adlen46 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %79, i32 0, i32 9
  %80 = load i32, i32* %adlen46, align 4, !tbaa !40
  %call47 = call i32 @blake2b_update(%struct.__blake2b_state* %BlakeHash, i8* %78, i32 %80) #6
  br label %if.end48

if.end48:                                         ; preds = %if.then44, %if.end41
  %81 = load i8*, i8** %blockhash.addr, align 4, !tbaa !2
  %call49 = call i32 @blake2b_final(%struct.__blake2b_state* %BlakeHash, i8* %81, i32 64) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end48, %if.then
  %82 = bitcast [4 x i8]* %value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #5
  %83 = bitcast %struct.__blake2b_state* %BlakeHash to i8*
  call void @llvm.lifetime.end.p0i8(i64 240, i8* %83) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: optsize
declare i32 @blake2b_init(%struct.__blake2b_state*, i32) #3

; Function Attrs: optsize
declare i32 @blake2b_update(%struct.__blake2b_state*, i8*, i32) #3

; Function Attrs: optsize
declare i32 @blake2b_final(%struct.__blake2b_state*, i8*, i32) #3

; Function Attrs: noinline nounwind optsize
define hidden i32 @initialize(%struct.Argon2_instance_t* %instance, %struct.Argon2_Context* %context) #0 {
entry:
  %retval = alloca i32, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %blockhash = alloca [72 x i8], align 16
  %result = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %0 = bitcast [72 x i8]* %blockhash to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %0) #5
  %1 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %result, align 4, !tbaa !7
  %2 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Argon2_instance_t* %2, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %cmp1 = icmp eq %struct.Argon2_Context* %3, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -25, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %4 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %5 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %context_ptr = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %5, i32 0, i32 10
  store %struct.Argon2_Context* %4, %struct.Argon2_Context** %context_ptr, align 4, !tbaa !47
  %6 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %7 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %7, i32 0, i32 0
  %8 = bitcast %struct.block_** %memory to i8**
  %9 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory_blocks = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %9, i32 0, i32 3
  %10 = load i32, i32* %memory_blocks, align 4, !tbaa !22
  %call = call i32 @allocate_memory(%struct.Argon2_Context* %6, i8** %8, i32 %10, i32 1024) #6
  store i32 %call, i32* %result, align 4, !tbaa !7
  %11 = load i32, i32* %result, align 4, !tbaa !7
  %cmp2 = icmp ne i32 %11, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %12 = load i32, i32* %result, align 4, !tbaa !7
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %if.end
  %arraydecay = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  %13 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !2
  %14 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %14, i32 0, i32 8
  %15 = load i32, i32* %type, align 4, !tbaa !48
  call void @initial_hash(i8* %arraydecay, %struct.Argon2_Context* %13, i32 %15) #6
  %arraydecay5 = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay5, i32 64
  call void @clear_internal_memory(i8* %add.ptr, i32 8) #6
  %arraydecay6 = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  %16 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  call void @fill_first_blocks(i8* %arraydecay6, %struct.Argon2_instance_t* %16) #6
  %arraydecay7 = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  call void @clear_internal_memory(i8* %arraydecay7, i32 72) #6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end4, %if.then3, %if.then
  %17 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast [72 x i8]* %blockhash to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %18) #5
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: noinline nounwind optsize
define internal void @store64(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i64 %w, i64* %w.addr, align 8, !tbaa !9
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

; Function Attrs: optsize
declare void @fill_segment(%struct.Argon2_instance_t*, %struct.Argon2_position_t* byval(%struct.Argon2_position_t) align 4) #3

; Function Attrs: optsize
declare i8* @calloc(i32, i32) #3

; Function Attrs: optsize
declare i32 @argon2_thread_join(%struct.__pthread*) #3

; Function Attrs: noinline nounwind optsize
define internal i64 @load64(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  %0 = bitcast i64* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  %1 = bitcast i64* %w to i8*
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %1, i8* align 1 %2, i32 8, i1 false)
  %3 = load i64, i64* %w, align 8, !tbaa !9
  %4 = bitcast i64* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %4) #5
  ret i64 %3
}

attributes #0 = { noinline nounwind optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { optsize }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!4, !4, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"long long", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"long", !4, i64 0}
!13 = !{!14, !3, i64 60}
!14 = !{!"Argon2_Context", !3, i64 0, !8, i64 4, !3, i64 8, !8, i64 12, !3, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !3, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !3, i64 60, !3, i64 64, !8, i64 68}
!15 = !{!14, !3, i64 64}
!16 = !{!17, !3, i64 0}
!17 = !{!"Argon2_instance_t", !3, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !4, i64 32, !8, i64 36, !3, i64 40}
!18 = !{!17, !8, i64 20}
!19 = !{!17, !8, i64 24}
!20 = !{!14, !3, i64 0}
!21 = !{!14, !8, i64 4}
!22 = !{!17, !8, i64 12}
!23 = !{!24, !8, i64 0}
!24 = !{!"Argon2_position_t", !8, i64 0, !8, i64 4, !4, i64 8, !8, i64 12}
!25 = !{!24, !4, i64 8}
!26 = !{!24, !8, i64 12}
!27 = !{!17, !8, i64 16}
!28 = !{!17, !8, i64 28}
!29 = !{!17, !8, i64 8}
!30 = !{!24, !8, i64 4}
!31 = !{!32, !3, i64 0}
!32 = !{!"Argon2_thread_data", !3, i64 0, !24, i64 4}
!33 = !{!14, !3, i64 8}
!34 = !{!14, !8, i64 12}
!35 = !{!14, !3, i64 16}
!36 = !{!14, !8, i64 20}
!37 = !{!14, !3, i64 24}
!38 = !{!14, !8, i64 28}
!39 = !{!14, !3, i64 32}
!40 = !{!14, !8, i64 36}
!41 = !{!14, !8, i64 44}
!42 = !{!14, !8, i64 48}
!43 = !{!14, !8, i64 40}
!44 = !{!14, !8, i64 52}
!45 = !{!14, !8, i64 56}
!46 = !{!14, !8, i64 68}
!47 = !{!17, !3, i64 40}
!48 = !{!17, !4, i64 32}
