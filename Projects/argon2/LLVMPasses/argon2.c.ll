; ModuleID = '/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/argon2.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/argon2.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Argon2_Context = type { i8*, i32, i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32 (i8**, i32)*, void (i8*, i32)*, i32 }
%struct.Argon2_instance_t = type { %struct.block_*, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.Argon2_Context* }
%struct.block_ = type { [128 x i64] }

@.str = private unnamed_addr constant [8 x i8] c"Argon2d\00", align 1
@.str.1 = private unnamed_addr constant [8 x i8] c"argon2d\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"Argon2i\00", align 1
@.str.3 = private unnamed_addr constant [8 x i8] c"argon2i\00", align 1
@.str.4 = private unnamed_addr constant [9 x i8] c"Argon2id\00", align 1
@.str.5 = private unnamed_addr constant [9 x i8] c"argon2id\00", align 1
@.str.6 = private unnamed_addr constant [3 x i8] c"OK\00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"Output pointer is NULL\00", align 1
@.str.8 = private unnamed_addr constant [20 x i8] c"Output is too short\00", align 1
@.str.9 = private unnamed_addr constant [19 x i8] c"Output is too long\00", align 1
@.str.10 = private unnamed_addr constant [22 x i8] c"Password is too short\00", align 1
@.str.11 = private unnamed_addr constant [21 x i8] c"Password is too long\00", align 1
@.str.12 = private unnamed_addr constant [18 x i8] c"Salt is too short\00", align 1
@.str.13 = private unnamed_addr constant [17 x i8] c"Salt is too long\00", align 1
@.str.14 = private unnamed_addr constant [29 x i8] c"Associated data is too short\00", align 1
@.str.15 = private unnamed_addr constant [28 x i8] c"Associated data is too long\00", align 1
@.str.16 = private unnamed_addr constant [20 x i8] c"Secret is too short\00", align 1
@.str.17 = private unnamed_addr constant [19 x i8] c"Secret is too long\00", align 1
@.str.18 = private unnamed_addr constant [23 x i8] c"Time cost is too small\00", align 1
@.str.19 = private unnamed_addr constant [23 x i8] c"Time cost is too large\00", align 1
@.str.20 = private unnamed_addr constant [25 x i8] c"Memory cost is too small\00", align 1
@.str.21 = private unnamed_addr constant [25 x i8] c"Memory cost is too large\00", align 1
@.str.22 = private unnamed_addr constant [14 x i8] c"Too few lanes\00", align 1
@.str.23 = private unnamed_addr constant [15 x i8] c"Too many lanes\00", align 1
@.str.24 = private unnamed_addr constant [55 x i8] c"Password pointer is NULL, but password length is not 0\00", align 1
@.str.25 = private unnamed_addr constant [47 x i8] c"Salt pointer is NULL, but salt length is not 0\00", align 1
@.str.26 = private unnamed_addr constant [51 x i8] c"Secret pointer is NULL, but secret length is not 0\00", align 1
@.str.27 = private unnamed_addr constant [56 x i8] c"Associated data pointer is NULL, but ad length is not 0\00", align 1
@.str.28 = private unnamed_addr constant [24 x i8] c"Memory allocation error\00", align 1
@.str.29 = private unnamed_addr constant [33 x i8] c"The free memory callback is NULL\00", align 1
@.str.30 = private unnamed_addr constant [37 x i8] c"The allocate memory callback is NULL\00", align 1
@.str.31 = private unnamed_addr constant [31 x i8] c"Argon2_Context context is NULL\00", align 1
@.str.32 = private unnamed_addr constant [35 x i8] c"There is no such version of Argon2\00", align 1
@.str.33 = private unnamed_addr constant [24 x i8] c"Output pointer mismatch\00", align 1
@.str.34 = private unnamed_addr constant [19 x i8] c"Not enough threads\00", align 1
@.str.35 = private unnamed_addr constant [17 x i8] c"Too many threads\00", align 1
@.str.36 = private unnamed_addr constant [18 x i8] c"Missing arguments\00", align 1
@.str.37 = private unnamed_addr constant [16 x i8] c"Encoding failed\00", align 1
@.str.38 = private unnamed_addr constant [16 x i8] c"Decoding failed\00", align 1
@.str.39 = private unnamed_addr constant [18 x i8] c"Threading failure\00", align 1
@.str.40 = private unnamed_addr constant [53 x i8] c"Some of encoded parameters are too long or too short\00", align 1
@.str.41 = private unnamed_addr constant [46 x i8] c"The password does not match the supplied hash\00", align 1
@.str.42 = private unnamed_addr constant [19 x i8] c"Unknown error code\00", align 1

; Function Attrs: noinline nounwind optsize
define hidden i8* @argon2_type2string(i32 %type, i32 %uppercase) #0 {
entry:
  %retval = alloca i8*, align 4
  %type.addr = alloca i32, align 4
  %uppercase.addr = alloca i32, align 4
  store i32 %type, i32* %type.addr, align 4, !tbaa !2
  store i32 %uppercase, i32* %uppercase.addr, align 4, !tbaa !5
  %0 = load i32, i32* %type.addr, align 4, !tbaa !2
  switch i32 %0, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %uppercase.addr, align 4, !tbaa !5
  %tobool = icmp ne i32 %1, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.1, i32 0, i32 0)
  store i8* %cond, i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %3 = load i32, i32* %uppercase.addr, align 4, !tbaa !5
  %tobool2 = icmp ne i32 %3, 0
  %4 = zext i1 %tobool2 to i64
  %cond3 = select i1 %tobool2, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i32 0, i32 0)
  store i8* %cond3, i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  %5 = load i32, i32* %uppercase.addr, align 4, !tbaa !5
  %tobool5 = icmp ne i32 %5, 0
  %6 = zext i1 %tobool5 to i64
  %cond6 = select i1 %tobool5, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.4, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.5, i32 0, i32 0)
  store i8* %cond6, i8** %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb4, %sw.bb1, %sw.bb
  %7 = load i8*, i8** %retval, align 4
  ret i8* %7
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2_ctx(%struct.Argon2_Context* %context, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %type.addr = alloca i32, align 4
  %result = alloca i32, align 4
  %memory_blocks = alloca i32, align 4
  %segment_length = alloca i32, align 4
  %instance = alloca %struct.Argon2_instance_t, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  store i32 %type, i32* %type.addr, align 4, !tbaa !2
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %call = call i32 @validate_inputs(%struct.Argon2_Context* %1) #4
  store i32 %call, i32* %result, align 4, !tbaa !5
  %2 = bitcast i32* %memory_blocks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %segment_length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast %struct.Argon2_instance_t* %instance to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %4) #3
  %5 = load i32, i32* %result, align 4, !tbaa !5
  %cmp = icmp ne i32 0, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %result, align 4, !tbaa !5
  store i32 %6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %7 = load i32, i32* %type.addr, align 4, !tbaa !2
  %cmp1 = icmp ne i32 0, %7
  br i1 %cmp1, label %land.lhs.true, label %if.end6

land.lhs.true:                                    ; preds = %if.end
  %8 = load i32, i32* %type.addr, align 4, !tbaa !2
  %cmp2 = icmp ne i32 1, %8
  br i1 %cmp2, label %land.lhs.true3, label %if.end6

land.lhs.true3:                                   ; preds = %land.lhs.true
  %9 = load i32, i32* %type.addr, align 4, !tbaa !2
  %cmp4 = icmp ne i32 2, %9
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %land.lhs.true3
  store i32 -26, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %land.lhs.true3, %land.lhs.true, %if.end
  %10 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %10, i32 0, i32 11
  %11 = load i32, i32* %m_cost, align 4, !tbaa !9
  store i32 %11, i32* %memory_blocks, align 4, !tbaa !5
  %12 = load i32, i32* %memory_blocks, align 4, !tbaa !5
  %13 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %13, i32 0, i32 12
  %14 = load i32, i32* %lanes, align 4, !tbaa !11
  %mul = mul i32 8, %14
  %cmp7 = icmp ult i32 %12, %mul
  br i1 %cmp7, label %if.then8, label %if.end11

if.then8:                                         ; preds = %if.end6
  %15 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %lanes9 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %15, i32 0, i32 12
  %16 = load i32, i32* %lanes9, align 4, !tbaa !11
  %mul10 = mul i32 8, %16
  store i32 %mul10, i32* %memory_blocks, align 4, !tbaa !5
  br label %if.end11

if.end11:                                         ; preds = %if.then8, %if.end6
  %17 = load i32, i32* %memory_blocks, align 4, !tbaa !5
  %18 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %lanes12 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %18, i32 0, i32 12
  %19 = load i32, i32* %lanes12, align 4, !tbaa !11
  %mul13 = mul i32 %19, 4
  %div = udiv i32 %17, %mul13
  store i32 %div, i32* %segment_length, align 4, !tbaa !5
  %20 = load i32, i32* %segment_length, align 4, !tbaa !5
  %21 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %lanes14 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %21, i32 0, i32 12
  %22 = load i32, i32* %lanes14, align 4, !tbaa !11
  %mul15 = mul i32 %22, 4
  %mul16 = mul i32 %20, %mul15
  store i32 %mul16, i32* %memory_blocks, align 4, !tbaa !5
  %23 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %version = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %23, i32 0, i32 14
  %24 = load i32, i32* %version, align 4, !tbaa !12
  %version17 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 1
  store i32 %24, i32* %version17, align 4, !tbaa !13
  %memory = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 0
  store %struct.block_* null, %struct.block_** %memory, align 4, !tbaa !15
  %25 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %25, i32 0, i32 10
  %26 = load i32, i32* %t_cost, align 4, !tbaa !16
  %passes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 2
  store i32 %26, i32* %passes, align 4, !tbaa !17
  %27 = load i32, i32* %memory_blocks, align 4, !tbaa !5
  %memory_blocks18 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 3
  store i32 %27, i32* %memory_blocks18, align 4, !tbaa !18
  %28 = load i32, i32* %segment_length, align 4, !tbaa !5
  %segment_length19 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 4
  store i32 %28, i32* %segment_length19, align 4, !tbaa !19
  %29 = load i32, i32* %segment_length, align 4, !tbaa !5
  %mul20 = mul i32 %29, 4
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 5
  store i32 %mul20, i32* %lane_length, align 4, !tbaa !20
  %30 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %lanes21 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %30, i32 0, i32 12
  %31 = load i32, i32* %lanes21, align 4, !tbaa !11
  %lanes22 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 6
  store i32 %31, i32* %lanes22, align 4, !tbaa !21
  %32 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %32, i32 0, i32 13
  %33 = load i32, i32* %threads, align 4, !tbaa !22
  %threads23 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 7
  store i32 %33, i32* %threads23, align 4, !tbaa !23
  %34 = load i32, i32* %type.addr, align 4, !tbaa !2
  %type24 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 8
  store i32 %34, i32* %type24, align 4, !tbaa !24
  %threads25 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 7
  %35 = load i32, i32* %threads25, align 4, !tbaa !23
  %lanes26 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 6
  %36 = load i32, i32* %lanes26, align 4, !tbaa !21
  %cmp27 = icmp ugt i32 %35, %36
  br i1 %cmp27, label %if.then28, label %if.end31

if.then28:                                        ; preds = %if.end11
  %lanes29 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 6
  %37 = load i32, i32* %lanes29, align 4, !tbaa !21
  %threads30 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 7
  store i32 %37, i32* %threads30, align 4, !tbaa !23
  br label %if.end31

if.end31:                                         ; preds = %if.then28, %if.end11
  %38 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %call32 = call i32 @initialize(%struct.Argon2_instance_t* %instance, %struct.Argon2_Context* %38) #4
  store i32 %call32, i32* %result, align 4, !tbaa !5
  %39 = load i32, i32* %result, align 4, !tbaa !5
  %cmp33 = icmp ne i32 0, %39
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end31
  %40 = load i32, i32* %result, align 4, !tbaa !5
  store i32 %40, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end35:                                         ; preds = %if.end31
  %call36 = call i32 @fill_memory_blocks(%struct.Argon2_instance_t* %instance) #4
  store i32 %call36, i32* %result, align 4, !tbaa !5
  %41 = load i32, i32* %result, align 4, !tbaa !5
  %cmp37 = icmp ne i32 0, %41
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.end35
  %42 = load i32, i32* %result, align 4, !tbaa !5
  store i32 %42, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end39:                                         ; preds = %if.end35
  %43 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  call void @finalize(%struct.Argon2_Context* %43, %struct.Argon2_instance_t* %instance) #4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end39, %if.then38, %if.then34, %if.then5, %if.then
  %44 = bitcast %struct.Argon2_instance_t* %instance to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %44) #3
  %45 = bitcast i32* %segment_length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i32* %memory_blocks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = load i32, i32* %retval, align 4
  ret i32 %48
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: optsize
declare i32 @validate_inputs(%struct.Argon2_Context*) #2

; Function Attrs: optsize
declare i32 @initialize(%struct.Argon2_instance_t*, %struct.Argon2_Context*) #2

; Function Attrs: optsize
declare i32 @fill_memory_blocks(%struct.Argon2_instance_t*) #2

; Function Attrs: optsize
declare void @finalize(%struct.Argon2_Context*, %struct.Argon2_instance_t*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2_hash(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i8* %hash, i32 %hashlen, i8* %encoded, i32 %encodedlen, i32 %type, i32 %version) #0 {
entry:
  %retval = alloca i32, align 4
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hash.addr = alloca i8*, align 4
  %hashlen.addr = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %encodedlen.addr = alloca i32, align 4
  %type.addr = alloca i32, align 4
  %version.addr = alloca i32, align 4
  %context = alloca %struct.Argon2_Context, align 4
  %result = alloca i32, align 4
  %out = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i8* %salt, i8** %salt.addr, align 4, !tbaa !7
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !25
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !25
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i32 %encodedlen, i32* %encodedlen.addr, align 4, !tbaa !25
  store i32 %type, i32* %type.addr, align 4, !tbaa !2
  store i32 %version, i32* %version.addr, align 4, !tbaa !5
  %0 = bitcast %struct.Argon2_Context* %context to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %0) #3
  %1 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i8** %out to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %cmp = icmp ugt i32 %3, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %cmp1 = icmp ugt i32 %4, -1
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %5 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %cmp4 = icmp ugt i32 %5, -1
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end3
  %6 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %cmp7 = icmp ult i32 %6, 4
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 -2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end6
  %7 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %call = call i8* @malloc(i32 %7) #4
  store i8* %call, i8** %out, align 4, !tbaa !7
  %8 = load i8*, i8** %out, align 4, !tbaa !7
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %if.end11, label %if.then10

if.then10:                                        ; preds = %if.end9
  store i32 -22, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %if.end9
  %9 = load i8*, i8** %out, align 4, !tbaa !7
  %out12 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 0
  store i8* %9, i8** %out12, align 4, !tbaa !27
  %10 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 1
  store i32 %10, i32* %outlen, align 4, !tbaa !28
  %11 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %12 = ptrtoint i8* %11 to i32
  %13 = inttoptr i32 %12 to i8*
  %pwd13 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 2
  store i8* %13, i8** %pwd13, align 4, !tbaa !29
  %14 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %pwdlen14 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 3
  store i32 %14, i32* %pwdlen14, align 4, !tbaa !30
  %15 = load i8*, i8** %salt.addr, align 4, !tbaa !7
  %16 = ptrtoint i8* %15 to i32
  %17 = inttoptr i32 %16 to i8*
  %salt15 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 4
  store i8* %17, i8** %salt15, align 4, !tbaa !31
  %18 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %saltlen16 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 5
  store i32 %18, i32* %saltlen16, align 4, !tbaa !32
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 6
  store i8* null, i8** %secret, align 4, !tbaa !33
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 7
  store i32 0, i32* %secretlen, align 4, !tbaa !34
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 8
  store i8* null, i8** %ad, align 4, !tbaa !35
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 9
  store i32 0, i32* %adlen, align 4, !tbaa !36
  %19 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %t_cost17 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 10
  store i32 %19, i32* %t_cost17, align 4, !tbaa !16
  %20 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %m_cost18 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 11
  store i32 %20, i32* %m_cost18, align 4, !tbaa !9
  %21 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 12
  store i32 %21, i32* %lanes, align 4, !tbaa !11
  %22 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 13
  store i32 %22, i32* %threads, align 4, !tbaa !22
  %allocate_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 15
  store i32 (i8**, i32)* null, i32 (i8**, i32)** %allocate_cbk, align 4, !tbaa !37
  %free_cbk = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 16
  store void (i8*, i32)* null, void (i8*, i32)** %free_cbk, align 4, !tbaa !38
  %flags = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 17
  store i32 0, i32* %flags, align 4, !tbaa !39
  %23 = load i32, i32* %version.addr, align 4, !tbaa !5
  %version19 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 14
  store i32 %23, i32* %version19, align 4, !tbaa !12
  %24 = load i32, i32* %type.addr, align 4, !tbaa !2
  %call20 = call i32 @argon2_ctx(%struct.Argon2_Context* %context, i32 %24) #4
  store i32 %call20, i32* %result, align 4, !tbaa !5
  %25 = load i32, i32* %result, align 4, !tbaa !5
  %cmp21 = icmp ne i32 %25, 0
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end11
  %26 = load i8*, i8** %out, align 4, !tbaa !7
  %27 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  call void @clear_internal_memory(i8* %26, i32 %27) #4
  %28 = load i8*, i8** %out, align 4, !tbaa !7
  call void @free(i8* %28) #4
  %29 = load i32, i32* %result, align 4, !tbaa !5
  store i32 %29, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end23:                                         ; preds = %if.end11
  %30 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %tobool24 = icmp ne i8* %30, null
  br i1 %tobool24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end23
  %31 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %32 = load i8*, i8** %out, align 4, !tbaa !7
  %33 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %31, i8* align 1 %32, i32 %33, i1 false)
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.end23
  %34 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %tobool27 = icmp ne i8* %34, null
  br i1 %tobool27, label %land.lhs.true, label %if.end34

land.lhs.true:                                    ; preds = %if.end26
  %35 = load i32, i32* %encodedlen.addr, align 4, !tbaa !25
  %tobool28 = icmp ne i32 %35, 0
  br i1 %tobool28, label %if.then29, label %if.end34

if.then29:                                        ; preds = %land.lhs.true
  %36 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %37 = load i32, i32* %encodedlen.addr, align 4, !tbaa !25
  %38 = load i32, i32* %type.addr, align 4, !tbaa !2
  %call30 = call i32 @encode_string(i8* %36, i32 %37, %struct.Argon2_Context* %context, i32 %38) #4
  %cmp31 = icmp ne i32 %call30, 0
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.then29
  %39 = load i8*, i8** %out, align 4, !tbaa !7
  %40 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  call void @clear_internal_memory(i8* %39, i32 %40) #4
  %41 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %42 = load i32, i32* %encodedlen.addr, align 4, !tbaa !25
  call void @clear_internal_memory(i8* %41, i32 %42) #4
  %43 = load i8*, i8** %out, align 4, !tbaa !7
  call void @free(i8* %43) #4
  store i32 -31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end33:                                         ; preds = %if.then29
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %land.lhs.true, %if.end26
  %44 = load i8*, i8** %out, align 4, !tbaa !7
  %45 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  call void @clear_internal_memory(i8* %44, i32 %45) #4
  %46 = load i8*, i8** %out, align 4, !tbaa !7
  call void @free(i8* %46) #4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end34, %if.then32, %if.then22, %if.then10, %if.then8, %if.then5, %if.then2, %if.then
  %47 = bitcast i8** %out to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  %49 = bitcast %struct.Argon2_Context* %context to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %49) #3
  %50 = load i32, i32* %retval, align 4
  ret i32 %50
}

; Function Attrs: optsize
declare i8* @malloc(i32) #2

; Function Attrs: optsize
declare void @clear_internal_memory(i8*, i32) #2

; Function Attrs: optsize
declare void @free(i8*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: optsize
declare i32 @encode_string(i8*, i32, %struct.Argon2_Context*, i32) #2

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2i_hash_encoded(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i32 %hashlen, i8* %encoded, i32 %encodedlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hashlen.addr = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %encodedlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i8* %salt, i8** %salt.addr, align 4, !tbaa !7
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !25
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !25
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i32 %encodedlen, i32* %encodedlen.addr, align 4, !tbaa !25
  %0 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %1 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %2 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %3 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %4 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %5 = load i8*, i8** %salt.addr, align 4, !tbaa !7
  %6 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %7 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %8 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %9 = load i32, i32* %encodedlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* null, i32 %7, i8* %8, i32 %9, i32 1, i32 19) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2i_hash_raw(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i8* %hash, i32 %hashlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hash.addr = alloca i8*, align 4
  %hashlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i8* %salt, i8** %salt.addr, align 4, !tbaa !7
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !25
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !25
  %0 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %1 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %2 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %3 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %4 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %5 = load i8*, i8** %salt.addr, align 4, !tbaa !7
  %6 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %7 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %8 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* %7, i32 %8, i8* null, i32 0, i32 1, i32 19) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2d_hash_encoded(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i32 %hashlen, i8* %encoded, i32 %encodedlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hashlen.addr = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %encodedlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i8* %salt, i8** %salt.addr, align 4, !tbaa !7
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !25
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !25
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i32 %encodedlen, i32* %encodedlen.addr, align 4, !tbaa !25
  %0 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %1 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %2 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %3 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %4 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %5 = load i8*, i8** %salt.addr, align 4, !tbaa !7
  %6 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %7 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %8 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %9 = load i32, i32* %encodedlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* null, i32 %7, i8* %8, i32 %9, i32 0, i32 19) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2d_hash_raw(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i8* %hash, i32 %hashlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hash.addr = alloca i8*, align 4
  %hashlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i8* %salt, i8** %salt.addr, align 4, !tbaa !7
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !25
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !25
  %0 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %1 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %2 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %3 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %4 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %5 = load i8*, i8** %salt.addr, align 4, !tbaa !7
  %6 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %7 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %8 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* %7, i32 %8, i8* null, i32 0, i32 0, i32 19) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2id_hash_encoded(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i32 %hashlen, i8* %encoded, i32 %encodedlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hashlen.addr = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %encodedlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i8* %salt, i8** %salt.addr, align 4, !tbaa !7
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !25
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !25
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i32 %encodedlen, i32* %encodedlen.addr, align 4, !tbaa !25
  %0 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %1 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %2 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %3 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %4 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %5 = load i8*, i8** %salt.addr, align 4, !tbaa !7
  %6 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %7 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %8 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %9 = load i32, i32* %encodedlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* null, i32 %7, i8* %8, i32 %9, i32 2, i32 19) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2id_hash_raw(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i8* %hash, i32 %hashlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hash.addr = alloca i8*, align 4
  %hashlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i8* %salt, i8** %salt.addr, align 4, !tbaa !7
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !25
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !25
  %0 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %1 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %2 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %3 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %4 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %5 = load i8*, i8** %salt.addr, align 4, !tbaa !7
  %6 = load i32, i32* %saltlen.addr, align 4, !tbaa !25
  %7 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %8 = load i32, i32* %hashlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* %7, i32 %8, i8* null, i32 0, i32 2, i32 19) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2_verify(i8* %encoded, i8* %pwd, i32 %pwdlen, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %type.addr = alloca i32, align 4
  %ctx = alloca %struct.Argon2_Context, align 4
  %desired_result = alloca i8*, align 4
  %ret = alloca i32, align 4
  %encoded_len = alloca i32, align 4
  %max_field_len = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  store i32 %type, i32* %type.addr, align 4, !tbaa !2
  %0 = bitcast %struct.Argon2_Context* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %0) #3
  %1 = bitcast i8** %desired_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i8* null, i8** %desired_result, align 4, !tbaa !7
  %2 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %ret, align 4, !tbaa !5
  %3 = bitcast i32* %encoded_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %max_field_len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %cmp = icmp ugt i32 %5, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %cmp1 = icmp eq i8* %6, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %7 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %call = call i32 @strlen(i8* %7) #4
  store i32 %call, i32* %encoded_len, align 4, !tbaa !25
  %8 = load i32, i32* %encoded_len, align 4, !tbaa !25
  %cmp4 = icmp ugt i32 %8, -1
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -32, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end3
  %9 = load i32, i32* %encoded_len, align 4, !tbaa !25
  store i32 %9, i32* %max_field_len, align 4, !tbaa !5
  %10 = load i32, i32* %max_field_len, align 4, !tbaa !5
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 5
  store i32 %10, i32* %saltlen, align 4, !tbaa !32
  %11 = load i32, i32* %max_field_len, align 4, !tbaa !5
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  store i32 %11, i32* %outlen, align 4, !tbaa !28
  %saltlen7 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 5
  %12 = load i32, i32* %saltlen7, align 4, !tbaa !32
  %call8 = call i8* @malloc(i32 %12) #4
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  store i8* %call8, i8** %salt, align 4, !tbaa !31
  %outlen9 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  %13 = load i32, i32* %outlen9, align 4, !tbaa !28
  %call10 = call i8* @malloc(i32 %13) #4
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  store i8* %call10, i8** %out, align 4, !tbaa !27
  %salt11 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %14 = load i8*, i8** %salt11, align 4, !tbaa !31
  %tobool = icmp ne i8* %14, null
  br i1 %tobool, label %lor.lhs.false, label %if.then14

lor.lhs.false:                                    ; preds = %if.end6
  %out12 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %15 = load i8*, i8** %out12, align 4, !tbaa !27
  %tobool13 = icmp ne i8* %15, null
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %lor.lhs.false, %if.end6
  store i32 -22, i32* %ret, align 4, !tbaa !5
  br label %fail

if.end15:                                         ; preds = %lor.lhs.false
  %16 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %pwd16 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 2
  store i8* %16, i8** %pwd16, align 4, !tbaa !29
  %17 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %pwdlen17 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 3
  store i32 %17, i32* %pwdlen17, align 4, !tbaa !30
  %18 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %19 = load i32, i32* %type.addr, align 4, !tbaa !2
  %call18 = call i32 @decode_string(%struct.Argon2_Context* %ctx, i8* %18, i32 %19) #4
  store i32 %call18, i32* %ret, align 4, !tbaa !5
  %20 = load i32, i32* %ret, align 4, !tbaa !5
  %cmp19 = icmp ne i32 %20, 0
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end15
  br label %fail

if.end21:                                         ; preds = %if.end15
  %out22 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %21 = load i8*, i8** %out22, align 4, !tbaa !27
  store i8* %21, i8** %desired_result, align 4, !tbaa !7
  %outlen23 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  %22 = load i32, i32* %outlen23, align 4, !tbaa !28
  %call24 = call i8* @malloc(i32 %22) #4
  %out25 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  store i8* %call24, i8** %out25, align 4, !tbaa !27
  %out26 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %23 = load i8*, i8** %out26, align 4, !tbaa !27
  %tobool27 = icmp ne i8* %23, null
  br i1 %tobool27, label %if.end29, label %if.then28

if.then28:                                        ; preds = %if.end21
  store i32 -22, i32* %ret, align 4, !tbaa !5
  br label %fail

if.end29:                                         ; preds = %if.end21
  %24 = load i8*, i8** %desired_result, align 4, !tbaa !7
  %25 = load i32, i32* %type.addr, align 4, !tbaa !2
  %call30 = call i32 @argon2_verify_ctx(%struct.Argon2_Context* %ctx, i8* %24, i32 %25) #4
  store i32 %call30, i32* %ret, align 4, !tbaa !5
  %26 = load i32, i32* %ret, align 4, !tbaa !5
  %cmp31 = icmp ne i32 %26, 0
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end29
  br label %fail

if.end33:                                         ; preds = %if.end29
  br label %fail

fail:                                             ; preds = %if.end33, %if.then32, %if.then28, %if.then20, %if.then14
  %salt34 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %27 = load i8*, i8** %salt34, align 4, !tbaa !31
  call void @free(i8* %27) #4
  %out35 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %28 = load i8*, i8** %out35, align 4, !tbaa !27
  call void @free(i8* %28) #4
  %29 = load i8*, i8** %desired_result, align 4, !tbaa !7
  call void @free(i8* %29) #4
  %30 = load i32, i32* %ret, align 4, !tbaa !5
  store i32 %30, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %fail, %if.then5, %if.then2, %if.then
  %31 = bitcast i32* %max_field_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = bitcast i32* %encoded_len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  %33 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  %34 = bitcast i8** %desired_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast %struct.Argon2_Context* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %35) #3
  %36 = load i32, i32* %retval, align 4
  ret i32 %36
}

; Function Attrs: optsize
declare i32 @strlen(i8*) #2

; Function Attrs: optsize
declare i32 @decode_string(%struct.Argon2_Context*, i8*, i32) #2

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2_verify_ctx(%struct.Argon2_Context* %context, i8* %hash, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %hash.addr = alloca i8*, align 4
  %type.addr = alloca i32, align 4
  %ret = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  store i32 %type, i32* %type.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %2 = load i32, i32* %type.addr, align 4, !tbaa !2
  %call = call i32 @argon2_ctx(%struct.Argon2_Context* %1, i32 %2) #4
  store i32 %call, i32* %ret, align 4, !tbaa !5
  %3 = load i32, i32* %ret, align 4, !tbaa !5
  %cmp = icmp ne i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %ret, align 4, !tbaa !5
  store i32 %4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %6 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %6, i32 0, i32 0
  %7 = load i8*, i8** %out, align 4, !tbaa !27
  %8 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %8, i32 0, i32 1
  %9 = load i32, i32* %outlen, align 4, !tbaa !28
  %call1 = call i32 @argon2_compare(i8* %5, i8* %7, i32 %9) #4
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -35, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end3, %if.then2, %if.then
  %10 = bitcast i32* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2i_verify(i8* %encoded, i8* %pwd, i32 %pwdlen) #0 {
entry:
  %encoded.addr = alloca i8*, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  %0 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %2 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_verify(i8* %0, i8* %1, i32 %2, i32 1) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2d_verify(i8* %encoded, i8* %pwd, i32 %pwdlen) #0 {
entry:
  %encoded.addr = alloca i8*, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  %0 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %2 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_verify(i8* %0, i8* %1, i32 %2, i32 0) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2id_verify(i8* %encoded, i8* %pwd, i32 %pwdlen) #0 {
entry:
  %encoded.addr = alloca i8*, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  store i8* %encoded, i8** %encoded.addr, align 4, !tbaa !7
  store i8* %pwd, i8** %pwd.addr, align 4, !tbaa !7
  store i32 %pwdlen, i32* %pwdlen.addr, align 4, !tbaa !25
  %0 = load i8*, i8** %encoded.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %pwd.addr, align 4, !tbaa !7
  %2 = load i32, i32* %pwdlen.addr, align 4, !tbaa !25
  %call = call i32 @argon2_verify(i8* %0, i8* %1, i32 %2, i32 2) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2d_ctx(%struct.Argon2_Context* %context) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %call = call i32 @argon2_ctx(%struct.Argon2_Context* %0, i32 0) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2i_ctx(%struct.Argon2_Context* %context) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %call = call i32 @argon2_ctx(%struct.Argon2_Context* %0, i32 1) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2id_ctx(%struct.Argon2_Context* %context) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %call = call i32 @argon2_ctx(%struct.Argon2_Context* %0, i32 2) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define internal i32 @argon2_compare(i8* %b1, i8* %b2, i32 %len) #0 {
entry:
  %b1.addr = alloca i8*, align 4
  %b2.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %d = alloca i8, align 1
  store i8* %b1, i8** %b1.addr, align 4, !tbaa !7
  store i8* %b2, i8** %b2.addr, align 4, !tbaa !7
  store i32 %len, i32* %len.addr, align 4, !tbaa !25
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %d) #3
  store i8 0, i8* %d, align 1, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %2 = load i32, i32* %len.addr, align 4, !tbaa !25
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %b1.addr, align 4, !tbaa !7
  %4 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %4
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !2
  %conv = zext i8 %5 to i32
  %6 = load i8*, i8** %b2.addr, align 4, !tbaa !7
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx1 = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx1, align 1, !tbaa !2
  %conv2 = zext i8 %8 to i32
  %xor = xor i32 %conv, %conv2
  %9 = load i8, i8* %d, align 1, !tbaa !2
  %conv3 = zext i8 %9 to i32
  %or = or i32 %conv3, %xor
  %conv4 = trunc i32 %or to i8
  store i8 %conv4, i8* %d, align 1, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load i8, i8* %d, align 1, !tbaa !2
  %conv5 = zext i8 %11 to i32
  %sub = sub nsw i32 %conv5, 1
  %shr = ashr i32 %sub, 8
  %and = and i32 1, %shr
  %sub6 = sub nsw i32 %and, 1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %d) #3
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #3
  ret i32 %sub6
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2d_verify_ctx(%struct.Argon2_Context* %context, i8* %hash) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %hash.addr = alloca i8*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %call = call i32 @argon2_verify_ctx(%struct.Argon2_Context* %0, i8* %1, i32 0) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2i_verify_ctx(%struct.Argon2_Context* %context, i8* %hash) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %hash.addr = alloca i8*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %call = call i32 @argon2_verify_ctx(%struct.Argon2_Context* %0, i8* %1, i32 1) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2id_verify_ctx(%struct.Argon2_Context* %context, i8* %hash) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %hash.addr = alloca i8*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  store i8* %hash, i8** %hash.addr, align 4, !tbaa !7
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %hash.addr, align 4, !tbaa !7
  %call = call i32 @argon2_verify_ctx(%struct.Argon2_Context* %0, i8* %1, i32 2) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optsize
define hidden i8* @argon2_error_message(i32 %error_code) #0 {
entry:
  %retval = alloca i8*, align 4
  %error_code.addr = alloca i32, align 4
  store i32 %error_code, i32* %error_code.addr, align 4, !tbaa !5
  %0 = load i32, i32* %error_code.addr, align 4, !tbaa !5
  switch i32 %0, label %sw.default [
    i32 0, label %sw.bb
    i32 -1, label %sw.bb1
    i32 -2, label %sw.bb2
    i32 -3, label %sw.bb3
    i32 -4, label %sw.bb4
    i32 -5, label %sw.bb5
    i32 -6, label %sw.bb6
    i32 -7, label %sw.bb7
    i32 -8, label %sw.bb8
    i32 -9, label %sw.bb9
    i32 -10, label %sw.bb10
    i32 -11, label %sw.bb11
    i32 -12, label %sw.bb12
    i32 -13, label %sw.bb13
    i32 -14, label %sw.bb14
    i32 -15, label %sw.bb15
    i32 -16, label %sw.bb16
    i32 -17, label %sw.bb17
    i32 -18, label %sw.bb18
    i32 -19, label %sw.bb19
    i32 -20, label %sw.bb20
    i32 -21, label %sw.bb21
    i32 -22, label %sw.bb22
    i32 -23, label %sw.bb23
    i32 -24, label %sw.bb24
    i32 -25, label %sw.bb25
    i32 -26, label %sw.bb26
    i32 -27, label %sw.bb27
    i32 -28, label %sw.bb28
    i32 -29, label %sw.bb29
    i32 -30, label %sw.bb30
    i32 -31, label %sw.bb31
    i32 -32, label %sw.bb32
    i32 -33, label %sw.bb33
    i32 -34, label %sw.bb34
    i32 -35, label %sw.bb35
  ]

sw.bb:                                            ; preds = %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.8, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.9, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.10, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.11, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  store i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.12, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  store i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.13, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  store i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.14, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb9:                                           ; preds = %entry
  store i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.15, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb10:                                          ; preds = %entry
  store i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.16, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb11:                                          ; preds = %entry
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.17, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb12:                                          ; preds = %entry
  store i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.18, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb13:                                          ; preds = %entry
  store i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.19, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb14:                                          ; preds = %entry
  store i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.20, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb15:                                          ; preds = %entry
  store i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.21, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb16:                                          ; preds = %entry
  store i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.22, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb17:                                          ; preds = %entry
  store i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.23, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb18:                                          ; preds = %entry
  store i8* getelementptr inbounds ([55 x i8], [55 x i8]* @.str.24, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb19:                                          ; preds = %entry
  store i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.25, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb20:                                          ; preds = %entry
  store i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.26, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb21:                                          ; preds = %entry
  store i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.27, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb22:                                          ; preds = %entry
  store i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.28, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb23:                                          ; preds = %entry
  store i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.29, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb24:                                          ; preds = %entry
  store i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.30, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb25:                                          ; preds = %entry
  store i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.31, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb26:                                          ; preds = %entry
  store i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.32, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb27:                                          ; preds = %entry
  store i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.33, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb28:                                          ; preds = %entry
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.34, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb29:                                          ; preds = %entry
  store i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.35, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb30:                                          ; preds = %entry
  store i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.36, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb31:                                          ; preds = %entry
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.37, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb32:                                          ; preds = %entry
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.38, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb33:                                          ; preds = %entry
  store i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.39, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb34:                                          ; preds = %entry
  store i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.40, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb35:                                          ; preds = %entry
  store i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.41, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.42, i32 0, i32 0), i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb35, %sw.bb34, %sw.bb33, %sw.bb32, %sw.bb31, %sw.bb30, %sw.bb29, %sw.bb28, %sw.bb27, %sw.bb26, %sw.bb25, %sw.bb24, %sw.bb23, %sw.bb22, %sw.bb21, %sw.bb20, %sw.bb19, %sw.bb18, %sw.bb17, %sw.bb16, %sw.bb15, %sw.bb14, %sw.bb13, %sw.bb12, %sw.bb11, %sw.bb10, %sw.bb9, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8*, i8** %retval, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optsize
define hidden i32 @argon2_encodedlen(i32 %t_cost, i32 %m_cost, i32 %parallelism, i32 %saltlen, i32 %hashlen, i32 %type) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %saltlen.addr = alloca i32, align 4
  %hashlen.addr = alloca i32, align 4
  %type.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4, !tbaa !5
  store i32 %m_cost, i32* %m_cost.addr, align 4, !tbaa !5
  store i32 %parallelism, i32* %parallelism.addr, align 4, !tbaa !5
  store i32 %saltlen, i32* %saltlen.addr, align 4, !tbaa !5
  store i32 %hashlen, i32* %hashlen.addr, align 4, !tbaa !5
  store i32 %type, i32* %type.addr, align 4, !tbaa !2
  %0 = load i32, i32* %type.addr, align 4, !tbaa !2
  %call = call i8* @argon2_type2string(i32 %0, i32 0) #4
  %call1 = call i32 @strlen(i8* %call) #4
  %add = add i32 15, %call1
  %1 = load i32, i32* %t_cost.addr, align 4, !tbaa !5
  %call2 = call i32 @numlen(i32 %1) #4
  %add3 = add i32 %add, %call2
  %2 = load i32, i32* %m_cost.addr, align 4, !tbaa !5
  %call4 = call i32 @numlen(i32 %2) #4
  %add5 = add i32 %add3, %call4
  %3 = load i32, i32* %parallelism.addr, align 4, !tbaa !5
  %call6 = call i32 @numlen(i32 %3) #4
  %add7 = add i32 %add5, %call6
  %4 = load i32, i32* %saltlen.addr, align 4, !tbaa !5
  %call8 = call i32 @b64len(i32 %4) #4
  %add9 = add i32 %add7, %call8
  %5 = load i32, i32* %hashlen.addr, align 4, !tbaa !5
  %call10 = call i32 @b64len(i32 %5) #4
  %add11 = add i32 %add9, %call10
  %call12 = call i32 @numlen(i32 19) #4
  %add13 = add i32 %add11, %call12
  %add14 = add i32 %add13, 1
  ret i32 %add14
}

; Function Attrs: optsize
declare i32 @numlen(i32) #2

; Function Attrs: optsize
declare i32 @b64len(i32) #2

attributes #0 = { noinline nounwind optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { optsize }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"int", !3, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"any pointer", !3, i64 0}
!9 = !{!10, !6, i64 44}
!10 = !{!"Argon2_Context", !8, i64 0, !6, i64 4, !8, i64 8, !6, i64 12, !8, i64 16, !6, i64 20, !8, i64 24, !6, i64 28, !8, i64 32, !6, i64 36, !6, i64 40, !6, i64 44, !6, i64 48, !6, i64 52, !6, i64 56, !8, i64 60, !8, i64 64, !6, i64 68}
!11 = !{!10, !6, i64 48}
!12 = !{!10, !6, i64 56}
!13 = !{!14, !6, i64 4}
!14 = !{!"Argon2_instance_t", !8, i64 0, !6, i64 4, !6, i64 8, !6, i64 12, !6, i64 16, !6, i64 20, !6, i64 24, !6, i64 28, !3, i64 32, !6, i64 36, !8, i64 40}
!15 = !{!14, !8, i64 0}
!16 = !{!10, !6, i64 40}
!17 = !{!14, !6, i64 8}
!18 = !{!14, !6, i64 12}
!19 = !{!14, !6, i64 16}
!20 = !{!14, !6, i64 20}
!21 = !{!14, !6, i64 24}
!22 = !{!10, !6, i64 52}
!23 = !{!14, !6, i64 28}
!24 = !{!14, !3, i64 32}
!25 = !{!26, !26, i64 0}
!26 = !{!"long", !3, i64 0}
!27 = !{!10, !8, i64 0}
!28 = !{!10, !6, i64 4}
!29 = !{!10, !8, i64 8}
!30 = !{!10, !6, i64 12}
!31 = !{!10, !8, i64 16}
!32 = !{!10, !6, i64 20}
!33 = !{!10, !8, i64 24}
!34 = !{!10, !6, i64 28}
!35 = !{!10, !8, i64 32}
!36 = !{!10, !6, i64 36}
!37 = !{!10, !8, i64 60}
!38 = !{!10, !8, i64 64}
!39 = !{!10, !6, i64 68}
