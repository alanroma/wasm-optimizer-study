; ModuleID = '/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/ref.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/argon2/Source/argon2/src/ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Argon2_instance_t = type { %struct.block_*, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.Argon2_Context* }
%struct.block_ = type { [128 x i64] }
%struct.Argon2_Context = type { i8*, i32, i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32 (i8**, i32)*, void (i8*, i32)*, i32 }
%struct.Argon2_position_t = type { i32, i32, i8, i32 }

; Function Attrs: noinline nounwind optsize
define hidden void @fill_segment(%struct.Argon2_instance_t* %instance, %struct.Argon2_position_t* byval(%struct.Argon2_position_t) align 4 %position) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %ref_block = alloca %struct.block_*, align 4
  %curr_block = alloca %struct.block_*, align 4
  %address_block = alloca %struct.block_, align 8
  %input_block = alloca %struct.block_, align 8
  %zero_block = alloca %struct.block_, align 8
  %pseudo_rand = alloca i64, align 8
  %ref_index = alloca i64, align 8
  %ref_lane = alloca i64, align 8
  %prev_offset = alloca i32, align 4
  %curr_offset = alloca i32, align 4
  %starting_index = alloca i32, align 4
  %i = alloca i32, align 4
  %data_independent_addressing = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %0 = bitcast %struct.block_** %ref_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store %struct.block_* null, %struct.block_** %ref_block, align 4, !tbaa !2
  %1 = bitcast %struct.block_** %curr_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store %struct.block_* null, %struct.block_** %curr_block, align 4, !tbaa !2
  %2 = bitcast %struct.block_* %address_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %2) #3
  %3 = bitcast %struct.block_* %input_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %3) #3
  %4 = bitcast %struct.block_* %zero_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %4) #3
  %5 = bitcast i64* %pseudo_rand to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #3
  %6 = bitcast i64* %ref_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #3
  %7 = bitcast i64* %ref_lane to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #3
  %8 = bitcast i32* %prev_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %curr_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %starting_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %data_independent_addressing to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Argon2_instance_t* %13, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %14 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %14, i32 0, i32 8
  %15 = load i32, i32* %type, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %15, 1
  br i1 %cmp1, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  %16 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %type2 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %16, i32 0, i32 8
  %17 = load i32, i32* %type2, align 4, !tbaa !6
  %cmp3 = icmp eq i32 %17, 2
  br i1 %cmp3, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %lor.rhs
  %pass = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %18 = load i32, i32* %pass, align 4, !tbaa !9
  %cmp4 = icmp eq i32 %18, 0
  br i1 %cmp4, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %19 = load i8, i8* %slice, align 4, !tbaa !11
  %conv = zext i8 %19 to i32
  %cmp5 = icmp ult i32 %conv, 2
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %lor.rhs
  %20 = phi i1 [ false, %land.lhs.true ], [ false, %lor.rhs ], [ %cmp5, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %if.end
  %21 = phi i1 [ true, %if.end ], [ %20, %land.end ]
  %lor.ext = zext i1 %21 to i32
  store i32 %lor.ext, i32* %data_independent_addressing, align 4, !tbaa !12
  %22 = load i32, i32* %data_independent_addressing, align 4, !tbaa !12
  %tobool = icmp ne i32 %22, 0
  br i1 %tobool, label %if.then7, label %if.end27

if.then7:                                         ; preds = %lor.end
  call void @init_block_value(%struct.block_* %zero_block, i8 zeroext 0) #4
  call void @init_block_value(%struct.block_* %input_block, i8 zeroext 0) #4
  %pass8 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %23 = load i32, i32* %pass8, align 4, !tbaa !9
  %conv9 = zext i32 %23 to i64
  %v = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  store i64 %conv9, i64* %arrayidx, align 8, !tbaa !13
  %lane = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %24 = load i32, i32* %lane, align 4, !tbaa !15
  %conv10 = zext i32 %24 to i64
  %v11 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [128 x i64], [128 x i64]* %v11, i32 0, i32 1
  store i64 %conv10, i64* %arrayidx12, align 8, !tbaa !13
  %slice13 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %25 = load i8, i8* %slice13, align 4, !tbaa !11
  %conv14 = zext i8 %25 to i64
  %v15 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [128 x i64], [128 x i64]* %v15, i32 0, i32 2
  store i64 %conv14, i64* %arrayidx16, align 8, !tbaa !13
  %26 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory_blocks = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %26, i32 0, i32 3
  %27 = load i32, i32* %memory_blocks, align 4, !tbaa !16
  %conv17 = zext i32 %27 to i64
  %v18 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [128 x i64], [128 x i64]* %v18, i32 0, i32 3
  store i64 %conv17, i64* %arrayidx19, align 8, !tbaa !13
  %28 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %passes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %28, i32 0, i32 2
  %29 = load i32, i32* %passes, align 4, !tbaa !17
  %conv20 = zext i32 %29 to i64
  %v21 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [128 x i64], [128 x i64]* %v21, i32 0, i32 4
  store i64 %conv20, i64* %arrayidx22, align 8, !tbaa !13
  %30 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %type23 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %30, i32 0, i32 8
  %31 = load i32, i32* %type23, align 4, !tbaa !6
  %conv24 = zext i32 %31 to i64
  %v25 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx26 = getelementptr inbounds [128 x i64], [128 x i64]* %v25, i32 0, i32 5
  store i64 %conv24, i64* %arrayidx26, align 8, !tbaa !13
  br label %if.end27

if.end27:                                         ; preds = %if.then7, %lor.end
  store i32 0, i32* %starting_index, align 4, !tbaa !12
  %pass28 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %32 = load i32, i32* %pass28, align 4, !tbaa !9
  %cmp29 = icmp eq i32 0, %32
  br i1 %cmp29, label %land.lhs.true31, label %if.end40

land.lhs.true31:                                  ; preds = %if.end27
  %slice32 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %33 = load i8, i8* %slice32, align 4, !tbaa !11
  %conv33 = zext i8 %33 to i32
  %cmp34 = icmp eq i32 0, %conv33
  br i1 %cmp34, label %if.then36, label %if.end40

if.then36:                                        ; preds = %land.lhs.true31
  store i32 2, i32* %starting_index, align 4, !tbaa !12
  %34 = load i32, i32* %data_independent_addressing, align 4, !tbaa !12
  %tobool37 = icmp ne i32 %34, 0
  br i1 %tobool37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.then36
  call void @next_addresses(%struct.block_* %address_block, %struct.block_* %input_block, %struct.block_* %zero_block) #4
  br label %if.end39

if.end39:                                         ; preds = %if.then38, %if.then36
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %land.lhs.true31, %if.end27
  %lane41 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %35 = load i32, i32* %lane41, align 4, !tbaa !15
  %36 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %36, i32 0, i32 5
  %37 = load i32, i32* %lane_length, align 4, !tbaa !18
  %mul = mul i32 %35, %37
  %slice42 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %38 = load i8, i8* %slice42, align 4, !tbaa !11
  %conv43 = zext i8 %38 to i32
  %39 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %segment_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %39, i32 0, i32 4
  %40 = load i32, i32* %segment_length, align 4, !tbaa !19
  %mul44 = mul i32 %conv43, %40
  %add = add i32 %mul, %mul44
  %41 = load i32, i32* %starting_index, align 4, !tbaa !12
  %add45 = add i32 %add, %41
  store i32 %add45, i32* %curr_offset, align 4, !tbaa !12
  %42 = load i32, i32* %curr_offset, align 4, !tbaa !12
  %43 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length46 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %43, i32 0, i32 5
  %44 = load i32, i32* %lane_length46, align 4, !tbaa !18
  %rem = urem i32 %42, %44
  %cmp47 = icmp eq i32 0, %rem
  br i1 %cmp47, label %if.then49, label %if.else

if.then49:                                        ; preds = %if.end40
  %45 = load i32, i32* %curr_offset, align 4, !tbaa !12
  %46 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length50 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %46, i32 0, i32 5
  %47 = load i32, i32* %lane_length50, align 4, !tbaa !18
  %add51 = add i32 %45, %47
  %sub = sub i32 %add51, 1
  store i32 %sub, i32* %prev_offset, align 4, !tbaa !12
  br label %if.end53

if.else:                                          ; preds = %if.end40
  %48 = load i32, i32* %curr_offset, align 4, !tbaa !12
  %sub52 = sub i32 %48, 1
  store i32 %sub52, i32* %prev_offset, align 4, !tbaa !12
  br label %if.end53

if.end53:                                         ; preds = %if.else, %if.then49
  %49 = load i32, i32* %starting_index, align 4, !tbaa !12
  store i32 %49, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end53
  %50 = load i32, i32* %i, align 4, !tbaa !12
  %51 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %segment_length54 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %51, i32 0, i32 4
  %52 = load i32, i32* %segment_length54, align 4, !tbaa !19
  %cmp55 = icmp ult i32 %50, %52
  br i1 %cmp55, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %53 = load i32, i32* %curr_offset, align 4, !tbaa !12
  %54 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length57 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %54, i32 0, i32 5
  %55 = load i32, i32* %lane_length57, align 4, !tbaa !18
  %rem58 = urem i32 %53, %55
  %cmp59 = icmp eq i32 %rem58, 1
  br i1 %cmp59, label %if.then61, label %if.end63

if.then61:                                        ; preds = %for.body
  %56 = load i32, i32* %curr_offset, align 4, !tbaa !12
  %sub62 = sub i32 %56, 1
  store i32 %sub62, i32* %prev_offset, align 4, !tbaa !12
  br label %if.end63

if.end63:                                         ; preds = %if.then61, %for.body
  %57 = load i32, i32* %data_independent_addressing, align 4, !tbaa !12
  %tobool64 = icmp ne i32 %57, 0
  br i1 %tobool64, label %if.then65, label %if.else74

if.then65:                                        ; preds = %if.end63
  %58 = load i32, i32* %i, align 4, !tbaa !12
  %rem66 = urem i32 %58, 128
  %cmp67 = icmp eq i32 %rem66, 0
  br i1 %cmp67, label %if.then69, label %if.end70

if.then69:                                        ; preds = %if.then65
  call void @next_addresses(%struct.block_* %address_block, %struct.block_* %input_block, %struct.block_* %zero_block) #4
  br label %if.end70

if.end70:                                         ; preds = %if.then69, %if.then65
  %v71 = getelementptr inbounds %struct.block_, %struct.block_* %address_block, i32 0, i32 0
  %59 = load i32, i32* %i, align 4, !tbaa !12
  %rem72 = urem i32 %59, 128
  %arrayidx73 = getelementptr inbounds [128 x i64], [128 x i64]* %v71, i32 0, i32 %rem72
  %60 = load i64, i64* %arrayidx73, align 8, !tbaa !13
  store i64 %60, i64* %pseudo_rand, align 8, !tbaa !13
  br label %if.end78

if.else74:                                        ; preds = %if.end63
  %61 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %61, i32 0, i32 0
  %62 = load %struct.block_*, %struct.block_** %memory, align 4, !tbaa !20
  %63 = load i32, i32* %prev_offset, align 4, !tbaa !12
  %arrayidx75 = getelementptr inbounds %struct.block_, %struct.block_* %62, i32 %63
  %v76 = getelementptr inbounds %struct.block_, %struct.block_* %arrayidx75, i32 0, i32 0
  %arrayidx77 = getelementptr inbounds [128 x i64], [128 x i64]* %v76, i32 0, i32 0
  %64 = load i64, i64* %arrayidx77, align 8, !tbaa !13
  store i64 %64, i64* %pseudo_rand, align 8, !tbaa !13
  br label %if.end78

if.end78:                                         ; preds = %if.else74, %if.end70
  %65 = load i64, i64* %pseudo_rand, align 8, !tbaa !13
  %shr = lshr i64 %65, 32
  %66 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %66, i32 0, i32 6
  %67 = load i32, i32* %lanes, align 4, !tbaa !21
  %conv79 = zext i32 %67 to i64
  %rem80 = urem i64 %shr, %conv79
  store i64 %rem80, i64* %ref_lane, align 8, !tbaa !13
  %pass81 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %68 = load i32, i32* %pass81, align 4, !tbaa !9
  %cmp82 = icmp eq i32 %68, 0
  br i1 %cmp82, label %land.lhs.true84, label %if.end92

land.lhs.true84:                                  ; preds = %if.end78
  %slice85 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %69 = load i8, i8* %slice85, align 4, !tbaa !11
  %conv86 = zext i8 %69 to i32
  %cmp87 = icmp eq i32 %conv86, 0
  br i1 %cmp87, label %if.then89, label %if.end92

if.then89:                                        ; preds = %land.lhs.true84
  %lane90 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %70 = load i32, i32* %lane90, align 4, !tbaa !15
  %conv91 = zext i32 %70 to i64
  store i64 %conv91, i64* %ref_lane, align 8, !tbaa !13
  br label %if.end92

if.end92:                                         ; preds = %if.then89, %land.lhs.true84, %if.end78
  %71 = load i32, i32* %i, align 4, !tbaa !12
  %index = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 3
  store i32 %71, i32* %index, align 4, !tbaa !22
  %72 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %73 = load i64, i64* %pseudo_rand, align 8, !tbaa !13
  %and = and i64 %73, 4294967295
  %conv93 = trunc i64 %and to i32
  %74 = load i64, i64* %ref_lane, align 8, !tbaa !13
  %lane94 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %75 = load i32, i32* %lane94, align 4, !tbaa !15
  %conv95 = zext i32 %75 to i64
  %cmp96 = icmp eq i64 %74, %conv95
  %conv97 = zext i1 %cmp96 to i32
  %call = call i32 @index_alpha(%struct.Argon2_instance_t* %72, %struct.Argon2_position_t* %position, i32 %conv93, i32 %conv97) #4
  %conv98 = zext i32 %call to i64
  store i64 %conv98, i64* %ref_index, align 8, !tbaa !13
  %76 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory99 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %76, i32 0, i32 0
  %77 = load %struct.block_*, %struct.block_** %memory99, align 4, !tbaa !20
  %78 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %lane_length100 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %78, i32 0, i32 5
  %79 = load i32, i32* %lane_length100, align 4, !tbaa !18
  %conv101 = zext i32 %79 to i64
  %80 = load i64, i64* %ref_lane, align 8, !tbaa !13
  %mul102 = mul i64 %conv101, %80
  %idx.ext = trunc i64 %mul102 to i32
  %add.ptr = getelementptr inbounds %struct.block_, %struct.block_* %77, i32 %idx.ext
  %81 = load i64, i64* %ref_index, align 8, !tbaa !13
  %idx.ext103 = trunc i64 %81 to i32
  %add.ptr104 = getelementptr inbounds %struct.block_, %struct.block_* %add.ptr, i32 %idx.ext103
  store %struct.block_* %add.ptr104, %struct.block_** %ref_block, align 4, !tbaa !2
  %82 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory105 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %82, i32 0, i32 0
  %83 = load %struct.block_*, %struct.block_** %memory105, align 4, !tbaa !20
  %84 = load i32, i32* %curr_offset, align 4, !tbaa !12
  %add.ptr106 = getelementptr inbounds %struct.block_, %struct.block_* %83, i32 %84
  store %struct.block_* %add.ptr106, %struct.block_** %curr_block, align 4, !tbaa !2
  %85 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %version = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %85, i32 0, i32 1
  %86 = load i32, i32* %version, align 4, !tbaa !23
  %cmp107 = icmp eq i32 16, %86
  br i1 %cmp107, label %if.then109, label %if.else112

if.then109:                                       ; preds = %if.end92
  %87 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory110 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %87, i32 0, i32 0
  %88 = load %struct.block_*, %struct.block_** %memory110, align 4, !tbaa !20
  %89 = load i32, i32* %prev_offset, align 4, !tbaa !12
  %add.ptr111 = getelementptr inbounds %struct.block_, %struct.block_* %88, i32 %89
  %90 = load %struct.block_*, %struct.block_** %ref_block, align 4, !tbaa !2
  %91 = load %struct.block_*, %struct.block_** %curr_block, align 4, !tbaa !2
  call void @fill_block(%struct.block_* %add.ptr111, %struct.block_* %90, %struct.block_* %91, i32 0) #4
  br label %if.end123

if.else112:                                       ; preds = %if.end92
  %pass113 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %92 = load i32, i32* %pass113, align 4, !tbaa !9
  %cmp114 = icmp eq i32 0, %92
  br i1 %cmp114, label %if.then116, label %if.else119

if.then116:                                       ; preds = %if.else112
  %93 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory117 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %93, i32 0, i32 0
  %94 = load %struct.block_*, %struct.block_** %memory117, align 4, !tbaa !20
  %95 = load i32, i32* %prev_offset, align 4, !tbaa !12
  %add.ptr118 = getelementptr inbounds %struct.block_, %struct.block_* %94, i32 %95
  %96 = load %struct.block_*, %struct.block_** %ref_block, align 4, !tbaa !2
  %97 = load %struct.block_*, %struct.block_** %curr_block, align 4, !tbaa !2
  call void @fill_block(%struct.block_* %add.ptr118, %struct.block_* %96, %struct.block_* %97, i32 0) #4
  br label %if.end122

if.else119:                                       ; preds = %if.else112
  %98 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4, !tbaa !2
  %memory120 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %98, i32 0, i32 0
  %99 = load %struct.block_*, %struct.block_** %memory120, align 4, !tbaa !20
  %100 = load i32, i32* %prev_offset, align 4, !tbaa !12
  %add.ptr121 = getelementptr inbounds %struct.block_, %struct.block_* %99, i32 %100
  %101 = load %struct.block_*, %struct.block_** %ref_block, align 4, !tbaa !2
  %102 = load %struct.block_*, %struct.block_** %curr_block, align 4, !tbaa !2
  call void @fill_block(%struct.block_* %add.ptr121, %struct.block_* %101, %struct.block_* %102, i32 1) #4
  br label %if.end122

if.end122:                                        ; preds = %if.else119, %if.then116
  br label %if.end123

if.end123:                                        ; preds = %if.end122, %if.then109
  br label %for.inc

for.inc:                                          ; preds = %if.end123
  %103 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %103, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  %104 = load i32, i32* %curr_offset, align 4, !tbaa !12
  %inc124 = add i32 %104, 1
  store i32 %inc124, i32* %curr_offset, align 4, !tbaa !12
  %105 = load i32, i32* %prev_offset, align 4, !tbaa !12
  %inc125 = add i32 %105, 1
  store i32 %inc125, i32* %prev_offset, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %106 = bitcast i32* %data_independent_addressing to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  %107 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast i32* %starting_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast i32* %curr_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast i32* %prev_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast i64* %ref_lane to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %111) #3
  %112 = bitcast i64* %ref_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %112) #3
  %113 = bitcast i64* %pseudo_rand to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %113) #3
  %114 = bitcast %struct.block_* %zero_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %114) #3
  %115 = bitcast %struct.block_* %input_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %115) #3
  %116 = bitcast %struct.block_* %address_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %116) #3
  %117 = bitcast %struct.block_** %curr_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast %struct.block_** %ref_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: optsize
declare void @init_block_value(%struct.block_*, i8 zeroext) #2

; Function Attrs: noinline nounwind optsize
define internal void @next_addresses(%struct.block_* %address_block, %struct.block_* %input_block, %struct.block_* %zero_block) #0 {
entry:
  %address_block.addr = alloca %struct.block_*, align 4
  %input_block.addr = alloca %struct.block_*, align 4
  %zero_block.addr = alloca %struct.block_*, align 4
  store %struct.block_* %address_block, %struct.block_** %address_block.addr, align 4, !tbaa !2
  store %struct.block_* %input_block, %struct.block_** %input_block.addr, align 4, !tbaa !2
  store %struct.block_* %zero_block, %struct.block_** %zero_block.addr, align 4, !tbaa !2
  %0 = load %struct.block_*, %struct.block_** %input_block.addr, align 4, !tbaa !2
  %v = getelementptr inbounds %struct.block_, %struct.block_* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 6
  %1 = load i64, i64* %arrayidx, align 8, !tbaa !13
  %inc = add i64 %1, 1
  store i64 %inc, i64* %arrayidx, align 8, !tbaa !13
  %2 = load %struct.block_*, %struct.block_** %zero_block.addr, align 4, !tbaa !2
  %3 = load %struct.block_*, %struct.block_** %input_block.addr, align 4, !tbaa !2
  %4 = load %struct.block_*, %struct.block_** %address_block.addr, align 4, !tbaa !2
  call void @fill_block(%struct.block_* %2, %struct.block_* %3, %struct.block_* %4, i32 0) #4
  %5 = load %struct.block_*, %struct.block_** %zero_block.addr, align 4, !tbaa !2
  %6 = load %struct.block_*, %struct.block_** %address_block.addr, align 4, !tbaa !2
  %7 = load %struct.block_*, %struct.block_** %address_block.addr, align 4, !tbaa !2
  call void @fill_block(%struct.block_* %5, %struct.block_* %6, %struct.block_* %7, i32 0) #4
  ret void
}

; Function Attrs: optsize
declare i32 @index_alpha(%struct.Argon2_instance_t*, %struct.Argon2_position_t*, i32, i32) #2

; Function Attrs: noinline nounwind optsize
define internal void @fill_block(%struct.block_* %prev_block, %struct.block_* %ref_block, %struct.block_* %next_block, i32 %with_xor) #0 {
entry:
  %prev_block.addr = alloca %struct.block_*, align 4
  %ref_block.addr = alloca %struct.block_*, align 4
  %next_block.addr = alloca %struct.block_*, align 4
  %with_xor.addr = alloca i32, align 4
  %blockR = alloca %struct.block_, align 8
  %block_tmp = alloca %struct.block_, align 8
  %i = alloca i32, align 4
  store %struct.block_* %prev_block, %struct.block_** %prev_block.addr, align 4, !tbaa !2
  store %struct.block_* %ref_block, %struct.block_** %ref_block.addr, align 4, !tbaa !2
  store %struct.block_* %next_block, %struct.block_** %next_block.addr, align 4, !tbaa !2
  store i32 %with_xor, i32* %with_xor.addr, align 4, !tbaa !12
  %0 = bitcast %struct.block_* %blockR to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %0) #3
  %1 = bitcast %struct.block_* %block_tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %1) #3
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.block_*, %struct.block_** %ref_block.addr, align 4, !tbaa !2
  call void @copy_block(%struct.block_* %blockR, %struct.block_* %3) #4
  %4 = load %struct.block_*, %struct.block_** %prev_block.addr, align 4, !tbaa !2
  call void @xor_block(%struct.block_* %blockR, %struct.block_* %4) #4
  call void @copy_block(%struct.block_* %block_tmp, %struct.block_* %blockR) #4
  %5 = load i32, i32* %with_xor.addr, align 4, !tbaa !12
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.block_*, %struct.block_** %next_block.addr, align 4, !tbaa !2
  call void @xor_block(%struct.block_* %block_tmp, %struct.block_* %6) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %cmp = icmp ult i32 %7, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %do.body

do.body:                                          ; preds = %for.body
  br label %do.body1

do.body1:                                         ; preds = %do.body
  %v = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %8 = load i32, i32* %i, align 4, !tbaa !12
  %mul = mul i32 16, %8
  %arrayidx = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 %mul
  %9 = load i64, i64* %arrayidx, align 8, !tbaa !13
  %v2 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %10 = load i32, i32* %i, align 4, !tbaa !12
  %mul3 = mul i32 16, %10
  %add = add i32 %mul3, 4
  %arrayidx4 = getelementptr inbounds [128 x i64], [128 x i64]* %v2, i32 0, i32 %add
  %11 = load i64, i64* %arrayidx4, align 8, !tbaa !13
  %call = call i64 @fBlaMka(i64 %9, i64 %11) #4
  %v5 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %12 = load i32, i32* %i, align 4, !tbaa !12
  %mul6 = mul i32 16, %12
  %arrayidx7 = getelementptr inbounds [128 x i64], [128 x i64]* %v5, i32 0, i32 %mul6
  store i64 %call, i64* %arrayidx7, align 8, !tbaa !13
  %v8 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %13 = load i32, i32* %i, align 4, !tbaa !12
  %mul9 = mul i32 16, %13
  %add10 = add i32 %mul9, 12
  %arrayidx11 = getelementptr inbounds [128 x i64], [128 x i64]* %v8, i32 0, i32 %add10
  %14 = load i64, i64* %arrayidx11, align 8, !tbaa !13
  %v12 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %15 = load i32, i32* %i, align 4, !tbaa !12
  %mul13 = mul i32 16, %15
  %arrayidx14 = getelementptr inbounds [128 x i64], [128 x i64]* %v12, i32 0, i32 %mul13
  %16 = load i64, i64* %arrayidx14, align 8, !tbaa !13
  %xor = xor i64 %14, %16
  %call15 = call i64 @rotr64(i64 %xor, i32 32) #4
  %v16 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %17 = load i32, i32* %i, align 4, !tbaa !12
  %mul17 = mul i32 16, %17
  %add18 = add i32 %mul17, 12
  %arrayidx19 = getelementptr inbounds [128 x i64], [128 x i64]* %v16, i32 0, i32 %add18
  store i64 %call15, i64* %arrayidx19, align 8, !tbaa !13
  %v20 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %18 = load i32, i32* %i, align 4, !tbaa !12
  %mul21 = mul i32 16, %18
  %add22 = add i32 %mul21, 8
  %arrayidx23 = getelementptr inbounds [128 x i64], [128 x i64]* %v20, i32 0, i32 %add22
  %19 = load i64, i64* %arrayidx23, align 8, !tbaa !13
  %v24 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %20 = load i32, i32* %i, align 4, !tbaa !12
  %mul25 = mul i32 16, %20
  %add26 = add i32 %mul25, 12
  %arrayidx27 = getelementptr inbounds [128 x i64], [128 x i64]* %v24, i32 0, i32 %add26
  %21 = load i64, i64* %arrayidx27, align 8, !tbaa !13
  %call28 = call i64 @fBlaMka(i64 %19, i64 %21) #4
  %v29 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %22 = load i32, i32* %i, align 4, !tbaa !12
  %mul30 = mul i32 16, %22
  %add31 = add i32 %mul30, 8
  %arrayidx32 = getelementptr inbounds [128 x i64], [128 x i64]* %v29, i32 0, i32 %add31
  store i64 %call28, i64* %arrayidx32, align 8, !tbaa !13
  %v33 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %23 = load i32, i32* %i, align 4, !tbaa !12
  %mul34 = mul i32 16, %23
  %add35 = add i32 %mul34, 4
  %arrayidx36 = getelementptr inbounds [128 x i64], [128 x i64]* %v33, i32 0, i32 %add35
  %24 = load i64, i64* %arrayidx36, align 8, !tbaa !13
  %v37 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %25 = load i32, i32* %i, align 4, !tbaa !12
  %mul38 = mul i32 16, %25
  %add39 = add i32 %mul38, 8
  %arrayidx40 = getelementptr inbounds [128 x i64], [128 x i64]* %v37, i32 0, i32 %add39
  %26 = load i64, i64* %arrayidx40, align 8, !tbaa !13
  %xor41 = xor i64 %24, %26
  %call42 = call i64 @rotr64(i64 %xor41, i32 24) #4
  %v43 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %27 = load i32, i32* %i, align 4, !tbaa !12
  %mul44 = mul i32 16, %27
  %add45 = add i32 %mul44, 4
  %arrayidx46 = getelementptr inbounds [128 x i64], [128 x i64]* %v43, i32 0, i32 %add45
  store i64 %call42, i64* %arrayidx46, align 8, !tbaa !13
  %v47 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %28 = load i32, i32* %i, align 4, !tbaa !12
  %mul48 = mul i32 16, %28
  %arrayidx49 = getelementptr inbounds [128 x i64], [128 x i64]* %v47, i32 0, i32 %mul48
  %29 = load i64, i64* %arrayidx49, align 8, !tbaa !13
  %v50 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %30 = load i32, i32* %i, align 4, !tbaa !12
  %mul51 = mul i32 16, %30
  %add52 = add i32 %mul51, 4
  %arrayidx53 = getelementptr inbounds [128 x i64], [128 x i64]* %v50, i32 0, i32 %add52
  %31 = load i64, i64* %arrayidx53, align 8, !tbaa !13
  %call54 = call i64 @fBlaMka(i64 %29, i64 %31) #4
  %v55 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %32 = load i32, i32* %i, align 4, !tbaa !12
  %mul56 = mul i32 16, %32
  %arrayidx57 = getelementptr inbounds [128 x i64], [128 x i64]* %v55, i32 0, i32 %mul56
  store i64 %call54, i64* %arrayidx57, align 8, !tbaa !13
  %v58 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %33 = load i32, i32* %i, align 4, !tbaa !12
  %mul59 = mul i32 16, %33
  %add60 = add i32 %mul59, 12
  %arrayidx61 = getelementptr inbounds [128 x i64], [128 x i64]* %v58, i32 0, i32 %add60
  %34 = load i64, i64* %arrayidx61, align 8, !tbaa !13
  %v62 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %35 = load i32, i32* %i, align 4, !tbaa !12
  %mul63 = mul i32 16, %35
  %arrayidx64 = getelementptr inbounds [128 x i64], [128 x i64]* %v62, i32 0, i32 %mul63
  %36 = load i64, i64* %arrayidx64, align 8, !tbaa !13
  %xor65 = xor i64 %34, %36
  %call66 = call i64 @rotr64(i64 %xor65, i32 16) #4
  %v67 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %37 = load i32, i32* %i, align 4, !tbaa !12
  %mul68 = mul i32 16, %37
  %add69 = add i32 %mul68, 12
  %arrayidx70 = getelementptr inbounds [128 x i64], [128 x i64]* %v67, i32 0, i32 %add69
  store i64 %call66, i64* %arrayidx70, align 8, !tbaa !13
  %v71 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %38 = load i32, i32* %i, align 4, !tbaa !12
  %mul72 = mul i32 16, %38
  %add73 = add i32 %mul72, 8
  %arrayidx74 = getelementptr inbounds [128 x i64], [128 x i64]* %v71, i32 0, i32 %add73
  %39 = load i64, i64* %arrayidx74, align 8, !tbaa !13
  %v75 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %40 = load i32, i32* %i, align 4, !tbaa !12
  %mul76 = mul i32 16, %40
  %add77 = add i32 %mul76, 12
  %arrayidx78 = getelementptr inbounds [128 x i64], [128 x i64]* %v75, i32 0, i32 %add77
  %41 = load i64, i64* %arrayidx78, align 8, !tbaa !13
  %call79 = call i64 @fBlaMka(i64 %39, i64 %41) #4
  %v80 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %42 = load i32, i32* %i, align 4, !tbaa !12
  %mul81 = mul i32 16, %42
  %add82 = add i32 %mul81, 8
  %arrayidx83 = getelementptr inbounds [128 x i64], [128 x i64]* %v80, i32 0, i32 %add82
  store i64 %call79, i64* %arrayidx83, align 8, !tbaa !13
  %v84 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %43 = load i32, i32* %i, align 4, !tbaa !12
  %mul85 = mul i32 16, %43
  %add86 = add i32 %mul85, 4
  %arrayidx87 = getelementptr inbounds [128 x i64], [128 x i64]* %v84, i32 0, i32 %add86
  %44 = load i64, i64* %arrayidx87, align 8, !tbaa !13
  %v88 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %45 = load i32, i32* %i, align 4, !tbaa !12
  %mul89 = mul i32 16, %45
  %add90 = add i32 %mul89, 8
  %arrayidx91 = getelementptr inbounds [128 x i64], [128 x i64]* %v88, i32 0, i32 %add90
  %46 = load i64, i64* %arrayidx91, align 8, !tbaa !13
  %xor92 = xor i64 %44, %46
  %call93 = call i64 @rotr64(i64 %xor92, i32 63) #4
  %v94 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %47 = load i32, i32* %i, align 4, !tbaa !12
  %mul95 = mul i32 16, %47
  %add96 = add i32 %mul95, 4
  %arrayidx97 = getelementptr inbounds [128 x i64], [128 x i64]* %v94, i32 0, i32 %add96
  store i64 %call93, i64* %arrayidx97, align 8, !tbaa !13
  br label %do.cond

do.cond:                                          ; preds = %do.body1
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body98

do.body98:                                        ; preds = %do.end
  %v99 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %48 = load i32, i32* %i, align 4, !tbaa !12
  %mul100 = mul i32 16, %48
  %add101 = add i32 %mul100, 1
  %arrayidx102 = getelementptr inbounds [128 x i64], [128 x i64]* %v99, i32 0, i32 %add101
  %49 = load i64, i64* %arrayidx102, align 8, !tbaa !13
  %v103 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %50 = load i32, i32* %i, align 4, !tbaa !12
  %mul104 = mul i32 16, %50
  %add105 = add i32 %mul104, 5
  %arrayidx106 = getelementptr inbounds [128 x i64], [128 x i64]* %v103, i32 0, i32 %add105
  %51 = load i64, i64* %arrayidx106, align 8, !tbaa !13
  %call107 = call i64 @fBlaMka(i64 %49, i64 %51) #4
  %v108 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %52 = load i32, i32* %i, align 4, !tbaa !12
  %mul109 = mul i32 16, %52
  %add110 = add i32 %mul109, 1
  %arrayidx111 = getelementptr inbounds [128 x i64], [128 x i64]* %v108, i32 0, i32 %add110
  store i64 %call107, i64* %arrayidx111, align 8, !tbaa !13
  %v112 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %53 = load i32, i32* %i, align 4, !tbaa !12
  %mul113 = mul i32 16, %53
  %add114 = add i32 %mul113, 13
  %arrayidx115 = getelementptr inbounds [128 x i64], [128 x i64]* %v112, i32 0, i32 %add114
  %54 = load i64, i64* %arrayidx115, align 8, !tbaa !13
  %v116 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %55 = load i32, i32* %i, align 4, !tbaa !12
  %mul117 = mul i32 16, %55
  %add118 = add i32 %mul117, 1
  %arrayidx119 = getelementptr inbounds [128 x i64], [128 x i64]* %v116, i32 0, i32 %add118
  %56 = load i64, i64* %arrayidx119, align 8, !tbaa !13
  %xor120 = xor i64 %54, %56
  %call121 = call i64 @rotr64(i64 %xor120, i32 32) #4
  %v122 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %57 = load i32, i32* %i, align 4, !tbaa !12
  %mul123 = mul i32 16, %57
  %add124 = add i32 %mul123, 13
  %arrayidx125 = getelementptr inbounds [128 x i64], [128 x i64]* %v122, i32 0, i32 %add124
  store i64 %call121, i64* %arrayidx125, align 8, !tbaa !13
  %v126 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %58 = load i32, i32* %i, align 4, !tbaa !12
  %mul127 = mul i32 16, %58
  %add128 = add i32 %mul127, 9
  %arrayidx129 = getelementptr inbounds [128 x i64], [128 x i64]* %v126, i32 0, i32 %add128
  %59 = load i64, i64* %arrayidx129, align 8, !tbaa !13
  %v130 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %60 = load i32, i32* %i, align 4, !tbaa !12
  %mul131 = mul i32 16, %60
  %add132 = add i32 %mul131, 13
  %arrayidx133 = getelementptr inbounds [128 x i64], [128 x i64]* %v130, i32 0, i32 %add132
  %61 = load i64, i64* %arrayidx133, align 8, !tbaa !13
  %call134 = call i64 @fBlaMka(i64 %59, i64 %61) #4
  %v135 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %62 = load i32, i32* %i, align 4, !tbaa !12
  %mul136 = mul i32 16, %62
  %add137 = add i32 %mul136, 9
  %arrayidx138 = getelementptr inbounds [128 x i64], [128 x i64]* %v135, i32 0, i32 %add137
  store i64 %call134, i64* %arrayidx138, align 8, !tbaa !13
  %v139 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %63 = load i32, i32* %i, align 4, !tbaa !12
  %mul140 = mul i32 16, %63
  %add141 = add i32 %mul140, 5
  %arrayidx142 = getelementptr inbounds [128 x i64], [128 x i64]* %v139, i32 0, i32 %add141
  %64 = load i64, i64* %arrayidx142, align 8, !tbaa !13
  %v143 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %65 = load i32, i32* %i, align 4, !tbaa !12
  %mul144 = mul i32 16, %65
  %add145 = add i32 %mul144, 9
  %arrayidx146 = getelementptr inbounds [128 x i64], [128 x i64]* %v143, i32 0, i32 %add145
  %66 = load i64, i64* %arrayidx146, align 8, !tbaa !13
  %xor147 = xor i64 %64, %66
  %call148 = call i64 @rotr64(i64 %xor147, i32 24) #4
  %v149 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %67 = load i32, i32* %i, align 4, !tbaa !12
  %mul150 = mul i32 16, %67
  %add151 = add i32 %mul150, 5
  %arrayidx152 = getelementptr inbounds [128 x i64], [128 x i64]* %v149, i32 0, i32 %add151
  store i64 %call148, i64* %arrayidx152, align 8, !tbaa !13
  %v153 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %68 = load i32, i32* %i, align 4, !tbaa !12
  %mul154 = mul i32 16, %68
  %add155 = add i32 %mul154, 1
  %arrayidx156 = getelementptr inbounds [128 x i64], [128 x i64]* %v153, i32 0, i32 %add155
  %69 = load i64, i64* %arrayidx156, align 8, !tbaa !13
  %v157 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %70 = load i32, i32* %i, align 4, !tbaa !12
  %mul158 = mul i32 16, %70
  %add159 = add i32 %mul158, 5
  %arrayidx160 = getelementptr inbounds [128 x i64], [128 x i64]* %v157, i32 0, i32 %add159
  %71 = load i64, i64* %arrayidx160, align 8, !tbaa !13
  %call161 = call i64 @fBlaMka(i64 %69, i64 %71) #4
  %v162 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %72 = load i32, i32* %i, align 4, !tbaa !12
  %mul163 = mul i32 16, %72
  %add164 = add i32 %mul163, 1
  %arrayidx165 = getelementptr inbounds [128 x i64], [128 x i64]* %v162, i32 0, i32 %add164
  store i64 %call161, i64* %arrayidx165, align 8, !tbaa !13
  %v166 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %73 = load i32, i32* %i, align 4, !tbaa !12
  %mul167 = mul i32 16, %73
  %add168 = add i32 %mul167, 13
  %arrayidx169 = getelementptr inbounds [128 x i64], [128 x i64]* %v166, i32 0, i32 %add168
  %74 = load i64, i64* %arrayidx169, align 8, !tbaa !13
  %v170 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %75 = load i32, i32* %i, align 4, !tbaa !12
  %mul171 = mul i32 16, %75
  %add172 = add i32 %mul171, 1
  %arrayidx173 = getelementptr inbounds [128 x i64], [128 x i64]* %v170, i32 0, i32 %add172
  %76 = load i64, i64* %arrayidx173, align 8, !tbaa !13
  %xor174 = xor i64 %74, %76
  %call175 = call i64 @rotr64(i64 %xor174, i32 16) #4
  %v176 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %77 = load i32, i32* %i, align 4, !tbaa !12
  %mul177 = mul i32 16, %77
  %add178 = add i32 %mul177, 13
  %arrayidx179 = getelementptr inbounds [128 x i64], [128 x i64]* %v176, i32 0, i32 %add178
  store i64 %call175, i64* %arrayidx179, align 8, !tbaa !13
  %v180 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %78 = load i32, i32* %i, align 4, !tbaa !12
  %mul181 = mul i32 16, %78
  %add182 = add i32 %mul181, 9
  %arrayidx183 = getelementptr inbounds [128 x i64], [128 x i64]* %v180, i32 0, i32 %add182
  %79 = load i64, i64* %arrayidx183, align 8, !tbaa !13
  %v184 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %80 = load i32, i32* %i, align 4, !tbaa !12
  %mul185 = mul i32 16, %80
  %add186 = add i32 %mul185, 13
  %arrayidx187 = getelementptr inbounds [128 x i64], [128 x i64]* %v184, i32 0, i32 %add186
  %81 = load i64, i64* %arrayidx187, align 8, !tbaa !13
  %call188 = call i64 @fBlaMka(i64 %79, i64 %81) #4
  %v189 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %82 = load i32, i32* %i, align 4, !tbaa !12
  %mul190 = mul i32 16, %82
  %add191 = add i32 %mul190, 9
  %arrayidx192 = getelementptr inbounds [128 x i64], [128 x i64]* %v189, i32 0, i32 %add191
  store i64 %call188, i64* %arrayidx192, align 8, !tbaa !13
  %v193 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %83 = load i32, i32* %i, align 4, !tbaa !12
  %mul194 = mul i32 16, %83
  %add195 = add i32 %mul194, 5
  %arrayidx196 = getelementptr inbounds [128 x i64], [128 x i64]* %v193, i32 0, i32 %add195
  %84 = load i64, i64* %arrayidx196, align 8, !tbaa !13
  %v197 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %85 = load i32, i32* %i, align 4, !tbaa !12
  %mul198 = mul i32 16, %85
  %add199 = add i32 %mul198, 9
  %arrayidx200 = getelementptr inbounds [128 x i64], [128 x i64]* %v197, i32 0, i32 %add199
  %86 = load i64, i64* %arrayidx200, align 8, !tbaa !13
  %xor201 = xor i64 %84, %86
  %call202 = call i64 @rotr64(i64 %xor201, i32 63) #4
  %v203 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %87 = load i32, i32* %i, align 4, !tbaa !12
  %mul204 = mul i32 16, %87
  %add205 = add i32 %mul204, 5
  %arrayidx206 = getelementptr inbounds [128 x i64], [128 x i64]* %v203, i32 0, i32 %add205
  store i64 %call202, i64* %arrayidx206, align 8, !tbaa !13
  br label %do.cond207

do.cond207:                                       ; preds = %do.body98
  br label %do.end208

do.end208:                                        ; preds = %do.cond207
  br label %do.body209

do.body209:                                       ; preds = %do.end208
  %v210 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %88 = load i32, i32* %i, align 4, !tbaa !12
  %mul211 = mul i32 16, %88
  %add212 = add i32 %mul211, 2
  %arrayidx213 = getelementptr inbounds [128 x i64], [128 x i64]* %v210, i32 0, i32 %add212
  %89 = load i64, i64* %arrayidx213, align 8, !tbaa !13
  %v214 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %90 = load i32, i32* %i, align 4, !tbaa !12
  %mul215 = mul i32 16, %90
  %add216 = add i32 %mul215, 6
  %arrayidx217 = getelementptr inbounds [128 x i64], [128 x i64]* %v214, i32 0, i32 %add216
  %91 = load i64, i64* %arrayidx217, align 8, !tbaa !13
  %call218 = call i64 @fBlaMka(i64 %89, i64 %91) #4
  %v219 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %92 = load i32, i32* %i, align 4, !tbaa !12
  %mul220 = mul i32 16, %92
  %add221 = add i32 %mul220, 2
  %arrayidx222 = getelementptr inbounds [128 x i64], [128 x i64]* %v219, i32 0, i32 %add221
  store i64 %call218, i64* %arrayidx222, align 8, !tbaa !13
  %v223 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %93 = load i32, i32* %i, align 4, !tbaa !12
  %mul224 = mul i32 16, %93
  %add225 = add i32 %mul224, 14
  %arrayidx226 = getelementptr inbounds [128 x i64], [128 x i64]* %v223, i32 0, i32 %add225
  %94 = load i64, i64* %arrayidx226, align 8, !tbaa !13
  %v227 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %95 = load i32, i32* %i, align 4, !tbaa !12
  %mul228 = mul i32 16, %95
  %add229 = add i32 %mul228, 2
  %arrayidx230 = getelementptr inbounds [128 x i64], [128 x i64]* %v227, i32 0, i32 %add229
  %96 = load i64, i64* %arrayidx230, align 8, !tbaa !13
  %xor231 = xor i64 %94, %96
  %call232 = call i64 @rotr64(i64 %xor231, i32 32) #4
  %v233 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %97 = load i32, i32* %i, align 4, !tbaa !12
  %mul234 = mul i32 16, %97
  %add235 = add i32 %mul234, 14
  %arrayidx236 = getelementptr inbounds [128 x i64], [128 x i64]* %v233, i32 0, i32 %add235
  store i64 %call232, i64* %arrayidx236, align 8, !tbaa !13
  %v237 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %98 = load i32, i32* %i, align 4, !tbaa !12
  %mul238 = mul i32 16, %98
  %add239 = add i32 %mul238, 10
  %arrayidx240 = getelementptr inbounds [128 x i64], [128 x i64]* %v237, i32 0, i32 %add239
  %99 = load i64, i64* %arrayidx240, align 8, !tbaa !13
  %v241 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %100 = load i32, i32* %i, align 4, !tbaa !12
  %mul242 = mul i32 16, %100
  %add243 = add i32 %mul242, 14
  %arrayidx244 = getelementptr inbounds [128 x i64], [128 x i64]* %v241, i32 0, i32 %add243
  %101 = load i64, i64* %arrayidx244, align 8, !tbaa !13
  %call245 = call i64 @fBlaMka(i64 %99, i64 %101) #4
  %v246 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %102 = load i32, i32* %i, align 4, !tbaa !12
  %mul247 = mul i32 16, %102
  %add248 = add i32 %mul247, 10
  %arrayidx249 = getelementptr inbounds [128 x i64], [128 x i64]* %v246, i32 0, i32 %add248
  store i64 %call245, i64* %arrayidx249, align 8, !tbaa !13
  %v250 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %103 = load i32, i32* %i, align 4, !tbaa !12
  %mul251 = mul i32 16, %103
  %add252 = add i32 %mul251, 6
  %arrayidx253 = getelementptr inbounds [128 x i64], [128 x i64]* %v250, i32 0, i32 %add252
  %104 = load i64, i64* %arrayidx253, align 8, !tbaa !13
  %v254 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %105 = load i32, i32* %i, align 4, !tbaa !12
  %mul255 = mul i32 16, %105
  %add256 = add i32 %mul255, 10
  %arrayidx257 = getelementptr inbounds [128 x i64], [128 x i64]* %v254, i32 0, i32 %add256
  %106 = load i64, i64* %arrayidx257, align 8, !tbaa !13
  %xor258 = xor i64 %104, %106
  %call259 = call i64 @rotr64(i64 %xor258, i32 24) #4
  %v260 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %107 = load i32, i32* %i, align 4, !tbaa !12
  %mul261 = mul i32 16, %107
  %add262 = add i32 %mul261, 6
  %arrayidx263 = getelementptr inbounds [128 x i64], [128 x i64]* %v260, i32 0, i32 %add262
  store i64 %call259, i64* %arrayidx263, align 8, !tbaa !13
  %v264 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %108 = load i32, i32* %i, align 4, !tbaa !12
  %mul265 = mul i32 16, %108
  %add266 = add i32 %mul265, 2
  %arrayidx267 = getelementptr inbounds [128 x i64], [128 x i64]* %v264, i32 0, i32 %add266
  %109 = load i64, i64* %arrayidx267, align 8, !tbaa !13
  %v268 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %110 = load i32, i32* %i, align 4, !tbaa !12
  %mul269 = mul i32 16, %110
  %add270 = add i32 %mul269, 6
  %arrayidx271 = getelementptr inbounds [128 x i64], [128 x i64]* %v268, i32 0, i32 %add270
  %111 = load i64, i64* %arrayidx271, align 8, !tbaa !13
  %call272 = call i64 @fBlaMka(i64 %109, i64 %111) #4
  %v273 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %112 = load i32, i32* %i, align 4, !tbaa !12
  %mul274 = mul i32 16, %112
  %add275 = add i32 %mul274, 2
  %arrayidx276 = getelementptr inbounds [128 x i64], [128 x i64]* %v273, i32 0, i32 %add275
  store i64 %call272, i64* %arrayidx276, align 8, !tbaa !13
  %v277 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %113 = load i32, i32* %i, align 4, !tbaa !12
  %mul278 = mul i32 16, %113
  %add279 = add i32 %mul278, 14
  %arrayidx280 = getelementptr inbounds [128 x i64], [128 x i64]* %v277, i32 0, i32 %add279
  %114 = load i64, i64* %arrayidx280, align 8, !tbaa !13
  %v281 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %115 = load i32, i32* %i, align 4, !tbaa !12
  %mul282 = mul i32 16, %115
  %add283 = add i32 %mul282, 2
  %arrayidx284 = getelementptr inbounds [128 x i64], [128 x i64]* %v281, i32 0, i32 %add283
  %116 = load i64, i64* %arrayidx284, align 8, !tbaa !13
  %xor285 = xor i64 %114, %116
  %call286 = call i64 @rotr64(i64 %xor285, i32 16) #4
  %v287 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %117 = load i32, i32* %i, align 4, !tbaa !12
  %mul288 = mul i32 16, %117
  %add289 = add i32 %mul288, 14
  %arrayidx290 = getelementptr inbounds [128 x i64], [128 x i64]* %v287, i32 0, i32 %add289
  store i64 %call286, i64* %arrayidx290, align 8, !tbaa !13
  %v291 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %118 = load i32, i32* %i, align 4, !tbaa !12
  %mul292 = mul i32 16, %118
  %add293 = add i32 %mul292, 10
  %arrayidx294 = getelementptr inbounds [128 x i64], [128 x i64]* %v291, i32 0, i32 %add293
  %119 = load i64, i64* %arrayidx294, align 8, !tbaa !13
  %v295 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %120 = load i32, i32* %i, align 4, !tbaa !12
  %mul296 = mul i32 16, %120
  %add297 = add i32 %mul296, 14
  %arrayidx298 = getelementptr inbounds [128 x i64], [128 x i64]* %v295, i32 0, i32 %add297
  %121 = load i64, i64* %arrayidx298, align 8, !tbaa !13
  %call299 = call i64 @fBlaMka(i64 %119, i64 %121) #4
  %v300 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %122 = load i32, i32* %i, align 4, !tbaa !12
  %mul301 = mul i32 16, %122
  %add302 = add i32 %mul301, 10
  %arrayidx303 = getelementptr inbounds [128 x i64], [128 x i64]* %v300, i32 0, i32 %add302
  store i64 %call299, i64* %arrayidx303, align 8, !tbaa !13
  %v304 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %123 = load i32, i32* %i, align 4, !tbaa !12
  %mul305 = mul i32 16, %123
  %add306 = add i32 %mul305, 6
  %arrayidx307 = getelementptr inbounds [128 x i64], [128 x i64]* %v304, i32 0, i32 %add306
  %124 = load i64, i64* %arrayidx307, align 8, !tbaa !13
  %v308 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %125 = load i32, i32* %i, align 4, !tbaa !12
  %mul309 = mul i32 16, %125
  %add310 = add i32 %mul309, 10
  %arrayidx311 = getelementptr inbounds [128 x i64], [128 x i64]* %v308, i32 0, i32 %add310
  %126 = load i64, i64* %arrayidx311, align 8, !tbaa !13
  %xor312 = xor i64 %124, %126
  %call313 = call i64 @rotr64(i64 %xor312, i32 63) #4
  %v314 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %127 = load i32, i32* %i, align 4, !tbaa !12
  %mul315 = mul i32 16, %127
  %add316 = add i32 %mul315, 6
  %arrayidx317 = getelementptr inbounds [128 x i64], [128 x i64]* %v314, i32 0, i32 %add316
  store i64 %call313, i64* %arrayidx317, align 8, !tbaa !13
  br label %do.cond318

do.cond318:                                       ; preds = %do.body209
  br label %do.end319

do.end319:                                        ; preds = %do.cond318
  br label %do.body320

do.body320:                                       ; preds = %do.end319
  %v321 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %128 = load i32, i32* %i, align 4, !tbaa !12
  %mul322 = mul i32 16, %128
  %add323 = add i32 %mul322, 3
  %arrayidx324 = getelementptr inbounds [128 x i64], [128 x i64]* %v321, i32 0, i32 %add323
  %129 = load i64, i64* %arrayidx324, align 8, !tbaa !13
  %v325 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %130 = load i32, i32* %i, align 4, !tbaa !12
  %mul326 = mul i32 16, %130
  %add327 = add i32 %mul326, 7
  %arrayidx328 = getelementptr inbounds [128 x i64], [128 x i64]* %v325, i32 0, i32 %add327
  %131 = load i64, i64* %arrayidx328, align 8, !tbaa !13
  %call329 = call i64 @fBlaMka(i64 %129, i64 %131) #4
  %v330 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %132 = load i32, i32* %i, align 4, !tbaa !12
  %mul331 = mul i32 16, %132
  %add332 = add i32 %mul331, 3
  %arrayidx333 = getelementptr inbounds [128 x i64], [128 x i64]* %v330, i32 0, i32 %add332
  store i64 %call329, i64* %arrayidx333, align 8, !tbaa !13
  %v334 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %133 = load i32, i32* %i, align 4, !tbaa !12
  %mul335 = mul i32 16, %133
  %add336 = add i32 %mul335, 15
  %arrayidx337 = getelementptr inbounds [128 x i64], [128 x i64]* %v334, i32 0, i32 %add336
  %134 = load i64, i64* %arrayidx337, align 8, !tbaa !13
  %v338 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %135 = load i32, i32* %i, align 4, !tbaa !12
  %mul339 = mul i32 16, %135
  %add340 = add i32 %mul339, 3
  %arrayidx341 = getelementptr inbounds [128 x i64], [128 x i64]* %v338, i32 0, i32 %add340
  %136 = load i64, i64* %arrayidx341, align 8, !tbaa !13
  %xor342 = xor i64 %134, %136
  %call343 = call i64 @rotr64(i64 %xor342, i32 32) #4
  %v344 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %137 = load i32, i32* %i, align 4, !tbaa !12
  %mul345 = mul i32 16, %137
  %add346 = add i32 %mul345, 15
  %arrayidx347 = getelementptr inbounds [128 x i64], [128 x i64]* %v344, i32 0, i32 %add346
  store i64 %call343, i64* %arrayidx347, align 8, !tbaa !13
  %v348 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %138 = load i32, i32* %i, align 4, !tbaa !12
  %mul349 = mul i32 16, %138
  %add350 = add i32 %mul349, 11
  %arrayidx351 = getelementptr inbounds [128 x i64], [128 x i64]* %v348, i32 0, i32 %add350
  %139 = load i64, i64* %arrayidx351, align 8, !tbaa !13
  %v352 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %140 = load i32, i32* %i, align 4, !tbaa !12
  %mul353 = mul i32 16, %140
  %add354 = add i32 %mul353, 15
  %arrayidx355 = getelementptr inbounds [128 x i64], [128 x i64]* %v352, i32 0, i32 %add354
  %141 = load i64, i64* %arrayidx355, align 8, !tbaa !13
  %call356 = call i64 @fBlaMka(i64 %139, i64 %141) #4
  %v357 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %142 = load i32, i32* %i, align 4, !tbaa !12
  %mul358 = mul i32 16, %142
  %add359 = add i32 %mul358, 11
  %arrayidx360 = getelementptr inbounds [128 x i64], [128 x i64]* %v357, i32 0, i32 %add359
  store i64 %call356, i64* %arrayidx360, align 8, !tbaa !13
  %v361 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %143 = load i32, i32* %i, align 4, !tbaa !12
  %mul362 = mul i32 16, %143
  %add363 = add i32 %mul362, 7
  %arrayidx364 = getelementptr inbounds [128 x i64], [128 x i64]* %v361, i32 0, i32 %add363
  %144 = load i64, i64* %arrayidx364, align 8, !tbaa !13
  %v365 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %145 = load i32, i32* %i, align 4, !tbaa !12
  %mul366 = mul i32 16, %145
  %add367 = add i32 %mul366, 11
  %arrayidx368 = getelementptr inbounds [128 x i64], [128 x i64]* %v365, i32 0, i32 %add367
  %146 = load i64, i64* %arrayidx368, align 8, !tbaa !13
  %xor369 = xor i64 %144, %146
  %call370 = call i64 @rotr64(i64 %xor369, i32 24) #4
  %v371 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %147 = load i32, i32* %i, align 4, !tbaa !12
  %mul372 = mul i32 16, %147
  %add373 = add i32 %mul372, 7
  %arrayidx374 = getelementptr inbounds [128 x i64], [128 x i64]* %v371, i32 0, i32 %add373
  store i64 %call370, i64* %arrayidx374, align 8, !tbaa !13
  %v375 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %148 = load i32, i32* %i, align 4, !tbaa !12
  %mul376 = mul i32 16, %148
  %add377 = add i32 %mul376, 3
  %arrayidx378 = getelementptr inbounds [128 x i64], [128 x i64]* %v375, i32 0, i32 %add377
  %149 = load i64, i64* %arrayidx378, align 8, !tbaa !13
  %v379 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %150 = load i32, i32* %i, align 4, !tbaa !12
  %mul380 = mul i32 16, %150
  %add381 = add i32 %mul380, 7
  %arrayidx382 = getelementptr inbounds [128 x i64], [128 x i64]* %v379, i32 0, i32 %add381
  %151 = load i64, i64* %arrayidx382, align 8, !tbaa !13
  %call383 = call i64 @fBlaMka(i64 %149, i64 %151) #4
  %v384 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %152 = load i32, i32* %i, align 4, !tbaa !12
  %mul385 = mul i32 16, %152
  %add386 = add i32 %mul385, 3
  %arrayidx387 = getelementptr inbounds [128 x i64], [128 x i64]* %v384, i32 0, i32 %add386
  store i64 %call383, i64* %arrayidx387, align 8, !tbaa !13
  %v388 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %153 = load i32, i32* %i, align 4, !tbaa !12
  %mul389 = mul i32 16, %153
  %add390 = add i32 %mul389, 15
  %arrayidx391 = getelementptr inbounds [128 x i64], [128 x i64]* %v388, i32 0, i32 %add390
  %154 = load i64, i64* %arrayidx391, align 8, !tbaa !13
  %v392 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %155 = load i32, i32* %i, align 4, !tbaa !12
  %mul393 = mul i32 16, %155
  %add394 = add i32 %mul393, 3
  %arrayidx395 = getelementptr inbounds [128 x i64], [128 x i64]* %v392, i32 0, i32 %add394
  %156 = load i64, i64* %arrayidx395, align 8, !tbaa !13
  %xor396 = xor i64 %154, %156
  %call397 = call i64 @rotr64(i64 %xor396, i32 16) #4
  %v398 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %157 = load i32, i32* %i, align 4, !tbaa !12
  %mul399 = mul i32 16, %157
  %add400 = add i32 %mul399, 15
  %arrayidx401 = getelementptr inbounds [128 x i64], [128 x i64]* %v398, i32 0, i32 %add400
  store i64 %call397, i64* %arrayidx401, align 8, !tbaa !13
  %v402 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %158 = load i32, i32* %i, align 4, !tbaa !12
  %mul403 = mul i32 16, %158
  %add404 = add i32 %mul403, 11
  %arrayidx405 = getelementptr inbounds [128 x i64], [128 x i64]* %v402, i32 0, i32 %add404
  %159 = load i64, i64* %arrayidx405, align 8, !tbaa !13
  %v406 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %160 = load i32, i32* %i, align 4, !tbaa !12
  %mul407 = mul i32 16, %160
  %add408 = add i32 %mul407, 15
  %arrayidx409 = getelementptr inbounds [128 x i64], [128 x i64]* %v406, i32 0, i32 %add408
  %161 = load i64, i64* %arrayidx409, align 8, !tbaa !13
  %call410 = call i64 @fBlaMka(i64 %159, i64 %161) #4
  %v411 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %162 = load i32, i32* %i, align 4, !tbaa !12
  %mul412 = mul i32 16, %162
  %add413 = add i32 %mul412, 11
  %arrayidx414 = getelementptr inbounds [128 x i64], [128 x i64]* %v411, i32 0, i32 %add413
  store i64 %call410, i64* %arrayidx414, align 8, !tbaa !13
  %v415 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %163 = load i32, i32* %i, align 4, !tbaa !12
  %mul416 = mul i32 16, %163
  %add417 = add i32 %mul416, 7
  %arrayidx418 = getelementptr inbounds [128 x i64], [128 x i64]* %v415, i32 0, i32 %add417
  %164 = load i64, i64* %arrayidx418, align 8, !tbaa !13
  %v419 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %165 = load i32, i32* %i, align 4, !tbaa !12
  %mul420 = mul i32 16, %165
  %add421 = add i32 %mul420, 11
  %arrayidx422 = getelementptr inbounds [128 x i64], [128 x i64]* %v419, i32 0, i32 %add421
  %166 = load i64, i64* %arrayidx422, align 8, !tbaa !13
  %xor423 = xor i64 %164, %166
  %call424 = call i64 @rotr64(i64 %xor423, i32 63) #4
  %v425 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %167 = load i32, i32* %i, align 4, !tbaa !12
  %mul426 = mul i32 16, %167
  %add427 = add i32 %mul426, 7
  %arrayidx428 = getelementptr inbounds [128 x i64], [128 x i64]* %v425, i32 0, i32 %add427
  store i64 %call424, i64* %arrayidx428, align 8, !tbaa !13
  br label %do.cond429

do.cond429:                                       ; preds = %do.body320
  br label %do.end430

do.end430:                                        ; preds = %do.cond429
  br label %do.body431

do.body431:                                       ; preds = %do.end430
  %v432 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %168 = load i32, i32* %i, align 4, !tbaa !12
  %mul433 = mul i32 16, %168
  %arrayidx434 = getelementptr inbounds [128 x i64], [128 x i64]* %v432, i32 0, i32 %mul433
  %169 = load i64, i64* %arrayidx434, align 8, !tbaa !13
  %v435 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %170 = load i32, i32* %i, align 4, !tbaa !12
  %mul436 = mul i32 16, %170
  %add437 = add i32 %mul436, 5
  %arrayidx438 = getelementptr inbounds [128 x i64], [128 x i64]* %v435, i32 0, i32 %add437
  %171 = load i64, i64* %arrayidx438, align 8, !tbaa !13
  %call439 = call i64 @fBlaMka(i64 %169, i64 %171) #4
  %v440 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %172 = load i32, i32* %i, align 4, !tbaa !12
  %mul441 = mul i32 16, %172
  %arrayidx442 = getelementptr inbounds [128 x i64], [128 x i64]* %v440, i32 0, i32 %mul441
  store i64 %call439, i64* %arrayidx442, align 8, !tbaa !13
  %v443 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %173 = load i32, i32* %i, align 4, !tbaa !12
  %mul444 = mul i32 16, %173
  %add445 = add i32 %mul444, 15
  %arrayidx446 = getelementptr inbounds [128 x i64], [128 x i64]* %v443, i32 0, i32 %add445
  %174 = load i64, i64* %arrayidx446, align 8, !tbaa !13
  %v447 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %175 = load i32, i32* %i, align 4, !tbaa !12
  %mul448 = mul i32 16, %175
  %arrayidx449 = getelementptr inbounds [128 x i64], [128 x i64]* %v447, i32 0, i32 %mul448
  %176 = load i64, i64* %arrayidx449, align 8, !tbaa !13
  %xor450 = xor i64 %174, %176
  %call451 = call i64 @rotr64(i64 %xor450, i32 32) #4
  %v452 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %177 = load i32, i32* %i, align 4, !tbaa !12
  %mul453 = mul i32 16, %177
  %add454 = add i32 %mul453, 15
  %arrayidx455 = getelementptr inbounds [128 x i64], [128 x i64]* %v452, i32 0, i32 %add454
  store i64 %call451, i64* %arrayidx455, align 8, !tbaa !13
  %v456 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %178 = load i32, i32* %i, align 4, !tbaa !12
  %mul457 = mul i32 16, %178
  %add458 = add i32 %mul457, 10
  %arrayidx459 = getelementptr inbounds [128 x i64], [128 x i64]* %v456, i32 0, i32 %add458
  %179 = load i64, i64* %arrayidx459, align 8, !tbaa !13
  %v460 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %180 = load i32, i32* %i, align 4, !tbaa !12
  %mul461 = mul i32 16, %180
  %add462 = add i32 %mul461, 15
  %arrayidx463 = getelementptr inbounds [128 x i64], [128 x i64]* %v460, i32 0, i32 %add462
  %181 = load i64, i64* %arrayidx463, align 8, !tbaa !13
  %call464 = call i64 @fBlaMka(i64 %179, i64 %181) #4
  %v465 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %182 = load i32, i32* %i, align 4, !tbaa !12
  %mul466 = mul i32 16, %182
  %add467 = add i32 %mul466, 10
  %arrayidx468 = getelementptr inbounds [128 x i64], [128 x i64]* %v465, i32 0, i32 %add467
  store i64 %call464, i64* %arrayidx468, align 8, !tbaa !13
  %v469 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %183 = load i32, i32* %i, align 4, !tbaa !12
  %mul470 = mul i32 16, %183
  %add471 = add i32 %mul470, 5
  %arrayidx472 = getelementptr inbounds [128 x i64], [128 x i64]* %v469, i32 0, i32 %add471
  %184 = load i64, i64* %arrayidx472, align 8, !tbaa !13
  %v473 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %185 = load i32, i32* %i, align 4, !tbaa !12
  %mul474 = mul i32 16, %185
  %add475 = add i32 %mul474, 10
  %arrayidx476 = getelementptr inbounds [128 x i64], [128 x i64]* %v473, i32 0, i32 %add475
  %186 = load i64, i64* %arrayidx476, align 8, !tbaa !13
  %xor477 = xor i64 %184, %186
  %call478 = call i64 @rotr64(i64 %xor477, i32 24) #4
  %v479 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %187 = load i32, i32* %i, align 4, !tbaa !12
  %mul480 = mul i32 16, %187
  %add481 = add i32 %mul480, 5
  %arrayidx482 = getelementptr inbounds [128 x i64], [128 x i64]* %v479, i32 0, i32 %add481
  store i64 %call478, i64* %arrayidx482, align 8, !tbaa !13
  %v483 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %188 = load i32, i32* %i, align 4, !tbaa !12
  %mul484 = mul i32 16, %188
  %arrayidx485 = getelementptr inbounds [128 x i64], [128 x i64]* %v483, i32 0, i32 %mul484
  %189 = load i64, i64* %arrayidx485, align 8, !tbaa !13
  %v486 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %190 = load i32, i32* %i, align 4, !tbaa !12
  %mul487 = mul i32 16, %190
  %add488 = add i32 %mul487, 5
  %arrayidx489 = getelementptr inbounds [128 x i64], [128 x i64]* %v486, i32 0, i32 %add488
  %191 = load i64, i64* %arrayidx489, align 8, !tbaa !13
  %call490 = call i64 @fBlaMka(i64 %189, i64 %191) #4
  %v491 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %192 = load i32, i32* %i, align 4, !tbaa !12
  %mul492 = mul i32 16, %192
  %arrayidx493 = getelementptr inbounds [128 x i64], [128 x i64]* %v491, i32 0, i32 %mul492
  store i64 %call490, i64* %arrayidx493, align 8, !tbaa !13
  %v494 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %193 = load i32, i32* %i, align 4, !tbaa !12
  %mul495 = mul i32 16, %193
  %add496 = add i32 %mul495, 15
  %arrayidx497 = getelementptr inbounds [128 x i64], [128 x i64]* %v494, i32 0, i32 %add496
  %194 = load i64, i64* %arrayidx497, align 8, !tbaa !13
  %v498 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %195 = load i32, i32* %i, align 4, !tbaa !12
  %mul499 = mul i32 16, %195
  %arrayidx500 = getelementptr inbounds [128 x i64], [128 x i64]* %v498, i32 0, i32 %mul499
  %196 = load i64, i64* %arrayidx500, align 8, !tbaa !13
  %xor501 = xor i64 %194, %196
  %call502 = call i64 @rotr64(i64 %xor501, i32 16) #4
  %v503 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %197 = load i32, i32* %i, align 4, !tbaa !12
  %mul504 = mul i32 16, %197
  %add505 = add i32 %mul504, 15
  %arrayidx506 = getelementptr inbounds [128 x i64], [128 x i64]* %v503, i32 0, i32 %add505
  store i64 %call502, i64* %arrayidx506, align 8, !tbaa !13
  %v507 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %198 = load i32, i32* %i, align 4, !tbaa !12
  %mul508 = mul i32 16, %198
  %add509 = add i32 %mul508, 10
  %arrayidx510 = getelementptr inbounds [128 x i64], [128 x i64]* %v507, i32 0, i32 %add509
  %199 = load i64, i64* %arrayidx510, align 8, !tbaa !13
  %v511 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %200 = load i32, i32* %i, align 4, !tbaa !12
  %mul512 = mul i32 16, %200
  %add513 = add i32 %mul512, 15
  %arrayidx514 = getelementptr inbounds [128 x i64], [128 x i64]* %v511, i32 0, i32 %add513
  %201 = load i64, i64* %arrayidx514, align 8, !tbaa !13
  %call515 = call i64 @fBlaMka(i64 %199, i64 %201) #4
  %v516 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %202 = load i32, i32* %i, align 4, !tbaa !12
  %mul517 = mul i32 16, %202
  %add518 = add i32 %mul517, 10
  %arrayidx519 = getelementptr inbounds [128 x i64], [128 x i64]* %v516, i32 0, i32 %add518
  store i64 %call515, i64* %arrayidx519, align 8, !tbaa !13
  %v520 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %203 = load i32, i32* %i, align 4, !tbaa !12
  %mul521 = mul i32 16, %203
  %add522 = add i32 %mul521, 5
  %arrayidx523 = getelementptr inbounds [128 x i64], [128 x i64]* %v520, i32 0, i32 %add522
  %204 = load i64, i64* %arrayidx523, align 8, !tbaa !13
  %v524 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %205 = load i32, i32* %i, align 4, !tbaa !12
  %mul525 = mul i32 16, %205
  %add526 = add i32 %mul525, 10
  %arrayidx527 = getelementptr inbounds [128 x i64], [128 x i64]* %v524, i32 0, i32 %add526
  %206 = load i64, i64* %arrayidx527, align 8, !tbaa !13
  %xor528 = xor i64 %204, %206
  %call529 = call i64 @rotr64(i64 %xor528, i32 63) #4
  %v530 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %207 = load i32, i32* %i, align 4, !tbaa !12
  %mul531 = mul i32 16, %207
  %add532 = add i32 %mul531, 5
  %arrayidx533 = getelementptr inbounds [128 x i64], [128 x i64]* %v530, i32 0, i32 %add532
  store i64 %call529, i64* %arrayidx533, align 8, !tbaa !13
  br label %do.cond534

do.cond534:                                       ; preds = %do.body431
  br label %do.end535

do.end535:                                        ; preds = %do.cond534
  br label %do.body536

do.body536:                                       ; preds = %do.end535
  %v537 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %208 = load i32, i32* %i, align 4, !tbaa !12
  %mul538 = mul i32 16, %208
  %add539 = add i32 %mul538, 1
  %arrayidx540 = getelementptr inbounds [128 x i64], [128 x i64]* %v537, i32 0, i32 %add539
  %209 = load i64, i64* %arrayidx540, align 8, !tbaa !13
  %v541 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %210 = load i32, i32* %i, align 4, !tbaa !12
  %mul542 = mul i32 16, %210
  %add543 = add i32 %mul542, 6
  %arrayidx544 = getelementptr inbounds [128 x i64], [128 x i64]* %v541, i32 0, i32 %add543
  %211 = load i64, i64* %arrayidx544, align 8, !tbaa !13
  %call545 = call i64 @fBlaMka(i64 %209, i64 %211) #4
  %v546 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %212 = load i32, i32* %i, align 4, !tbaa !12
  %mul547 = mul i32 16, %212
  %add548 = add i32 %mul547, 1
  %arrayidx549 = getelementptr inbounds [128 x i64], [128 x i64]* %v546, i32 0, i32 %add548
  store i64 %call545, i64* %arrayidx549, align 8, !tbaa !13
  %v550 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %213 = load i32, i32* %i, align 4, !tbaa !12
  %mul551 = mul i32 16, %213
  %add552 = add i32 %mul551, 12
  %arrayidx553 = getelementptr inbounds [128 x i64], [128 x i64]* %v550, i32 0, i32 %add552
  %214 = load i64, i64* %arrayidx553, align 8, !tbaa !13
  %v554 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %215 = load i32, i32* %i, align 4, !tbaa !12
  %mul555 = mul i32 16, %215
  %add556 = add i32 %mul555, 1
  %arrayidx557 = getelementptr inbounds [128 x i64], [128 x i64]* %v554, i32 0, i32 %add556
  %216 = load i64, i64* %arrayidx557, align 8, !tbaa !13
  %xor558 = xor i64 %214, %216
  %call559 = call i64 @rotr64(i64 %xor558, i32 32) #4
  %v560 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %217 = load i32, i32* %i, align 4, !tbaa !12
  %mul561 = mul i32 16, %217
  %add562 = add i32 %mul561, 12
  %arrayidx563 = getelementptr inbounds [128 x i64], [128 x i64]* %v560, i32 0, i32 %add562
  store i64 %call559, i64* %arrayidx563, align 8, !tbaa !13
  %v564 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %218 = load i32, i32* %i, align 4, !tbaa !12
  %mul565 = mul i32 16, %218
  %add566 = add i32 %mul565, 11
  %arrayidx567 = getelementptr inbounds [128 x i64], [128 x i64]* %v564, i32 0, i32 %add566
  %219 = load i64, i64* %arrayidx567, align 8, !tbaa !13
  %v568 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %220 = load i32, i32* %i, align 4, !tbaa !12
  %mul569 = mul i32 16, %220
  %add570 = add i32 %mul569, 12
  %arrayidx571 = getelementptr inbounds [128 x i64], [128 x i64]* %v568, i32 0, i32 %add570
  %221 = load i64, i64* %arrayidx571, align 8, !tbaa !13
  %call572 = call i64 @fBlaMka(i64 %219, i64 %221) #4
  %v573 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %222 = load i32, i32* %i, align 4, !tbaa !12
  %mul574 = mul i32 16, %222
  %add575 = add i32 %mul574, 11
  %arrayidx576 = getelementptr inbounds [128 x i64], [128 x i64]* %v573, i32 0, i32 %add575
  store i64 %call572, i64* %arrayidx576, align 8, !tbaa !13
  %v577 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %223 = load i32, i32* %i, align 4, !tbaa !12
  %mul578 = mul i32 16, %223
  %add579 = add i32 %mul578, 6
  %arrayidx580 = getelementptr inbounds [128 x i64], [128 x i64]* %v577, i32 0, i32 %add579
  %224 = load i64, i64* %arrayidx580, align 8, !tbaa !13
  %v581 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %225 = load i32, i32* %i, align 4, !tbaa !12
  %mul582 = mul i32 16, %225
  %add583 = add i32 %mul582, 11
  %arrayidx584 = getelementptr inbounds [128 x i64], [128 x i64]* %v581, i32 0, i32 %add583
  %226 = load i64, i64* %arrayidx584, align 8, !tbaa !13
  %xor585 = xor i64 %224, %226
  %call586 = call i64 @rotr64(i64 %xor585, i32 24) #4
  %v587 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %227 = load i32, i32* %i, align 4, !tbaa !12
  %mul588 = mul i32 16, %227
  %add589 = add i32 %mul588, 6
  %arrayidx590 = getelementptr inbounds [128 x i64], [128 x i64]* %v587, i32 0, i32 %add589
  store i64 %call586, i64* %arrayidx590, align 8, !tbaa !13
  %v591 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %228 = load i32, i32* %i, align 4, !tbaa !12
  %mul592 = mul i32 16, %228
  %add593 = add i32 %mul592, 1
  %arrayidx594 = getelementptr inbounds [128 x i64], [128 x i64]* %v591, i32 0, i32 %add593
  %229 = load i64, i64* %arrayidx594, align 8, !tbaa !13
  %v595 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %230 = load i32, i32* %i, align 4, !tbaa !12
  %mul596 = mul i32 16, %230
  %add597 = add i32 %mul596, 6
  %arrayidx598 = getelementptr inbounds [128 x i64], [128 x i64]* %v595, i32 0, i32 %add597
  %231 = load i64, i64* %arrayidx598, align 8, !tbaa !13
  %call599 = call i64 @fBlaMka(i64 %229, i64 %231) #4
  %v600 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %232 = load i32, i32* %i, align 4, !tbaa !12
  %mul601 = mul i32 16, %232
  %add602 = add i32 %mul601, 1
  %arrayidx603 = getelementptr inbounds [128 x i64], [128 x i64]* %v600, i32 0, i32 %add602
  store i64 %call599, i64* %arrayidx603, align 8, !tbaa !13
  %v604 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %233 = load i32, i32* %i, align 4, !tbaa !12
  %mul605 = mul i32 16, %233
  %add606 = add i32 %mul605, 12
  %arrayidx607 = getelementptr inbounds [128 x i64], [128 x i64]* %v604, i32 0, i32 %add606
  %234 = load i64, i64* %arrayidx607, align 8, !tbaa !13
  %v608 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %235 = load i32, i32* %i, align 4, !tbaa !12
  %mul609 = mul i32 16, %235
  %add610 = add i32 %mul609, 1
  %arrayidx611 = getelementptr inbounds [128 x i64], [128 x i64]* %v608, i32 0, i32 %add610
  %236 = load i64, i64* %arrayidx611, align 8, !tbaa !13
  %xor612 = xor i64 %234, %236
  %call613 = call i64 @rotr64(i64 %xor612, i32 16) #4
  %v614 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %237 = load i32, i32* %i, align 4, !tbaa !12
  %mul615 = mul i32 16, %237
  %add616 = add i32 %mul615, 12
  %arrayidx617 = getelementptr inbounds [128 x i64], [128 x i64]* %v614, i32 0, i32 %add616
  store i64 %call613, i64* %arrayidx617, align 8, !tbaa !13
  %v618 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %238 = load i32, i32* %i, align 4, !tbaa !12
  %mul619 = mul i32 16, %238
  %add620 = add i32 %mul619, 11
  %arrayidx621 = getelementptr inbounds [128 x i64], [128 x i64]* %v618, i32 0, i32 %add620
  %239 = load i64, i64* %arrayidx621, align 8, !tbaa !13
  %v622 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %240 = load i32, i32* %i, align 4, !tbaa !12
  %mul623 = mul i32 16, %240
  %add624 = add i32 %mul623, 12
  %arrayidx625 = getelementptr inbounds [128 x i64], [128 x i64]* %v622, i32 0, i32 %add624
  %241 = load i64, i64* %arrayidx625, align 8, !tbaa !13
  %call626 = call i64 @fBlaMka(i64 %239, i64 %241) #4
  %v627 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %242 = load i32, i32* %i, align 4, !tbaa !12
  %mul628 = mul i32 16, %242
  %add629 = add i32 %mul628, 11
  %arrayidx630 = getelementptr inbounds [128 x i64], [128 x i64]* %v627, i32 0, i32 %add629
  store i64 %call626, i64* %arrayidx630, align 8, !tbaa !13
  %v631 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %243 = load i32, i32* %i, align 4, !tbaa !12
  %mul632 = mul i32 16, %243
  %add633 = add i32 %mul632, 6
  %arrayidx634 = getelementptr inbounds [128 x i64], [128 x i64]* %v631, i32 0, i32 %add633
  %244 = load i64, i64* %arrayidx634, align 8, !tbaa !13
  %v635 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %245 = load i32, i32* %i, align 4, !tbaa !12
  %mul636 = mul i32 16, %245
  %add637 = add i32 %mul636, 11
  %arrayidx638 = getelementptr inbounds [128 x i64], [128 x i64]* %v635, i32 0, i32 %add637
  %246 = load i64, i64* %arrayidx638, align 8, !tbaa !13
  %xor639 = xor i64 %244, %246
  %call640 = call i64 @rotr64(i64 %xor639, i32 63) #4
  %v641 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %247 = load i32, i32* %i, align 4, !tbaa !12
  %mul642 = mul i32 16, %247
  %add643 = add i32 %mul642, 6
  %arrayidx644 = getelementptr inbounds [128 x i64], [128 x i64]* %v641, i32 0, i32 %add643
  store i64 %call640, i64* %arrayidx644, align 8, !tbaa !13
  br label %do.cond645

do.cond645:                                       ; preds = %do.body536
  br label %do.end646

do.end646:                                        ; preds = %do.cond645
  br label %do.body647

do.body647:                                       ; preds = %do.end646
  %v648 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %248 = load i32, i32* %i, align 4, !tbaa !12
  %mul649 = mul i32 16, %248
  %add650 = add i32 %mul649, 2
  %arrayidx651 = getelementptr inbounds [128 x i64], [128 x i64]* %v648, i32 0, i32 %add650
  %249 = load i64, i64* %arrayidx651, align 8, !tbaa !13
  %v652 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %250 = load i32, i32* %i, align 4, !tbaa !12
  %mul653 = mul i32 16, %250
  %add654 = add i32 %mul653, 7
  %arrayidx655 = getelementptr inbounds [128 x i64], [128 x i64]* %v652, i32 0, i32 %add654
  %251 = load i64, i64* %arrayidx655, align 8, !tbaa !13
  %call656 = call i64 @fBlaMka(i64 %249, i64 %251) #4
  %v657 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %252 = load i32, i32* %i, align 4, !tbaa !12
  %mul658 = mul i32 16, %252
  %add659 = add i32 %mul658, 2
  %arrayidx660 = getelementptr inbounds [128 x i64], [128 x i64]* %v657, i32 0, i32 %add659
  store i64 %call656, i64* %arrayidx660, align 8, !tbaa !13
  %v661 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %253 = load i32, i32* %i, align 4, !tbaa !12
  %mul662 = mul i32 16, %253
  %add663 = add i32 %mul662, 13
  %arrayidx664 = getelementptr inbounds [128 x i64], [128 x i64]* %v661, i32 0, i32 %add663
  %254 = load i64, i64* %arrayidx664, align 8, !tbaa !13
  %v665 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %255 = load i32, i32* %i, align 4, !tbaa !12
  %mul666 = mul i32 16, %255
  %add667 = add i32 %mul666, 2
  %arrayidx668 = getelementptr inbounds [128 x i64], [128 x i64]* %v665, i32 0, i32 %add667
  %256 = load i64, i64* %arrayidx668, align 8, !tbaa !13
  %xor669 = xor i64 %254, %256
  %call670 = call i64 @rotr64(i64 %xor669, i32 32) #4
  %v671 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %257 = load i32, i32* %i, align 4, !tbaa !12
  %mul672 = mul i32 16, %257
  %add673 = add i32 %mul672, 13
  %arrayidx674 = getelementptr inbounds [128 x i64], [128 x i64]* %v671, i32 0, i32 %add673
  store i64 %call670, i64* %arrayidx674, align 8, !tbaa !13
  %v675 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %258 = load i32, i32* %i, align 4, !tbaa !12
  %mul676 = mul i32 16, %258
  %add677 = add i32 %mul676, 8
  %arrayidx678 = getelementptr inbounds [128 x i64], [128 x i64]* %v675, i32 0, i32 %add677
  %259 = load i64, i64* %arrayidx678, align 8, !tbaa !13
  %v679 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %260 = load i32, i32* %i, align 4, !tbaa !12
  %mul680 = mul i32 16, %260
  %add681 = add i32 %mul680, 13
  %arrayidx682 = getelementptr inbounds [128 x i64], [128 x i64]* %v679, i32 0, i32 %add681
  %261 = load i64, i64* %arrayidx682, align 8, !tbaa !13
  %call683 = call i64 @fBlaMka(i64 %259, i64 %261) #4
  %v684 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %262 = load i32, i32* %i, align 4, !tbaa !12
  %mul685 = mul i32 16, %262
  %add686 = add i32 %mul685, 8
  %arrayidx687 = getelementptr inbounds [128 x i64], [128 x i64]* %v684, i32 0, i32 %add686
  store i64 %call683, i64* %arrayidx687, align 8, !tbaa !13
  %v688 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %263 = load i32, i32* %i, align 4, !tbaa !12
  %mul689 = mul i32 16, %263
  %add690 = add i32 %mul689, 7
  %arrayidx691 = getelementptr inbounds [128 x i64], [128 x i64]* %v688, i32 0, i32 %add690
  %264 = load i64, i64* %arrayidx691, align 8, !tbaa !13
  %v692 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %265 = load i32, i32* %i, align 4, !tbaa !12
  %mul693 = mul i32 16, %265
  %add694 = add i32 %mul693, 8
  %arrayidx695 = getelementptr inbounds [128 x i64], [128 x i64]* %v692, i32 0, i32 %add694
  %266 = load i64, i64* %arrayidx695, align 8, !tbaa !13
  %xor696 = xor i64 %264, %266
  %call697 = call i64 @rotr64(i64 %xor696, i32 24) #4
  %v698 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %267 = load i32, i32* %i, align 4, !tbaa !12
  %mul699 = mul i32 16, %267
  %add700 = add i32 %mul699, 7
  %arrayidx701 = getelementptr inbounds [128 x i64], [128 x i64]* %v698, i32 0, i32 %add700
  store i64 %call697, i64* %arrayidx701, align 8, !tbaa !13
  %v702 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %268 = load i32, i32* %i, align 4, !tbaa !12
  %mul703 = mul i32 16, %268
  %add704 = add i32 %mul703, 2
  %arrayidx705 = getelementptr inbounds [128 x i64], [128 x i64]* %v702, i32 0, i32 %add704
  %269 = load i64, i64* %arrayidx705, align 8, !tbaa !13
  %v706 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %270 = load i32, i32* %i, align 4, !tbaa !12
  %mul707 = mul i32 16, %270
  %add708 = add i32 %mul707, 7
  %arrayidx709 = getelementptr inbounds [128 x i64], [128 x i64]* %v706, i32 0, i32 %add708
  %271 = load i64, i64* %arrayidx709, align 8, !tbaa !13
  %call710 = call i64 @fBlaMka(i64 %269, i64 %271) #4
  %v711 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %272 = load i32, i32* %i, align 4, !tbaa !12
  %mul712 = mul i32 16, %272
  %add713 = add i32 %mul712, 2
  %arrayidx714 = getelementptr inbounds [128 x i64], [128 x i64]* %v711, i32 0, i32 %add713
  store i64 %call710, i64* %arrayidx714, align 8, !tbaa !13
  %v715 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %273 = load i32, i32* %i, align 4, !tbaa !12
  %mul716 = mul i32 16, %273
  %add717 = add i32 %mul716, 13
  %arrayidx718 = getelementptr inbounds [128 x i64], [128 x i64]* %v715, i32 0, i32 %add717
  %274 = load i64, i64* %arrayidx718, align 8, !tbaa !13
  %v719 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %275 = load i32, i32* %i, align 4, !tbaa !12
  %mul720 = mul i32 16, %275
  %add721 = add i32 %mul720, 2
  %arrayidx722 = getelementptr inbounds [128 x i64], [128 x i64]* %v719, i32 0, i32 %add721
  %276 = load i64, i64* %arrayidx722, align 8, !tbaa !13
  %xor723 = xor i64 %274, %276
  %call724 = call i64 @rotr64(i64 %xor723, i32 16) #4
  %v725 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %277 = load i32, i32* %i, align 4, !tbaa !12
  %mul726 = mul i32 16, %277
  %add727 = add i32 %mul726, 13
  %arrayidx728 = getelementptr inbounds [128 x i64], [128 x i64]* %v725, i32 0, i32 %add727
  store i64 %call724, i64* %arrayidx728, align 8, !tbaa !13
  %v729 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %278 = load i32, i32* %i, align 4, !tbaa !12
  %mul730 = mul i32 16, %278
  %add731 = add i32 %mul730, 8
  %arrayidx732 = getelementptr inbounds [128 x i64], [128 x i64]* %v729, i32 0, i32 %add731
  %279 = load i64, i64* %arrayidx732, align 8, !tbaa !13
  %v733 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %280 = load i32, i32* %i, align 4, !tbaa !12
  %mul734 = mul i32 16, %280
  %add735 = add i32 %mul734, 13
  %arrayidx736 = getelementptr inbounds [128 x i64], [128 x i64]* %v733, i32 0, i32 %add735
  %281 = load i64, i64* %arrayidx736, align 8, !tbaa !13
  %call737 = call i64 @fBlaMka(i64 %279, i64 %281) #4
  %v738 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %282 = load i32, i32* %i, align 4, !tbaa !12
  %mul739 = mul i32 16, %282
  %add740 = add i32 %mul739, 8
  %arrayidx741 = getelementptr inbounds [128 x i64], [128 x i64]* %v738, i32 0, i32 %add740
  store i64 %call737, i64* %arrayidx741, align 8, !tbaa !13
  %v742 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %283 = load i32, i32* %i, align 4, !tbaa !12
  %mul743 = mul i32 16, %283
  %add744 = add i32 %mul743, 7
  %arrayidx745 = getelementptr inbounds [128 x i64], [128 x i64]* %v742, i32 0, i32 %add744
  %284 = load i64, i64* %arrayidx745, align 8, !tbaa !13
  %v746 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %285 = load i32, i32* %i, align 4, !tbaa !12
  %mul747 = mul i32 16, %285
  %add748 = add i32 %mul747, 8
  %arrayidx749 = getelementptr inbounds [128 x i64], [128 x i64]* %v746, i32 0, i32 %add748
  %286 = load i64, i64* %arrayidx749, align 8, !tbaa !13
  %xor750 = xor i64 %284, %286
  %call751 = call i64 @rotr64(i64 %xor750, i32 63) #4
  %v752 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %287 = load i32, i32* %i, align 4, !tbaa !12
  %mul753 = mul i32 16, %287
  %add754 = add i32 %mul753, 7
  %arrayidx755 = getelementptr inbounds [128 x i64], [128 x i64]* %v752, i32 0, i32 %add754
  store i64 %call751, i64* %arrayidx755, align 8, !tbaa !13
  br label %do.cond756

do.cond756:                                       ; preds = %do.body647
  br label %do.end757

do.end757:                                        ; preds = %do.cond756
  br label %do.body758

do.body758:                                       ; preds = %do.end757
  %v759 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %288 = load i32, i32* %i, align 4, !tbaa !12
  %mul760 = mul i32 16, %288
  %add761 = add i32 %mul760, 3
  %arrayidx762 = getelementptr inbounds [128 x i64], [128 x i64]* %v759, i32 0, i32 %add761
  %289 = load i64, i64* %arrayidx762, align 8, !tbaa !13
  %v763 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %290 = load i32, i32* %i, align 4, !tbaa !12
  %mul764 = mul i32 16, %290
  %add765 = add i32 %mul764, 4
  %arrayidx766 = getelementptr inbounds [128 x i64], [128 x i64]* %v763, i32 0, i32 %add765
  %291 = load i64, i64* %arrayidx766, align 8, !tbaa !13
  %call767 = call i64 @fBlaMka(i64 %289, i64 %291) #4
  %v768 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %292 = load i32, i32* %i, align 4, !tbaa !12
  %mul769 = mul i32 16, %292
  %add770 = add i32 %mul769, 3
  %arrayidx771 = getelementptr inbounds [128 x i64], [128 x i64]* %v768, i32 0, i32 %add770
  store i64 %call767, i64* %arrayidx771, align 8, !tbaa !13
  %v772 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %293 = load i32, i32* %i, align 4, !tbaa !12
  %mul773 = mul i32 16, %293
  %add774 = add i32 %mul773, 14
  %arrayidx775 = getelementptr inbounds [128 x i64], [128 x i64]* %v772, i32 0, i32 %add774
  %294 = load i64, i64* %arrayidx775, align 8, !tbaa !13
  %v776 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %295 = load i32, i32* %i, align 4, !tbaa !12
  %mul777 = mul i32 16, %295
  %add778 = add i32 %mul777, 3
  %arrayidx779 = getelementptr inbounds [128 x i64], [128 x i64]* %v776, i32 0, i32 %add778
  %296 = load i64, i64* %arrayidx779, align 8, !tbaa !13
  %xor780 = xor i64 %294, %296
  %call781 = call i64 @rotr64(i64 %xor780, i32 32) #4
  %v782 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %297 = load i32, i32* %i, align 4, !tbaa !12
  %mul783 = mul i32 16, %297
  %add784 = add i32 %mul783, 14
  %arrayidx785 = getelementptr inbounds [128 x i64], [128 x i64]* %v782, i32 0, i32 %add784
  store i64 %call781, i64* %arrayidx785, align 8, !tbaa !13
  %v786 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %298 = load i32, i32* %i, align 4, !tbaa !12
  %mul787 = mul i32 16, %298
  %add788 = add i32 %mul787, 9
  %arrayidx789 = getelementptr inbounds [128 x i64], [128 x i64]* %v786, i32 0, i32 %add788
  %299 = load i64, i64* %arrayidx789, align 8, !tbaa !13
  %v790 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %300 = load i32, i32* %i, align 4, !tbaa !12
  %mul791 = mul i32 16, %300
  %add792 = add i32 %mul791, 14
  %arrayidx793 = getelementptr inbounds [128 x i64], [128 x i64]* %v790, i32 0, i32 %add792
  %301 = load i64, i64* %arrayidx793, align 8, !tbaa !13
  %call794 = call i64 @fBlaMka(i64 %299, i64 %301) #4
  %v795 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %302 = load i32, i32* %i, align 4, !tbaa !12
  %mul796 = mul i32 16, %302
  %add797 = add i32 %mul796, 9
  %arrayidx798 = getelementptr inbounds [128 x i64], [128 x i64]* %v795, i32 0, i32 %add797
  store i64 %call794, i64* %arrayidx798, align 8, !tbaa !13
  %v799 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %303 = load i32, i32* %i, align 4, !tbaa !12
  %mul800 = mul i32 16, %303
  %add801 = add i32 %mul800, 4
  %arrayidx802 = getelementptr inbounds [128 x i64], [128 x i64]* %v799, i32 0, i32 %add801
  %304 = load i64, i64* %arrayidx802, align 8, !tbaa !13
  %v803 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %305 = load i32, i32* %i, align 4, !tbaa !12
  %mul804 = mul i32 16, %305
  %add805 = add i32 %mul804, 9
  %arrayidx806 = getelementptr inbounds [128 x i64], [128 x i64]* %v803, i32 0, i32 %add805
  %306 = load i64, i64* %arrayidx806, align 8, !tbaa !13
  %xor807 = xor i64 %304, %306
  %call808 = call i64 @rotr64(i64 %xor807, i32 24) #4
  %v809 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %307 = load i32, i32* %i, align 4, !tbaa !12
  %mul810 = mul i32 16, %307
  %add811 = add i32 %mul810, 4
  %arrayidx812 = getelementptr inbounds [128 x i64], [128 x i64]* %v809, i32 0, i32 %add811
  store i64 %call808, i64* %arrayidx812, align 8, !tbaa !13
  %v813 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %308 = load i32, i32* %i, align 4, !tbaa !12
  %mul814 = mul i32 16, %308
  %add815 = add i32 %mul814, 3
  %arrayidx816 = getelementptr inbounds [128 x i64], [128 x i64]* %v813, i32 0, i32 %add815
  %309 = load i64, i64* %arrayidx816, align 8, !tbaa !13
  %v817 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %310 = load i32, i32* %i, align 4, !tbaa !12
  %mul818 = mul i32 16, %310
  %add819 = add i32 %mul818, 4
  %arrayidx820 = getelementptr inbounds [128 x i64], [128 x i64]* %v817, i32 0, i32 %add819
  %311 = load i64, i64* %arrayidx820, align 8, !tbaa !13
  %call821 = call i64 @fBlaMka(i64 %309, i64 %311) #4
  %v822 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %312 = load i32, i32* %i, align 4, !tbaa !12
  %mul823 = mul i32 16, %312
  %add824 = add i32 %mul823, 3
  %arrayidx825 = getelementptr inbounds [128 x i64], [128 x i64]* %v822, i32 0, i32 %add824
  store i64 %call821, i64* %arrayidx825, align 8, !tbaa !13
  %v826 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %313 = load i32, i32* %i, align 4, !tbaa !12
  %mul827 = mul i32 16, %313
  %add828 = add i32 %mul827, 14
  %arrayidx829 = getelementptr inbounds [128 x i64], [128 x i64]* %v826, i32 0, i32 %add828
  %314 = load i64, i64* %arrayidx829, align 8, !tbaa !13
  %v830 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %315 = load i32, i32* %i, align 4, !tbaa !12
  %mul831 = mul i32 16, %315
  %add832 = add i32 %mul831, 3
  %arrayidx833 = getelementptr inbounds [128 x i64], [128 x i64]* %v830, i32 0, i32 %add832
  %316 = load i64, i64* %arrayidx833, align 8, !tbaa !13
  %xor834 = xor i64 %314, %316
  %call835 = call i64 @rotr64(i64 %xor834, i32 16) #4
  %v836 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %317 = load i32, i32* %i, align 4, !tbaa !12
  %mul837 = mul i32 16, %317
  %add838 = add i32 %mul837, 14
  %arrayidx839 = getelementptr inbounds [128 x i64], [128 x i64]* %v836, i32 0, i32 %add838
  store i64 %call835, i64* %arrayidx839, align 8, !tbaa !13
  %v840 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %318 = load i32, i32* %i, align 4, !tbaa !12
  %mul841 = mul i32 16, %318
  %add842 = add i32 %mul841, 9
  %arrayidx843 = getelementptr inbounds [128 x i64], [128 x i64]* %v840, i32 0, i32 %add842
  %319 = load i64, i64* %arrayidx843, align 8, !tbaa !13
  %v844 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %320 = load i32, i32* %i, align 4, !tbaa !12
  %mul845 = mul i32 16, %320
  %add846 = add i32 %mul845, 14
  %arrayidx847 = getelementptr inbounds [128 x i64], [128 x i64]* %v844, i32 0, i32 %add846
  %321 = load i64, i64* %arrayidx847, align 8, !tbaa !13
  %call848 = call i64 @fBlaMka(i64 %319, i64 %321) #4
  %v849 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %322 = load i32, i32* %i, align 4, !tbaa !12
  %mul850 = mul i32 16, %322
  %add851 = add i32 %mul850, 9
  %arrayidx852 = getelementptr inbounds [128 x i64], [128 x i64]* %v849, i32 0, i32 %add851
  store i64 %call848, i64* %arrayidx852, align 8, !tbaa !13
  %v853 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %323 = load i32, i32* %i, align 4, !tbaa !12
  %mul854 = mul i32 16, %323
  %add855 = add i32 %mul854, 4
  %arrayidx856 = getelementptr inbounds [128 x i64], [128 x i64]* %v853, i32 0, i32 %add855
  %324 = load i64, i64* %arrayidx856, align 8, !tbaa !13
  %v857 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %325 = load i32, i32* %i, align 4, !tbaa !12
  %mul858 = mul i32 16, %325
  %add859 = add i32 %mul858, 9
  %arrayidx860 = getelementptr inbounds [128 x i64], [128 x i64]* %v857, i32 0, i32 %add859
  %326 = load i64, i64* %arrayidx860, align 8, !tbaa !13
  %xor861 = xor i64 %324, %326
  %call862 = call i64 @rotr64(i64 %xor861, i32 63) #4
  %v863 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %327 = load i32, i32* %i, align 4, !tbaa !12
  %mul864 = mul i32 16, %327
  %add865 = add i32 %mul864, 4
  %arrayidx866 = getelementptr inbounds [128 x i64], [128 x i64]* %v863, i32 0, i32 %add865
  store i64 %call862, i64* %arrayidx866, align 8, !tbaa !13
  br label %do.cond867

do.cond867:                                       ; preds = %do.body758
  br label %do.end868

do.end868:                                        ; preds = %do.cond867
  br label %do.cond869

do.cond869:                                       ; preds = %do.end868
  br label %do.end870

do.end870:                                        ; preds = %do.cond869
  br label %for.inc

for.inc:                                          ; preds = %do.end870
  %328 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %328, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond871

for.cond871:                                      ; preds = %for.inc1753, %for.end
  %329 = load i32, i32* %i, align 4, !tbaa !12
  %cmp872 = icmp ult i32 %329, 8
  br i1 %cmp872, label %for.body873, label %for.end1755

for.body873:                                      ; preds = %for.cond871
  br label %do.body874

do.body874:                                       ; preds = %for.body873
  br label %do.body875

do.body875:                                       ; preds = %do.body874
  %v876 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %330 = load i32, i32* %i, align 4, !tbaa !12
  %mul877 = mul i32 2, %330
  %arrayidx878 = getelementptr inbounds [128 x i64], [128 x i64]* %v876, i32 0, i32 %mul877
  %331 = load i64, i64* %arrayidx878, align 8, !tbaa !13
  %v879 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %332 = load i32, i32* %i, align 4, !tbaa !12
  %mul880 = mul i32 2, %332
  %add881 = add i32 %mul880, 32
  %arrayidx882 = getelementptr inbounds [128 x i64], [128 x i64]* %v879, i32 0, i32 %add881
  %333 = load i64, i64* %arrayidx882, align 8, !tbaa !13
  %call883 = call i64 @fBlaMka(i64 %331, i64 %333) #4
  %v884 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %334 = load i32, i32* %i, align 4, !tbaa !12
  %mul885 = mul i32 2, %334
  %arrayidx886 = getelementptr inbounds [128 x i64], [128 x i64]* %v884, i32 0, i32 %mul885
  store i64 %call883, i64* %arrayidx886, align 8, !tbaa !13
  %v887 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %335 = load i32, i32* %i, align 4, !tbaa !12
  %mul888 = mul i32 2, %335
  %add889 = add i32 %mul888, 96
  %arrayidx890 = getelementptr inbounds [128 x i64], [128 x i64]* %v887, i32 0, i32 %add889
  %336 = load i64, i64* %arrayidx890, align 8, !tbaa !13
  %v891 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %337 = load i32, i32* %i, align 4, !tbaa !12
  %mul892 = mul i32 2, %337
  %arrayidx893 = getelementptr inbounds [128 x i64], [128 x i64]* %v891, i32 0, i32 %mul892
  %338 = load i64, i64* %arrayidx893, align 8, !tbaa !13
  %xor894 = xor i64 %336, %338
  %call895 = call i64 @rotr64(i64 %xor894, i32 32) #4
  %v896 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %339 = load i32, i32* %i, align 4, !tbaa !12
  %mul897 = mul i32 2, %339
  %add898 = add i32 %mul897, 96
  %arrayidx899 = getelementptr inbounds [128 x i64], [128 x i64]* %v896, i32 0, i32 %add898
  store i64 %call895, i64* %arrayidx899, align 8, !tbaa !13
  %v900 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %340 = load i32, i32* %i, align 4, !tbaa !12
  %mul901 = mul i32 2, %340
  %add902 = add i32 %mul901, 64
  %arrayidx903 = getelementptr inbounds [128 x i64], [128 x i64]* %v900, i32 0, i32 %add902
  %341 = load i64, i64* %arrayidx903, align 8, !tbaa !13
  %v904 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %342 = load i32, i32* %i, align 4, !tbaa !12
  %mul905 = mul i32 2, %342
  %add906 = add i32 %mul905, 96
  %arrayidx907 = getelementptr inbounds [128 x i64], [128 x i64]* %v904, i32 0, i32 %add906
  %343 = load i64, i64* %arrayidx907, align 8, !tbaa !13
  %call908 = call i64 @fBlaMka(i64 %341, i64 %343) #4
  %v909 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %344 = load i32, i32* %i, align 4, !tbaa !12
  %mul910 = mul i32 2, %344
  %add911 = add i32 %mul910, 64
  %arrayidx912 = getelementptr inbounds [128 x i64], [128 x i64]* %v909, i32 0, i32 %add911
  store i64 %call908, i64* %arrayidx912, align 8, !tbaa !13
  %v913 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %345 = load i32, i32* %i, align 4, !tbaa !12
  %mul914 = mul i32 2, %345
  %add915 = add i32 %mul914, 32
  %arrayidx916 = getelementptr inbounds [128 x i64], [128 x i64]* %v913, i32 0, i32 %add915
  %346 = load i64, i64* %arrayidx916, align 8, !tbaa !13
  %v917 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %347 = load i32, i32* %i, align 4, !tbaa !12
  %mul918 = mul i32 2, %347
  %add919 = add i32 %mul918, 64
  %arrayidx920 = getelementptr inbounds [128 x i64], [128 x i64]* %v917, i32 0, i32 %add919
  %348 = load i64, i64* %arrayidx920, align 8, !tbaa !13
  %xor921 = xor i64 %346, %348
  %call922 = call i64 @rotr64(i64 %xor921, i32 24) #4
  %v923 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %349 = load i32, i32* %i, align 4, !tbaa !12
  %mul924 = mul i32 2, %349
  %add925 = add i32 %mul924, 32
  %arrayidx926 = getelementptr inbounds [128 x i64], [128 x i64]* %v923, i32 0, i32 %add925
  store i64 %call922, i64* %arrayidx926, align 8, !tbaa !13
  %v927 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %350 = load i32, i32* %i, align 4, !tbaa !12
  %mul928 = mul i32 2, %350
  %arrayidx929 = getelementptr inbounds [128 x i64], [128 x i64]* %v927, i32 0, i32 %mul928
  %351 = load i64, i64* %arrayidx929, align 8, !tbaa !13
  %v930 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %352 = load i32, i32* %i, align 4, !tbaa !12
  %mul931 = mul i32 2, %352
  %add932 = add i32 %mul931, 32
  %arrayidx933 = getelementptr inbounds [128 x i64], [128 x i64]* %v930, i32 0, i32 %add932
  %353 = load i64, i64* %arrayidx933, align 8, !tbaa !13
  %call934 = call i64 @fBlaMka(i64 %351, i64 %353) #4
  %v935 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %354 = load i32, i32* %i, align 4, !tbaa !12
  %mul936 = mul i32 2, %354
  %arrayidx937 = getelementptr inbounds [128 x i64], [128 x i64]* %v935, i32 0, i32 %mul936
  store i64 %call934, i64* %arrayidx937, align 8, !tbaa !13
  %v938 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %355 = load i32, i32* %i, align 4, !tbaa !12
  %mul939 = mul i32 2, %355
  %add940 = add i32 %mul939, 96
  %arrayidx941 = getelementptr inbounds [128 x i64], [128 x i64]* %v938, i32 0, i32 %add940
  %356 = load i64, i64* %arrayidx941, align 8, !tbaa !13
  %v942 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %357 = load i32, i32* %i, align 4, !tbaa !12
  %mul943 = mul i32 2, %357
  %arrayidx944 = getelementptr inbounds [128 x i64], [128 x i64]* %v942, i32 0, i32 %mul943
  %358 = load i64, i64* %arrayidx944, align 8, !tbaa !13
  %xor945 = xor i64 %356, %358
  %call946 = call i64 @rotr64(i64 %xor945, i32 16) #4
  %v947 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %359 = load i32, i32* %i, align 4, !tbaa !12
  %mul948 = mul i32 2, %359
  %add949 = add i32 %mul948, 96
  %arrayidx950 = getelementptr inbounds [128 x i64], [128 x i64]* %v947, i32 0, i32 %add949
  store i64 %call946, i64* %arrayidx950, align 8, !tbaa !13
  %v951 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %360 = load i32, i32* %i, align 4, !tbaa !12
  %mul952 = mul i32 2, %360
  %add953 = add i32 %mul952, 64
  %arrayidx954 = getelementptr inbounds [128 x i64], [128 x i64]* %v951, i32 0, i32 %add953
  %361 = load i64, i64* %arrayidx954, align 8, !tbaa !13
  %v955 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %362 = load i32, i32* %i, align 4, !tbaa !12
  %mul956 = mul i32 2, %362
  %add957 = add i32 %mul956, 96
  %arrayidx958 = getelementptr inbounds [128 x i64], [128 x i64]* %v955, i32 0, i32 %add957
  %363 = load i64, i64* %arrayidx958, align 8, !tbaa !13
  %call959 = call i64 @fBlaMka(i64 %361, i64 %363) #4
  %v960 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %364 = load i32, i32* %i, align 4, !tbaa !12
  %mul961 = mul i32 2, %364
  %add962 = add i32 %mul961, 64
  %arrayidx963 = getelementptr inbounds [128 x i64], [128 x i64]* %v960, i32 0, i32 %add962
  store i64 %call959, i64* %arrayidx963, align 8, !tbaa !13
  %v964 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %365 = load i32, i32* %i, align 4, !tbaa !12
  %mul965 = mul i32 2, %365
  %add966 = add i32 %mul965, 32
  %arrayidx967 = getelementptr inbounds [128 x i64], [128 x i64]* %v964, i32 0, i32 %add966
  %366 = load i64, i64* %arrayidx967, align 8, !tbaa !13
  %v968 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %367 = load i32, i32* %i, align 4, !tbaa !12
  %mul969 = mul i32 2, %367
  %add970 = add i32 %mul969, 64
  %arrayidx971 = getelementptr inbounds [128 x i64], [128 x i64]* %v968, i32 0, i32 %add970
  %368 = load i64, i64* %arrayidx971, align 8, !tbaa !13
  %xor972 = xor i64 %366, %368
  %call973 = call i64 @rotr64(i64 %xor972, i32 63) #4
  %v974 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %369 = load i32, i32* %i, align 4, !tbaa !12
  %mul975 = mul i32 2, %369
  %add976 = add i32 %mul975, 32
  %arrayidx977 = getelementptr inbounds [128 x i64], [128 x i64]* %v974, i32 0, i32 %add976
  store i64 %call973, i64* %arrayidx977, align 8, !tbaa !13
  br label %do.cond978

do.cond978:                                       ; preds = %do.body875
  br label %do.end979

do.end979:                                        ; preds = %do.cond978
  br label %do.body980

do.body980:                                       ; preds = %do.end979
  %v981 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %370 = load i32, i32* %i, align 4, !tbaa !12
  %mul982 = mul i32 2, %370
  %add983 = add i32 %mul982, 1
  %arrayidx984 = getelementptr inbounds [128 x i64], [128 x i64]* %v981, i32 0, i32 %add983
  %371 = load i64, i64* %arrayidx984, align 8, !tbaa !13
  %v985 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %372 = load i32, i32* %i, align 4, !tbaa !12
  %mul986 = mul i32 2, %372
  %add987 = add i32 %mul986, 33
  %arrayidx988 = getelementptr inbounds [128 x i64], [128 x i64]* %v985, i32 0, i32 %add987
  %373 = load i64, i64* %arrayidx988, align 8, !tbaa !13
  %call989 = call i64 @fBlaMka(i64 %371, i64 %373) #4
  %v990 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %374 = load i32, i32* %i, align 4, !tbaa !12
  %mul991 = mul i32 2, %374
  %add992 = add i32 %mul991, 1
  %arrayidx993 = getelementptr inbounds [128 x i64], [128 x i64]* %v990, i32 0, i32 %add992
  store i64 %call989, i64* %arrayidx993, align 8, !tbaa !13
  %v994 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %375 = load i32, i32* %i, align 4, !tbaa !12
  %mul995 = mul i32 2, %375
  %add996 = add i32 %mul995, 97
  %arrayidx997 = getelementptr inbounds [128 x i64], [128 x i64]* %v994, i32 0, i32 %add996
  %376 = load i64, i64* %arrayidx997, align 8, !tbaa !13
  %v998 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %377 = load i32, i32* %i, align 4, !tbaa !12
  %mul999 = mul i32 2, %377
  %add1000 = add i32 %mul999, 1
  %arrayidx1001 = getelementptr inbounds [128 x i64], [128 x i64]* %v998, i32 0, i32 %add1000
  %378 = load i64, i64* %arrayidx1001, align 8, !tbaa !13
  %xor1002 = xor i64 %376, %378
  %call1003 = call i64 @rotr64(i64 %xor1002, i32 32) #4
  %v1004 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %379 = load i32, i32* %i, align 4, !tbaa !12
  %mul1005 = mul i32 2, %379
  %add1006 = add i32 %mul1005, 97
  %arrayidx1007 = getelementptr inbounds [128 x i64], [128 x i64]* %v1004, i32 0, i32 %add1006
  store i64 %call1003, i64* %arrayidx1007, align 8, !tbaa !13
  %v1008 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %380 = load i32, i32* %i, align 4, !tbaa !12
  %mul1009 = mul i32 2, %380
  %add1010 = add i32 %mul1009, 65
  %arrayidx1011 = getelementptr inbounds [128 x i64], [128 x i64]* %v1008, i32 0, i32 %add1010
  %381 = load i64, i64* %arrayidx1011, align 8, !tbaa !13
  %v1012 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %382 = load i32, i32* %i, align 4, !tbaa !12
  %mul1013 = mul i32 2, %382
  %add1014 = add i32 %mul1013, 97
  %arrayidx1015 = getelementptr inbounds [128 x i64], [128 x i64]* %v1012, i32 0, i32 %add1014
  %383 = load i64, i64* %arrayidx1015, align 8, !tbaa !13
  %call1016 = call i64 @fBlaMka(i64 %381, i64 %383) #4
  %v1017 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %384 = load i32, i32* %i, align 4, !tbaa !12
  %mul1018 = mul i32 2, %384
  %add1019 = add i32 %mul1018, 65
  %arrayidx1020 = getelementptr inbounds [128 x i64], [128 x i64]* %v1017, i32 0, i32 %add1019
  store i64 %call1016, i64* %arrayidx1020, align 8, !tbaa !13
  %v1021 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %385 = load i32, i32* %i, align 4, !tbaa !12
  %mul1022 = mul i32 2, %385
  %add1023 = add i32 %mul1022, 33
  %arrayidx1024 = getelementptr inbounds [128 x i64], [128 x i64]* %v1021, i32 0, i32 %add1023
  %386 = load i64, i64* %arrayidx1024, align 8, !tbaa !13
  %v1025 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %387 = load i32, i32* %i, align 4, !tbaa !12
  %mul1026 = mul i32 2, %387
  %add1027 = add i32 %mul1026, 65
  %arrayidx1028 = getelementptr inbounds [128 x i64], [128 x i64]* %v1025, i32 0, i32 %add1027
  %388 = load i64, i64* %arrayidx1028, align 8, !tbaa !13
  %xor1029 = xor i64 %386, %388
  %call1030 = call i64 @rotr64(i64 %xor1029, i32 24) #4
  %v1031 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %389 = load i32, i32* %i, align 4, !tbaa !12
  %mul1032 = mul i32 2, %389
  %add1033 = add i32 %mul1032, 33
  %arrayidx1034 = getelementptr inbounds [128 x i64], [128 x i64]* %v1031, i32 0, i32 %add1033
  store i64 %call1030, i64* %arrayidx1034, align 8, !tbaa !13
  %v1035 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %390 = load i32, i32* %i, align 4, !tbaa !12
  %mul1036 = mul i32 2, %390
  %add1037 = add i32 %mul1036, 1
  %arrayidx1038 = getelementptr inbounds [128 x i64], [128 x i64]* %v1035, i32 0, i32 %add1037
  %391 = load i64, i64* %arrayidx1038, align 8, !tbaa !13
  %v1039 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %392 = load i32, i32* %i, align 4, !tbaa !12
  %mul1040 = mul i32 2, %392
  %add1041 = add i32 %mul1040, 33
  %arrayidx1042 = getelementptr inbounds [128 x i64], [128 x i64]* %v1039, i32 0, i32 %add1041
  %393 = load i64, i64* %arrayidx1042, align 8, !tbaa !13
  %call1043 = call i64 @fBlaMka(i64 %391, i64 %393) #4
  %v1044 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %394 = load i32, i32* %i, align 4, !tbaa !12
  %mul1045 = mul i32 2, %394
  %add1046 = add i32 %mul1045, 1
  %arrayidx1047 = getelementptr inbounds [128 x i64], [128 x i64]* %v1044, i32 0, i32 %add1046
  store i64 %call1043, i64* %arrayidx1047, align 8, !tbaa !13
  %v1048 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %395 = load i32, i32* %i, align 4, !tbaa !12
  %mul1049 = mul i32 2, %395
  %add1050 = add i32 %mul1049, 97
  %arrayidx1051 = getelementptr inbounds [128 x i64], [128 x i64]* %v1048, i32 0, i32 %add1050
  %396 = load i64, i64* %arrayidx1051, align 8, !tbaa !13
  %v1052 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %397 = load i32, i32* %i, align 4, !tbaa !12
  %mul1053 = mul i32 2, %397
  %add1054 = add i32 %mul1053, 1
  %arrayidx1055 = getelementptr inbounds [128 x i64], [128 x i64]* %v1052, i32 0, i32 %add1054
  %398 = load i64, i64* %arrayidx1055, align 8, !tbaa !13
  %xor1056 = xor i64 %396, %398
  %call1057 = call i64 @rotr64(i64 %xor1056, i32 16) #4
  %v1058 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %399 = load i32, i32* %i, align 4, !tbaa !12
  %mul1059 = mul i32 2, %399
  %add1060 = add i32 %mul1059, 97
  %arrayidx1061 = getelementptr inbounds [128 x i64], [128 x i64]* %v1058, i32 0, i32 %add1060
  store i64 %call1057, i64* %arrayidx1061, align 8, !tbaa !13
  %v1062 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %400 = load i32, i32* %i, align 4, !tbaa !12
  %mul1063 = mul i32 2, %400
  %add1064 = add i32 %mul1063, 65
  %arrayidx1065 = getelementptr inbounds [128 x i64], [128 x i64]* %v1062, i32 0, i32 %add1064
  %401 = load i64, i64* %arrayidx1065, align 8, !tbaa !13
  %v1066 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %402 = load i32, i32* %i, align 4, !tbaa !12
  %mul1067 = mul i32 2, %402
  %add1068 = add i32 %mul1067, 97
  %arrayidx1069 = getelementptr inbounds [128 x i64], [128 x i64]* %v1066, i32 0, i32 %add1068
  %403 = load i64, i64* %arrayidx1069, align 8, !tbaa !13
  %call1070 = call i64 @fBlaMka(i64 %401, i64 %403) #4
  %v1071 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %404 = load i32, i32* %i, align 4, !tbaa !12
  %mul1072 = mul i32 2, %404
  %add1073 = add i32 %mul1072, 65
  %arrayidx1074 = getelementptr inbounds [128 x i64], [128 x i64]* %v1071, i32 0, i32 %add1073
  store i64 %call1070, i64* %arrayidx1074, align 8, !tbaa !13
  %v1075 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %405 = load i32, i32* %i, align 4, !tbaa !12
  %mul1076 = mul i32 2, %405
  %add1077 = add i32 %mul1076, 33
  %arrayidx1078 = getelementptr inbounds [128 x i64], [128 x i64]* %v1075, i32 0, i32 %add1077
  %406 = load i64, i64* %arrayidx1078, align 8, !tbaa !13
  %v1079 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %407 = load i32, i32* %i, align 4, !tbaa !12
  %mul1080 = mul i32 2, %407
  %add1081 = add i32 %mul1080, 65
  %arrayidx1082 = getelementptr inbounds [128 x i64], [128 x i64]* %v1079, i32 0, i32 %add1081
  %408 = load i64, i64* %arrayidx1082, align 8, !tbaa !13
  %xor1083 = xor i64 %406, %408
  %call1084 = call i64 @rotr64(i64 %xor1083, i32 63) #4
  %v1085 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %409 = load i32, i32* %i, align 4, !tbaa !12
  %mul1086 = mul i32 2, %409
  %add1087 = add i32 %mul1086, 33
  %arrayidx1088 = getelementptr inbounds [128 x i64], [128 x i64]* %v1085, i32 0, i32 %add1087
  store i64 %call1084, i64* %arrayidx1088, align 8, !tbaa !13
  br label %do.cond1089

do.cond1089:                                      ; preds = %do.body980
  br label %do.end1090

do.end1090:                                       ; preds = %do.cond1089
  br label %do.body1091

do.body1091:                                      ; preds = %do.end1090
  %v1092 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %410 = load i32, i32* %i, align 4, !tbaa !12
  %mul1093 = mul i32 2, %410
  %add1094 = add i32 %mul1093, 16
  %arrayidx1095 = getelementptr inbounds [128 x i64], [128 x i64]* %v1092, i32 0, i32 %add1094
  %411 = load i64, i64* %arrayidx1095, align 8, !tbaa !13
  %v1096 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %412 = load i32, i32* %i, align 4, !tbaa !12
  %mul1097 = mul i32 2, %412
  %add1098 = add i32 %mul1097, 48
  %arrayidx1099 = getelementptr inbounds [128 x i64], [128 x i64]* %v1096, i32 0, i32 %add1098
  %413 = load i64, i64* %arrayidx1099, align 8, !tbaa !13
  %call1100 = call i64 @fBlaMka(i64 %411, i64 %413) #4
  %v1101 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %414 = load i32, i32* %i, align 4, !tbaa !12
  %mul1102 = mul i32 2, %414
  %add1103 = add i32 %mul1102, 16
  %arrayidx1104 = getelementptr inbounds [128 x i64], [128 x i64]* %v1101, i32 0, i32 %add1103
  store i64 %call1100, i64* %arrayidx1104, align 8, !tbaa !13
  %v1105 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %415 = load i32, i32* %i, align 4, !tbaa !12
  %mul1106 = mul i32 2, %415
  %add1107 = add i32 %mul1106, 112
  %arrayidx1108 = getelementptr inbounds [128 x i64], [128 x i64]* %v1105, i32 0, i32 %add1107
  %416 = load i64, i64* %arrayidx1108, align 8, !tbaa !13
  %v1109 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %417 = load i32, i32* %i, align 4, !tbaa !12
  %mul1110 = mul i32 2, %417
  %add1111 = add i32 %mul1110, 16
  %arrayidx1112 = getelementptr inbounds [128 x i64], [128 x i64]* %v1109, i32 0, i32 %add1111
  %418 = load i64, i64* %arrayidx1112, align 8, !tbaa !13
  %xor1113 = xor i64 %416, %418
  %call1114 = call i64 @rotr64(i64 %xor1113, i32 32) #4
  %v1115 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %419 = load i32, i32* %i, align 4, !tbaa !12
  %mul1116 = mul i32 2, %419
  %add1117 = add i32 %mul1116, 112
  %arrayidx1118 = getelementptr inbounds [128 x i64], [128 x i64]* %v1115, i32 0, i32 %add1117
  store i64 %call1114, i64* %arrayidx1118, align 8, !tbaa !13
  %v1119 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %420 = load i32, i32* %i, align 4, !tbaa !12
  %mul1120 = mul i32 2, %420
  %add1121 = add i32 %mul1120, 80
  %arrayidx1122 = getelementptr inbounds [128 x i64], [128 x i64]* %v1119, i32 0, i32 %add1121
  %421 = load i64, i64* %arrayidx1122, align 8, !tbaa !13
  %v1123 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %422 = load i32, i32* %i, align 4, !tbaa !12
  %mul1124 = mul i32 2, %422
  %add1125 = add i32 %mul1124, 112
  %arrayidx1126 = getelementptr inbounds [128 x i64], [128 x i64]* %v1123, i32 0, i32 %add1125
  %423 = load i64, i64* %arrayidx1126, align 8, !tbaa !13
  %call1127 = call i64 @fBlaMka(i64 %421, i64 %423) #4
  %v1128 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %424 = load i32, i32* %i, align 4, !tbaa !12
  %mul1129 = mul i32 2, %424
  %add1130 = add i32 %mul1129, 80
  %arrayidx1131 = getelementptr inbounds [128 x i64], [128 x i64]* %v1128, i32 0, i32 %add1130
  store i64 %call1127, i64* %arrayidx1131, align 8, !tbaa !13
  %v1132 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %425 = load i32, i32* %i, align 4, !tbaa !12
  %mul1133 = mul i32 2, %425
  %add1134 = add i32 %mul1133, 48
  %arrayidx1135 = getelementptr inbounds [128 x i64], [128 x i64]* %v1132, i32 0, i32 %add1134
  %426 = load i64, i64* %arrayidx1135, align 8, !tbaa !13
  %v1136 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %427 = load i32, i32* %i, align 4, !tbaa !12
  %mul1137 = mul i32 2, %427
  %add1138 = add i32 %mul1137, 80
  %arrayidx1139 = getelementptr inbounds [128 x i64], [128 x i64]* %v1136, i32 0, i32 %add1138
  %428 = load i64, i64* %arrayidx1139, align 8, !tbaa !13
  %xor1140 = xor i64 %426, %428
  %call1141 = call i64 @rotr64(i64 %xor1140, i32 24) #4
  %v1142 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %429 = load i32, i32* %i, align 4, !tbaa !12
  %mul1143 = mul i32 2, %429
  %add1144 = add i32 %mul1143, 48
  %arrayidx1145 = getelementptr inbounds [128 x i64], [128 x i64]* %v1142, i32 0, i32 %add1144
  store i64 %call1141, i64* %arrayidx1145, align 8, !tbaa !13
  %v1146 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %430 = load i32, i32* %i, align 4, !tbaa !12
  %mul1147 = mul i32 2, %430
  %add1148 = add i32 %mul1147, 16
  %arrayidx1149 = getelementptr inbounds [128 x i64], [128 x i64]* %v1146, i32 0, i32 %add1148
  %431 = load i64, i64* %arrayidx1149, align 8, !tbaa !13
  %v1150 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %432 = load i32, i32* %i, align 4, !tbaa !12
  %mul1151 = mul i32 2, %432
  %add1152 = add i32 %mul1151, 48
  %arrayidx1153 = getelementptr inbounds [128 x i64], [128 x i64]* %v1150, i32 0, i32 %add1152
  %433 = load i64, i64* %arrayidx1153, align 8, !tbaa !13
  %call1154 = call i64 @fBlaMka(i64 %431, i64 %433) #4
  %v1155 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %434 = load i32, i32* %i, align 4, !tbaa !12
  %mul1156 = mul i32 2, %434
  %add1157 = add i32 %mul1156, 16
  %arrayidx1158 = getelementptr inbounds [128 x i64], [128 x i64]* %v1155, i32 0, i32 %add1157
  store i64 %call1154, i64* %arrayidx1158, align 8, !tbaa !13
  %v1159 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %435 = load i32, i32* %i, align 4, !tbaa !12
  %mul1160 = mul i32 2, %435
  %add1161 = add i32 %mul1160, 112
  %arrayidx1162 = getelementptr inbounds [128 x i64], [128 x i64]* %v1159, i32 0, i32 %add1161
  %436 = load i64, i64* %arrayidx1162, align 8, !tbaa !13
  %v1163 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %437 = load i32, i32* %i, align 4, !tbaa !12
  %mul1164 = mul i32 2, %437
  %add1165 = add i32 %mul1164, 16
  %arrayidx1166 = getelementptr inbounds [128 x i64], [128 x i64]* %v1163, i32 0, i32 %add1165
  %438 = load i64, i64* %arrayidx1166, align 8, !tbaa !13
  %xor1167 = xor i64 %436, %438
  %call1168 = call i64 @rotr64(i64 %xor1167, i32 16) #4
  %v1169 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %439 = load i32, i32* %i, align 4, !tbaa !12
  %mul1170 = mul i32 2, %439
  %add1171 = add i32 %mul1170, 112
  %arrayidx1172 = getelementptr inbounds [128 x i64], [128 x i64]* %v1169, i32 0, i32 %add1171
  store i64 %call1168, i64* %arrayidx1172, align 8, !tbaa !13
  %v1173 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %440 = load i32, i32* %i, align 4, !tbaa !12
  %mul1174 = mul i32 2, %440
  %add1175 = add i32 %mul1174, 80
  %arrayidx1176 = getelementptr inbounds [128 x i64], [128 x i64]* %v1173, i32 0, i32 %add1175
  %441 = load i64, i64* %arrayidx1176, align 8, !tbaa !13
  %v1177 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %442 = load i32, i32* %i, align 4, !tbaa !12
  %mul1178 = mul i32 2, %442
  %add1179 = add i32 %mul1178, 112
  %arrayidx1180 = getelementptr inbounds [128 x i64], [128 x i64]* %v1177, i32 0, i32 %add1179
  %443 = load i64, i64* %arrayidx1180, align 8, !tbaa !13
  %call1181 = call i64 @fBlaMka(i64 %441, i64 %443) #4
  %v1182 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %444 = load i32, i32* %i, align 4, !tbaa !12
  %mul1183 = mul i32 2, %444
  %add1184 = add i32 %mul1183, 80
  %arrayidx1185 = getelementptr inbounds [128 x i64], [128 x i64]* %v1182, i32 0, i32 %add1184
  store i64 %call1181, i64* %arrayidx1185, align 8, !tbaa !13
  %v1186 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %445 = load i32, i32* %i, align 4, !tbaa !12
  %mul1187 = mul i32 2, %445
  %add1188 = add i32 %mul1187, 48
  %arrayidx1189 = getelementptr inbounds [128 x i64], [128 x i64]* %v1186, i32 0, i32 %add1188
  %446 = load i64, i64* %arrayidx1189, align 8, !tbaa !13
  %v1190 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %447 = load i32, i32* %i, align 4, !tbaa !12
  %mul1191 = mul i32 2, %447
  %add1192 = add i32 %mul1191, 80
  %arrayidx1193 = getelementptr inbounds [128 x i64], [128 x i64]* %v1190, i32 0, i32 %add1192
  %448 = load i64, i64* %arrayidx1193, align 8, !tbaa !13
  %xor1194 = xor i64 %446, %448
  %call1195 = call i64 @rotr64(i64 %xor1194, i32 63) #4
  %v1196 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %449 = load i32, i32* %i, align 4, !tbaa !12
  %mul1197 = mul i32 2, %449
  %add1198 = add i32 %mul1197, 48
  %arrayidx1199 = getelementptr inbounds [128 x i64], [128 x i64]* %v1196, i32 0, i32 %add1198
  store i64 %call1195, i64* %arrayidx1199, align 8, !tbaa !13
  br label %do.cond1200

do.cond1200:                                      ; preds = %do.body1091
  br label %do.end1201

do.end1201:                                       ; preds = %do.cond1200
  br label %do.body1202

do.body1202:                                      ; preds = %do.end1201
  %v1203 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %450 = load i32, i32* %i, align 4, !tbaa !12
  %mul1204 = mul i32 2, %450
  %add1205 = add i32 %mul1204, 17
  %arrayidx1206 = getelementptr inbounds [128 x i64], [128 x i64]* %v1203, i32 0, i32 %add1205
  %451 = load i64, i64* %arrayidx1206, align 8, !tbaa !13
  %v1207 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %452 = load i32, i32* %i, align 4, !tbaa !12
  %mul1208 = mul i32 2, %452
  %add1209 = add i32 %mul1208, 49
  %arrayidx1210 = getelementptr inbounds [128 x i64], [128 x i64]* %v1207, i32 0, i32 %add1209
  %453 = load i64, i64* %arrayidx1210, align 8, !tbaa !13
  %call1211 = call i64 @fBlaMka(i64 %451, i64 %453) #4
  %v1212 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %454 = load i32, i32* %i, align 4, !tbaa !12
  %mul1213 = mul i32 2, %454
  %add1214 = add i32 %mul1213, 17
  %arrayidx1215 = getelementptr inbounds [128 x i64], [128 x i64]* %v1212, i32 0, i32 %add1214
  store i64 %call1211, i64* %arrayidx1215, align 8, !tbaa !13
  %v1216 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %455 = load i32, i32* %i, align 4, !tbaa !12
  %mul1217 = mul i32 2, %455
  %add1218 = add i32 %mul1217, 113
  %arrayidx1219 = getelementptr inbounds [128 x i64], [128 x i64]* %v1216, i32 0, i32 %add1218
  %456 = load i64, i64* %arrayidx1219, align 8, !tbaa !13
  %v1220 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %457 = load i32, i32* %i, align 4, !tbaa !12
  %mul1221 = mul i32 2, %457
  %add1222 = add i32 %mul1221, 17
  %arrayidx1223 = getelementptr inbounds [128 x i64], [128 x i64]* %v1220, i32 0, i32 %add1222
  %458 = load i64, i64* %arrayidx1223, align 8, !tbaa !13
  %xor1224 = xor i64 %456, %458
  %call1225 = call i64 @rotr64(i64 %xor1224, i32 32) #4
  %v1226 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %459 = load i32, i32* %i, align 4, !tbaa !12
  %mul1227 = mul i32 2, %459
  %add1228 = add i32 %mul1227, 113
  %arrayidx1229 = getelementptr inbounds [128 x i64], [128 x i64]* %v1226, i32 0, i32 %add1228
  store i64 %call1225, i64* %arrayidx1229, align 8, !tbaa !13
  %v1230 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %460 = load i32, i32* %i, align 4, !tbaa !12
  %mul1231 = mul i32 2, %460
  %add1232 = add i32 %mul1231, 81
  %arrayidx1233 = getelementptr inbounds [128 x i64], [128 x i64]* %v1230, i32 0, i32 %add1232
  %461 = load i64, i64* %arrayidx1233, align 8, !tbaa !13
  %v1234 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %462 = load i32, i32* %i, align 4, !tbaa !12
  %mul1235 = mul i32 2, %462
  %add1236 = add i32 %mul1235, 113
  %arrayidx1237 = getelementptr inbounds [128 x i64], [128 x i64]* %v1234, i32 0, i32 %add1236
  %463 = load i64, i64* %arrayidx1237, align 8, !tbaa !13
  %call1238 = call i64 @fBlaMka(i64 %461, i64 %463) #4
  %v1239 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %464 = load i32, i32* %i, align 4, !tbaa !12
  %mul1240 = mul i32 2, %464
  %add1241 = add i32 %mul1240, 81
  %arrayidx1242 = getelementptr inbounds [128 x i64], [128 x i64]* %v1239, i32 0, i32 %add1241
  store i64 %call1238, i64* %arrayidx1242, align 8, !tbaa !13
  %v1243 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %465 = load i32, i32* %i, align 4, !tbaa !12
  %mul1244 = mul i32 2, %465
  %add1245 = add i32 %mul1244, 49
  %arrayidx1246 = getelementptr inbounds [128 x i64], [128 x i64]* %v1243, i32 0, i32 %add1245
  %466 = load i64, i64* %arrayidx1246, align 8, !tbaa !13
  %v1247 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %467 = load i32, i32* %i, align 4, !tbaa !12
  %mul1248 = mul i32 2, %467
  %add1249 = add i32 %mul1248, 81
  %arrayidx1250 = getelementptr inbounds [128 x i64], [128 x i64]* %v1247, i32 0, i32 %add1249
  %468 = load i64, i64* %arrayidx1250, align 8, !tbaa !13
  %xor1251 = xor i64 %466, %468
  %call1252 = call i64 @rotr64(i64 %xor1251, i32 24) #4
  %v1253 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %469 = load i32, i32* %i, align 4, !tbaa !12
  %mul1254 = mul i32 2, %469
  %add1255 = add i32 %mul1254, 49
  %arrayidx1256 = getelementptr inbounds [128 x i64], [128 x i64]* %v1253, i32 0, i32 %add1255
  store i64 %call1252, i64* %arrayidx1256, align 8, !tbaa !13
  %v1257 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %470 = load i32, i32* %i, align 4, !tbaa !12
  %mul1258 = mul i32 2, %470
  %add1259 = add i32 %mul1258, 17
  %arrayidx1260 = getelementptr inbounds [128 x i64], [128 x i64]* %v1257, i32 0, i32 %add1259
  %471 = load i64, i64* %arrayidx1260, align 8, !tbaa !13
  %v1261 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %472 = load i32, i32* %i, align 4, !tbaa !12
  %mul1262 = mul i32 2, %472
  %add1263 = add i32 %mul1262, 49
  %arrayidx1264 = getelementptr inbounds [128 x i64], [128 x i64]* %v1261, i32 0, i32 %add1263
  %473 = load i64, i64* %arrayidx1264, align 8, !tbaa !13
  %call1265 = call i64 @fBlaMka(i64 %471, i64 %473) #4
  %v1266 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %474 = load i32, i32* %i, align 4, !tbaa !12
  %mul1267 = mul i32 2, %474
  %add1268 = add i32 %mul1267, 17
  %arrayidx1269 = getelementptr inbounds [128 x i64], [128 x i64]* %v1266, i32 0, i32 %add1268
  store i64 %call1265, i64* %arrayidx1269, align 8, !tbaa !13
  %v1270 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %475 = load i32, i32* %i, align 4, !tbaa !12
  %mul1271 = mul i32 2, %475
  %add1272 = add i32 %mul1271, 113
  %arrayidx1273 = getelementptr inbounds [128 x i64], [128 x i64]* %v1270, i32 0, i32 %add1272
  %476 = load i64, i64* %arrayidx1273, align 8, !tbaa !13
  %v1274 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %477 = load i32, i32* %i, align 4, !tbaa !12
  %mul1275 = mul i32 2, %477
  %add1276 = add i32 %mul1275, 17
  %arrayidx1277 = getelementptr inbounds [128 x i64], [128 x i64]* %v1274, i32 0, i32 %add1276
  %478 = load i64, i64* %arrayidx1277, align 8, !tbaa !13
  %xor1278 = xor i64 %476, %478
  %call1279 = call i64 @rotr64(i64 %xor1278, i32 16) #4
  %v1280 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %479 = load i32, i32* %i, align 4, !tbaa !12
  %mul1281 = mul i32 2, %479
  %add1282 = add i32 %mul1281, 113
  %arrayidx1283 = getelementptr inbounds [128 x i64], [128 x i64]* %v1280, i32 0, i32 %add1282
  store i64 %call1279, i64* %arrayidx1283, align 8, !tbaa !13
  %v1284 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %480 = load i32, i32* %i, align 4, !tbaa !12
  %mul1285 = mul i32 2, %480
  %add1286 = add i32 %mul1285, 81
  %arrayidx1287 = getelementptr inbounds [128 x i64], [128 x i64]* %v1284, i32 0, i32 %add1286
  %481 = load i64, i64* %arrayidx1287, align 8, !tbaa !13
  %v1288 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %482 = load i32, i32* %i, align 4, !tbaa !12
  %mul1289 = mul i32 2, %482
  %add1290 = add i32 %mul1289, 113
  %arrayidx1291 = getelementptr inbounds [128 x i64], [128 x i64]* %v1288, i32 0, i32 %add1290
  %483 = load i64, i64* %arrayidx1291, align 8, !tbaa !13
  %call1292 = call i64 @fBlaMka(i64 %481, i64 %483) #4
  %v1293 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %484 = load i32, i32* %i, align 4, !tbaa !12
  %mul1294 = mul i32 2, %484
  %add1295 = add i32 %mul1294, 81
  %arrayidx1296 = getelementptr inbounds [128 x i64], [128 x i64]* %v1293, i32 0, i32 %add1295
  store i64 %call1292, i64* %arrayidx1296, align 8, !tbaa !13
  %v1297 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %485 = load i32, i32* %i, align 4, !tbaa !12
  %mul1298 = mul i32 2, %485
  %add1299 = add i32 %mul1298, 49
  %arrayidx1300 = getelementptr inbounds [128 x i64], [128 x i64]* %v1297, i32 0, i32 %add1299
  %486 = load i64, i64* %arrayidx1300, align 8, !tbaa !13
  %v1301 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %487 = load i32, i32* %i, align 4, !tbaa !12
  %mul1302 = mul i32 2, %487
  %add1303 = add i32 %mul1302, 81
  %arrayidx1304 = getelementptr inbounds [128 x i64], [128 x i64]* %v1301, i32 0, i32 %add1303
  %488 = load i64, i64* %arrayidx1304, align 8, !tbaa !13
  %xor1305 = xor i64 %486, %488
  %call1306 = call i64 @rotr64(i64 %xor1305, i32 63) #4
  %v1307 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %489 = load i32, i32* %i, align 4, !tbaa !12
  %mul1308 = mul i32 2, %489
  %add1309 = add i32 %mul1308, 49
  %arrayidx1310 = getelementptr inbounds [128 x i64], [128 x i64]* %v1307, i32 0, i32 %add1309
  store i64 %call1306, i64* %arrayidx1310, align 8, !tbaa !13
  br label %do.cond1311

do.cond1311:                                      ; preds = %do.body1202
  br label %do.end1312

do.end1312:                                       ; preds = %do.cond1311
  br label %do.body1313

do.body1313:                                      ; preds = %do.end1312
  %v1314 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %490 = load i32, i32* %i, align 4, !tbaa !12
  %mul1315 = mul i32 2, %490
  %arrayidx1316 = getelementptr inbounds [128 x i64], [128 x i64]* %v1314, i32 0, i32 %mul1315
  %491 = load i64, i64* %arrayidx1316, align 8, !tbaa !13
  %v1317 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %492 = load i32, i32* %i, align 4, !tbaa !12
  %mul1318 = mul i32 2, %492
  %add1319 = add i32 %mul1318, 33
  %arrayidx1320 = getelementptr inbounds [128 x i64], [128 x i64]* %v1317, i32 0, i32 %add1319
  %493 = load i64, i64* %arrayidx1320, align 8, !tbaa !13
  %call1321 = call i64 @fBlaMka(i64 %491, i64 %493) #4
  %v1322 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %494 = load i32, i32* %i, align 4, !tbaa !12
  %mul1323 = mul i32 2, %494
  %arrayidx1324 = getelementptr inbounds [128 x i64], [128 x i64]* %v1322, i32 0, i32 %mul1323
  store i64 %call1321, i64* %arrayidx1324, align 8, !tbaa !13
  %v1325 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %495 = load i32, i32* %i, align 4, !tbaa !12
  %mul1326 = mul i32 2, %495
  %add1327 = add i32 %mul1326, 113
  %arrayidx1328 = getelementptr inbounds [128 x i64], [128 x i64]* %v1325, i32 0, i32 %add1327
  %496 = load i64, i64* %arrayidx1328, align 8, !tbaa !13
  %v1329 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %497 = load i32, i32* %i, align 4, !tbaa !12
  %mul1330 = mul i32 2, %497
  %arrayidx1331 = getelementptr inbounds [128 x i64], [128 x i64]* %v1329, i32 0, i32 %mul1330
  %498 = load i64, i64* %arrayidx1331, align 8, !tbaa !13
  %xor1332 = xor i64 %496, %498
  %call1333 = call i64 @rotr64(i64 %xor1332, i32 32) #4
  %v1334 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %499 = load i32, i32* %i, align 4, !tbaa !12
  %mul1335 = mul i32 2, %499
  %add1336 = add i32 %mul1335, 113
  %arrayidx1337 = getelementptr inbounds [128 x i64], [128 x i64]* %v1334, i32 0, i32 %add1336
  store i64 %call1333, i64* %arrayidx1337, align 8, !tbaa !13
  %v1338 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %500 = load i32, i32* %i, align 4, !tbaa !12
  %mul1339 = mul i32 2, %500
  %add1340 = add i32 %mul1339, 80
  %arrayidx1341 = getelementptr inbounds [128 x i64], [128 x i64]* %v1338, i32 0, i32 %add1340
  %501 = load i64, i64* %arrayidx1341, align 8, !tbaa !13
  %v1342 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %502 = load i32, i32* %i, align 4, !tbaa !12
  %mul1343 = mul i32 2, %502
  %add1344 = add i32 %mul1343, 113
  %arrayidx1345 = getelementptr inbounds [128 x i64], [128 x i64]* %v1342, i32 0, i32 %add1344
  %503 = load i64, i64* %arrayidx1345, align 8, !tbaa !13
  %call1346 = call i64 @fBlaMka(i64 %501, i64 %503) #4
  %v1347 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %504 = load i32, i32* %i, align 4, !tbaa !12
  %mul1348 = mul i32 2, %504
  %add1349 = add i32 %mul1348, 80
  %arrayidx1350 = getelementptr inbounds [128 x i64], [128 x i64]* %v1347, i32 0, i32 %add1349
  store i64 %call1346, i64* %arrayidx1350, align 8, !tbaa !13
  %v1351 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %505 = load i32, i32* %i, align 4, !tbaa !12
  %mul1352 = mul i32 2, %505
  %add1353 = add i32 %mul1352, 33
  %arrayidx1354 = getelementptr inbounds [128 x i64], [128 x i64]* %v1351, i32 0, i32 %add1353
  %506 = load i64, i64* %arrayidx1354, align 8, !tbaa !13
  %v1355 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %507 = load i32, i32* %i, align 4, !tbaa !12
  %mul1356 = mul i32 2, %507
  %add1357 = add i32 %mul1356, 80
  %arrayidx1358 = getelementptr inbounds [128 x i64], [128 x i64]* %v1355, i32 0, i32 %add1357
  %508 = load i64, i64* %arrayidx1358, align 8, !tbaa !13
  %xor1359 = xor i64 %506, %508
  %call1360 = call i64 @rotr64(i64 %xor1359, i32 24) #4
  %v1361 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %509 = load i32, i32* %i, align 4, !tbaa !12
  %mul1362 = mul i32 2, %509
  %add1363 = add i32 %mul1362, 33
  %arrayidx1364 = getelementptr inbounds [128 x i64], [128 x i64]* %v1361, i32 0, i32 %add1363
  store i64 %call1360, i64* %arrayidx1364, align 8, !tbaa !13
  %v1365 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %510 = load i32, i32* %i, align 4, !tbaa !12
  %mul1366 = mul i32 2, %510
  %arrayidx1367 = getelementptr inbounds [128 x i64], [128 x i64]* %v1365, i32 0, i32 %mul1366
  %511 = load i64, i64* %arrayidx1367, align 8, !tbaa !13
  %v1368 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %512 = load i32, i32* %i, align 4, !tbaa !12
  %mul1369 = mul i32 2, %512
  %add1370 = add i32 %mul1369, 33
  %arrayidx1371 = getelementptr inbounds [128 x i64], [128 x i64]* %v1368, i32 0, i32 %add1370
  %513 = load i64, i64* %arrayidx1371, align 8, !tbaa !13
  %call1372 = call i64 @fBlaMka(i64 %511, i64 %513) #4
  %v1373 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %514 = load i32, i32* %i, align 4, !tbaa !12
  %mul1374 = mul i32 2, %514
  %arrayidx1375 = getelementptr inbounds [128 x i64], [128 x i64]* %v1373, i32 0, i32 %mul1374
  store i64 %call1372, i64* %arrayidx1375, align 8, !tbaa !13
  %v1376 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %515 = load i32, i32* %i, align 4, !tbaa !12
  %mul1377 = mul i32 2, %515
  %add1378 = add i32 %mul1377, 113
  %arrayidx1379 = getelementptr inbounds [128 x i64], [128 x i64]* %v1376, i32 0, i32 %add1378
  %516 = load i64, i64* %arrayidx1379, align 8, !tbaa !13
  %v1380 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %517 = load i32, i32* %i, align 4, !tbaa !12
  %mul1381 = mul i32 2, %517
  %arrayidx1382 = getelementptr inbounds [128 x i64], [128 x i64]* %v1380, i32 0, i32 %mul1381
  %518 = load i64, i64* %arrayidx1382, align 8, !tbaa !13
  %xor1383 = xor i64 %516, %518
  %call1384 = call i64 @rotr64(i64 %xor1383, i32 16) #4
  %v1385 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %519 = load i32, i32* %i, align 4, !tbaa !12
  %mul1386 = mul i32 2, %519
  %add1387 = add i32 %mul1386, 113
  %arrayidx1388 = getelementptr inbounds [128 x i64], [128 x i64]* %v1385, i32 0, i32 %add1387
  store i64 %call1384, i64* %arrayidx1388, align 8, !tbaa !13
  %v1389 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %520 = load i32, i32* %i, align 4, !tbaa !12
  %mul1390 = mul i32 2, %520
  %add1391 = add i32 %mul1390, 80
  %arrayidx1392 = getelementptr inbounds [128 x i64], [128 x i64]* %v1389, i32 0, i32 %add1391
  %521 = load i64, i64* %arrayidx1392, align 8, !tbaa !13
  %v1393 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %522 = load i32, i32* %i, align 4, !tbaa !12
  %mul1394 = mul i32 2, %522
  %add1395 = add i32 %mul1394, 113
  %arrayidx1396 = getelementptr inbounds [128 x i64], [128 x i64]* %v1393, i32 0, i32 %add1395
  %523 = load i64, i64* %arrayidx1396, align 8, !tbaa !13
  %call1397 = call i64 @fBlaMka(i64 %521, i64 %523) #4
  %v1398 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %524 = load i32, i32* %i, align 4, !tbaa !12
  %mul1399 = mul i32 2, %524
  %add1400 = add i32 %mul1399, 80
  %arrayidx1401 = getelementptr inbounds [128 x i64], [128 x i64]* %v1398, i32 0, i32 %add1400
  store i64 %call1397, i64* %arrayidx1401, align 8, !tbaa !13
  %v1402 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %525 = load i32, i32* %i, align 4, !tbaa !12
  %mul1403 = mul i32 2, %525
  %add1404 = add i32 %mul1403, 33
  %arrayidx1405 = getelementptr inbounds [128 x i64], [128 x i64]* %v1402, i32 0, i32 %add1404
  %526 = load i64, i64* %arrayidx1405, align 8, !tbaa !13
  %v1406 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %527 = load i32, i32* %i, align 4, !tbaa !12
  %mul1407 = mul i32 2, %527
  %add1408 = add i32 %mul1407, 80
  %arrayidx1409 = getelementptr inbounds [128 x i64], [128 x i64]* %v1406, i32 0, i32 %add1408
  %528 = load i64, i64* %arrayidx1409, align 8, !tbaa !13
  %xor1410 = xor i64 %526, %528
  %call1411 = call i64 @rotr64(i64 %xor1410, i32 63) #4
  %v1412 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %529 = load i32, i32* %i, align 4, !tbaa !12
  %mul1413 = mul i32 2, %529
  %add1414 = add i32 %mul1413, 33
  %arrayidx1415 = getelementptr inbounds [128 x i64], [128 x i64]* %v1412, i32 0, i32 %add1414
  store i64 %call1411, i64* %arrayidx1415, align 8, !tbaa !13
  br label %do.cond1416

do.cond1416:                                      ; preds = %do.body1313
  br label %do.end1417

do.end1417:                                       ; preds = %do.cond1416
  br label %do.body1418

do.body1418:                                      ; preds = %do.end1417
  %v1419 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %530 = load i32, i32* %i, align 4, !tbaa !12
  %mul1420 = mul i32 2, %530
  %add1421 = add i32 %mul1420, 1
  %arrayidx1422 = getelementptr inbounds [128 x i64], [128 x i64]* %v1419, i32 0, i32 %add1421
  %531 = load i64, i64* %arrayidx1422, align 8, !tbaa !13
  %v1423 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %532 = load i32, i32* %i, align 4, !tbaa !12
  %mul1424 = mul i32 2, %532
  %add1425 = add i32 %mul1424, 48
  %arrayidx1426 = getelementptr inbounds [128 x i64], [128 x i64]* %v1423, i32 0, i32 %add1425
  %533 = load i64, i64* %arrayidx1426, align 8, !tbaa !13
  %call1427 = call i64 @fBlaMka(i64 %531, i64 %533) #4
  %v1428 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %534 = load i32, i32* %i, align 4, !tbaa !12
  %mul1429 = mul i32 2, %534
  %add1430 = add i32 %mul1429, 1
  %arrayidx1431 = getelementptr inbounds [128 x i64], [128 x i64]* %v1428, i32 0, i32 %add1430
  store i64 %call1427, i64* %arrayidx1431, align 8, !tbaa !13
  %v1432 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %535 = load i32, i32* %i, align 4, !tbaa !12
  %mul1433 = mul i32 2, %535
  %add1434 = add i32 %mul1433, 96
  %arrayidx1435 = getelementptr inbounds [128 x i64], [128 x i64]* %v1432, i32 0, i32 %add1434
  %536 = load i64, i64* %arrayidx1435, align 8, !tbaa !13
  %v1436 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %537 = load i32, i32* %i, align 4, !tbaa !12
  %mul1437 = mul i32 2, %537
  %add1438 = add i32 %mul1437, 1
  %arrayidx1439 = getelementptr inbounds [128 x i64], [128 x i64]* %v1436, i32 0, i32 %add1438
  %538 = load i64, i64* %arrayidx1439, align 8, !tbaa !13
  %xor1440 = xor i64 %536, %538
  %call1441 = call i64 @rotr64(i64 %xor1440, i32 32) #4
  %v1442 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %539 = load i32, i32* %i, align 4, !tbaa !12
  %mul1443 = mul i32 2, %539
  %add1444 = add i32 %mul1443, 96
  %arrayidx1445 = getelementptr inbounds [128 x i64], [128 x i64]* %v1442, i32 0, i32 %add1444
  store i64 %call1441, i64* %arrayidx1445, align 8, !tbaa !13
  %v1446 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %540 = load i32, i32* %i, align 4, !tbaa !12
  %mul1447 = mul i32 2, %540
  %add1448 = add i32 %mul1447, 81
  %arrayidx1449 = getelementptr inbounds [128 x i64], [128 x i64]* %v1446, i32 0, i32 %add1448
  %541 = load i64, i64* %arrayidx1449, align 8, !tbaa !13
  %v1450 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %542 = load i32, i32* %i, align 4, !tbaa !12
  %mul1451 = mul i32 2, %542
  %add1452 = add i32 %mul1451, 96
  %arrayidx1453 = getelementptr inbounds [128 x i64], [128 x i64]* %v1450, i32 0, i32 %add1452
  %543 = load i64, i64* %arrayidx1453, align 8, !tbaa !13
  %call1454 = call i64 @fBlaMka(i64 %541, i64 %543) #4
  %v1455 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %544 = load i32, i32* %i, align 4, !tbaa !12
  %mul1456 = mul i32 2, %544
  %add1457 = add i32 %mul1456, 81
  %arrayidx1458 = getelementptr inbounds [128 x i64], [128 x i64]* %v1455, i32 0, i32 %add1457
  store i64 %call1454, i64* %arrayidx1458, align 8, !tbaa !13
  %v1459 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %545 = load i32, i32* %i, align 4, !tbaa !12
  %mul1460 = mul i32 2, %545
  %add1461 = add i32 %mul1460, 48
  %arrayidx1462 = getelementptr inbounds [128 x i64], [128 x i64]* %v1459, i32 0, i32 %add1461
  %546 = load i64, i64* %arrayidx1462, align 8, !tbaa !13
  %v1463 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %547 = load i32, i32* %i, align 4, !tbaa !12
  %mul1464 = mul i32 2, %547
  %add1465 = add i32 %mul1464, 81
  %arrayidx1466 = getelementptr inbounds [128 x i64], [128 x i64]* %v1463, i32 0, i32 %add1465
  %548 = load i64, i64* %arrayidx1466, align 8, !tbaa !13
  %xor1467 = xor i64 %546, %548
  %call1468 = call i64 @rotr64(i64 %xor1467, i32 24) #4
  %v1469 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %549 = load i32, i32* %i, align 4, !tbaa !12
  %mul1470 = mul i32 2, %549
  %add1471 = add i32 %mul1470, 48
  %arrayidx1472 = getelementptr inbounds [128 x i64], [128 x i64]* %v1469, i32 0, i32 %add1471
  store i64 %call1468, i64* %arrayidx1472, align 8, !tbaa !13
  %v1473 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %550 = load i32, i32* %i, align 4, !tbaa !12
  %mul1474 = mul i32 2, %550
  %add1475 = add i32 %mul1474, 1
  %arrayidx1476 = getelementptr inbounds [128 x i64], [128 x i64]* %v1473, i32 0, i32 %add1475
  %551 = load i64, i64* %arrayidx1476, align 8, !tbaa !13
  %v1477 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %552 = load i32, i32* %i, align 4, !tbaa !12
  %mul1478 = mul i32 2, %552
  %add1479 = add i32 %mul1478, 48
  %arrayidx1480 = getelementptr inbounds [128 x i64], [128 x i64]* %v1477, i32 0, i32 %add1479
  %553 = load i64, i64* %arrayidx1480, align 8, !tbaa !13
  %call1481 = call i64 @fBlaMka(i64 %551, i64 %553) #4
  %v1482 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %554 = load i32, i32* %i, align 4, !tbaa !12
  %mul1483 = mul i32 2, %554
  %add1484 = add i32 %mul1483, 1
  %arrayidx1485 = getelementptr inbounds [128 x i64], [128 x i64]* %v1482, i32 0, i32 %add1484
  store i64 %call1481, i64* %arrayidx1485, align 8, !tbaa !13
  %v1486 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %555 = load i32, i32* %i, align 4, !tbaa !12
  %mul1487 = mul i32 2, %555
  %add1488 = add i32 %mul1487, 96
  %arrayidx1489 = getelementptr inbounds [128 x i64], [128 x i64]* %v1486, i32 0, i32 %add1488
  %556 = load i64, i64* %arrayidx1489, align 8, !tbaa !13
  %v1490 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %557 = load i32, i32* %i, align 4, !tbaa !12
  %mul1491 = mul i32 2, %557
  %add1492 = add i32 %mul1491, 1
  %arrayidx1493 = getelementptr inbounds [128 x i64], [128 x i64]* %v1490, i32 0, i32 %add1492
  %558 = load i64, i64* %arrayidx1493, align 8, !tbaa !13
  %xor1494 = xor i64 %556, %558
  %call1495 = call i64 @rotr64(i64 %xor1494, i32 16) #4
  %v1496 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %559 = load i32, i32* %i, align 4, !tbaa !12
  %mul1497 = mul i32 2, %559
  %add1498 = add i32 %mul1497, 96
  %arrayidx1499 = getelementptr inbounds [128 x i64], [128 x i64]* %v1496, i32 0, i32 %add1498
  store i64 %call1495, i64* %arrayidx1499, align 8, !tbaa !13
  %v1500 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %560 = load i32, i32* %i, align 4, !tbaa !12
  %mul1501 = mul i32 2, %560
  %add1502 = add i32 %mul1501, 81
  %arrayidx1503 = getelementptr inbounds [128 x i64], [128 x i64]* %v1500, i32 0, i32 %add1502
  %561 = load i64, i64* %arrayidx1503, align 8, !tbaa !13
  %v1504 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %562 = load i32, i32* %i, align 4, !tbaa !12
  %mul1505 = mul i32 2, %562
  %add1506 = add i32 %mul1505, 96
  %arrayidx1507 = getelementptr inbounds [128 x i64], [128 x i64]* %v1504, i32 0, i32 %add1506
  %563 = load i64, i64* %arrayidx1507, align 8, !tbaa !13
  %call1508 = call i64 @fBlaMka(i64 %561, i64 %563) #4
  %v1509 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %564 = load i32, i32* %i, align 4, !tbaa !12
  %mul1510 = mul i32 2, %564
  %add1511 = add i32 %mul1510, 81
  %arrayidx1512 = getelementptr inbounds [128 x i64], [128 x i64]* %v1509, i32 0, i32 %add1511
  store i64 %call1508, i64* %arrayidx1512, align 8, !tbaa !13
  %v1513 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %565 = load i32, i32* %i, align 4, !tbaa !12
  %mul1514 = mul i32 2, %565
  %add1515 = add i32 %mul1514, 48
  %arrayidx1516 = getelementptr inbounds [128 x i64], [128 x i64]* %v1513, i32 0, i32 %add1515
  %566 = load i64, i64* %arrayidx1516, align 8, !tbaa !13
  %v1517 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %567 = load i32, i32* %i, align 4, !tbaa !12
  %mul1518 = mul i32 2, %567
  %add1519 = add i32 %mul1518, 81
  %arrayidx1520 = getelementptr inbounds [128 x i64], [128 x i64]* %v1517, i32 0, i32 %add1519
  %568 = load i64, i64* %arrayidx1520, align 8, !tbaa !13
  %xor1521 = xor i64 %566, %568
  %call1522 = call i64 @rotr64(i64 %xor1521, i32 63) #4
  %v1523 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %569 = load i32, i32* %i, align 4, !tbaa !12
  %mul1524 = mul i32 2, %569
  %add1525 = add i32 %mul1524, 48
  %arrayidx1526 = getelementptr inbounds [128 x i64], [128 x i64]* %v1523, i32 0, i32 %add1525
  store i64 %call1522, i64* %arrayidx1526, align 8, !tbaa !13
  br label %do.cond1527

do.cond1527:                                      ; preds = %do.body1418
  br label %do.end1528

do.end1528:                                       ; preds = %do.cond1527
  br label %do.body1529

do.body1529:                                      ; preds = %do.end1528
  %v1530 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %570 = load i32, i32* %i, align 4, !tbaa !12
  %mul1531 = mul i32 2, %570
  %add1532 = add i32 %mul1531, 16
  %arrayidx1533 = getelementptr inbounds [128 x i64], [128 x i64]* %v1530, i32 0, i32 %add1532
  %571 = load i64, i64* %arrayidx1533, align 8, !tbaa !13
  %v1534 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %572 = load i32, i32* %i, align 4, !tbaa !12
  %mul1535 = mul i32 2, %572
  %add1536 = add i32 %mul1535, 49
  %arrayidx1537 = getelementptr inbounds [128 x i64], [128 x i64]* %v1534, i32 0, i32 %add1536
  %573 = load i64, i64* %arrayidx1537, align 8, !tbaa !13
  %call1538 = call i64 @fBlaMka(i64 %571, i64 %573) #4
  %v1539 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %574 = load i32, i32* %i, align 4, !tbaa !12
  %mul1540 = mul i32 2, %574
  %add1541 = add i32 %mul1540, 16
  %arrayidx1542 = getelementptr inbounds [128 x i64], [128 x i64]* %v1539, i32 0, i32 %add1541
  store i64 %call1538, i64* %arrayidx1542, align 8, !tbaa !13
  %v1543 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %575 = load i32, i32* %i, align 4, !tbaa !12
  %mul1544 = mul i32 2, %575
  %add1545 = add i32 %mul1544, 97
  %arrayidx1546 = getelementptr inbounds [128 x i64], [128 x i64]* %v1543, i32 0, i32 %add1545
  %576 = load i64, i64* %arrayidx1546, align 8, !tbaa !13
  %v1547 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %577 = load i32, i32* %i, align 4, !tbaa !12
  %mul1548 = mul i32 2, %577
  %add1549 = add i32 %mul1548, 16
  %arrayidx1550 = getelementptr inbounds [128 x i64], [128 x i64]* %v1547, i32 0, i32 %add1549
  %578 = load i64, i64* %arrayidx1550, align 8, !tbaa !13
  %xor1551 = xor i64 %576, %578
  %call1552 = call i64 @rotr64(i64 %xor1551, i32 32) #4
  %v1553 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %579 = load i32, i32* %i, align 4, !tbaa !12
  %mul1554 = mul i32 2, %579
  %add1555 = add i32 %mul1554, 97
  %arrayidx1556 = getelementptr inbounds [128 x i64], [128 x i64]* %v1553, i32 0, i32 %add1555
  store i64 %call1552, i64* %arrayidx1556, align 8, !tbaa !13
  %v1557 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %580 = load i32, i32* %i, align 4, !tbaa !12
  %mul1558 = mul i32 2, %580
  %add1559 = add i32 %mul1558, 64
  %arrayidx1560 = getelementptr inbounds [128 x i64], [128 x i64]* %v1557, i32 0, i32 %add1559
  %581 = load i64, i64* %arrayidx1560, align 8, !tbaa !13
  %v1561 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %582 = load i32, i32* %i, align 4, !tbaa !12
  %mul1562 = mul i32 2, %582
  %add1563 = add i32 %mul1562, 97
  %arrayidx1564 = getelementptr inbounds [128 x i64], [128 x i64]* %v1561, i32 0, i32 %add1563
  %583 = load i64, i64* %arrayidx1564, align 8, !tbaa !13
  %call1565 = call i64 @fBlaMka(i64 %581, i64 %583) #4
  %v1566 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %584 = load i32, i32* %i, align 4, !tbaa !12
  %mul1567 = mul i32 2, %584
  %add1568 = add i32 %mul1567, 64
  %arrayidx1569 = getelementptr inbounds [128 x i64], [128 x i64]* %v1566, i32 0, i32 %add1568
  store i64 %call1565, i64* %arrayidx1569, align 8, !tbaa !13
  %v1570 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %585 = load i32, i32* %i, align 4, !tbaa !12
  %mul1571 = mul i32 2, %585
  %add1572 = add i32 %mul1571, 49
  %arrayidx1573 = getelementptr inbounds [128 x i64], [128 x i64]* %v1570, i32 0, i32 %add1572
  %586 = load i64, i64* %arrayidx1573, align 8, !tbaa !13
  %v1574 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %587 = load i32, i32* %i, align 4, !tbaa !12
  %mul1575 = mul i32 2, %587
  %add1576 = add i32 %mul1575, 64
  %arrayidx1577 = getelementptr inbounds [128 x i64], [128 x i64]* %v1574, i32 0, i32 %add1576
  %588 = load i64, i64* %arrayidx1577, align 8, !tbaa !13
  %xor1578 = xor i64 %586, %588
  %call1579 = call i64 @rotr64(i64 %xor1578, i32 24) #4
  %v1580 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %589 = load i32, i32* %i, align 4, !tbaa !12
  %mul1581 = mul i32 2, %589
  %add1582 = add i32 %mul1581, 49
  %arrayidx1583 = getelementptr inbounds [128 x i64], [128 x i64]* %v1580, i32 0, i32 %add1582
  store i64 %call1579, i64* %arrayidx1583, align 8, !tbaa !13
  %v1584 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %590 = load i32, i32* %i, align 4, !tbaa !12
  %mul1585 = mul i32 2, %590
  %add1586 = add i32 %mul1585, 16
  %arrayidx1587 = getelementptr inbounds [128 x i64], [128 x i64]* %v1584, i32 0, i32 %add1586
  %591 = load i64, i64* %arrayidx1587, align 8, !tbaa !13
  %v1588 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %592 = load i32, i32* %i, align 4, !tbaa !12
  %mul1589 = mul i32 2, %592
  %add1590 = add i32 %mul1589, 49
  %arrayidx1591 = getelementptr inbounds [128 x i64], [128 x i64]* %v1588, i32 0, i32 %add1590
  %593 = load i64, i64* %arrayidx1591, align 8, !tbaa !13
  %call1592 = call i64 @fBlaMka(i64 %591, i64 %593) #4
  %v1593 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %594 = load i32, i32* %i, align 4, !tbaa !12
  %mul1594 = mul i32 2, %594
  %add1595 = add i32 %mul1594, 16
  %arrayidx1596 = getelementptr inbounds [128 x i64], [128 x i64]* %v1593, i32 0, i32 %add1595
  store i64 %call1592, i64* %arrayidx1596, align 8, !tbaa !13
  %v1597 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %595 = load i32, i32* %i, align 4, !tbaa !12
  %mul1598 = mul i32 2, %595
  %add1599 = add i32 %mul1598, 97
  %arrayidx1600 = getelementptr inbounds [128 x i64], [128 x i64]* %v1597, i32 0, i32 %add1599
  %596 = load i64, i64* %arrayidx1600, align 8, !tbaa !13
  %v1601 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %597 = load i32, i32* %i, align 4, !tbaa !12
  %mul1602 = mul i32 2, %597
  %add1603 = add i32 %mul1602, 16
  %arrayidx1604 = getelementptr inbounds [128 x i64], [128 x i64]* %v1601, i32 0, i32 %add1603
  %598 = load i64, i64* %arrayidx1604, align 8, !tbaa !13
  %xor1605 = xor i64 %596, %598
  %call1606 = call i64 @rotr64(i64 %xor1605, i32 16) #4
  %v1607 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %599 = load i32, i32* %i, align 4, !tbaa !12
  %mul1608 = mul i32 2, %599
  %add1609 = add i32 %mul1608, 97
  %arrayidx1610 = getelementptr inbounds [128 x i64], [128 x i64]* %v1607, i32 0, i32 %add1609
  store i64 %call1606, i64* %arrayidx1610, align 8, !tbaa !13
  %v1611 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %600 = load i32, i32* %i, align 4, !tbaa !12
  %mul1612 = mul i32 2, %600
  %add1613 = add i32 %mul1612, 64
  %arrayidx1614 = getelementptr inbounds [128 x i64], [128 x i64]* %v1611, i32 0, i32 %add1613
  %601 = load i64, i64* %arrayidx1614, align 8, !tbaa !13
  %v1615 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %602 = load i32, i32* %i, align 4, !tbaa !12
  %mul1616 = mul i32 2, %602
  %add1617 = add i32 %mul1616, 97
  %arrayidx1618 = getelementptr inbounds [128 x i64], [128 x i64]* %v1615, i32 0, i32 %add1617
  %603 = load i64, i64* %arrayidx1618, align 8, !tbaa !13
  %call1619 = call i64 @fBlaMka(i64 %601, i64 %603) #4
  %v1620 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %604 = load i32, i32* %i, align 4, !tbaa !12
  %mul1621 = mul i32 2, %604
  %add1622 = add i32 %mul1621, 64
  %arrayidx1623 = getelementptr inbounds [128 x i64], [128 x i64]* %v1620, i32 0, i32 %add1622
  store i64 %call1619, i64* %arrayidx1623, align 8, !tbaa !13
  %v1624 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %605 = load i32, i32* %i, align 4, !tbaa !12
  %mul1625 = mul i32 2, %605
  %add1626 = add i32 %mul1625, 49
  %arrayidx1627 = getelementptr inbounds [128 x i64], [128 x i64]* %v1624, i32 0, i32 %add1626
  %606 = load i64, i64* %arrayidx1627, align 8, !tbaa !13
  %v1628 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %607 = load i32, i32* %i, align 4, !tbaa !12
  %mul1629 = mul i32 2, %607
  %add1630 = add i32 %mul1629, 64
  %arrayidx1631 = getelementptr inbounds [128 x i64], [128 x i64]* %v1628, i32 0, i32 %add1630
  %608 = load i64, i64* %arrayidx1631, align 8, !tbaa !13
  %xor1632 = xor i64 %606, %608
  %call1633 = call i64 @rotr64(i64 %xor1632, i32 63) #4
  %v1634 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %609 = load i32, i32* %i, align 4, !tbaa !12
  %mul1635 = mul i32 2, %609
  %add1636 = add i32 %mul1635, 49
  %arrayidx1637 = getelementptr inbounds [128 x i64], [128 x i64]* %v1634, i32 0, i32 %add1636
  store i64 %call1633, i64* %arrayidx1637, align 8, !tbaa !13
  br label %do.cond1638

do.cond1638:                                      ; preds = %do.body1529
  br label %do.end1639

do.end1639:                                       ; preds = %do.cond1638
  br label %do.body1640

do.body1640:                                      ; preds = %do.end1639
  %v1641 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %610 = load i32, i32* %i, align 4, !tbaa !12
  %mul1642 = mul i32 2, %610
  %add1643 = add i32 %mul1642, 17
  %arrayidx1644 = getelementptr inbounds [128 x i64], [128 x i64]* %v1641, i32 0, i32 %add1643
  %611 = load i64, i64* %arrayidx1644, align 8, !tbaa !13
  %v1645 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %612 = load i32, i32* %i, align 4, !tbaa !12
  %mul1646 = mul i32 2, %612
  %add1647 = add i32 %mul1646, 32
  %arrayidx1648 = getelementptr inbounds [128 x i64], [128 x i64]* %v1645, i32 0, i32 %add1647
  %613 = load i64, i64* %arrayidx1648, align 8, !tbaa !13
  %call1649 = call i64 @fBlaMka(i64 %611, i64 %613) #4
  %v1650 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %614 = load i32, i32* %i, align 4, !tbaa !12
  %mul1651 = mul i32 2, %614
  %add1652 = add i32 %mul1651, 17
  %arrayidx1653 = getelementptr inbounds [128 x i64], [128 x i64]* %v1650, i32 0, i32 %add1652
  store i64 %call1649, i64* %arrayidx1653, align 8, !tbaa !13
  %v1654 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %615 = load i32, i32* %i, align 4, !tbaa !12
  %mul1655 = mul i32 2, %615
  %add1656 = add i32 %mul1655, 112
  %arrayidx1657 = getelementptr inbounds [128 x i64], [128 x i64]* %v1654, i32 0, i32 %add1656
  %616 = load i64, i64* %arrayidx1657, align 8, !tbaa !13
  %v1658 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %617 = load i32, i32* %i, align 4, !tbaa !12
  %mul1659 = mul i32 2, %617
  %add1660 = add i32 %mul1659, 17
  %arrayidx1661 = getelementptr inbounds [128 x i64], [128 x i64]* %v1658, i32 0, i32 %add1660
  %618 = load i64, i64* %arrayidx1661, align 8, !tbaa !13
  %xor1662 = xor i64 %616, %618
  %call1663 = call i64 @rotr64(i64 %xor1662, i32 32) #4
  %v1664 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %619 = load i32, i32* %i, align 4, !tbaa !12
  %mul1665 = mul i32 2, %619
  %add1666 = add i32 %mul1665, 112
  %arrayidx1667 = getelementptr inbounds [128 x i64], [128 x i64]* %v1664, i32 0, i32 %add1666
  store i64 %call1663, i64* %arrayidx1667, align 8, !tbaa !13
  %v1668 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %620 = load i32, i32* %i, align 4, !tbaa !12
  %mul1669 = mul i32 2, %620
  %add1670 = add i32 %mul1669, 65
  %arrayidx1671 = getelementptr inbounds [128 x i64], [128 x i64]* %v1668, i32 0, i32 %add1670
  %621 = load i64, i64* %arrayidx1671, align 8, !tbaa !13
  %v1672 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %622 = load i32, i32* %i, align 4, !tbaa !12
  %mul1673 = mul i32 2, %622
  %add1674 = add i32 %mul1673, 112
  %arrayidx1675 = getelementptr inbounds [128 x i64], [128 x i64]* %v1672, i32 0, i32 %add1674
  %623 = load i64, i64* %arrayidx1675, align 8, !tbaa !13
  %call1676 = call i64 @fBlaMka(i64 %621, i64 %623) #4
  %v1677 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %624 = load i32, i32* %i, align 4, !tbaa !12
  %mul1678 = mul i32 2, %624
  %add1679 = add i32 %mul1678, 65
  %arrayidx1680 = getelementptr inbounds [128 x i64], [128 x i64]* %v1677, i32 0, i32 %add1679
  store i64 %call1676, i64* %arrayidx1680, align 8, !tbaa !13
  %v1681 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %625 = load i32, i32* %i, align 4, !tbaa !12
  %mul1682 = mul i32 2, %625
  %add1683 = add i32 %mul1682, 32
  %arrayidx1684 = getelementptr inbounds [128 x i64], [128 x i64]* %v1681, i32 0, i32 %add1683
  %626 = load i64, i64* %arrayidx1684, align 8, !tbaa !13
  %v1685 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %627 = load i32, i32* %i, align 4, !tbaa !12
  %mul1686 = mul i32 2, %627
  %add1687 = add i32 %mul1686, 65
  %arrayidx1688 = getelementptr inbounds [128 x i64], [128 x i64]* %v1685, i32 0, i32 %add1687
  %628 = load i64, i64* %arrayidx1688, align 8, !tbaa !13
  %xor1689 = xor i64 %626, %628
  %call1690 = call i64 @rotr64(i64 %xor1689, i32 24) #4
  %v1691 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %629 = load i32, i32* %i, align 4, !tbaa !12
  %mul1692 = mul i32 2, %629
  %add1693 = add i32 %mul1692, 32
  %arrayidx1694 = getelementptr inbounds [128 x i64], [128 x i64]* %v1691, i32 0, i32 %add1693
  store i64 %call1690, i64* %arrayidx1694, align 8, !tbaa !13
  %v1695 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %630 = load i32, i32* %i, align 4, !tbaa !12
  %mul1696 = mul i32 2, %630
  %add1697 = add i32 %mul1696, 17
  %arrayidx1698 = getelementptr inbounds [128 x i64], [128 x i64]* %v1695, i32 0, i32 %add1697
  %631 = load i64, i64* %arrayidx1698, align 8, !tbaa !13
  %v1699 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %632 = load i32, i32* %i, align 4, !tbaa !12
  %mul1700 = mul i32 2, %632
  %add1701 = add i32 %mul1700, 32
  %arrayidx1702 = getelementptr inbounds [128 x i64], [128 x i64]* %v1699, i32 0, i32 %add1701
  %633 = load i64, i64* %arrayidx1702, align 8, !tbaa !13
  %call1703 = call i64 @fBlaMka(i64 %631, i64 %633) #4
  %v1704 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %634 = load i32, i32* %i, align 4, !tbaa !12
  %mul1705 = mul i32 2, %634
  %add1706 = add i32 %mul1705, 17
  %arrayidx1707 = getelementptr inbounds [128 x i64], [128 x i64]* %v1704, i32 0, i32 %add1706
  store i64 %call1703, i64* %arrayidx1707, align 8, !tbaa !13
  %v1708 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %635 = load i32, i32* %i, align 4, !tbaa !12
  %mul1709 = mul i32 2, %635
  %add1710 = add i32 %mul1709, 112
  %arrayidx1711 = getelementptr inbounds [128 x i64], [128 x i64]* %v1708, i32 0, i32 %add1710
  %636 = load i64, i64* %arrayidx1711, align 8, !tbaa !13
  %v1712 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %637 = load i32, i32* %i, align 4, !tbaa !12
  %mul1713 = mul i32 2, %637
  %add1714 = add i32 %mul1713, 17
  %arrayidx1715 = getelementptr inbounds [128 x i64], [128 x i64]* %v1712, i32 0, i32 %add1714
  %638 = load i64, i64* %arrayidx1715, align 8, !tbaa !13
  %xor1716 = xor i64 %636, %638
  %call1717 = call i64 @rotr64(i64 %xor1716, i32 16) #4
  %v1718 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %639 = load i32, i32* %i, align 4, !tbaa !12
  %mul1719 = mul i32 2, %639
  %add1720 = add i32 %mul1719, 112
  %arrayidx1721 = getelementptr inbounds [128 x i64], [128 x i64]* %v1718, i32 0, i32 %add1720
  store i64 %call1717, i64* %arrayidx1721, align 8, !tbaa !13
  %v1722 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %640 = load i32, i32* %i, align 4, !tbaa !12
  %mul1723 = mul i32 2, %640
  %add1724 = add i32 %mul1723, 65
  %arrayidx1725 = getelementptr inbounds [128 x i64], [128 x i64]* %v1722, i32 0, i32 %add1724
  %641 = load i64, i64* %arrayidx1725, align 8, !tbaa !13
  %v1726 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %642 = load i32, i32* %i, align 4, !tbaa !12
  %mul1727 = mul i32 2, %642
  %add1728 = add i32 %mul1727, 112
  %arrayidx1729 = getelementptr inbounds [128 x i64], [128 x i64]* %v1726, i32 0, i32 %add1728
  %643 = load i64, i64* %arrayidx1729, align 8, !tbaa !13
  %call1730 = call i64 @fBlaMka(i64 %641, i64 %643) #4
  %v1731 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %644 = load i32, i32* %i, align 4, !tbaa !12
  %mul1732 = mul i32 2, %644
  %add1733 = add i32 %mul1732, 65
  %arrayidx1734 = getelementptr inbounds [128 x i64], [128 x i64]* %v1731, i32 0, i32 %add1733
  store i64 %call1730, i64* %arrayidx1734, align 8, !tbaa !13
  %v1735 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %645 = load i32, i32* %i, align 4, !tbaa !12
  %mul1736 = mul i32 2, %645
  %add1737 = add i32 %mul1736, 32
  %arrayidx1738 = getelementptr inbounds [128 x i64], [128 x i64]* %v1735, i32 0, i32 %add1737
  %646 = load i64, i64* %arrayidx1738, align 8, !tbaa !13
  %v1739 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %647 = load i32, i32* %i, align 4, !tbaa !12
  %mul1740 = mul i32 2, %647
  %add1741 = add i32 %mul1740, 65
  %arrayidx1742 = getelementptr inbounds [128 x i64], [128 x i64]* %v1739, i32 0, i32 %add1741
  %648 = load i64, i64* %arrayidx1742, align 8, !tbaa !13
  %xor1743 = xor i64 %646, %648
  %call1744 = call i64 @rotr64(i64 %xor1743, i32 63) #4
  %v1745 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %649 = load i32, i32* %i, align 4, !tbaa !12
  %mul1746 = mul i32 2, %649
  %add1747 = add i32 %mul1746, 32
  %arrayidx1748 = getelementptr inbounds [128 x i64], [128 x i64]* %v1745, i32 0, i32 %add1747
  store i64 %call1744, i64* %arrayidx1748, align 8, !tbaa !13
  br label %do.cond1749

do.cond1749:                                      ; preds = %do.body1640
  br label %do.end1750

do.end1750:                                       ; preds = %do.cond1749
  br label %do.cond1751

do.cond1751:                                      ; preds = %do.end1750
  br label %do.end1752

do.end1752:                                       ; preds = %do.cond1751
  br label %for.inc1753

for.inc1753:                                      ; preds = %do.end1752
  %650 = load i32, i32* %i, align 4, !tbaa !12
  %inc1754 = add i32 %650, 1
  store i32 %inc1754, i32* %i, align 4, !tbaa !12
  br label %for.cond871

for.end1755:                                      ; preds = %for.cond871
  %651 = load %struct.block_*, %struct.block_** %next_block.addr, align 4, !tbaa !2
  call void @copy_block(%struct.block_* %651, %struct.block_* %block_tmp) #4
  %652 = load %struct.block_*, %struct.block_** %next_block.addr, align 4, !tbaa !2
  call void @xor_block(%struct.block_* %652, %struct.block_* %blockR) #4
  %653 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %653) #3
  %654 = bitcast %struct.block_* %block_tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %654) #3
  %655 = bitcast %struct.block_* %blockR to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %655) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: optsize
declare void @copy_block(%struct.block_*, %struct.block_*) #2

; Function Attrs: optsize
declare void @xor_block(%struct.block_*, %struct.block_*) #2

; Function Attrs: noinline nounwind optsize
define internal i64 @fBlaMka(i64 %x, i64 %y) #0 {
entry:
  %x.addr = alloca i64, align 8
  %y.addr = alloca i64, align 8
  %m = alloca i64, align 8
  %xy = alloca i64, align 8
  store i64 %x, i64* %x.addr, align 8, !tbaa !13
  store i64 %y, i64* %y.addr, align 8, !tbaa !13
  %0 = bitcast i64* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #3
  store i64 4294967295, i64* %m, align 8, !tbaa !13
  %1 = bitcast i64* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #3
  %2 = load i64, i64* %x.addr, align 8, !tbaa !13
  %and = and i64 %2, 4294967295
  %3 = load i64, i64* %y.addr, align 8, !tbaa !13
  %and1 = and i64 %3, 4294967295
  %mul = mul i64 %and, %and1
  store i64 %mul, i64* %xy, align 8, !tbaa !13
  %4 = load i64, i64* %x.addr, align 8, !tbaa !13
  %5 = load i64, i64* %y.addr, align 8, !tbaa !13
  %add = add i64 %4, %5
  %6 = load i64, i64* %xy, align 8, !tbaa !13
  %mul2 = mul i64 2, %6
  %add3 = add i64 %add, %mul2
  %7 = bitcast i64* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %7) #3
  %8 = bitcast i64* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %8) #3
  ret i64 %add3
}

; Function Attrs: noinline nounwind optsize
define internal i64 @rotr64(i64 %w, i32 %c) #0 {
entry:
  %w.addr = alloca i64, align 8
  %c.addr = alloca i32, align 4
  store i64 %w, i64* %w.addr, align 8, !tbaa !13
  store i32 %c, i32* %c.addr, align 4, !tbaa !12
  %0 = load i64, i64* %w.addr, align 8, !tbaa !13
  %1 = load i32, i32* %c.addr, align 4, !tbaa !12
  %sh_prom = zext i32 %1 to i64
  %shr = lshr i64 %0, %sh_prom
  %2 = load i64, i64* %w.addr, align 8, !tbaa !13
  %3 = load i32, i32* %c.addr, align 4, !tbaa !12
  %sub = sub i32 64, %3
  %sh_prom1 = zext i32 %sub to i64
  %shl = shl i64 %2, %sh_prom1
  %or = or i64 %shr, %shl
  ret i64 %or
}

attributes #0 = { noinline nounwind optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { optsize }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !4, i64 32}
!7 = !{!"Argon2_instance_t", !3, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !4, i64 32, !8, i64 36, !3, i64 40}
!8 = !{!"int", !4, i64 0}
!9 = !{!10, !8, i64 0}
!10 = !{!"Argon2_position_t", !8, i64 0, !8, i64 4, !4, i64 8, !8, i64 12}
!11 = !{!10, !4, i64 8}
!12 = !{!8, !8, i64 0}
!13 = !{!14, !14, i64 0}
!14 = !{!"long long", !4, i64 0}
!15 = !{!10, !8, i64 4}
!16 = !{!7, !8, i64 12}
!17 = !{!7, !8, i64 8}
!18 = !{!7, !8, i64 20}
!19 = !{!7, !8, i64 16}
!20 = !{!7, !3, i64 0}
!21 = !{!7, !8, i64 24}
!22 = !{!10, !8, i64 12}
!23 = !{!7, !8, i64 4}
