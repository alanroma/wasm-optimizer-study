(module
  (type (;0;) (func (param i32) (result i32)))
  (type (;1;) (func (param i32 i32)))
  (type (;2;) (func (param i32 i32 i32) (result i32)))
  (type (;3;) (func (param i32 i32) (result i32)))
  (type (;4;) (func (param i32 i32 i32)))
  (type (;5;) (func (param i32 i32 i32 i32) (result i32)))
  (type (;6;) (func (param i32)))
  (type (;7;) (func (param i32 i32 i32 i32)))
  (type (;8;) (func (param i32 i64)))
  (type (;9;) (func (param i64 i32) (result i32)))
  (type (;10;) (func))
  (type (;11;) (func (param i32 i32 i32 i32 i32)))
  (type (;12;) (func (param i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32) (result i32)))
  (type (;13;) (func (param i32 f64 i32 i32 i32 i32) (result i32)))
  (type (;14;) (func (param i64 i32 i32) (result i32)))
  (type (;15;) (func (param i64 i32) (result i64)))
  (type (;16;) (func (param i64 i64) (result i64)))
  (import "a" "a" (func (;0;) (type 2)))
  (import "a" "b" (func (;1;) (type 0)))
  (import "a" "c" (func (;2;) (type 3)))
  (import "a" "memory" (memory (;0;) 256 32767))
  (import "a" "table" (table (;0;) 3 funcref))
  (func (;3;) (type 15) (param i64 i32) (result i64)
    local.get 0
    local.get 1
    i64.extend_i32_u
    i64.rotr)
  (func (;4;) (type 16) (param i64 i64) (result i64)
    local.get 0
    local.get 1
    i64.add
    local.get 0
    i64.const 1
    i64.shl
    i64.const 8589934590
    i64.and
    local.get 1
    i64.const 4294967295
    i64.and
    i64.mul
    i64.add)
  (func (;5;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32 i32)
    block  ;; label = @1
      local.get 2
      i32.eqz
      br_if 0 (;@1;)
      i32.const -1
      local.set 3
      local.get 0
      i32.eqz
      local.get 1
      i32.eqz
      i32.or
      br_if 0 (;@1;)
      local.get 0
      i64.load offset=80
      i64.const 0
      i64.ne
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 0
        i32.load offset=224
        local.tee 3
        local.get 2
        i32.add
        i32.const 129
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.const 96
        i32.add
        local.tee 5
        local.get 3
        i32.add
        local.get 1
        i32.const 128
        local.get 3
        i32.sub
        local.tee 4
        call 6
        drop
        local.get 0
        i64.const 128
        call 27
        local.get 0
        local.get 5
        call 25
        i32.const 0
        local.set 3
        local.get 0
        i32.const 0
        i32.store offset=224
        local.get 1
        local.get 4
        i32.add
        local.set 1
        local.get 2
        local.get 4
        i32.sub
        local.tee 2
        i32.const 129
        i32.lt_u
        br_if 0 (;@2;)
        loop  ;; label = @3
          local.get 0
          i64.const 128
          call 27
          local.get 0
          local.get 1
          call 25
          local.get 1
          i32.const 128
          i32.add
          local.set 1
          local.get 2
          i32.const -128
          i32.add
          local.tee 2
          i32.const 128
          i32.gt_u
          br_if 0 (;@3;)
        end
        local.get 0
        i32.load offset=224
        local.set 3
      end
      local.get 0
      local.get 3
      i32.add
      i32.const 96
      i32.add
      local.get 1
      local.get 2
      call 6
      drop
      local.get 0
      local.get 0
      i32.load offset=224
      local.get 2
      i32.add
      i32.store offset=224
      i32.const 0
      local.set 3
    end
    local.get 3)
  (func (;6;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32 i32)
    local.get 2
    i32.const 512
    i32.ge_u
    if  ;; label = @1
      local.get 0
      local.get 1
      local.get 2
      call 0
      drop
      local.get 0
      return
    end
    local.get 0
    local.get 2
    i32.add
    local.set 3
    block  ;; label = @1
      local.get 0
      local.get 1
      i32.xor
      i32.const 3
      i32.and
      i32.eqz
      if  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.const 1
          i32.lt_s
          if  ;; label = @4
            local.get 0
            local.set 2
            br 1 (;@3;)
          end
          local.get 0
          i32.const 3
          i32.and
          i32.eqz
          if  ;; label = @4
            local.get 0
            local.set 2
            br 1 (;@3;)
          end
          local.get 0
          local.set 2
          loop  ;; label = @4
            local.get 2
            local.get 1
            i32.load8_u
            i32.store8
            local.get 1
            i32.const 1
            i32.add
            local.set 1
            local.get 2
            i32.const 1
            i32.add
            local.tee 2
            local.get 3
            i32.ge_u
            br_if 1 (;@3;)
            local.get 2
            i32.const 3
            i32.and
            br_if 0 (;@4;)
          end
        end
        block  ;; label = @3
          local.get 3
          i32.const -4
          i32.and
          local.tee 4
          i32.const 64
          i32.lt_u
          br_if 0 (;@3;)
          local.get 2
          local.get 4
          i32.const -64
          i32.add
          local.tee 5
          i32.gt_u
          br_if 0 (;@3;)
          loop  ;; label = @4
            local.get 2
            local.get 1
            i32.load
            i32.store
            local.get 2
            local.get 1
            i32.load offset=4
            i32.store offset=4
            local.get 2
            local.get 1
            i32.load offset=8
            i32.store offset=8
            local.get 2
            local.get 1
            i32.load offset=12
            i32.store offset=12
            local.get 2
            local.get 1
            i32.load offset=16
            i32.store offset=16
            local.get 2
            local.get 1
            i32.load offset=20
            i32.store offset=20
            local.get 2
            local.get 1
            i32.load offset=24
            i32.store offset=24
            local.get 2
            local.get 1
            i32.load offset=28
            i32.store offset=28
            local.get 2
            local.get 1
            i32.load offset=32
            i32.store offset=32
            local.get 2
            local.get 1
            i32.load offset=36
            i32.store offset=36
            local.get 2
            local.get 1
            i32.load offset=40
            i32.store offset=40
            local.get 2
            local.get 1
            i32.load offset=44
            i32.store offset=44
            local.get 2
            local.get 1
            i32.load offset=48
            i32.store offset=48
            local.get 2
            local.get 1
            i32.load offset=52
            i32.store offset=52
            local.get 2
            local.get 1
            i32.load offset=56
            i32.store offset=56
            local.get 2
            local.get 1
            i32.load offset=60
            i32.store offset=60
            local.get 1
            i32.const -64
            i32.sub
            local.set 1
            local.get 2
            i32.const -64
            i32.sub
            local.tee 2
            local.get 5
            i32.le_u
            br_if 0 (;@4;)
          end
        end
        local.get 2
        local.get 4
        i32.ge_u
        br_if 1 (;@1;)
        loop  ;; label = @3
          local.get 2
          local.get 1
          i32.load
          i32.store
          local.get 1
          i32.const 4
          i32.add
          local.set 1
          local.get 2
          i32.const 4
          i32.add
          local.tee 2
          local.get 4
          i32.lt_u
          br_if 0 (;@3;)
        end
        br 1 (;@1;)
      end
      local.get 3
      i32.const 4
      i32.lt_u
      if  ;; label = @2
        local.get 0
        local.set 2
        br 1 (;@1;)
      end
      local.get 3
      i32.const -4
      i32.add
      local.tee 4
      local.get 0
      i32.lt_u
      if  ;; label = @2
        local.get 0
        local.set 2
        br 1 (;@1;)
      end
      local.get 0
      local.set 2
      loop  ;; label = @2
        local.get 2
        local.get 1
        i32.load8_u
        i32.store8
        local.get 2
        local.get 1
        i32.load8_u offset=1
        i32.store8 offset=1
        local.get 2
        local.get 1
        i32.load8_u offset=2
        i32.store8 offset=2
        local.get 2
        local.get 1
        i32.load8_u offset=3
        i32.store8 offset=3
        local.get 1
        i32.const 4
        i32.add
        local.set 1
        local.get 2
        i32.const 4
        i32.add
        local.tee 2
        local.get 4
        i32.le_u
        br_if 0 (;@2;)
      end
    end
    local.get 2
    local.get 3
    i32.lt_u
    if  ;; label = @1
      loop  ;; label = @2
        local.get 2
        local.get 1
        i32.load8_u
        i32.store8
        local.get 1
        i32.const 1
        i32.add
        local.set 1
        local.get 2
        i32.const 1
        i32.add
        local.tee 2
        local.get 3
        i32.ne
        br_if 0 (;@2;)
      end
    end
    local.get 0)
  (func (;7;) (type 1) (param i32 i32)
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      i32.const 3712
      i32.load
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      call 29
    end)
  (func (;8;) (type 1) (param i32 i32)
    local.get 0
    local.get 1
    i32.store align=1)
  (func (;9;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32 i64)
    block  ;; label = @1
      local.get 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.add
      local.tee 3
      i32.const -1
      i32.add
      local.get 1
      i32.store8
      local.get 0
      local.get 1
      i32.store8
      local.get 2
      i32.const 3
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      i32.const -2
      i32.add
      local.get 1
      i32.store8
      local.get 0
      local.get 1
      i32.store8 offset=1
      local.get 3
      i32.const -3
      i32.add
      local.get 1
      i32.store8
      local.get 0
      local.get 1
      i32.store8 offset=2
      local.get 2
      i32.const 7
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      i32.const -4
      i32.add
      local.get 1
      i32.store8
      local.get 0
      local.get 1
      i32.store8 offset=3
      local.get 2
      i32.const 9
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 0
      local.get 0
      i32.sub
      i32.const 3
      i32.and
      local.tee 4
      i32.add
      local.tee 3
      local.get 1
      i32.const 255
      i32.and
      i32.const 16843009
      i32.mul
      local.tee 1
      i32.store
      local.get 3
      local.get 2
      local.get 4
      i32.sub
      i32.const -4
      i32.and
      local.tee 4
      i32.add
      local.tee 2
      i32.const -4
      i32.add
      local.get 1
      i32.store
      local.get 4
      i32.const 9
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      local.get 1
      i32.store offset=8
      local.get 3
      local.get 1
      i32.store offset=4
      local.get 2
      i32.const -8
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -12
      i32.add
      local.get 1
      i32.store
      local.get 4
      i32.const 25
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      local.get 1
      i32.store offset=24
      local.get 3
      local.get 1
      i32.store offset=20
      local.get 3
      local.get 1
      i32.store offset=16
      local.get 3
      local.get 1
      i32.store offset=12
      local.get 2
      i32.const -16
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -20
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -24
      i32.add
      local.get 1
      i32.store
      local.get 2
      i32.const -28
      i32.add
      local.get 1
      i32.store
      local.get 4
      local.get 3
      i32.const 4
      i32.and
      i32.const 24
      i32.or
      local.tee 4
      i32.sub
      local.tee 2
      i32.const 32
      i32.lt_u
      br_if 0 (;@1;)
      local.get 1
      i64.extend_i32_u
      local.tee 5
      i64.const 32
      i64.shl
      local.get 5
      i64.or
      local.set 5
      local.get 3
      local.get 4
      i32.add
      local.set 1
      loop  ;; label = @2
        local.get 1
        local.get 5
        i64.store offset=24
        local.get 1
        local.get 5
        i64.store offset=16
        local.get 1
        local.get 5
        i64.store offset=8
        local.get 1
        local.get 5
        i64.store
        local.get 1
        i32.const 32
        i32.add
        local.set 1
        local.get 2
        i32.const -32
        i32.add
        local.tee 2
        i32.const 31
        i32.gt_u
        br_if 0 (;@2;)
      end
    end
    local.get 0)
  (func (;10;) (type 6) (param i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const -8
      i32.add
      local.tee 3
      local.get 0
      i32.const -4
      i32.add
      i32.load
      local.tee 1
      i32.const -8
      i32.and
      local.tee 0
      i32.add
      local.set 5
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.and
        br_if 0 (;@2;)
        local.get 1
        i32.const 3
        i32.and
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        local.get 3
        i32.load
        local.tee 1
        i32.sub
        local.tee 3
        i32.const 4032
        i32.load
        local.tee 2
        i32.lt_u
        br_if 1 (;@1;)
        local.get 0
        local.get 1
        i32.add
        local.set 0
        local.get 3
        i32.const 4036
        i32.load
        i32.ne
        if  ;; label = @3
          local.get 1
          i32.const 255
          i32.le_u
          if  ;; label = @4
            local.get 3
            i32.load offset=8
            local.tee 4
            local.get 1
            i32.const 3
            i32.shr_u
            local.tee 1
            i32.const 3
            i32.shl
            i32.const 4056
            i32.add
            i32.ne
            drop
            local.get 4
            local.get 3
            i32.load offset=12
            local.tee 2
            i32.eq
            if  ;; label = @5
              i32.const 4016
              i32.const 4016
              i32.load
              i32.const -2
              local.get 1
              i32.rotl
              i32.and
              i32.store
              br 3 (;@2;)
            end
            local.get 4
            local.get 2
            i32.store offset=12
            local.get 2
            local.get 4
            i32.store offset=8
            br 2 (;@2;)
          end
          local.get 3
          i32.load offset=24
          local.set 7
          block  ;; label = @4
            local.get 3
            local.get 3
            i32.load offset=12
            local.tee 1
            i32.ne
            if  ;; label = @5
              local.get 2
              local.get 3
              i32.load offset=8
              local.tee 2
              i32.le_u
              if  ;; label = @6
                local.get 2
                i32.load offset=12
                drop
              end
              local.get 2
              local.get 1
              i32.store offset=12
              local.get 1
              local.get 2
              i32.store offset=8
              br 1 (;@4;)
            end
            block  ;; label = @5
              local.get 3
              i32.const 20
              i32.add
              local.tee 4
              i32.load
              local.tee 2
              br_if 0 (;@5;)
              local.get 3
              i32.const 16
              i32.add
              local.tee 4
              i32.load
              local.tee 2
              br_if 0 (;@5;)
              i32.const 0
              local.set 1
              br 1 (;@4;)
            end
            loop  ;; label = @5
              local.get 4
              local.set 6
              local.get 2
              local.tee 1
              i32.const 20
              i32.add
              local.tee 4
              i32.load
              local.tee 2
              br_if 0 (;@5;)
              local.get 1
              i32.const 16
              i32.add
              local.set 4
              local.get 1
              i32.load offset=16
              local.tee 2
              br_if 0 (;@5;)
            end
            local.get 6
            i32.const 0
            i32.store
          end
          local.get 7
          i32.eqz
          br_if 1 (;@2;)
          block  ;; label = @4
            local.get 3
            local.get 3
            i32.load offset=28
            local.tee 4
            i32.const 2
            i32.shl
            i32.const 4320
            i32.add
            local.tee 2
            i32.load
            i32.eq
            if  ;; label = @5
              local.get 2
              local.get 1
              i32.store
              local.get 1
              br_if 1 (;@4;)
              i32.const 4020
              i32.const 4020
              i32.load
              i32.const -2
              local.get 4
              i32.rotl
              i32.and
              i32.store
              br 3 (;@2;)
            end
            local.get 7
            i32.const 16
            i32.const 20
            local.get 7
            i32.load offset=16
            local.get 3
            i32.eq
            select
            i32.add
            local.get 1
            i32.store
            local.get 1
            i32.eqz
            br_if 2 (;@2;)
          end
          local.get 1
          local.get 7
          i32.store offset=24
          local.get 3
          i32.load offset=16
          local.tee 2
          if  ;; label = @4
            local.get 1
            local.get 2
            i32.store offset=16
            local.get 2
            local.get 1
            i32.store offset=24
          end
          local.get 3
          i32.load offset=20
          local.tee 2
          i32.eqz
          br_if 1 (;@2;)
          local.get 1
          local.get 2
          i32.store offset=20
          local.get 2
          local.get 1
          i32.store offset=24
          br 1 (;@2;)
        end
        local.get 5
        i32.load offset=4
        local.tee 1
        i32.const 3
        i32.and
        i32.const 3
        i32.ne
        br_if 0 (;@2;)
        i32.const 4024
        local.get 0
        i32.store
        local.get 5
        local.get 1
        i32.const -2
        i32.and
        i32.store offset=4
        local.get 3
        local.get 0
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 0
        local.get 3
        i32.add
        local.get 0
        i32.store
        return
      end
      local.get 5
      local.get 3
      i32.le_u
      br_if 0 (;@1;)
      local.get 5
      i32.load offset=4
      local.tee 1
      i32.const 1
      i32.and
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 1
        i32.const 2
        i32.and
        i32.eqz
        if  ;; label = @3
          local.get 5
          i32.const 4040
          i32.load
          i32.eq
          if  ;; label = @4
            i32.const 4040
            local.get 3
            i32.store
            i32.const 4028
            i32.const 4028
            i32.load
            local.get 0
            i32.add
            local.tee 0
            i32.store
            local.get 3
            local.get 0
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 3
            i32.const 4036
            i32.load
            i32.ne
            br_if 3 (;@1;)
            i32.const 4024
            i32.const 0
            i32.store
            i32.const 4036
            i32.const 0
            i32.store
            return
          end
          local.get 5
          i32.const 4036
          i32.load
          i32.eq
          if  ;; label = @4
            i32.const 4036
            local.get 3
            i32.store
            i32.const 4024
            i32.const 4024
            i32.load
            local.get 0
            i32.add
            local.tee 0
            i32.store
            local.get 3
            local.get 0
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 0
            local.get 3
            i32.add
            local.get 0
            i32.store
            return
          end
          local.get 1
          i32.const -8
          i32.and
          local.get 0
          i32.add
          local.set 0
          block  ;; label = @4
            local.get 1
            i32.const 255
            i32.le_u
            if  ;; label = @5
              local.get 5
              i32.load offset=12
              local.set 6
              local.get 5
              i32.load offset=8
              local.tee 4
              local.get 1
              i32.const 3
              i32.shr_u
              local.tee 2
              i32.const 3
              i32.shl
              i32.const 4056
              i32.add
              local.tee 1
              i32.ne
              if  ;; label = @6
                i32.const 4032
                i32.load
                drop
              end
              local.get 4
              local.get 6
              i32.eq
              if  ;; label = @6
                i32.const 4016
                i32.const 4016
                i32.load
                i32.const -2
                local.get 2
                i32.rotl
                i32.and
                i32.store
                br 2 (;@4;)
              end
              local.get 1
              local.get 6
              i32.ne
              if  ;; label = @6
                i32.const 4032
                i32.load
                drop
              end
              local.get 4
              local.get 6
              i32.store offset=12
              local.get 6
              local.get 4
              i32.store offset=8
              br 1 (;@4;)
            end
            local.get 5
            i32.load offset=24
            local.set 7
            block  ;; label = @5
              local.get 5
              local.get 5
              i32.load offset=12
              local.tee 1
              i32.ne
              if  ;; label = @6
                i32.const 4032
                i32.load
                local.get 5
                i32.load offset=8
                local.tee 2
                i32.le_u
                if  ;; label = @7
                  local.get 2
                  i32.load offset=12
                  drop
                end
                local.get 2
                local.get 1
                i32.store offset=12
                local.get 1
                local.get 2
                i32.store offset=8
                br 1 (;@5;)
              end
              block  ;; label = @6
                local.get 5
                i32.const 20
                i32.add
                local.tee 4
                i32.load
                local.tee 2
                br_if 0 (;@6;)
                local.get 5
                i32.const 16
                i32.add
                local.tee 4
                i32.load
                local.tee 2
                br_if 0 (;@6;)
                i32.const 0
                local.set 1
                br 1 (;@5;)
              end
              loop  ;; label = @6
                local.get 4
                local.set 6
                local.get 2
                local.tee 1
                i32.const 20
                i32.add
                local.tee 4
                i32.load
                local.tee 2
                br_if 0 (;@6;)
                local.get 1
                i32.const 16
                i32.add
                local.set 4
                local.get 1
                i32.load offset=16
                local.tee 2
                br_if 0 (;@6;)
              end
              local.get 6
              i32.const 0
              i32.store
            end
            local.get 7
            i32.eqz
            br_if 0 (;@4;)
            block  ;; label = @5
              local.get 5
              local.get 5
              i32.load offset=28
              local.tee 4
              i32.const 2
              i32.shl
              i32.const 4320
              i32.add
              local.tee 2
              i32.load
              i32.eq
              if  ;; label = @6
                local.get 2
                local.get 1
                i32.store
                local.get 1
                br_if 1 (;@5;)
                i32.const 4020
                i32.const 4020
                i32.load
                i32.const -2
                local.get 4
                i32.rotl
                i32.and
                i32.store
                br 2 (;@4;)
              end
              local.get 7
              i32.const 16
              i32.const 20
              local.get 7
              i32.load offset=16
              local.get 5
              i32.eq
              select
              i32.add
              local.get 1
              i32.store
              local.get 1
              i32.eqz
              br_if 1 (;@4;)
            end
            local.get 1
            local.get 7
            i32.store offset=24
            local.get 5
            i32.load offset=16
            local.tee 2
            if  ;; label = @5
              local.get 1
              local.get 2
              i32.store offset=16
              local.get 2
              local.get 1
              i32.store offset=24
            end
            local.get 5
            i32.load offset=20
            local.tee 2
            i32.eqz
            br_if 0 (;@4;)
            local.get 1
            local.get 2
            i32.store offset=20
            local.get 2
            local.get 1
            i32.store offset=24
          end
          local.get 3
          local.get 0
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 0
          local.get 3
          i32.add
          local.get 0
          i32.store
          local.get 3
          i32.const 4036
          i32.load
          i32.ne
          br_if 1 (;@2;)
          i32.const 4024
          local.get 0
          i32.store
          return
        end
        local.get 5
        local.get 1
        i32.const -2
        i32.and
        i32.store offset=4
        local.get 3
        local.get 0
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 0
        local.get 3
        i32.add
        local.get 0
        i32.store
      end
      local.get 0
      i32.const 255
      i32.le_u
      if  ;; label = @2
        local.get 0
        i32.const 3
        i32.shr_u
        local.tee 0
        i32.const 3
        i32.shl
        i32.const 4056
        i32.add
        local.set 2
        block (result i32)  ;; label = @3
          i32.const 4016
          i32.load
          local.tee 1
          i32.const 1
          local.get 0
          i32.shl
          local.tee 0
          i32.and
          i32.eqz
          if  ;; label = @4
            i32.const 4016
            local.get 0
            local.get 1
            i32.or
            i32.store
            local.get 2
            br 1 (;@3;)
          end
          local.get 2
          i32.load offset=8
        end
        local.set 0
        local.get 2
        local.get 3
        i32.store offset=8
        local.get 0
        local.get 3
        i32.store offset=12
        local.get 3
        local.get 2
        i32.store offset=12
        local.get 3
        local.get 0
        i32.store offset=8
        return
      end
      local.get 3
      i64.const 0
      i64.store offset=16 align=4
      local.get 3
      block (result i32)  ;; label = @2
        i32.const 0
        local.get 0
        i32.const 8
        i32.shr_u
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        drop
        i32.const 31
        local.get 0
        i32.const 16777215
        i32.gt_u
        br_if 0 (;@2;)
        drop
        local.get 1
        local.get 1
        i32.const 1048320
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 8
        i32.and
        local.tee 4
        i32.shl
        local.tee 1
        local.get 1
        i32.const 520192
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 4
        i32.and
        local.tee 2
        i32.shl
        local.tee 1
        local.get 1
        i32.const 245760
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 2
        i32.and
        local.tee 1
        i32.shl
        i32.const 15
        i32.shr_u
        local.get 2
        local.get 4
        i32.or
        local.get 1
        i32.or
        i32.sub
        local.tee 1
        i32.const 1
        i32.shl
        local.get 0
        local.get 1
        i32.const 21
        i32.add
        i32.shr_u
        i32.const 1
        i32.and
        i32.or
        i32.const 28
        i32.add
      end
      local.tee 4
      i32.store offset=28
      local.get 4
      i32.const 2
      i32.shl
      i32.const 4320
      i32.add
      local.set 6
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            i32.const 4020
            i32.load
            local.tee 2
            i32.const 1
            local.get 4
            i32.shl
            local.tee 1
            i32.and
            i32.eqz
            if  ;; label = @5
              i32.const 4020
              local.get 1
              local.get 2
              i32.or
              i32.store
              local.get 6
              local.get 3
              i32.store
              local.get 3
              local.get 6
              i32.store offset=24
              br 1 (;@4;)
            end
            local.get 0
            i32.const 0
            i32.const 25
            local.get 4
            i32.const 1
            i32.shr_u
            i32.sub
            local.get 4
            i32.const 31
            i32.eq
            select
            i32.shl
            local.set 4
            local.get 6
            i32.load
            local.set 1
            loop  ;; label = @5
              local.get 1
              local.tee 2
              i32.load offset=4
              i32.const -8
              i32.and
              local.get 0
              i32.eq
              br_if 2 (;@3;)
              local.get 4
              i32.const 29
              i32.shr_u
              local.set 1
              local.get 4
              i32.const 1
              i32.shl
              local.set 4
              local.get 2
              local.get 1
              i32.const 4
              i32.and
              i32.add
              local.tee 6
              i32.const 16
              i32.add
              i32.load
              local.tee 1
              br_if 0 (;@5;)
            end
            local.get 6
            local.get 3
            i32.store offset=16
            local.get 3
            local.get 2
            i32.store offset=24
          end
          local.get 3
          local.get 3
          i32.store offset=12
          local.get 3
          local.get 3
          i32.store offset=8
          br 1 (;@2;)
        end
        local.get 2
        i32.load offset=8
        local.tee 0
        local.get 3
        i32.store offset=12
        local.get 2
        local.get 3
        i32.store offset=8
        local.get 3
        i32.const 0
        i32.store offset=24
        local.get 3
        local.get 2
        i32.store offset=12
        local.get 3
        local.get 0
        i32.store offset=8
      end
      i32.const 4048
      i32.const 4048
      i32.load
      i32.const -1
      i32.add
      local.tee 0
      i32.store
      local.get 0
      br_if 0 (;@1;)
      i32.const 4472
      local.set 3
      loop  ;; label = @2
        local.get 3
        i32.load
        local.tee 0
        i32.const 8
        i32.add
        local.set 3
        local.get 0
        br_if 0 (;@2;)
      end
      i32.const 4048
      i32.const -1
      i32.store
    end)
  (func (;11;) (type 0) (param i32) (result i32)
    (local i32 i32 i32)
    local.get 0
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.const 3
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load8_u
        i32.eqz
        if  ;; label = @3
          i32.const 0
          return
        end
        loop  ;; label = @3
          local.get 1
          i32.const 1
          i32.add
          local.tee 1
          i32.const 3
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 1
          i32.load8_u
          br_if 0 (;@3;)
        end
        br 1 (;@1;)
      end
      loop  ;; label = @2
        local.get 1
        local.tee 2
        i32.const 4
        i32.add
        local.set 1
        local.get 2
        i32.load
        local.tee 3
        i32.const -1
        i32.xor
        local.get 3
        i32.const -16843009
        i32.add
        i32.and
        i32.const -2139062144
        i32.and
        i32.eqz
        br_if 0 (;@2;)
      end
      local.get 3
      i32.const 255
      i32.and
      i32.eqz
      if  ;; label = @2
        local.get 2
        local.get 0
        i32.sub
        return
      end
      loop  ;; label = @2
        local.get 2
        i32.load8_u offset=1
        local.get 2
        i32.const 1
        i32.add
        local.tee 1
        local.set 2
        br_if 0 (;@2;)
      end
    end
    local.get 1
    local.get 0
    i32.sub)
  (func (;12;) (type 0) (param i32) (result i32)
    (local i32 i32)
    i32.const 4512
    i32.load
    local.tee 1
    local.get 0
    i32.const 3
    i32.add
    i32.const -4
    i32.and
    local.tee 2
    i32.add
    local.set 0
    block  ;; label = @1
      local.get 2
      i32.const 1
      i32.ge_s
      i32.const 0
      local.get 0
      local.get 1
      i32.le_u
      select
      br_if 0 (;@1;)
      local.get 0
      memory.size
      i32.const 16
      i32.shl
      i32.gt_u
      if  ;; label = @2
        local.get 0
        call 1
        i32.eqz
        br_if 1 (;@1;)
      end
      i32.const 4512
      local.get 0
      i32.store
      local.get 1
      return
    end
    i32.const 3948
    i32.const 48
    i32.store
    i32.const -1)
  (func (;13;) (type 0) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 11
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 0
                          i32.const 244
                          i32.le_u
                          if  ;; label = @12
                            i32.const 4016
                            i32.load
                            local.tee 6
                            i32.const 16
                            local.get 0
                            i32.const 11
                            i32.add
                            i32.const -8
                            i32.and
                            local.get 0
                            i32.const 11
                            i32.lt_u
                            select
                            local.tee 5
                            i32.const 3
                            i32.shr_u
                            local.tee 0
                            i32.shr_u
                            local.tee 1
                            i32.const 3
                            i32.and
                            if  ;; label = @13
                              local.get 1
                              i32.const -1
                              i32.xor
                              i32.const 1
                              i32.and
                              local.get 0
                              i32.add
                              local.tee 2
                              i32.const 3
                              i32.shl
                              local.tee 4
                              i32.const 4064
                              i32.add
                              i32.load
                              local.tee 1
                              i32.const 8
                              i32.add
                              local.set 0
                              block  ;; label = @14
                                local.get 1
                                i32.load offset=8
                                local.tee 3
                                local.get 4
                                i32.const 4056
                                i32.add
                                local.tee 4
                                i32.eq
                                if  ;; label = @15
                                  i32.const 4016
                                  local.get 6
                                  i32.const -2
                                  local.get 2
                                  i32.rotl
                                  i32.and
                                  i32.store
                                  br 1 (;@14;)
                                end
                                i32.const 4032
                                i32.load
                                drop
                                local.get 3
                                local.get 4
                                i32.store offset=12
                                local.get 4
                                local.get 3
                                i32.store offset=8
                              end
                              local.get 1
                              local.get 2
                              i32.const 3
                              i32.shl
                              local.tee 2
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              local.get 1
                              local.get 2
                              i32.add
                              local.tee 1
                              local.get 1
                              i32.load offset=4
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              br 12 (;@1;)
                            end
                            local.get 5
                            i32.const 4024
                            i32.load
                            local.tee 8
                            i32.le_u
                            br_if 1 (;@11;)
                            local.get 1
                            if  ;; label = @13
                              block  ;; label = @14
                                i32.const 2
                                local.get 0
                                i32.shl
                                local.tee 2
                                i32.const 0
                                local.get 2
                                i32.sub
                                i32.or
                                local.get 1
                                local.get 0
                                i32.shl
                                i32.and
                                local.tee 0
                                i32.const 0
                                local.get 0
                                i32.sub
                                i32.and
                                i32.const -1
                                i32.add
                                local.tee 0
                                local.get 0
                                i32.const 12
                                i32.shr_u
                                i32.const 16
                                i32.and
                                local.tee 0
                                i32.shr_u
                                local.tee 1
                                i32.const 5
                                i32.shr_u
                                i32.const 8
                                i32.and
                                local.tee 2
                                local.get 0
                                i32.or
                                local.get 1
                                local.get 2
                                i32.shr_u
                                local.tee 0
                                i32.const 2
                                i32.shr_u
                                i32.const 4
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 2
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 1
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                i32.add
                                local.tee 2
                                i32.const 3
                                i32.shl
                                local.tee 3
                                i32.const 4064
                                i32.add
                                i32.load
                                local.tee 1
                                i32.load offset=8
                                local.tee 0
                                local.get 3
                                i32.const 4056
                                i32.add
                                local.tee 3
                                i32.eq
                                if  ;; label = @15
                                  i32.const 4016
                                  local.get 6
                                  i32.const -2
                                  local.get 2
                                  i32.rotl
                                  i32.and
                                  local.tee 6
                                  i32.store
                                  br 1 (;@14;)
                                end
                                i32.const 4032
                                i32.load
                                drop
                                local.get 0
                                local.get 3
                                i32.store offset=12
                                local.get 3
                                local.get 0
                                i32.store offset=8
                              end
                              local.get 1
                              i32.const 8
                              i32.add
                              local.set 0
                              local.get 1
                              local.get 5
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              local.get 1
                              local.get 5
                              i32.add
                              local.tee 7
                              local.get 2
                              i32.const 3
                              i32.shl
                              local.tee 2
                              local.get 5
                              i32.sub
                              local.tee 3
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              local.get 1
                              local.get 2
                              i32.add
                              local.get 3
                              i32.store
                              local.get 8
                              if  ;; label = @14
                                local.get 8
                                i32.const 3
                                i32.shr_u
                                local.tee 4
                                i32.const 3
                                i32.shl
                                i32.const 4056
                                i32.add
                                local.set 1
                                i32.const 4036
                                i32.load
                                local.set 2
                                block (result i32)  ;; label = @15
                                  local.get 6
                                  i32.const 1
                                  local.get 4
                                  i32.shl
                                  local.tee 4
                                  i32.and
                                  i32.eqz
                                  if  ;; label = @16
                                    i32.const 4016
                                    local.get 4
                                    local.get 6
                                    i32.or
                                    i32.store
                                    local.get 1
                                    br 1 (;@15;)
                                  end
                                  local.get 1
                                  i32.load offset=8
                                end
                                local.set 4
                                local.get 1
                                local.get 2
                                i32.store offset=8
                                local.get 4
                                local.get 2
                                i32.store offset=12
                                local.get 2
                                local.get 1
                                i32.store offset=12
                                local.get 2
                                local.get 4
                                i32.store offset=8
                              end
                              i32.const 4036
                              local.get 7
                              i32.store
                              i32.const 4024
                              local.get 3
                              i32.store
                              br 12 (;@1;)
                            end
                            i32.const 4020
                            i32.load
                            local.tee 10
                            i32.eqz
                            br_if 1 (;@11;)
                            local.get 10
                            i32.const 0
                            local.get 10
                            i32.sub
                            i32.and
                            i32.const -1
                            i32.add
                            local.tee 0
                            local.get 0
                            i32.const 12
                            i32.shr_u
                            i32.const 16
                            i32.and
                            local.tee 0
                            i32.shr_u
                            local.tee 1
                            i32.const 5
                            i32.shr_u
                            i32.const 8
                            i32.and
                            local.tee 2
                            local.get 0
                            i32.or
                            local.get 1
                            local.get 2
                            i32.shr_u
                            local.tee 0
                            i32.const 2
                            i32.shr_u
                            i32.const 4
                            i32.and
                            local.tee 1
                            i32.or
                            local.get 0
                            local.get 1
                            i32.shr_u
                            local.tee 0
                            i32.const 1
                            i32.shr_u
                            i32.const 2
                            i32.and
                            local.tee 1
                            i32.or
                            local.get 0
                            local.get 1
                            i32.shr_u
                            local.tee 0
                            i32.const 1
                            i32.shr_u
                            i32.const 1
                            i32.and
                            local.tee 1
                            i32.or
                            local.get 0
                            local.get 1
                            i32.shr_u
                            i32.add
                            i32.const 2
                            i32.shl
                            i32.const 4320
                            i32.add
                            i32.load
                            local.tee 1
                            i32.load offset=4
                            i32.const -8
                            i32.and
                            local.get 5
                            i32.sub
                            local.set 3
                            local.get 1
                            local.set 2
                            loop  ;; label = @13
                              block  ;; label = @14
                                local.get 2
                                i32.load offset=16
                                local.tee 0
                                i32.eqz
                                if  ;; label = @15
                                  local.get 2
                                  i32.load offset=20
                                  local.tee 0
                                  i32.eqz
                                  br_if 1 (;@14;)
                                end
                                local.get 0
                                i32.load offset=4
                                i32.const -8
                                i32.and
                                local.get 5
                                i32.sub
                                local.tee 2
                                local.get 3
                                local.get 2
                                local.get 3
                                i32.lt_u
                                local.tee 2
                                select
                                local.set 3
                                local.get 0
                                local.get 1
                                local.get 2
                                select
                                local.set 1
                                local.get 0
                                local.set 2
                                br 1 (;@13;)
                              end
                            end
                            local.get 1
                            i32.load offset=24
                            local.set 9
                            local.get 1
                            local.get 1
                            i32.load offset=12
                            local.tee 4
                            i32.ne
                            if  ;; label = @13
                              i32.const 4032
                              i32.load
                              local.get 1
                              i32.load offset=8
                              local.tee 0
                              i32.le_u
                              if  ;; label = @14
                                local.get 0
                                i32.load offset=12
                                drop
                              end
                              local.get 0
                              local.get 4
                              i32.store offset=12
                              local.get 4
                              local.get 0
                              i32.store offset=8
                              br 11 (;@2;)
                            end
                            local.get 1
                            i32.const 20
                            i32.add
                            local.tee 2
                            i32.load
                            local.tee 0
                            i32.eqz
                            if  ;; label = @13
                              local.get 1
                              i32.load offset=16
                              local.tee 0
                              i32.eqz
                              br_if 3 (;@10;)
                              local.get 1
                              i32.const 16
                              i32.add
                              local.set 2
                            end
                            loop  ;; label = @13
                              local.get 2
                              local.set 7
                              local.get 0
                              local.tee 4
                              i32.const 20
                              i32.add
                              local.tee 2
                              i32.load
                              local.tee 0
                              br_if 0 (;@13;)
                              local.get 4
                              i32.const 16
                              i32.add
                              local.set 2
                              local.get 4
                              i32.load offset=16
                              local.tee 0
                              br_if 0 (;@13;)
                            end
                            local.get 7
                            i32.const 0
                            i32.store
                            br 10 (;@2;)
                          end
                          i32.const -1
                          local.set 5
                          local.get 0
                          i32.const -65
                          i32.gt_u
                          br_if 0 (;@11;)
                          local.get 0
                          i32.const 11
                          i32.add
                          local.tee 0
                          i32.const -8
                          i32.and
                          local.set 5
                          i32.const 4020
                          i32.load
                          local.tee 7
                          i32.eqz
                          br_if 0 (;@11;)
                          i32.const 0
                          local.get 5
                          i32.sub
                          local.set 2
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block (result i32)  ;; label = @15
                                  i32.const 0
                                  local.get 0
                                  i32.const 8
                                  i32.shr_u
                                  local.tee 0
                                  i32.eqz
                                  br_if 0 (;@15;)
                                  drop
                                  i32.const 31
                                  local.get 5
                                  i32.const 16777215
                                  i32.gt_u
                                  br_if 0 (;@15;)
                                  drop
                                  local.get 0
                                  local.get 0
                                  i32.const 1048320
                                  i32.add
                                  i32.const 16
                                  i32.shr_u
                                  i32.const 8
                                  i32.and
                                  local.tee 0
                                  i32.shl
                                  local.tee 1
                                  local.get 1
                                  i32.const 520192
                                  i32.add
                                  i32.const 16
                                  i32.shr_u
                                  i32.const 4
                                  i32.and
                                  local.tee 1
                                  i32.shl
                                  local.tee 3
                                  local.get 3
                                  i32.const 245760
                                  i32.add
                                  i32.const 16
                                  i32.shr_u
                                  i32.const 2
                                  i32.and
                                  local.tee 3
                                  i32.shl
                                  i32.const 15
                                  i32.shr_u
                                  local.get 0
                                  local.get 1
                                  i32.or
                                  local.get 3
                                  i32.or
                                  i32.sub
                                  local.tee 0
                                  i32.const 1
                                  i32.shl
                                  local.get 5
                                  local.get 0
                                  i32.const 21
                                  i32.add
                                  i32.shr_u
                                  i32.const 1
                                  i32.and
                                  i32.or
                                  i32.const 28
                                  i32.add
                                end
                                local.tee 8
                                i32.const 2
                                i32.shl
                                i32.const 4320
                                i32.add
                                i32.load
                                local.tee 3
                                i32.eqz
                                if  ;; label = @15
                                  i32.const 0
                                  local.set 0
                                  br 1 (;@14;)
                                end
                                local.get 5
                                i32.const 0
                                i32.const 25
                                local.get 8
                                i32.const 1
                                i32.shr_u
                                i32.sub
                                local.get 8
                                i32.const 31
                                i32.eq
                                select
                                i32.shl
                                local.set 1
                                i32.const 0
                                local.set 0
                                loop  ;; label = @15
                                  block  ;; label = @16
                                    local.get 3
                                    i32.load offset=4
                                    i32.const -8
                                    i32.and
                                    local.get 5
                                    i32.sub
                                    local.tee 6
                                    local.get 2
                                    i32.ge_u
                                    br_if 0 (;@16;)
                                    local.get 3
                                    local.set 4
                                    local.get 6
                                    local.tee 2
                                    br_if 0 (;@16;)
                                    i32.const 0
                                    local.set 2
                                    local.get 3
                                    local.set 0
                                    br 3 (;@13;)
                                  end
                                  local.get 0
                                  local.get 3
                                  i32.load offset=20
                                  local.tee 6
                                  local.get 6
                                  local.get 3
                                  local.get 1
                                  i32.const 29
                                  i32.shr_u
                                  i32.const 4
                                  i32.and
                                  i32.add
                                  i32.load offset=16
                                  local.tee 3
                                  i32.eq
                                  select
                                  local.get 0
                                  local.get 6
                                  select
                                  local.set 0
                                  local.get 1
                                  local.get 3
                                  i32.const 0
                                  i32.ne
                                  i32.shl
                                  local.set 1
                                  local.get 3
                                  br_if 0 (;@15;)
                                end
                              end
                              local.get 0
                              local.get 4
                              i32.or
                              i32.eqz
                              if  ;; label = @14
                                i32.const 2
                                local.get 8
                                i32.shl
                                local.tee 0
                                i32.const 0
                                local.get 0
                                i32.sub
                                i32.or
                                local.get 7
                                i32.and
                                local.tee 0
                                i32.eqz
                                br_if 3 (;@11;)
                                local.get 0
                                i32.const 0
                                local.get 0
                                i32.sub
                                i32.and
                                i32.const -1
                                i32.add
                                local.tee 0
                                local.get 0
                                i32.const 12
                                i32.shr_u
                                i32.const 16
                                i32.and
                                local.tee 0
                                i32.shr_u
                                local.tee 1
                                i32.const 5
                                i32.shr_u
                                i32.const 8
                                i32.and
                                local.tee 3
                                local.get 0
                                i32.or
                                local.get 1
                                local.get 3
                                i32.shr_u
                                local.tee 0
                                i32.const 2
                                i32.shr_u
                                i32.const 4
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 2
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 1
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                i32.add
                                i32.const 2
                                i32.shl
                                i32.const 4320
                                i32.add
                                i32.load
                                local.set 0
                              end
                              local.get 0
                              i32.eqz
                              br_if 1 (;@12;)
                            end
                            loop  ;; label = @13
                              local.get 0
                              i32.load offset=4
                              i32.const -8
                              i32.and
                              local.get 5
                              i32.sub
                              local.tee 3
                              local.get 2
                              i32.lt_u
                              local.set 1
                              local.get 3
                              local.get 2
                              local.get 1
                              select
                              local.set 2
                              local.get 0
                              local.get 4
                              local.get 1
                              select
                              local.set 4
                              local.get 0
                              i32.load offset=16
                              local.tee 1
                              if (result i32)  ;; label = @14
                                local.get 1
                              else
                                local.get 0
                                i32.load offset=20
                              end
                              local.tee 0
                              br_if 0 (;@13;)
                            end
                          end
                          local.get 4
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 2
                          i32.const 4024
                          i32.load
                          local.get 5
                          i32.sub
                          i32.ge_u
                          br_if 0 (;@11;)
                          local.get 4
                          i32.load offset=24
                          local.set 8
                          local.get 4
                          local.get 4
                          i32.load offset=12
                          local.tee 1
                          i32.ne
                          if  ;; label = @12
                            i32.const 4032
                            i32.load
                            local.get 4
                            i32.load offset=8
                            local.tee 0
                            i32.le_u
                            if  ;; label = @13
                              local.get 0
                              i32.load offset=12
                              drop
                            end
                            local.get 0
                            local.get 1
                            i32.store offset=12
                            local.get 1
                            local.get 0
                            i32.store offset=8
                            br 9 (;@3;)
                          end
                          local.get 4
                          i32.const 20
                          i32.add
                          local.tee 3
                          i32.load
                          local.tee 0
                          i32.eqz
                          if  ;; label = @12
                            local.get 4
                            i32.load offset=16
                            local.tee 0
                            i32.eqz
                            br_if 3 (;@9;)
                            local.get 4
                            i32.const 16
                            i32.add
                            local.set 3
                          end
                          loop  ;; label = @12
                            local.get 3
                            local.set 6
                            local.get 0
                            local.tee 1
                            i32.const 20
                            i32.add
                            local.tee 3
                            i32.load
                            local.tee 0
                            br_if 0 (;@12;)
                            local.get 1
                            i32.const 16
                            i32.add
                            local.set 3
                            local.get 1
                            i32.load offset=16
                            local.tee 0
                            br_if 0 (;@12;)
                          end
                          local.get 6
                          i32.const 0
                          i32.store
                          br 8 (;@3;)
                        end
                        i32.const 4024
                        i32.load
                        local.tee 1
                        local.get 5
                        i32.ge_u
                        if  ;; label = @11
                          i32.const 4036
                          i32.load
                          local.set 0
                          block  ;; label = @12
                            local.get 1
                            local.get 5
                            i32.sub
                            local.tee 2
                            i32.const 16
                            i32.ge_u
                            if  ;; label = @13
                              i32.const 4024
                              local.get 2
                              i32.store
                              i32.const 4036
                              local.get 0
                              local.get 5
                              i32.add
                              local.tee 3
                              i32.store
                              local.get 3
                              local.get 2
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              local.get 0
                              local.get 1
                              i32.add
                              local.get 2
                              i32.store
                              local.get 0
                              local.get 5
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              br 1 (;@12;)
                            end
                            i32.const 4036
                            i32.const 0
                            i32.store
                            i32.const 4024
                            i32.const 0
                            i32.store
                            local.get 0
                            local.get 1
                            i32.const 3
                            i32.or
                            i32.store offset=4
                            local.get 0
                            local.get 1
                            i32.add
                            local.tee 1
                            local.get 1
                            i32.load offset=4
                            i32.const 1
                            i32.or
                            i32.store offset=4
                          end
                          local.get 0
                          i32.const 8
                          i32.add
                          local.set 0
                          br 10 (;@1;)
                        end
                        i32.const 4028
                        i32.load
                        local.tee 1
                        local.get 5
                        i32.gt_u
                        if  ;; label = @11
                          i32.const 4028
                          local.get 1
                          local.get 5
                          i32.sub
                          local.tee 1
                          i32.store
                          i32.const 4040
                          i32.const 4040
                          i32.load
                          local.tee 0
                          local.get 5
                          i32.add
                          local.tee 2
                          i32.store
                          local.get 2
                          local.get 1
                          i32.const 1
                          i32.or
                          i32.store offset=4
                          local.get 0
                          local.get 5
                          i32.const 3
                          i32.or
                          i32.store offset=4
                          local.get 0
                          i32.const 8
                          i32.add
                          local.set 0
                          br 10 (;@1;)
                        end
                        i32.const 0
                        local.set 0
                        local.get 5
                        i32.const 47
                        i32.add
                        local.tee 4
                        block (result i32)  ;; label = @11
                          i32.const 4488
                          i32.load
                          if  ;; label = @12
                            i32.const 4496
                            i32.load
                            br 1 (;@11;)
                          end
                          i32.const 4500
                          i64.const -1
                          i64.store align=4
                          i32.const 4492
                          i64.const 17592186048512
                          i64.store align=4
                          i32.const 4488
                          local.get 11
                          i32.const 12
                          i32.add
                          i32.const -16
                          i32.and
                          i32.const 1431655768
                          i32.xor
                          i32.store
                          i32.const 4508
                          i32.const 0
                          i32.store
                          i32.const 4460
                          i32.const 0
                          i32.store
                          i32.const 4096
                        end
                        local.tee 2
                        i32.add
                        local.tee 6
                        i32.const 0
                        local.get 2
                        i32.sub
                        local.tee 7
                        i32.and
                        local.tee 2
                        local.get 5
                        i32.le_u
                        br_if 9 (;@1;)
                        i32.const 4456
                        i32.load
                        local.tee 3
                        if  ;; label = @11
                          i32.const 4448
                          i32.load
                          local.tee 8
                          local.get 2
                          i32.add
                          local.tee 9
                          local.get 8
                          i32.le_u
                          local.get 9
                          local.get 3
                          i32.gt_u
                          i32.or
                          br_if 10 (;@1;)
                        end
                        i32.const 4460
                        i32.load8_u
                        i32.const 4
                        i32.and
                        br_if 4 (;@6;)
                        block  ;; label = @11
                          block  ;; label = @12
                            i32.const 4040
                            i32.load
                            local.tee 3
                            if  ;; label = @13
                              i32.const 4464
                              local.set 0
                              loop  ;; label = @14
                                local.get 0
                                i32.load
                                local.tee 8
                                local.get 3
                                i32.le_u
                                if  ;; label = @15
                                  local.get 8
                                  local.get 0
                                  i32.load offset=4
                                  i32.add
                                  local.get 3
                                  i32.gt_u
                                  br_if 3 (;@12;)
                                end
                                local.get 0
                                i32.load offset=8
                                local.tee 0
                                br_if 0 (;@14;)
                              end
                            end
                            i32.const 0
                            call 12
                            local.tee 1
                            i32.const -1
                            i32.eq
                            br_if 5 (;@7;)
                            local.get 2
                            local.set 6
                            i32.const 4492
                            i32.load
                            local.tee 0
                            i32.const -1
                            i32.add
                            local.tee 3
                            local.get 1
                            i32.and
                            if  ;; label = @13
                              local.get 2
                              local.get 1
                              i32.sub
                              local.get 1
                              local.get 3
                              i32.add
                              i32.const 0
                              local.get 0
                              i32.sub
                              i32.and
                              i32.add
                              local.set 6
                            end
                            local.get 6
                            local.get 5
                            i32.le_u
                            local.get 6
                            i32.const 2147483646
                            i32.gt_u
                            i32.or
                            br_if 5 (;@7;)
                            i32.const 4456
                            i32.load
                            local.tee 0
                            if  ;; label = @13
                              i32.const 4448
                              i32.load
                              local.tee 3
                              local.get 6
                              i32.add
                              local.tee 7
                              local.get 3
                              i32.le_u
                              local.get 7
                              local.get 0
                              i32.gt_u
                              i32.or
                              br_if 6 (;@7;)
                            end
                            local.get 6
                            call 12
                            local.tee 0
                            local.get 1
                            i32.ne
                            br_if 1 (;@11;)
                            br 7 (;@5;)
                          end
                          local.get 6
                          local.get 1
                          i32.sub
                          local.get 7
                          i32.and
                          local.tee 6
                          i32.const 2147483646
                          i32.gt_u
                          br_if 4 (;@7;)
                          local.get 6
                          call 12
                          local.tee 1
                          local.get 0
                          i32.load
                          local.get 0
                          i32.load offset=4
                          i32.add
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 1
                          local.set 0
                        end
                        local.get 0
                        i32.const -1
                        i32.eq
                        local.get 5
                        i32.const 48
                        i32.add
                        local.get 6
                        i32.le_u
                        i32.or
                        i32.eqz
                        if  ;; label = @11
                          i32.const 4496
                          i32.load
                          local.tee 1
                          local.get 4
                          local.get 6
                          i32.sub
                          i32.add
                          i32.const 0
                          local.get 1
                          i32.sub
                          i32.and
                          local.tee 1
                          i32.const 2147483646
                          i32.gt_u
                          if  ;; label = @12
                            local.get 0
                            local.set 1
                            br 7 (;@5;)
                          end
                          local.get 1
                          call 12
                          i32.const -1
                          i32.ne
                          if  ;; label = @12
                            local.get 1
                            local.get 6
                            i32.add
                            local.set 6
                            local.get 0
                            local.set 1
                            br 7 (;@5;)
                          end
                          i32.const 0
                          local.get 6
                          i32.sub
                          call 12
                          drop
                          br 4 (;@7;)
                        end
                        local.get 0
                        local.tee 1
                        i32.const -1
                        i32.ne
                        br_if 5 (;@5;)
                        br 3 (;@7;)
                      end
                      i32.const 0
                      local.set 4
                      br 7 (;@2;)
                    end
                    i32.const 0
                    local.set 1
                    br 5 (;@3;)
                  end
                  local.get 1
                  i32.const -1
                  i32.ne
                  br_if 2 (;@5;)
                end
                i32.const 4460
                i32.const 4460
                i32.load
                i32.const 4
                i32.or
                i32.store
              end
              local.get 2
              i32.const 2147483646
              i32.gt_u
              br_if 1 (;@4;)
              local.get 2
              call 12
              local.tee 1
              i32.const 0
              call 12
              local.tee 0
              i32.ge_u
              local.get 1
              i32.const -1
              i32.eq
              i32.or
              local.get 0
              i32.const -1
              i32.eq
              i32.or
              br_if 1 (;@4;)
              local.get 0
              local.get 1
              i32.sub
              local.tee 6
              local.get 5
              i32.const 40
              i32.add
              i32.le_u
              br_if 1 (;@4;)
            end
            i32.const 4448
            i32.const 4448
            i32.load
            local.get 6
            i32.add
            local.tee 0
            i32.store
            local.get 0
            i32.const 4452
            i32.load
            i32.gt_u
            if  ;; label = @5
              i32.const 4452
              local.get 0
              i32.store
            end
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  i32.const 4040
                  i32.load
                  local.tee 3
                  if  ;; label = @8
                    i32.const 4464
                    local.set 0
                    loop  ;; label = @9
                      local.get 1
                      local.get 0
                      i32.load
                      local.tee 2
                      local.get 0
                      i32.load offset=4
                      local.tee 4
                      i32.add
                      i32.eq
                      br_if 2 (;@7;)
                      local.get 0
                      i32.load offset=8
                      local.tee 0
                      br_if 0 (;@9;)
                    end
                    br 2 (;@6;)
                  end
                  i32.const 4032
                  i32.load
                  local.tee 0
                  i32.const 0
                  local.get 1
                  local.get 0
                  i32.ge_u
                  select
                  i32.eqz
                  if  ;; label = @8
                    i32.const 4032
                    local.get 1
                    i32.store
                  end
                  i32.const 0
                  local.set 0
                  i32.const 4468
                  local.get 6
                  i32.store
                  i32.const 4464
                  local.get 1
                  i32.store
                  i32.const 4048
                  i32.const -1
                  i32.store
                  i32.const 4052
                  i32.const 4488
                  i32.load
                  i32.store
                  i32.const 4476
                  i32.const 0
                  i32.store
                  loop  ;; label = @8
                    local.get 0
                    i32.const 3
                    i32.shl
                    local.tee 2
                    i32.const 4064
                    i32.add
                    local.get 2
                    i32.const 4056
                    i32.add
                    local.tee 3
                    i32.store
                    local.get 2
                    i32.const 4068
                    i32.add
                    local.get 3
                    i32.store
                    local.get 0
                    i32.const 1
                    i32.add
                    local.tee 0
                    i32.const 32
                    i32.ne
                    br_if 0 (;@8;)
                  end
                  i32.const 4028
                  local.get 6
                  i32.const -40
                  i32.add
                  local.tee 0
                  i32.const -8
                  local.get 1
                  i32.sub
                  i32.const 7
                  i32.and
                  i32.const 0
                  local.get 1
                  i32.const 8
                  i32.add
                  i32.const 7
                  i32.and
                  select
                  local.tee 2
                  i32.sub
                  local.tee 3
                  i32.store
                  i32.const 4040
                  local.get 1
                  local.get 2
                  i32.add
                  local.tee 2
                  i32.store
                  local.get 2
                  local.get 3
                  i32.const 1
                  i32.or
                  i32.store offset=4
                  local.get 0
                  local.get 1
                  i32.add
                  i32.const 40
                  i32.store offset=4
                  i32.const 4044
                  i32.const 4504
                  i32.load
                  i32.store
                  br 2 (;@5;)
                end
                local.get 0
                i32.load8_u offset=12
                i32.const 8
                i32.and
                local.get 1
                local.get 3
                i32.le_u
                i32.or
                local.get 2
                local.get 3
                i32.gt_u
                i32.or
                br_if 0 (;@6;)
                local.get 0
                local.get 4
                local.get 6
                i32.add
                i32.store offset=4
                i32.const 4040
                local.get 3
                i32.const -8
                local.get 3
                i32.sub
                i32.const 7
                i32.and
                i32.const 0
                local.get 3
                i32.const 8
                i32.add
                i32.const 7
                i32.and
                select
                local.tee 0
                i32.add
                local.tee 1
                i32.store
                i32.const 4028
                i32.const 4028
                i32.load
                local.get 6
                i32.add
                local.tee 2
                local.get 0
                i32.sub
                local.tee 0
                i32.store
                local.get 1
                local.get 0
                i32.const 1
                i32.or
                i32.store offset=4
                local.get 2
                local.get 3
                i32.add
                i32.const 40
                i32.store offset=4
                i32.const 4044
                i32.const 4504
                i32.load
                i32.store
                br 1 (;@5;)
              end
              local.get 1
              i32.const 4032
              i32.load
              local.tee 4
              i32.lt_u
              if  ;; label = @6
                i32.const 4032
                local.get 1
                i32.store
                local.get 1
                local.set 4
              end
              local.get 1
              local.get 6
              i32.add
              local.set 2
              i32.const 4464
              local.set 0
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          loop  ;; label = @12
                            local.get 2
                            local.get 0
                            i32.load
                            i32.ne
                            if  ;; label = @13
                              local.get 0
                              i32.load offset=8
                              local.tee 0
                              br_if 1 (;@12;)
                              br 2 (;@11;)
                            end
                          end
                          local.get 0
                          i32.load8_u offset=12
                          i32.const 8
                          i32.and
                          i32.eqz
                          br_if 1 (;@10;)
                        end
                        i32.const 4464
                        local.set 0
                        loop  ;; label = @11
                          local.get 0
                          i32.load
                          local.tee 2
                          local.get 3
                          i32.le_u
                          if  ;; label = @12
                            local.get 2
                            local.get 0
                            i32.load offset=4
                            i32.add
                            local.tee 4
                            local.get 3
                            i32.gt_u
                            br_if 3 (;@9;)
                          end
                          local.get 0
                          i32.load offset=8
                          local.set 0
                          br 0 (;@11;)
                        end
                        unreachable
                      end
                      local.get 0
                      local.get 1
                      i32.store
                      local.get 0
                      local.get 0
                      i32.load offset=4
                      local.get 6
                      i32.add
                      i32.store offset=4
                      local.get 1
                      i32.const -8
                      local.get 1
                      i32.sub
                      i32.const 7
                      i32.and
                      i32.const 0
                      local.get 1
                      i32.const 8
                      i32.add
                      i32.const 7
                      i32.and
                      select
                      i32.add
                      local.tee 9
                      local.get 5
                      i32.const 3
                      i32.or
                      i32.store offset=4
                      local.get 2
                      i32.const -8
                      local.get 2
                      i32.sub
                      i32.const 7
                      i32.and
                      i32.const 0
                      local.get 2
                      i32.const 8
                      i32.add
                      i32.const 7
                      i32.and
                      select
                      i32.add
                      local.tee 1
                      local.get 9
                      i32.sub
                      local.get 5
                      i32.sub
                      local.set 0
                      local.get 5
                      local.get 9
                      i32.add
                      local.set 7
                      local.get 1
                      local.get 3
                      i32.eq
                      if  ;; label = @10
                        i32.const 4040
                        local.get 7
                        i32.store
                        i32.const 4028
                        i32.const 4028
                        i32.load
                        local.get 0
                        i32.add
                        local.tee 0
                        i32.store
                        local.get 7
                        local.get 0
                        i32.const 1
                        i32.or
                        i32.store offset=4
                        br 3 (;@7;)
                      end
                      local.get 1
                      i32.const 4036
                      i32.load
                      i32.eq
                      if  ;; label = @10
                        i32.const 4036
                        local.get 7
                        i32.store
                        i32.const 4024
                        i32.const 4024
                        i32.load
                        local.get 0
                        i32.add
                        local.tee 0
                        i32.store
                        local.get 7
                        local.get 0
                        i32.const 1
                        i32.or
                        i32.store offset=4
                        local.get 0
                        local.get 7
                        i32.add
                        local.get 0
                        i32.store
                        br 3 (;@7;)
                      end
                      local.get 1
                      i32.load offset=4
                      local.tee 2
                      i32.const 3
                      i32.and
                      i32.const 1
                      i32.eq
                      if  ;; label = @10
                        local.get 2
                        i32.const -8
                        i32.and
                        local.set 10
                        block  ;; label = @11
                          local.get 2
                          i32.const 255
                          i32.le_u
                          if  ;; label = @12
                            local.get 1
                            i32.load offset=8
                            local.tee 3
                            local.get 2
                            i32.const 3
                            i32.shr_u
                            local.tee 4
                            i32.const 3
                            i32.shl
                            i32.const 4056
                            i32.add
                            i32.ne
                            drop
                            local.get 3
                            local.get 1
                            i32.load offset=12
                            local.tee 2
                            i32.eq
                            if  ;; label = @13
                              i32.const 4016
                              i32.const 4016
                              i32.load
                              i32.const -2
                              local.get 4
                              i32.rotl
                              i32.and
                              i32.store
                              br 2 (;@11;)
                            end
                            local.get 3
                            local.get 2
                            i32.store offset=12
                            local.get 2
                            local.get 3
                            i32.store offset=8
                            br 1 (;@11;)
                          end
                          local.get 1
                          i32.load offset=24
                          local.set 8
                          block  ;; label = @12
                            local.get 1
                            local.get 1
                            i32.load offset=12
                            local.tee 6
                            i32.ne
                            if  ;; label = @13
                              local.get 4
                              local.get 1
                              i32.load offset=8
                              local.tee 2
                              i32.le_u
                              if  ;; label = @14
                                local.get 2
                                i32.load offset=12
                                drop
                              end
                              local.get 2
                              local.get 6
                              i32.store offset=12
                              local.get 6
                              local.get 2
                              i32.store offset=8
                              br 1 (;@12;)
                            end
                            block  ;; label = @13
                              local.get 1
                              i32.const 20
                              i32.add
                              local.tee 3
                              i32.load
                              local.tee 5
                              br_if 0 (;@13;)
                              local.get 1
                              i32.const 16
                              i32.add
                              local.tee 3
                              i32.load
                              local.tee 5
                              br_if 0 (;@13;)
                              i32.const 0
                              local.set 6
                              br 1 (;@12;)
                            end
                            loop  ;; label = @13
                              local.get 3
                              local.set 2
                              local.get 5
                              local.tee 6
                              i32.const 20
                              i32.add
                              local.tee 3
                              i32.load
                              local.tee 5
                              br_if 0 (;@13;)
                              local.get 6
                              i32.const 16
                              i32.add
                              local.set 3
                              local.get 6
                              i32.load offset=16
                              local.tee 5
                              br_if 0 (;@13;)
                            end
                            local.get 2
                            i32.const 0
                            i32.store
                          end
                          local.get 8
                          i32.eqz
                          br_if 0 (;@11;)
                          block  ;; label = @12
                            local.get 1
                            local.get 1
                            i32.load offset=28
                            local.tee 2
                            i32.const 2
                            i32.shl
                            i32.const 4320
                            i32.add
                            local.tee 3
                            i32.load
                            i32.eq
                            if  ;; label = @13
                              local.get 3
                              local.get 6
                              i32.store
                              local.get 6
                              br_if 1 (;@12;)
                              i32.const 4020
                              i32.const 4020
                              i32.load
                              i32.const -2
                              local.get 2
                              i32.rotl
                              i32.and
                              i32.store
                              br 2 (;@11;)
                            end
                            local.get 8
                            i32.const 16
                            i32.const 20
                            local.get 8
                            i32.load offset=16
                            local.get 1
                            i32.eq
                            select
                            i32.add
                            local.get 6
                            i32.store
                            local.get 6
                            i32.eqz
                            br_if 1 (;@11;)
                          end
                          local.get 6
                          local.get 8
                          i32.store offset=24
                          local.get 1
                          i32.load offset=16
                          local.tee 2
                          if  ;; label = @12
                            local.get 6
                            local.get 2
                            i32.store offset=16
                            local.get 2
                            local.get 6
                            i32.store offset=24
                          end
                          local.get 1
                          i32.load offset=20
                          local.tee 2
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 6
                          local.get 2
                          i32.store offset=20
                          local.get 2
                          local.get 6
                          i32.store offset=24
                        end
                        local.get 1
                        local.get 10
                        i32.add
                        local.set 1
                        local.get 0
                        local.get 10
                        i32.add
                        local.set 0
                      end
                      local.get 1
                      local.get 1
                      i32.load offset=4
                      i32.const -2
                      i32.and
                      i32.store offset=4
                      local.get 7
                      local.get 0
                      i32.const 1
                      i32.or
                      i32.store offset=4
                      local.get 0
                      local.get 7
                      i32.add
                      local.get 0
                      i32.store
                      local.get 0
                      i32.const 255
                      i32.le_u
                      if  ;; label = @10
                        local.get 0
                        i32.const 3
                        i32.shr_u
                        local.tee 1
                        i32.const 3
                        i32.shl
                        i32.const 4056
                        i32.add
                        local.set 0
                        block (result i32)  ;; label = @11
                          i32.const 4016
                          i32.load
                          local.tee 2
                          i32.const 1
                          local.get 1
                          i32.shl
                          local.tee 1
                          i32.and
                          i32.eqz
                          if  ;; label = @12
                            i32.const 4016
                            local.get 1
                            local.get 2
                            i32.or
                            i32.store
                            local.get 0
                            br 1 (;@11;)
                          end
                          local.get 0
                          i32.load offset=8
                        end
                        local.set 1
                        local.get 0
                        local.get 7
                        i32.store offset=8
                        local.get 1
                        local.get 7
                        i32.store offset=12
                        local.get 7
                        local.get 0
                        i32.store offset=12
                        local.get 7
                        local.get 1
                        i32.store offset=8
                        br 3 (;@7;)
                      end
                      local.get 7
                      block (result i32)  ;; label = @10
                        i32.const 0
                        local.get 0
                        i32.const 8
                        i32.shr_u
                        local.tee 1
                        i32.eqz
                        br_if 0 (;@10;)
                        drop
                        i32.const 31
                        local.get 0
                        i32.const 16777215
                        i32.gt_u
                        br_if 0 (;@10;)
                        drop
                        local.get 1
                        local.get 1
                        i32.const 1048320
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 8
                        i32.and
                        local.tee 1
                        i32.shl
                        local.tee 2
                        local.get 2
                        i32.const 520192
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 4
                        i32.and
                        local.tee 2
                        i32.shl
                        local.tee 3
                        local.get 3
                        i32.const 245760
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 2
                        i32.and
                        local.tee 3
                        i32.shl
                        i32.const 15
                        i32.shr_u
                        local.get 1
                        local.get 2
                        i32.or
                        local.get 3
                        i32.or
                        i32.sub
                        local.tee 1
                        i32.const 1
                        i32.shl
                        local.get 0
                        local.get 1
                        i32.const 21
                        i32.add
                        i32.shr_u
                        i32.const 1
                        i32.and
                        i32.or
                        i32.const 28
                        i32.add
                      end
                      local.tee 1
                      i32.store offset=28
                      local.get 7
                      i64.const 0
                      i64.store offset=16 align=4
                      local.get 1
                      i32.const 2
                      i32.shl
                      i32.const 4320
                      i32.add
                      local.set 2
                      block  ;; label = @10
                        i32.const 4020
                        i32.load
                        local.tee 3
                        i32.const 1
                        local.get 1
                        i32.shl
                        local.tee 4
                        i32.and
                        i32.eqz
                        if  ;; label = @11
                          i32.const 4020
                          local.get 3
                          local.get 4
                          i32.or
                          i32.store
                          local.get 2
                          local.get 7
                          i32.store
                          br 1 (;@10;)
                        end
                        local.get 0
                        i32.const 0
                        i32.const 25
                        local.get 1
                        i32.const 1
                        i32.shr_u
                        i32.sub
                        local.get 1
                        i32.const 31
                        i32.eq
                        select
                        i32.shl
                        local.set 3
                        local.get 2
                        i32.load
                        local.set 1
                        loop  ;; label = @11
                          local.get 1
                          local.tee 2
                          i32.load offset=4
                          i32.const -8
                          i32.and
                          local.get 0
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 3
                          i32.const 29
                          i32.shr_u
                          local.set 1
                          local.get 3
                          i32.const 1
                          i32.shl
                          local.set 3
                          local.get 2
                          local.get 1
                          i32.const 4
                          i32.and
                          i32.add
                          local.tee 4
                          i32.load offset=16
                          local.tee 1
                          br_if 0 (;@11;)
                        end
                        local.get 4
                        local.get 7
                        i32.store offset=16
                      end
                      local.get 7
                      local.get 2
                      i32.store offset=24
                      local.get 7
                      local.get 7
                      i32.store offset=12
                      local.get 7
                      local.get 7
                      i32.store offset=8
                      br 2 (;@7;)
                    end
                    i32.const 4028
                    local.get 6
                    i32.const -40
                    i32.add
                    local.tee 0
                    i32.const -8
                    local.get 1
                    i32.sub
                    i32.const 7
                    i32.and
                    i32.const 0
                    local.get 1
                    i32.const 8
                    i32.add
                    i32.const 7
                    i32.and
                    select
                    local.tee 2
                    i32.sub
                    local.tee 7
                    i32.store
                    i32.const 4040
                    local.get 1
                    local.get 2
                    i32.add
                    local.tee 2
                    i32.store
                    local.get 2
                    local.get 7
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 0
                    local.get 1
                    i32.add
                    i32.const 40
                    i32.store offset=4
                    i32.const 4044
                    i32.const 4504
                    i32.load
                    i32.store
                    local.get 3
                    local.get 4
                    i32.const 39
                    local.get 4
                    i32.sub
                    i32.const 7
                    i32.and
                    i32.const 0
                    local.get 4
                    i32.const -39
                    i32.add
                    i32.const 7
                    i32.and
                    select
                    i32.add
                    i32.const -47
                    i32.add
                    local.tee 0
                    local.get 0
                    local.get 3
                    i32.const 16
                    i32.add
                    i32.lt_u
                    select
                    local.tee 2
                    i32.const 27
                    i32.store offset=4
                    local.get 2
                    i32.const 4472
                    i64.load align=4
                    i64.store offset=16 align=4
                    local.get 2
                    i32.const 4464
                    i64.load align=4
                    i64.store offset=8 align=4
                    i32.const 4472
                    local.get 2
                    i32.const 8
                    i32.add
                    i32.store
                    i32.const 4468
                    local.get 6
                    i32.store
                    i32.const 4464
                    local.get 1
                    i32.store
                    i32.const 4476
                    i32.const 0
                    i32.store
                    local.get 2
                    i32.const 24
                    i32.add
                    local.set 0
                    loop  ;; label = @9
                      local.get 0
                      i32.const 7
                      i32.store offset=4
                      local.get 0
                      i32.const 8
                      i32.add
                      local.set 1
                      local.get 0
                      i32.const 4
                      i32.add
                      local.set 0
                      local.get 4
                      local.get 1
                      i32.gt_u
                      br_if 0 (;@9;)
                    end
                    local.get 2
                    local.get 3
                    i32.eq
                    br_if 3 (;@5;)
                    local.get 2
                    local.get 2
                    i32.load offset=4
                    i32.const -2
                    i32.and
                    i32.store offset=4
                    local.get 3
                    local.get 2
                    local.get 3
                    i32.sub
                    local.tee 4
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 2
                    local.get 4
                    i32.store
                    local.get 4
                    i32.const 255
                    i32.le_u
                    if  ;; label = @9
                      local.get 4
                      i32.const 3
                      i32.shr_u
                      local.tee 1
                      i32.const 3
                      i32.shl
                      i32.const 4056
                      i32.add
                      local.set 0
                      block (result i32)  ;; label = @10
                        i32.const 4016
                        i32.load
                        local.tee 2
                        i32.const 1
                        local.get 1
                        i32.shl
                        local.tee 1
                        i32.and
                        i32.eqz
                        if  ;; label = @11
                          i32.const 4016
                          local.get 1
                          local.get 2
                          i32.or
                          i32.store
                          local.get 0
                          br 1 (;@10;)
                        end
                        local.get 0
                        i32.load offset=8
                      end
                      local.set 1
                      local.get 0
                      local.get 3
                      i32.store offset=8
                      local.get 1
                      local.get 3
                      i32.store offset=12
                      local.get 3
                      local.get 0
                      i32.store offset=12
                      local.get 3
                      local.get 1
                      i32.store offset=8
                      br 4 (;@5;)
                    end
                    local.get 3
                    i64.const 0
                    i64.store offset=16 align=4
                    local.get 3
                    block (result i32)  ;; label = @9
                      i32.const 0
                      local.get 4
                      i32.const 8
                      i32.shr_u
                      local.tee 0
                      i32.eqz
                      br_if 0 (;@9;)
                      drop
                      i32.const 31
                      local.get 4
                      i32.const 16777215
                      i32.gt_u
                      br_if 0 (;@9;)
                      drop
                      local.get 0
                      local.get 0
                      i32.const 1048320
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 8
                      i32.and
                      local.tee 0
                      i32.shl
                      local.tee 1
                      local.get 1
                      i32.const 520192
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 4
                      i32.and
                      local.tee 1
                      i32.shl
                      local.tee 2
                      local.get 2
                      i32.const 245760
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 2
                      i32.and
                      local.tee 2
                      i32.shl
                      i32.const 15
                      i32.shr_u
                      local.get 0
                      local.get 1
                      i32.or
                      local.get 2
                      i32.or
                      i32.sub
                      local.tee 0
                      i32.const 1
                      i32.shl
                      local.get 4
                      local.get 0
                      i32.const 21
                      i32.add
                      i32.shr_u
                      i32.const 1
                      i32.and
                      i32.or
                      i32.const 28
                      i32.add
                    end
                    local.tee 0
                    i32.store offset=28
                    local.get 0
                    i32.const 2
                    i32.shl
                    i32.const 4320
                    i32.add
                    local.set 1
                    block  ;; label = @9
                      i32.const 4020
                      i32.load
                      local.tee 2
                      i32.const 1
                      local.get 0
                      i32.shl
                      local.tee 6
                      i32.and
                      i32.eqz
                      if  ;; label = @10
                        i32.const 4020
                        local.get 2
                        local.get 6
                        i32.or
                        i32.store
                        local.get 1
                        local.get 3
                        i32.store
                        local.get 3
                        local.get 1
                        i32.store offset=24
                        br 1 (;@9;)
                      end
                      local.get 4
                      i32.const 0
                      i32.const 25
                      local.get 0
                      i32.const 1
                      i32.shr_u
                      i32.sub
                      local.get 0
                      i32.const 31
                      i32.eq
                      select
                      i32.shl
                      local.set 0
                      local.get 1
                      i32.load
                      local.set 1
                      loop  ;; label = @10
                        local.get 1
                        local.tee 2
                        i32.load offset=4
                        i32.const -8
                        i32.and
                        local.get 4
                        i32.eq
                        br_if 4 (;@6;)
                        local.get 0
                        i32.const 29
                        i32.shr_u
                        local.set 1
                        local.get 0
                        i32.const 1
                        i32.shl
                        local.set 0
                        local.get 2
                        local.get 1
                        i32.const 4
                        i32.and
                        i32.add
                        local.tee 6
                        i32.load offset=16
                        local.tee 1
                        br_if 0 (;@10;)
                      end
                      local.get 6
                      local.get 3
                      i32.store offset=16
                      local.get 3
                      local.get 2
                      i32.store offset=24
                    end
                    local.get 3
                    local.get 3
                    i32.store offset=12
                    local.get 3
                    local.get 3
                    i32.store offset=8
                    br 3 (;@5;)
                  end
                  local.get 2
                  i32.load offset=8
                  local.tee 0
                  local.get 7
                  i32.store offset=12
                  local.get 2
                  local.get 7
                  i32.store offset=8
                  local.get 7
                  i32.const 0
                  i32.store offset=24
                  local.get 7
                  local.get 2
                  i32.store offset=12
                  local.get 7
                  local.get 0
                  i32.store offset=8
                end
                local.get 9
                i32.const 8
                i32.add
                local.set 0
                br 5 (;@1;)
              end
              local.get 2
              i32.load offset=8
              local.tee 0
              local.get 3
              i32.store offset=12
              local.get 2
              local.get 3
              i32.store offset=8
              local.get 3
              i32.const 0
              i32.store offset=24
              local.get 3
              local.get 2
              i32.store offset=12
              local.get 3
              local.get 0
              i32.store offset=8
            end
            i32.const 4028
            i32.load
            local.tee 0
            local.get 5
            i32.le_u
            br_if 0 (;@4;)
            i32.const 4028
            local.get 0
            local.get 5
            i32.sub
            local.tee 1
            i32.store
            i32.const 4040
            i32.const 4040
            i32.load
            local.tee 0
            local.get 5
            i32.add
            local.tee 2
            i32.store
            local.get 2
            local.get 1
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 0
            local.get 5
            i32.const 3
            i32.or
            i32.store offset=4
            local.get 0
            i32.const 8
            i32.add
            local.set 0
            br 3 (;@1;)
          end
          i32.const 3948
          i32.const 48
          i32.store
          i32.const 0
          local.set 0
          br 2 (;@1;)
        end
        block  ;; label = @3
          local.get 8
          i32.eqz
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 4
            i32.load offset=28
            local.tee 0
            i32.const 2
            i32.shl
            i32.const 4320
            i32.add
            local.tee 3
            i32.load
            local.get 4
            i32.eq
            if  ;; label = @5
              local.get 3
              local.get 1
              i32.store
              local.get 1
              br_if 1 (;@4;)
              i32.const 4020
              local.get 7
              i32.const -2
              local.get 0
              i32.rotl
              i32.and
              local.tee 7
              i32.store
              br 2 (;@3;)
            end
            local.get 8
            i32.const 16
            i32.const 20
            local.get 8
            i32.load offset=16
            local.get 4
            i32.eq
            select
            i32.add
            local.get 1
            i32.store
            local.get 1
            i32.eqz
            br_if 1 (;@3;)
          end
          local.get 1
          local.get 8
          i32.store offset=24
          local.get 4
          i32.load offset=16
          local.tee 0
          if  ;; label = @4
            local.get 1
            local.get 0
            i32.store offset=16
            local.get 0
            local.get 1
            i32.store offset=24
          end
          local.get 4
          i32.load offset=20
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          local.get 0
          i32.store offset=20
          local.get 0
          local.get 1
          i32.store offset=24
        end
        block  ;; label = @3
          local.get 2
          i32.const 15
          i32.le_u
          if  ;; label = @4
            local.get 4
            local.get 2
            local.get 5
            i32.add
            local.tee 0
            i32.const 3
            i32.or
            i32.store offset=4
            local.get 0
            local.get 4
            i32.add
            local.tee 0
            local.get 0
            i32.load offset=4
            i32.const 1
            i32.or
            i32.store offset=4
            br 1 (;@3;)
          end
          local.get 4
          local.get 5
          i32.const 3
          i32.or
          i32.store offset=4
          local.get 4
          local.get 5
          i32.add
          local.tee 3
          local.get 2
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 2
          local.get 3
          i32.add
          local.get 2
          i32.store
          local.get 2
          i32.const 255
          i32.le_u
          if  ;; label = @4
            local.get 2
            i32.const 3
            i32.shr_u
            local.tee 1
            i32.const 3
            i32.shl
            i32.const 4056
            i32.add
            local.set 0
            block (result i32)  ;; label = @5
              i32.const 4016
              i32.load
              local.tee 2
              i32.const 1
              local.get 1
              i32.shl
              local.tee 1
              i32.and
              i32.eqz
              if  ;; label = @6
                i32.const 4016
                local.get 1
                local.get 2
                i32.or
                i32.store
                local.get 0
                br 1 (;@5;)
              end
              local.get 0
              i32.load offset=8
            end
            local.set 1
            local.get 0
            local.get 3
            i32.store offset=8
            local.get 1
            local.get 3
            i32.store offset=12
            local.get 3
            local.get 0
            i32.store offset=12
            local.get 3
            local.get 1
            i32.store offset=8
            br 1 (;@3;)
          end
          local.get 3
          block (result i32)  ;; label = @4
            i32.const 0
            local.get 2
            i32.const 8
            i32.shr_u
            local.tee 0
            i32.eqz
            br_if 0 (;@4;)
            drop
            i32.const 31
            local.get 2
            i32.const 16777215
            i32.gt_u
            br_if 0 (;@4;)
            drop
            local.get 0
            local.get 0
            i32.const 1048320
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 8
            i32.and
            local.tee 0
            i32.shl
            local.tee 1
            local.get 1
            i32.const 520192
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 4
            i32.and
            local.tee 1
            i32.shl
            local.tee 5
            local.get 5
            i32.const 245760
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 2
            i32.and
            local.tee 5
            i32.shl
            i32.const 15
            i32.shr_u
            local.get 0
            local.get 1
            i32.or
            local.get 5
            i32.or
            i32.sub
            local.tee 0
            i32.const 1
            i32.shl
            local.get 2
            local.get 0
            i32.const 21
            i32.add
            i32.shr_u
            i32.const 1
            i32.and
            i32.or
            i32.const 28
            i32.add
          end
          local.tee 0
          i32.store offset=28
          local.get 3
          i64.const 0
          i64.store offset=16 align=4
          local.get 0
          i32.const 2
          i32.shl
          i32.const 4320
          i32.add
          local.set 1
          block  ;; label = @4
            block  ;; label = @5
              local.get 7
              i32.const 1
              local.get 0
              i32.shl
              local.tee 5
              i32.and
              i32.eqz
              if  ;; label = @6
                i32.const 4020
                local.get 5
                local.get 7
                i32.or
                i32.store
                local.get 1
                local.get 3
                i32.store
                br 1 (;@5;)
              end
              local.get 2
              i32.const 0
              i32.const 25
              local.get 0
              i32.const 1
              i32.shr_u
              i32.sub
              local.get 0
              i32.const 31
              i32.eq
              select
              i32.shl
              local.set 0
              local.get 1
              i32.load
              local.set 5
              loop  ;; label = @6
                local.get 5
                local.tee 1
                i32.load offset=4
                i32.const -8
                i32.and
                local.get 2
                i32.eq
                br_if 2 (;@4;)
                local.get 0
                i32.const 29
                i32.shr_u
                local.set 5
                local.get 0
                i32.const 1
                i32.shl
                local.set 0
                local.get 1
                local.get 5
                i32.const 4
                i32.and
                i32.add
                local.tee 6
                i32.load offset=16
                local.tee 5
                br_if 0 (;@6;)
              end
              local.get 6
              local.get 3
              i32.store offset=16
            end
            local.get 3
            local.get 1
            i32.store offset=24
            local.get 3
            local.get 3
            i32.store offset=12
            local.get 3
            local.get 3
            i32.store offset=8
            br 1 (;@3;)
          end
          local.get 1
          i32.load offset=8
          local.tee 0
          local.get 3
          i32.store offset=12
          local.get 1
          local.get 3
          i32.store offset=8
          local.get 3
          i32.const 0
          i32.store offset=24
          local.get 3
          local.get 1
          i32.store offset=12
          local.get 3
          local.get 0
          i32.store offset=8
        end
        local.get 4
        i32.const 8
        i32.add
        local.set 0
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 9
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 1
          i32.load offset=28
          local.tee 0
          i32.const 2
          i32.shl
          i32.const 4320
          i32.add
          local.tee 2
          i32.load
          local.get 1
          i32.eq
          if  ;; label = @4
            local.get 2
            local.get 4
            i32.store
            local.get 4
            br_if 1 (;@3;)
            i32.const 4020
            local.get 10
            i32.const -2
            local.get 0
            i32.rotl
            i32.and
            i32.store
            br 2 (;@2;)
          end
          local.get 9
          i32.const 16
          i32.const 20
          local.get 9
          i32.load offset=16
          local.get 1
          i32.eq
          select
          i32.add
          local.get 4
          i32.store
          local.get 4
          i32.eqz
          br_if 1 (;@2;)
        end
        local.get 4
        local.get 9
        i32.store offset=24
        local.get 1
        i32.load offset=16
        local.tee 0
        if  ;; label = @3
          local.get 4
          local.get 0
          i32.store offset=16
          local.get 0
          local.get 4
          i32.store offset=24
        end
        local.get 1
        i32.load offset=20
        local.tee 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 4
        local.get 0
        i32.store offset=20
        local.get 0
        local.get 4
        i32.store offset=24
      end
      block  ;; label = @2
        local.get 3
        i32.const 15
        i32.le_u
        if  ;; label = @3
          local.get 1
          local.get 3
          local.get 5
          i32.add
          local.tee 0
          i32.const 3
          i32.or
          i32.store offset=4
          local.get 0
          local.get 1
          i32.add
          local.tee 0
          local.get 0
          i32.load offset=4
          i32.const 1
          i32.or
          i32.store offset=4
          br 1 (;@2;)
        end
        local.get 1
        local.get 5
        i32.const 3
        i32.or
        i32.store offset=4
        local.get 1
        local.get 5
        i32.add
        local.tee 4
        local.get 3
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 3
        local.get 4
        i32.add
        local.get 3
        i32.store
        local.get 8
        if  ;; label = @3
          local.get 8
          i32.const 3
          i32.shr_u
          local.tee 5
          i32.const 3
          i32.shl
          i32.const 4056
          i32.add
          local.set 0
          i32.const 4036
          i32.load
          local.set 2
          block (result i32)  ;; label = @4
            i32.const 1
            local.get 5
            i32.shl
            local.tee 5
            local.get 6
            i32.and
            i32.eqz
            if  ;; label = @5
              i32.const 4016
              local.get 5
              local.get 6
              i32.or
              i32.store
              local.get 0
              br 1 (;@4;)
            end
            local.get 0
            i32.load offset=8
          end
          local.set 5
          local.get 0
          local.get 2
          i32.store offset=8
          local.get 5
          local.get 2
          i32.store offset=12
          local.get 2
          local.get 0
          i32.store offset=12
          local.get 2
          local.get 5
          i32.store offset=8
        end
        i32.const 4036
        local.get 4
        i32.store
        i32.const 4024
        local.get 3
        i32.store
      end
      local.get 1
      i32.const 8
      i32.add
      local.set 0
    end
    local.get 11
    i32.const 16
    i32.add
    global.set 0
    local.get 0)
  (func (;14;) (type 11) (param i32 i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 256
    i32.sub
    local.tee 5
    global.set 0
    local.get 4
    i32.const 73728
    i32.and
    local.get 2
    local.get 3
    i32.le_s
    i32.or
    i32.eqz
    if  ;; label = @1
      local.get 5
      local.get 1
      local.get 2
      local.get 3
      i32.sub
      local.tee 2
      i32.const 256
      local.get 2
      i32.const 256
      i32.lt_u
      local.tee 1
      select
      call 9
      drop
      local.get 1
      i32.eqz
      if  ;; label = @2
        loop  ;; label = @3
          local.get 0
          local.get 5
          i32.const 256
          call 15
          local.get 2
          i32.const -256
          i32.add
          local.tee 2
          i32.const 255
          i32.gt_u
          br_if 0 (;@3;)
        end
      end
      local.get 0
      local.get 5
      local.get 2
      call 15
    end
    local.get 5
    i32.const 256
    i32.add
    global.set 0)
  (func (;15;) (type 4) (param i32 i32 i32)
    local.get 0
    i32.load8_u
    i32.const 32
    i32.and
    i32.eqz
    if  ;; label = @1
      local.get 1
      local.get 2
      local.get 0
      call 49
    end)
  (func (;16;) (type 0) (param i32) (result i32)
    local.get 0
    i32.const -48
    i32.add
    i32.const 10
    i32.lt_u)
  (func (;17;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32 i32)
    local.get 2
    i32.eqz
    if  ;; label = @1
      i32.const 0
      return
    end
    block  ;; label = @1
      local.get 0
      i32.load8_u
      local.tee 3
      i32.eqz
      br_if 0 (;@1;)
      loop  ;; label = @2
        block  ;; label = @3
          local.get 3
          local.get 1
          i32.load8_u
          local.tee 4
          i32.ne
          br_if 0 (;@3;)
          local.get 2
          i32.const -1
          i32.add
          local.tee 2
          i32.eqz
          local.get 4
          i32.eqz
          i32.or
          br_if 0 (;@3;)
          local.get 1
          i32.const 1
          i32.add
          local.set 1
          local.get 0
          i32.load8_u offset=1
          local.set 3
          local.get 0
          i32.const 1
          i32.add
          local.set 0
          local.get 3
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
      end
      local.get 3
      local.set 5
    end
    local.get 5
    i32.const 255
    i32.and
    local.get 1
    i32.load8_u
    i32.sub)
  (func (;18;) (type 7) (param i32 i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64)
    global.get 0
    i32.const 2048
    i32.sub
    local.tee 4
    global.set 0
    local.get 4
    i32.const 1024
    i32.add
    local.get 1
    call 23
    local.get 4
    i32.const 1024
    i32.add
    local.get 0
    call 22
    local.get 4
    local.get 4
    i32.const 1024
    i32.add
    call 23
    local.get 3
    if  ;; label = @1
      local.get 4
      local.get 2
      call 22
    end
    i32.const 0
    local.set 0
    i32.const 0
    local.set 1
    loop  ;; label = @1
      local.get 4
      i32.const 1024
      i32.add
      local.get 1
      i32.const 7
      i32.shl
      local.tee 3
      i32.const 96
      i32.or
      i32.add
      local.tee 5
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.add
      local.tee 9
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 32
      i32.or
      i32.add
      local.tee 6
      i64.load
      local.tee 26
      call 4
      local.tee 27
      i64.xor
      i32.const 32
      call 3
      local.set 20
      local.get 5
      local.get 20
      local.get 27
      local.get 26
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 64
      i32.or
      i32.add
      local.tee 7
      i64.load
      local.get 20
      call 4
      local.tee 27
      i64.xor
      i32.const 24
      call 3
      local.tee 25
      call 4
      local.tee 23
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store
      local.get 7
      local.get 27
      local.get 20
      call 4
      local.tee 26
      i64.store
      local.get 6
      local.get 25
      local.get 26
      i64.xor
      i32.const 63
      call 3
      local.tee 27
      i64.store
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 72
      i32.or
      i32.add
      local.tee 8
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 104
      i32.or
      i32.add
      local.tee 10
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 8
      i32.or
      i32.add
      local.tee 11
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 40
      i32.or
      i32.add
      local.tee 12
      i64.load
      local.tee 21
      call 4
      local.tee 22
      i64.xor
      i32.const 32
      call 3
      local.tee 24
      call 4
      local.set 25
      local.get 8
      local.get 25
      local.get 24
      local.get 22
      local.get 21
      local.get 25
      i64.xor
      i32.const 24
      call 3
      local.tee 21
      call 4
      local.tee 34
      i64.xor
      i32.const 16
      call 3
      local.tee 35
      call 4
      local.tee 25
      i64.store
      local.get 21
      local.get 25
      i64.xor
      i32.const 63
      call 3
      local.set 21
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 80
      i32.or
      i32.add
      local.tee 13
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 112
      i32.or
      i32.add
      local.tee 14
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 16
      i32.or
      i32.add
      local.tee 15
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 48
      i32.or
      i32.add
      local.tee 16
      i64.load
      local.tee 22
      call 4
      local.tee 24
      i64.xor
      i32.const 32
      call 3
      local.tee 29
      call 4
      local.tee 30
      local.get 22
      i64.xor
      i32.const 24
      call 3
      local.set 22
      local.get 22
      local.get 30
      local.get 29
      local.get 24
      local.get 22
      call 4
      local.tee 30
      i64.xor
      i32.const 16
      call 3
      local.tee 29
      call 4
      local.tee 31
      i64.xor
      i32.const 63
      call 3
      local.set 22
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 88
      i32.or
      i32.add
      local.tee 17
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 120
      i32.or
      i32.add
      local.tee 18
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 24
      i32.or
      i32.add
      local.tee 19
      i64.load
      local.get 4
      i32.const 1024
      i32.add
      local.get 3
      i32.const 56
      i32.or
      i32.add
      local.tee 3
      i64.load
      local.tee 24
      call 4
      local.tee 32
      i64.xor
      i32.const 32
      call 3
      local.tee 28
      call 4
      local.tee 33
      local.get 24
      i64.xor
      i32.const 24
      call 3
      local.set 24
      local.get 24
      local.get 33
      local.get 28
      local.get 32
      local.get 24
      call 4
      local.tee 32
      i64.xor
      i32.const 16
      call 3
      local.tee 28
      call 4
      local.tee 33
      i64.xor
      i32.const 63
      call 3
      local.set 24
      local.get 9
      local.get 23
      local.get 21
      call 4
      local.tee 23
      local.get 21
      local.get 31
      local.get 23
      local.get 28
      i64.xor
      i32.const 32
      call 3
      local.tee 23
      call 4
      local.tee 31
      i64.xor
      i32.const 24
      call 3
      local.tee 21
      call 4
      local.tee 28
      i64.store
      local.get 18
      local.get 23
      local.get 28
      i64.xor
      i32.const 16
      call 3
      local.tee 23
      i64.store
      local.get 13
      local.get 31
      local.get 23
      call 4
      local.tee 23
      i64.store
      local.get 12
      local.get 21
      local.get 23
      i64.xor
      i32.const 63
      call 3
      i64.store
      local.get 11
      local.get 34
      local.get 22
      call 4
      local.tee 21
      local.get 22
      local.get 33
      local.get 20
      local.get 21
      i64.xor
      i32.const 32
      call 3
      local.tee 20
      call 4
      local.tee 21
      i64.xor
      i32.const 24
      call 3
      local.tee 22
      call 4
      local.tee 23
      i64.store
      local.get 5
      local.get 20
      local.get 23
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store
      local.get 17
      local.get 21
      local.get 20
      call 4
      local.tee 20
      i64.store
      local.get 16
      local.get 20
      local.get 22
      i64.xor
      i32.const 63
      call 3
      i64.store
      local.get 15
      local.get 30
      local.get 24
      call 4
      local.tee 20
      local.get 24
      local.get 26
      local.get 20
      local.get 35
      i64.xor
      i32.const 32
      call 3
      local.tee 20
      call 4
      local.tee 26
      i64.xor
      i32.const 24
      call 3
      local.tee 21
      call 4
      local.tee 22
      i64.store
      local.get 10
      local.get 20
      local.get 22
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store
      local.get 7
      local.get 26
      local.get 20
      call 4
      local.tee 20
      i64.store
      local.get 3
      local.get 20
      local.get 21
      i64.xor
      i32.const 63
      call 3
      i64.store
      local.get 19
      local.get 32
      local.get 27
      call 4
      local.tee 20
      local.get 27
      local.get 25
      local.get 20
      local.get 29
      i64.xor
      i32.const 32
      call 3
      local.tee 20
      call 4
      local.tee 26
      i64.xor
      i32.const 24
      call 3
      local.tee 27
      call 4
      local.tee 25
      i64.store
      local.get 14
      local.get 20
      local.get 25
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store
      local.get 8
      local.get 26
      local.get 20
      call 4
      local.tee 20
      i64.store
      local.get 6
      local.get 20
      local.get 27
      i64.xor
      i32.const 63
      call 3
      i64.store
      local.get 1
      i32.const 1
      i32.add
      local.tee 1
      i32.const 8
      i32.ne
      br_if 0 (;@1;)
    end
    loop  ;; label = @1
      local.get 0
      i32.const 4
      i32.shl
      local.tee 5
      local.get 4
      i32.const 1024
      i32.add
      i32.add
      local.tee 1
      local.tee 3
      i32.const 768
      i32.add
      i64.load
      local.get 1
      i64.load
      local.get 1
      i64.load offset=256
      local.tee 26
      call 4
      local.tee 27
      i64.xor
      i32.const 32
      call 3
      local.set 20
      local.get 3
      local.get 20
      local.get 27
      local.get 26
      local.get 1
      i64.load offset=512
      local.get 20
      call 4
      local.tee 27
      i64.xor
      i32.const 24
      call 3
      local.tee 25
      call 4
      local.tee 23
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store offset=768
      local.get 1
      local.get 27
      local.get 20
      call 4
      local.tee 26
      i64.store offset=512
      local.get 1
      local.get 25
      local.get 26
      i64.xor
      i32.const 63
      call 3
      local.tee 27
      i64.store offset=256
      local.get 1
      local.get 1
      i64.load offset=520
      local.get 1
      i64.load offset=776
      local.get 4
      i32.const 1024
      i32.add
      local.get 5
      i32.const 8
      i32.or
      i32.add
      local.tee 5
      i64.load
      local.get 1
      i64.load offset=264
      local.tee 25
      call 4
      local.tee 21
      i64.xor
      i32.const 32
      call 3
      local.tee 22
      call 4
      local.tee 24
      local.get 22
      local.get 21
      local.get 24
      local.get 25
      i64.xor
      i32.const 24
      call 3
      local.tee 21
      call 4
      local.tee 34
      i64.xor
      i32.const 16
      call 3
      local.tee 35
      call 4
      local.tee 25
      i64.store offset=520
      local.get 21
      local.get 25
      i64.xor
      i32.const 63
      call 3
      local.set 21
      local.get 1
      i64.load offset=640
      local.get 1
      i64.load offset=896
      local.get 1
      i64.load offset=128
      local.get 1
      i64.load offset=384
      local.tee 22
      call 4
      local.tee 24
      i64.xor
      i32.const 32
      call 3
      local.tee 29
      call 4
      local.tee 30
      local.get 22
      i64.xor
      i32.const 24
      call 3
      local.set 22
      local.get 22
      local.get 30
      local.get 29
      local.get 24
      local.get 22
      call 4
      local.tee 30
      i64.xor
      i32.const 16
      call 3
      local.tee 29
      call 4
      local.tee 31
      i64.xor
      i32.const 63
      call 3
      local.set 22
      local.get 1
      i64.load offset=648
      local.get 1
      i64.load offset=904
      local.get 1
      i64.load offset=136
      local.get 1
      i64.load offset=392
      local.tee 24
      call 4
      local.tee 32
      i64.xor
      i32.const 32
      call 3
      local.tee 28
      call 4
      local.tee 33
      local.get 24
      i64.xor
      i32.const 24
      call 3
      local.set 24
      local.get 24
      local.get 33
      local.get 28
      local.get 32
      local.get 24
      call 4
      local.tee 32
      i64.xor
      i32.const 16
      call 3
      local.tee 28
      call 4
      local.tee 33
      i64.xor
      i32.const 63
      call 3
      local.set 24
      local.get 1
      local.get 23
      local.get 21
      call 4
      local.tee 23
      local.get 21
      local.get 31
      local.get 23
      local.get 28
      i64.xor
      i32.const 32
      call 3
      local.tee 23
      call 4
      local.tee 31
      i64.xor
      i32.const 24
      call 3
      local.tee 21
      call 4
      local.tee 28
      i64.store
      local.get 1
      local.get 23
      local.get 28
      i64.xor
      i32.const 16
      call 3
      local.tee 23
      i64.store offset=904
      local.get 1
      local.get 31
      local.get 23
      call 4
      local.tee 23
      i64.store offset=640
      local.get 1
      local.get 21
      local.get 23
      i64.xor
      i32.const 63
      call 3
      i64.store offset=264
      local.get 5
      local.get 34
      local.get 22
      call 4
      local.tee 21
      local.get 22
      local.get 33
      local.get 20
      local.get 21
      i64.xor
      i32.const 32
      call 3
      local.tee 20
      call 4
      local.tee 21
      i64.xor
      i32.const 24
      call 3
      local.tee 22
      call 4
      local.tee 23
      i64.store
      local.get 3
      local.get 20
      local.get 23
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store offset=768
      local.get 1
      local.get 21
      local.get 20
      call 4
      local.tee 20
      i64.store offset=648
      local.get 1
      local.get 20
      local.get 22
      i64.xor
      i32.const 63
      call 3
      i64.store offset=384
      local.get 1
      local.get 30
      local.get 24
      call 4
      local.tee 20
      local.get 24
      local.get 26
      local.get 20
      local.get 35
      i64.xor
      i32.const 32
      call 3
      local.tee 20
      call 4
      local.tee 26
      i64.xor
      i32.const 24
      call 3
      local.tee 21
      call 4
      local.tee 22
      i64.store offset=128
      local.get 1
      local.get 20
      local.get 22
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store offset=776
      local.get 1
      local.get 26
      local.get 20
      call 4
      local.tee 20
      i64.store offset=512
      local.get 1
      local.get 20
      local.get 21
      i64.xor
      i32.const 63
      call 3
      i64.store offset=392
      local.get 1
      local.get 32
      local.get 27
      call 4
      local.tee 20
      local.get 27
      local.get 25
      local.get 20
      local.get 29
      i64.xor
      i32.const 32
      call 3
      local.tee 20
      call 4
      local.tee 26
      i64.xor
      i32.const 24
      call 3
      local.tee 27
      call 4
      local.tee 25
      i64.store offset=136
      local.get 1
      local.get 20
      local.get 25
      i64.xor
      i32.const 16
      call 3
      local.tee 20
      i64.store offset=896
      local.get 1
      local.get 26
      local.get 20
      call 4
      local.tee 20
      i64.store offset=520
      local.get 1
      local.get 20
      local.get 27
      i64.xor
      i32.const 63
      call 3
      i64.store offset=256
      local.get 0
      i32.const 1
      i32.add
      local.tee 0
      i32.const 8
      i32.ne
      br_if 0 (;@1;)
    end
    local.get 2
    local.get 4
    call 23
    local.get 2
    local.get 4
    i32.const 1024
    i32.add
    call 22
    local.get 4
    i32.const 2048
    i32.add
    global.set 0)
  (func (;19;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const -64
    i32.add
    local.tee 3
    global.set 0
    local.get 3
    i32.const 0
    i32.const 64
    call 9
    local.set 4
    i32.const -1
    local.set 3
    block  ;; label = @1
      local.get 0
      i32.eqz
      local.get 1
      i32.eqz
      i32.or
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=228
      local.get 2
      i32.gt_u
      br_if 0 (;@1;)
      local.get 0
      i64.load offset=80
      i64.const 0
      i64.ne
      br_if 0 (;@1;)
      local.get 0
      local.get 0
      i64.load32_u offset=224
      call 27
      local.get 0
      call 41
      i32.const 0
      local.set 3
      local.get 0
      i32.const 96
      i32.add
      local.tee 2
      local.get 0
      i32.load offset=224
      local.tee 5
      i32.add
      i32.const 0
      i32.const 128
      local.get 5
      i32.sub
      call 9
      drop
      local.get 0
      local.get 2
      call 25
      loop  ;; label = @2
        local.get 4
        local.get 3
        i32.const 3
        i32.shl
        local.tee 5
        i32.add
        local.get 0
        local.get 5
        i32.add
        i64.load
        call 47
        local.get 3
        i32.const 1
        i32.add
        local.tee 3
        i32.const 8
        i32.ne
        br_if 0 (;@2;)
      end
      local.get 1
      local.get 4
      local.get 0
      i32.load offset=228
      call 6
      drop
      local.get 4
      i32.const 64
      call 7
      local.get 2
      i32.const 128
      call 7
      local.get 0
      i32.const 64
      call 7
      i32.const 0
      local.set 3
    end
    local.get 4
    i32.const -64
    i32.sub
    global.set 0
    local.get 3)
  (func (;20;) (type 1) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 0
    local.get 1
    call 58
    local.get 2
    i32.const 16
    i32.add
    global.set 0)
  (func (;21;) (type 3) (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load8_u
      local.tee 5
      i32.const -48
      i32.add
      i32.const 255
      i32.and
      i32.const 9
      i32.gt_u
      br_if 0 (;@1;)
      local.get 5
      local.set 2
      loop  ;; label = @2
        local.get 3
        local.set 6
        local.get 4
        i32.const 429496729
        i32.gt_u
        br_if 1 (;@1;)
        local.get 2
        i32.const 255
        i32.and
        i32.const -48
        i32.add
        local.tee 2
        local.get 4
        i32.const 10
        i32.mul
        local.tee 3
        i32.const -1
        i32.xor
        i32.gt_u
        br_if 1 (;@1;)
        local.get 2
        local.get 3
        i32.add
        local.set 4
        local.get 0
        local.get 6
        i32.const 1
        i32.add
        local.tee 3
        i32.add
        local.tee 7
        i32.load8_u
        local.tee 2
        i32.const -48
        i32.add
        i32.const 255
        i32.and
        i32.const 10
        i32.lt_u
        br_if 0 (;@2;)
      end
      local.get 6
      i32.const 0
      local.get 5
      i32.const 48
      i32.eq
      select
      br_if 0 (;@1;)
      local.get 1
      local.get 4
      i32.store
      local.get 7
      local.set 8
    end
    local.get 8)
  (func (;22;) (type 1) (param i32 i32)
    (local i32 i32 i32)
    loop  ;; label = @1
      local.get 0
      local.get 2
      i32.const 3
      i32.shl
      local.tee 3
      i32.add
      local.tee 4
      local.get 4
      i64.load
      local.get 1
      local.get 3
      i32.add
      i64.load
      i64.xor
      i64.store
      local.get 2
      i32.const 1
      i32.add
      local.tee 2
      i32.const 128
      i32.ne
      br_if 0 (;@1;)
    end)
  (func (;23;) (type 1) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 1024
    call 6
    drop)
  (func (;24;) (type 3) (param i32 i32) (result i32)
    (local i32 i32)
    global.get 0
    i32.const -64
    i32.add
    local.tee 2
    global.set 0
    i32.const -1
    local.set 3
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const -1
      i32.add
      i32.const 64
      i32.ge_u
      if  ;; label = @2
        local.get 0
        i32.const 240
        call 7
        local.get 0
        call 41
        br 1 (;@1;)
      end
      local.get 2
      i32.const 1
      i32.store8 offset=3
      local.get 2
      i32.const 256
      i32.store16 offset=1 align=1
      local.get 2
      local.get 1
      i32.store8
      local.get 2
      i32.const 4
      i32.or
      i32.const 0
      i32.const 60
      call 9
      drop
      local.get 0
      local.get 2
      call 77
      local.set 3
    end
    local.get 2
    i32.const -64
    i32.sub
    global.set 0
    local.get 3)
  (func (;25;) (type 1) (param i32 i32)
    (local i32 i32 i32 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64)
    global.get 0
    i32.const 256
    i32.sub
    local.tee 3
    global.set 0
    loop  ;; label = @1
      local.get 2
      i32.const 3
      i32.shl
      local.tee 4
      local.get 3
      i32.const 128
      i32.add
      i32.add
      local.get 1
      local.get 4
      i32.add
      i64.load align=1
      i64.store
      local.get 2
      i32.const 1
      i32.add
      local.tee 2
      i32.const 16
      i32.ne
      br_if 0 (;@1;)
    end
    i64.const -6534734903238641935
    local.set 12
    local.get 3
    local.get 0
    i32.const 64
    call 6
    local.tee 1
    i64.const -6534734903238641935
    i64.store offset=88
    i64.const 4354685564936845355
    local.set 7
    local.get 1
    i64.const 4354685564936845355
    i64.store offset=80
    i64.const -4942790177534073029
    local.set 13
    local.get 1
    i64.const -4942790177534073029
    i64.store offset=72
    i64.const 7640891576956012808
    local.set 5
    local.get 1
    i64.const 7640891576956012808
    i64.store offset=64
    local.get 1
    local.get 0
    i64.load offset=64
    i64.const 5840696475078001361
    i64.xor
    local.tee 14
    i64.store offset=96
    local.get 1
    local.get 0
    i64.load offset=72
    i64.const -7276294671716946913
    i64.xor
    local.tee 15
    i64.store offset=104
    local.get 1
    local.get 0
    i64.load offset=80
    i64.const 2270897969802886507
    i64.xor
    local.tee 16
    i64.store offset=112
    local.get 1
    local.get 0
    i64.load offset=88
    i64.const 6620516959819538809
    i64.xor
    local.tee 19
    i64.store offset=120
    i32.const 0
    local.set 3
    local.get 1
    i64.load offset=56
    local.set 6
    local.get 1
    i64.load offset=24
    local.set 18
    local.get 1
    i64.load offset=48
    local.set 8
    local.get 1
    i64.load offset=16
    local.set 20
    local.get 1
    i64.load offset=40
    local.set 9
    local.get 1
    i64.load offset=8
    local.set 17
    local.get 1
    i64.load offset=32
    local.set 10
    local.get 1
    i64.load
    local.set 11
    loop  ;; label = @1
      local.get 10
      local.get 5
      local.get 14
      local.get 1
      i32.const 128
      i32.add
      local.get 3
      i32.const 6
      i32.shl
      local.tee 2
      i32.const 1088
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 10
      local.get 11
      i64.add
      i64.add
      local.tee 11
      i64.xor
      i32.const 32
      call 3
      local.tee 14
      i64.add
      local.tee 21
      i64.xor
      i32.const 24
      call 3
      local.set 5
      local.get 5
      local.get 21
      local.get 14
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1092
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 5
      local.get 11
      i64.add
      i64.add
      local.tee 11
      i64.xor
      i32.const 16
      call 3
      local.tee 14
      i64.add
      local.tee 21
      i64.xor
      i32.const 63
      call 3
      local.set 10
      local.get 9
      local.get 13
      local.get 15
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1096
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 9
      local.get 17
      i64.add
      i64.add
      local.tee 17
      i64.xor
      i32.const 32
      call 3
      local.tee 15
      i64.add
      local.tee 13
      i64.xor
      i32.const 24
      call 3
      local.set 5
      local.get 5
      local.get 13
      local.get 15
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1100
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 5
      local.get 17
      i64.add
      i64.add
      local.tee 17
      i64.xor
      i32.const 16
      call 3
      local.tee 15
      i64.add
      local.tee 13
      i64.xor
      i32.const 63
      call 3
      local.set 5
      local.get 7
      local.get 16
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1104
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 8
      local.get 20
      i64.add
      i64.add
      local.tee 9
      i64.xor
      i32.const 32
      call 3
      local.tee 16
      i64.add
      local.tee 7
      local.get 8
      i64.xor
      i32.const 24
      call 3
      local.set 8
      local.get 8
      local.get 16
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1108
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 8
      local.get 9
      i64.add
      i64.add
      local.tee 20
      i64.xor
      i32.const 16
      call 3
      local.tee 16
      local.get 7
      i64.add
      local.tee 9
      i64.xor
      i32.const 63
      call 3
      local.set 8
      local.get 12
      local.get 19
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1112
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 6
      local.get 18
      i64.add
      i64.add
      local.tee 7
      i64.xor
      i32.const 32
      call 3
      local.tee 19
      i64.add
      local.tee 12
      local.get 6
      i64.xor
      i32.const 24
      call 3
      local.set 6
      local.get 6
      local.get 12
      local.get 19
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1116
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 6
      local.get 7
      i64.add
      i64.add
      local.tee 18
      i64.xor
      i32.const 16
      call 3
      local.tee 7
      i64.add
      local.tee 12
      i64.xor
      i32.const 63
      call 3
      local.set 6
      local.get 5
      local.get 9
      local.get 7
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1120
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 5
      local.get 11
      i64.add
      i64.add
      local.tee 11
      i64.xor
      i32.const 32
      call 3
      local.tee 7
      i64.add
      local.tee 9
      i64.xor
      i32.const 24
      call 3
      local.set 5
      local.get 5
      local.get 9
      local.get 7
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1124
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 5
      local.get 11
      i64.add
      i64.add
      local.tee 11
      i64.xor
      i32.const 16
      call 3
      local.tee 19
      i64.add
      local.tee 7
      i64.xor
      i32.const 63
      call 3
      local.set 9
      local.get 8
      local.get 12
      local.get 14
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1128
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 8
      local.get 17
      i64.add
      i64.add
      local.tee 17
      i64.xor
      i32.const 32
      call 3
      local.tee 14
      i64.add
      local.tee 12
      i64.xor
      i32.const 24
      call 3
      local.set 5
      local.get 5
      local.get 12
      local.get 14
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1132
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 5
      local.get 17
      i64.add
      i64.add
      local.tee 17
      i64.xor
      i32.const 16
      call 3
      local.tee 14
      i64.add
      local.tee 12
      i64.xor
      i32.const 63
      call 3
      local.set 8
      local.get 6
      local.get 15
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1136
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 6
      local.get 20
      i64.add
      i64.add
      local.tee 5
      i64.xor
      i32.const 32
      call 3
      local.tee 15
      local.get 21
      i64.add
      local.tee 21
      i64.xor
      i32.const 24
      call 3
      local.set 6
      local.get 6
      local.get 21
      local.get 15
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1140
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 5
      local.get 6
      i64.add
      i64.add
      local.tee 20
      i64.xor
      i32.const 16
      call 3
      local.tee 15
      i64.add
      local.tee 5
      i64.xor
      i32.const 63
      call 3
      local.set 6
      local.get 10
      local.get 13
      local.get 16
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1144
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 10
      local.get 18
      i64.add
      i64.add
      local.tee 18
      i64.xor
      i32.const 32
      call 3
      local.tee 16
      i64.add
      local.tee 13
      i64.xor
      i32.const 24
      call 3
      local.set 10
      local.get 10
      local.get 13
      local.get 16
      local.get 1
      i32.const 128
      i32.add
      local.get 2
      i32.const 1148
      i32.add
      i32.load
      i32.const 3
      i32.shl
      i32.add
      i64.load
      local.get 10
      local.get 18
      i64.add
      i64.add
      local.tee 18
      i64.xor
      i32.const 16
      call 3
      local.tee 16
      i64.add
      local.tee 13
      i64.xor
      i32.const 63
      call 3
      local.set 10
      local.get 3
      i32.const 1
      i32.add
      local.tee 3
      i32.const 12
      i32.ne
      br_if 0 (;@1;)
    end
    local.get 1
    local.get 14
    i64.store offset=96
    local.get 1
    local.get 10
    i64.store offset=32
    local.get 1
    local.get 15
    i64.store offset=104
    local.get 1
    local.get 9
    i64.store offset=40
    local.get 1
    local.get 17
    i64.store offset=8
    local.get 1
    local.get 13
    i64.store offset=72
    local.get 1
    local.get 16
    i64.store offset=112
    local.get 1
    local.get 8
    i64.store offset=48
    local.get 1
    local.get 20
    i64.store offset=16
    local.get 1
    local.get 7
    i64.store offset=80
    local.get 1
    local.get 19
    i64.store offset=120
    local.get 1
    local.get 6
    i64.store offset=56
    local.get 1
    local.get 18
    i64.store offset=24
    local.get 1
    local.get 12
    i64.store offset=88
    local.get 1
    local.get 5
    i64.store offset=64
    local.get 1
    local.get 11
    i64.store
    local.get 0
    local.get 11
    local.get 0
    i64.load
    i64.xor
    local.get 5
    i64.xor
    i64.store
    i32.const 1
    local.set 2
    loop  ;; label = @1
      local.get 0
      local.get 2
      i32.const 3
      i32.shl
      local.tee 3
      i32.add
      local.tee 4
      local.get 1
      local.get 3
      i32.add
      local.tee 3
      i64.load
      local.get 4
      i64.load
      i64.xor
      local.get 3
      i32.const -64
      i32.sub
      i64.load
      i64.xor
      i64.store
      local.get 2
      i32.const 1
      i32.add
      local.tee 2
      i32.const 8
      i32.ne
      br_if 0 (;@1;)
    end
    local.get 1
    i32.const 256
    i32.add
    global.set 0)
  (func (;26;) (type 5) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 80
    i32.sub
    local.tee 5
    global.set 0
    local.get 5
    i32.const 3052
    i32.store offset=76
    local.get 5
    i32.const 55
    i32.add
    local.set 19
    local.get 5
    i32.const 56
    i32.add
    local.set 16
    block  ;; label = @1
      block  ;; label = @2
        loop  ;; label = @3
          block  ;; label = @4
            local.get 14
            i32.const 0
            i32.lt_s
            br_if 0 (;@4;)
            local.get 4
            i32.const 2147483647
            local.get 14
            i32.sub
            i32.gt_s
            if  ;; label = @5
              i32.const 3948
              i32.const 61
              i32.store
              i32.const -1
              local.set 14
              br 1 (;@4;)
            end
            local.get 4
            local.get 14
            i32.add
            local.set 14
          end
          local.get 5
          i32.load offset=76
          local.tee 10
          local.set 4
          block  ;; label = @4
            block  ;; label = @5
              local.get 10
              i32.load8_u
              local.tee 6
              if  ;; label = @6
                loop  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 6
                      i32.const 255
                      i32.and
                      local.tee 7
                      i32.eqz
                      if  ;; label = @10
                        local.get 4
                        local.set 6
                        br 1 (;@9;)
                      end
                      local.get 7
                      i32.const 37
                      i32.ne
                      br_if 1 (;@8;)
                      local.get 4
                      local.set 6
                      loop  ;; label = @10
                        local.get 4
                        i32.load8_u offset=1
                        i32.const 37
                        i32.ne
                        br_if 1 (;@9;)
                        local.get 5
                        local.get 4
                        i32.const 2
                        i32.add
                        local.tee 7
                        i32.store offset=76
                        local.get 6
                        i32.const 1
                        i32.add
                        local.set 6
                        local.get 4
                        i32.load8_u offset=2
                        local.get 7
                        local.set 4
                        i32.const 37
                        i32.eq
                        br_if 0 (;@10;)
                      end
                    end
                    local.get 6
                    local.get 10
                    i32.sub
                    local.set 4
                    local.get 0
                    if  ;; label = @9
                      local.get 0
                      local.get 10
                      local.get 4
                      call 15
                    end
                    local.get 4
                    br_if 5 (;@3;)
                    i32.const -1
                    local.set 15
                    i32.const 1
                    local.set 6
                    local.get 5
                    i32.load offset=76
                    i32.load8_s offset=1
                    call 16
                    local.set 4
                    local.get 5
                    i32.load offset=76
                    local.set 7
                    block  ;; label = @9
                      local.get 4
                      i32.eqz
                      br_if 0 (;@9;)
                      local.get 7
                      i32.load8_u offset=2
                      i32.const 36
                      i32.ne
                      br_if 0 (;@9;)
                      local.get 7
                      i32.load8_s offset=1
                      i32.const -48
                      i32.add
                      local.set 15
                      i32.const 1
                      local.set 18
                      i32.const 3
                      local.set 6
                    end
                    local.get 5
                    local.get 6
                    local.get 7
                    i32.add
                    local.tee 4
                    i32.store offset=76
                    i32.const 0
                    local.set 6
                    block  ;; label = @9
                      local.get 4
                      i32.load8_s
                      local.tee 17
                      i32.const -32
                      i32.add
                      local.tee 9
                      i32.const 31
                      i32.gt_u
                      if  ;; label = @10
                        local.get 4
                        local.set 7
                        br 1 (;@9;)
                      end
                      local.get 4
                      local.set 7
                      i32.const 1
                      local.get 9
                      i32.shl
                      local.tee 12
                      i32.const 75913
                      i32.and
                      i32.eqz
                      br_if 0 (;@9;)
                      loop  ;; label = @10
                        local.get 5
                        local.get 4
                        i32.const 1
                        i32.add
                        local.tee 7
                        i32.store offset=76
                        local.get 6
                        local.get 12
                        i32.or
                        local.set 6
                        local.get 4
                        i32.load8_s offset=1
                        local.tee 17
                        i32.const -32
                        i32.add
                        local.tee 9
                        i32.const 31
                        i32.gt_u
                        br_if 1 (;@9;)
                        local.get 7
                        local.set 4
                        i32.const 1
                        local.get 9
                        i32.shl
                        local.tee 12
                        i32.const 75913
                        i32.and
                        br_if 0 (;@10;)
                      end
                    end
                    block  ;; label = @9
                      local.get 17
                      i32.const 42
                      i32.eq
                      if  ;; label = @10
                        local.get 5
                        block (result i32)  ;; label = @11
                          block  ;; label = @12
                            local.get 7
                            i32.load8_s offset=1
                            call 16
                            i32.eqz
                            br_if 0 (;@12;)
                            local.get 5
                            i32.load offset=76
                            local.tee 4
                            i32.load8_u offset=2
                            i32.const 36
                            i32.ne
                            br_if 0 (;@12;)
                            local.get 4
                            i32.load8_s offset=1
                            i32.const 2
                            i32.shl
                            local.get 3
                            i32.add
                            i32.const -192
                            i32.add
                            i32.const 10
                            i32.store
                            local.get 4
                            i32.load8_s offset=1
                            i32.const 3
                            i32.shl
                            local.get 2
                            i32.add
                            i32.const -384
                            i32.add
                            i32.load
                            local.set 13
                            i32.const 1
                            local.set 18
                            local.get 4
                            i32.const 3
                            i32.add
                            br 1 (;@11;)
                          end
                          local.get 18
                          br_if 9 (;@2;)
                          i32.const 0
                          local.set 18
                          i32.const 0
                          local.set 13
                          local.get 0
                          if  ;; label = @12
                            local.get 1
                            local.get 1
                            i32.load
                            local.tee 4
                            i32.const 4
                            i32.add
                            i32.store
                            local.get 4
                            i32.load
                            local.set 13
                          end
                          local.get 5
                          i32.load offset=76
                          i32.const 1
                          i32.add
                        end
                        local.tee 4
                        i32.store offset=76
                        local.get 13
                        i32.const -1
                        i32.gt_s
                        br_if 1 (;@9;)
                        i32.const 0
                        local.get 13
                        i32.sub
                        local.set 13
                        local.get 6
                        i32.const 8192
                        i32.or
                        local.set 6
                        br 1 (;@9;)
                      end
                      local.get 5
                      i32.const 76
                      i32.add
                      call 32
                      local.tee 13
                      i32.const 0
                      i32.lt_s
                      br_if 7 (;@2;)
                      local.get 5
                      i32.load offset=76
                      local.set 4
                    end
                    i32.const -1
                    local.set 8
                    block  ;; label = @9
                      local.get 4
                      i32.load8_u
                      i32.const 46
                      i32.ne
                      br_if 0 (;@9;)
                      local.get 4
                      i32.load8_u offset=1
                      i32.const 42
                      i32.eq
                      if  ;; label = @10
                        block  ;; label = @11
                          local.get 4
                          i32.load8_s offset=2
                          call 16
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 5
                          i32.load offset=76
                          local.tee 4
                          i32.load8_u offset=3
                          i32.const 36
                          i32.ne
                          br_if 0 (;@11;)
                          local.get 4
                          i32.load8_s offset=2
                          i32.const 2
                          i32.shl
                          local.get 3
                          i32.add
                          i32.const -192
                          i32.add
                          i32.const 10
                          i32.store
                          local.get 4
                          i32.load8_s offset=2
                          i32.const 3
                          i32.shl
                          local.get 2
                          i32.add
                          i32.const -384
                          i32.add
                          i32.load
                          local.set 8
                          local.get 5
                          local.get 4
                          i32.const 4
                          i32.add
                          local.tee 4
                          i32.store offset=76
                          br 2 (;@9;)
                        end
                        local.get 18
                        br_if 8 (;@2;)
                        local.get 0
                        if (result i32)  ;; label = @11
                          local.get 1
                          local.get 1
                          i32.load
                          local.tee 4
                          i32.const 4
                          i32.add
                          i32.store
                          local.get 4
                          i32.load
                        else
                          i32.const 0
                        end
                        local.set 8
                        local.get 5
                        local.get 5
                        i32.load offset=76
                        i32.const 2
                        i32.add
                        local.tee 4
                        i32.store offset=76
                        br 1 (;@9;)
                      end
                      local.get 5
                      local.get 4
                      i32.const 1
                      i32.add
                      i32.store offset=76
                      local.get 5
                      i32.const 76
                      i32.add
                      call 32
                      local.set 8
                      local.get 5
                      i32.load offset=76
                      local.set 4
                    end
                    i32.const 0
                    local.set 7
                    loop  ;; label = @9
                      local.get 7
                      local.set 12
                      i32.const -1
                      local.set 11
                      local.get 4
                      i32.load8_s
                      i32.const -65
                      i32.add
                      i32.const 57
                      i32.gt_u
                      br_if 8 (;@1;)
                      local.get 5
                      local.get 4
                      i32.const 1
                      i32.add
                      local.tee 17
                      i32.store offset=76
                      local.get 4
                      i32.load8_s
                      local.get 17
                      local.set 4
                      local.get 12
                      i32.const 58
                      i32.mul
                      i32.add
                      i32.const 3167
                      i32.add
                      i32.load8_u
                      local.tee 7
                      i32.const -1
                      i32.add
                      i32.const 8
                      i32.lt_u
                      br_if 0 (;@9;)
                    end
                    local.get 7
                    i32.eqz
                    br_if 7 (;@1;)
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 7
                          i32.const 19
                          i32.eq
                          if  ;; label = @12
                            local.get 15
                            i32.const -1
                            i32.le_s
                            br_if 1 (;@11;)
                            br 11 (;@1;)
                          end
                          local.get 15
                          i32.const 0
                          i32.lt_s
                          br_if 1 (;@10;)
                          local.get 3
                          local.get 15
                          i32.const 2
                          i32.shl
                          i32.add
                          local.get 7
                          i32.store
                          local.get 5
                          local.get 2
                          local.get 15
                          i32.const 3
                          i32.shl
                          i32.add
                          i64.load
                          i64.store offset=64
                        end
                        i32.const 0
                        local.set 4
                        local.get 0
                        i32.eqz
                        br_if 7 (;@3;)
                        br 1 (;@9;)
                      end
                      local.get 0
                      i32.eqz
                      br_if 5 (;@4;)
                      local.get 5
                      i32.const -64
                      i32.sub
                      local.get 7
                      local.get 1
                      call 31
                      local.get 5
                      i32.load offset=76
                      local.set 17
                    end
                    local.get 6
                    i32.const -65537
                    i32.and
                    local.tee 9
                    local.get 6
                    local.get 6
                    i32.const 8192
                    i32.and
                    select
                    local.set 6
                    i32.const 0
                    local.set 11
                    i32.const 3200
                    local.set 15
                    local.get 16
                    local.set 7
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          block (result i32)  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  block  ;; label = @16
                                    block (result i32)  ;; label = @17
                                      block  ;; label = @18
                                        block  ;; label = @19
                                          block  ;; label = @20
                                            block  ;; label = @21
                                              block  ;; label = @22
                                                block  ;; label = @23
                                                  block  ;; label = @24
                                                    local.get 17
                                                    i32.const -1
                                                    i32.add
                                                    i32.load8_s
                                                    local.tee 4
                                                    i32.const -33
                                                    i32.and
                                                    local.get 4
                                                    local.get 4
                                                    i32.const 15
                                                    i32.and
                                                    i32.const 3
                                                    i32.eq
                                                    select
                                                    local.get 4
                                                    local.get 12
                                                    select
                                                    local.tee 4
                                                    i32.const -88
                                                    i32.add
                                                    br_table 4 (;@20;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 14 (;@10;) 19 (;@5;) 15 (;@9;) 6 (;@18;) 14 (;@10;) 14 (;@10;) 14 (;@10;) 19 (;@5;) 6 (;@18;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 19 (;@5;) 2 (;@22;) 5 (;@19;) 3 (;@21;) 19 (;@5;) 19 (;@5;) 9 (;@15;) 19 (;@5;) 1 (;@23;) 19 (;@5;) 19 (;@5;) 4 (;@20;) 0 (;@24;)
                                                  end
                                                  block  ;; label = @24
                                                    local.get 4
                                                    i32.const -65
                                                    i32.add
                                                    br_table 14 (;@10;) 19 (;@5;) 11 (;@13;) 19 (;@5;) 14 (;@10;) 14 (;@10;) 14 (;@10;) 0 (;@24;)
                                                  end
                                                  local.get 4
                                                  i32.const 83
                                                  i32.eq
                                                  br_if 9 (;@14;)
                                                  br 18 (;@5;)
                                                end
                                                local.get 5
                                                i64.load offset=64
                                                local.set 20
                                                i32.const 3200
                                                br 5 (;@17;)
                                              end
                                              i32.const 0
                                              local.set 4
                                              block  ;; label = @22
                                                block  ;; label = @23
                                                  block  ;; label = @24
                                                    block  ;; label = @25
                                                      block  ;; label = @26
                                                        block  ;; label = @27
                                                          block  ;; label = @28
                                                            local.get 12
                                                            i32.const 255
                                                            i32.and
                                                            br_table 0 (;@28;) 1 (;@27;) 2 (;@26;) 3 (;@25;) 4 (;@24;) 25 (;@3;) 5 (;@23;) 6 (;@22;) 25 (;@3;)
                                                          end
                                                          local.get 5
                                                          i32.load offset=64
                                                          local.get 14
                                                          i32.store
                                                          br 24 (;@3;)
                                                        end
                                                        local.get 5
                                                        i32.load offset=64
                                                        local.get 14
                                                        i32.store
                                                        br 23 (;@3;)
                                                      end
                                                      local.get 5
                                                      i32.load offset=64
                                                      local.get 14
                                                      i64.extend_i32_s
                                                      i64.store
                                                      br 22 (;@3;)
                                                    end
                                                    local.get 5
                                                    i32.load offset=64
                                                    local.get 14
                                                    i32.store16
                                                    br 21 (;@3;)
                                                  end
                                                  local.get 5
                                                  i32.load offset=64
                                                  local.get 14
                                                  i32.store8
                                                  br 20 (;@3;)
                                                end
                                                local.get 5
                                                i32.load offset=64
                                                local.get 14
                                                i32.store
                                                br 19 (;@3;)
                                              end
                                              local.get 5
                                              i32.load offset=64
                                              local.get 14
                                              i64.extend_i32_s
                                              i64.store
                                              br 18 (;@3;)
                                            end
                                            local.get 8
                                            i32.const 8
                                            local.get 8
                                            i32.const 8
                                            i32.gt_u
                                            select
                                            local.set 8
                                            local.get 6
                                            i32.const 8
                                            i32.or
                                            local.set 6
                                            i32.const 120
                                            local.set 4
                                          end
                                          local.get 5
                                          i64.load offset=64
                                          local.get 16
                                          local.get 4
                                          i32.const 32
                                          i32.and
                                          call 54
                                          local.set 10
                                          local.get 6
                                          i32.const 8
                                          i32.and
                                          i32.eqz
                                          br_if 3 (;@16;)
                                          local.get 5
                                          i64.load offset=64
                                          i64.eqz
                                          br_if 3 (;@16;)
                                          local.get 4
                                          i32.const 4
                                          i32.shr_u
                                          i32.const 3200
                                          i32.add
                                          local.set 15
                                          i32.const 2
                                          local.set 11
                                          br 3 (;@16;)
                                        end
                                        local.get 5
                                        i64.load offset=64
                                        local.get 16
                                        call 53
                                        local.set 10
                                        local.get 6
                                        i32.const 8
                                        i32.and
                                        i32.eqz
                                        br_if 2 (;@16;)
                                        local.get 8
                                        local.get 16
                                        local.get 10
                                        i32.sub
                                        local.tee 4
                                        i32.const 1
                                        i32.add
                                        local.get 8
                                        local.get 4
                                        i32.gt_s
                                        select
                                        local.set 8
                                        br 2 (;@16;)
                                      end
                                      local.get 5
                                      i64.load offset=64
                                      local.tee 20
                                      i64.const -1
                                      i64.le_s
                                      if  ;; label = @18
                                        local.get 5
                                        i64.const 0
                                        local.get 20
                                        i64.sub
                                        local.tee 20
                                        i64.store offset=64
                                        i32.const 1
                                        local.set 11
                                        i32.const 3200
                                        br 1 (;@17;)
                                      end
                                      local.get 6
                                      i32.const 2048
                                      i32.and
                                      if  ;; label = @18
                                        i32.const 1
                                        local.set 11
                                        i32.const 3201
                                        br 1 (;@17;)
                                      end
                                      i32.const 3202
                                      i32.const 3200
                                      local.get 6
                                      i32.const 1
                                      i32.and
                                      local.tee 11
                                      select
                                    end
                                    local.set 15
                                    local.get 20
                                    local.get 16
                                    call 52
                                    local.set 10
                                  end
                                  local.get 6
                                  i32.const -65537
                                  i32.and
                                  local.get 6
                                  local.get 8
                                  i32.const -1
                                  i32.gt_s
                                  select
                                  local.set 6
                                  local.get 8
                                  local.get 5
                                  i64.load offset=64
                                  local.tee 20
                                  i64.eqz
                                  i32.eqz
                                  i32.or
                                  i32.eqz
                                  if  ;; label = @16
                                    i32.const 0
                                    local.set 8
                                    local.get 16
                                    local.set 10
                                    br 11 (;@5;)
                                  end
                                  local.get 8
                                  local.get 20
                                  i64.eqz
                                  local.get 16
                                  local.get 10
                                  i32.sub
                                  i32.add
                                  local.tee 4
                                  local.get 8
                                  local.get 4
                                  i32.gt_s
                                  select
                                  local.set 8
                                  br 10 (;@5;)
                                end
                                local.get 5
                                i32.load offset=64
                                local.tee 4
                                i32.const 3210
                                local.get 4
                                select
                                local.tee 10
                                local.get 8
                                call 57
                                local.tee 4
                                local.get 8
                                local.get 10
                                i32.add
                                local.get 4
                                select
                                local.set 7
                                local.get 9
                                local.set 6
                                local.get 4
                                local.get 10
                                i32.sub
                                local.get 8
                                local.get 4
                                select
                                local.set 8
                                br 9 (;@5;)
                              end
                              local.get 8
                              if  ;; label = @14
                                local.get 5
                                i32.load offset=64
                                br 2 (;@12;)
                              end
                              i32.const 0
                              local.set 4
                              local.get 0
                              i32.const 32
                              local.get 13
                              i32.const 0
                              local.get 6
                              call 14
                              br 2 (;@11;)
                            end
                            local.get 5
                            i32.const 0
                            i32.store offset=12
                            local.get 5
                            local.get 5
                            i64.load offset=64
                            i64.store32 offset=8
                            local.get 5
                            local.get 5
                            i32.const 8
                            i32.add
                            i32.store offset=64
                            i32.const -1
                            local.set 8
                            local.get 5
                            i32.const 8
                            i32.add
                          end
                          local.set 7
                          i32.const 0
                          local.set 4
                          block  ;; label = @12
                            loop  ;; label = @13
                              local.get 7
                              i32.load
                              local.tee 9
                              i32.eqz
                              br_if 1 (;@12;)
                              local.get 5
                              i32.const 4
                              i32.add
                              local.get 9
                              call 33
                              local.tee 10
                              i32.const 0
                              i32.lt_s
                              local.tee 9
                              local.get 10
                              local.get 8
                              local.get 4
                              i32.sub
                              i32.gt_u
                              i32.or
                              i32.eqz
                              if  ;; label = @14
                                local.get 7
                                i32.const 4
                                i32.add
                                local.set 7
                                local.get 8
                                local.get 4
                                local.get 10
                                i32.add
                                local.tee 4
                                i32.gt_u
                                br_if 1 (;@13;)
                                br 2 (;@12;)
                              end
                            end
                            i32.const -1
                            local.set 11
                            local.get 9
                            br_if 11 (;@1;)
                          end
                          local.get 0
                          i32.const 32
                          local.get 13
                          local.get 4
                          local.get 6
                          call 14
                          local.get 4
                          i32.eqz
                          if  ;; label = @12
                            i32.const 0
                            local.set 4
                            br 1 (;@11;)
                          end
                          i32.const 0
                          local.set 12
                          local.get 5
                          i32.load offset=64
                          local.set 7
                          loop  ;; label = @12
                            local.get 7
                            i32.load
                            local.tee 9
                            i32.eqz
                            br_if 1 (;@11;)
                            local.get 5
                            i32.const 4
                            i32.add
                            local.get 9
                            call 33
                            local.tee 9
                            local.get 12
                            i32.add
                            local.tee 12
                            local.get 4
                            i32.gt_s
                            br_if 1 (;@11;)
                            local.get 0
                            local.get 5
                            i32.const 4
                            i32.add
                            local.get 9
                            call 15
                            local.get 7
                            i32.const 4
                            i32.add
                            local.set 7
                            local.get 12
                            local.get 4
                            i32.lt_u
                            br_if 0 (;@12;)
                          end
                        end
                        local.get 0
                        i32.const 32
                        local.get 13
                        local.get 4
                        local.get 6
                        i32.const 8192
                        i32.xor
                        call 14
                        local.get 13
                        local.get 4
                        local.get 13
                        local.get 4
                        i32.gt_s
                        select
                        local.set 4
                        br 7 (;@3;)
                      end
                      local.get 0
                      local.get 5
                      f64.load offset=64
                      local.get 13
                      local.get 8
                      local.get 6
                      local.get 4
                      i32.const 0
                      call_indirect (type 13)
                      local.set 4
                      br 6 (;@3;)
                    end
                    local.get 5
                    local.get 5
                    i64.load offset=64
                    i64.store8 offset=55
                    i32.const 1
                    local.set 8
                    local.get 19
                    local.set 10
                    local.get 9
                    local.set 6
                    br 3 (;@5;)
                  end
                  local.get 5
                  local.get 4
                  i32.const 1
                  i32.add
                  local.tee 7
                  i32.store offset=76
                  local.get 4
                  i32.load8_u offset=1
                  local.set 6
                  local.get 7
                  local.set 4
                  br 0 (;@7;)
                end
                unreachable
              end
              local.get 14
              local.set 11
              local.get 0
              br_if 4 (;@1;)
              local.get 18
              i32.eqz
              br_if 1 (;@4;)
              i32.const 1
              local.set 4
              loop  ;; label = @6
                local.get 3
                local.get 4
                i32.const 2
                i32.shl
                i32.add
                i32.load
                local.tee 0
                if  ;; label = @7
                  local.get 2
                  local.get 4
                  i32.const 3
                  i32.shl
                  i32.add
                  local.get 0
                  local.get 1
                  call 31
                  i32.const 1
                  local.set 11
                  local.get 4
                  i32.const 1
                  i32.add
                  local.tee 4
                  i32.const 10
                  i32.ne
                  br_if 1 (;@6;)
                  br 6 (;@1;)
                end
              end
              i32.const 1
              local.set 11
              local.get 4
              i32.const 9
              i32.gt_u
              br_if 4 (;@1;)
              i32.const -1
              local.set 11
              local.get 3
              local.get 4
              i32.const 2
              i32.shl
              i32.add
              i32.load
              br_if 4 (;@1;)
              loop  ;; label = @6
                local.get 4
                local.tee 0
                i32.const 1
                i32.add
                local.tee 4
                i32.const 10
                i32.ne
                if  ;; label = @7
                  local.get 3
                  local.get 4
                  i32.const 2
                  i32.shl
                  i32.add
                  i32.load
                  i32.eqz
                  br_if 1 (;@6;)
                end
              end
              i32.const -1
              i32.const 1
              local.get 0
              i32.const 9
              i32.lt_u
              select
              local.set 11
              br 4 (;@1;)
            end
            local.get 0
            i32.const 32
            local.get 11
            local.get 7
            local.get 10
            i32.sub
            local.tee 9
            local.get 8
            local.get 8
            local.get 9
            i32.lt_s
            select
            local.tee 7
            i32.add
            local.tee 12
            local.get 13
            local.get 13
            local.get 12
            i32.lt_s
            select
            local.tee 4
            local.get 12
            local.get 6
            call 14
            local.get 0
            local.get 15
            local.get 11
            call 15
            local.get 0
            i32.const 48
            local.get 4
            local.get 12
            local.get 6
            i32.const 65536
            i32.xor
            call 14
            local.get 0
            i32.const 48
            local.get 7
            local.get 9
            i32.const 0
            call 14
            local.get 0
            local.get 10
            local.get 9
            call 15
            local.get 0
            i32.const 32
            local.get 4
            local.get 12
            local.get 6
            i32.const 8192
            i32.xor
            call 14
            br 1 (;@3;)
          end
        end
        i32.const 0
        local.set 11
        br 1 (;@1;)
      end
      i32.const -1
      local.set 11
    end
    local.get 5
    i32.const 80
    i32.add
    global.set 0
    local.get 11)
  (func (;27;) (type 8) (param i32 i64)
    (local i64)
    local.get 0
    local.get 1
    local.get 0
    i64.load offset=64
    local.tee 2
    i64.add
    local.tee 1
    i64.store offset=64
    local.get 0
    local.get 0
    i64.load offset=72
    local.get 1
    local.get 2
    i64.lt_u
    i64.extend_i32_u
    i64.add
    i64.store offset=72)
  (func (;28;) (type 0) (param i32) (result i32)
    (local i32)
    local.get 0
    i32.eqz
    if  ;; label = @1
      i32.const -25
      return
    end
    local.get 0
    i32.load
    i32.eqz
    if  ;; label = @1
      i32.const -1
      return
    end
    block  ;; label = @1
      block (result i32)  ;; label = @2
        i32.const -2
        local.get 0
        i32.load offset=4
        i32.const 4
        i32.lt_u
        br_if 0 (;@2;)
        drop
        local.get 0
        i32.load offset=8
        i32.eqz
        if  ;; label = @3
          i32.const -18
          local.get 0
          i32.load offset=12
          br_if 1 (;@2;)
          drop
        end
        local.get 0
        i32.load offset=20
        local.set 1
        local.get 0
        i32.load offset=16
        i32.eqz
        br_if 1 (;@1;)
        i32.const -6
        local.get 1
        i32.const 8
        i32.lt_u
        br_if 0 (;@2;)
        drop
        local.get 0
        i32.load offset=24
        i32.eqz
        if  ;; label = @3
          i32.const -20
          local.get 0
          i32.load offset=28
          br_if 1 (;@2;)
          drop
        end
        local.get 0
        i32.load offset=32
        i32.eqz
        if  ;; label = @3
          i32.const -21
          local.get 0
          i32.load offset=36
          br_if 1 (;@2;)
          drop
        end
        i32.const -14
        local.get 0
        i32.load offset=44
        local.tee 1
        i32.const 8
        i32.lt_u
        br_if 0 (;@2;)
        drop
        i32.const -15
        local.get 1
        i32.const 2097152
        i32.gt_u
        br_if 0 (;@2;)
        drop
        i32.const -14
        local.get 1
        local.get 0
        i32.load offset=48
        local.tee 1
        i32.const 3
        i32.shl
        i32.lt_u
        br_if 0 (;@2;)
        drop
        local.get 0
        i32.load offset=40
        i32.eqz
        if  ;; label = @3
          i32.const -12
          return
        end
        local.get 1
        i32.eqz
        if  ;; label = @3
          i32.const -16
          return
        end
        i32.const -17
        local.get 1
        i32.const 16777215
        i32.gt_u
        br_if 0 (;@2;)
        drop
        local.get 0
        i32.load offset=52
        local.tee 1
        i32.eqz
        if  ;; label = @3
          i32.const -28
          return
        end
        i32.const -29
        local.get 1
        i32.const 16777215
        i32.gt_u
        br_if 0 (;@2;)
        drop
        local.get 0
        i32.load offset=64
        local.set 1
        block  ;; label = @3
          local.get 0
          i32.load offset=60
          if  ;; label = @4
            local.get 1
            br_if 1 (;@3;)
            i32.const -23
            return
          end
          i32.const -24
          local.get 1
          br_if 1 (;@2;)
          drop
        end
        i32.const 0
      end
      return
    end
    i32.const -19
    i32.const -6
    local.get 1
    select)
  (func (;29;) (type 1) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 2
    local.get 1
    i32.store offset=8
    local.get 2
    i32.load offset=12
    i32.const 0
    local.get 2
    i32.load offset=8
    i32.const 3032
    i32.load
    call_indirect (type 2)
    drop
    local.get 2
    i32.const 16
    i32.add
    global.set 0)
  (func (;30;) (type 7) (param i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 384
    i32.sub
    local.tee 4
    global.set 0
    local.get 4
    i32.const 0
    i32.store offset=140
    local.get 4
    i32.const 140
    i32.add
    local.get 1
    call 8
    block  ;; label = @1
      local.get 1
      i32.const 64
      i32.le_u
      if  ;; label = @2
        local.get 4
        i32.const 144
        i32.add
        local.get 1
        call 24
        i32.const 0
        i32.lt_s
        br_if 1 (;@1;)
        local.get 4
        i32.const 144
        i32.add
        local.get 4
        i32.const 140
        i32.add
        i32.const 4
        call 5
        i32.const 0
        i32.lt_s
        br_if 1 (;@1;)
        local.get 4
        i32.const 144
        i32.add
        local.get 2
        local.get 3
        call 5
        i32.const 0
        i32.lt_s
        br_if 1 (;@1;)
        local.get 4
        i32.const 144
        i32.add
        local.get 0
        local.get 1
        call 19
        drop
        br 1 (;@1;)
      end
      local.get 4
      i32.const 144
      i32.add
      i32.const 64
      call 24
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 4
      i32.const 144
      i32.add
      local.get 4
      i32.const 140
      i32.add
      i32.const 4
      call 5
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 4
      i32.const 144
      i32.add
      local.get 2
      local.get 3
      call 5
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 4
      i32.const 144
      i32.add
      local.get 4
      i32.const -64
      i32.sub
      i32.const 64
      call 19
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 0
      local.get 4
      i64.load offset=64
      i64.store align=1
      local.get 0
      local.get 4
      i64.load offset=72
      i64.store offset=8 align=1
      local.get 0
      local.get 4
      i64.load offset=88
      i64.store offset=24 align=1
      local.get 0
      local.get 4
      i64.load offset=80
      i64.store offset=16 align=1
      local.get 4
      local.get 4
      i32.const -64
      i32.sub
      i32.const 64
      call 6
      local.set 2
      local.get 0
      i32.const 32
      i32.add
      local.set 0
      local.get 1
      i32.const -32
      i32.add
      local.tee 1
      i32.const 65
      i32.ge_u
      if  ;; label = @2
        loop  ;; label = @3
          local.get 2
          i32.const -64
          i32.sub
          i32.const 64
          local.get 2
          call 46
          i32.const 0
          i32.lt_s
          br_if 2 (;@1;)
          local.get 0
          local.get 2
          i64.load offset=64
          i64.store align=1
          local.get 0
          local.get 2
          i64.load offset=72
          i64.store offset=8 align=1
          local.get 0
          local.get 4
          i64.load offset=88
          i64.store offset=24 align=1
          local.get 0
          local.get 4
          i64.load offset=80
          i64.store offset=16 align=1
          local.get 0
          i32.const 32
          i32.add
          local.set 0
          local.get 2
          local.get 2
          i32.const -64
          i32.sub
          i32.const 64
          call 6
          drop
          local.get 1
          i32.const -32
          i32.add
          local.tee 1
          i32.const 64
          i32.gt_u
          br_if 0 (;@3;)
        end
      end
      local.get 2
      i32.const -64
      i32.sub
      local.get 1
      local.get 2
      call 46
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.const -64
      i32.sub
      local.get 1
      call 6
      drop
    end
    local.get 4
    i32.const 144
    i32.add
    i32.const 240
    call 7
    local.get 4
    i32.const 384
    i32.add
    global.set 0)
  (func (;31;) (type 4) (param i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 20
        i32.gt_u
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 1
                        i32.const -9
                        i32.add
                        br_table 0 (;@10;) 1 (;@9;) 2 (;@8;) 9 (;@1;) 3 (;@7;) 4 (;@6;) 5 (;@5;) 6 (;@4;) 9 (;@1;) 7 (;@3;) 8 (;@2;)
                      end
                      local.get 2
                      local.get 2
                      i32.load
                      local.tee 1
                      i32.const 4
                      i32.add
                      i32.store
                      local.get 0
                      local.get 1
                      i32.load
                      i32.store
                      return
                    end
                    local.get 2
                    local.get 2
                    i32.load
                    local.tee 1
                    i32.const 4
                    i32.add
                    i32.store
                    local.get 0
                    local.get 1
                    i64.load32_s
                    i64.store
                    return
                  end
                  local.get 2
                  local.get 2
                  i32.load
                  local.tee 1
                  i32.const 4
                  i32.add
                  i32.store
                  local.get 0
                  local.get 1
                  i64.load32_u
                  i64.store
                  return
                end
                local.get 2
                local.get 2
                i32.load
                local.tee 1
                i32.const 4
                i32.add
                i32.store
                local.get 0
                local.get 1
                i64.load16_s
                i64.store
                return
              end
              local.get 2
              local.get 2
              i32.load
              local.tee 1
              i32.const 4
              i32.add
              i32.store
              local.get 0
              local.get 1
              i64.load16_u
              i64.store
              return
            end
            local.get 2
            local.get 2
            i32.load
            local.tee 1
            i32.const 4
            i32.add
            i32.store
            local.get 0
            local.get 1
            i64.load8_s
            i64.store
            return
          end
          local.get 2
          local.get 2
          i32.load
          local.tee 1
          i32.const 4
          i32.add
          i32.store
          local.get 0
          local.get 1
          i64.load8_u
          i64.store
          return
        end
        local.get 0
        local.get 2
        i32.const 0
        call_indirect (type 1)
      end
      return
    end
    local.get 2
    local.get 2
    i32.load
    i32.const 7
    i32.add
    i32.const -8
    i32.and
    local.tee 1
    i32.const 8
    i32.add
    i32.store
    local.get 0
    local.get 1
    i64.load
    i64.store)
  (func (;32;) (type 0) (param i32) (result i32)
    (local i32 i32 i32)
    local.get 0
    i32.load
    i32.load8_s
    call 16
    if  ;; label = @1
      loop  ;; label = @2
        local.get 0
        i32.load
        local.tee 1
        i32.load8_s
        local.get 0
        local.get 1
        i32.const 1
        i32.add
        i32.store
        local.get 2
        i32.const 10
        i32.mul
        i32.add
        i32.const -48
        i32.add
        local.set 2
        local.get 1
        i32.load8_s offset=1
        call 16
        br_if 0 (;@2;)
      end
    end
    local.get 2)
  (func (;33;) (type 3) (param i32 i32) (result i32)
    local.get 0
    i32.eqz
    if  ;; label = @1
      i32.const 0
      return
    end
    local.get 0
    local.get 1
    call 56)
  (func (;34;) (type 0) (param i32) (result i32)
    local.get 0
    i32.const 0
    call 2)
  (func (;35;) (type 4) (param i32 i32 i32)
    local.get 1
    local.get 1
    i64.load offset=48
    i64.const 1
    i64.add
    i64.store offset=48
    local.get 2
    local.get 1
    local.get 0
    i32.const 0
    call 18
    local.get 2
    local.get 0
    local.get 0
    i32.const 0
    call 18)
  (func (;36;) (type 1) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i64 i64)
    global.get 0
    i32.const 3072
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        block (result i32)  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 0
                i32.load offset=32
                i32.const -1
                i32.add
                br_table 2 (;@4;) 1 (;@5;) 0 (;@6;)
              end
              local.get 1
              i32.load
              local.set 4
              i32.const 0
              br 2 (;@3;)
            end
            local.get 1
            i32.load
            br_if 2 (;@2;)
            local.get 1
            i32.load8_u offset=8
            i32.const 2
            i32.lt_u
            br_if 0 (;@4;)
            local.get 1
            i32.load8_u offset=8
            i32.eqz
            i32.const 1
            i32.shl
            local.set 6
            br 2 (;@2;)
          end
          local.get 2
          call 43
          local.get 2
          i32.const 1024
          i32.add
          call 43
          local.get 2
          local.get 1
          i32.load
          local.tee 4
          i64.extend_i32_u
          i64.store offset=1024
          local.get 2
          local.get 1
          i64.load32_u offset=4
          i64.store offset=1032
          local.get 2
          local.get 1
          i64.load8_u offset=8
          i64.store offset=1040
          local.get 2
          local.get 0
          i64.load32_u offset=12
          i64.store offset=1048
          local.get 2
          local.get 0
          i64.load32_u offset=8
          i64.store offset=1056
          local.get 2
          local.get 0
          i64.load32_u offset=32
          i64.store offset=1064
          i32.const 1
        end
        local.set 9
        local.get 4
        br_if 0 (;@2;)
        local.get 1
        i32.load8_u offset=8
        local.tee 4
        i32.eqz
        i32.const 1
        i32.shl
        local.set 6
        local.get 4
        local.get 9
        i32.const 1
        i32.xor
        i32.or
        br_if 0 (;@2;)
        local.get 2
        i32.const 2048
        i32.add
        local.get 2
        i32.const 1024
        i32.add
        local.get 2
        call 35
        i32.const 2
        local.set 6
      end
      local.get 6
      local.get 0
      i32.load offset=16
      local.tee 4
      i32.ge_u
      br_if 0 (;@1;)
      i32.const -1
      local.get 0
      i32.load offset=20
      local.tee 5
      i32.const -1
      i32.add
      local.get 5
      local.get 1
      i32.load offset=4
      i32.mul
      local.get 6
      i32.add
      local.get 4
      local.get 1
      i32.load8_u offset=8
      i32.mul
      i32.add
      local.tee 4
      local.get 5
      i32.rem_u
      select
      local.get 4
      i32.add
      local.set 3
      loop  ;; label = @2
        local.get 4
        i32.const -1
        i32.add
        local.get 3
        local.get 4
        local.get 5
        i32.rem_u
        i32.const 1
        i32.eq
        select
        local.set 5
        block (result i32)  ;; label = @3
          local.get 9
          if  ;; label = @4
            local.get 6
            i32.const 127
            i32.and
            local.tee 3
            i32.eqz
            if  ;; label = @5
              local.get 2
              i32.const 2048
              i32.add
              local.get 2
              i32.const 1024
              i32.add
              local.get 2
              call 35
            end
            local.get 2
            i32.const 2048
            i32.add
            local.get 3
            i32.const 3
            i32.shl
            i32.add
            br 1 (;@3;)
          end
          local.get 0
          i32.load
          local.get 5
          i32.const 10
          i32.shl
          i32.add
        end
        local.tee 3
        i32.load offset=4
        local.set 7
        local.get 3
        i32.load
        local.set 3
        local.get 0
        i32.load offset=24
        local.set 8
        local.get 1
        local.get 6
        i32.store offset=12
        local.get 0
        local.get 1
        local.get 3
        local.get 7
        local.get 8
        i32.rem_u
        i64.extend_i32_u
        local.tee 10
        local.get 10
        local.get 1
        i64.load32_u offset=4
        local.tee 10
        local.get 1
        i32.load8_u offset=8
        select
        local.get 1
        i32.load
        select
        local.tee 11
        local.get 10
        i64.eq
        call 68
        local.set 7
        local.get 0
        i32.load
        local.tee 3
        local.get 0
        i32.load offset=20
        local.get 11
        i32.wrap_i64
        i32.mul
        i32.const 10
        i32.shl
        i32.add
        local.get 7
        i32.const 10
        i32.shl
        i32.add
        local.set 7
        local.get 3
        local.get 4
        i32.const 10
        i32.shl
        i32.add
        local.set 8
        block  ;; label = @3
          local.get 0
          i32.load offset=4
          i32.const 16
          i32.eq
          if  ;; label = @4
            local.get 3
            local.get 5
            i32.const 10
            i32.shl
            i32.add
            local.get 7
            local.get 8
            i32.const 0
            call 18
            br 1 (;@3;)
          end
          local.get 3
          local.get 5
          i32.const 10
          i32.shl
          i32.add
          local.set 3
          local.get 1
          i32.load
          i32.eqz
          if  ;; label = @4
            local.get 3
            local.get 7
            local.get 8
            i32.const 0
            call 18
            br 1 (;@3;)
          end
          local.get 3
          local.get 7
          local.get 8
          i32.const 1
          call 18
        end
        local.get 6
        i32.const 1
        i32.add
        local.tee 6
        local.get 0
        i32.load offset=16
        i32.ge_u
        br_if 1 (;@1;)
        local.get 4
        i32.const 1
        i32.add
        local.set 4
        local.get 5
        i32.const 1
        i32.add
        local.set 3
        local.get 0
        i32.load offset=20
        local.set 5
        br 0 (;@2;)
      end
      unreachable
    end
    local.get 2
    i32.const 3072
    i32.add
    global.set 0)
  (func (;37;) (type 0) (param i32) (result i32)
    (local i32 i32)
    local.get 0
    i32.const 16320
    i32.xor
    i32.const 1
    i32.add
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.const 47
    i32.and
    local.get 0
    i32.const 16321
    i32.xor
    i32.const 1
    i32.add
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.const 43
    i32.and
    local.get 0
    i32.const 65510
    i32.add
    i32.const 8
    i32.shr_u
    i32.const 255
    i32.and
    local.tee 1
    local.get 0
    i32.const 65
    i32.add
    i32.and
    i32.or
    i32.or
    local.get 0
    i32.const 65484
    i32.add
    i32.const 8
    i32.shr_u
    local.tee 2
    local.get 0
    i32.const 71
    i32.add
    i32.and
    local.get 1
    i32.const 255
    i32.xor
    i32.and
    i32.or
    local.get 0
    i32.const 252
    i32.add
    local.get 0
    i32.const 65474
    i32.add
    i32.const 8
    i32.shr_u
    i32.and
    local.get 2
    i32.const -1
    i32.xor
    i32.and
    i32.const 255
    i32.and
    i32.or)
  (func (;38;) (type 5) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32)
    i32.const -1
    local.set 5
    local.get 3
    i32.const 3
    i32.div_u
    local.tee 6
    i32.const 2
    i32.shl
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 6
          i32.const 3
          i32.mul
          i32.const -1
          i32.xor
          local.get 3
          i32.add
          br_table 1 (;@2;) 0 (;@3;) 2 (;@1;)
        end
        local.get 4
        i32.const 1
        i32.or
        local.set 4
      end
      local.get 4
      i32.const 2
      i32.add
      local.set 4
    end
    local.get 4
    local.get 1
    i32.lt_u
    if (result i32)  ;; label = @1
      block  ;; label = @2
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        i32.const 0
        local.set 1
        loop  ;; label = @3
          local.get 1
          i32.const 8
          i32.add
          local.set 1
          local.get 2
          i32.load8_u
          local.get 7
          i32.const 8
          i32.shl
          i32.or
          local.set 7
          loop  ;; label = @4
            local.get 0
            local.tee 5
            local.get 7
            local.get 1
            local.tee 6
            i32.const -6
            i32.add
            local.tee 1
            i32.shr_u
            i32.const 63
            i32.and
            call 37
            i32.store8
            local.get 5
            i32.const 1
            i32.add
            local.set 0
            local.get 1
            i32.const 5
            i32.gt_u
            br_if 0 (;@4;)
          end
          local.get 2
          i32.const 1
          i32.add
          local.set 2
          local.get 3
          i32.const -1
          i32.add
          local.tee 3
          br_if 0 (;@3;)
        end
        local.get 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 5
        local.get 7
        i32.const 12
        local.get 6
        i32.sub
        i32.shl
        i32.const 63
        i32.and
        call 37
        i32.store8 offset=1
        local.get 5
        i32.const 2
        i32.add
        local.set 0
      end
      local.get 0
      i32.const 0
      i32.store8
      local.get 4
    else
      local.get 5
    end)
  (func (;39;) (type 0) (param i32) (result i32)
    (local i32)
    i32.const 0
    local.get 0
    i32.const 4
    i32.add
    local.get 0
    i32.const 65488
    i32.add
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.and
    i32.const 57
    local.get 0
    i32.sub
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.and
    i32.const 255
    i32.and
    local.get 0
    i32.const -65
    i32.add
    local.tee 1
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    local.get 1
    i32.and
    i32.const 90
    local.get 0
    i32.sub
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.and
    i32.const 255
    i32.and
    local.get 0
    i32.const 185
    i32.add
    local.get 0
    i32.const 65439
    i32.add
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.and
    i32.const 122
    local.get 0
    i32.sub
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.and
    i32.const 255
    i32.and
    local.get 0
    i32.const 16336
    i32.xor
    i32.const 1
    i32.add
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.const 63
    i32.and
    local.get 0
    i32.const 16340
    i32.xor
    i32.const 1
    i32.add
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    i32.const 62
    i32.and
    i32.or
    i32.or
    i32.or
    i32.or
    local.tee 1
    i32.sub
    i32.const 8
    i32.shr_u
    i32.const -1
    i32.xor
    local.get 0
    i32.const 65470
    i32.xor
    i32.const 1
    i32.add
    i32.const 8
    i32.shr_u
    i32.and
    i32.const 255
    i32.and
    local.get 1
    i32.or)
  (func (;40;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32)
    block (result i32)  ;; label = @1
      local.get 2
      i32.load8_s
      call 39
      local.tee 4
      i32.const 255
      i32.ne
      if  ;; label = @2
        loop  ;; label = @3
          local.get 4
          local.get 5
          i32.const 6
          i32.shl
          i32.add
          local.set 5
          block  ;; label = @4
            local.get 3
            i32.const 6
            i32.add
            local.tee 4
            i32.const 8
            i32.lt_u
            if  ;; label = @5
              local.get 4
              local.set 3
              br 1 (;@4;)
            end
            local.get 6
            local.get 1
            i32.load
            i32.ge_u
            if  ;; label = @5
              i32.const 0
              return
            end
            local.get 0
            local.get 5
            local.get 3
            i32.const -2
            i32.add
            local.tee 3
            i32.shr_u
            i32.store8
            local.get 0
            i32.const 1
            i32.add
            local.set 0
            local.get 6
            i32.const 1
            i32.add
            local.set 6
          end
          local.get 2
          i32.const 1
          i32.add
          local.tee 2
          i32.load8_s
          call 39
          local.tee 4
          i32.const 255
          i32.ne
          br_if 0 (;@3;)
        end
        i32.const 0
        local.get 3
        i32.const 4
        i32.gt_u
        br_if 1 (;@1;)
        drop
      end
      i32.const 0
      local.get 5
      i32.const -1
      local.get 3
      i32.shl
      i32.const -1
      i32.xor
      i32.and
      br_if 0 (;@1;)
      drop
      local.get 1
      local.get 6
      i32.store
      local.get 2
    end)
  (func (;41;) (type 6) (param i32)
    local.get 0
    i32.load8_u offset=232
    if  ;; label = @1
      local.get 0
      i64.const -1
      i64.store offset=88
    end
    local.get 0
    i64.const -1
    i64.store offset=80)
  (func (;42;) (type 1) (param i32 i32)
    (local i32 i32)
    loop  ;; label = @1
      local.get 0
      local.get 2
      i32.const 3
      i32.shl
      local.tee 3
      i32.add
      local.get 1
      local.get 3
      i32.add
      i64.load align=1
      i64.store
      local.get 2
      i32.const 1
      i32.add
      local.tee 2
      i32.const 128
      i32.ne
      br_if 0 (;@1;)
    end)
  (func (;43;) (type 6) (param i32)
    local.get 0
    i32.const 0
    i32.const 1024
    call 9
    drop)
  (func (;44;) (type 3) (param i32 i32) (result i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      local.get 0
      call 28
      local.tee 3
      br_if 0 (;@1;)
      i32.const -26
      local.set 3
      local.get 1
      i32.const 2
      i32.gt_u
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=44
      local.set 4
      local.get 0
      i32.load offset=48
      local.set 3
      local.get 0
      i32.load offset=56
      local.set 5
      local.get 2
      i32.const 0
      i32.store
      local.get 2
      local.get 5
      i32.store offset=4
      local.get 0
      i32.load offset=40
      local.set 5
      local.get 2
      local.get 3
      i32.store offset=24
      local.get 2
      local.get 5
      i32.store offset=8
      local.get 2
      local.get 3
      i32.const 3
      i32.shl
      local.tee 5
      local.get 4
      local.get 4
      local.get 5
      i32.lt_u
      select
      local.get 3
      i32.const 2
      i32.shl
      local.tee 5
      i32.div_u
      local.tee 4
      i32.store offset=16
      local.get 2
      local.get 4
      i32.const 2
      i32.shl
      i32.store offset=20
      local.get 2
      local.get 4
      local.get 5
      i32.mul
      i32.store offset=12
      local.get 0
      i32.load offset=52
      local.set 4
      local.get 2
      local.get 1
      i32.store offset=32
      local.get 2
      local.get 4
      i32.store offset=28
      local.get 4
      local.get 3
      i32.gt_u
      if  ;; label = @2
        local.get 2
        local.get 3
        i32.store offset=28
      end
      local.get 2
      local.get 0
      call 62
      local.tee 3
      br_if 0 (;@1;)
      local.get 2
      call 67
      local.tee 3
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      call 70
      i32.const 0
      local.set 3
    end
    local.get 2
    i32.const 48
    i32.add
    global.set 0
    local.get 3)
  (func (;45;) (type 0) (param i32) (result i32)
    (local i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            br_table 0 (;@4;) 1 (;@3;) 2 (;@2;) 3 (;@1;)
          end
          i32.const 1864
          return
        end
        i32.const 1880
        return
      end
      i32.const 1897
      local.set 1
    end
    local.get 1)
  (func (;46;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32)
    global.get 0
    i32.const 240
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      local.get 2
      i32.eqz
      if  ;; label = @2
        i32.const -1
        local.set 4
        br 1 (;@1;)
      end
      i32.const -1
      local.set 4
      local.get 0
      i32.eqz
      local.get 1
      i32.const -1
      i32.add
      i32.const 63
      i32.gt_u
      i32.or
      br_if 0 (;@1;)
      local.get 3
      local.get 1
      call 24
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 3
      local.get 2
      i32.const 64
      call 5
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 3
      local.get 0
      local.get 1
      call 19
      local.set 4
    end
    local.get 3
    i32.const 240
    call 7
    local.get 3
    i32.const 240
    i32.add
    global.set 0
    local.get 4)
  (func (;47;) (type 8) (param i32 i64)
    local.get 0
    local.get 1
    i64.store align=1)
  (func (;48;) (type 0) (param i32) (result i32)
    global.get 0
    local.get 0
    i32.sub
    i32.const -16
    i32.and
    local.tee 0
    global.set 0
    local.get 0)
  (func (;49;) (type 4) (param i32 i32 i32)
    (local i32 i32 i32)
    block  ;; label = @1
      local.get 2
      i32.load offset=16
      local.tee 4
      if (result i32)  ;; label = @2
        local.get 4
      else
        local.get 2
        call 50
        br_if 1 (;@1;)
        local.get 2
        i32.load offset=16
      end
      local.get 2
      i32.load offset=20
      local.tee 5
      i32.sub
      local.get 1
      i32.lt_u
      if  ;; label = @2
        local.get 2
        local.get 0
        local.get 1
        local.get 2
        i32.load offset=36
        call_indirect (type 2)
        drop
        return
      end
      block  ;; label = @2
        local.get 2
        i32.load8_s offset=75
        i32.const 0
        i32.lt_s
        br_if 0 (;@2;)
        local.get 1
        local.set 4
        loop  ;; label = @3
          local.get 4
          local.tee 3
          i32.eqz
          br_if 1 (;@2;)
          local.get 0
          local.get 3
          i32.const -1
          i32.add
          local.tee 4
          i32.add
          i32.load8_u
          i32.const 10
          i32.ne
          br_if 0 (;@3;)
        end
        local.get 2
        local.get 0
        local.get 3
        local.get 2
        i32.load offset=36
        call_indirect (type 2)
        local.get 3
        i32.lt_u
        br_if 1 (;@1;)
        local.get 1
        local.get 3
        i32.sub
        local.set 1
        local.get 0
        local.get 3
        i32.add
        local.set 0
        local.get 2
        i32.load offset=20
        local.set 5
      end
      local.get 5
      local.get 0
      local.get 1
      call 6
      drop
      local.get 2
      local.get 2
      i32.load offset=20
      local.get 1
      i32.add
      i32.store offset=20
    end)
  (func (;50;) (type 0) (param i32) (result i32)
    (local i32)
    local.get 0
    local.get 0
    i32.load8_u offset=74
    local.tee 1
    i32.const -1
    i32.add
    local.get 1
    i32.or
    i32.store8 offset=74
    local.get 0
    i32.load
    local.tee 1
    i32.const 8
    i32.and
    if  ;; label = @1
      local.get 0
      local.get 1
      i32.const 32
      i32.or
      i32.store
      i32.const -1
      return
    end
    local.get 0
    i64.const 0
    i64.store offset=4 align=4
    local.get 0
    local.get 0
    i32.load offset=44
    local.tee 1
    i32.store offset=28
    local.get 0
    local.get 1
    i32.store offset=20
    local.get 0
    local.get 1
    local.get 0
    i32.load offset=48
    i32.add
    i32.store offset=16
    i32.const 0)
  (func (;51;) (type 0) (param i32) (result i32)
    (local i32 i64)
    block  ;; label = @1
      block (result i32)  ;; label = @2
        i32.const 0
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        drop
        local.get 0
        i64.extend_i32_u
        i64.const 4
        i64.mul
        local.tee 2
        i32.wrap_i64
        local.tee 1
        local.get 0
        i32.const 4
        i32.or
        i32.const 65536
        i32.lt_u
        br_if 0 (;@2;)
        drop
        i32.const -1
        local.get 1
        local.get 2
        i64.const 32
        i64.shr_u
        i32.wrap_i64
        select
      end
      local.tee 1
      call 13
      local.tee 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const -4
      i32.add
      i32.load8_u
      i32.const 3
      i32.and
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 0
      local.get 1
      call 9
      drop
    end
    local.get 0)
  (func (;52;) (type 9) (param i64 i32) (result i32)
    (local i32 i32 i32 i64)
    block  ;; label = @1
      local.get 0
      i64.const 4294967296
      i64.lt_u
      if  ;; label = @2
        local.get 0
        local.set 5
        br 1 (;@1;)
      end
      loop  ;; label = @2
        local.get 1
        i32.const -1
        i32.add
        local.tee 1
        local.get 0
        local.get 0
        i64.const 10
        i64.div_u
        local.tee 5
        i64.const 10
        i64.mul
        i64.sub
        i32.wrap_i64
        i32.const 48
        i32.or
        i32.store8
        local.get 0
        i64.const 42949672959
        i64.gt_u
        local.get 5
        local.set 0
        br_if 0 (;@2;)
      end
    end
    local.get 5
    i32.wrap_i64
    local.tee 2
    if  ;; label = @1
      loop  ;; label = @2
        local.get 1
        i32.const -1
        i32.add
        local.tee 1
        local.get 2
        local.get 2
        i32.const 10
        i32.div_u
        local.tee 3
        i32.const 10
        i32.mul
        i32.sub
        i32.const 48
        i32.or
        i32.store8
        local.get 2
        i32.const 9
        i32.gt_u
        local.get 3
        local.set 2
        br_if 0 (;@2;)
      end
    end
    local.get 1)
  (func (;53;) (type 9) (param i64 i32) (result i32)
    local.get 0
    i64.eqz
    i32.eqz
    if  ;; label = @1
      loop  ;; label = @2
        local.get 1
        i32.const -1
        i32.add
        local.tee 1
        local.get 0
        i32.wrap_i64
        i32.const 7
        i32.and
        i32.const 48
        i32.or
        i32.store8
        local.get 0
        i64.const 3
        i64.shr_u
        local.tee 0
        i64.const 0
        i64.ne
        br_if 0 (;@2;)
      end
    end
    local.get 1)
  (func (;54;) (type 14) (param i64 i32 i32) (result i32)
    local.get 0
    i64.eqz
    i32.eqz
    if  ;; label = @1
      loop  ;; label = @2
        local.get 1
        i32.const -1
        i32.add
        local.tee 1
        local.get 0
        i32.wrap_i64
        i32.const 15
        i32.and
        i32.const 3696
        i32.add
        i32.load8_u
        local.get 2
        i32.or
        i32.store8
        local.get 0
        i64.const 4
        i64.shr_u
        local.tee 0
        i64.const 0
        i64.ne
        br_if 0 (;@2;)
      end
    end
    local.get 1)
  (func (;55;) (type 1) (param i32 i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 208
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 1
    i32.store offset=204
    i32.const 0
    local.set 1
    local.get 2
    i32.const 160
    i32.add
    i32.const 0
    i32.const 40
    call 9
    drop
    local.get 2
    local.get 2
    i32.load offset=204
    i32.store offset=200
    block  ;; label = @1
      i32.const 0
      local.get 2
      i32.const 200
      i32.add
      local.get 2
      i32.const 80
      i32.add
      local.get 2
      i32.const 160
      i32.add
      call 26
      i32.const 0
      i32.lt_s
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=76
      i32.const 0
      i32.ge_s
      if  ;; label = @2
        i32.const 1
        local.set 1
      end
      local.get 0
      i32.load
      local.set 3
      local.get 0
      i32.load8_s offset=74
      i32.const 0
      i32.le_s
      if  ;; label = @2
        local.get 0
        local.get 3
        i32.const -33
        i32.and
        i32.store
      end
      local.get 3
      i32.const 32
      i32.and
      local.set 4
      block (result i32)  ;; label = @2
        local.get 0
        i32.load offset=48
        if  ;; label = @3
          local.get 0
          local.get 2
          i32.const 200
          i32.add
          local.get 2
          i32.const 80
          i32.add
          local.get 2
          i32.const 160
          i32.add
          call 26
          br 1 (;@2;)
        end
        local.get 0
        i32.const 80
        i32.store offset=48
        local.get 0
        local.get 2
        i32.const 80
        i32.add
        i32.store offset=16
        local.get 0
        local.get 2
        i32.store offset=28
        local.get 0
        local.get 2
        i32.store offset=20
        local.get 0
        i32.load offset=44
        local.set 3
        local.get 0
        local.get 2
        i32.store offset=44
        local.get 0
        local.get 2
        i32.const 200
        i32.add
        local.get 2
        i32.const 80
        i32.add
        local.get 2
        i32.const 160
        i32.add
        call 26
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        drop
        local.get 0
        i32.const 0
        i32.const 0
        local.get 0
        i32.load offset=36
        call_indirect (type 2)
        drop
        local.get 0
        i32.const 0
        i32.store offset=48
        local.get 0
        local.get 3
        i32.store offset=44
        local.get 0
        i32.const 0
        i32.store offset=28
        local.get 0
        i32.const 0
        i32.store offset=16
        local.get 0
        i32.load offset=20
        drop
        local.get 0
        i32.const 0
        i32.store offset=20
        i32.const 0
      end
      drop
      local.get 0
      local.get 0
      i32.load
      local.get 4
      i32.or
      i32.store
      local.get 1
      i32.eqz
      br_if 0 (;@1;)
    end
    local.get 2
    i32.const 208
    i32.add
    global.set 0)
  (func (;56;) (type 3) (param i32 i32) (result i32)
    block  ;; label = @1
      local.get 0
      if (result i32)  ;; label = @2
        local.get 1
        i32.const 127
        i32.le_u
        br_if 1 (;@1;)
        block  ;; label = @3
          i32.const 3892
          i32.load
          i32.load
          i32.eqz
          if  ;; label = @4
            local.get 1
            i32.const -128
            i32.and
            i32.const 57216
            i32.eq
            br_if 3 (;@1;)
            br 1 (;@3;)
          end
          local.get 1
          i32.const 2047
          i32.le_u
          if  ;; label = @4
            local.get 0
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=1
            local.get 0
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 192
            i32.or
            i32.store8
            i32.const 2
            return
          end
          local.get 1
          i32.const 55296
          i32.ge_u
          i32.const 0
          local.get 1
          i32.const -8192
          i32.and
          i32.const 57344
          i32.ne
          select
          i32.eqz
          if  ;; label = @4
            local.get 0
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=2
            local.get 0
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 224
            i32.or
            i32.store8
            local.get 0
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=1
            i32.const 3
            return
          end
          local.get 1
          i32.const -65536
          i32.add
          i32.const 1048575
          i32.le_u
          if  ;; label = @4
            local.get 0
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=3
            local.get 0
            local.get 1
            i32.const 18
            i32.shr_u
            i32.const 240
            i32.or
            i32.store8
            local.get 0
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=2
            local.get 0
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=1
            i32.const 4
            return
          end
        end
        i32.const 3948
        i32.const 25
        i32.store
        i32.const -1
      else
        i32.const 1
      end
      return
    end
    local.get 0
    local.get 1
    i32.store8
    i32.const 1)
  (func (;57;) (type 3) (param i32 i32) (result i32)
    (local i32)
    local.get 1
    i32.const 0
    i32.ne
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.eqz
          local.get 0
          i32.const 3
          i32.and
          i32.eqz
          i32.or
          br_if 0 (;@3;)
          loop  ;; label = @4
            local.get 0
            i32.load8_u
            i32.eqz
            br_if 2 (;@2;)
            local.get 0
            i32.const 1
            i32.add
            local.set 0
            local.get 1
            i32.const -1
            i32.add
            local.tee 1
            i32.const 0
            i32.ne
            local.set 2
            local.get 1
            i32.eqz
            br_if 1 (;@3;)
            local.get 0
            i32.const 3
            i32.and
            br_if 0 (;@4;)
          end
        end
        local.get 2
        i32.eqz
        br_if 1 (;@1;)
      end
      block  ;; label = @2
        local.get 0
        i32.load8_u
        i32.eqz
        local.get 1
        i32.const 4
        i32.lt_u
        i32.or
        br_if 0 (;@2;)
        loop  ;; label = @3
          local.get 0
          i32.load
          local.tee 2
          i32.const -1
          i32.xor
          local.get 2
          i32.const -16843009
          i32.add
          i32.and
          i32.const -2139062144
          i32.and
          br_if 1 (;@2;)
          local.get 0
          i32.const 4
          i32.add
          local.set 0
          local.get 1
          i32.const -4
          i32.add
          local.tee 1
          i32.const 3
          i32.gt_u
          br_if 0 (;@3;)
        end
      end
      local.get 1
      i32.eqz
      br_if 0 (;@1;)
      loop  ;; label = @2
        local.get 0
        i32.load8_u
        i32.eqz
        if  ;; label = @3
          local.get 0
          return
        end
        local.get 0
        i32.const 1
        i32.add
        local.set 0
        local.get 1
        i32.const -1
        i32.add
        local.tee 1
        br_if 0 (;@2;)
      end
    end
    i32.const 0)
  (func (;58;) (type 1) (param i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 160
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    i32.const 8
    i32.add
    i32.const 3056
    i32.const 144
    call 6
    drop
    local.get 2
    local.get 0
    i32.store offset=52
    local.get 2
    local.get 0
    i32.store offset=28
    local.get 2
    i32.const -2
    local.get 0
    i32.sub
    local.tee 3
    i32.const 2147483647
    i32.const 2147483647
    local.get 3
    i32.gt_u
    select
    local.tee 3
    i32.store offset=56
    local.get 2
    local.get 0
    local.get 3
    i32.add
    local.tee 0
    i32.store offset=36
    local.get 2
    local.get 0
    i32.store offset=24
    local.get 2
    i32.const 8
    i32.add
    local.get 1
    call 55
    local.get 3
    if  ;; label = @1
      local.get 2
      i32.load offset=28
      local.tee 0
      local.get 0
      local.get 2
      i32.load offset=24
      i32.eq
      i32.sub
      i32.const 0
      i32.store8
    end
    local.get 2
    i32.const 160
    i32.add
    global.set 0)
  (func (;59;) (type 2) (param i32 i32 i32) (result i32)
    (local i32)
    local.get 0
    i32.load offset=20
    local.tee 3
    local.get 1
    local.get 2
    local.get 0
    i32.load offset=16
    local.get 3
    i32.sub
    local.tee 1
    local.get 1
    local.get 2
    i32.gt_u
    select
    local.tee 1
    call 6
    drop
    local.get 0
    local.get 0
    i32.load offset=20
    local.get 1
    i32.add
    i32.store offset=20
    local.get 2)
  (func (;60;) (type 5) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 96
    i32.sub
    local.tee 4
    global.set 0
    local.get 3
    call 45
    local.set 5
    local.get 2
    call 28
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        local.get 5
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        br_if 1 (;@1;)
        local.get 1
        i32.const 2
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.const 36
        i32.store16 align=1
        local.get 1
        i32.const -1
        i32.add
        local.tee 3
        local.get 5
        call 11
        local.tee 1
        i32.le_u
        br_if 0 (;@2;)
        local.get 0
        i32.const 1
        i32.add
        local.get 5
        local.get 1
        i32.const 1
        i32.add
        call 6
        local.get 3
        local.get 1
        i32.sub
        local.tee 3
        i32.const 4
        i32.lt_u
        br_if 0 (;@2;)
        local.get 1
        i32.add
        local.tee 1
        i32.const 4027940
        i32.store align=1
        local.get 4
        local.get 2
        i32.load offset=56
        i32.store offset=48
        local.get 4
        i32.const -64
        i32.sub
        local.get 4
        i32.const 48
        i32.add
        call 20
        local.get 3
        i32.const -3
        i32.add
        local.tee 3
        local.get 4
        i32.const -64
        i32.sub
        call 11
        local.tee 0
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        i32.const 3
        i32.add
        local.get 4
        i32.const -64
        i32.sub
        local.get 0
        i32.const 1
        i32.add
        call 6
        local.set 1
        local.get 3
        local.get 0
        i32.sub
        local.tee 3
        i32.const 4
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        local.get 1
        i32.add
        local.tee 1
        i32.const 4025636
        i32.store align=1
        local.get 4
        local.get 2
        i32.load offset=44
        i32.store offset=32
        local.get 4
        i32.const -64
        i32.sub
        local.get 4
        i32.const 32
        i32.add
        call 20
        local.get 3
        i32.const -3
        i32.add
        local.tee 3
        local.get 4
        i32.const -64
        i32.sub
        call 11
        local.tee 0
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        i32.const 3
        i32.add
        local.get 4
        i32.const -64
        i32.sub
        local.get 0
        i32.const 1
        i32.add
        call 6
        local.set 1
        local.get 3
        local.get 0
        i32.sub
        local.tee 3
        i32.const 4
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        local.get 1
        i32.add
        local.tee 1
        i32.const 4027436
        i32.store align=1
        local.get 4
        local.get 2
        i32.load offset=40
        i32.store offset=16
        local.get 4
        i32.const -64
        i32.sub
        local.get 4
        i32.const 16
        i32.add
        call 20
        local.get 3
        i32.const -3
        i32.add
        local.tee 3
        local.get 4
        i32.const -64
        i32.sub
        call 11
        local.tee 0
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        i32.const 3
        i32.add
        local.get 4
        i32.const -64
        i32.sub
        local.get 0
        i32.const 1
        i32.add
        call 6
        local.set 1
        local.get 3
        local.get 0
        i32.sub
        local.tee 3
        i32.const 4
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        local.get 1
        i32.add
        local.tee 1
        i32.const 4026412
        i32.store align=1
        local.get 4
        local.get 2
        i32.load offset=48
        i32.store
        local.get 4
        i32.const -64
        i32.sub
        local.get 4
        call 20
        local.get 3
        i32.const -3
        i32.add
        local.tee 3
        local.get 4
        i32.const -64
        i32.sub
        call 11
        local.tee 0
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        i32.const 3
        i32.add
        local.get 4
        i32.const -64
        i32.sub
        local.get 0
        i32.const 1
        i32.add
        call 6
        local.set 1
        local.get 3
        local.get 0
        i32.sub
        local.tee 3
        i32.const 2
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        local.get 1
        i32.add
        local.tee 0
        i32.const 36
        i32.store16 align=1
        local.get 0
        i32.const 1
        i32.add
        local.tee 0
        local.get 3
        i32.const -1
        i32.add
        local.tee 6
        local.get 2
        i32.load offset=16
        local.get 2
        i32.load offset=20
        call 38
        local.tee 1
        i32.const -1
        i32.eq
        local.tee 5
        br_if 0 (;@2;)
        i32.const -31
        local.set 3
        local.get 6
        i32.const 0
        local.get 1
        local.get 5
        select
        i32.sub
        local.tee 6
        i32.const 2
        i32.lt_u
        br_if 1 (;@1;)
        local.get 0
        local.get 0
        local.get 1
        i32.add
        local.get 5
        select
        local.tee 0
        i32.const 36
        i32.store16 align=1
        local.get 0
        i32.const 1
        i32.add
        local.get 6
        i32.const -1
        i32.add
        local.get 2
        i32.load
        local.get 2
        i32.load offset=4
        call 38
        local.set 0
        local.get 4
        i32.const 96
        i32.add
        global.set 0
        i32.const -31
        i32.const 0
        local.get 0
        i32.const -1
        i32.eq
        select
        return
      end
      i32.const -31
      local.set 3
    end
    local.get 4
    i32.const 96
    i32.add
    global.set 0
    local.get 3)
  (func (;61;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    local.get 0
    i32.load offset=4
    local.set 6
    local.get 0
    i32.load offset=20
    local.set 7
    block  ;; label = @1
      local.get 2
      call 45
      local.tee 4
      i32.eqz
      if  ;; label = @2
        i32.const -26
        local.set 2
        br 1 (;@1;)
      end
      i32.const -32
      local.set 2
      local.get 1
      i32.load8_u
      local.tee 5
      i32.const 36
      i32.ne
      br_if 0 (;@1;)
      local.get 1
      i32.const 1
      i32.add
      local.get 1
      local.get 5
      i32.const 36
      i32.eq
      select
      local.tee 1
      local.get 4
      local.get 4
      call 11
      local.tee 4
      call 17
      local.tee 5
      br_if 0 (;@1;)
      local.get 0
      i32.const 16
      i32.store offset=56
      local.get 1
      local.get 1
      local.get 4
      i32.add
      local.tee 1
      local.get 5
      select
      local.tee 4
      i32.const 3036
      i32.const 3
      call 17
      i32.eqz
      if  ;; label = @2
        local.get 4
        i32.const 3
        i32.add
        local.get 3
        i32.const 12
        i32.add
        call 21
        local.tee 1
        i32.eqz
        br_if 1 (;@1;)
        local.get 0
        local.get 3
        i32.load offset=12
        i32.store offset=56
      end
      local.get 1
      i32.const 3040
      i32.const 3
      call 17
      br_if 0 (;@1;)
      local.get 1
      i32.const 3
      i32.add
      local.get 3
      i32.const 12
      i32.add
      call 21
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 3
      i32.load offset=12
      i32.store offset=44
      local.get 1
      i32.const 3044
      i32.const 3
      call 17
      br_if 0 (;@1;)
      local.get 1
      i32.const 3
      i32.add
      local.get 3
      i32.const 12
      i32.add
      call 21
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 3
      i32.load offset=12
      i32.store offset=40
      local.get 1
      i32.const 3048
      i32.const 3
      call 17
      br_if 0 (;@1;)
      local.get 1
      i32.const 3
      i32.add
      local.get 3
      i32.const 12
      i32.add
      call 21
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 3
      i32.load offset=12
      local.tee 4
      i32.store offset=48
      local.get 0
      local.get 4
      i32.store offset=52
      local.get 1
      i32.load8_u
      i32.const 36
      i32.ne
      br_if 0 (;@1;)
      local.get 3
      local.get 7
      i32.store offset=12
      local.get 0
      i32.load offset=16
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.const 1
      i32.add
      call 40
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 3
      i32.load offset=12
      i32.store offset=20
      local.get 1
      i32.load8_u
      i32.const 36
      i32.ne
      br_if 0 (;@1;)
      local.get 3
      local.get 6
      i32.store offset=12
      local.get 0
      i32.load
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.const 1
      i32.add
      call 40
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 3
      i32.load offset=12
      i32.store offset=4
      local.get 0
      i32.const 0
      i32.store offset=68
      local.get 0
      i64.const 0
      i64.store offset=60 align=4
      local.get 0
      i64.const 0
      i64.store offset=24 align=4
      local.get 0
      i64.const 0
      i64.store offset=32 align=4
      local.get 0
      call 28
      local.tee 2
      br_if 0 (;@1;)
      i32.const -32
      i32.const 0
      local.get 1
      i32.load8_u
      select
      local.set 2
    end
    local.get 3
    i32.const 16
    i32.add
    global.set 0
    local.get 2)
  (func (;62;) (type 3) (param i32 i32) (result i32)
    (local i32 i32)
    global.get 0
    i32.const 80
    i32.sub
    local.tee 2
    global.set 0
    i32.const -25
    local.set 3
    block  ;; label = @1
      local.get 0
      i32.eqz
      local.get 1
      i32.eqz
      i32.or
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      i32.store offset=40
      local.get 1
      local.get 0
      local.get 0
      i32.load offset=12
      call 72
      local.tee 3
      br_if 0 (;@1;)
      local.get 2
      local.get 1
      local.get 0
      i32.load offset=32
      call 63
      local.get 2
      i32.const -64
      i32.sub
      i32.const 8
      call 7
      local.get 2
      local.get 0
      call 64
      local.get 2
      i32.const 72
      call 7
      i32.const 0
      local.set 3
    end
    local.get 2
    i32.const 80
    i32.add
    global.set 0
    local.get 3)
  (func (;63;) (type 4) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 256
    i32.sub
    local.tee 3
    global.set 0
    local.get 0
    i32.eqz
    local.get 1
    i32.eqz
    i32.or
    i32.eqz
    if  ;; label = @1
      local.get 3
      i32.const 16
      i32.add
      i32.const 64
      call 24
      drop
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=48
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=4
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=44
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=40
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=56
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 3
      i32.const 12
      i32.add
      local.get 2
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=12
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      block  ;; label = @2
        local.get 1
        i32.load offset=8
        local.tee 2
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        i32.const 16
        i32.add
        local.get 2
        local.get 1
        i32.load offset=12
        call 5
        drop
        local.get 1
        i32.load8_u offset=68
        i32.const 1
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load offset=8
        local.get 1
        i32.load offset=12
        call 29
        local.get 1
        i32.const 0
        i32.store offset=12
      end
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=20
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 1
      i32.load offset=16
      local.tee 2
      if  ;; label = @2
        local.get 3
        i32.const 16
        i32.add
        local.get 2
        local.get 1
        i32.load offset=20
        call 5
        drop
      end
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=28
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      block  ;; label = @2
        local.get 1
        i32.load offset=24
        local.tee 2
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        i32.const 16
        i32.add
        local.get 2
        local.get 1
        i32.load offset=28
        call 5
        drop
        local.get 1
        i32.load8_u offset=68
        i32.const 2
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load offset=24
        local.get 1
        i32.load offset=28
        call 29
        local.get 1
        i32.const 0
        i32.store offset=28
      end
      local.get 3
      i32.const 12
      i32.add
      local.get 1
      i32.load offset=36
      call 8
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 12
      i32.add
      i32.const 4
      call 5
      drop
      local.get 1
      i32.load offset=32
      local.tee 2
      if  ;; label = @2
        local.get 3
        i32.const 16
        i32.add
        local.get 2
        local.get 1
        i32.load offset=36
        call 5
        drop
      end
      local.get 3
      i32.const 16
      i32.add
      local.get 0
      i32.const 64
      call 19
      drop
    end
    local.get 3
    i32.const 256
    i32.add
    global.set 0)
  (func (;64;) (type 1) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 1024
    i32.sub
    local.tee 2
    global.set 0
    local.get 1
    i32.load offset=24
    if  ;; label = @1
      local.get 0
      i32.const 68
      i32.add
      local.set 5
      local.get 0
      i32.const -64
      i32.sub
      local.set 4
      loop  ;; label = @2
        local.get 4
        i32.const 0
        call 8
        local.get 5
        local.get 3
        call 8
        local.get 2
        i32.const 1024
        local.get 0
        i32.const 72
        call 30
        local.get 1
        i32.load
        local.get 1
        i32.load offset=20
        local.get 3
        i32.mul
        i32.const 10
        i32.shl
        i32.add
        local.get 2
        call 42
        local.get 4
        i32.const 1
        call 8
        local.get 2
        i32.const 1024
        local.get 0
        i32.const 72
        call 30
        local.get 1
        i32.load
        local.get 1
        i32.load offset=20
        local.get 3
        i32.mul
        i32.const 10
        i32.shl
        i32.add
        i32.const 1024
        i32.add
        local.get 2
        call 42
        local.get 3
        i32.const 1
        i32.add
        local.tee 3
        local.get 1
        i32.load offset=24
        i32.lt_u
        br_if 0 (;@2;)
      end
    end
    local.get 2
    i32.const 1024
    call 7
    local.get 2
    i32.const 1024
    i32.add
    global.set 0)
  (func (;65;) (type 0) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      local.get 0
      i32.load offset=24
      local.tee 3
      call 51
      local.tee 4
      i32.eqz
      if  ;; label = @2
        i32.const -22
        local.set 1
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 0
        i32.load offset=8
        i32.eqz
        br_if 0 (;@2;)
        loop  ;; label = @3
          i32.const 0
          local.set 5
          block  ;; label = @4
            loop  ;; label = @5
              block  ;; label = @6
                local.get 3
                i32.eqz
                if  ;; label = @7
                  i32.const 0
                  local.set 6
                  br 1 (;@6;)
                end
                i32.const 0
                local.set 1
                loop  ;; label = @7
                  local.get 1
                  local.get 0
                  i32.load offset=28
                  local.tee 3
                  i32.ge_u
                  if  ;; label = @8
                    local.get 4
                    local.get 1
                    local.get 3
                    i32.sub
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    call 34
                    br_if 4 (;@4;)
                  end
                  local.get 2
                  i32.const 0
                  i32.store offset=28
                  local.get 2
                  local.get 5
                  i32.store8 offset=24
                  local.get 2
                  local.get 2
                  i64.load offset=24
                  i64.store offset=8
                  local.get 2
                  local.get 7
                  i32.store offset=16
                  local.get 2
                  local.get 1
                  i32.store offset=20
                  local.get 2
                  local.get 2
                  i64.load offset=16
                  i64.store
                  local.get 0
                  local.get 2
                  call 36
                  local.get 0
                  i32.load offset=24
                  local.tee 3
                  local.set 6
                  local.get 1
                  i32.const 1
                  i32.add
                  local.tee 1
                  local.get 3
                  i32.lt_u
                  br_if 0 (;@7;)
                end
              end
              local.get 6
              local.get 0
              i32.load offset=28
              i32.sub
              local.tee 1
              local.get 3
              i32.lt_u
              if  ;; label = @6
                loop  ;; label = @7
                  local.get 4
                  local.get 1
                  i32.const 2
                  i32.shl
                  i32.add
                  i32.load
                  call 34
                  br_if 3 (;@4;)
                  local.get 1
                  i32.const 1
                  i32.add
                  local.tee 1
                  local.get 0
                  i32.load offset=24
                  local.tee 3
                  i32.lt_u
                  br_if 0 (;@7;)
                end
              end
              local.get 5
              i32.const 1
              i32.add
              local.tee 5
              i32.const 4
              i32.ne
              br_if 0 (;@5;)
            end
            i32.const 0
            local.set 1
            local.get 7
            i32.const 1
            i32.add
            local.tee 7
            local.get 0
            i32.load offset=8
            i32.lt_u
            br_if 1 (;@3;)
            br 2 (;@2;)
          end
        end
        i32.const -33
        local.set 1
      end
      local.get 4
      call 10
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 1)
  (func (;66;) (type 6) (param i32)
    (local i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 1
    global.set 0
    local.get 0
    i32.load offset=8
    if  ;; label = @1
      local.get 0
      i32.load offset=24
      local.set 2
      loop  ;; label = @2
        local.get 2
        local.set 3
        i32.const 0
        local.set 4
        loop  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            if  ;; label = @5
              i32.const 0
              local.set 3
              br 1 (;@4;)
            end
            i32.const 0
            local.set 5
            loop  ;; label = @5
              local.get 1
              i32.const 0
              i32.store offset=28
              local.get 1
              local.get 4
              i32.store8 offset=24
              local.get 1
              local.get 1
              i64.load offset=24
              i64.store offset=8
              local.get 1
              local.get 6
              i32.store offset=16
              local.get 1
              local.get 5
              i32.store offset=20
              local.get 1
              local.get 1
              i64.load offset=16
              i64.store
              local.get 0
              local.get 1
              call 36
              local.get 0
              i32.load offset=24
              local.tee 2
              local.set 3
              local.get 5
              i32.const 1
              i32.add
              local.tee 5
              local.get 2
              i32.lt_u
              br_if 0 (;@5;)
            end
          end
          local.get 4
          i32.const 1
          i32.add
          local.tee 4
          i32.const 4
          i32.ne
          br_if 0 (;@3;)
        end
        local.get 6
        i32.const 1
        i32.add
        local.tee 6
        local.get 0
        i32.load offset=8
        i32.lt_u
        br_if 0 (;@2;)
      end
    end
    local.get 1
    i32.const 32
    i32.add
    global.set 0)
  (func (;67;) (type 0) (param i32) (result i32)
    (local i32)
    i32.const -25
    local.set 1
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=24
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=28
      i32.const 1
      i32.eq
      if  ;; label = @2
        local.get 0
        call 66
        i32.const 0
        return
      end
      local.get 0
      call 65
      local.set 1
    end
    local.get 1)
  (func (;68;) (type 5) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i64 i64)
    block (result i64)  ;; label = @1
      block (result i32)  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.load
          i32.eqz
          if  ;; label = @4
            local.get 1
            i32.load8_u offset=8
            local.tee 4
            i32.eqz
            if  ;; label = @5
              local.get 1
              i32.load offset=12
              i32.const -1
              i32.add
              br 3 (;@2;)
            end
            local.get 0
            i32.load offset=16
            local.get 4
            i32.mul
            local.set 4
            local.get 1
            i32.load offset=12
            local.set 1
            local.get 3
            i32.eqz
            br_if 1 (;@3;)
            local.get 1
            local.get 4
            i32.add
            i32.const -1
            i32.add
            br 2 (;@2;)
          end
          local.get 0
          i32.load offset=20
          local.tee 4
          local.get 0
          i32.load offset=16
          local.tee 0
          i32.sub
          local.get 1
          i32.load offset=12
          local.tee 5
          i32.const -1
          i32.add
          i32.const 0
          i32.const -1
          local.get 5
          select
          local.get 3
          select
          i32.add
          local.set 3
          local.get 2
          i64.extend_i32_u
          local.set 6
          i64.const 0
          local.get 1
          i32.load8_u offset=8
          local.tee 1
          i32.const 3
          i32.eq
          br_if 2 (;@1;)
          drop
          local.get 0
          local.get 1
          i32.const 1
          i32.add
          i32.mul
          i64.extend_i32_u
          br 2 (;@1;)
        end
        local.get 4
        local.get 1
        i32.eqz
        i32.sub
      end
      local.set 3
      local.get 0
      i32.load offset=20
      local.set 4
      local.get 2
      i64.extend_i32_u
      local.set 6
      i64.const 0
    end
    local.get 3
    i32.const -1
    i32.add
    i64.extend_i32_u
    i64.add
    local.get 3
    i64.extend_i32_u
    local.get 6
    local.get 6
    i64.mul
    i64.const 32
    i64.shr_u
    i64.mul
    i64.const 32
    i64.shr_u
    i64.sub
    local.get 4
    i64.extend_i32_u
    i64.rem_u
    i32.wrap_i64)
  (func (;69;) (type 1) (param i32 i32)
    (local i32 i32)
    loop  ;; label = @1
      local.get 0
      local.get 2
      i32.const 3
      i32.shl
      local.tee 3
      i32.add
      local.get 1
      local.get 3
      i32.add
      i64.load
      call 47
      local.get 2
      i32.const 1
      i32.add
      local.tee 2
      i32.const 128
      i32.ne
      br_if 0 (;@1;)
    end)
  (func (;70;) (type 1) (param i32 i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 2048
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.eqz
    local.get 1
    i32.eqz
    i32.or
    i32.eqz
    if  ;; label = @1
      local.get 2
      i32.const 1024
      i32.add
      local.get 1
      i32.load
      local.get 1
      i32.load offset=20
      i32.const 10
      i32.shl
      i32.add
      i32.const -1024
      i32.add
      call 23
      local.get 1
      i32.load offset=24
      i32.const 2
      i32.ge_u
      if  ;; label = @2
        i32.const 1
        local.set 3
        loop  ;; label = @3
          local.get 2
          i32.const 1024
          i32.add
          local.get 1
          i32.load
          local.get 1
          i32.load offset=20
          local.tee 4
          local.get 3
          local.get 4
          i32.mul
          i32.add
          i32.const 10
          i32.shl
          i32.add
          i32.const -1024
          i32.add
          call 22
          local.get 3
          i32.const 1
          i32.add
          local.tee 3
          local.get 1
          i32.load offset=24
          i32.lt_u
          br_if 0 (;@3;)
        end
      end
      local.get 2
      local.get 2
      i32.const 1024
      i32.add
      call 69
      local.get 0
      i32.load
      local.get 0
      i32.load offset=4
      local.get 2
      i32.const 1024
      call 30
      local.get 2
      i32.const 1024
      i32.add
      i32.const 1024
      call 7
      local.get 2
      i32.const 1024
      call 7
      local.get 0
      local.get 1
      i32.load
      local.get 1
      i32.load offset=12
      call 71
    end
    local.get 2
    i32.const 2048
    i32.add
    global.set 0)
  (func (;71;) (type 4) (param i32 i32 i32)
    local.get 1
    local.get 2
    i32.const 10
    i32.shl
    local.tee 2
    call 7
    local.get 0
    i32.load offset=64
    local.tee 0
    if  ;; label = @1
      local.get 1
      local.get 2
      local.get 0
      call_indirect (type 1)
      return
    end
    local.get 1
    call 10)
  (func (;72;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i64)
    i32.const -22
    local.set 3
    block  ;; label = @1
      local.get 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 2
      i64.extend_i32_u
      i64.const 1024
      i64.mul
      local.tee 4
      i64.const 32
      i64.shr_u
      i32.wrap_i64
      br_if 0 (;@1;)
      local.get 4
      i32.wrap_i64
      local.set 2
      block  ;; label = @2
        local.get 0
        i32.load offset=60
        local.tee 0
        if  ;; label = @3
          local.get 1
          local.get 2
          local.get 0
          call_indirect (type 3)
          drop
          local.get 1
          i32.load
          local.set 3
          br 1 (;@2;)
        end
        local.get 1
        local.get 2
        call 13
        local.tee 3
        i32.store
      end
      i32.const 0
      i32.const -22
      local.get 3
      select
      local.set 3
    end
    local.get 3)
  (func (;73;) (type 0) (param i32) (result i32)
    local.get 0
    i32.const 35
    i32.add
    local.tee 0
    i32.const 35
    i32.le_u
    if  ;; label = @1
      local.get 0
      i32.const 2
      i32.shl
      i32.const 2888
      i32.add
      i32.load
      return
    end
    i32.const 2866)
  (func (;74;) (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32)
    local.get 2
    if (result i32)  ;; label = @1
      loop  ;; label = @2
        local.get 4
        local.get 1
        local.get 3
        i32.add
        i32.load8_u
        local.get 0
        local.get 3
        i32.add
        i32.load8_u
        i32.xor
        i32.or
        local.set 4
        local.get 3
        i32.const 1
        i32.add
        local.tee 3
        local.get 2
        i32.ne
        br_if 0 (;@2;)
      end
      local.get 4
      i32.const -1
      i32.add
      i32.const 8
      i32.shr_u
      i32.const 1
      i32.and
      i32.const -1
      i32.add
    else
      i32.const 0
    end)
  (func (;75;) (type 5) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 80
    i32.sub
    local.tee 4
    global.set 0
    block  ;; label = @1
      local.get 0
      i32.eqz
      if  ;; label = @2
        i32.const -32
        local.set 0
        br 1 (;@1;)
      end
      local.get 4
      local.get 0
      call 11
      local.tee 5
      i32.store offset=12
      local.get 4
      local.get 5
      i32.store offset=28
      local.get 4
      local.get 5
      call 13
      local.tee 6
      i32.store offset=24
      local.get 4
      local.get 5
      call 13
      local.tee 7
      i32.store offset=8
      i32.const 0
      local.set 5
      block  ;; label = @2
        block  ;; label = @3
          local.get 6
          i32.eqz
          local.get 7
          i32.eqz
          i32.or
          br_if 0 (;@3;)
          local.get 4
          local.get 2
          i32.store offset=20
          local.get 4
          local.get 1
          i32.store offset=16
          local.get 4
          i32.const 8
          i32.add
          local.get 0
          local.get 3
          call 61
          local.tee 0
          br_if 1 (;@2;)
          local.get 4
          i32.load offset=8
          local.set 5
          local.get 4
          local.get 4
          i32.load offset=12
          call 13
          local.tee 0
          i32.store offset=8
          local.get 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 5
          local.set 1
          local.get 4
          i32.const 8
          i32.add
          local.tee 2
          local.get 3
          call 44
          local.tee 0
          i32.eqz
          if  ;; label = @4
            i32.const -35
            i32.const 0
            local.get 1
            local.get 2
            i32.load
            local.get 2
            i32.load offset=4
            call 74
            select
            local.set 0
          end
          br 1 (;@2;)
        end
        i32.const -22
        local.set 0
      end
      local.get 4
      i32.load offset=24
      call 10
      local.get 4
      i32.load offset=8
      call 10
      local.get 5
      call 10
    end
    local.get 4
    i32.const 80
    i32.add
    global.set 0
    local.get 0)
  (func (;76;) (type 12) (param i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 80
    i32.sub
    local.tee 13
    global.set 0
    i32.const -2
    local.set 15
    block  ;; label = @1
      local.get 8
      i32.const 4
      i32.lt_u
      br_if 0 (;@1;)
      local.get 8
      call 13
      local.tee 14
      i32.eqz
      if  ;; label = @2
        i32.const -22
        local.set 15
        br 1 (;@1;)
      end
      local.get 13
      i64.const 0
      i64.store offset=40
      local.get 13
      i64.const 0
      i64.store offset=32
      local.get 13
      local.get 6
      i32.store offset=28
      local.get 13
      local.get 5
      i32.store offset=24
      local.get 13
      local.get 4
      i32.store offset=20
      local.get 13
      local.get 3
      i32.store offset=16
      local.get 13
      local.get 8
      i32.store offset=12
      local.get 13
      local.get 14
      i32.store offset=8
      local.get 13
      i32.const 0
      i32.store offset=76
      local.get 13
      i64.const 0
      i64.store offset=68 align=4
      local.get 13
      local.get 2
      i32.store offset=60
      local.get 13
      local.get 2
      i32.store offset=56
      local.get 13
      local.get 1
      i32.store offset=52
      local.get 13
      local.get 0
      i32.store offset=48
      local.get 13
      local.get 12
      i32.store offset=64
      local.get 13
      i32.const 8
      i32.add
      local.get 11
      call 44
      local.tee 15
      if  ;; label = @2
        local.get 14
        local.get 8
        call 7
        local.get 14
        call 10
        br 1 (;@1;)
      end
      local.get 7
      if  ;; label = @2
        local.get 7
        local.get 14
        local.get 8
        call 6
        drop
      end
      block  ;; label = @2
        local.get 9
        i32.eqz
        local.get 10
        i32.eqz
        i32.or
        br_if 0 (;@2;)
        local.get 9
        local.get 10
        local.get 13
        i32.const 8
        i32.add
        local.get 11
        call 60
        i32.eqz
        br_if 0 (;@2;)
        local.get 14
        local.get 8
        call 7
        local.get 9
        local.get 10
        call 7
        local.get 14
        call 10
        i32.const -31
        local.set 15
        br 1 (;@1;)
      end
      local.get 14
      local.get 8
      call 7
      local.get 14
      call 10
      i32.const 0
      local.set 15
    end
    local.get 13
    i32.const 80
    i32.add
    global.set 0
    local.get 15)
  (func (;77;) (type 3) (param i32 i32) (result i32)
    (local i32 i32 i32)
    local.get 0
    i32.eqz
    local.get 1
    i32.eqz
    i32.or
    if (result i32)  ;; label = @1
      i32.const -1
    else
      local.get 0
      i32.const -64
      i32.sub
      i32.const 0
      i32.const 176
      call 9
      drop
      local.get 0
      i32.const 1024
      i32.const 64
      call 6
      drop
      loop  ;; label = @2
        local.get 0
        local.get 2
        i32.const 3
        i32.shl
        local.tee 3
        i32.add
        local.tee 4
        local.get 1
        local.get 3
        i32.add
        i64.load align=1
        local.get 4
        i64.load
        i64.xor
        i64.store
        local.get 2
        i32.const 1
        i32.add
        local.tee 2
        i32.const 8
        i32.ne
        br_if 0 (;@2;)
      end
      local.get 0
      local.get 1
      i32.load8_u
      i32.store offset=228
      i32.const 0
    end)
  (func (;78;) (type 10)
    nop)
  (global (;0;) (mut i32) (i32.const 5247552))
  (export "d" (func 78))
  (export "e" (func 76))
  (export "f" (func 13))
  (export "g" (func 10))
  (export "h" (func 75))
  (export "i" (func 73))
  (export "j" (func 48))
  (elem (;0;) (i32.const 1) func 9 59)
  (data (;0;) (i32.const 1024) "\08\c9\bc\f3g\e6\09j;\a7\ca\84\85\aeg\bb+\f8\94\fer\f3n<\f16\1d_:\f5O\a5\d1\82\e6\ad\7fR\0eQ\1fl>+\8ch\05\9bk\bdA\fb\ab\d9\83\1fy!~\13\19\cd\e0[\00\00\00\00\01\00\00\00\02\00\00\00\03\00\00\00\04\00\00\00\05\00\00\00\06\00\00\00\07\00\00\00\08\00\00\00\09\00\00\00\0a\00\00\00\0b\00\00\00\0c\00\00\00\0d\00\00\00\0e\00\00\00\0f\00\00\00\0e\00\00\00\0a\00\00\00\04\00\00\00\08\00\00\00\09\00\00\00\0f\00\00\00\0d\00\00\00\06\00\00\00\01\00\00\00\0c\00\00\00\00\00\00\00\02\00\00\00\0b\00\00\00\07\00\00\00\05\00\00\00\03\00\00\00\0b\00\00\00\08\00\00\00\0c\00\00\00\00\00\00\00\05\00\00\00\02\00\00\00\0f\00\00\00\0d\00\00\00\0a\00\00\00\0e\00\00\00\03\00\00\00\06\00\00\00\07\00\00\00\01\00\00\00\09\00\00\00\04\00\00\00\07\00\00\00\09\00\00\00\03\00\00\00\01\00\00\00\0d\00\00\00\0c\00\00\00\0b\00\00\00\0e\00\00\00\02\00\00\00\06\00\00\00\05\00\00\00\0a\00\00\00\04\00\00\00\00\00\00\00\0f\00\00\00\08\00\00\00\09\00\00\00\00\00\00\00\05\00\00\00\07\00\00\00\02\00\00\00\04\00\00\00\0a\00\00\00\0f\00\00\00\0e\00\00\00\01\00\00\00\0b\00\00\00\0c\00\00\00\06\00\00\00\08\00\00\00\03\00\00\00\0d\00\00\00\02\00\00\00\0c\00\00\00\06\00\00\00\0a\00\00\00\00\00\00\00\0b\00\00\00\08\00\00\00\03\00\00\00\04\00\00\00\0d\00\00\00\07\00\00\00\05\00\00\00\0f\00\00\00\0e\00\00\00\01\00\00\00\09\00\00\00\0c\00\00\00\05\00\00\00\01\00\00\00\0f\00\00\00\0e\00\00\00\0d\00\00\00\04\00\00\00\0a\00\00\00\00\00\00\00\07\00\00\00\06\00\00\00\03\00\00\00\09\00\00\00\02\00\00\00\08\00\00\00\0b\00\00\00\0d\00\00\00\0b\00\00\00\07\00\00\00\0e\00\00\00\0c\00\00\00\01\00\00\00\03\00\00\00\09\00\00\00\05\00\00\00\00\00\00\00\0f\00\00\00\04\00\00\00\08\00\00\00\06\00\00\00\02\00\00\00\0a\00\00\00\06\00\00\00\0f\00\00\00\0e\00\00\00\09\00\00\00\0b\00\00\00\03\00\00\00\00\00\00\00\08\00\00\00\0c\00\00\00\02\00\00\00\0d\00\00\00\07\00\00\00\01\00\00\00\04\00\00\00\0a\00\00\00\05\00\00\00\0a\00\00\00\02\00\00\00\08\00\00\00\04\00\00\00\07\00\00\00\06\00\00\00\01\00\00\00\05\00\00\00\0f\00\00\00\0b\00\00\00\09\00\00\00\0e\00\00\00\03\00\00\00\0c\00\00\00\0d")
  (data (;1;) (i32.const 1732) "\01\00\00\00\02\00\00\00\03\00\00\00\04\00\00\00\05\00\00\00\06\00\00\00\07\00\00\00\08\00\00\00\09\00\00\00\0a\00\00\00\0b\00\00\00\0c\00\00\00\0d\00\00\00\0e\00\00\00\0f\00\00\00\0e\00\00\00\0a\00\00\00\04\00\00\00\08\00\00\00\09\00\00\00\0f\00\00\00\0d\00\00\00\06\00\00\00\01\00\00\00\0c\00\00\00\00\00\00\00\02\00\00\00\0b\00\00\00\07\00\00\00\05\00\00\00\03\00\00\00Argon2d\00argon2d\00Argon2i\00argon2i\00Argon2id\00argon2id\00OK\00Output pointer is NULL\00Output is too short\00Output is too long\00Password is too short\00Password is too long\00Salt is too short\00Salt is too long\00Associated data is too short\00Associated data is too long\00Secret is too short\00Secret is too long\00Time cost is too small\00Time cost is too large\00Memory cost is too small\00Memory cost is too large\00Too few lanes\00Too many lanes\00Password pointer is NULL, but password length is not 0\00Salt pointer is NULL, but salt length is not 0\00Secret pointer is NULL, but secret length is not 0\00Associated data pointer is NULL, but ad length is not 0\00Memory allocation error\00The free memory callback is NULL\00The allocate memory callback is NULL\00Argon2_Context context is NULL\00There is no such version of Argon2\00Output pointer mismatch\00Not enough threads\00Too many threads\00Missing arguments\00Encoding failed\00Decoding failed\00Threading failure\00Some of encoded parameters are too long or too short\00The password does not match the supplied hash\00Unknown error code\00\00\00\00\04\0b\00\00\cf\0a\00\00\bd\0a\00\00\ad\0a\00\00\9d\0a\00\00\8b\0a\00\00z\0a\00\00g\0a\00\00O\0a\00\00,\0a\00\00\0d\0a\00\00\e8\09\00\00\c7\09\00\00\af\09\00\00w\09\00\00D\09\00\00\15\09\00\00\de\08\00\00\cf\08\00\00\c1\08\00\00\a8\08\00\00\8f\08\00\00x\08\00\00a\08\00\00N\08\00\00:\08\00\00\1e\08\00\00\01\08\00\00\f0\07\00\00\de\07\00\00\c9\07\00\00\b3\07\00\00\a0\07\00\00\8c\07\00\00u\07\00\00r\07\00\00\01\00\00\00$v=\00$m=\00,t=\00,p=\00%lu")
  (data (;2;) (i32.const 3092) "\02")
  (data (;3;) (i32.const 3131) "\ff\ff\ff\ff\ff")
  (data (;4;) (i32.const 3200) "-+   0X0x\00(null)")
  (data (;5;) (i32.const 3232) "\11\00\0a\00\11\11\11\00\00\00\00\05\00\00\00\00\00\00\09\00\00\00\00\0b\00\00\00\00\00\00\00\00\11\00\0f\0a\11\11\11\03\0a\07\00\01\00\09\0b\0b\00\00\09\06\0b\00\00\0b\00\06\11\00\00\00\11\11\11")
  (data (;6;) (i32.const 3313) "\0b\00\00\00\00\00\00\00\00\11\00\0a\0a\11\11\11\00\0a\00\00\02\00\09\0b\00\00\00\09\00\0b\00\00\0b")
  (data (;7;) (i32.const 3371) "\0c")
  (data (;8;) (i32.const 3383) "\0c\00\00\00\00\0c\00\00\00\00\09\0c\00\00\00\00\00\0c\00\00\0c")
  (data (;9;) (i32.const 3429) "\0e")
  (data (;10;) (i32.const 3441) "\0d\00\00\00\04\0d\00\00\00\00\09\0e\00\00\00\00\00\0e\00\00\0e")
  (data (;11;) (i32.const 3487) "\10")
  (data (;12;) (i32.const 3499) "\0f\00\00\00\00\0f\00\00\00\00\09\10\00\00\00\00\00\10\00\00\10\00\00\12\00\00\00\12\12\12")
  (data (;13;) (i32.const 3554) "\12\00\00\00\12\12\12\00\00\00\00\00\00\09")
  (data (;14;) (i32.const 3603) "\0b")
  (data (;15;) (i32.const 3615) "\0a\00\00\00\00\0a\00\00\00\00\09\0b\00\00\00\00\00\0b\00\00\0b")
  (data (;16;) (i32.const 3661) "\0c")
  (data (;17;) (i32.const 3673) "\0c\00\00\00\00\0c\00\00\00\00\09\0c\00\00\00\00\00\0c\00\00\0c\00\000123456789ABCDEF")
  (data (;18;) (i32.const 3712) "\01")
  (data (;19;) (i32.const 3892) "\98\0f"))
