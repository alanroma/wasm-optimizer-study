[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    5.641e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000147071 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.268e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000264805 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000368492 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 1.9066e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.00495292 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00184636 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00162552 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000320364 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00159886 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00040245 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.00742906 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0189879 seconds.
[PassRunner] (final validation)
