[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.0374506 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 3.8742e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.0592048 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0197354 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0279005 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00253883 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00958643 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00471685 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.174806 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0102519 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0681211 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0230061 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00792651 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00941734 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                   0.00839055 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0123357 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0271528 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0161207 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00374644 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0113117 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00355023 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.016828 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.00470221 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00813524 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0167928 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00175081 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00664194 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.0439525 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0069778 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00765451 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0138238 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.054631 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0389793 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00315152 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   2.0167e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00158353 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.0027681 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      0.000375456 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00174189 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0332832 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.801103 seconds.
[PassRunner] (final validation)
