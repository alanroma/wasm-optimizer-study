[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    6.1343e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00105633 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    5.6403e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00403716 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00637931 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 5.8152e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.0741702 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0254386 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0356031 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00342178 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0124886 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00595138 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.168722 seconds.
[PassRunner] (final validation)
