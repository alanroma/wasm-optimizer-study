/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// The Module object: Our interface to the outside world. We import
// and export values on it. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to check if Module already exists (e.g. case 3 above).
// Substitution will be replaced with actual code on later stage of the build,
// this way Closure Compiler will not mangle it (e.g. case 4. above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module = typeof Module !== 'undefined' ? Module : {};



// --pre-jses are emitted after the Module integration code, so that they can
// refer to Module (if they choose; they can also define Module)
// {{PRE_JSES}}

// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
var key;
for (key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}

var arguments_ = [];
var thisProgram = './this.program';
var quit_ = function(status, toThrow) {
  throw toThrow;
};

// Determine the runtime environment we are in. You can customize this by
// setting the ENVIRONMENT setting at compile time (see settings.js).

var ENVIRONMENT_IS_WEB = false;
var ENVIRONMENT_IS_WORKER = false;
var ENVIRONMENT_IS_NODE = false;
var ENVIRONMENT_IS_SHELL = false;
ENVIRONMENT_IS_WEB = typeof window === 'object';
ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
// N.b. Electron.js environment is simultaneously a NODE-environment, but
// also a web environment.
ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof process.versions === 'object' && typeof process.versions.node === 'string';
ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;




// `/` should be present at the end if `scriptDirectory` is not empty
var scriptDirectory = '';
function locateFile(path) {
  if (Module['locateFile']) {
    return Module['locateFile'](path, scriptDirectory);
  }
  return scriptDirectory + path;
}

// Hooks that are implemented differently in different runtime environments.
var read_,
    readAsync,
    readBinary,
    setWindowTitle;

var nodeFS;
var nodePath;

if (ENVIRONMENT_IS_NODE) {
  if (ENVIRONMENT_IS_WORKER) {
    scriptDirectory = require('path').dirname(scriptDirectory) + '/';
  } else {
    scriptDirectory = __dirname + '/';
  }


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

  read_ = function shell_read(filename, binary) {
    if (!nodeFS) nodeFS = require('fs');
    if (!nodePath) nodePath = require('path');
    filename = nodePath['normalize'](filename);
    return nodeFS['readFileSync'](filename, binary ? null : 'utf8');
  };

  readBinary = function readBinary(filename) {
    var ret = read_(filename, true);
    if (!ret.buffer) {
      ret = new Uint8Array(ret);
    }
    assert(ret.buffer);
    return ret;
  };




  if (process['argv'].length > 1) {
    thisProgram = process['argv'][1].replace(/\\/g, '/');
  }

  arguments_ = process['argv'].slice(2);

  if (typeof module !== 'undefined') {
    module['exports'] = Module;
  }

  process['on']('uncaughtException', function(ex) {
    // suppress ExitStatus exceptions from showing an error
    if (!(ex instanceof ExitStatus)) {
      throw ex;
    }
  });

  process['on']('unhandledRejection', abort);

  quit_ = function(status) {
    process['exit'](status);
  };

  Module['inspect'] = function () { return '[Emscripten Module object]'; };



} else
if (ENVIRONMENT_IS_SHELL) {


  if (typeof read != 'undefined') {
    read_ = function shell_read(f) {
      return read(f);
    };
  }

  readBinary = function readBinary(f) {
    var data;
    if (typeof readbuffer === 'function') {
      return new Uint8Array(readbuffer(f));
    }
    data = read(f, 'binary');
    assert(typeof data === 'object');
    return data;
  };

  if (typeof scriptArgs != 'undefined') {
    arguments_ = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    arguments_ = arguments;
  }

  if (typeof quit === 'function') {
    quit_ = function(status) {
      quit(status);
    };
  }

  if (typeof print !== 'undefined') {
    // Prefer to use print/printErr where they exist, as they usually work better.
    if (typeof console === 'undefined') console = /** @type{!Console} */({});
    console.log = /** @type{!function(this:Console, ...*): undefined} */ (print);
    console.warn = console.error = /** @type{!function(this:Console, ...*): undefined} */ (typeof printErr !== 'undefined' ? printErr : print);
  }


} else

// Note that this includes Node.js workers when relevant (pthreads is enabled).
// Node.js workers are detected as a combination of ENVIRONMENT_IS_WORKER and
// ENVIRONMENT_IS_NODE.
if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  if (ENVIRONMENT_IS_WORKER) { // Check worker, not web, since window could be polyfilled
    scriptDirectory = self.location.href;
  } else if (document.currentScript) { // web
    scriptDirectory = document.currentScript.src;
  }
  // blob urls look like blob:http://site.com/etc/etc and we cannot infer anything from them.
  // otherwise, slice off the final part of the url to find the script directory.
  // if scriptDirectory does not contain a slash, lastIndexOf will return -1,
  // and scriptDirectory will correctly be replaced with an empty string.
  if (scriptDirectory.indexOf('blob:') !== 0) {
    scriptDirectory = scriptDirectory.substr(0, scriptDirectory.lastIndexOf('/')+1);
  } else {
    scriptDirectory = '';
  }


  // Differentiate the Web Worker from the Node Worker case, as reading must
  // be done differently.
  {


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

  read_ = function shell_read(url) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, false);
      xhr.send(null);
      return xhr.responseText;
  };

  if (ENVIRONMENT_IS_WORKER) {
    readBinary = function readBinary(url) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, false);
        xhr.responseType = 'arraybuffer';
        xhr.send(null);
        return new Uint8Array(/** @type{!ArrayBuffer} */(xhr.response));
    };
  }

  readAsync = function readAsync(url, onload, onerror) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function xhr_onload() {
      if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
        onload(xhr.response);
        return;
      }
      onerror();
    };
    xhr.onerror = onerror;
    xhr.send(null);
  };




  }

  setWindowTitle = function(title) { document.title = title };
} else
{
}


// Set up the out() and err() hooks, which are how we can print to stdout or
// stderr, respectively.
var out = Module['print'] || console.log.bind(console);
var err = Module['printErr'] || console.warn.bind(console);

// Merge back in the overrides
for (key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}
// Free the object hierarchy contained in the overrides, this lets the GC
// reclaim data used e.g. in memoryInitializerRequest, which is a large typed array.
moduleOverrides = null;

// Emit code to handle expected values on the Module object. This applies Module.x
// to the proper local x. This has two benefits: first, we only emit it if it is
// expected to arrive, and second, by using a local everywhere else that can be
// minified.
if (Module['arguments']) arguments_ = Module['arguments'];
if (Module['thisProgram']) thisProgram = Module['thisProgram'];
if (Module['quit']) quit_ = Module['quit'];

// perform assertions in shell.js after we set up out() and err(), as otherwise if an assertion fails it cannot print the message



/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// {{PREAMBLE_ADDITIONS}}

var STACK_ALIGN = 16;

function dynamicAlloc(size) {
  var ret = HEAP32[DYNAMICTOP_PTR>>2];
  var end = (ret + size + 15) & -16;
  HEAP32[DYNAMICTOP_PTR>>2] = end;
  return ret;
}

function alignMemory(size, factor) {
  if (!factor) factor = STACK_ALIGN; // stack alignment (16-byte) by default
  return Math.ceil(size / factor) * factor;
}

function getNativeTypeSize(type) {
  switch (type) {
    case 'i1': case 'i8': return 1;
    case 'i16': return 2;
    case 'i32': return 4;
    case 'i64': return 8;
    case 'float': return 4;
    case 'double': return 8;
    default: {
      if (type[type.length-1] === '*') {
        return 4; // A pointer
      } else if (type[0] === 'i') {
        var bits = Number(type.substr(1));
        assert(bits % 8 === 0, 'getNativeTypeSize invalid bits ' + bits + ', type ' + type);
        return bits / 8;
      } else {
        return 0;
      }
    }
  }
}

function warnOnce(text) {
  if (!warnOnce.shown) warnOnce.shown = {};
  if (!warnOnce.shown[text]) {
    warnOnce.shown[text] = 1;
    err(text);
  }
}





/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */


// Wraps a JS function as a wasm function with a given signature.
function convertJsFunctionToWasm(func, sig) {

  // If the type reflection proposal is available, use the new
  // "WebAssembly.Function" constructor.
  // Otherwise, construct a minimal wasm module importing the JS function and
  // re-exporting it.
  if (typeof WebAssembly.Function === "function") {
    var typeNames = {
      'i': 'i32',
      'j': 'i64',
      'f': 'f32',
      'd': 'f64'
    };
    var type = {
      parameters: [],
      results: sig[0] == 'v' ? [] : [typeNames[sig[0]]]
    };
    for (var i = 1; i < sig.length; ++i) {
      type.parameters.push(typeNames[sig[i]]);
    }
    return new WebAssembly.Function(type, func);
  }

  // The module is static, with the exception of the type section, which is
  // generated based on the signature passed in.
  var typeSection = [
    0x01, // id: section,
    0x00, // length: 0 (placeholder)
    0x01, // count: 1
    0x60, // form: func
  ];
  var sigRet = sig.slice(0, 1);
  var sigParam = sig.slice(1);
  var typeCodes = {
    'i': 0x7f, // i32
    'j': 0x7e, // i64
    'f': 0x7d, // f32
    'd': 0x7c, // f64
  };

  // Parameters, length + signatures
  typeSection.push(sigParam.length);
  for (var i = 0; i < sigParam.length; ++i) {
    typeSection.push(typeCodes[sigParam[i]]);
  }

  // Return values, length + signatures
  // With no multi-return in MVP, either 0 (void) or 1 (anything else)
  if (sigRet == 'v') {
    typeSection.push(0x00);
  } else {
    typeSection = typeSection.concat([0x01, typeCodes[sigRet]]);
  }

  // Write the overall length of the type section back into the section header
  // (excepting the 2 bytes for the section id and length)
  typeSection[1] = typeSection.length - 2;

  // Rest of the module is static
  var bytes = new Uint8Array([
    0x00, 0x61, 0x73, 0x6d, // magic ("\0asm")
    0x01, 0x00, 0x00, 0x00, // version: 1
  ].concat(typeSection, [
    0x02, 0x07, // import section
      // (import "e" "f" (func 0 (type 0)))
      0x01, 0x01, 0x65, 0x01, 0x66, 0x00, 0x00,
    0x07, 0x05, // export section
      // (export "f" (func 0 (type 0)))
      0x01, 0x01, 0x66, 0x00, 0x00,
  ]));

   // We can compile this wasm module synchronously because it is very small.
  // This accepts an import (at "e.f"), that it reroutes to an export (at "f")
  var module = new WebAssembly.Module(bytes);
  var instance = new WebAssembly.Instance(module, {
    'e': {
      'f': func
    }
  });
  var wrappedFunc = instance.exports['f'];
  return wrappedFunc;
}

var freeTableIndexes = [];

// Weak map of functions in the table to their indexes, created on first use.
var functionsInTableMap;

// Add a wasm function to the table.
function addFunctionWasm(func, sig) {
  var table = wasmTable;

  // Check if the function is already in the table, to ensure each function
  // gets a unique index. First, create the map if this is the first use.
  if (!functionsInTableMap) {
    functionsInTableMap = new WeakMap();
    for (var i = 0; i < table.length; i++) {
      var item = table.get(i);
      // Ignore null values.
      if (item) {
        functionsInTableMap.set(item, i);
      }
    }
  }
  if (functionsInTableMap.has(func)) {
    return functionsInTableMap.get(func);
  }

  // It's not in the table, add it now.


  var ret;
  // Reuse a free index if there is one, otherwise grow.
  if (freeTableIndexes.length) {
    ret = freeTableIndexes.pop();
  } else {
    ret = table.length;
    // Grow the table
    try {
      table.grow(1);
    } catch (err) {
      if (!(err instanceof RangeError)) {
        throw err;
      }
      throw 'Unable to grow wasm table. Set ALLOW_TABLE_GROWTH.';
    }
  }

  // Set the new value.
  try {
    // Attempting to call this with JS function will cause of table.set() to fail
    table.set(ret, func);
  } catch (err) {
    if (!(err instanceof TypeError)) {
      throw err;
    }
    var wrapped = convertJsFunctionToWasm(func, sig);
    table.set(ret, wrapped);
  }

  functionsInTableMap.set(func, ret);

  return ret;
}

function removeFunctionWasm(index) {
  functionsInTableMap.delete(wasmTable.get(index));
  freeTableIndexes.push(index);
}

// 'sig' parameter is required for the llvm backend but only when func is not
// already a WebAssembly function.
function addFunction(func, sig) {

  return addFunctionWasm(func, sig);
}

function removeFunction(index) {
  removeFunctionWasm(index);
}



var funcWrappers = {};

function getFuncWrapper(func, sig) {
  if (!func) return; // on null pointer, return undefined
  assert(sig);
  if (!funcWrappers[sig]) {
    funcWrappers[sig] = {};
  }
  var sigCache = funcWrappers[sig];
  if (!sigCache[func]) {
    // optimize away arguments usage in common cases
    if (sig.length === 1) {
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func);
      };
    } else if (sig.length === 2) {
      sigCache[func] = function dynCall_wrapper(arg) {
        return dynCall(sig, func, [arg]);
      };
    } else {
      // general case
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func, Array.prototype.slice.call(arguments));
      };
    }
  }
  return sigCache[func];
}


/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




function makeBigInt(low, high, unsigned) {
  return unsigned ? ((+((low>>>0)))+((+((high>>>0)))*4294967296.0)) : ((+((low>>>0)))+((+((high|0)))*4294967296.0));
}

/** @param {Array=} args */
function dynCall(sig, ptr, args) {
  if (args && args.length) {
    return Module['dynCall_' + sig].apply(null, [ptr].concat(args));
  } else {
    return Module['dynCall_' + sig].call(null, ptr);
  }
}

var tempRet0 = 0;

var setTempRet0 = function(value) {
  tempRet0 = value;
};

var getTempRet0 = function() {
  return tempRet0;
};


// The address globals begin at. Very low in memory, for code size and optimization opportunities.
// Above 0 is static memory, starting with globals.
// Then the stack.
// Then 'dynamic' memory for sbrk.
var GLOBAL_BASE = 1024;



/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// === Preamble library stuff ===

// Documentation for the public APIs defined in this file must be updated in:
//    site/source/docs/api_reference/preamble.js.rst
// A prebuilt local version of the documentation is available at:
//    site/build/text/docs/api_reference/preamble.js.txt
// You can also build docs locally as HTML or other formats in site/
// An online HTML version (which may be of a different version of Emscripten)
//    is up at http://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html


var wasmBinary;if (Module['wasmBinary']) wasmBinary = Module['wasmBinary'];
var noExitRuntime;if (Module['noExitRuntime']) noExitRuntime = Module['noExitRuntime'];


if (typeof WebAssembly !== 'object') {
  err('no native wasm support detected');
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// In MINIMAL_RUNTIME, setValue() and getValue() are only available when building with safe heap enabled, for heap safety checking.
// In traditional runtime, setValue() and getValue() are always available (although their use is highly discouraged due to perf penalties)

/** @param {number} ptr
    @param {number} value
    @param {string} type
    @param {number|boolean=} noSafe */
function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[((ptr)>>0)]=value; break;
      case 'i8': HEAP8[((ptr)>>0)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math_min((+(Math_floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}

/** @param {number} ptr
    @param {string} type
    @param {number|boolean=} noSafe */
function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[((ptr)>>0)];
      case 'i8': return HEAP8[((ptr)>>0)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for getValue: ' + type);
    }
  return null;
}






// Wasm globals

var wasmMemory;

// In fastcomp asm.js, we don't need a wasm Table at all.
// In the wasm backend, we polyfill the WebAssembly object,
// so this creates a (non-native-wasm) table for us.
var wasmTable = new WebAssembly.Table({
  'initial': 91,
  'maximum': 91 + 0,
  'element': 'anyfunc'
});


//========================================
// Runtime essentials
//========================================

// whether we are quitting the application. no code should run after this.
// set in exit() and abort()
var ABORT = false;

// set by exit() and abort().  Passed to 'onExit' handler.
// NOTE: This is also used as the process return code code in shell environments
// but only when noExitRuntime is false.
var EXITSTATUS = 0;

/** @type {function(*, string=)} */
function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}

// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  var func = Module['_' + ident]; // closure exported function
  assert(func, 'Cannot call unknown function ' + ident + ', make sure it is exported');
  return func;
}

// C calling interface.
/** @param {string|null=} returnType
    @param {Array=} argTypes
    @param {Arguments|Array=} args
    @param {Object=} opts */
function ccall(ident, returnType, argTypes, args, opts) {
  // For fast lookup of conversion functions
  var toC = {
    'string': function(str) {
      var ret = 0;
      if (str !== null && str !== undefined && str !== 0) { // null string
        // at most 4 bytes per UTF-8 code point, +1 for the trailing '\0'
        var len = (str.length << 2) + 1;
        ret = stackAlloc(len);
        stringToUTF8(str, ret, len);
      }
      return ret;
    },
    'array': function(arr) {
      var ret = stackAlloc(arr.length);
      writeArrayToMemory(arr, ret);
      return ret;
    }
  };

  function convertReturnValue(ret) {
    if (returnType === 'string') return UTF8ToString(ret);
    if (returnType === 'boolean') return Boolean(ret);
    return ret;
  }

  var func = getCFunc(ident);
  var cArgs = [];
  var stack = 0;
  if (args) {
    for (var i = 0; i < args.length; i++) {
      var converter = toC[argTypes[i]];
      if (converter) {
        if (stack === 0) stack = stackSave();
        cArgs[i] = converter(args[i]);
      } else {
        cArgs[i] = args[i];
      }
    }
  }
  var ret = func.apply(null, cArgs);

  ret = convertReturnValue(ret);
  if (stack !== 0) stackRestore(stack);
  return ret;
}

/** @param {string=} returnType
    @param {Array=} argTypes
    @param {Object=} opts */
function cwrap(ident, returnType, argTypes, opts) {
  argTypes = argTypes || [];
  // When the function takes numbers and returns a number, we can just return
  // the original function
  var numericArgs = argTypes.every(function(type){ return type === 'number'});
  var numericRet = returnType !== 'string';
  if (numericRet && numericArgs && !opts) {
    return getCFunc(ident);
  }
  return function() {
    return ccall(ident, returnType, argTypes, arguments, opts);
  }
}

var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_DYNAMIC = 2; // Cannot be freed except through sbrk
var ALLOC_NONE = 3; // Do not allocate

// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
/** @type {function((TypedArray|Array<number>|number), string, number, number=)} */
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }

  var singleType = typeof types === 'string' ? types : null;

  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [_malloc,
    stackAlloc,
    dynamicAlloc][allocator](Math.max(size, singleType ? 1 : types.length));
  }

  if (zeroinit) {
    var stop;
    ptr = ret;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)>>0)]=0;
    }
    return ret;
  }

  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(/** @type {!Uint8Array} */ (slab), ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }

  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];

    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }

    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later

    setValue(ret+i, curr, type);

    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }

  return ret;
}

// Allocate memory during any stage of startup - static memory early on, dynamic memory later, malloc when ready
function getMemory(size) {
  if (!runtimeInitialized) return dynamicAlloc(size);
  return _malloc(size);
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// runtime_strings.js: Strings related runtime functions that are part of both MINIMAL_RUNTIME and regular runtime.

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the given array that contains uint8 values, returns
// a copy of that string as a Javascript String object.

var UTF8Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf8') : undefined;

/**
 * @param {number} idx
 * @param {number=} maxBytesToRead
 * @return {string}
 */
function UTF8ArrayToString(heap, idx, maxBytesToRead) {
  var endIdx = idx + maxBytesToRead;
  var endPtr = idx;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  // (As a tiny code save trick, compare endPtr against endIdx using a negation, so that undefined means Infinity)
  while (heap[endPtr] && !(endPtr >= endIdx)) ++endPtr;

  if (endPtr - idx > 16 && heap.subarray && UTF8Decoder) {
    return UTF8Decoder.decode(heap.subarray(idx, endPtr));
  } else {
    var str = '';
    // If building with TextDecoder, we have already computed the string length above, so test loop end condition against that
    while (idx < endPtr) {
      // For UTF8 byte structure, see:
      // http://en.wikipedia.org/wiki/UTF-8#Description
      // https://www.ietf.org/rfc/rfc2279.txt
      // https://tools.ietf.org/html/rfc3629
      var u0 = heap[idx++];
      if (!(u0 & 0x80)) { str += String.fromCharCode(u0); continue; }
      var u1 = heap[idx++] & 63;
      if ((u0 & 0xE0) == 0xC0) { str += String.fromCharCode(((u0 & 31) << 6) | u1); continue; }
      var u2 = heap[idx++] & 63;
      if ((u0 & 0xF0) == 0xE0) {
        u0 = ((u0 & 15) << 12) | (u1 << 6) | u2;
      } else {
        u0 = ((u0 & 7) << 18) | (u1 << 12) | (u2 << 6) | (heap[idx++] & 63);
      }

      if (u0 < 0x10000) {
        str += String.fromCharCode(u0);
      } else {
        var ch = u0 - 0x10000;
        str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
      }
    }
  }
  return str;
}

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the emscripten HEAP, returns a
// copy of that string as a Javascript String object.
// maxBytesToRead: an optional length that specifies the maximum number of bytes to read. You can omit
//                 this parameter to scan the string until the first \0 byte. If maxBytesToRead is
//                 passed, and the string at [ptr, ptr+maxBytesToReadr[ contains a null byte in the
//                 middle, then the string will cut short at that byte index (i.e. maxBytesToRead will
//                 not produce a string of exact length [ptr, ptr+maxBytesToRead[)
//                 N.B. mixing frequent uses of UTF8ToString() with and without maxBytesToRead may
//                 throw JS JIT optimizations off, so it is worth to consider consistently using one
//                 style or the other.
/**
 * @param {number} ptr
 * @param {number=} maxBytesToRead
 * @return {string}
 */
function UTF8ToString(ptr, maxBytesToRead) {
  return ptr ? UTF8ArrayToString(HEAPU8, ptr, maxBytesToRead) : '';
}

// Copies the given Javascript String object 'str' to the given byte array at address 'outIdx',
// encoded in UTF8 form and null-terminated. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   heap: the array to copy to. Each index in this array is assumed to be one 8-byte element.
//   outIdx: The starting offset in the array to begin the copying.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array.
//                    This count should include the null terminator,
//                    i.e. if maxBytesToWrite=1, only the null terminator will be written and nothing else.
//                    maxBytesToWrite=0 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8Array(str, heap, outIdx, maxBytesToWrite) {
  if (!(maxBytesToWrite > 0)) // Parameter maxBytesToWrite is not optional. Negative values, 0, null, undefined and false each don't write out any bytes.
    return 0;

  var startIdx = outIdx;
  var endIdx = outIdx + maxBytesToWrite - 1; // -1 for string null terminator.
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) {
      var u1 = str.charCodeAt(++i);
      u = 0x10000 + ((u & 0x3FF) << 10) | (u1 & 0x3FF);
    }
    if (u <= 0x7F) {
      if (outIdx >= endIdx) break;
      heap[outIdx++] = u;
    } else if (u <= 0x7FF) {
      if (outIdx + 1 >= endIdx) break;
      heap[outIdx++] = 0xC0 | (u >> 6);
      heap[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0xFFFF) {
      if (outIdx + 2 >= endIdx) break;
      heap[outIdx++] = 0xE0 | (u >> 12);
      heap[outIdx++] = 0x80 | ((u >> 6) & 63);
      heap[outIdx++] = 0x80 | (u & 63);
    } else {
      if (outIdx + 3 >= endIdx) break;
      heap[outIdx++] = 0xF0 | (u >> 18);
      heap[outIdx++] = 0x80 | ((u >> 12) & 63);
      heap[outIdx++] = 0x80 | ((u >> 6) & 63);
      heap[outIdx++] = 0x80 | (u & 63);
    }
  }
  // Null-terminate the pointer to the buffer.
  heap[outIdx] = 0;
  return outIdx - startIdx;
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF8 form. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8(str, outPtr, maxBytesToWrite) {
  return stringToUTF8Array(str, HEAPU8,outPtr, maxBytesToWrite);
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF8 byte array, EXCLUDING the null terminator byte.
function lengthBytesUTF8(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) ++len;
    else if (u <= 0x7FF) len += 2;
    else if (u <= 0xFFFF) len += 3;
    else len += 4;
  }
  return len;
}



/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// runtime_strings_extra.js: Strings related runtime functions that are available only in regular runtime.

// Given a pointer 'ptr' to a null-terminated ASCII-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function AsciiToString(ptr) {
  var str = '';
  while (1) {
    var ch = HEAPU8[((ptr++)>>0)];
    if (!ch) return str;
    str += String.fromCharCode(ch);
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in ASCII form. The copy will require at most str.length+1 bytes of space in the HEAP.

function stringToAscii(str, outPtr) {
  return writeAsciiToMemory(str, outPtr, false);
}

// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

var UTF16Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf-16le') : undefined;

function UTF16ToString(ptr, maxBytesToRead) {
  var endPtr = ptr;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  var idx = endPtr >> 1;
  var maxIdx = idx + maxBytesToRead / 2;
  // If maxBytesToRead is not passed explicitly, it will be undefined, and this
  // will always evaluate to true. This saves on code size.
  while (!(idx >= maxIdx) && HEAPU16[idx]) ++idx;
  endPtr = idx << 1;

  if (endPtr - ptr > 32 && UTF16Decoder) {
    return UTF16Decoder.decode(HEAPU8.subarray(ptr, endPtr));
  } else {
    var i = 0;

    var str = '';
    while (1) {
      var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
      if (codeUnit == 0 || i == maxBytesToRead / 2) return str;
      ++i;
      // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
      str += String.fromCharCode(codeUnit);
    }
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16 form. The copy will require at most str.length*4+2 bytes of space in the HEAP.
// Use the function lengthBytesUTF16() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=2, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<2 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF16(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 2) return 0;
  maxBytesToWrite -= 2; // Null terminator.
  var startPtr = outPtr;
  var numCharsToWrite = (maxBytesToWrite < str.length*2) ? (maxBytesToWrite / 2) : str.length;
  for (var i = 0; i < numCharsToWrite; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[((outPtr)>>1)]=codeUnit;
    outPtr += 2;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[((outPtr)>>1)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF16(str) {
  return str.length*2;
}

function UTF32ToString(ptr, maxBytesToRead) {
  var i = 0;

  var str = '';
  // If maxBytesToRead is not passed explicitly, it will be undefined, and this
  // will always evaluate to true. This saves on code size.
  while (!(i >= maxBytesToRead / 4)) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0) break;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
  return str;
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32 form. The copy will require at most str.length*4+4 bytes of space in the HEAP.
// Use the function lengthBytesUTF32() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=4, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<4 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF32(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 4) return 0;
  var startPtr = outPtr;
  var endPtr = startPtr + maxBytesToWrite - 4;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++i);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[((outPtr)>>2)]=codeUnit;
    outPtr += 4;
    if (outPtr + 4 > endPtr) break;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[((outPtr)>>2)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF32(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i);
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) ++i; // possibly a lead surrogate, so skip over the tail surrogate.
    len += 4;
  }

  return len;
}

// Allocate heap space for a JS string, and write it there.
// It is the responsibility of the caller to free() that memory.
function allocateUTF8(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = _malloc(size);
  if (ret) stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

// Allocate stack space for a JS string, and write it there.
function allocateUTF8OnStack(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = stackAlloc(size);
  stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

// Deprecated: This function should not be called because it is unsafe and does not provide
// a maximum length limit of how many bytes it is allowed to write. Prefer calling the
// function stringToUTF8Array() instead, which takes in a maximum length that can be used
// to be secure from out of bounds writes.
/** @deprecated
    @param {boolean=} dontAddNull */
function writeStringToMemory(string, buffer, dontAddNull) {
  warnOnce('writeStringToMemory is deprecated and should not be called! Use stringToUTF8() instead!');

  var /** @type {number} */ lastChar, /** @type {number} */ end;
  if (dontAddNull) {
    // stringToUTF8Array always appends null. If we don't want to do that, remember the
    // character that existed at the location where the null will be placed, and restore
    // that after the write (below).
    end = buffer + lengthBytesUTF8(string);
    lastChar = HEAP8[end];
  }
  stringToUTF8(string, buffer, Infinity);
  if (dontAddNull) HEAP8[end] = lastChar; // Restore the value under the null character.
}

function writeArrayToMemory(array, buffer) {
  HEAP8.set(array, buffer);
}

/** @param {boolean=} dontAddNull */
function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; ++i) {
    HEAP8[((buffer++)>>0)]=str.charCodeAt(i);
  }
  // Null-terminate the pointer to the HEAP.
  if (!dontAddNull) HEAP8[((buffer)>>0)]=0;
}



// Memory management

var PAGE_SIZE = 16384;
var WASM_PAGE_SIZE = 65536;
var ASMJS_PAGE_SIZE = 16777216;

function alignUp(x, multiple) {
  if (x % multiple > 0) {
    x += multiple - (x % multiple);
  }
  return x;
}

var HEAP,
/** @type {ArrayBuffer} */
  buffer,
/** @type {Int8Array} */
  HEAP8,
/** @type {Uint8Array} */
  HEAPU8,
/** @type {Int16Array} */
  HEAP16,
/** @type {Uint16Array} */
  HEAPU16,
/** @type {Int32Array} */
  HEAP32,
/** @type {Uint32Array} */
  HEAPU32,
/** @type {Float32Array} */
  HEAPF32,
/** @type {Float64Array} */
  HEAPF64;

function updateGlobalBufferAndViews(buf) {
  buffer = buf;
  Module['HEAP8'] = HEAP8 = new Int8Array(buf);
  Module['HEAP16'] = HEAP16 = new Int16Array(buf);
  Module['HEAP32'] = HEAP32 = new Int32Array(buf);
  Module['HEAPU8'] = HEAPU8 = new Uint8Array(buf);
  Module['HEAPU16'] = HEAPU16 = new Uint16Array(buf);
  Module['HEAPU32'] = HEAPU32 = new Uint32Array(buf);
  Module['HEAPF32'] = HEAPF32 = new Float32Array(buf);
  Module['HEAPF64'] = HEAPF64 = new Float64Array(buf);
}

var STATIC_BASE = 1024,
    STACK_BASE = 5247344,
    STACKTOP = STACK_BASE,
    STACK_MAX = 4464,
    DYNAMIC_BASE = 5247344,
    DYNAMICTOP_PTR = 4304;



var TOTAL_STACK = 5242880;

var INITIAL_INITIAL_MEMORY = Module['INITIAL_MEMORY'] || 16777216;




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */






/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback(Module); // Pass the module as the first argument.
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Module['dynCall_v'](func);
      } else {
        Module['dynCall_vi'](func, callback.arg);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}

var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the main() is called

var runtimeInitialized = false;
var runtimeExited = false;


function preRun() {

  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }

  callRuntimeCallbacks(__ATPRERUN__);
}

function initRuntime() {
  runtimeInitialized = true;
  
  callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
  
  callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
  runtimeExited = true;
}

function postRun() {

  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }

  callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}

function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}

function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}

function addOnExit(cb) {
}

function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}

/** @param {number|boolean=} ignore */
function unSign(value, bits, ignore) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
/** @param {number|boolean=} ignore */
function reSign(value, bits, ignore) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/imul

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/fround

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/clz32

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc


var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_round = Math.round;
var Math_min = Math.min;
var Math_max = Math.max;
var Math_clz32 = Math.clz32;
var Math_trunc = Math.trunc;



// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// Module.preRun (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled

function getUniqueRunDependency(id) {
  return id;
}

function addRunDependency(id) {
  runDependencies++;

  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }

}

function removeRunDependency(id) {
  runDependencies--;

  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }

  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}

Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data

/** @param {string|number=} what */
function abort(what) {
  if (Module['onAbort']) {
    Module['onAbort'](what);
  }

  what += '';
  out(what);
  err(what);

  ABORT = true;
  EXITSTATUS = 1;

  what = 'abort(' + what + '). Build with -s ASSERTIONS=1 for more info.';

  // Throw a wasm runtime error, because a JS error might be seen as a foreign
  // exception, which means we'd run destructors on it. We need the error to
  // simply make the program stop.
  throw new WebAssembly.RuntimeError(what);
}


var memoryInitializer = null;


/**
 * @license
 * Copyright 2015 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */







/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

function hasPrefix(str, prefix) {
  return String.prototype.startsWith ?
      str.startsWith(prefix) :
      str.indexOf(prefix) === 0;
}

// Prefix of data URIs emitted by SINGLE_FILE and related options.
var dataURIPrefix = 'data:application/octet-stream;base64,';

// Indicates whether filename is a base64 data URI.
function isDataURI(filename) {
  return hasPrefix(filename, dataURIPrefix);
}

var fileURIPrefix = "file://";

// Indicates whether filename is delivered via file protocol (as opposed to http/https)
function isFileURI(filename) {
  return hasPrefix(filename, fileURIPrefix);
}




var wasmBinaryFile = 'jsnet.wasm';
if (!isDataURI(wasmBinaryFile)) {
  wasmBinaryFile = locateFile(wasmBinaryFile);
}

function getBinary() {
  try {
    if (wasmBinary) {
      return new Uint8Array(wasmBinary);
    }

    if (readBinary) {
      return readBinary(wasmBinaryFile);
    } else {
      throw "both async and sync fetching of the wasm failed";
    }
  }
  catch (err) {
    abort(err);
  }
}

function getBinaryPromise() {
  // If we don't have the binary yet, and have the Fetch api, use that;
  // in some environments, like Electron's render process, Fetch api may be present, but have a different context than expected, let's only use it on the Web
  if (!wasmBinary && (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) && typeof fetch === 'function'
      // Let's not use fetch to get objects over file:// as it's most likely Cordova which doesn't support fetch for file://
      && !isFileURI(wasmBinaryFile)
      ) {
    return fetch(wasmBinaryFile, { credentials: 'same-origin' }).then(function(response) {
      if (!response['ok']) {
        throw "failed to load wasm binary file at '" + wasmBinaryFile + "'";
      }
      return response['arrayBuffer']();
    }).catch(function () {
      return getBinary();
    });
  }
  // Otherwise, getBinary should be able to get it synchronously
  return new Promise(function(resolve, reject) {
    resolve(getBinary());
  });
}



// Create the wasm instance.
// Receives the wasm imports, returns the exports.
function createWasm() {
  // prepare imports
  var info = {
    'env': asmLibraryArg,
    'wasi_snapshot_preview1': asmLibraryArg
  };
  // Load the wasm module and create an instance of using native support in the JS engine.
  // handle a generated wasm instance, receiving its exports and
  // performing other necessary setup
  /** @param {WebAssembly.Module=} module*/
  function receiveInstance(instance, module) {
    var exports = instance.exports;
    Module['asm'] = exports;
    // In pure wasm mode the memory is created in the wasm (not imported), and
    // then exported.
    // TODO: do not create a Memory earlier in JS
    wasmMemory = exports['memory'];
    updateGlobalBufferAndViews(wasmMemory.buffer);
    removeRunDependency('wasm-instantiate');
  }
  // we can't run yet (except in a pthread, where we have a custom sync instantiator)
  addRunDependency('wasm-instantiate');


  function receiveInstantiatedSource(output) {
    // 'output' is a WebAssemblyInstantiatedSource object which has both the module and instance.
    // receiveInstance() will swap in the exports (to Module.asm) so they can be called
    // TODO: Due to Closure regression https://github.com/google/closure-compiler/issues/3193, the above line no longer optimizes out down to the following line.
    // When the regression is fixed, can restore the above USE_PTHREADS-enabled path.
    receiveInstance(output['instance']);
  }


  function instantiateArrayBuffer(receiver) {
    return getBinaryPromise().then(function(binary) {
      return WebAssembly.instantiate(binary, info);
    }).then(receiver, function(reason) {
      err('failed to asynchronously prepare wasm: ' + reason);
      abort(reason);
    });
  }

  // Prefer streaming instantiation if available.
  function instantiateAsync() {
    if (!wasmBinary &&
        typeof WebAssembly.instantiateStreaming === 'function' &&
        !isDataURI(wasmBinaryFile) &&
        // Don't use streaming for file:// delivered objects in a webview, fetch them synchronously.
        !isFileURI(wasmBinaryFile) &&
        typeof fetch === 'function') {
      fetch(wasmBinaryFile, { credentials: 'same-origin' }).then(function (response) {
        var result = WebAssembly.instantiateStreaming(response, info);
        return result.then(receiveInstantiatedSource, function(reason) {
            // We expect the most common failure cause to be a bad MIME type for the binary,
            // in which case falling back to ArrayBuffer instantiation should work.
            err('wasm streaming compile failed: ' + reason);
            err('falling back to ArrayBuffer instantiation');
            return instantiateArrayBuffer(receiveInstantiatedSource);
          });
      });
    } else {
      return instantiateArrayBuffer(receiveInstantiatedSource);
    }
  }
  // User shell pages can write their own Module.instantiateWasm = function(imports, successCallback) callback
  // to manually instantiate the Wasm module themselves. This allows pages to run the instantiation parallel
  // to any other async startup actions they are performing.
  if (Module['instantiateWasm']) {
    try {
      var exports = Module['instantiateWasm'](info, receiveInstance);
      return exports;
    } catch(e) {
      err('Module.instantiateWasm callback failed with error: ' + e);
      return false;
    }
  }

  instantiateAsync();
  return {}; // no exports yet; we'll fill them in later
}


// Globals used by JS i64 conversions
var tempDouble;
var tempI64;

// === Body ===

var ASM_CONSTS = {
  1212: function() {if (typeof window!='undefined') { window.dispatchEvent(new CustomEvent('jsNetWASMLoaded')); window.global = window.global || {}; } global.onWASMLoaded && global.onWASMLoaded();}
};

function _emscripten_asm_const_iii(code, sigPtr, argbuf) {
  var args = readAsmConstArgs(sigPtr, argbuf);
  return ASM_CONSTS[code].apply(null, args);
}



// STATICTOP = STATIC_BASE + 3440;
/* global initializers */  __ATINIT__.push();




/* no memory initializer */
// {{PRE_LIBRARY}}


  function demangle(func) {
      return func;
    }

  function demangleAll(text) {
      var regex =
        /\b_Z[\w\d_]+/g;
      return text.replace(regex,
        function(x) {
          var y = demangle(x);
          return x === y ? x : (y + ' [' + x + ']');
        });
    }

  function jsStackTrace() {
      var err = new Error();
      if (!err.stack) {
        // IE10+ special cases: It does have callstack info, but it is only populated if an Error object is thrown,
        // so try that as a special-case.
        try {
          throw new Error();
        } catch(e) {
          err = e;
        }
        if (!err.stack) {
          return '(no stack trace available)';
        }
      }
      return err.stack.toString();
    }

  function stackTrace() {
      var js = jsStackTrace();
      if (Module['extraStackTrace']) js += '\n' + Module['extraStackTrace']();
      return demangleAll(js);
    }

  function _args_get(argv, argv_buf) {
      var bufSize = 0;
      mainArgs.forEach(function(arg, i) {
        var ptr = argv_buf + bufSize;
        HEAP32[(((argv)+(i * 4))>>2)]=ptr;
        writeAsciiToMemory(arg, ptr);
        bufSize += arg.length + 1;
      });
      return 0;
    }

  function _args_sizes_get(pargc, pargv_buf_size) {
      HEAP32[((pargc)>>2)]=mainArgs.length;
      var bufSize = 0;
      mainArgs.forEach(function(arg) {
        bufSize += arg.length + 1;
      });
      HEAP32[((pargv_buf_size)>>2)]=bufSize;
      return 0;
    }

  function _emscripten_get_sbrk_ptr() {
      return 4304;
    }

  
  function flush_NO_FILESYSTEM() {
      // flush anything remaining in the buffers during shutdown
      if (typeof _fflush !== 'undefined') _fflush(0);
      var buffers = SYSCALLS.buffers;
      if (buffers[1].length) SYSCALLS.printChar(1, 10);
      if (buffers[2].length) SYSCALLS.printChar(2, 10);
    }
  
  
  var PATH={splitPath:function(filename) {
        var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
        return splitPathRe.exec(filename).slice(1);
      },normalizeArray:function(parts, allowAboveRoot) {
        // if the path tries to go above the root, `up` ends up > 0
        var up = 0;
        for (var i = parts.length - 1; i >= 0; i--) {
          var last = parts[i];
          if (last === '.') {
            parts.splice(i, 1);
          } else if (last === '..') {
            parts.splice(i, 1);
            up++;
          } else if (up) {
            parts.splice(i, 1);
            up--;
          }
        }
        // if the path is allowed to go above the root, restore leading ..s
        if (allowAboveRoot) {
          for (; up; up--) {
            parts.unshift('..');
          }
        }
        return parts;
      },normalize:function(path) {
        var isAbsolute = path.charAt(0) === '/',
            trailingSlash = path.substr(-1) === '/';
        // Normalize the path
        path = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), !isAbsolute).join('/');
        if (!path && !isAbsolute) {
          path = '.';
        }
        if (path && trailingSlash) {
          path += '/';
        }
        return (isAbsolute ? '/' : '') + path;
      },dirname:function(path) {
        var result = PATH.splitPath(path),
            root = result[0],
            dir = result[1];
        if (!root && !dir) {
          // No dirname whatsoever
          return '.';
        }
        if (dir) {
          // It has a dirname, strip trailing slash
          dir = dir.substr(0, dir.length - 1);
        }
        return root + dir;
      },basename:function(path) {
        // EMSCRIPTEN return '/'' for '/', not an empty string
        if (path === '/') return '/';
        var lastSlash = path.lastIndexOf('/');
        if (lastSlash === -1) return path;
        return path.substr(lastSlash+1);
      },extname:function(path) {
        return PATH.splitPath(path)[3];
      },join:function() {
        var paths = Array.prototype.slice.call(arguments, 0);
        return PATH.normalize(paths.join('/'));
      },join2:function(l, r) {
        return PATH.normalize(l + '/' + r);
      }};var SYSCALLS={mappings:{},buffers:[null,[],[]],printChar:function(stream, curr) {
        var buffer = SYSCALLS.buffers[stream];
        if (curr === 0 || curr === 10) {
          (stream === 1 ? out : err)(UTF8ArrayToString(buffer, 0));
          buffer.length = 0;
        } else {
          buffer.push(curr);
        }
      },varargs:undefined,get:function() {
        SYSCALLS.varargs += 4;
        var ret = HEAP32[(((SYSCALLS.varargs)-(4))>>2)];
        return ret;
      },getStr:function(ptr) {
        var ret = UTF8ToString(ptr);
        return ret;
      },get64:function(low, high) {
        return low;
      }};function _fd_write(fd, iov, iovcnt, pnum) {
      // hack to support printf in SYSCALLS_REQUIRE_FILESYSTEM=0
      var num = 0;
      for (var i = 0; i < iovcnt; i++) {
        var ptr = HEAP32[(((iov)+(i*8))>>2)];
        var len = HEAP32[(((iov)+(i*8 + 4))>>2)];
        for (var j = 0; j < len; j++) {
          SYSCALLS.printChar(fd, HEAPU8[ptr+j]);
        }
        num += len;
      }
      HEAP32[((pnum)>>2)]=num
      return 0;
    }

  
  function _exit(status) {
      // void _exit(int status);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/exit.html
      exit(status);
    }function _proc_exit(code) {
      _exit(code);
    }

  
  var __readAsmConstArgsArray=[];function readAsmConstArgs(sigPtr, buf) {
      __readAsmConstArgsArray.length = 0;
      var ch;
      buf >>= 2; // Align buf up front to index Int32Array (HEAP32)
      while (ch = HEAPU8[sigPtr++]) {
        __readAsmConstArgsArray.push(ch < 105 ? HEAPF64[++buf >> 1] : HEAP32[buf]);
        ++buf;
      }
      return __readAsmConstArgsArray;
    }
var ASSERTIONS = false;

/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

/** @type {function(string, boolean=, number=)} */
function intArrayFromString(stringy, dontAddNull, length) {
  var len = length > 0 ? length : lengthBytesUTF8(stringy)+1;
  var u8array = new Array(len);
  var numBytesWritten = stringToUTF8Array(stringy, u8array, 0, u8array.length);
  if (dontAddNull) u8array.length = numBytesWritten;
  return u8array;
}

function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
      if (ASSERTIONS) {
        assert(false, 'Character code ' + chr + ' (' + String.fromCharCode(chr) + ')  at offset ' + i + ' not in 0x00-0xFF.');
      }
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}


var asmGlobalArg = {};
var asmLibraryArg = { "args_get": _args_get, "args_sizes_get": _args_sizes_get, "emscripten_asm_const_iii": _emscripten_asm_const_iii, "emscripten_get_sbrk_ptr": _emscripten_get_sbrk_ptr, "fd_write": _fd_write, "proc_exit": _proc_exit };
var asm = createWasm();
/** @type {function(...*):?} */
var ___wasm_call_ctors = Module["___wasm_call_ctors"] = function() {
  return (___wasm_call_ctors = Module["___wasm_call_ctors"] = Module["asm"]["__wasm_call_ctors"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _newNetwork = Module["_newNetwork"] = function() {
  return (_newNetwork = Module["_newNetwork"] = Module["asm"]["newNetwork"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _deleteNetwork = Module["_deleteNetwork"] = function() {
  return (_deleteNetwork = Module["_deleteNetwork"] = Module["asm"]["deleteNetwork"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _getLearningRate = Module["_getLearningRate"] = function() {
  return (_getLearningRate = Module["_getLearningRate"] = Module["asm"]["getLearningRate"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _setLearningRate = Module["_setLearningRate"] = function() {
  return (_setLearningRate = Module["_setLearningRate"] = Module["asm"]["setLearningRate"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _getError = Module["_getError"] = function() {
  return (_getError = Module["_getError"] = Module["asm"]["getError"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _getValidationError = Module["_getValidationError"] = function() {
  return (_getValidationError = Module["_getValidationError"] = Module["asm"]["getValidationError"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _getLastValidationError = Module["_getLastValidationError"] = function() {
  return (_getLastValidationError = Module["_getLastValidationError"] = Module["asm"]["getLastValidationError"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_iterations = Module["_get_iterations"] = function() {
  return (_get_iterations = Module["_get_iterations"] = Module["asm"]["get_iterations"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_iterations = Module["_set_iterations"] = function() {
  return (_set_iterations = Module["_set_iterations"] = Module["asm"]["set_iterations"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_validations = Module["_get_validations"] = function() {
  return (_get_validations = Module["_get_validations"] = Module["asm"]["get_validations"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_validations = Module["_set_validations"] = function() {
  return (_set_validations = Module["_set_validations"] = Module["asm"]["set_validations"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_validationInterval = Module["_get_validationInterval"] = function() {
  return (_get_validationInterval = Module["_get_validationInterval"] = Module["asm"]["get_validationInterval"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_validationInterval = Module["_set_validationInterval"] = function() {
  return (_set_validationInterval = Module["_set_validationInterval"] = Module["asm"]["set_validationInterval"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_stoppedEarly = Module["_get_stoppedEarly"] = function() {
  return (_get_stoppedEarly = Module["_get_stoppedEarly"] = Module["asm"]["get_stoppedEarly"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_stoppedEarly = Module["_set_stoppedEarly"] = function() {
  return (_set_stoppedEarly = Module["_set_stoppedEarly"] = Module["asm"]["set_stoppedEarly"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_trainingLogging = Module["_get_trainingLogging"] = function() {
  return (_get_trainingLogging = Module["_get_trainingLogging"] = Module["asm"]["get_trainingLogging"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_trainingLogging = Module["_set_trainingLogging"] = function() {
  return (_set_trainingLogging = Module["_set_trainingLogging"] = Module["asm"]["set_trainingLogging"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_earlyStoppingType = Module["_get_earlyStoppingType"] = function() {
  return (_get_earlyStoppingType = Module["_get_earlyStoppingType"] = Module["asm"]["get_earlyStoppingType"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_earlyStoppingType = Module["_set_earlyStoppingType"] = function() {
  return (_set_earlyStoppingType = Module["_set_earlyStoppingType"] = Module["asm"]["set_earlyStoppingType"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_earlyStoppingThreshold = Module["_get_earlyStoppingThreshold"] = function() {
  return (_get_earlyStoppingThreshold = Module["_get_earlyStoppingThreshold"] = Module["asm"]["get_earlyStoppingThreshold"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_earlyStoppingThreshold = Module["_set_earlyStoppingThreshold"] = function() {
  return (_set_earlyStoppingThreshold = Module["_set_earlyStoppingThreshold"] = Module["asm"]["set_earlyStoppingThreshold"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_earlyStoppingBestError = Module["_get_earlyStoppingBestError"] = function() {
  return (_get_earlyStoppingBestError = Module["_get_earlyStoppingBestError"] = Module["asm"]["get_earlyStoppingBestError"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_earlyStoppingBestError = Module["_set_earlyStoppingBestError"] = function() {
  return (_set_earlyStoppingBestError = Module["_set_earlyStoppingBestError"] = Module["asm"]["set_earlyStoppingBestError"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_earlyStoppingPatienceCounter = Module["_get_earlyStoppingPatienceCounter"] = function() {
  return (_get_earlyStoppingPatienceCounter = Module["_get_earlyStoppingPatienceCounter"] = Module["asm"]["get_earlyStoppingPatienceCounter"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_earlyStoppingPatienceCounter = Module["_set_earlyStoppingPatienceCounter"] = function() {
  return (_set_earlyStoppingPatienceCounter = Module["_set_earlyStoppingPatienceCounter"] = Module["asm"]["set_earlyStoppingPatienceCounter"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_earlyStoppingPatience = Module["_get_earlyStoppingPatience"] = function() {
  return (_get_earlyStoppingPatience = Module["_get_earlyStoppingPatience"] = Module["asm"]["get_earlyStoppingPatience"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_earlyStoppingPatience = Module["_set_earlyStoppingPatience"] = function() {
  return (_set_earlyStoppingPatience = Module["_set_earlyStoppingPatience"] = Module["asm"]["set_earlyStoppingPatience"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_earlyStoppingPercent = Module["_get_earlyStoppingPercent"] = function() {
  return (_get_earlyStoppingPercent = Module["_get_earlyStoppingPercent"] = Module["asm"]["get_earlyStoppingPercent"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_earlyStoppingPercent = Module["_set_earlyStoppingPercent"] = function() {
  return (_set_earlyStoppingPercent = Module["_set_earlyStoppingPercent"] = Module["asm"]["set_earlyStoppingPercent"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _setActivation = Module["_setActivation"] = function() {
  return (_setActivation = Module["_setActivation"] = Module["asm"]["setActivation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _setCostFunction = Module["_setCostFunction"] = function() {
  return (_setCostFunction = Module["_setCostFunction"] = Module["asm"]["setCostFunction"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_trainingConfusionMatrix = Module["_get_trainingConfusionMatrix"] = function() {
  return (_get_trainingConfusionMatrix = Module["_get_trainingConfusionMatrix"] = Module["asm"]["get_trainingConfusionMatrix"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_testConfusionMatrix = Module["_get_testConfusionMatrix"] = function() {
  return (_get_testConfusionMatrix = Module["_get_testConfusionMatrix"] = Module["asm"]["get_testConfusionMatrix"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_validationConfusionMatrix = Module["_get_validationConfusionMatrix"] = function() {
  return (_get_validationConfusionMatrix = Module["_get_validationConfusionMatrix"] = Module["asm"]["get_validationConfusionMatrix"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _setOutputSoftmax = Module["_setOutputSoftmax"] = function() {
  return (_setOutputSoftmax = Module["_setOutputSoftmax"] = Module["asm"]["setOutputSoftmax"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_momentum = Module["_set_momentum"] = function() {
  return (_set_momentum = Module["_set_momentum"] = Module["asm"]["set_momentum"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_momentum = Module["_get_momentum"] = function() {
  return (_get_momentum = Module["_get_momentum"] = Module["asm"]["get_momentum"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_rmsDecay = Module["_set_rmsDecay"] = function() {
  return (_set_rmsDecay = Module["_set_rmsDecay"] = Module["asm"]["set_rmsDecay"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_rmsDecay = Module["_get_rmsDecay"] = function() {
  return (_get_rmsDecay = Module["_get_rmsDecay"] = Module["asm"]["get_rmsDecay"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_rho = Module["_set_rho"] = function() {
  return (_set_rho = Module["_set_rho"] = Module["asm"]["set_rho"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_rho = Module["_get_rho"] = function() {
  return (_get_rho = Module["_get_rho"] = Module["asm"]["get_rho"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_lreluSlope = Module["_set_lreluSlope"] = function() {
  return (_set_lreluSlope = Module["_set_lreluSlope"] = Module["asm"]["set_lreluSlope"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_lreluSlope = Module["_get_lreluSlope"] = function() {
  return (_get_lreluSlope = Module["_get_lreluSlope"] = Module["asm"]["get_lreluSlope"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_eluAlpha = Module["_set_eluAlpha"] = function() {
  return (_set_eluAlpha = Module["_set_eluAlpha"] = Module["asm"]["set_eluAlpha"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_eluAlpha = Module["_get_eluAlpha"] = function() {
  return (_get_eluAlpha = Module["_get_eluAlpha"] = Module["asm"]["get_eluAlpha"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_dropout = Module["_set_dropout"] = function() {
  return (_set_dropout = Module["_set_dropout"] = Module["asm"]["set_dropout"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_dropout = Module["_get_dropout"] = function() {
  return (_get_dropout = Module["_get_dropout"] = Module["asm"]["get_dropout"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_l2 = Module["_set_l2"] = function() {
  return (_set_l2 = Module["_set_l2"] = Module["asm"]["set_l2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_l2 = Module["_get_l2"] = function() {
  return (_get_l2 = Module["_get_l2"] = Module["asm"]["get_l2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_l2Error = Module["_set_l2Error"] = function() {
  return (_set_l2Error = Module["_set_l2Error"] = Module["asm"]["set_l2Error"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_l2Error = Module["_get_l2Error"] = function() {
  return (_get_l2Error = Module["_get_l2Error"] = Module["asm"]["get_l2Error"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_l1 = Module["_set_l1"] = function() {
  return (_set_l1 = Module["_set_l1"] = Module["asm"]["set_l1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_l1 = Module["_get_l1"] = function() {
  return (_get_l1 = Module["_get_l1"] = Module["asm"]["get_l1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_l1Error = Module["_set_l1Error"] = function() {
  return (_set_l1Error = Module["_set_l1Error"] = Module["asm"]["set_l1Error"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_l1Error = Module["_get_l1Error"] = function() {
  return (_get_l1Error = Module["_get_l1Error"] = Module["asm"]["get_l1Error"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_maxNorm = Module["_set_maxNorm"] = function() {
  return (_set_maxNorm = Module["_set_maxNorm"] = Module["asm"]["set_maxNorm"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_maxNorm = Module["_get_maxNorm"] = function() {
  return (_get_maxNorm = Module["_get_maxNorm"] = Module["asm"]["get_maxNorm"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_maxNormTotal = Module["_set_maxNormTotal"] = function() {
  return (_set_maxNormTotal = Module["_set_maxNormTotal"] = Module["asm"]["set_maxNormTotal"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_maxNormTotal = Module["_get_maxNormTotal"] = function() {
  return (_get_maxNormTotal = Module["_get_maxNormTotal"] = Module["asm"]["get_maxNormTotal"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_channels = Module["_set_channels"] = function() {
  return (_set_channels = Module["_set_channels"] = Module["asm"]["set_channels"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_channels = Module["_get_channels"] = function() {
  return (_get_channels = Module["_get_channels"] = Module["asm"]["get_channels"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_distribution = Module["_set_distribution"] = function() {
  return (_set_distribution = Module["_set_distribution"] = Module["asm"]["set_distribution"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_distribution = Module["_get_distribution"] = function() {
  return (_get_distribution = Module["_get_distribution"] = Module["asm"]["get_distribution"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_limit = Module["_set_limit"] = function() {
  return (_set_limit = Module["_set_limit"] = Module["asm"]["set_limit"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_limit = Module["_get_limit"] = function() {
  return (_get_limit = Module["_get_limit"] = Module["asm"]["get_limit"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_mean = Module["_set_mean"] = function() {
  return (_set_mean = Module["_set_mean"] = Module["asm"]["set_mean"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_mean = Module["_get_mean"] = function() {
  return (_get_mean = Module["_get_mean"] = Module["asm"]["get_mean"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_stdDeviation = Module["_set_stdDeviation"] = function() {
  return (_set_stdDeviation = Module["_set_stdDeviation"] = Module["asm"]["set_stdDeviation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_stdDeviation = Module["_get_stdDeviation"] = function() {
  return (_get_stdDeviation = Module["_get_stdDeviation"] = Module["asm"]["get_stdDeviation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_updateFn = Module["_set_updateFn"] = function() {
  return (_set_updateFn = Module["_set_updateFn"] = Module["asm"]["set_updateFn"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_updateFn = Module["_get_updateFn"] = function() {
  return (_get_updateFn = Module["_get_updateFn"] = Module["asm"]["get_updateFn"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _addFCLayer = Module["_addFCLayer"] = function() {
  return (_addFCLayer = Module["_addFCLayer"] = Module["asm"]["addFCLayer"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _addConvLayer = Module["_addConvLayer"] = function() {
  return (_addConvLayer = Module["_addConvLayer"] = Module["asm"]["addConvLayer"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _addPoolLayer = Module["_addPoolLayer"] = function() {
  return (_addPoolLayer = Module["_addPoolLayer"] = Module["asm"]["addPoolLayer"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _initLayers = Module["_initLayers"] = function() {
  return (_initLayers = Module["_initLayers"] = Module["asm"]["initLayers"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _forward = Module["_forward"] = function() {
  return (_forward = Module["_forward"] = Module["asm"]["forward"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _loadTrainingData = Module["_loadTrainingData"] = function() {
  return (_loadTrainingData = Module["_loadTrainingData"] = Module["asm"]["loadTrainingData"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_collected_training = Module["_get_collected_training"] = function() {
  return (_get_collected_training = Module["_get_collected_training"] = Module["asm"]["get_collected_training"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_collected_test = Module["_get_collected_test"] = function() {
  return (_get_collected_test = Module["_get_collected_test"] = Module["asm"]["get_collected_test"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_collected_validation = Module["_get_collected_validation"] = function() {
  return (_get_collected_validation = Module["_get_collected_validation"] = Module["asm"]["get_collected_validation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _collectErrors = Module["_collectErrors"] = function() {
  return (_collectErrors = Module["_collectErrors"] = Module["asm"]["collectErrors"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _loadValidationData = Module["_loadValidationData"] = function() {
  return (_loadValidationData = Module["_loadValidationData"] = Module["asm"]["loadValidationData"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _train = Module["_train"] = function() {
  return (_train = Module["_train"] = Module["asm"]["train"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _loadTestingData = Module["_loadTestingData"] = function() {
  return (_loadTestingData = Module["_loadTestingData"] = Module["asm"]["loadTestingData"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _shuffleTrainingData = Module["_shuffleTrainingData"] = function() {
  return (_shuffleTrainingData = Module["_shuffleTrainingData"] = Module["asm"]["shuffleTrainingData"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _restoreValidation = Module["_restoreValidation"] = function() {
  return (_restoreValidation = Module["_restoreValidation"] = Module["asm"]["restoreValidation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _test = Module["_test"] = function() {
  return (_test = Module["_test"] = Module["asm"]["test"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_miniBatchSize = Module["_set_miniBatchSize"] = function() {
  return (_set_miniBatchSize = Module["_set_miniBatchSize"] = Module["asm"]["set_miniBatchSize"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _resetDeltaWeights = Module["_resetDeltaWeights"] = function() {
  return (_resetDeltaWeights = Module["_resetDeltaWeights"] = Module["asm"]["resetDeltaWeights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_fc_activation = Module["_set_fc_activation"] = function() {
  return (_set_fc_activation = Module["_set_fc_activation"] = Module["asm"]["set_fc_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_fc_activation = Module["_get_fc_activation"] = function() {
  return (_get_fc_activation = Module["_get_fc_activation"] = Module["asm"]["get_fc_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_channels = Module["_get_conv_channels"] = function() {
  return (_get_conv_channels = Module["_get_conv_channels"] = Module["asm"]["get_conv_channels"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_channels = Module["_set_conv_channels"] = function() {
  return (_set_conv_channels = Module["_set_conv_channels"] = Module["asm"]["set_conv_channels"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_filterSize = Module["_get_conv_filterSize"] = function() {
  return (_get_conv_filterSize = Module["_get_conv_filterSize"] = Module["asm"]["get_conv_filterSize"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_filterSize = Module["_set_conv_filterSize"] = function() {
  return (_set_conv_filterSize = Module["_set_conv_filterSize"] = Module["asm"]["set_conv_filterSize"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_stride = Module["_get_conv_stride"] = function() {
  return (_get_conv_stride = Module["_get_conv_stride"] = Module["asm"]["get_conv_stride"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_stride = Module["_set_conv_stride"] = function() {
  return (_set_conv_stride = Module["_set_conv_stride"] = Module["asm"]["set_conv_stride"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_zeroPadding = Module["_get_conv_zeroPadding"] = function() {
  return (_get_conv_zeroPadding = Module["_get_conv_zeroPadding"] = Module["asm"]["get_conv_zeroPadding"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_zeroPadding = Module["_set_conv_zeroPadding"] = function() {
  return (_set_conv_zeroPadding = Module["_set_conv_zeroPadding"] = Module["asm"]["set_conv_zeroPadding"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_inMapValuesCount = Module["_get_conv_inMapValuesCount"] = function() {
  return (_get_conv_inMapValuesCount = Module["_get_conv_inMapValuesCount"] = Module["asm"]["get_conv_inMapValuesCount"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_inMapValuesCount = Module["_set_conv_inMapValuesCount"] = function() {
  return (_set_conv_inMapValuesCount = Module["_set_conv_inMapValuesCount"] = Module["asm"]["set_conv_inMapValuesCount"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_inZPMapValuesCount = Module["_get_conv_inZPMapValuesCount"] = function() {
  return (_get_conv_inZPMapValuesCount = Module["_get_conv_inZPMapValuesCount"] = Module["asm"]["get_conv_inZPMapValuesCount"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_inZPMapValuesCount = Module["_set_conv_inZPMapValuesCount"] = function() {
  return (_set_conv_inZPMapValuesCount = Module["_set_conv_inZPMapValuesCount"] = Module["asm"]["set_conv_inZPMapValuesCount"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_outMapSize = Module["_get_conv_outMapSize"] = function() {
  return (_get_conv_outMapSize = Module["_get_conv_outMapSize"] = Module["asm"]["get_conv_outMapSize"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_outMapSize = Module["_set_conv_outMapSize"] = function() {
  return (_set_conv_outMapSize = Module["_set_conv_outMapSize"] = Module["asm"]["set_conv_outMapSize"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_conv_activation = Module["_set_conv_activation"] = function() {
  return (_set_conv_activation = Module["_set_conv_activation"] = Module["asm"]["set_conv_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_conv_activation = Module["_get_conv_activation"] = function() {
  return (_get_conv_activation = Module["_get_conv_activation"] = Module["asm"]["get_conv_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_activation = Module["_set_pool_activation"] = function() {
  return (_set_pool_activation = Module["_set_pool_activation"] = Module["asm"]["set_pool_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_activation = Module["_get_pool_activation"] = function() {
  return (_get_pool_activation = Module["_get_pool_activation"] = Module["asm"]["get_pool_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_channels = Module["_get_pool_channels"] = function() {
  return (_get_pool_channels = Module["_get_pool_channels"] = Module["asm"]["get_pool_channels"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_channels = Module["_set_pool_channels"] = function() {
  return (_set_pool_channels = Module["_set_pool_channels"] = Module["asm"]["set_pool_channels"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_stride = Module["_get_pool_stride"] = function() {
  return (_get_pool_stride = Module["_get_pool_stride"] = Module["asm"]["get_pool_stride"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_stride = Module["_set_pool_stride"] = function() {
  return (_set_pool_stride = Module["_set_pool_stride"] = Module["asm"]["set_pool_stride"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_inMapValuesCount = Module["_get_pool_inMapValuesCount"] = function() {
  return (_get_pool_inMapValuesCount = Module["_get_pool_inMapValuesCount"] = Module["asm"]["get_pool_inMapValuesCount"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_inMapValuesCount = Module["_set_pool_inMapValuesCount"] = function() {
  return (_set_pool_inMapValuesCount = Module["_set_pool_inMapValuesCount"] = Module["asm"]["set_pool_inMapValuesCount"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_outMapSize = Module["_get_pool_outMapSize"] = function() {
  return (_get_pool_outMapSize = Module["_get_pool_outMapSize"] = Module["asm"]["get_pool_outMapSize"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_outMapSize = Module["_set_pool_outMapSize"] = function() {
  return (_set_pool_outMapSize = Module["_set_pool_outMapSize"] = Module["asm"]["set_pool_outMapSize"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_prevLayerOutWidth = Module["_get_pool_prevLayerOutWidth"] = function() {
  return (_get_pool_prevLayerOutWidth = Module["_get_pool_prevLayerOutWidth"] = Module["asm"]["get_pool_prevLayerOutWidth"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_prevLayerOutWidth = Module["_set_pool_prevLayerOutWidth"] = function() {
  return (_set_pool_prevLayerOutWidth = Module["_set_pool_prevLayerOutWidth"] = Module["asm"]["set_pool_prevLayerOutWidth"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_errors = Module["_get_pool_errors"] = function() {
  return (_get_pool_errors = Module["_get_pool_errors"] = Module["asm"]["get_pool_errors"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_errors = Module["_set_pool_errors"] = function() {
  return (_set_pool_errors = Module["_set_pool_errors"] = Module["asm"]["set_pool_errors"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_activations = Module["_get_pool_activations"] = function() {
  return (_get_pool_activations = Module["_get_pool_activations"] = Module["asm"]["get_pool_activations"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_activations = Module["_set_pool_activations"] = function() {
  return (_set_pool_activations = Module["_set_pool_activations"] = Module["asm"]["set_pool_activations"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_pool_indeces = Module["_get_pool_indeces"] = function() {
  return (_get_pool_indeces = Module["_get_pool_indeces"] = Module["asm"]["get_pool_indeces"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_pool_indeces = Module["_set_pool_indeces"] = function() {
  return (_set_pool_indeces = Module["_set_pool_indeces"] = Module["asm"]["set_pool_indeces"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_weights = Module["_get_neuron_weights"] = function() {
  return (_get_neuron_weights = Module["_get_neuron_weights"] = Module["asm"]["get_neuron_weights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_weights = Module["_set_neuron_weights"] = function() {
  return (_set_neuron_weights = Module["_set_neuron_weights"] = Module["asm"]["set_neuron_weights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_bias = Module["_get_neuron_bias"] = function() {
  return (_get_neuron_bias = Module["_get_neuron_bias"] = Module["asm"]["get_neuron_bias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_bias = Module["_set_neuron_bias"] = function() {
  return (_set_neuron_bias = Module["_set_neuron_bias"] = Module["asm"]["set_neuron_bias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_deltaWeights = Module["_get_neuron_deltaWeights"] = function() {
  return (_get_neuron_deltaWeights = Module["_get_neuron_deltaWeights"] = Module["asm"]["get_neuron_deltaWeights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_deltaWeights = Module["_set_neuron_deltaWeights"] = function() {
  return (_set_neuron_deltaWeights = Module["_set_neuron_deltaWeights"] = Module["asm"]["set_neuron_deltaWeights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_deltaBias = Module["_get_neuron_deltaBias"] = function() {
  return (_get_neuron_deltaBias = Module["_get_neuron_deltaBias"] = Module["asm"]["get_neuron_deltaBias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_deltaBias = Module["_set_neuron_deltaBias"] = function() {
  return (_set_neuron_deltaBias = Module["_set_neuron_deltaBias"] = Module["asm"]["set_neuron_deltaBias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_weightGain = Module["_get_neuron_weightGain"] = function() {
  return (_get_neuron_weightGain = Module["_get_neuron_weightGain"] = Module["asm"]["get_neuron_weightGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_weightGain = Module["_set_neuron_weightGain"] = function() {
  return (_set_neuron_weightGain = Module["_set_neuron_weightGain"] = Module["asm"]["set_neuron_weightGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_weightsCache = Module["_get_neuron_weightsCache"] = function() {
  return (_get_neuron_weightsCache = Module["_get_neuron_weightsCache"] = Module["asm"]["get_neuron_weightsCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_weightsCache = Module["_set_neuron_weightsCache"] = function() {
  return (_set_neuron_weightsCache = Module["_set_neuron_weightsCache"] = Module["asm"]["set_neuron_weightsCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_biasGain = Module["_set_neuron_biasGain"] = function() {
  return (_set_neuron_biasGain = Module["_set_neuron_biasGain"] = Module["asm"]["set_neuron_biasGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_biasGain = Module["_get_neuron_biasGain"] = function() {
  return (_get_neuron_biasGain = Module["_get_neuron_biasGain"] = Module["asm"]["get_neuron_biasGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_biasCache = Module["_set_neuron_biasCache"] = function() {
  return (_set_neuron_biasCache = Module["_set_neuron_biasCache"] = Module["asm"]["set_neuron_biasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_biasCache = Module["_get_neuron_biasCache"] = function() {
  return (_get_neuron_biasCache = Module["_get_neuron_biasCache"] = Module["asm"]["get_neuron_biasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_m = Module["_set_neuron_m"] = function() {
  return (_set_neuron_m = Module["_set_neuron_m"] = Module["asm"]["set_neuron_m"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_m = Module["_get_neuron_m"] = function() {
  return (_get_neuron_m = Module["_get_neuron_m"] = Module["asm"]["get_neuron_m"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_v = Module["_set_neuron_v"] = function() {
  return (_set_neuron_v = Module["_set_neuron_v"] = Module["asm"]["set_neuron_v"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_v = Module["_get_neuron_v"] = function() {
  return (_get_neuron_v = Module["_get_neuron_v"] = Module["asm"]["get_neuron_v"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_adadeltaBiasCache = Module["_set_neuron_adadeltaBiasCache"] = function() {
  return (_set_neuron_adadeltaBiasCache = Module["_set_neuron_adadeltaBiasCache"] = Module["asm"]["set_neuron_adadeltaBiasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_adadeltaBiasCache = Module["_get_neuron_adadeltaBiasCache"] = function() {
  return (_get_neuron_adadeltaBiasCache = Module["_get_neuron_adadeltaBiasCache"] = Module["asm"]["get_neuron_adadeltaBiasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_adadeltaCache = Module["_get_neuron_adadeltaCache"] = function() {
  return (_get_neuron_adadeltaCache = Module["_get_neuron_adadeltaCache"] = Module["asm"]["get_neuron_adadeltaCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_adadeltaCache = Module["_set_neuron_adadeltaCache"] = function() {
  return (_set_neuron_adadeltaCache = Module["_set_neuron_adadeltaCache"] = Module["asm"]["set_neuron_adadeltaCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_sum = Module["_get_neuron_sum"] = function() {
  return (_get_neuron_sum = Module["_get_neuron_sum"] = Module["asm"]["get_neuron_sum"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_sum = Module["_set_neuron_sum"] = function() {
  return (_set_neuron_sum = Module["_set_neuron_sum"] = Module["asm"]["set_neuron_sum"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_dropped = Module["_get_neuron_dropped"] = function() {
  return (_get_neuron_dropped = Module["_get_neuron_dropped"] = Module["asm"]["get_neuron_dropped"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_dropped = Module["_set_neuron_dropped"] = function() {
  return (_set_neuron_dropped = Module["_set_neuron_dropped"] = Module["asm"]["set_neuron_dropped"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_activation = Module["_get_neuron_activation"] = function() {
  return (_get_neuron_activation = Module["_get_neuron_activation"] = Module["asm"]["get_neuron_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_activation = Module["_set_neuron_activation"] = function() {
  return (_set_neuron_activation = Module["_set_neuron_activation"] = Module["asm"]["set_neuron_activation"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_error = Module["_get_neuron_error"] = function() {
  return (_get_neuron_error = Module["_get_neuron_error"] = Module["asm"]["get_neuron_error"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_error = Module["_set_neuron_error"] = function() {
  return (_set_neuron_error = Module["_set_neuron_error"] = Module["asm"]["set_neuron_error"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_neuron_derivative = Module["_get_neuron_derivative"] = function() {
  return (_get_neuron_derivative = Module["_get_neuron_derivative"] = Module["asm"]["get_neuron_derivative"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_neuron_derivative = Module["_set_neuron_derivative"] = function() {
  return (_set_neuron_derivative = Module["_set_neuron_derivative"] = Module["asm"]["set_neuron_derivative"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_bias = Module["_get_filter_bias"] = function() {
  return (_get_filter_bias = Module["_get_filter_bias"] = Module["asm"]["get_filter_bias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_bias = Module["_set_filter_bias"] = function() {
  return (_set_filter_bias = Module["_set_filter_bias"] = Module["asm"]["set_filter_bias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_weights = Module["_get_filter_weights"] = function() {
  return (_get_filter_weights = Module["_get_filter_weights"] = Module["asm"]["get_filter_weights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_weights = Module["_set_filter_weights"] = function() {
  return (_set_filter_weights = Module["_set_filter_weights"] = Module["asm"]["set_filter_weights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_deltaBias = Module["_get_filter_deltaBias"] = function() {
  return (_get_filter_deltaBias = Module["_get_filter_deltaBias"] = Module["asm"]["get_filter_deltaBias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_deltaBias = Module["_set_filter_deltaBias"] = function() {
  return (_set_filter_deltaBias = Module["_set_filter_deltaBias"] = Module["asm"]["set_filter_deltaBias"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_deltaWeights = Module["_get_filter_deltaWeights"] = function() {
  return (_get_filter_deltaWeights = Module["_get_filter_deltaWeights"] = Module["asm"]["get_filter_deltaWeights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_deltaWeights = Module["_set_filter_deltaWeights"] = function() {
  return (_set_filter_deltaWeights = Module["_set_filter_deltaWeights"] = Module["asm"]["set_filter_deltaWeights"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_biasGain = Module["_get_filter_biasGain"] = function() {
  return (_get_filter_biasGain = Module["_get_filter_biasGain"] = Module["asm"]["get_filter_biasGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_biasGain = Module["_set_filter_biasGain"] = function() {
  return (_set_filter_biasGain = Module["_set_filter_biasGain"] = Module["asm"]["set_filter_biasGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_weightGain = Module["_get_filter_weightGain"] = function() {
  return (_get_filter_weightGain = Module["_get_filter_weightGain"] = Module["asm"]["get_filter_weightGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_weightGain = Module["_set_filter_weightGain"] = function() {
  return (_set_filter_weightGain = Module["_set_filter_weightGain"] = Module["asm"]["set_filter_weightGain"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_biasCache = Module["_get_filter_biasCache"] = function() {
  return (_get_filter_biasCache = Module["_get_filter_biasCache"] = Module["asm"]["get_filter_biasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_biasCache = Module["_set_filter_biasCache"] = function() {
  return (_set_filter_biasCache = Module["_set_filter_biasCache"] = Module["asm"]["set_filter_biasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_weightsCache = Module["_get_filter_weightsCache"] = function() {
  return (_get_filter_weightsCache = Module["_get_filter_weightsCache"] = Module["asm"]["get_filter_weightsCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_weightsCache = Module["_set_filter_weightsCache"] = function() {
  return (_set_filter_weightsCache = Module["_set_filter_weightsCache"] = Module["asm"]["set_filter_weightsCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_adadeltaBiasCache = Module["_get_filter_adadeltaBiasCache"] = function() {
  return (_get_filter_adadeltaBiasCache = Module["_get_filter_adadeltaBiasCache"] = Module["asm"]["get_filter_adadeltaBiasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_adadeltaBiasCache = Module["_set_filter_adadeltaBiasCache"] = function() {
  return (_set_filter_adadeltaBiasCache = Module["_set_filter_adadeltaBiasCache"] = Module["asm"]["set_filter_adadeltaBiasCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_adadeltaWeightsCache = Module["_get_filter_adadeltaWeightsCache"] = function() {
  return (_get_filter_adadeltaWeightsCache = Module["_get_filter_adadeltaWeightsCache"] = Module["asm"]["get_filter_adadeltaWeightsCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_adadeltaWeightsCache = Module["_set_filter_adadeltaWeightsCache"] = function() {
  return (_set_filter_adadeltaWeightsCache = Module["_set_filter_adadeltaWeightsCache"] = Module["asm"]["set_filter_adadeltaWeightsCache"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_m = Module["_get_filter_m"] = function() {
  return (_get_filter_m = Module["_get_filter_m"] = Module["asm"]["get_filter_m"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_m = Module["_set_filter_m"] = function() {
  return (_set_filter_m = Module["_set_filter_m"] = Module["asm"]["set_filter_m"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_v = Module["_get_filter_v"] = function() {
  return (_get_filter_v = Module["_get_filter_v"] = Module["asm"]["get_filter_v"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_v = Module["_set_filter_v"] = function() {
  return (_set_filter_v = Module["_set_filter_v"] = Module["asm"]["set_filter_v"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_activationMap = Module["_get_filter_activationMap"] = function() {
  return (_get_filter_activationMap = Module["_get_filter_activationMap"] = Module["asm"]["get_filter_activationMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_activationMap = Module["_set_filter_activationMap"] = function() {
  return (_set_filter_activationMap = Module["_set_filter_activationMap"] = Module["asm"]["set_filter_activationMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_errorMap = Module["_get_filter_errorMap"] = function() {
  return (_get_filter_errorMap = Module["_get_filter_errorMap"] = Module["asm"]["get_filter_errorMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_errorMap = Module["_set_filter_errorMap"] = function() {
  return (_set_filter_errorMap = Module["_set_filter_errorMap"] = Module["asm"]["set_filter_errorMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_sumMap = Module["_get_filter_sumMap"] = function() {
  return (_get_filter_sumMap = Module["_get_filter_sumMap"] = Module["asm"]["get_filter_sumMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_sumMap = Module["_set_filter_sumMap"] = function() {
  return (_set_filter_sumMap = Module["_set_filter_sumMap"] = Module["asm"]["set_filter_sumMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _get_filter_dropoutMap = Module["_get_filter_dropoutMap"] = function() {
  return (_get_filter_dropoutMap = Module["_get_filter_dropoutMap"] = Module["asm"]["get_filter_dropoutMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _set_filter_dropoutMap = Module["_set_filter_dropoutMap"] = function() {
  return (_set_filter_dropoutMap = Module["_set_filter_dropoutMap"] = Module["asm"]["set_filter_dropoutMap"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var __start = Module["__start"] = function() {
  return (__start = Module["__start"] = Module["asm"]["_start"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var ___errno_location = Module["___errno_location"] = function() {
  return (___errno_location = Module["___errno_location"] = Module["asm"]["__errno_location"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _setThrew = Module["_setThrew"] = function() {
  return (_setThrew = Module["_setThrew"] = Module["asm"]["setThrew"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackSave = Module["stackSave"] = function() {
  return (stackSave = Module["stackSave"] = Module["asm"]["stackSave"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackRestore = Module["stackRestore"] = function() {
  return (stackRestore = Module["stackRestore"] = Module["asm"]["stackRestore"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackAlloc = Module["stackAlloc"] = function() {
  return (stackAlloc = Module["stackAlloc"] = Module["asm"]["stackAlloc"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _malloc = Module["_malloc"] = function() {
  return (_malloc = Module["_malloc"] = Module["asm"]["malloc"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _free = Module["_free"] = function() {
  return (_free = Module["_free"] = Module["asm"]["free"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var __growWasmMemory = Module["__growWasmMemory"] = function() {
  return (__growWasmMemory = Module["__growWasmMemory"] = Module["asm"]["__growWasmMemory"]).apply(null, arguments);
};



/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// === Auto-generated postamble setup entry stuff ===











































































































































var calledRun;

/**
 * @constructor
 * @this {ExitStatus}
 */
function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
}

var calledMain = false;

var mainArgs = undefined;

dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!calledRun) run();
  if (!calledRun) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
};

function callMain(args) {

  var entryFunction = Module['__start'];


  mainArgs = [thisProgram].concat(args)

  try {


    entryFunction();
    // _start (in crt1.c) will call exit() if main return non-zero.  So we know
    // that if we get here main returned zero.
    var ret = 0;


    // In PROXY_TO_PTHREAD builds, we should never exit the runtime below, as execution is asynchronously handed
    // off to a pthread.
    // if we're not running an evented main loop, it's time to exit
      exit(ret, /* implicit = */ true);
  }
  catch(e) {
    if (e instanceof ExitStatus) {
      // exit() throws this once it's done to make sure execution
      // has been stopped completely
      return;
    } else if (e == 'unwind') {
      // running an evented main loop, don't immediately exit
      noExitRuntime = true;
      return;
    } else {
      var toLog = e;
      if (e && typeof e === 'object' && e.stack) {
        toLog = [e, e.stack];
      }
      err('exception thrown: ' + toLog);
      quit_(1, e);
    }
  } finally {
    calledMain = true;
  }
}




/** @type {function(Array=)} */
function run(args) {
  args = args || arguments_;

  if (runDependencies > 0) {
    return;
  }


  preRun();

  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later

  function doRun() {
    // run may have just been called through dependencies being fulfilled just in this very frame,
    // or while the async setStatus time below was happening
    if (calledRun) return;
    calledRun = true;
    Module['calledRun'] = true;

    if (ABORT) return;

    initRuntime();

    preMain();

    if (Module['onRuntimeInitialized']) Module['onRuntimeInitialized']();

    if (shouldRunNow) callMain(args);

    postRun();
  }

  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      doRun();
    }, 1);
  } else
  {
    doRun();
  }
}
Module['run'] = run;


/** @param {boolean|number=} implicit */
function exit(status, implicit) {

  // if this is just main exit-ing implicitly, and the status is 0, then we
  // don't need to do anything here and can just leave. if the status is
  // non-zero, though, then we need to report it.
  // (we may have warned about this earlier, if a situation justifies doing so)
  if (implicit && noExitRuntime && status === 0) {
    return;
  }

  if (noExitRuntime) {
  } else {

    ABORT = true;
    EXITSTATUS = status;

    exitRuntime();

    if (Module['onExit']) Module['onExit'](status);
  }

  quit_(status, new ExitStatus(status));
}

if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}

// shouldRunNow refers to calling main(), not run().
var shouldRunNow = true;

if (Module['noInitialRun']) shouldRunNow = false;


  noExitRuntime = true;

run();






// {{MODULE_ADDITIONS}}



