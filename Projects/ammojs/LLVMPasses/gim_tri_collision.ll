; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/gim_tri_collision.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/gim_tri_collision.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.GIM_TRIANGLE = type { float, [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%struct.GIM_TRIANGLE_CONTACT_DATA = type { float, i32, %class.btVector4, [16 x %class.btVector3] }
%class.btVector4 = type { %class.btVector3 }
%class.GIM_TRIANGLE_CALCULATION_CACHE = type { float, [3 x %class.btVector3], [3 x %class.btVector3], %class.btVector4, %class.btVector4, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [4 x float], float, float, [4 x float], float, float, [16 x %class.btVector3], [16 x %class.btVector3], [16 x %class.btVector3] }
%class.DISTANCE_PLANE_3D_FUNC = type { i8 }

$_ZN30GIM_TRIANGLE_CALCULATION_CACHEC2Ev = comdat any

$_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector4C2Ev = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_ = comdat any

$_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z21PLANE_CLIP_TRIANGLE3DI9btVector39btVector4EjRKT0_RKT_S7_S7_PS5_ = comdat any

$_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_ = comdat any

$_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_ = comdat any

$_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_ = comdat any

$_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj = comdat any

$_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_ = comdat any

$_ZN25GIM_TRIANGLE_CONTACT_DATA19mergepoints_genericI22DISTANCE_PLANE_3D_FUNC9btVector4EEvRKT0_fPK9btVector3jT_ = comdat any

define hidden zeroext i1 @_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA(%class.GIM_TRIANGLE* %this, %class.GIM_TRIANGLE* nonnull align 4 dereferenceable(52) %other, %struct.GIM_TRIANGLE_CONTACT_DATA* nonnull align 4 dereferenceable(280) %contact_data) #0 {
entry:
  %this.addr = alloca %class.GIM_TRIANGLE*, align 4
  %other.addr = alloca %class.GIM_TRIANGLE*, align 4
  %contact_data.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %calc_cache = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE, align 4
  store %class.GIM_TRIANGLE* %this, %class.GIM_TRIANGLE** %this.addr, align 4, !tbaa !2
  store %class.GIM_TRIANGLE* %other, %class.GIM_TRIANGLE** %other.addr, align 4, !tbaa !2
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %contact_data, %struct.GIM_TRIANGLE_CONTACT_DATA** %contact_data.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %this.addr, align 4
  %0 = bitcast %class.GIM_TRIANGLE_CALCULATION_CACHE* %calc_cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 1012, i8* %0) #4
  %call = call %class.GIM_TRIANGLE_CALCULATION_CACHE* @_ZN30GIM_TRIANGLE_CALCULATION_CACHEC2Ev(%class.GIM_TRIANGLE_CALCULATION_CACHE* %calc_cache)
  %m_vertices = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %m_vertices2 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices2, i32 0, i32 1
  %m_vertices4 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices4, i32 0, i32 2
  %m_margin = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 0
  %1 = load float, float* %m_margin, align 4, !tbaa !6
  %2 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4, !tbaa !2
  %m_vertices6 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %2, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices6, i32 0, i32 0
  %3 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4, !tbaa !2
  %m_vertices8 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %3, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices8, i32 0, i32 1
  %4 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4, !tbaa !2
  %m_vertices10 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %4, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices10, i32 0, i32 2
  %5 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4, !tbaa !2
  %m_margin12 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %5, i32 0, i32 0
  %6 = load float, float* %m_margin12, align 4, !tbaa !6
  %7 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contact_data.addr, align 4, !tbaa !2
  %call13 = call zeroext i1 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA(%class.GIM_TRIANGLE_CALCULATION_CACHE* %calc_cache, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5, float %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11, float %6, %struct.GIM_TRIANGLE_CONTACT_DATA* nonnull align 4 dereferenceable(280) %7)
  %8 = bitcast %class.GIM_TRIANGLE_CALCULATION_CACHE* %calc_cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 1012, i8* %8) #4
  ret i1 %call13
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint
define linkonce_odr hidden %class.GIM_TRIANGLE_CALCULATION_CACHE* @_ZN30GIM_TRIANGLE_CALCULATION_CACHEC2Ev(%class.GIM_TRIANGLE_CALCULATION_CACHE* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  %this.addr = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, %class.GIM_TRIANGLE_CALCULATION_CACHE** %retval, align 4
  %tu_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %tv_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %array.begin2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices, i32 0, i32 0
  %arrayctor.end3 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin2, i32 3
  br label %arrayctor.loop4

arrayctor.loop4:                                  ; preds = %arrayctor.loop4, %arrayctor.cont
  %arrayctor.cur5 = phi %class.btVector3* [ %array.begin2, %arrayctor.cont ], [ %arrayctor.next7, %arrayctor.loop4 ]
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur5)
  %arrayctor.next7 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur5, i32 1
  %arrayctor.done8 = icmp eq %class.btVector3* %arrayctor.next7, %arrayctor.end3
  br i1 %arrayctor.done8, label %arrayctor.cont9, label %arrayctor.loop4

arrayctor.cont9:                                  ; preds = %arrayctor.loop4
  %tu_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %call10 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %tu_plane)
  %tv_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %call11 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %tv_plane)
  %closest_point_u = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 5
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %closest_point_u)
  %closest_point_v = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 6
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %closest_point_v)
  %edge_edge_dir = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edge_edge_dir)
  %distances = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %distances)
  %temp_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 15
  %array.begin16 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points, i32 0, i32 0
  %arrayctor.end17 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin16, i32 16
  br label %arrayctor.loop18

arrayctor.loop18:                                 ; preds = %arrayctor.loop18, %arrayctor.cont9
  %arrayctor.cur19 = phi %class.btVector3* [ %array.begin16, %arrayctor.cont9 ], [ %arrayctor.next21, %arrayctor.loop18 ]
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur19)
  %arrayctor.next21 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur19, i32 1
  %arrayctor.done22 = icmp eq %class.btVector3* %arrayctor.next21, %arrayctor.end17
  br i1 %arrayctor.done22, label %arrayctor.cont23, label %arrayctor.loop18

arrayctor.cont23:                                 ; preds = %arrayctor.loop18
  %temp_points1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 16
  %array.begin24 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1, i32 0, i32 0
  %arrayctor.end25 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin24, i32 16
  br label %arrayctor.loop26

arrayctor.loop26:                                 ; preds = %arrayctor.loop26, %arrayctor.cont23
  %arrayctor.cur27 = phi %class.btVector3* [ %array.begin24, %arrayctor.cont23 ], [ %arrayctor.next29, %arrayctor.loop26 ]
  %call28 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur27)
  %arrayctor.next29 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur27, i32 1
  %arrayctor.done30 = icmp eq %class.btVector3* %arrayctor.next29, %arrayctor.end25
  br i1 %arrayctor.done30, label %arrayctor.cont31, label %arrayctor.loop26

arrayctor.cont31:                                 ; preds = %arrayctor.loop26
  %contact_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %array.begin32 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points, i32 0, i32 0
  %arrayctor.end33 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin32, i32 16
  br label %arrayctor.loop34

arrayctor.loop34:                                 ; preds = %arrayctor.loop34, %arrayctor.cont31
  %arrayctor.cur35 = phi %class.btVector3* [ %array.begin32, %arrayctor.cont31 ], [ %arrayctor.next37, %arrayctor.loop34 ]
  %call36 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur35)
  %arrayctor.next37 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur35, i32 1
  %arrayctor.done38 = icmp eq %class.btVector3* %arrayctor.next37, %arrayctor.end33
  br i1 %arrayctor.done38, label %arrayctor.cont39, label %arrayctor.loop34

arrayctor.cont39:                                 ; preds = %arrayctor.loop34
  %0 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %retval, align 4
  ret %class.GIM_TRIANGLE_CALCULATION_CACHE* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %u0, %class.btVector3* nonnull align 4 dereferenceable(16) %u1, %class.btVector3* nonnull align 4 dereferenceable(16) %u2, float %margin_u, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, float %margin_v, %struct.GIM_TRIANGLE_CONTACT_DATA* nonnull align 4 dereferenceable(280) %contacts) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  %u0.addr = alloca %class.btVector3*, align 4
  %u1.addr = alloca %class.btVector3*, align 4
  %u2.addr = alloca %class.btVector3*, align 4
  %margin_u.addr = alloca float, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %margin_v.addr = alloca float, align 4
  %contacts.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %_dif1 = alloca [3 x float], align 4
  %_dif2 = alloca [3 x float], align 4
  %len = alloca float, align 4
  %_pp = alloca float, align 4
  %_x = alloca float, align 4
  %_y = alloca i32, align 4
  %_dif1770 = alloca [3 x float], align 4
  %_dif2771 = alloca [3 x float], align 4
  %len861 = alloca float, align 4
  %_pp862 = alloca float, align 4
  %_x889 = alloca float, align 4
  %_y891 = alloca i32, align 4
  %bl = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %point_count = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %u0, %class.btVector3** %u0.addr, align 4, !tbaa !2
  store %class.btVector3* %u1, %class.btVector3** %u1.addr, align 4, !tbaa !2
  store %class.btVector3* %u2, %class.btVector3** %u2.addr, align 4, !tbaa !2
  store float %margin_u, float* %margin_u.addr, align 4, !tbaa !9
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  store float %margin_v, float* %margin_v.addr, align 4, !tbaa !9
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %contacts, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  %0 = load float, float* %margin_u.addr, align 4, !tbaa !9
  %1 = load float, float* %margin_v.addr, align 4, !tbaa !9
  %add = fadd float %0, %1
  %margin = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  store float %add, float* %margin, align 4, !tbaa !10
  %2 = load %class.btVector3*, %class.btVector3** %u0.addr, align 4, !tbaa !2
  %tu_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices, i32 0, i32 0
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !14
  %5 = load %class.btVector3*, %class.btVector3** %u1.addr, align 4, !tbaa !2
  %tu_vertices2 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices2, i32 0, i32 1
  %6 = bitcast %class.btVector3* %arrayidx3 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !14
  %8 = load %class.btVector3*, %class.btVector3** %u2.addr, align 4, !tbaa !2
  %tu_vertices4 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices4, i32 0, i32 2
  %9 = bitcast %class.btVector3* %arrayidx5 to i8*
  %10 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !14
  %11 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %tv_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices, i32 0, i32 0
  %12 = bitcast %class.btVector3* %arrayidx6 to i8*
  %13 = bitcast %class.btVector3* %11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !14
  %14 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %tv_vertices7 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices7, i32 0, i32 1
  %15 = bitcast %class.btVector3* %arrayidx8 to i8*
  %16 = bitcast %class.btVector3* %14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !14
  %17 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %tv_vertices9 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices9, i32 0, i32 2
  %18 = bitcast %class.btVector3* %arrayidx10 to i8*
  %19 = bitcast %class.btVector3* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !14
  %20 = bitcast [3 x float]* %_dif1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %20) #4
  %21 = bitcast [3 x float]* %_dif2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %21) #4
  %tv_vertices11 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices11, i32 0, i32 1
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx12)
  %arrayidx13 = getelementptr inbounds float, float* %call, i32 0
  %22 = load float, float* %arrayidx13, align 4, !tbaa !9
  %tv_vertices14 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices14, i32 0, i32 0
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx15)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 0
  %23 = load float, float* %arrayidx17, align 4, !tbaa !9
  %sub = fsub float %22, %23
  %arrayidx18 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 0
  store float %sub, float* %arrayidx18, align 4, !tbaa !9
  %tv_vertices19 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices19, i32 0, i32 1
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 1
  %24 = load float, float* %arrayidx22, align 4, !tbaa !9
  %tv_vertices23 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices23, i32 0, i32 0
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %25 = load float, float* %arrayidx26, align 4, !tbaa !9
  %sub27 = fsub float %24, %25
  %arrayidx28 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 1
  store float %sub27, float* %arrayidx28, align 4, !tbaa !9
  %tv_vertices29 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices29, i32 0, i32 1
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %26 = load float, float* %arrayidx32, align 4, !tbaa !9
  %tv_vertices33 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx34 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices33, i32 0, i32 0
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %27 = load float, float* %arrayidx36, align 4, !tbaa !9
  %sub37 = fsub float %26, %27
  %arrayidx38 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 2
  store float %sub37, float* %arrayidx38, align 4, !tbaa !9
  %tv_vertices39 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices39, i32 0, i32 2
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx40)
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 0
  %28 = load float, float* %arrayidx42, align 4, !tbaa !9
  %tv_vertices43 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx44 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices43, i32 0, i32 0
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx44)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %29 = load float, float* %arrayidx46, align 4, !tbaa !9
  %sub47 = fsub float %28, %29
  %arrayidx48 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 0
  store float %sub47, float* %arrayidx48, align 4, !tbaa !9
  %tv_vertices49 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx50 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices49, i32 0, i32 2
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx50)
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 1
  %30 = load float, float* %arrayidx52, align 4, !tbaa !9
  %tv_vertices53 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx54 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices53, i32 0, i32 0
  %call55 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx54)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %31 = load float, float* %arrayidx56, align 4, !tbaa !9
  %sub57 = fsub float %30, %31
  %arrayidx58 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 1
  store float %sub57, float* %arrayidx58, align 4, !tbaa !9
  %tv_vertices59 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx60 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices59, i32 0, i32 2
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx60)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 2
  %32 = load float, float* %arrayidx62, align 4, !tbaa !9
  %tv_vertices63 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx64 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices63, i32 0, i32 0
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx64)
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 2
  %33 = load float, float* %arrayidx66, align 4, !tbaa !9
  %sub67 = fsub float %32, %33
  %arrayidx68 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 2
  store float %sub67, float* %arrayidx68, align 4, !tbaa !9
  %arrayidx69 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 1
  %34 = load float, float* %arrayidx69, align 4, !tbaa !9
  %arrayidx70 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 2
  %35 = load float, float* %arrayidx70, align 4, !tbaa !9
  %mul = fmul float %34, %35
  %arrayidx71 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 2
  %36 = load float, float* %arrayidx71, align 4, !tbaa !9
  %arrayidx72 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 1
  %37 = load float, float* %arrayidx72, align 4, !tbaa !9
  %mul73 = fmul float %36, %37
  %sub74 = fsub float %mul, %mul73
  %tv_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %38 = bitcast %class.btVector4* %tv_plane to %class.btVector3*
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %38)
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 0
  store float %sub74, float* %arrayidx76, align 4, !tbaa !9
  %arrayidx77 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 2
  %39 = load float, float* %arrayidx77, align 4, !tbaa !9
  %arrayidx78 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 0
  %40 = load float, float* %arrayidx78, align 4, !tbaa !9
  %mul79 = fmul float %39, %40
  %arrayidx80 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 0
  %41 = load float, float* %arrayidx80, align 4, !tbaa !9
  %arrayidx81 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 2
  %42 = load float, float* %arrayidx81, align 4, !tbaa !9
  %mul82 = fmul float %41, %42
  %sub83 = fsub float %mul79, %mul82
  %tv_plane84 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %43 = bitcast %class.btVector4* %tv_plane84 to %class.btVector3*
  %call85 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %43)
  %arrayidx86 = getelementptr inbounds float, float* %call85, i32 1
  store float %sub83, float* %arrayidx86, align 4, !tbaa !9
  %arrayidx87 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 0
  %44 = load float, float* %arrayidx87, align 4, !tbaa !9
  %arrayidx88 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 1
  %45 = load float, float* %arrayidx88, align 4, !tbaa !9
  %mul89 = fmul float %44, %45
  %arrayidx90 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 1
  %46 = load float, float* %arrayidx90, align 4, !tbaa !9
  %arrayidx91 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 0
  %47 = load float, float* %arrayidx91, align 4, !tbaa !9
  %mul92 = fmul float %46, %47
  %sub93 = fsub float %mul89, %mul92
  %tv_plane94 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %48 = bitcast %class.btVector4* %tv_plane94 to %class.btVector3*
  %call95 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %48)
  %arrayidx96 = getelementptr inbounds float, float* %call95, i32 2
  store float %sub93, float* %arrayidx96, align 4, !tbaa !9
  %49 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #4
  %50 = bitcast float* %_pp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #4
  %tv_plane97 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %51 = bitcast %class.btVector4* %tv_plane97 to %class.btVector3*
  %call98 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx99 = getelementptr inbounds float, float* %call98, i32 0
  %52 = load float, float* %arrayidx99, align 4, !tbaa !9
  %tv_plane100 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %53 = bitcast %class.btVector4* %tv_plane100 to %class.btVector3*
  %call101 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 0
  %54 = load float, float* %arrayidx102, align 4, !tbaa !9
  %mul103 = fmul float %52, %54
  %tv_plane104 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %55 = bitcast %class.btVector4* %tv_plane104 to %class.btVector3*
  %call105 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx106 = getelementptr inbounds float, float* %call105, i32 1
  %56 = load float, float* %arrayidx106, align 4, !tbaa !9
  %tv_plane107 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %57 = bitcast %class.btVector4* %tv_plane107 to %class.btVector3*
  %call108 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %57)
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 1
  %58 = load float, float* %arrayidx109, align 4, !tbaa !9
  %mul110 = fmul float %56, %58
  %add111 = fadd float %mul103, %mul110
  %tv_plane112 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %59 = bitcast %class.btVector4* %tv_plane112 to %class.btVector3*
  %call113 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %59)
  %arrayidx114 = getelementptr inbounds float, float* %call113, i32 2
  %60 = load float, float* %arrayidx114, align 4, !tbaa !9
  %tv_plane115 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %61 = bitcast %class.btVector4* %tv_plane115 to %class.btVector3*
  %call116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %61)
  %arrayidx117 = getelementptr inbounds float, float* %call116, i32 2
  %62 = load float, float* %arrayidx117, align 4, !tbaa !9
  %mul118 = fmul float %60, %62
  %add119 = fadd float %add111, %mul118
  store float %add119, float* %_pp, align 4, !tbaa !9
  %63 = load float, float* %_pp, align 4, !tbaa !9
  %cmp = fcmp ole float %63, 0x3E7AD7F2A0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 0x47EFFFFFE0000000, float* %len, align 4, !tbaa !9
  br label %if.end

if.else:                                          ; preds = %entry
  %64 = bitcast float* %_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #4
  %65 = load float, float* %_pp, align 4, !tbaa !9
  %mul120 = fmul float %65, 5.000000e-01
  store float %mul120, float* %_x, align 4, !tbaa !9
  %66 = bitcast i32* %_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #4
  %67 = bitcast float* %_pp to i32*
  %68 = load i32, i32* %67, align 4, !tbaa !16
  %shr = lshr i32 %68, 1
  %sub121 = sub i32 1597463007, %shr
  store i32 %sub121, i32* %_y, align 4, !tbaa !16
  %69 = bitcast i32* %_y to float*
  %70 = load float, float* %69, align 4, !tbaa !9
  store float %70, float* %len, align 4, !tbaa !9
  %71 = load float, float* %len, align 4, !tbaa !9
  %72 = load float, float* %_x, align 4, !tbaa !9
  %73 = load float, float* %len, align 4, !tbaa !9
  %mul122 = fmul float %72, %73
  %74 = load float, float* %len, align 4, !tbaa !9
  %mul123 = fmul float %mul122, %74
  %sub124 = fsub float 1.500000e+00, %mul123
  %mul125 = fmul float %71, %sub124
  store float %mul125, float* %len, align 4, !tbaa !9
  %75 = bitcast i32* %_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  %76 = bitcast float* %_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %77 = bitcast float* %_pp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = load float, float* %len, align 4, !tbaa !9
  %cmp126 = fcmp olt float %78, 0x47EFFFFFE0000000
  br i1 %cmp126, label %if.then127, label %if.end140

if.then127:                                       ; preds = %if.end
  %79 = load float, float* %len, align 4, !tbaa !9
  %tv_plane128 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %80 = bitcast %class.btVector4* %tv_plane128 to %class.btVector3*
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %80)
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 0
  %81 = load float, float* %arrayidx130, align 4, !tbaa !9
  %mul131 = fmul float %81, %79
  store float %mul131, float* %arrayidx130, align 4, !tbaa !9
  %82 = load float, float* %len, align 4, !tbaa !9
  %tv_plane132 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %83 = bitcast %class.btVector4* %tv_plane132 to %class.btVector3*
  %call133 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %83)
  %arrayidx134 = getelementptr inbounds float, float* %call133, i32 1
  %84 = load float, float* %arrayidx134, align 4, !tbaa !9
  %mul135 = fmul float %84, %82
  store float %mul135, float* %arrayidx134, align 4, !tbaa !9
  %85 = load float, float* %len, align 4, !tbaa !9
  %tv_plane136 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %86 = bitcast %class.btVector4* %tv_plane136 to %class.btVector3*
  %call137 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %86)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 2
  %87 = load float, float* %arrayidx138, align 4, !tbaa !9
  %mul139 = fmul float %87, %85
  store float %mul139, float* %arrayidx138, align 4, !tbaa !9
  br label %if.end140

if.end140:                                        ; preds = %if.then127, %if.end
  %88 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast [3 x float]* %_dif2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %89) #4
  %90 = bitcast [3 x float]* %_dif1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %90) #4
  %tv_vertices141 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx142 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices141, i32 0, i32 0
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx142)
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 0
  %91 = load float, float* %arrayidx144, align 4, !tbaa !9
  %tv_plane145 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %92 = bitcast %class.btVector4* %tv_plane145 to %class.btVector3*
  %call146 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %92)
  %arrayidx147 = getelementptr inbounds float, float* %call146, i32 0
  %93 = load float, float* %arrayidx147, align 4, !tbaa !9
  %mul148 = fmul float %91, %93
  %tv_vertices149 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx150 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices149, i32 0, i32 0
  %call151 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx150)
  %arrayidx152 = getelementptr inbounds float, float* %call151, i32 1
  %94 = load float, float* %arrayidx152, align 4, !tbaa !9
  %tv_plane153 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %95 = bitcast %class.btVector4* %tv_plane153 to %class.btVector3*
  %call154 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %95)
  %arrayidx155 = getelementptr inbounds float, float* %call154, i32 1
  %96 = load float, float* %arrayidx155, align 4, !tbaa !9
  %mul156 = fmul float %94, %96
  %add157 = fadd float %mul148, %mul156
  %tv_vertices158 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx159 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices158, i32 0, i32 0
  %call160 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx159)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 2
  %97 = load float, float* %arrayidx161, align 4, !tbaa !9
  %tv_plane162 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %98 = bitcast %class.btVector4* %tv_plane162 to %class.btVector3*
  %call163 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %98)
  %arrayidx164 = getelementptr inbounds float, float* %call163, i32 2
  %99 = load float, float* %arrayidx164, align 4, !tbaa !9
  %mul165 = fmul float %97, %99
  %add166 = fadd float %add157, %mul165
  %tv_plane167 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %100 = bitcast %class.btVector4* %tv_plane167 to %class.btVector3*
  %call168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %100)
  %arrayidx169 = getelementptr inbounds float, float* %call168, i32 3
  store float %add166, float* %arrayidx169, align 4, !tbaa !9
  %tv_plane170 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %101 = bitcast %class.btVector4* %tv_plane170 to %class.btVector3*
  %call171 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %101)
  %arrayidx172 = getelementptr inbounds float, float* %call171, i32 0
  %102 = load float, float* %arrayidx172, align 4, !tbaa !9
  %tu_vertices173 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx174 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices173, i32 0, i32 0
  %call175 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx174)
  %arrayidx176 = getelementptr inbounds float, float* %call175, i32 0
  %103 = load float, float* %arrayidx176, align 4, !tbaa !9
  %mul177 = fmul float %102, %103
  %tv_plane178 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %104 = bitcast %class.btVector4* %tv_plane178 to %class.btVector3*
  %call179 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %104)
  %arrayidx180 = getelementptr inbounds float, float* %call179, i32 1
  %105 = load float, float* %arrayidx180, align 4, !tbaa !9
  %tu_vertices181 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx182 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices181, i32 0, i32 0
  %call183 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx182)
  %arrayidx184 = getelementptr inbounds float, float* %call183, i32 1
  %106 = load float, float* %arrayidx184, align 4, !tbaa !9
  %mul185 = fmul float %105, %106
  %add186 = fadd float %mul177, %mul185
  %tv_plane187 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %107 = bitcast %class.btVector4* %tv_plane187 to %class.btVector3*
  %call188 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %107)
  %arrayidx189 = getelementptr inbounds float, float* %call188, i32 2
  %108 = load float, float* %arrayidx189, align 4, !tbaa !9
  %tu_vertices190 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx191 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices190, i32 0, i32 0
  %call192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx191)
  %arrayidx193 = getelementptr inbounds float, float* %call192, i32 2
  %109 = load float, float* %arrayidx193, align 4, !tbaa !9
  %mul194 = fmul float %108, %109
  %add195 = fadd float %add186, %mul194
  %tv_plane196 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %110 = bitcast %class.btVector4* %tv_plane196 to %class.btVector3*
  %call197 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %110)
  %arrayidx198 = getelementptr inbounds float, float* %call197, i32 3
  %111 = load float, float* %arrayidx198, align 4, !tbaa !9
  %sub199 = fsub float %add195, %111
  %du = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx200 = getelementptr inbounds [4 x float], [4 x float]* %du, i32 0, i32 0
  store float %sub199, float* %arrayidx200, align 4, !tbaa !9
  %tv_plane201 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %112 = bitcast %class.btVector4* %tv_plane201 to %class.btVector3*
  %call202 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %112)
  %arrayidx203 = getelementptr inbounds float, float* %call202, i32 0
  %113 = load float, float* %arrayidx203, align 4, !tbaa !9
  %tu_vertices204 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx205 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices204, i32 0, i32 1
  %call206 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx205)
  %arrayidx207 = getelementptr inbounds float, float* %call206, i32 0
  %114 = load float, float* %arrayidx207, align 4, !tbaa !9
  %mul208 = fmul float %113, %114
  %tv_plane209 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %115 = bitcast %class.btVector4* %tv_plane209 to %class.btVector3*
  %call210 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %115)
  %arrayidx211 = getelementptr inbounds float, float* %call210, i32 1
  %116 = load float, float* %arrayidx211, align 4, !tbaa !9
  %tu_vertices212 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx213 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices212, i32 0, i32 1
  %call214 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx213)
  %arrayidx215 = getelementptr inbounds float, float* %call214, i32 1
  %117 = load float, float* %arrayidx215, align 4, !tbaa !9
  %mul216 = fmul float %116, %117
  %add217 = fadd float %mul208, %mul216
  %tv_plane218 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %118 = bitcast %class.btVector4* %tv_plane218 to %class.btVector3*
  %call219 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %118)
  %arrayidx220 = getelementptr inbounds float, float* %call219, i32 2
  %119 = load float, float* %arrayidx220, align 4, !tbaa !9
  %tu_vertices221 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx222 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices221, i32 0, i32 1
  %call223 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx222)
  %arrayidx224 = getelementptr inbounds float, float* %call223, i32 2
  %120 = load float, float* %arrayidx224, align 4, !tbaa !9
  %mul225 = fmul float %119, %120
  %add226 = fadd float %add217, %mul225
  %tv_plane227 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %121 = bitcast %class.btVector4* %tv_plane227 to %class.btVector3*
  %call228 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %121)
  %arrayidx229 = getelementptr inbounds float, float* %call228, i32 3
  %122 = load float, float* %arrayidx229, align 4, !tbaa !9
  %sub230 = fsub float %add226, %122
  %du231 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx232 = getelementptr inbounds [4 x float], [4 x float]* %du231, i32 0, i32 1
  store float %sub230, float* %arrayidx232, align 4, !tbaa !9
  %tv_plane233 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %123 = bitcast %class.btVector4* %tv_plane233 to %class.btVector3*
  %call234 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %123)
  %arrayidx235 = getelementptr inbounds float, float* %call234, i32 0
  %124 = load float, float* %arrayidx235, align 4, !tbaa !9
  %tu_vertices236 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx237 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices236, i32 0, i32 2
  %call238 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx237)
  %arrayidx239 = getelementptr inbounds float, float* %call238, i32 0
  %125 = load float, float* %arrayidx239, align 4, !tbaa !9
  %mul240 = fmul float %124, %125
  %tv_plane241 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %126 = bitcast %class.btVector4* %tv_plane241 to %class.btVector3*
  %call242 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %126)
  %arrayidx243 = getelementptr inbounds float, float* %call242, i32 1
  %127 = load float, float* %arrayidx243, align 4, !tbaa !9
  %tu_vertices244 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx245 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices244, i32 0, i32 2
  %call246 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx245)
  %arrayidx247 = getelementptr inbounds float, float* %call246, i32 1
  %128 = load float, float* %arrayidx247, align 4, !tbaa !9
  %mul248 = fmul float %127, %128
  %add249 = fadd float %mul240, %mul248
  %tv_plane250 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %129 = bitcast %class.btVector4* %tv_plane250 to %class.btVector3*
  %call251 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %129)
  %arrayidx252 = getelementptr inbounds float, float* %call251, i32 2
  %130 = load float, float* %arrayidx252, align 4, !tbaa !9
  %tu_vertices253 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx254 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices253, i32 0, i32 2
  %call255 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx254)
  %arrayidx256 = getelementptr inbounds float, float* %call255, i32 2
  %131 = load float, float* %arrayidx256, align 4, !tbaa !9
  %mul257 = fmul float %130, %131
  %add258 = fadd float %add249, %mul257
  %tv_plane259 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %132 = bitcast %class.btVector4* %tv_plane259 to %class.btVector3*
  %call260 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %132)
  %arrayidx261 = getelementptr inbounds float, float* %call260, i32 3
  %133 = load float, float* %arrayidx261, align 4, !tbaa !9
  %sub262 = fsub float %add258, %133
  %du263 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx264 = getelementptr inbounds [4 x float], [4 x float]* %du263, i32 0, i32 2
  store float %sub262, float* %arrayidx264, align 4, !tbaa !9
  %du265 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx266 = getelementptr inbounds [4 x float], [4 x float]* %du265, i32 0, i32 0
  %134 = load float, float* %arrayidx266, align 4, !tbaa !9
  %du267 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx268 = getelementptr inbounds [4 x float], [4 x float]* %du267, i32 0, i32 1
  %135 = load float, float* %arrayidx268, align 4, !tbaa !9
  %mul269 = fmul float %134, %135
  %du0du1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 10
  store float %mul269, float* %du0du1, align 4, !tbaa !18
  %du270 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx271 = getelementptr inbounds [4 x float], [4 x float]* %du270, i32 0, i32 0
  %136 = load float, float* %arrayidx271, align 4, !tbaa !9
  %du272 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx273 = getelementptr inbounds [4 x float], [4 x float]* %du272, i32 0, i32 2
  %137 = load float, float* %arrayidx273, align 4, !tbaa !9
  %mul274 = fmul float %136, %137
  %du0du2 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 11
  store float %mul274, float* %du0du2, align 4, !tbaa !19
  %du0du1275 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 10
  %138 = load float, float* %du0du1275, align 4, !tbaa !18
  %cmp276 = fcmp ogt float %138, 0.000000e+00
  br i1 %cmp276, label %land.lhs.true, label %if.else522

land.lhs.true:                                    ; preds = %if.end140
  %du0du2277 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 11
  %139 = load float, float* %du0du2277, align 4, !tbaa !19
  %cmp278 = fcmp ogt float %139, 0.000000e+00
  br i1 %cmp278, label %if.then279, label %if.else522

if.then279:                                       ; preds = %land.lhs.true
  %du280 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx281 = getelementptr inbounds [4 x float], [4 x float]* %du280, i32 0, i32 0
  %140 = load float, float* %arrayidx281, align 4, !tbaa !9
  %cmp282 = fcmp olt float %140, 0.000000e+00
  br i1 %cmp282, label %if.then283, label %if.else475

if.then283:                                       ; preds = %if.then279
  %du284 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx285 = getelementptr inbounds [4 x float], [4 x float]* %du284, i32 0, i32 0
  %141 = load float, float* %arrayidx285, align 4, !tbaa !9
  %du286 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx287 = getelementptr inbounds [4 x float], [4 x float]* %du286, i32 0, i32 1
  %142 = load float, float* %arrayidx287, align 4, !tbaa !9
  %du288 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx289 = getelementptr inbounds [4 x float], [4 x float]* %du288, i32 0, i32 2
  %143 = load float, float* %arrayidx289, align 4, !tbaa !9
  %cmp290 = fcmp olt float %142, %143
  br i1 %cmp290, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then283
  %du291 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx292 = getelementptr inbounds [4 x float], [4 x float]* %du291, i32 0, i32 2
  %144 = load float, float* %arrayidx292, align 4, !tbaa !9
  br label %cond.end

cond.false:                                       ; preds = %if.then283
  %du293 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx294 = getelementptr inbounds [4 x float], [4 x float]* %du293, i32 0, i32 1
  %145 = load float, float* %arrayidx294, align 4, !tbaa !9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %144, %cond.true ], [ %145, %cond.false ]
  %cmp295 = fcmp olt float %141, %cond
  br i1 %cmp295, label %cond.true296, label %cond.false310

cond.true296:                                     ; preds = %cond.end
  %du297 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx298 = getelementptr inbounds [4 x float], [4 x float]* %du297, i32 0, i32 1
  %146 = load float, float* %arrayidx298, align 4, !tbaa !9
  %du299 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx300 = getelementptr inbounds [4 x float], [4 x float]* %du299, i32 0, i32 2
  %147 = load float, float* %arrayidx300, align 4, !tbaa !9
  %cmp301 = fcmp olt float %146, %147
  br i1 %cmp301, label %cond.true302, label %cond.false305

cond.true302:                                     ; preds = %cond.true296
  %du303 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx304 = getelementptr inbounds [4 x float], [4 x float]* %du303, i32 0, i32 2
  %148 = load float, float* %arrayidx304, align 4, !tbaa !9
  br label %cond.end308

cond.false305:                                    ; preds = %cond.true296
  %du306 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx307 = getelementptr inbounds [4 x float], [4 x float]* %du306, i32 0, i32 1
  %149 = load float, float* %arrayidx307, align 4, !tbaa !9
  br label %cond.end308

cond.end308:                                      ; preds = %cond.false305, %cond.true302
  %cond309 = phi float [ %148, %cond.true302 ], [ %149, %cond.false305 ]
  br label %cond.end313

cond.false310:                                    ; preds = %cond.end
  %du311 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx312 = getelementptr inbounds [4 x float], [4 x float]* %du311, i32 0, i32 0
  %150 = load float, float* %arrayidx312, align 4, !tbaa !9
  br label %cond.end313

cond.end313:                                      ; preds = %cond.false310, %cond.end308
  %cond314 = phi float [ %cond309, %cond.end308 ], [ %150, %cond.false310 ]
  %distances = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call315 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances)
  %arrayidx316 = getelementptr inbounds float, float* %call315, i32 0
  store float %cond314, float* %arrayidx316, align 4, !tbaa !9
  %distances317 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call318 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances317)
  %arrayidx319 = getelementptr inbounds float, float* %call318, i32 0
  %151 = load float, float* %arrayidx319, align 4, !tbaa !9
  %fneg = fneg float %151
  %distances320 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call321 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances320)
  %arrayidx322 = getelementptr inbounds float, float* %call321, i32 0
  store float %fneg, float* %arrayidx322, align 4, !tbaa !9
  %distances323 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call324 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances323)
  %arrayidx325 = getelementptr inbounds float, float* %call324, i32 0
  %152 = load float, float* %arrayidx325, align 4, !tbaa !9
  %margin326 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %153 = load float, float* %margin326, align 4, !tbaa !10
  %cmp327 = fcmp ogt float %152, %153
  br i1 %cmp327, label %if.then328, label %if.end329

if.then328:                                       ; preds = %cond.end313
  store i1 false, i1* %retval, align 1
  br label %return

if.end329:                                        ; preds = %cond.end313
  %tv_vertices330 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx331 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices330, i32 0, i32 0
  %call332 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx331)
  %arrayidx333 = getelementptr inbounds float, float* %call332, i32 0
  %154 = load float, float* %arrayidx333, align 4, !tbaa !9
  %tv_vertices334 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx335 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices334, i32 0, i32 1
  %call336 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx335)
  %arrayidx337 = getelementptr inbounds float, float* %call336, i32 0
  %155 = load float, float* %arrayidx337, align 4, !tbaa !9
  %add338 = fadd float %154, %155
  %tv_vertices339 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx340 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices339, i32 0, i32 0
  %call341 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx340)
  %arrayidx342 = getelementptr inbounds float, float* %call341, i32 0
  store float %add338, float* %arrayidx342, align 4, !tbaa !9
  %tv_vertices343 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx344 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices343, i32 0, i32 0
  %call345 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx344)
  %arrayidx346 = getelementptr inbounds float, float* %call345, i32 0
  %156 = load float, float* %arrayidx346, align 4, !tbaa !9
  %tv_vertices347 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx348 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices347, i32 0, i32 1
  %call349 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx348)
  %arrayidx350 = getelementptr inbounds float, float* %call349, i32 0
  %157 = load float, float* %arrayidx350, align 4, !tbaa !9
  %sub351 = fsub float %156, %157
  %tv_vertices352 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx353 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices352, i32 0, i32 1
  %call354 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx353)
  %arrayidx355 = getelementptr inbounds float, float* %call354, i32 0
  store float %sub351, float* %arrayidx355, align 4, !tbaa !9
  %tv_vertices356 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx357 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices356, i32 0, i32 0
  %call358 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx357)
  %arrayidx359 = getelementptr inbounds float, float* %call358, i32 0
  %158 = load float, float* %arrayidx359, align 4, !tbaa !9
  %tv_vertices360 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx361 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices360, i32 0, i32 1
  %call362 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx361)
  %arrayidx363 = getelementptr inbounds float, float* %call362, i32 0
  %159 = load float, float* %arrayidx363, align 4, !tbaa !9
  %sub364 = fsub float %158, %159
  %tv_vertices365 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx366 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices365, i32 0, i32 0
  %call367 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx366)
  %arrayidx368 = getelementptr inbounds float, float* %call367, i32 0
  store float %sub364, float* %arrayidx368, align 4, !tbaa !9
  %tv_vertices369 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx370 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices369, i32 0, i32 0
  %call371 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx370)
  %arrayidx372 = getelementptr inbounds float, float* %call371, i32 1
  %160 = load float, float* %arrayidx372, align 4, !tbaa !9
  %tv_vertices373 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx374 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices373, i32 0, i32 1
  %call375 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx374)
  %arrayidx376 = getelementptr inbounds float, float* %call375, i32 1
  %161 = load float, float* %arrayidx376, align 4, !tbaa !9
  %add377 = fadd float %160, %161
  %tv_vertices378 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx379 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices378, i32 0, i32 0
  %call380 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx379)
  %arrayidx381 = getelementptr inbounds float, float* %call380, i32 1
  store float %add377, float* %arrayidx381, align 4, !tbaa !9
  %tv_vertices382 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx383 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices382, i32 0, i32 0
  %call384 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx383)
  %arrayidx385 = getelementptr inbounds float, float* %call384, i32 1
  %162 = load float, float* %arrayidx385, align 4, !tbaa !9
  %tv_vertices386 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx387 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices386, i32 0, i32 1
  %call388 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx387)
  %arrayidx389 = getelementptr inbounds float, float* %call388, i32 1
  %163 = load float, float* %arrayidx389, align 4, !tbaa !9
  %sub390 = fsub float %162, %163
  %tv_vertices391 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx392 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices391, i32 0, i32 1
  %call393 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx392)
  %arrayidx394 = getelementptr inbounds float, float* %call393, i32 1
  store float %sub390, float* %arrayidx394, align 4, !tbaa !9
  %tv_vertices395 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx396 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices395, i32 0, i32 0
  %call397 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx396)
  %arrayidx398 = getelementptr inbounds float, float* %call397, i32 1
  %164 = load float, float* %arrayidx398, align 4, !tbaa !9
  %tv_vertices399 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx400 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices399, i32 0, i32 1
  %call401 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx400)
  %arrayidx402 = getelementptr inbounds float, float* %call401, i32 1
  %165 = load float, float* %arrayidx402, align 4, !tbaa !9
  %sub403 = fsub float %164, %165
  %tv_vertices404 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx405 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices404, i32 0, i32 0
  %call406 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx405)
  %arrayidx407 = getelementptr inbounds float, float* %call406, i32 1
  store float %sub403, float* %arrayidx407, align 4, !tbaa !9
  %tv_vertices408 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx409 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices408, i32 0, i32 0
  %call410 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx409)
  %arrayidx411 = getelementptr inbounds float, float* %call410, i32 2
  %166 = load float, float* %arrayidx411, align 4, !tbaa !9
  %tv_vertices412 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx413 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices412, i32 0, i32 1
  %call414 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx413)
  %arrayidx415 = getelementptr inbounds float, float* %call414, i32 2
  %167 = load float, float* %arrayidx415, align 4, !tbaa !9
  %add416 = fadd float %166, %167
  %tv_vertices417 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx418 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices417, i32 0, i32 0
  %call419 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx418)
  %arrayidx420 = getelementptr inbounds float, float* %call419, i32 2
  store float %add416, float* %arrayidx420, align 4, !tbaa !9
  %tv_vertices421 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx422 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices421, i32 0, i32 0
  %call423 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx422)
  %arrayidx424 = getelementptr inbounds float, float* %call423, i32 2
  %168 = load float, float* %arrayidx424, align 4, !tbaa !9
  %tv_vertices425 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx426 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices425, i32 0, i32 1
  %call427 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx426)
  %arrayidx428 = getelementptr inbounds float, float* %call427, i32 2
  %169 = load float, float* %arrayidx428, align 4, !tbaa !9
  %sub429 = fsub float %168, %169
  %tv_vertices430 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx431 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices430, i32 0, i32 1
  %call432 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx431)
  %arrayidx433 = getelementptr inbounds float, float* %call432, i32 2
  store float %sub429, float* %arrayidx433, align 4, !tbaa !9
  %tv_vertices434 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx435 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices434, i32 0, i32 0
  %call436 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx435)
  %arrayidx437 = getelementptr inbounds float, float* %call436, i32 2
  %170 = load float, float* %arrayidx437, align 4, !tbaa !9
  %tv_vertices438 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx439 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices438, i32 0, i32 1
  %call440 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx439)
  %arrayidx441 = getelementptr inbounds float, float* %call440, i32 2
  %171 = load float, float* %arrayidx441, align 4, !tbaa !9
  %sub442 = fsub float %170, %171
  %tv_vertices443 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx444 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices443, i32 0, i32 0
  %call445 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx444)
  %arrayidx446 = getelementptr inbounds float, float* %call445, i32 2
  store float %sub442, float* %arrayidx446, align 4, !tbaa !9
  %tv_plane447 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %172 = bitcast %class.btVector4* %tv_plane447 to %class.btVector3*
  %call448 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %172)
  %arrayidx449 = getelementptr inbounds float, float* %call448, i32 0
  %173 = load float, float* %arrayidx449, align 4, !tbaa !9
  %mul450 = fmul float -1.000000e+00, %173
  %tv_plane451 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %174 = bitcast %class.btVector4* %tv_plane451 to %class.btVector3*
  %call452 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %174)
  %arrayidx453 = getelementptr inbounds float, float* %call452, i32 0
  store float %mul450, float* %arrayidx453, align 4, !tbaa !9
  %tv_plane454 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %175 = bitcast %class.btVector4* %tv_plane454 to %class.btVector3*
  %call455 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %175)
  %arrayidx456 = getelementptr inbounds float, float* %call455, i32 1
  %176 = load float, float* %arrayidx456, align 4, !tbaa !9
  %mul457 = fmul float -1.000000e+00, %176
  %tv_plane458 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %177 = bitcast %class.btVector4* %tv_plane458 to %class.btVector3*
  %call459 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %177)
  %arrayidx460 = getelementptr inbounds float, float* %call459, i32 1
  store float %mul457, float* %arrayidx460, align 4, !tbaa !9
  %tv_plane461 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %178 = bitcast %class.btVector4* %tv_plane461 to %class.btVector3*
  %call462 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %178)
  %arrayidx463 = getelementptr inbounds float, float* %call462, i32 2
  %179 = load float, float* %arrayidx463, align 4, !tbaa !9
  %mul464 = fmul float -1.000000e+00, %179
  %tv_plane465 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %180 = bitcast %class.btVector4* %tv_plane465 to %class.btVector3*
  %call466 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %180)
  %arrayidx467 = getelementptr inbounds float, float* %call466, i32 2
  store float %mul464, float* %arrayidx467, align 4, !tbaa !9
  %tv_plane468 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %181 = bitcast %class.btVector4* %tv_plane468 to %class.btVector3*
  %call469 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %181)
  %arrayidx470 = getelementptr inbounds float, float* %call469, i32 3
  %182 = load float, float* %arrayidx470, align 4, !tbaa !9
  %mul471 = fmul float -1.000000e+00, %182
  %tv_plane472 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %183 = bitcast %class.btVector4* %tv_plane472 to %class.btVector3*
  %call473 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %183)
  %arrayidx474 = getelementptr inbounds float, float* %call473, i32 3
  store float %mul471, float* %arrayidx474, align 4, !tbaa !9
  br label %if.end521

if.else475:                                       ; preds = %if.then279
  %du476 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx477 = getelementptr inbounds [4 x float], [4 x float]* %du476, i32 0, i32 0
  %184 = load float, float* %arrayidx477, align 4, !tbaa !9
  %du478 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx479 = getelementptr inbounds [4 x float], [4 x float]* %du478, i32 0, i32 1
  %185 = load float, float* %arrayidx479, align 4, !tbaa !9
  %du480 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx481 = getelementptr inbounds [4 x float], [4 x float]* %du480, i32 0, i32 2
  %186 = load float, float* %arrayidx481, align 4, !tbaa !9
  %cmp482 = fcmp ogt float %185, %186
  br i1 %cmp482, label %cond.true483, label %cond.false486

cond.true483:                                     ; preds = %if.else475
  %du484 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx485 = getelementptr inbounds [4 x float], [4 x float]* %du484, i32 0, i32 2
  %187 = load float, float* %arrayidx485, align 4, !tbaa !9
  br label %cond.end489

cond.false486:                                    ; preds = %if.else475
  %du487 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx488 = getelementptr inbounds [4 x float], [4 x float]* %du487, i32 0, i32 1
  %188 = load float, float* %arrayidx488, align 4, !tbaa !9
  br label %cond.end489

cond.end489:                                      ; preds = %cond.false486, %cond.true483
  %cond490 = phi float [ %187, %cond.true483 ], [ %188, %cond.false486 ]
  %cmp491 = fcmp ogt float %184, %cond490
  br i1 %cmp491, label %cond.true492, label %cond.false506

cond.true492:                                     ; preds = %cond.end489
  %du493 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx494 = getelementptr inbounds [4 x float], [4 x float]* %du493, i32 0, i32 1
  %189 = load float, float* %arrayidx494, align 4, !tbaa !9
  %du495 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx496 = getelementptr inbounds [4 x float], [4 x float]* %du495, i32 0, i32 2
  %190 = load float, float* %arrayidx496, align 4, !tbaa !9
  %cmp497 = fcmp ogt float %189, %190
  br i1 %cmp497, label %cond.true498, label %cond.false501

cond.true498:                                     ; preds = %cond.true492
  %du499 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx500 = getelementptr inbounds [4 x float], [4 x float]* %du499, i32 0, i32 2
  %191 = load float, float* %arrayidx500, align 4, !tbaa !9
  br label %cond.end504

cond.false501:                                    ; preds = %cond.true492
  %du502 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx503 = getelementptr inbounds [4 x float], [4 x float]* %du502, i32 0, i32 1
  %192 = load float, float* %arrayidx503, align 4, !tbaa !9
  br label %cond.end504

cond.end504:                                      ; preds = %cond.false501, %cond.true498
  %cond505 = phi float [ %191, %cond.true498 ], [ %192, %cond.false501 ]
  br label %cond.end509

cond.false506:                                    ; preds = %cond.end489
  %du507 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx508 = getelementptr inbounds [4 x float], [4 x float]* %du507, i32 0, i32 0
  %193 = load float, float* %arrayidx508, align 4, !tbaa !9
  br label %cond.end509

cond.end509:                                      ; preds = %cond.false506, %cond.end504
  %cond510 = phi float [ %cond505, %cond.end504 ], [ %193, %cond.false506 ]
  %distances511 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call512 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances511)
  %arrayidx513 = getelementptr inbounds float, float* %call512, i32 0
  store float %cond510, float* %arrayidx513, align 4, !tbaa !9
  %distances514 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call515 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances514)
  %arrayidx516 = getelementptr inbounds float, float* %call515, i32 0
  %194 = load float, float* %arrayidx516, align 4, !tbaa !9
  %margin517 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %195 = load float, float* %margin517, align 4, !tbaa !10
  %cmp518 = fcmp ogt float %194, %195
  br i1 %cmp518, label %if.then519, label %if.end520

if.then519:                                       ; preds = %cond.end509
  store i1 false, i1* %retval, align 1
  br label %return

if.end520:                                        ; preds = %cond.end509
  br label %if.end521

if.end521:                                        ; preds = %if.end520, %if.end329
  br label %if.end769

if.else522:                                       ; preds = %land.lhs.true, %if.end140
  %du523 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx524 = getelementptr inbounds [4 x float], [4 x float]* %du523, i32 0, i32 0
  %196 = load float, float* %arrayidx524, align 4, !tbaa !9
  %du525 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx526 = getelementptr inbounds [4 x float], [4 x float]* %du525, i32 0, i32 1
  %197 = load float, float* %arrayidx526, align 4, !tbaa !9
  %add527 = fadd float %196, %197
  %du528 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx529 = getelementptr inbounds [4 x float], [4 x float]* %du528, i32 0, i32 2
  %198 = load float, float* %arrayidx529, align 4, !tbaa !9
  %add530 = fadd float %add527, %198
  %div = fdiv float %add530, 3.000000e+00
  %distances531 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call532 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances531)
  %arrayidx533 = getelementptr inbounds float, float* %call532, i32 0
  store float %div, float* %arrayidx533, align 4, !tbaa !9
  %distances534 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call535 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances534)
  %arrayidx536 = getelementptr inbounds float, float* %call535, i32 0
  %199 = load float, float* %arrayidx536, align 4, !tbaa !9
  %cmp537 = fcmp olt float %199, 0.000000e+00
  br i1 %cmp537, label %if.then538, label %if.else729

if.then538:                                       ; preds = %if.else522
  %tv_vertices539 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx540 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices539, i32 0, i32 0
  %call541 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx540)
  %arrayidx542 = getelementptr inbounds float, float* %call541, i32 0
  %200 = load float, float* %arrayidx542, align 4, !tbaa !9
  %tv_vertices543 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx544 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices543, i32 0, i32 1
  %call545 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx544)
  %arrayidx546 = getelementptr inbounds float, float* %call545, i32 0
  %201 = load float, float* %arrayidx546, align 4, !tbaa !9
  %add547 = fadd float %200, %201
  %tv_vertices548 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx549 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices548, i32 0, i32 0
  %call550 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx549)
  %arrayidx551 = getelementptr inbounds float, float* %call550, i32 0
  store float %add547, float* %arrayidx551, align 4, !tbaa !9
  %tv_vertices552 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx553 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices552, i32 0, i32 0
  %call554 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx553)
  %arrayidx555 = getelementptr inbounds float, float* %call554, i32 0
  %202 = load float, float* %arrayidx555, align 4, !tbaa !9
  %tv_vertices556 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx557 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices556, i32 0, i32 1
  %call558 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx557)
  %arrayidx559 = getelementptr inbounds float, float* %call558, i32 0
  %203 = load float, float* %arrayidx559, align 4, !tbaa !9
  %sub560 = fsub float %202, %203
  %tv_vertices561 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx562 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices561, i32 0, i32 1
  %call563 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx562)
  %arrayidx564 = getelementptr inbounds float, float* %call563, i32 0
  store float %sub560, float* %arrayidx564, align 4, !tbaa !9
  %tv_vertices565 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx566 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices565, i32 0, i32 0
  %call567 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx566)
  %arrayidx568 = getelementptr inbounds float, float* %call567, i32 0
  %204 = load float, float* %arrayidx568, align 4, !tbaa !9
  %tv_vertices569 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx570 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices569, i32 0, i32 1
  %call571 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx570)
  %arrayidx572 = getelementptr inbounds float, float* %call571, i32 0
  %205 = load float, float* %arrayidx572, align 4, !tbaa !9
  %sub573 = fsub float %204, %205
  %tv_vertices574 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx575 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices574, i32 0, i32 0
  %call576 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx575)
  %arrayidx577 = getelementptr inbounds float, float* %call576, i32 0
  store float %sub573, float* %arrayidx577, align 4, !tbaa !9
  %tv_vertices578 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx579 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices578, i32 0, i32 0
  %call580 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx579)
  %arrayidx581 = getelementptr inbounds float, float* %call580, i32 1
  %206 = load float, float* %arrayidx581, align 4, !tbaa !9
  %tv_vertices582 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx583 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices582, i32 0, i32 1
  %call584 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx583)
  %arrayidx585 = getelementptr inbounds float, float* %call584, i32 1
  %207 = load float, float* %arrayidx585, align 4, !tbaa !9
  %add586 = fadd float %206, %207
  %tv_vertices587 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx588 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices587, i32 0, i32 0
  %call589 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx588)
  %arrayidx590 = getelementptr inbounds float, float* %call589, i32 1
  store float %add586, float* %arrayidx590, align 4, !tbaa !9
  %tv_vertices591 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx592 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices591, i32 0, i32 0
  %call593 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx592)
  %arrayidx594 = getelementptr inbounds float, float* %call593, i32 1
  %208 = load float, float* %arrayidx594, align 4, !tbaa !9
  %tv_vertices595 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx596 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices595, i32 0, i32 1
  %call597 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx596)
  %arrayidx598 = getelementptr inbounds float, float* %call597, i32 1
  %209 = load float, float* %arrayidx598, align 4, !tbaa !9
  %sub599 = fsub float %208, %209
  %tv_vertices600 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx601 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices600, i32 0, i32 1
  %call602 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx601)
  %arrayidx603 = getelementptr inbounds float, float* %call602, i32 1
  store float %sub599, float* %arrayidx603, align 4, !tbaa !9
  %tv_vertices604 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx605 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices604, i32 0, i32 0
  %call606 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx605)
  %arrayidx607 = getelementptr inbounds float, float* %call606, i32 1
  %210 = load float, float* %arrayidx607, align 4, !tbaa !9
  %tv_vertices608 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx609 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices608, i32 0, i32 1
  %call610 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx609)
  %arrayidx611 = getelementptr inbounds float, float* %call610, i32 1
  %211 = load float, float* %arrayidx611, align 4, !tbaa !9
  %sub612 = fsub float %210, %211
  %tv_vertices613 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx614 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices613, i32 0, i32 0
  %call615 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx614)
  %arrayidx616 = getelementptr inbounds float, float* %call615, i32 1
  store float %sub612, float* %arrayidx616, align 4, !tbaa !9
  %tv_vertices617 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx618 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices617, i32 0, i32 0
  %call619 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx618)
  %arrayidx620 = getelementptr inbounds float, float* %call619, i32 2
  %212 = load float, float* %arrayidx620, align 4, !tbaa !9
  %tv_vertices621 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx622 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices621, i32 0, i32 1
  %call623 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx622)
  %arrayidx624 = getelementptr inbounds float, float* %call623, i32 2
  %213 = load float, float* %arrayidx624, align 4, !tbaa !9
  %add625 = fadd float %212, %213
  %tv_vertices626 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx627 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices626, i32 0, i32 0
  %call628 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx627)
  %arrayidx629 = getelementptr inbounds float, float* %call628, i32 2
  store float %add625, float* %arrayidx629, align 4, !tbaa !9
  %tv_vertices630 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx631 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices630, i32 0, i32 0
  %call632 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx631)
  %arrayidx633 = getelementptr inbounds float, float* %call632, i32 2
  %214 = load float, float* %arrayidx633, align 4, !tbaa !9
  %tv_vertices634 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx635 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices634, i32 0, i32 1
  %call636 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx635)
  %arrayidx637 = getelementptr inbounds float, float* %call636, i32 2
  %215 = load float, float* %arrayidx637, align 4, !tbaa !9
  %sub638 = fsub float %214, %215
  %tv_vertices639 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx640 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices639, i32 0, i32 1
  %call641 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx640)
  %arrayidx642 = getelementptr inbounds float, float* %call641, i32 2
  store float %sub638, float* %arrayidx642, align 4, !tbaa !9
  %tv_vertices643 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx644 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices643, i32 0, i32 0
  %call645 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx644)
  %arrayidx646 = getelementptr inbounds float, float* %call645, i32 2
  %216 = load float, float* %arrayidx646, align 4, !tbaa !9
  %tv_vertices647 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx648 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices647, i32 0, i32 1
  %call649 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx648)
  %arrayidx650 = getelementptr inbounds float, float* %call649, i32 2
  %217 = load float, float* %arrayidx650, align 4, !tbaa !9
  %sub651 = fsub float %216, %217
  %tv_vertices652 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx653 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices652, i32 0, i32 0
  %call654 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx653)
  %arrayidx655 = getelementptr inbounds float, float* %call654, i32 2
  store float %sub651, float* %arrayidx655, align 4, !tbaa !9
  %tv_plane656 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %218 = bitcast %class.btVector4* %tv_plane656 to %class.btVector3*
  %call657 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %218)
  %arrayidx658 = getelementptr inbounds float, float* %call657, i32 0
  %219 = load float, float* %arrayidx658, align 4, !tbaa !9
  %mul659 = fmul float -1.000000e+00, %219
  %tv_plane660 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %220 = bitcast %class.btVector4* %tv_plane660 to %class.btVector3*
  %call661 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %220)
  %arrayidx662 = getelementptr inbounds float, float* %call661, i32 0
  store float %mul659, float* %arrayidx662, align 4, !tbaa !9
  %tv_plane663 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %221 = bitcast %class.btVector4* %tv_plane663 to %class.btVector3*
  %call664 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %221)
  %arrayidx665 = getelementptr inbounds float, float* %call664, i32 1
  %222 = load float, float* %arrayidx665, align 4, !tbaa !9
  %mul666 = fmul float -1.000000e+00, %222
  %tv_plane667 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %223 = bitcast %class.btVector4* %tv_plane667 to %class.btVector3*
  %call668 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %223)
  %arrayidx669 = getelementptr inbounds float, float* %call668, i32 1
  store float %mul666, float* %arrayidx669, align 4, !tbaa !9
  %tv_plane670 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %224 = bitcast %class.btVector4* %tv_plane670 to %class.btVector3*
  %call671 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %224)
  %arrayidx672 = getelementptr inbounds float, float* %call671, i32 2
  %225 = load float, float* %arrayidx672, align 4, !tbaa !9
  %mul673 = fmul float -1.000000e+00, %225
  %tv_plane674 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %226 = bitcast %class.btVector4* %tv_plane674 to %class.btVector3*
  %call675 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %226)
  %arrayidx676 = getelementptr inbounds float, float* %call675, i32 2
  store float %mul673, float* %arrayidx676, align 4, !tbaa !9
  %tv_plane677 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %227 = bitcast %class.btVector4* %tv_plane677 to %class.btVector3*
  %call678 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %227)
  %arrayidx679 = getelementptr inbounds float, float* %call678, i32 3
  %228 = load float, float* %arrayidx679, align 4, !tbaa !9
  %mul680 = fmul float -1.000000e+00, %228
  %tv_plane681 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %229 = bitcast %class.btVector4* %tv_plane681 to %class.btVector3*
  %call682 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %229)
  %arrayidx683 = getelementptr inbounds float, float* %call682, i32 3
  store float %mul680, float* %arrayidx683, align 4, !tbaa !9
  %du684 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx685 = getelementptr inbounds [4 x float], [4 x float]* %du684, i32 0, i32 0
  %230 = load float, float* %arrayidx685, align 4, !tbaa !9
  %du686 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx687 = getelementptr inbounds [4 x float], [4 x float]* %du686, i32 0, i32 1
  %231 = load float, float* %arrayidx687, align 4, !tbaa !9
  %du688 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx689 = getelementptr inbounds [4 x float], [4 x float]* %du688, i32 0, i32 2
  %232 = load float, float* %arrayidx689, align 4, !tbaa !9
  %cmp690 = fcmp olt float %231, %232
  br i1 %cmp690, label %cond.true691, label %cond.false694

cond.true691:                                     ; preds = %if.then538
  %du692 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx693 = getelementptr inbounds [4 x float], [4 x float]* %du692, i32 0, i32 2
  %233 = load float, float* %arrayidx693, align 4, !tbaa !9
  br label %cond.end697

cond.false694:                                    ; preds = %if.then538
  %du695 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx696 = getelementptr inbounds [4 x float], [4 x float]* %du695, i32 0, i32 1
  %234 = load float, float* %arrayidx696, align 4, !tbaa !9
  br label %cond.end697

cond.end697:                                      ; preds = %cond.false694, %cond.true691
  %cond698 = phi float [ %233, %cond.true691 ], [ %234, %cond.false694 ]
  %cmp699 = fcmp olt float %230, %cond698
  br i1 %cmp699, label %cond.true700, label %cond.false714

cond.true700:                                     ; preds = %cond.end697
  %du701 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx702 = getelementptr inbounds [4 x float], [4 x float]* %du701, i32 0, i32 1
  %235 = load float, float* %arrayidx702, align 4, !tbaa !9
  %du703 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx704 = getelementptr inbounds [4 x float], [4 x float]* %du703, i32 0, i32 2
  %236 = load float, float* %arrayidx704, align 4, !tbaa !9
  %cmp705 = fcmp olt float %235, %236
  br i1 %cmp705, label %cond.true706, label %cond.false709

cond.true706:                                     ; preds = %cond.true700
  %du707 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx708 = getelementptr inbounds [4 x float], [4 x float]* %du707, i32 0, i32 2
  %237 = load float, float* %arrayidx708, align 4, !tbaa !9
  br label %cond.end712

cond.false709:                                    ; preds = %cond.true700
  %du710 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx711 = getelementptr inbounds [4 x float], [4 x float]* %du710, i32 0, i32 1
  %238 = load float, float* %arrayidx711, align 4, !tbaa !9
  br label %cond.end712

cond.end712:                                      ; preds = %cond.false709, %cond.true706
  %cond713 = phi float [ %237, %cond.true706 ], [ %238, %cond.false709 ]
  br label %cond.end717

cond.false714:                                    ; preds = %cond.end697
  %du715 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx716 = getelementptr inbounds [4 x float], [4 x float]* %du715, i32 0, i32 0
  %239 = load float, float* %arrayidx716, align 4, !tbaa !9
  br label %cond.end717

cond.end717:                                      ; preds = %cond.false714, %cond.end712
  %cond718 = phi float [ %cond713, %cond.end712 ], [ %239, %cond.false714 ]
  %distances719 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call720 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances719)
  %arrayidx721 = getelementptr inbounds float, float* %call720, i32 0
  store float %cond718, float* %arrayidx721, align 4, !tbaa !9
  %distances722 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call723 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances722)
  %arrayidx724 = getelementptr inbounds float, float* %call723, i32 0
  %240 = load float, float* %arrayidx724, align 4, !tbaa !9
  %fneg725 = fneg float %240
  %distances726 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call727 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances726)
  %arrayidx728 = getelementptr inbounds float, float* %call727, i32 0
  store float %fneg725, float* %arrayidx728, align 4, !tbaa !9
  br label %if.end768

if.else729:                                       ; preds = %if.else522
  %du730 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx731 = getelementptr inbounds [4 x float], [4 x float]* %du730, i32 0, i32 0
  %241 = load float, float* %arrayidx731, align 4, !tbaa !9
  %du732 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx733 = getelementptr inbounds [4 x float], [4 x float]* %du732, i32 0, i32 1
  %242 = load float, float* %arrayidx733, align 4, !tbaa !9
  %du734 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx735 = getelementptr inbounds [4 x float], [4 x float]* %du734, i32 0, i32 2
  %243 = load float, float* %arrayidx735, align 4, !tbaa !9
  %cmp736 = fcmp ogt float %242, %243
  br i1 %cmp736, label %cond.true737, label %cond.false740

cond.true737:                                     ; preds = %if.else729
  %du738 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx739 = getelementptr inbounds [4 x float], [4 x float]* %du738, i32 0, i32 2
  %244 = load float, float* %arrayidx739, align 4, !tbaa !9
  br label %cond.end743

cond.false740:                                    ; preds = %if.else729
  %du741 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx742 = getelementptr inbounds [4 x float], [4 x float]* %du741, i32 0, i32 1
  %245 = load float, float* %arrayidx742, align 4, !tbaa !9
  br label %cond.end743

cond.end743:                                      ; preds = %cond.false740, %cond.true737
  %cond744 = phi float [ %244, %cond.true737 ], [ %245, %cond.false740 ]
  %cmp745 = fcmp ogt float %241, %cond744
  br i1 %cmp745, label %cond.true746, label %cond.false760

cond.true746:                                     ; preds = %cond.end743
  %du747 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx748 = getelementptr inbounds [4 x float], [4 x float]* %du747, i32 0, i32 1
  %246 = load float, float* %arrayidx748, align 4, !tbaa !9
  %du749 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx750 = getelementptr inbounds [4 x float], [4 x float]* %du749, i32 0, i32 2
  %247 = load float, float* %arrayidx750, align 4, !tbaa !9
  %cmp751 = fcmp ogt float %246, %247
  br i1 %cmp751, label %cond.true752, label %cond.false755

cond.true752:                                     ; preds = %cond.true746
  %du753 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx754 = getelementptr inbounds [4 x float], [4 x float]* %du753, i32 0, i32 2
  %248 = load float, float* %arrayidx754, align 4, !tbaa !9
  br label %cond.end758

cond.false755:                                    ; preds = %cond.true746
  %du756 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx757 = getelementptr inbounds [4 x float], [4 x float]* %du756, i32 0, i32 1
  %249 = load float, float* %arrayidx757, align 4, !tbaa !9
  br label %cond.end758

cond.end758:                                      ; preds = %cond.false755, %cond.true752
  %cond759 = phi float [ %248, %cond.true752 ], [ %249, %cond.false755 ]
  br label %cond.end763

cond.false760:                                    ; preds = %cond.end743
  %du761 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx762 = getelementptr inbounds [4 x float], [4 x float]* %du761, i32 0, i32 0
  %250 = load float, float* %arrayidx762, align 4, !tbaa !9
  br label %cond.end763

cond.end763:                                      ; preds = %cond.false760, %cond.end758
  %cond764 = phi float [ %cond759, %cond.end758 ], [ %250, %cond.false760 ]
  %distances765 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call766 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances765)
  %arrayidx767 = getelementptr inbounds float, float* %call766, i32 0
  store float %cond764, float* %arrayidx767, align 4, !tbaa !9
  br label %if.end768

if.end768:                                        ; preds = %cond.end763, %cond.end717
  br label %if.end769

if.end769:                                        ; preds = %if.end768, %if.end521
  %251 = bitcast [3 x float]* %_dif1770 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %251) #4
  %252 = bitcast [3 x float]* %_dif2771 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %252) #4
  %tu_vertices772 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx773 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices772, i32 0, i32 1
  %call774 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx773)
  %arrayidx775 = getelementptr inbounds float, float* %call774, i32 0
  %253 = load float, float* %arrayidx775, align 4, !tbaa !9
  %tu_vertices776 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx777 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices776, i32 0, i32 0
  %call778 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx777)
  %arrayidx779 = getelementptr inbounds float, float* %call778, i32 0
  %254 = load float, float* %arrayidx779, align 4, !tbaa !9
  %sub780 = fsub float %253, %254
  %arrayidx781 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 0
  store float %sub780, float* %arrayidx781, align 4, !tbaa !9
  %tu_vertices782 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx783 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices782, i32 0, i32 1
  %call784 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx783)
  %arrayidx785 = getelementptr inbounds float, float* %call784, i32 1
  %255 = load float, float* %arrayidx785, align 4, !tbaa !9
  %tu_vertices786 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx787 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices786, i32 0, i32 0
  %call788 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx787)
  %arrayidx789 = getelementptr inbounds float, float* %call788, i32 1
  %256 = load float, float* %arrayidx789, align 4, !tbaa !9
  %sub790 = fsub float %255, %256
  %arrayidx791 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 1
  store float %sub790, float* %arrayidx791, align 4, !tbaa !9
  %tu_vertices792 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx793 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices792, i32 0, i32 1
  %call794 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx793)
  %arrayidx795 = getelementptr inbounds float, float* %call794, i32 2
  %257 = load float, float* %arrayidx795, align 4, !tbaa !9
  %tu_vertices796 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx797 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices796, i32 0, i32 0
  %call798 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx797)
  %arrayidx799 = getelementptr inbounds float, float* %call798, i32 2
  %258 = load float, float* %arrayidx799, align 4, !tbaa !9
  %sub800 = fsub float %257, %258
  %arrayidx801 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 2
  store float %sub800, float* %arrayidx801, align 4, !tbaa !9
  %tu_vertices802 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx803 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices802, i32 0, i32 2
  %call804 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx803)
  %arrayidx805 = getelementptr inbounds float, float* %call804, i32 0
  %259 = load float, float* %arrayidx805, align 4, !tbaa !9
  %tu_vertices806 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx807 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices806, i32 0, i32 0
  %call808 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx807)
  %arrayidx809 = getelementptr inbounds float, float* %call808, i32 0
  %260 = load float, float* %arrayidx809, align 4, !tbaa !9
  %sub810 = fsub float %259, %260
  %arrayidx811 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 0
  store float %sub810, float* %arrayidx811, align 4, !tbaa !9
  %tu_vertices812 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx813 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices812, i32 0, i32 2
  %call814 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx813)
  %arrayidx815 = getelementptr inbounds float, float* %call814, i32 1
  %261 = load float, float* %arrayidx815, align 4, !tbaa !9
  %tu_vertices816 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx817 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices816, i32 0, i32 0
  %call818 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx817)
  %arrayidx819 = getelementptr inbounds float, float* %call818, i32 1
  %262 = load float, float* %arrayidx819, align 4, !tbaa !9
  %sub820 = fsub float %261, %262
  %arrayidx821 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 1
  store float %sub820, float* %arrayidx821, align 4, !tbaa !9
  %tu_vertices822 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx823 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices822, i32 0, i32 2
  %call824 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx823)
  %arrayidx825 = getelementptr inbounds float, float* %call824, i32 2
  %263 = load float, float* %arrayidx825, align 4, !tbaa !9
  %tu_vertices826 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx827 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices826, i32 0, i32 0
  %call828 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx827)
  %arrayidx829 = getelementptr inbounds float, float* %call828, i32 2
  %264 = load float, float* %arrayidx829, align 4, !tbaa !9
  %sub830 = fsub float %263, %264
  %arrayidx831 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 2
  store float %sub830, float* %arrayidx831, align 4, !tbaa !9
  %arrayidx832 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 1
  %265 = load float, float* %arrayidx832, align 4, !tbaa !9
  %arrayidx833 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 2
  %266 = load float, float* %arrayidx833, align 4, !tbaa !9
  %mul834 = fmul float %265, %266
  %arrayidx835 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 2
  %267 = load float, float* %arrayidx835, align 4, !tbaa !9
  %arrayidx836 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 1
  %268 = load float, float* %arrayidx836, align 4, !tbaa !9
  %mul837 = fmul float %267, %268
  %sub838 = fsub float %mul834, %mul837
  %tu_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %269 = bitcast %class.btVector4* %tu_plane to %class.btVector3*
  %call839 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %269)
  %arrayidx840 = getelementptr inbounds float, float* %call839, i32 0
  store float %sub838, float* %arrayidx840, align 4, !tbaa !9
  %arrayidx841 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 2
  %270 = load float, float* %arrayidx841, align 4, !tbaa !9
  %arrayidx842 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 0
  %271 = load float, float* %arrayidx842, align 4, !tbaa !9
  %mul843 = fmul float %270, %271
  %arrayidx844 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 0
  %272 = load float, float* %arrayidx844, align 4, !tbaa !9
  %arrayidx845 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 2
  %273 = load float, float* %arrayidx845, align 4, !tbaa !9
  %mul846 = fmul float %272, %273
  %sub847 = fsub float %mul843, %mul846
  %tu_plane848 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %274 = bitcast %class.btVector4* %tu_plane848 to %class.btVector3*
  %call849 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %274)
  %arrayidx850 = getelementptr inbounds float, float* %call849, i32 1
  store float %sub847, float* %arrayidx850, align 4, !tbaa !9
  %arrayidx851 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 0
  %275 = load float, float* %arrayidx851, align 4, !tbaa !9
  %arrayidx852 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 1
  %276 = load float, float* %arrayidx852, align 4, !tbaa !9
  %mul853 = fmul float %275, %276
  %arrayidx854 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 1
  %277 = load float, float* %arrayidx854, align 4, !tbaa !9
  %arrayidx855 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 0
  %278 = load float, float* %arrayidx855, align 4, !tbaa !9
  %mul856 = fmul float %277, %278
  %sub857 = fsub float %mul853, %mul856
  %tu_plane858 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %279 = bitcast %class.btVector4* %tu_plane858 to %class.btVector3*
  %call859 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %279)
  %arrayidx860 = getelementptr inbounds float, float* %call859, i32 2
  store float %sub857, float* %arrayidx860, align 4, !tbaa !9
  %280 = bitcast float* %len861 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %280) #4
  %281 = bitcast float* %_pp862 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %281) #4
  %tu_plane863 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %282 = bitcast %class.btVector4* %tu_plane863 to %class.btVector3*
  %call864 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %282)
  %arrayidx865 = getelementptr inbounds float, float* %call864, i32 0
  %283 = load float, float* %arrayidx865, align 4, !tbaa !9
  %tu_plane866 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %284 = bitcast %class.btVector4* %tu_plane866 to %class.btVector3*
  %call867 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %284)
  %arrayidx868 = getelementptr inbounds float, float* %call867, i32 0
  %285 = load float, float* %arrayidx868, align 4, !tbaa !9
  %mul869 = fmul float %283, %285
  %tu_plane870 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %286 = bitcast %class.btVector4* %tu_plane870 to %class.btVector3*
  %call871 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %286)
  %arrayidx872 = getelementptr inbounds float, float* %call871, i32 1
  %287 = load float, float* %arrayidx872, align 4, !tbaa !9
  %tu_plane873 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %288 = bitcast %class.btVector4* %tu_plane873 to %class.btVector3*
  %call874 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %288)
  %arrayidx875 = getelementptr inbounds float, float* %call874, i32 1
  %289 = load float, float* %arrayidx875, align 4, !tbaa !9
  %mul876 = fmul float %287, %289
  %add877 = fadd float %mul869, %mul876
  %tu_plane878 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %290 = bitcast %class.btVector4* %tu_plane878 to %class.btVector3*
  %call879 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %290)
  %arrayidx880 = getelementptr inbounds float, float* %call879, i32 2
  %291 = load float, float* %arrayidx880, align 4, !tbaa !9
  %tu_plane881 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %292 = bitcast %class.btVector4* %tu_plane881 to %class.btVector3*
  %call882 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %292)
  %arrayidx883 = getelementptr inbounds float, float* %call882, i32 2
  %293 = load float, float* %arrayidx883, align 4, !tbaa !9
  %mul884 = fmul float %291, %293
  %add885 = fadd float %add877, %mul884
  store float %add885, float* %_pp862, align 4, !tbaa !9
  %294 = load float, float* %_pp862, align 4, !tbaa !9
  %cmp886 = fcmp ole float %294, 0x3E7AD7F2A0000000
  br i1 %cmp886, label %if.then887, label %if.else888

if.then887:                                       ; preds = %if.end769
  store float 0x47EFFFFFE0000000, float* %len861, align 4, !tbaa !9
  br label %if.end898

if.else888:                                       ; preds = %if.end769
  %295 = bitcast float* %_x889 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %295) #4
  %296 = load float, float* %_pp862, align 4, !tbaa !9
  %mul890 = fmul float %296, 5.000000e-01
  store float %mul890, float* %_x889, align 4, !tbaa !9
  %297 = bitcast i32* %_y891 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %297) #4
  %298 = bitcast float* %_pp862 to i32*
  %299 = load i32, i32* %298, align 4, !tbaa !16
  %shr892 = lshr i32 %299, 1
  %sub893 = sub i32 1597463007, %shr892
  store i32 %sub893, i32* %_y891, align 4, !tbaa !16
  %300 = bitcast i32* %_y891 to float*
  %301 = load float, float* %300, align 4, !tbaa !9
  store float %301, float* %len861, align 4, !tbaa !9
  %302 = load float, float* %len861, align 4, !tbaa !9
  %303 = load float, float* %_x889, align 4, !tbaa !9
  %304 = load float, float* %len861, align 4, !tbaa !9
  %mul894 = fmul float %303, %304
  %305 = load float, float* %len861, align 4, !tbaa !9
  %mul895 = fmul float %mul894, %305
  %sub896 = fsub float 1.500000e+00, %mul895
  %mul897 = fmul float %302, %sub896
  store float %mul897, float* %len861, align 4, !tbaa !9
  %306 = bitcast i32* %_y891 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #4
  %307 = bitcast float* %_x889 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %307) #4
  br label %if.end898

if.end898:                                        ; preds = %if.else888, %if.then887
  %308 = bitcast float* %_pp862 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #4
  %309 = load float, float* %len861, align 4, !tbaa !9
  %cmp899 = fcmp olt float %309, 0x47EFFFFFE0000000
  br i1 %cmp899, label %if.then900, label %if.end913

if.then900:                                       ; preds = %if.end898
  %310 = load float, float* %len861, align 4, !tbaa !9
  %tu_plane901 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %311 = bitcast %class.btVector4* %tu_plane901 to %class.btVector3*
  %call902 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %311)
  %arrayidx903 = getelementptr inbounds float, float* %call902, i32 0
  %312 = load float, float* %arrayidx903, align 4, !tbaa !9
  %mul904 = fmul float %312, %310
  store float %mul904, float* %arrayidx903, align 4, !tbaa !9
  %313 = load float, float* %len861, align 4, !tbaa !9
  %tu_plane905 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %314 = bitcast %class.btVector4* %tu_plane905 to %class.btVector3*
  %call906 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %314)
  %arrayidx907 = getelementptr inbounds float, float* %call906, i32 1
  %315 = load float, float* %arrayidx907, align 4, !tbaa !9
  %mul908 = fmul float %315, %313
  store float %mul908, float* %arrayidx907, align 4, !tbaa !9
  %316 = load float, float* %len861, align 4, !tbaa !9
  %tu_plane909 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %317 = bitcast %class.btVector4* %tu_plane909 to %class.btVector3*
  %call910 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %317)
  %arrayidx911 = getelementptr inbounds float, float* %call910, i32 2
  %318 = load float, float* %arrayidx911, align 4, !tbaa !9
  %mul912 = fmul float %318, %316
  store float %mul912, float* %arrayidx911, align 4, !tbaa !9
  br label %if.end913

if.end913:                                        ; preds = %if.then900, %if.end898
  %319 = bitcast float* %len861 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #4
  %320 = bitcast [3 x float]* %_dif2771 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %320) #4
  %321 = bitcast [3 x float]* %_dif1770 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %321) #4
  %tu_vertices914 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx915 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices914, i32 0, i32 0
  %call916 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx915)
  %arrayidx917 = getelementptr inbounds float, float* %call916, i32 0
  %322 = load float, float* %arrayidx917, align 4, !tbaa !9
  %tu_plane918 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %323 = bitcast %class.btVector4* %tu_plane918 to %class.btVector3*
  %call919 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %323)
  %arrayidx920 = getelementptr inbounds float, float* %call919, i32 0
  %324 = load float, float* %arrayidx920, align 4, !tbaa !9
  %mul921 = fmul float %322, %324
  %tu_vertices922 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx923 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices922, i32 0, i32 0
  %call924 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx923)
  %arrayidx925 = getelementptr inbounds float, float* %call924, i32 1
  %325 = load float, float* %arrayidx925, align 4, !tbaa !9
  %tu_plane926 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %326 = bitcast %class.btVector4* %tu_plane926 to %class.btVector3*
  %call927 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %326)
  %arrayidx928 = getelementptr inbounds float, float* %call927, i32 1
  %327 = load float, float* %arrayidx928, align 4, !tbaa !9
  %mul929 = fmul float %325, %327
  %add930 = fadd float %mul921, %mul929
  %tu_vertices931 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx932 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices931, i32 0, i32 0
  %call933 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx932)
  %arrayidx934 = getelementptr inbounds float, float* %call933, i32 2
  %328 = load float, float* %arrayidx934, align 4, !tbaa !9
  %tu_plane935 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %329 = bitcast %class.btVector4* %tu_plane935 to %class.btVector3*
  %call936 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %329)
  %arrayidx937 = getelementptr inbounds float, float* %call936, i32 2
  %330 = load float, float* %arrayidx937, align 4, !tbaa !9
  %mul938 = fmul float %328, %330
  %add939 = fadd float %add930, %mul938
  %tu_plane940 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %331 = bitcast %class.btVector4* %tu_plane940 to %class.btVector3*
  %call941 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %331)
  %arrayidx942 = getelementptr inbounds float, float* %call941, i32 3
  store float %add939, float* %arrayidx942, align 4, !tbaa !9
  %tu_plane943 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %332 = bitcast %class.btVector4* %tu_plane943 to %class.btVector3*
  %call944 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %332)
  %arrayidx945 = getelementptr inbounds float, float* %call944, i32 0
  %333 = load float, float* %arrayidx945, align 4, !tbaa !9
  %tv_vertices946 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx947 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices946, i32 0, i32 0
  %call948 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx947)
  %arrayidx949 = getelementptr inbounds float, float* %call948, i32 0
  %334 = load float, float* %arrayidx949, align 4, !tbaa !9
  %mul950 = fmul float %333, %334
  %tu_plane951 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %335 = bitcast %class.btVector4* %tu_plane951 to %class.btVector3*
  %call952 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %335)
  %arrayidx953 = getelementptr inbounds float, float* %call952, i32 1
  %336 = load float, float* %arrayidx953, align 4, !tbaa !9
  %tv_vertices954 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx955 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices954, i32 0, i32 0
  %call956 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx955)
  %arrayidx957 = getelementptr inbounds float, float* %call956, i32 1
  %337 = load float, float* %arrayidx957, align 4, !tbaa !9
  %mul958 = fmul float %336, %337
  %add959 = fadd float %mul950, %mul958
  %tu_plane960 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %338 = bitcast %class.btVector4* %tu_plane960 to %class.btVector3*
  %call961 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %338)
  %arrayidx962 = getelementptr inbounds float, float* %call961, i32 2
  %339 = load float, float* %arrayidx962, align 4, !tbaa !9
  %tv_vertices963 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx964 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices963, i32 0, i32 0
  %call965 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx964)
  %arrayidx966 = getelementptr inbounds float, float* %call965, i32 2
  %340 = load float, float* %arrayidx966, align 4, !tbaa !9
  %mul967 = fmul float %339, %340
  %add968 = fadd float %add959, %mul967
  %tu_plane969 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %341 = bitcast %class.btVector4* %tu_plane969 to %class.btVector3*
  %call970 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %341)
  %arrayidx971 = getelementptr inbounds float, float* %call970, i32 3
  %342 = load float, float* %arrayidx971, align 4, !tbaa !9
  %sub972 = fsub float %add968, %342
  %dv = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx973 = getelementptr inbounds [4 x float], [4 x float]* %dv, i32 0, i32 0
  store float %sub972, float* %arrayidx973, align 4, !tbaa !9
  %tu_plane974 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %343 = bitcast %class.btVector4* %tu_plane974 to %class.btVector3*
  %call975 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %343)
  %arrayidx976 = getelementptr inbounds float, float* %call975, i32 0
  %344 = load float, float* %arrayidx976, align 4, !tbaa !9
  %tv_vertices977 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx978 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices977, i32 0, i32 1
  %call979 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx978)
  %arrayidx980 = getelementptr inbounds float, float* %call979, i32 0
  %345 = load float, float* %arrayidx980, align 4, !tbaa !9
  %mul981 = fmul float %344, %345
  %tu_plane982 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %346 = bitcast %class.btVector4* %tu_plane982 to %class.btVector3*
  %call983 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %346)
  %arrayidx984 = getelementptr inbounds float, float* %call983, i32 1
  %347 = load float, float* %arrayidx984, align 4, !tbaa !9
  %tv_vertices985 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx986 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices985, i32 0, i32 1
  %call987 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx986)
  %arrayidx988 = getelementptr inbounds float, float* %call987, i32 1
  %348 = load float, float* %arrayidx988, align 4, !tbaa !9
  %mul989 = fmul float %347, %348
  %add990 = fadd float %mul981, %mul989
  %tu_plane991 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %349 = bitcast %class.btVector4* %tu_plane991 to %class.btVector3*
  %call992 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %349)
  %arrayidx993 = getelementptr inbounds float, float* %call992, i32 2
  %350 = load float, float* %arrayidx993, align 4, !tbaa !9
  %tv_vertices994 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx995 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices994, i32 0, i32 1
  %call996 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx995)
  %arrayidx997 = getelementptr inbounds float, float* %call996, i32 2
  %351 = load float, float* %arrayidx997, align 4, !tbaa !9
  %mul998 = fmul float %350, %351
  %add999 = fadd float %add990, %mul998
  %tu_plane1000 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %352 = bitcast %class.btVector4* %tu_plane1000 to %class.btVector3*
  %call1001 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %352)
  %arrayidx1002 = getelementptr inbounds float, float* %call1001, i32 3
  %353 = load float, float* %arrayidx1002, align 4, !tbaa !9
  %sub1003 = fsub float %add999, %353
  %dv1004 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1005 = getelementptr inbounds [4 x float], [4 x float]* %dv1004, i32 0, i32 1
  store float %sub1003, float* %arrayidx1005, align 4, !tbaa !9
  %tu_plane1006 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %354 = bitcast %class.btVector4* %tu_plane1006 to %class.btVector3*
  %call1007 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %354)
  %arrayidx1008 = getelementptr inbounds float, float* %call1007, i32 0
  %355 = load float, float* %arrayidx1008, align 4, !tbaa !9
  %tv_vertices1009 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx1010 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1009, i32 0, i32 2
  %call1011 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1010)
  %arrayidx1012 = getelementptr inbounds float, float* %call1011, i32 0
  %356 = load float, float* %arrayidx1012, align 4, !tbaa !9
  %mul1013 = fmul float %355, %356
  %tu_plane1014 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %357 = bitcast %class.btVector4* %tu_plane1014 to %class.btVector3*
  %call1015 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %357)
  %arrayidx1016 = getelementptr inbounds float, float* %call1015, i32 1
  %358 = load float, float* %arrayidx1016, align 4, !tbaa !9
  %tv_vertices1017 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx1018 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1017, i32 0, i32 2
  %call1019 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1018)
  %arrayidx1020 = getelementptr inbounds float, float* %call1019, i32 1
  %359 = load float, float* %arrayidx1020, align 4, !tbaa !9
  %mul1021 = fmul float %358, %359
  %add1022 = fadd float %mul1013, %mul1021
  %tu_plane1023 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %360 = bitcast %class.btVector4* %tu_plane1023 to %class.btVector3*
  %call1024 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %360)
  %arrayidx1025 = getelementptr inbounds float, float* %call1024, i32 2
  %361 = load float, float* %arrayidx1025, align 4, !tbaa !9
  %tv_vertices1026 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx1027 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1026, i32 0, i32 2
  %call1028 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1027)
  %arrayidx1029 = getelementptr inbounds float, float* %call1028, i32 2
  %362 = load float, float* %arrayidx1029, align 4, !tbaa !9
  %mul1030 = fmul float %361, %362
  %add1031 = fadd float %add1022, %mul1030
  %tu_plane1032 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %363 = bitcast %class.btVector4* %tu_plane1032 to %class.btVector3*
  %call1033 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %363)
  %arrayidx1034 = getelementptr inbounds float, float* %call1033, i32 3
  %364 = load float, float* %arrayidx1034, align 4, !tbaa !9
  %sub1035 = fsub float %add1031, %364
  %dv1036 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1037 = getelementptr inbounds [4 x float], [4 x float]* %dv1036, i32 0, i32 2
  store float %sub1035, float* %arrayidx1037, align 4, !tbaa !9
  %dv1038 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1039 = getelementptr inbounds [4 x float], [4 x float]* %dv1038, i32 0, i32 0
  %365 = load float, float* %arrayidx1039, align 4, !tbaa !9
  %dv1040 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1041 = getelementptr inbounds [4 x float], [4 x float]* %dv1040, i32 0, i32 1
  %366 = load float, float* %arrayidx1041, align 4, !tbaa !9
  %mul1042 = fmul float %365, %366
  %dv0dv1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 13
  store float %mul1042, float* %dv0dv1, align 4, !tbaa !20
  %dv1043 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1044 = getelementptr inbounds [4 x float], [4 x float]* %dv1043, i32 0, i32 0
  %367 = load float, float* %arrayidx1044, align 4, !tbaa !9
  %dv1045 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1046 = getelementptr inbounds [4 x float], [4 x float]* %dv1045, i32 0, i32 2
  %368 = load float, float* %arrayidx1046, align 4, !tbaa !9
  %mul1047 = fmul float %367, %368
  %dv0dv2 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 14
  store float %mul1047, float* %dv0dv2, align 4, !tbaa !21
  %dv0dv11048 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 13
  %369 = load float, float* %dv0dv11048, align 4, !tbaa !20
  %cmp1049 = fcmp ogt float %369, 0.000000e+00
  br i1 %cmp1049, label %land.lhs.true1050, label %if.else1302

land.lhs.true1050:                                ; preds = %if.end913
  %dv0dv21051 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 14
  %370 = load float, float* %dv0dv21051, align 4, !tbaa !21
  %cmp1052 = fcmp ogt float %370, 0.000000e+00
  br i1 %cmp1052, label %if.then1053, label %if.else1302

if.then1053:                                      ; preds = %land.lhs.true1050
  %dv1054 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1055 = getelementptr inbounds [4 x float], [4 x float]* %dv1054, i32 0, i32 0
  %371 = load float, float* %arrayidx1055, align 4, !tbaa !9
  %cmp1056 = fcmp olt float %371, 0.000000e+00
  br i1 %cmp1056, label %if.then1057, label %if.else1255

if.then1057:                                      ; preds = %if.then1053
  %dv1058 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1059 = getelementptr inbounds [4 x float], [4 x float]* %dv1058, i32 0, i32 0
  %372 = load float, float* %arrayidx1059, align 4, !tbaa !9
  %dv1060 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1061 = getelementptr inbounds [4 x float], [4 x float]* %dv1060, i32 0, i32 1
  %373 = load float, float* %arrayidx1061, align 4, !tbaa !9
  %dv1062 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1063 = getelementptr inbounds [4 x float], [4 x float]* %dv1062, i32 0, i32 2
  %374 = load float, float* %arrayidx1063, align 4, !tbaa !9
  %cmp1064 = fcmp olt float %373, %374
  br i1 %cmp1064, label %cond.true1065, label %cond.false1068

cond.true1065:                                    ; preds = %if.then1057
  %dv1066 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1067 = getelementptr inbounds [4 x float], [4 x float]* %dv1066, i32 0, i32 2
  %375 = load float, float* %arrayidx1067, align 4, !tbaa !9
  br label %cond.end1071

cond.false1068:                                   ; preds = %if.then1057
  %dv1069 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1070 = getelementptr inbounds [4 x float], [4 x float]* %dv1069, i32 0, i32 1
  %376 = load float, float* %arrayidx1070, align 4, !tbaa !9
  br label %cond.end1071

cond.end1071:                                     ; preds = %cond.false1068, %cond.true1065
  %cond1072 = phi float [ %375, %cond.true1065 ], [ %376, %cond.false1068 ]
  %cmp1073 = fcmp olt float %372, %cond1072
  br i1 %cmp1073, label %cond.true1074, label %cond.false1088

cond.true1074:                                    ; preds = %cond.end1071
  %dv1075 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1076 = getelementptr inbounds [4 x float], [4 x float]* %dv1075, i32 0, i32 1
  %377 = load float, float* %arrayidx1076, align 4, !tbaa !9
  %dv1077 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1078 = getelementptr inbounds [4 x float], [4 x float]* %dv1077, i32 0, i32 2
  %378 = load float, float* %arrayidx1078, align 4, !tbaa !9
  %cmp1079 = fcmp olt float %377, %378
  br i1 %cmp1079, label %cond.true1080, label %cond.false1083

cond.true1080:                                    ; preds = %cond.true1074
  %dv1081 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1082 = getelementptr inbounds [4 x float], [4 x float]* %dv1081, i32 0, i32 2
  %379 = load float, float* %arrayidx1082, align 4, !tbaa !9
  br label %cond.end1086

cond.false1083:                                   ; preds = %cond.true1074
  %dv1084 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1085 = getelementptr inbounds [4 x float], [4 x float]* %dv1084, i32 0, i32 1
  %380 = load float, float* %arrayidx1085, align 4, !tbaa !9
  br label %cond.end1086

cond.end1086:                                     ; preds = %cond.false1083, %cond.true1080
  %cond1087 = phi float [ %379, %cond.true1080 ], [ %380, %cond.false1083 ]
  br label %cond.end1091

cond.false1088:                                   ; preds = %cond.end1071
  %dv1089 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1090 = getelementptr inbounds [4 x float], [4 x float]* %dv1089, i32 0, i32 0
  %381 = load float, float* %arrayidx1090, align 4, !tbaa !9
  br label %cond.end1091

cond.end1091:                                     ; preds = %cond.false1088, %cond.end1086
  %cond1092 = phi float [ %cond1087, %cond.end1086 ], [ %381, %cond.false1088 ]
  %distances1093 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1094 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1093)
  %arrayidx1095 = getelementptr inbounds float, float* %call1094, i32 1
  store float %cond1092, float* %arrayidx1095, align 4, !tbaa !9
  %distances1096 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1097 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1096)
  %arrayidx1098 = getelementptr inbounds float, float* %call1097, i32 1
  %382 = load float, float* %arrayidx1098, align 4, !tbaa !9
  %fneg1099 = fneg float %382
  %distances1100 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1101 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1100)
  %arrayidx1102 = getelementptr inbounds float, float* %call1101, i32 1
  store float %fneg1099, float* %arrayidx1102, align 4, !tbaa !9
  %distances1103 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1104 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1103)
  %arrayidx1105 = getelementptr inbounds float, float* %call1104, i32 1
  %383 = load float, float* %arrayidx1105, align 4, !tbaa !9
  %margin1106 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %384 = load float, float* %margin1106, align 4, !tbaa !10
  %cmp1107 = fcmp ogt float %383, %384
  br i1 %cmp1107, label %if.then1108, label %if.end1109

if.then1108:                                      ; preds = %cond.end1091
  store i1 false, i1* %retval, align 1
  br label %return

if.end1109:                                       ; preds = %cond.end1091
  %tu_vertices1110 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1110, i32 0, i32 0
  %call1112 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1111)
  %arrayidx1113 = getelementptr inbounds float, float* %call1112, i32 0
  %385 = load float, float* %arrayidx1113, align 4, !tbaa !9
  %tu_vertices1114 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1115 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1114, i32 0, i32 1
  %call1116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1115)
  %arrayidx1117 = getelementptr inbounds float, float* %call1116, i32 0
  %386 = load float, float* %arrayidx1117, align 4, !tbaa !9
  %add1118 = fadd float %385, %386
  %tu_vertices1119 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1120 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1119, i32 0, i32 0
  %call1121 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1120)
  %arrayidx1122 = getelementptr inbounds float, float* %call1121, i32 0
  store float %add1118, float* %arrayidx1122, align 4, !tbaa !9
  %tu_vertices1123 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1123, i32 0, i32 0
  %call1125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1124)
  %arrayidx1126 = getelementptr inbounds float, float* %call1125, i32 0
  %387 = load float, float* %arrayidx1126, align 4, !tbaa !9
  %tu_vertices1127 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1128 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1127, i32 0, i32 1
  %call1129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1128)
  %arrayidx1130 = getelementptr inbounds float, float* %call1129, i32 0
  %388 = load float, float* %arrayidx1130, align 4, !tbaa !9
  %sub1131 = fsub float %387, %388
  %tu_vertices1132 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1133 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1132, i32 0, i32 1
  %call1134 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1133)
  %arrayidx1135 = getelementptr inbounds float, float* %call1134, i32 0
  store float %sub1131, float* %arrayidx1135, align 4, !tbaa !9
  %tu_vertices1136 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1137 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1136, i32 0, i32 0
  %call1138 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1137)
  %arrayidx1139 = getelementptr inbounds float, float* %call1138, i32 0
  %389 = load float, float* %arrayidx1139, align 4, !tbaa !9
  %tu_vertices1140 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1141 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1140, i32 0, i32 1
  %call1142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1141)
  %arrayidx1143 = getelementptr inbounds float, float* %call1142, i32 0
  %390 = load float, float* %arrayidx1143, align 4, !tbaa !9
  %sub1144 = fsub float %389, %390
  %tu_vertices1145 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1146 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1145, i32 0, i32 0
  %call1147 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1146)
  %arrayidx1148 = getelementptr inbounds float, float* %call1147, i32 0
  store float %sub1144, float* %arrayidx1148, align 4, !tbaa !9
  %tu_vertices1149 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1150 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1149, i32 0, i32 0
  %call1151 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1150)
  %arrayidx1152 = getelementptr inbounds float, float* %call1151, i32 1
  %391 = load float, float* %arrayidx1152, align 4, !tbaa !9
  %tu_vertices1153 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1154 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1153, i32 0, i32 1
  %call1155 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1154)
  %arrayidx1156 = getelementptr inbounds float, float* %call1155, i32 1
  %392 = load float, float* %arrayidx1156, align 4, !tbaa !9
  %add1157 = fadd float %391, %392
  %tu_vertices1158 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1159 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1158, i32 0, i32 0
  %call1160 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1159)
  %arrayidx1161 = getelementptr inbounds float, float* %call1160, i32 1
  store float %add1157, float* %arrayidx1161, align 4, !tbaa !9
  %tu_vertices1162 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1163 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1162, i32 0, i32 0
  %call1164 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1163)
  %arrayidx1165 = getelementptr inbounds float, float* %call1164, i32 1
  %393 = load float, float* %arrayidx1165, align 4, !tbaa !9
  %tu_vertices1166 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1167 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1166, i32 0, i32 1
  %call1168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1167)
  %arrayidx1169 = getelementptr inbounds float, float* %call1168, i32 1
  %394 = load float, float* %arrayidx1169, align 4, !tbaa !9
  %sub1170 = fsub float %393, %394
  %tu_vertices1171 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1172 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1171, i32 0, i32 1
  %call1173 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1172)
  %arrayidx1174 = getelementptr inbounds float, float* %call1173, i32 1
  store float %sub1170, float* %arrayidx1174, align 4, !tbaa !9
  %tu_vertices1175 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1176 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1175, i32 0, i32 0
  %call1177 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1176)
  %arrayidx1178 = getelementptr inbounds float, float* %call1177, i32 1
  %395 = load float, float* %arrayidx1178, align 4, !tbaa !9
  %tu_vertices1179 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1180 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1179, i32 0, i32 1
  %call1181 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1180)
  %arrayidx1182 = getelementptr inbounds float, float* %call1181, i32 1
  %396 = load float, float* %arrayidx1182, align 4, !tbaa !9
  %sub1183 = fsub float %395, %396
  %tu_vertices1184 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1185 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1184, i32 0, i32 0
  %call1186 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1185)
  %arrayidx1187 = getelementptr inbounds float, float* %call1186, i32 1
  store float %sub1183, float* %arrayidx1187, align 4, !tbaa !9
  %tu_vertices1188 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1189 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1188, i32 0, i32 0
  %call1190 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1189)
  %arrayidx1191 = getelementptr inbounds float, float* %call1190, i32 2
  %397 = load float, float* %arrayidx1191, align 4, !tbaa !9
  %tu_vertices1192 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1193 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1192, i32 0, i32 1
  %call1194 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1193)
  %arrayidx1195 = getelementptr inbounds float, float* %call1194, i32 2
  %398 = load float, float* %arrayidx1195, align 4, !tbaa !9
  %add1196 = fadd float %397, %398
  %tu_vertices1197 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1198 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1197, i32 0, i32 0
  %call1199 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1198)
  %arrayidx1200 = getelementptr inbounds float, float* %call1199, i32 2
  store float %add1196, float* %arrayidx1200, align 4, !tbaa !9
  %tu_vertices1201 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1202 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1201, i32 0, i32 0
  %call1203 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1202)
  %arrayidx1204 = getelementptr inbounds float, float* %call1203, i32 2
  %399 = load float, float* %arrayidx1204, align 4, !tbaa !9
  %tu_vertices1205 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1206 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1205, i32 0, i32 1
  %call1207 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1206)
  %arrayidx1208 = getelementptr inbounds float, float* %call1207, i32 2
  %400 = load float, float* %arrayidx1208, align 4, !tbaa !9
  %sub1209 = fsub float %399, %400
  %tu_vertices1210 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1211 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1210, i32 0, i32 1
  %call1212 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1211)
  %arrayidx1213 = getelementptr inbounds float, float* %call1212, i32 2
  store float %sub1209, float* %arrayidx1213, align 4, !tbaa !9
  %tu_vertices1214 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1215 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1214, i32 0, i32 0
  %call1216 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1215)
  %arrayidx1217 = getelementptr inbounds float, float* %call1216, i32 2
  %401 = load float, float* %arrayidx1217, align 4, !tbaa !9
  %tu_vertices1218 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1219 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1218, i32 0, i32 1
  %call1220 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1219)
  %arrayidx1221 = getelementptr inbounds float, float* %call1220, i32 2
  %402 = load float, float* %arrayidx1221, align 4, !tbaa !9
  %sub1222 = fsub float %401, %402
  %tu_vertices1223 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1224 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1223, i32 0, i32 0
  %call1225 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1224)
  %arrayidx1226 = getelementptr inbounds float, float* %call1225, i32 2
  store float %sub1222, float* %arrayidx1226, align 4, !tbaa !9
  %tu_plane1227 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %403 = bitcast %class.btVector4* %tu_plane1227 to %class.btVector3*
  %call1228 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %403)
  %arrayidx1229 = getelementptr inbounds float, float* %call1228, i32 0
  %404 = load float, float* %arrayidx1229, align 4, !tbaa !9
  %mul1230 = fmul float -1.000000e+00, %404
  %tu_plane1231 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %405 = bitcast %class.btVector4* %tu_plane1231 to %class.btVector3*
  %call1232 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %405)
  %arrayidx1233 = getelementptr inbounds float, float* %call1232, i32 0
  store float %mul1230, float* %arrayidx1233, align 4, !tbaa !9
  %tu_plane1234 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %406 = bitcast %class.btVector4* %tu_plane1234 to %class.btVector3*
  %call1235 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %406)
  %arrayidx1236 = getelementptr inbounds float, float* %call1235, i32 1
  %407 = load float, float* %arrayidx1236, align 4, !tbaa !9
  %mul1237 = fmul float -1.000000e+00, %407
  %tu_plane1238 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %408 = bitcast %class.btVector4* %tu_plane1238 to %class.btVector3*
  %call1239 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %408)
  %arrayidx1240 = getelementptr inbounds float, float* %call1239, i32 1
  store float %mul1237, float* %arrayidx1240, align 4, !tbaa !9
  %tu_plane1241 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %409 = bitcast %class.btVector4* %tu_plane1241 to %class.btVector3*
  %call1242 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %409)
  %arrayidx1243 = getelementptr inbounds float, float* %call1242, i32 2
  %410 = load float, float* %arrayidx1243, align 4, !tbaa !9
  %mul1244 = fmul float -1.000000e+00, %410
  %tu_plane1245 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %411 = bitcast %class.btVector4* %tu_plane1245 to %class.btVector3*
  %call1246 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %411)
  %arrayidx1247 = getelementptr inbounds float, float* %call1246, i32 2
  store float %mul1244, float* %arrayidx1247, align 4, !tbaa !9
  %tu_plane1248 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %412 = bitcast %class.btVector4* %tu_plane1248 to %class.btVector3*
  %call1249 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %412)
  %arrayidx1250 = getelementptr inbounds float, float* %call1249, i32 3
  %413 = load float, float* %arrayidx1250, align 4, !tbaa !9
  %mul1251 = fmul float -1.000000e+00, %413
  %tu_plane1252 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %414 = bitcast %class.btVector4* %tu_plane1252 to %class.btVector3*
  %call1253 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %414)
  %arrayidx1254 = getelementptr inbounds float, float* %call1253, i32 3
  store float %mul1251, float* %arrayidx1254, align 4, !tbaa !9
  br label %if.end1301

if.else1255:                                      ; preds = %if.then1053
  %dv1256 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1257 = getelementptr inbounds [4 x float], [4 x float]* %dv1256, i32 0, i32 0
  %415 = load float, float* %arrayidx1257, align 4, !tbaa !9
  %dv1258 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1259 = getelementptr inbounds [4 x float], [4 x float]* %dv1258, i32 0, i32 1
  %416 = load float, float* %arrayidx1259, align 4, !tbaa !9
  %dv1260 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1261 = getelementptr inbounds [4 x float], [4 x float]* %dv1260, i32 0, i32 2
  %417 = load float, float* %arrayidx1261, align 4, !tbaa !9
  %cmp1262 = fcmp ogt float %416, %417
  br i1 %cmp1262, label %cond.true1263, label %cond.false1266

cond.true1263:                                    ; preds = %if.else1255
  %dv1264 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1265 = getelementptr inbounds [4 x float], [4 x float]* %dv1264, i32 0, i32 2
  %418 = load float, float* %arrayidx1265, align 4, !tbaa !9
  br label %cond.end1269

cond.false1266:                                   ; preds = %if.else1255
  %dv1267 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1268 = getelementptr inbounds [4 x float], [4 x float]* %dv1267, i32 0, i32 1
  %419 = load float, float* %arrayidx1268, align 4, !tbaa !9
  br label %cond.end1269

cond.end1269:                                     ; preds = %cond.false1266, %cond.true1263
  %cond1270 = phi float [ %418, %cond.true1263 ], [ %419, %cond.false1266 ]
  %cmp1271 = fcmp ogt float %415, %cond1270
  br i1 %cmp1271, label %cond.true1272, label %cond.false1286

cond.true1272:                                    ; preds = %cond.end1269
  %dv1273 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1274 = getelementptr inbounds [4 x float], [4 x float]* %dv1273, i32 0, i32 1
  %420 = load float, float* %arrayidx1274, align 4, !tbaa !9
  %dv1275 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1276 = getelementptr inbounds [4 x float], [4 x float]* %dv1275, i32 0, i32 2
  %421 = load float, float* %arrayidx1276, align 4, !tbaa !9
  %cmp1277 = fcmp ogt float %420, %421
  br i1 %cmp1277, label %cond.true1278, label %cond.false1281

cond.true1278:                                    ; preds = %cond.true1272
  %dv1279 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1280 = getelementptr inbounds [4 x float], [4 x float]* %dv1279, i32 0, i32 2
  %422 = load float, float* %arrayidx1280, align 4, !tbaa !9
  br label %cond.end1284

cond.false1281:                                   ; preds = %cond.true1272
  %dv1282 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1283 = getelementptr inbounds [4 x float], [4 x float]* %dv1282, i32 0, i32 1
  %423 = load float, float* %arrayidx1283, align 4, !tbaa !9
  br label %cond.end1284

cond.end1284:                                     ; preds = %cond.false1281, %cond.true1278
  %cond1285 = phi float [ %422, %cond.true1278 ], [ %423, %cond.false1281 ]
  br label %cond.end1289

cond.false1286:                                   ; preds = %cond.end1269
  %dv1287 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1288 = getelementptr inbounds [4 x float], [4 x float]* %dv1287, i32 0, i32 0
  %424 = load float, float* %arrayidx1288, align 4, !tbaa !9
  br label %cond.end1289

cond.end1289:                                     ; preds = %cond.false1286, %cond.end1284
  %cond1290 = phi float [ %cond1285, %cond.end1284 ], [ %424, %cond.false1286 ]
  %distances1291 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1292 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1291)
  %arrayidx1293 = getelementptr inbounds float, float* %call1292, i32 1
  store float %cond1290, float* %arrayidx1293, align 4, !tbaa !9
  %distances1294 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1295 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1294)
  %arrayidx1296 = getelementptr inbounds float, float* %call1295, i32 1
  %425 = load float, float* %arrayidx1296, align 4, !tbaa !9
  %margin1297 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %426 = load float, float* %margin1297, align 4, !tbaa !10
  %cmp1298 = fcmp ogt float %425, %426
  br i1 %cmp1298, label %if.then1299, label %if.end1300

if.then1299:                                      ; preds = %cond.end1289
  store i1 false, i1* %retval, align 1
  br label %return

if.end1300:                                       ; preds = %cond.end1289
  br label %if.end1301

if.end1301:                                       ; preds = %if.end1300, %if.end1109
  br label %if.end1550

if.else1302:                                      ; preds = %land.lhs.true1050, %if.end913
  %dv1303 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1304 = getelementptr inbounds [4 x float], [4 x float]* %dv1303, i32 0, i32 0
  %427 = load float, float* %arrayidx1304, align 4, !tbaa !9
  %dv1305 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1306 = getelementptr inbounds [4 x float], [4 x float]* %dv1305, i32 0, i32 1
  %428 = load float, float* %arrayidx1306, align 4, !tbaa !9
  %add1307 = fadd float %427, %428
  %dv1308 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1309 = getelementptr inbounds [4 x float], [4 x float]* %dv1308, i32 0, i32 2
  %429 = load float, float* %arrayidx1309, align 4, !tbaa !9
  %add1310 = fadd float %add1307, %429
  %div1311 = fdiv float %add1310, 3.000000e+00
  %distances1312 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1313 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1312)
  %arrayidx1314 = getelementptr inbounds float, float* %call1313, i32 1
  store float %div1311, float* %arrayidx1314, align 4, !tbaa !9
  %distances1315 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1316 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1315)
  %arrayidx1317 = getelementptr inbounds float, float* %call1316, i32 1
  %430 = load float, float* %arrayidx1317, align 4, !tbaa !9
  %cmp1318 = fcmp olt float %430, 0.000000e+00
  br i1 %cmp1318, label %if.then1319, label %if.else1510

if.then1319:                                      ; preds = %if.else1302
  %tu_vertices1320 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1321 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1320, i32 0, i32 0
  %call1322 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1321)
  %arrayidx1323 = getelementptr inbounds float, float* %call1322, i32 0
  %431 = load float, float* %arrayidx1323, align 4, !tbaa !9
  %tu_vertices1324 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1325 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1324, i32 0, i32 1
  %call1326 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1325)
  %arrayidx1327 = getelementptr inbounds float, float* %call1326, i32 0
  %432 = load float, float* %arrayidx1327, align 4, !tbaa !9
  %add1328 = fadd float %431, %432
  %tu_vertices1329 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1330 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1329, i32 0, i32 0
  %call1331 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1330)
  %arrayidx1332 = getelementptr inbounds float, float* %call1331, i32 0
  store float %add1328, float* %arrayidx1332, align 4, !tbaa !9
  %tu_vertices1333 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1334 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1333, i32 0, i32 0
  %call1335 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1334)
  %arrayidx1336 = getelementptr inbounds float, float* %call1335, i32 0
  %433 = load float, float* %arrayidx1336, align 4, !tbaa !9
  %tu_vertices1337 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1338 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1337, i32 0, i32 1
  %call1339 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1338)
  %arrayidx1340 = getelementptr inbounds float, float* %call1339, i32 0
  %434 = load float, float* %arrayidx1340, align 4, !tbaa !9
  %sub1341 = fsub float %433, %434
  %tu_vertices1342 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1343 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1342, i32 0, i32 1
  %call1344 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1343)
  %arrayidx1345 = getelementptr inbounds float, float* %call1344, i32 0
  store float %sub1341, float* %arrayidx1345, align 4, !tbaa !9
  %tu_vertices1346 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1347 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1346, i32 0, i32 0
  %call1348 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1347)
  %arrayidx1349 = getelementptr inbounds float, float* %call1348, i32 0
  %435 = load float, float* %arrayidx1349, align 4, !tbaa !9
  %tu_vertices1350 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1351 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1350, i32 0, i32 1
  %call1352 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1351)
  %arrayidx1353 = getelementptr inbounds float, float* %call1352, i32 0
  %436 = load float, float* %arrayidx1353, align 4, !tbaa !9
  %sub1354 = fsub float %435, %436
  %tu_vertices1355 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1356 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1355, i32 0, i32 0
  %call1357 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1356)
  %arrayidx1358 = getelementptr inbounds float, float* %call1357, i32 0
  store float %sub1354, float* %arrayidx1358, align 4, !tbaa !9
  %tu_vertices1359 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1360 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1359, i32 0, i32 0
  %call1361 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1360)
  %arrayidx1362 = getelementptr inbounds float, float* %call1361, i32 1
  %437 = load float, float* %arrayidx1362, align 4, !tbaa !9
  %tu_vertices1363 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1364 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1363, i32 0, i32 1
  %call1365 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1364)
  %arrayidx1366 = getelementptr inbounds float, float* %call1365, i32 1
  %438 = load float, float* %arrayidx1366, align 4, !tbaa !9
  %add1367 = fadd float %437, %438
  %tu_vertices1368 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1369 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1368, i32 0, i32 0
  %call1370 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1369)
  %arrayidx1371 = getelementptr inbounds float, float* %call1370, i32 1
  store float %add1367, float* %arrayidx1371, align 4, !tbaa !9
  %tu_vertices1372 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1373 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1372, i32 0, i32 0
  %call1374 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1373)
  %arrayidx1375 = getelementptr inbounds float, float* %call1374, i32 1
  %439 = load float, float* %arrayidx1375, align 4, !tbaa !9
  %tu_vertices1376 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1377 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1376, i32 0, i32 1
  %call1378 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1377)
  %arrayidx1379 = getelementptr inbounds float, float* %call1378, i32 1
  %440 = load float, float* %arrayidx1379, align 4, !tbaa !9
  %sub1380 = fsub float %439, %440
  %tu_vertices1381 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1382 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1381, i32 0, i32 1
  %call1383 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1382)
  %arrayidx1384 = getelementptr inbounds float, float* %call1383, i32 1
  store float %sub1380, float* %arrayidx1384, align 4, !tbaa !9
  %tu_vertices1385 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1386 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1385, i32 0, i32 0
  %call1387 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1386)
  %arrayidx1388 = getelementptr inbounds float, float* %call1387, i32 1
  %441 = load float, float* %arrayidx1388, align 4, !tbaa !9
  %tu_vertices1389 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1390 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1389, i32 0, i32 1
  %call1391 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1390)
  %arrayidx1392 = getelementptr inbounds float, float* %call1391, i32 1
  %442 = load float, float* %arrayidx1392, align 4, !tbaa !9
  %sub1393 = fsub float %441, %442
  %tu_vertices1394 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1395 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1394, i32 0, i32 0
  %call1396 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1395)
  %arrayidx1397 = getelementptr inbounds float, float* %call1396, i32 1
  store float %sub1393, float* %arrayidx1397, align 4, !tbaa !9
  %tu_vertices1398 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1399 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1398, i32 0, i32 0
  %call1400 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1399)
  %arrayidx1401 = getelementptr inbounds float, float* %call1400, i32 2
  %443 = load float, float* %arrayidx1401, align 4, !tbaa !9
  %tu_vertices1402 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1403 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1402, i32 0, i32 1
  %call1404 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1403)
  %arrayidx1405 = getelementptr inbounds float, float* %call1404, i32 2
  %444 = load float, float* %arrayidx1405, align 4, !tbaa !9
  %add1406 = fadd float %443, %444
  %tu_vertices1407 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1408 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1407, i32 0, i32 0
  %call1409 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1408)
  %arrayidx1410 = getelementptr inbounds float, float* %call1409, i32 2
  store float %add1406, float* %arrayidx1410, align 4, !tbaa !9
  %tu_vertices1411 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1412 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1411, i32 0, i32 0
  %call1413 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1412)
  %arrayidx1414 = getelementptr inbounds float, float* %call1413, i32 2
  %445 = load float, float* %arrayidx1414, align 4, !tbaa !9
  %tu_vertices1415 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1416 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1415, i32 0, i32 1
  %call1417 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1416)
  %arrayidx1418 = getelementptr inbounds float, float* %call1417, i32 2
  %446 = load float, float* %arrayidx1418, align 4, !tbaa !9
  %sub1419 = fsub float %445, %446
  %tu_vertices1420 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1421 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1420, i32 0, i32 1
  %call1422 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1421)
  %arrayidx1423 = getelementptr inbounds float, float* %call1422, i32 2
  store float %sub1419, float* %arrayidx1423, align 4, !tbaa !9
  %tu_vertices1424 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1425 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1424, i32 0, i32 0
  %call1426 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1425)
  %arrayidx1427 = getelementptr inbounds float, float* %call1426, i32 2
  %447 = load float, float* %arrayidx1427, align 4, !tbaa !9
  %tu_vertices1428 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1429 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1428, i32 0, i32 1
  %call1430 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1429)
  %arrayidx1431 = getelementptr inbounds float, float* %call1430, i32 2
  %448 = load float, float* %arrayidx1431, align 4, !tbaa !9
  %sub1432 = fsub float %447, %448
  %tu_vertices1433 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1434 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1433, i32 0, i32 0
  %call1435 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1434)
  %arrayidx1436 = getelementptr inbounds float, float* %call1435, i32 2
  store float %sub1432, float* %arrayidx1436, align 4, !tbaa !9
  %tu_plane1437 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %449 = bitcast %class.btVector4* %tu_plane1437 to %class.btVector3*
  %call1438 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %449)
  %arrayidx1439 = getelementptr inbounds float, float* %call1438, i32 0
  %450 = load float, float* %arrayidx1439, align 4, !tbaa !9
  %mul1440 = fmul float -1.000000e+00, %450
  %tu_plane1441 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %451 = bitcast %class.btVector4* %tu_plane1441 to %class.btVector3*
  %call1442 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %451)
  %arrayidx1443 = getelementptr inbounds float, float* %call1442, i32 0
  store float %mul1440, float* %arrayidx1443, align 4, !tbaa !9
  %tu_plane1444 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %452 = bitcast %class.btVector4* %tu_plane1444 to %class.btVector3*
  %call1445 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %452)
  %arrayidx1446 = getelementptr inbounds float, float* %call1445, i32 1
  %453 = load float, float* %arrayidx1446, align 4, !tbaa !9
  %mul1447 = fmul float -1.000000e+00, %453
  %tu_plane1448 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %454 = bitcast %class.btVector4* %tu_plane1448 to %class.btVector3*
  %call1449 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %454)
  %arrayidx1450 = getelementptr inbounds float, float* %call1449, i32 1
  store float %mul1447, float* %arrayidx1450, align 4, !tbaa !9
  %tu_plane1451 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %455 = bitcast %class.btVector4* %tu_plane1451 to %class.btVector3*
  %call1452 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %455)
  %arrayidx1453 = getelementptr inbounds float, float* %call1452, i32 2
  %456 = load float, float* %arrayidx1453, align 4, !tbaa !9
  %mul1454 = fmul float -1.000000e+00, %456
  %tu_plane1455 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %457 = bitcast %class.btVector4* %tu_plane1455 to %class.btVector3*
  %call1456 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %457)
  %arrayidx1457 = getelementptr inbounds float, float* %call1456, i32 2
  store float %mul1454, float* %arrayidx1457, align 4, !tbaa !9
  %tu_plane1458 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %458 = bitcast %class.btVector4* %tu_plane1458 to %class.btVector3*
  %call1459 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %458)
  %arrayidx1460 = getelementptr inbounds float, float* %call1459, i32 3
  %459 = load float, float* %arrayidx1460, align 4, !tbaa !9
  %mul1461 = fmul float -1.000000e+00, %459
  %tu_plane1462 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %460 = bitcast %class.btVector4* %tu_plane1462 to %class.btVector3*
  %call1463 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %460)
  %arrayidx1464 = getelementptr inbounds float, float* %call1463, i32 3
  store float %mul1461, float* %arrayidx1464, align 4, !tbaa !9
  %dv1465 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1466 = getelementptr inbounds [4 x float], [4 x float]* %dv1465, i32 0, i32 0
  %461 = load float, float* %arrayidx1466, align 4, !tbaa !9
  %dv1467 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1468 = getelementptr inbounds [4 x float], [4 x float]* %dv1467, i32 0, i32 1
  %462 = load float, float* %arrayidx1468, align 4, !tbaa !9
  %dv1469 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1470 = getelementptr inbounds [4 x float], [4 x float]* %dv1469, i32 0, i32 2
  %463 = load float, float* %arrayidx1470, align 4, !tbaa !9
  %cmp1471 = fcmp olt float %462, %463
  br i1 %cmp1471, label %cond.true1472, label %cond.false1475

cond.true1472:                                    ; preds = %if.then1319
  %dv1473 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1474 = getelementptr inbounds [4 x float], [4 x float]* %dv1473, i32 0, i32 2
  %464 = load float, float* %arrayidx1474, align 4, !tbaa !9
  br label %cond.end1478

cond.false1475:                                   ; preds = %if.then1319
  %dv1476 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1477 = getelementptr inbounds [4 x float], [4 x float]* %dv1476, i32 0, i32 1
  %465 = load float, float* %arrayidx1477, align 4, !tbaa !9
  br label %cond.end1478

cond.end1478:                                     ; preds = %cond.false1475, %cond.true1472
  %cond1479 = phi float [ %464, %cond.true1472 ], [ %465, %cond.false1475 ]
  %cmp1480 = fcmp olt float %461, %cond1479
  br i1 %cmp1480, label %cond.true1481, label %cond.false1495

cond.true1481:                                    ; preds = %cond.end1478
  %dv1482 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1483 = getelementptr inbounds [4 x float], [4 x float]* %dv1482, i32 0, i32 1
  %466 = load float, float* %arrayidx1483, align 4, !tbaa !9
  %dv1484 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1485 = getelementptr inbounds [4 x float], [4 x float]* %dv1484, i32 0, i32 2
  %467 = load float, float* %arrayidx1485, align 4, !tbaa !9
  %cmp1486 = fcmp olt float %466, %467
  br i1 %cmp1486, label %cond.true1487, label %cond.false1490

cond.true1487:                                    ; preds = %cond.true1481
  %dv1488 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1489 = getelementptr inbounds [4 x float], [4 x float]* %dv1488, i32 0, i32 2
  %468 = load float, float* %arrayidx1489, align 4, !tbaa !9
  br label %cond.end1493

cond.false1490:                                   ; preds = %cond.true1481
  %dv1491 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1492 = getelementptr inbounds [4 x float], [4 x float]* %dv1491, i32 0, i32 1
  %469 = load float, float* %arrayidx1492, align 4, !tbaa !9
  br label %cond.end1493

cond.end1493:                                     ; preds = %cond.false1490, %cond.true1487
  %cond1494 = phi float [ %468, %cond.true1487 ], [ %469, %cond.false1490 ]
  br label %cond.end1498

cond.false1495:                                   ; preds = %cond.end1478
  %dv1496 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1497 = getelementptr inbounds [4 x float], [4 x float]* %dv1496, i32 0, i32 0
  %470 = load float, float* %arrayidx1497, align 4, !tbaa !9
  br label %cond.end1498

cond.end1498:                                     ; preds = %cond.false1495, %cond.end1493
  %cond1499 = phi float [ %cond1494, %cond.end1493 ], [ %470, %cond.false1495 ]
  %distances1500 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1501 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1500)
  %arrayidx1502 = getelementptr inbounds float, float* %call1501, i32 1
  store float %cond1499, float* %arrayidx1502, align 4, !tbaa !9
  %distances1503 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1504 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1503)
  %arrayidx1505 = getelementptr inbounds float, float* %call1504, i32 1
  %471 = load float, float* %arrayidx1505, align 4, !tbaa !9
  %fneg1506 = fneg float %471
  %distances1507 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1508 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1507)
  %arrayidx1509 = getelementptr inbounds float, float* %call1508, i32 1
  store float %fneg1506, float* %arrayidx1509, align 4, !tbaa !9
  br label %if.end1549

if.else1510:                                      ; preds = %if.else1302
  %dv1511 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1512 = getelementptr inbounds [4 x float], [4 x float]* %dv1511, i32 0, i32 0
  %472 = load float, float* %arrayidx1512, align 4, !tbaa !9
  %dv1513 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1514 = getelementptr inbounds [4 x float], [4 x float]* %dv1513, i32 0, i32 1
  %473 = load float, float* %arrayidx1514, align 4, !tbaa !9
  %dv1515 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1516 = getelementptr inbounds [4 x float], [4 x float]* %dv1515, i32 0, i32 2
  %474 = load float, float* %arrayidx1516, align 4, !tbaa !9
  %cmp1517 = fcmp ogt float %473, %474
  br i1 %cmp1517, label %cond.true1518, label %cond.false1521

cond.true1518:                                    ; preds = %if.else1510
  %dv1519 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1520 = getelementptr inbounds [4 x float], [4 x float]* %dv1519, i32 0, i32 2
  %475 = load float, float* %arrayidx1520, align 4, !tbaa !9
  br label %cond.end1524

cond.false1521:                                   ; preds = %if.else1510
  %dv1522 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1523 = getelementptr inbounds [4 x float], [4 x float]* %dv1522, i32 0, i32 1
  %476 = load float, float* %arrayidx1523, align 4, !tbaa !9
  br label %cond.end1524

cond.end1524:                                     ; preds = %cond.false1521, %cond.true1518
  %cond1525 = phi float [ %475, %cond.true1518 ], [ %476, %cond.false1521 ]
  %cmp1526 = fcmp ogt float %472, %cond1525
  br i1 %cmp1526, label %cond.true1527, label %cond.false1541

cond.true1527:                                    ; preds = %cond.end1524
  %dv1528 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1529 = getelementptr inbounds [4 x float], [4 x float]* %dv1528, i32 0, i32 1
  %477 = load float, float* %arrayidx1529, align 4, !tbaa !9
  %dv1530 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1531 = getelementptr inbounds [4 x float], [4 x float]* %dv1530, i32 0, i32 2
  %478 = load float, float* %arrayidx1531, align 4, !tbaa !9
  %cmp1532 = fcmp ogt float %477, %478
  br i1 %cmp1532, label %cond.true1533, label %cond.false1536

cond.true1533:                                    ; preds = %cond.true1527
  %dv1534 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1535 = getelementptr inbounds [4 x float], [4 x float]* %dv1534, i32 0, i32 2
  %479 = load float, float* %arrayidx1535, align 4, !tbaa !9
  br label %cond.end1539

cond.false1536:                                   ; preds = %cond.true1527
  %dv1537 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1538 = getelementptr inbounds [4 x float], [4 x float]* %dv1537, i32 0, i32 1
  %480 = load float, float* %arrayidx1538, align 4, !tbaa !9
  br label %cond.end1539

cond.end1539:                                     ; preds = %cond.false1536, %cond.true1533
  %cond1540 = phi float [ %479, %cond.true1533 ], [ %480, %cond.false1536 ]
  br label %cond.end1544

cond.false1541:                                   ; preds = %cond.end1524
  %dv1542 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1543 = getelementptr inbounds [4 x float], [4 x float]* %dv1542, i32 0, i32 0
  %481 = load float, float* %arrayidx1543, align 4, !tbaa !9
  br label %cond.end1544

cond.end1544:                                     ; preds = %cond.false1541, %cond.end1539
  %cond1545 = phi float [ %cond1540, %cond.end1539 ], [ %481, %cond.false1541 ]
  %distances1546 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1547 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1546)
  %arrayidx1548 = getelementptr inbounds float, float* %call1547, i32 1
  store float %cond1545, float* %arrayidx1548, align 4, !tbaa !9
  br label %if.end1549

if.end1549:                                       ; preds = %cond.end1544, %cond.end1498
  br label %if.end1550

if.end1550:                                       ; preds = %if.end1549, %if.end1301
  %482 = bitcast i32* %bl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %482) #4
  store i32 0, i32* %bl, align 4, !tbaa !16
  %distances1551 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1552 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1551)
  %arrayidx1553 = getelementptr inbounds float, float* %call1552, i32 0
  %483 = load float, float* %arrayidx1553, align 4, !tbaa !9
  %distances1554 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1555 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1554)
  %arrayidx1556 = getelementptr inbounds float, float* %call1555, i32 1
  %484 = load float, float* %arrayidx1556, align 4, !tbaa !9
  %cmp1557 = fcmp olt float %483, %484
  br i1 %cmp1557, label %if.then1558, label %if.end1559

if.then1558:                                      ; preds = %if.end1550
  store i32 1, i32* %bl, align 4, !tbaa !16
  br label %if.end1559

if.end1559:                                       ; preds = %if.then1558, %if.end1550
  %485 = load i32, i32* %bl, align 4, !tbaa !16
  %cmp1560 = icmp eq i32 %485, 2
  br i1 %cmp1560, label %if.then1561, label %if.end1592

if.then1561:                                      ; preds = %if.end1559
  %distances1562 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1563 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1562)
  %arrayidx1564 = getelementptr inbounds float, float* %call1563, i32 2
  %486 = load float, float* %arrayidx1564, align 4, !tbaa !9
  %margin1565 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %487 = load float, float* %margin1565, align 4, !tbaa !10
  %cmp1566 = fcmp ogt float %486, %487
  br i1 %cmp1566, label %if.then1567, label %if.end1568

if.then1567:                                      ; preds = %if.then1561
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1631

if.end1568:                                       ; preds = %if.then1561
  %distances1569 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1570 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1569)
  %arrayidx1571 = getelementptr inbounds float, float* %call1570, i32 2
  %488 = load float, float* %arrayidx1571, align 4, !tbaa !9
  %fneg1572 = fneg float %488
  %margin1573 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %489 = load float, float* %margin1573, align 4, !tbaa !10
  %add1574 = fadd float %fneg1572, %489
  %490 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %490, i32 0, i32 0
  store float %add1574, float* %m_penetration_depth, align 4, !tbaa !22
  %closest_point_v = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 6
  %491 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %491, i32 0, i32 3
  %arrayidx1575 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 0
  %492 = bitcast %class.btVector3* %arrayidx1575 to i8*
  %493 = bitcast %class.btVector3* %closest_point_v to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %492, i8* align 4 %493, i32 16, i1 false), !tbaa.struct !14
  %494 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %494, i32 0, i32 1
  store i32 1, i32* %m_point_count, align 4, !tbaa !24
  %edge_edge_dir = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call1576 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %edge_edge_dir)
  %arrayidx1577 = getelementptr inbounds float, float* %call1576, i32 0
  %495 = load float, float* %arrayidx1577, align 4, !tbaa !9
  %496 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %496, i32 0, i32 2
  %497 = bitcast %class.btVector4* %m_separating_normal to %class.btVector3*
  %call1578 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %497)
  %arrayidx1579 = getelementptr inbounds float, float* %call1578, i32 0
  store float %495, float* %arrayidx1579, align 4, !tbaa !9
  %edge_edge_dir1580 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call1581 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %edge_edge_dir1580)
  %arrayidx1582 = getelementptr inbounds float, float* %call1581, i32 1
  %498 = load float, float* %arrayidx1582, align 4, !tbaa !9
  %499 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_separating_normal1583 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %499, i32 0, i32 2
  %500 = bitcast %class.btVector4* %m_separating_normal1583 to %class.btVector3*
  %call1584 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %500)
  %arrayidx1585 = getelementptr inbounds float, float* %call1584, i32 1
  store float %498, float* %arrayidx1585, align 4, !tbaa !9
  %edge_edge_dir1586 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call1587 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %edge_edge_dir1586)
  %arrayidx1588 = getelementptr inbounds float, float* %call1587, i32 2
  %501 = load float, float* %arrayidx1588, align 4, !tbaa !9
  %502 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_separating_normal1589 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %502, i32 0, i32 2
  %503 = bitcast %class.btVector4* %m_separating_normal1589 to %class.btVector3*
  %call1590 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %503)
  %arrayidx1591 = getelementptr inbounds float, float* %call1590, i32 2
  store float %501, float* %arrayidx1591, align 4, !tbaa !9
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1631

if.end1592:                                       ; preds = %if.end1559
  %504 = bitcast i32* %point_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %504) #4
  %505 = load i32, i32* %bl, align 4, !tbaa !16
  %cmp1593 = icmp eq i32 %505, 0
  br i1 %cmp1593, label %if.then1594, label %if.else1608

if.then1594:                                      ; preds = %if.end1592
  %tv_plane1595 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %tv_vertices1596 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1596, i32 0, i32 0
  %tu_vertices1597 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arraydecay1598 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1597, i32 0, i32 0
  %contact_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1599 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points, i32 0, i32 0
  %call1600 = call i32 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, %class.btVector4* nonnull align 4 dereferenceable(16) %tv_plane1595, %class.btVector3* %arraydecay, %class.btVector3* %arraydecay1598, %class.btVector3* %arraydecay1599)
  store i32 %call1600, i32* %point_count, align 4, !tbaa !16
  %506 = load i32, i32* %point_count, align 4, !tbaa !16
  %cmp1601 = icmp eq i32 %506, 0
  br i1 %cmp1601, label %if.then1602, label %if.end1603

if.then1602:                                      ; preds = %if.then1594
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end1603:                                       ; preds = %if.then1594
  %507 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %tv_plane1604 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %margin1605 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %508 = load float, float* %margin1605, align 4, !tbaa !10
  %contact_points1606 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1607 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points1606, i32 0, i32 0
  %509 = load i32, i32* %point_count, align 4, !tbaa !16
  call void @_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j(%struct.GIM_TRIANGLE_CONTACT_DATA* %507, %class.btVector4* nonnull align 4 dereferenceable(16) %tv_plane1604, float %508, %class.btVector3* %arraydecay1607, i32 %509)
  br label %if.end1626

if.else1608:                                      ; preds = %if.end1592
  %tu_plane1609 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %tu_vertices1610 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arraydecay1611 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1610, i32 0, i32 0
  %tv_vertices1612 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arraydecay1613 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1612, i32 0, i32 0
  %contact_points1614 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1615 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points1614, i32 0, i32 0
  %call1616 = call i32 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, %class.btVector4* nonnull align 4 dereferenceable(16) %tu_plane1609, %class.btVector3* %arraydecay1611, %class.btVector3* %arraydecay1613, %class.btVector3* %arraydecay1615)
  store i32 %call1616, i32* %point_count, align 4, !tbaa !16
  %510 = load i32, i32* %point_count, align 4, !tbaa !16
  %cmp1617 = icmp eq i32 %510, 0
  br i1 %cmp1617, label %if.then1618, label %if.end1619

if.then1618:                                      ; preds = %if.else1608
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end1619:                                       ; preds = %if.else1608
  %511 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %tu_plane1620 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %margin1621 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %512 = load float, float* %margin1621, align 4, !tbaa !10
  %contact_points1622 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1623 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points1622, i32 0, i32 0
  %513 = load i32, i32* %point_count, align 4, !tbaa !16
  call void @_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j(%struct.GIM_TRIANGLE_CONTACT_DATA* %511, %class.btVector4* nonnull align 4 dereferenceable(16) %tu_plane1620, float %512, %class.btVector3* %arraydecay1623, i32 %513)
  %514 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %514) #4
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !9
  %515 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_separating_normal1624 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %515, i32 0, i32 2
  %516 = bitcast %class.btVector4* %m_separating_normal1624 to %class.btVector3*
  %call1625 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %516, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %517 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %517) #4
  br label %if.end1626

if.end1626:                                       ; preds = %if.end1619, %if.end1603
  %518 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4, !tbaa !2
  %m_point_count1627 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %518, i32 0, i32 1
  %519 = load i32, i32* %m_point_count1627, align 4, !tbaa !24
  %cmp1628 = icmp eq i32 %519, 0
  br i1 %cmp1628, label %if.then1629, label %if.end1630

if.then1629:                                      ; preds = %if.end1626
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end1630:                                       ; preds = %if.end1626
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end1630, %if.then1629, %if.then1618, %if.then1602
  %520 = bitcast i32* %point_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %520) #4
  br label %cleanup1631

cleanup1631:                                      ; preds = %cleanup, %if.end1568, %if.then1567
  %521 = bitcast i32* %bl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %521) #4
  br label %return

return:                                           ; preds = %cleanup1631, %if.then1299, %if.then1108, %if.then519, %if.then328
  %522 = load i1, i1* %retval, align 1
  ret i1 %522
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %0)
  ret %class.btVector4* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %tri_plane, %class.btVector3* %tripoints, %class.btVector3* %srcpoints, %class.btVector3* %clip_points) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  %tri_plane.addr = alloca %class.btVector4*, align 4
  %tripoints.addr = alloca %class.btVector3*, align 4
  %srcpoints.addr = alloca %class.btVector3*, align 4
  %clip_points.addr = alloca %class.btVector3*, align 4
  %edgeplane = alloca %class.btVector4, align 4
  %_dif = alloca [3 x float], align 4
  %len = alloca float, align 4
  %_pp = alloca float, align 4
  %_x = alloca float, align 4
  %_y = alloca i32, align 4
  %clipped_count = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %_dif119 = alloca [3 x float], align 4
  %len177 = alloca float, align 4
  %_pp178 = alloca float, align 4
  %_x199 = alloca float, align 4
  %_y201 = alloca i32, align 4
  %_dif250 = alloca [3 x float], align 4
  %len308 = alloca float, align 4
  %_pp309 = alloca float, align 4
  %_x330 = alloca float, align 4
  %_y332 = alloca i32, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4, !tbaa !2
  store %class.btVector4* %tri_plane, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  store %class.btVector3* %tripoints, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  store %class.btVector3* %srcpoints, %class.btVector3** %srcpoints.addr, align 4, !tbaa !2
  store %class.btVector3* %clip_points, %class.btVector3** %clip_points.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  %0 = bitcast %class.btVector4* %edgeplane to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #4
  %call = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %edgeplane)
  %1 = bitcast [3 x float]* %_dif to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %1) #4
  %2 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 1
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %3 = load float, float* %arrayidx3, align 4, !tbaa !9
  %4 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %5 = load float, float* %arrayidx6, align 4, !tbaa !9
  %sub = fsub float %3, %5
  %arrayidx7 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 0
  store float %sub, float* %arrayidx7, align 4, !tbaa !9
  %6 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 1
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %7 = load float, float* %arrayidx10, align 4, !tbaa !9
  %8 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %9 = load float, float* %arrayidx13, align 4, !tbaa !9
  %sub14 = fsub float %7, %9
  %arrayidx15 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 1
  store float %sub14, float* %arrayidx15, align 4, !tbaa !9
  %10 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 1
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 2
  %11 = load float, float* %arrayidx18, align 4, !tbaa !9
  %12 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 2
  %13 = load float, float* %arrayidx21, align 4, !tbaa !9
  %sub22 = fsub float %11, %13
  %arrayidx23 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 2
  store float %sub22, float* %arrayidx23, align 4, !tbaa !9
  %arrayidx24 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 1
  %14 = load float, float* %arrayidx24, align 4, !tbaa !9
  %15 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector4* %15 to %class.btVector3*
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  %17 = load float, float* %arrayidx26, align 4, !tbaa !9
  %mul = fmul float %14, %17
  %arrayidx27 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 2
  %18 = load float, float* %arrayidx27, align 4, !tbaa !9
  %19 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %20 = bitcast %class.btVector4* %19 to %class.btVector3*
  %call28 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %20)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 1
  %21 = load float, float* %arrayidx29, align 4, !tbaa !9
  %mul30 = fmul float %18, %21
  %sub31 = fsub float %mul, %mul30
  %22 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 0
  store float %sub31, float* %arrayidx33, align 4, !tbaa !9
  %arrayidx34 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 2
  %23 = load float, float* %arrayidx34, align 4, !tbaa !9
  %24 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %25 = bitcast %class.btVector4* %24 to %class.btVector3*
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %26 = load float, float* %arrayidx36, align 4, !tbaa !9
  %mul37 = fmul float %23, %26
  %arrayidx38 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 0
  %27 = load float, float* %arrayidx38, align 4, !tbaa !9
  %28 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %29 = bitcast %class.btVector4* %28 to %class.btVector3*
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %29)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %30 = load float, float* %arrayidx40, align 4, !tbaa !9
  %mul41 = fmul float %27, %30
  %sub42 = fsub float %mul37, %mul41
  %31 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call43 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %31)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 1
  store float %sub42, float* %arrayidx44, align 4, !tbaa !9
  %arrayidx45 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 0
  %32 = load float, float* %arrayidx45, align 4, !tbaa !9
  %33 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %34 = bitcast %class.btVector4* %33 to %class.btVector3*
  %call46 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %34)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 1
  %35 = load float, float* %arrayidx47, align 4, !tbaa !9
  %mul48 = fmul float %32, %35
  %arrayidx49 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 1
  %36 = load float, float* %arrayidx49, align 4, !tbaa !9
  %37 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %38 = bitcast %class.btVector4* %37 to %class.btVector3*
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %39 = load float, float* %arrayidx51, align 4, !tbaa !9
  %mul52 = fmul float %36, %39
  %sub53 = fsub float %mul48, %mul52
  %40 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %40)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  store float %sub53, float* %arrayidx55, align 4, !tbaa !9
  %41 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = bitcast float* %_pp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  %43 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call56 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %43)
  %arrayidx57 = getelementptr inbounds float, float* %call56, i32 0
  %44 = load float, float* %arrayidx57, align 4, !tbaa !9
  %45 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 0
  %46 = load float, float* %arrayidx59, align 4, !tbaa !9
  %mul60 = fmul float %44, %46
  %47 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %47)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 1
  %48 = load float, float* %arrayidx62, align 4, !tbaa !9
  %49 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call63 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 1
  %50 = load float, float* %arrayidx64, align 4, !tbaa !9
  %mul65 = fmul float %48, %50
  %add = fadd float %mul60, %mul65
  %51 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %52 = load float, float* %arrayidx67, align 4, !tbaa !9
  %53 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  %54 = load float, float* %arrayidx69, align 4, !tbaa !9
  %mul70 = fmul float %52, %54
  %add71 = fadd float %add, %mul70
  store float %add71, float* %_pp, align 4, !tbaa !9
  %55 = load float, float* %_pp, align 4, !tbaa !9
  %cmp = fcmp ole float %55, 0x3E7AD7F2A0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 0x47EFFFFFE0000000, float* %len, align 4, !tbaa !9
  br label %if.end

if.else:                                          ; preds = %entry
  %56 = bitcast float* %_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #4
  %57 = load float, float* %_pp, align 4, !tbaa !9
  %mul72 = fmul float %57, 5.000000e-01
  store float %mul72, float* %_x, align 4, !tbaa !9
  %58 = bitcast i32* %_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #4
  %59 = bitcast float* %_pp to i32*
  %60 = load i32, i32* %59, align 4, !tbaa !16
  %shr = lshr i32 %60, 1
  %sub73 = sub i32 1597463007, %shr
  store i32 %sub73, i32* %_y, align 4, !tbaa !16
  %61 = bitcast i32* %_y to float*
  %62 = load float, float* %61, align 4, !tbaa !9
  store float %62, float* %len, align 4, !tbaa !9
  %63 = load float, float* %len, align 4, !tbaa !9
  %64 = load float, float* %_x, align 4, !tbaa !9
  %65 = load float, float* %len, align 4, !tbaa !9
  %mul74 = fmul float %64, %65
  %66 = load float, float* %len, align 4, !tbaa !9
  %mul75 = fmul float %mul74, %66
  %sub76 = fsub float 1.500000e+00, %mul75
  %mul77 = fmul float %63, %sub76
  store float %mul77, float* %len, align 4, !tbaa !9
  %67 = bitcast i32* %_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  %68 = bitcast float* %_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %69 = bitcast float* %_pp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = load float, float* %len, align 4, !tbaa !9
  %cmp78 = fcmp olt float %70, 0x47EFFFFFE0000000
  br i1 %cmp78, label %if.then79, label %if.end89

if.then79:                                        ; preds = %if.end
  %71 = load float, float* %len, align 4, !tbaa !9
  %72 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %72)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %73 = load float, float* %arrayidx81, align 4, !tbaa !9
  %mul82 = fmul float %73, %71
  store float %mul82, float* %arrayidx81, align 4, !tbaa !9
  %74 = load float, float* %len, align 4, !tbaa !9
  %75 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %75)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  %76 = load float, float* %arrayidx84, align 4, !tbaa !9
  %mul85 = fmul float %76, %74
  store float %mul85, float* %arrayidx84, align 4, !tbaa !9
  %77 = load float, float* %len, align 4, !tbaa !9
  %78 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %78)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  %79 = load float, float* %arrayidx87, align 4, !tbaa !9
  %mul88 = fmul float %79, %77
  store float %mul88, float* %arrayidx87, align 4, !tbaa !9
  br label %if.end89

if.end89:                                         ; preds = %if.then79, %if.end
  %80 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds %class.btVector3, %class.btVector3* %81, i32 0
  %call91 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx90)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 0
  %82 = load float, float* %arrayidx92, align 4, !tbaa !9
  %83 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call93 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %83)
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 0
  %84 = load float, float* %arrayidx94, align 4, !tbaa !9
  %mul95 = fmul float %82, %84
  %85 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds %class.btVector3, %class.btVector3* %85, i32 0
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 1
  %86 = load float, float* %arrayidx98, align 4, !tbaa !9
  %87 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call99 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %87)
  %arrayidx100 = getelementptr inbounds float, float* %call99, i32 1
  %88 = load float, float* %arrayidx100, align 4, !tbaa !9
  %mul101 = fmul float %86, %88
  %add102 = fadd float %mul95, %mul101
  %89 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds %class.btVector3, %class.btVector3* %89, i32 0
  %call104 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx103)
  %arrayidx105 = getelementptr inbounds float, float* %call104, i32 2
  %90 = load float, float* %arrayidx105, align 4, !tbaa !9
  %91 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %91)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 2
  %92 = load float, float* %arrayidx107, align 4, !tbaa !9
  %mul108 = fmul float %90, %92
  %add109 = fadd float %add102, %mul108
  %93 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call110 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %93)
  %arrayidx111 = getelementptr inbounds float, float* %call110, i32 3
  store float %add109, float* %arrayidx111, align 4, !tbaa !9
  %94 = bitcast [3 x float]* %_dif to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %94) #4
  %95 = bitcast i32* %clipped_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #4
  %96 = load %class.btVector3*, %class.btVector3** %srcpoints.addr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds %class.btVector3, %class.btVector3* %96, i32 0
  %97 = load %class.btVector3*, %class.btVector3** %srcpoints.addr, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds %class.btVector3, %class.btVector3* %97, i32 1
  %98 = load %class.btVector3*, %class.btVector3** %srcpoints.addr, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds %class.btVector3, %class.btVector3* %98, i32 2
  %temp_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 15
  %arraydecay = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points, i32 0, i32 0
  %call115 = call i32 @_Z21PLANE_CLIP_TRIANGLE3DI9btVector39btVector4EjRKT0_RKT_S7_S7_PS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx112, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx113, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx114, %class.btVector3* %arraydecay)
  store i32 %call115, i32* %clipped_count, align 4, !tbaa !16
  %99 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %cmp116 = icmp eq i32 %99, 0
  br i1 %cmp116, label %if.then117, label %if.end118

if.then117:                                       ; preds = %if.end89
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end118:                                        ; preds = %if.end89
  %100 = bitcast [3 x float]* %_dif119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %100) #4
  %101 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds %class.btVector3, %class.btVector3* %101, i32 2
  %call121 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx120)
  %arrayidx122 = getelementptr inbounds float, float* %call121, i32 0
  %102 = load float, float* %arrayidx122, align 4, !tbaa !9
  %103 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds %class.btVector3, %class.btVector3* %103, i32 1
  %call124 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx123)
  %arrayidx125 = getelementptr inbounds float, float* %call124, i32 0
  %104 = load float, float* %arrayidx125, align 4, !tbaa !9
  %sub126 = fsub float %102, %104
  %arrayidx127 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 0
  store float %sub126, float* %arrayidx127, align 4, !tbaa !9
  %105 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds %class.btVector3, %class.btVector3* %105, i32 2
  %call129 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx128)
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 1
  %106 = load float, float* %arrayidx130, align 4, !tbaa !9
  %107 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds %class.btVector3, %class.btVector3* %107, i32 1
  %call132 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx131)
  %arrayidx133 = getelementptr inbounds float, float* %call132, i32 1
  %108 = load float, float* %arrayidx133, align 4, !tbaa !9
  %sub134 = fsub float %106, %108
  %arrayidx135 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 1
  store float %sub134, float* %arrayidx135, align 4, !tbaa !9
  %109 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds %class.btVector3, %class.btVector3* %109, i32 2
  %call137 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx136)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 2
  %110 = load float, float* %arrayidx138, align 4, !tbaa !9
  %111 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds %class.btVector3, %class.btVector3* %111, i32 1
  %call140 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx139)
  %arrayidx141 = getelementptr inbounds float, float* %call140, i32 2
  %112 = load float, float* %arrayidx141, align 4, !tbaa !9
  %sub142 = fsub float %110, %112
  %arrayidx143 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 2
  store float %sub142, float* %arrayidx143, align 4, !tbaa !9
  %arrayidx144 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 1
  %113 = load float, float* %arrayidx144, align 4, !tbaa !9
  %114 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %115 = bitcast %class.btVector4* %114 to %class.btVector3*
  %call145 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %115)
  %arrayidx146 = getelementptr inbounds float, float* %call145, i32 2
  %116 = load float, float* %arrayidx146, align 4, !tbaa !9
  %mul147 = fmul float %113, %116
  %arrayidx148 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 2
  %117 = load float, float* %arrayidx148, align 4, !tbaa !9
  %118 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %119 = bitcast %class.btVector4* %118 to %class.btVector3*
  %call149 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %119)
  %arrayidx150 = getelementptr inbounds float, float* %call149, i32 1
  %120 = load float, float* %arrayidx150, align 4, !tbaa !9
  %mul151 = fmul float %117, %120
  %sub152 = fsub float %mul147, %mul151
  %121 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call153 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %121)
  %arrayidx154 = getelementptr inbounds float, float* %call153, i32 0
  store float %sub152, float* %arrayidx154, align 4, !tbaa !9
  %arrayidx155 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 2
  %122 = load float, float* %arrayidx155, align 4, !tbaa !9
  %123 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %124 = bitcast %class.btVector4* %123 to %class.btVector3*
  %call156 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %124)
  %arrayidx157 = getelementptr inbounds float, float* %call156, i32 0
  %125 = load float, float* %arrayidx157, align 4, !tbaa !9
  %mul158 = fmul float %122, %125
  %arrayidx159 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 0
  %126 = load float, float* %arrayidx159, align 4, !tbaa !9
  %127 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %128 = bitcast %class.btVector4* %127 to %class.btVector3*
  %call160 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %128)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 2
  %129 = load float, float* %arrayidx161, align 4, !tbaa !9
  %mul162 = fmul float %126, %129
  %sub163 = fsub float %mul158, %mul162
  %130 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call164 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %130)
  %arrayidx165 = getelementptr inbounds float, float* %call164, i32 1
  store float %sub163, float* %arrayidx165, align 4, !tbaa !9
  %arrayidx166 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 0
  %131 = load float, float* %arrayidx166, align 4, !tbaa !9
  %132 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %133 = bitcast %class.btVector4* %132 to %class.btVector3*
  %call167 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %133)
  %arrayidx168 = getelementptr inbounds float, float* %call167, i32 1
  %134 = load float, float* %arrayidx168, align 4, !tbaa !9
  %mul169 = fmul float %131, %134
  %arrayidx170 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 1
  %135 = load float, float* %arrayidx170, align 4, !tbaa !9
  %136 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %137 = bitcast %class.btVector4* %136 to %class.btVector3*
  %call171 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %137)
  %arrayidx172 = getelementptr inbounds float, float* %call171, i32 0
  %138 = load float, float* %arrayidx172, align 4, !tbaa !9
  %mul173 = fmul float %135, %138
  %sub174 = fsub float %mul169, %mul173
  %139 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call175 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %139)
  %arrayidx176 = getelementptr inbounds float, float* %call175, i32 2
  store float %sub174, float* %arrayidx176, align 4, !tbaa !9
  %140 = bitcast float* %len177 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #4
  %141 = bitcast float* %_pp178 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #4
  %142 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call179 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %142)
  %arrayidx180 = getelementptr inbounds float, float* %call179, i32 0
  %143 = load float, float* %arrayidx180, align 4, !tbaa !9
  %144 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call181 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %144)
  %arrayidx182 = getelementptr inbounds float, float* %call181, i32 0
  %145 = load float, float* %arrayidx182, align 4, !tbaa !9
  %mul183 = fmul float %143, %145
  %146 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call184 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %146)
  %arrayidx185 = getelementptr inbounds float, float* %call184, i32 1
  %147 = load float, float* %arrayidx185, align 4, !tbaa !9
  %148 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call186 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %148)
  %arrayidx187 = getelementptr inbounds float, float* %call186, i32 1
  %149 = load float, float* %arrayidx187, align 4, !tbaa !9
  %mul188 = fmul float %147, %149
  %add189 = fadd float %mul183, %mul188
  %150 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call190 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %150)
  %arrayidx191 = getelementptr inbounds float, float* %call190, i32 2
  %151 = load float, float* %arrayidx191, align 4, !tbaa !9
  %152 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %152)
  %arrayidx193 = getelementptr inbounds float, float* %call192, i32 2
  %153 = load float, float* %arrayidx193, align 4, !tbaa !9
  %mul194 = fmul float %151, %153
  %add195 = fadd float %add189, %mul194
  store float %add195, float* %_pp178, align 4, !tbaa !9
  %154 = load float, float* %_pp178, align 4, !tbaa !9
  %cmp196 = fcmp ole float %154, 0x3E7AD7F2A0000000
  br i1 %cmp196, label %if.then197, label %if.else198

if.then197:                                       ; preds = %if.end118
  store float 0x47EFFFFFE0000000, float* %len177, align 4, !tbaa !9
  br label %if.end208

if.else198:                                       ; preds = %if.end118
  %155 = bitcast float* %_x199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %155) #4
  %156 = load float, float* %_pp178, align 4, !tbaa !9
  %mul200 = fmul float %156, 5.000000e-01
  store float %mul200, float* %_x199, align 4, !tbaa !9
  %157 = bitcast i32* %_y201 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #4
  %158 = bitcast float* %_pp178 to i32*
  %159 = load i32, i32* %158, align 4, !tbaa !16
  %shr202 = lshr i32 %159, 1
  %sub203 = sub i32 1597463007, %shr202
  store i32 %sub203, i32* %_y201, align 4, !tbaa !16
  %160 = bitcast i32* %_y201 to float*
  %161 = load float, float* %160, align 4, !tbaa !9
  store float %161, float* %len177, align 4, !tbaa !9
  %162 = load float, float* %len177, align 4, !tbaa !9
  %163 = load float, float* %_x199, align 4, !tbaa !9
  %164 = load float, float* %len177, align 4, !tbaa !9
  %mul204 = fmul float %163, %164
  %165 = load float, float* %len177, align 4, !tbaa !9
  %mul205 = fmul float %mul204, %165
  %sub206 = fsub float 1.500000e+00, %mul205
  %mul207 = fmul float %162, %sub206
  store float %mul207, float* %len177, align 4, !tbaa !9
  %166 = bitcast i32* %_y201 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast float* %_x199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  br label %if.end208

if.end208:                                        ; preds = %if.else198, %if.then197
  %168 = bitcast float* %_pp178 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = load float, float* %len177, align 4, !tbaa !9
  %cmp209 = fcmp olt float %169, 0x47EFFFFFE0000000
  br i1 %cmp209, label %if.then210, label %if.end220

if.then210:                                       ; preds = %if.end208
  %170 = load float, float* %len177, align 4, !tbaa !9
  %171 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call211 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %171)
  %arrayidx212 = getelementptr inbounds float, float* %call211, i32 0
  %172 = load float, float* %arrayidx212, align 4, !tbaa !9
  %mul213 = fmul float %172, %170
  store float %mul213, float* %arrayidx212, align 4, !tbaa !9
  %173 = load float, float* %len177, align 4, !tbaa !9
  %174 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call214 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %174)
  %arrayidx215 = getelementptr inbounds float, float* %call214, i32 1
  %175 = load float, float* %arrayidx215, align 4, !tbaa !9
  %mul216 = fmul float %175, %173
  store float %mul216, float* %arrayidx215, align 4, !tbaa !9
  %176 = load float, float* %len177, align 4, !tbaa !9
  %177 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call217 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %177)
  %arrayidx218 = getelementptr inbounds float, float* %call217, i32 2
  %178 = load float, float* %arrayidx218, align 4, !tbaa !9
  %mul219 = fmul float %178, %176
  store float %mul219, float* %arrayidx218, align 4, !tbaa !9
  br label %if.end220

if.end220:                                        ; preds = %if.then210, %if.end208
  %179 = bitcast float* %len177 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #4
  %180 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx221 = getelementptr inbounds %class.btVector3, %class.btVector3* %180, i32 1
  %call222 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx221)
  %arrayidx223 = getelementptr inbounds float, float* %call222, i32 0
  %181 = load float, float* %arrayidx223, align 4, !tbaa !9
  %182 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call224 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %182)
  %arrayidx225 = getelementptr inbounds float, float* %call224, i32 0
  %183 = load float, float* %arrayidx225, align 4, !tbaa !9
  %mul226 = fmul float %181, %183
  %184 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx227 = getelementptr inbounds %class.btVector3, %class.btVector3* %184, i32 1
  %call228 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx227)
  %arrayidx229 = getelementptr inbounds float, float* %call228, i32 1
  %185 = load float, float* %arrayidx229, align 4, !tbaa !9
  %186 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call230 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %186)
  %arrayidx231 = getelementptr inbounds float, float* %call230, i32 1
  %187 = load float, float* %arrayidx231, align 4, !tbaa !9
  %mul232 = fmul float %185, %187
  %add233 = fadd float %mul226, %mul232
  %188 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds %class.btVector3, %class.btVector3* %188, i32 1
  %call235 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx234)
  %arrayidx236 = getelementptr inbounds float, float* %call235, i32 2
  %189 = load float, float* %arrayidx236, align 4, !tbaa !9
  %190 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call237 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %190)
  %arrayidx238 = getelementptr inbounds float, float* %call237, i32 2
  %191 = load float, float* %arrayidx238, align 4, !tbaa !9
  %mul239 = fmul float %189, %191
  %add240 = fadd float %add233, %mul239
  %192 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call241 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %192)
  %arrayidx242 = getelementptr inbounds float, float* %call241, i32 3
  store float %add240, float* %arrayidx242, align 4, !tbaa !9
  %193 = bitcast [3 x float]* %_dif119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %193) #4
  %temp_points243 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 15
  %arraydecay244 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points243, i32 0, i32 0
  %194 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %temp_points1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 16
  %arraydecay245 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1, i32 0, i32 0
  %call246 = call i32 @_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* %arraydecay244, i32 %194, %class.btVector3* %arraydecay245)
  store i32 %call246, i32* %clipped_count, align 4, !tbaa !16
  %195 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %cmp247 = icmp eq i32 %195, 0
  br i1 %cmp247, label %if.then248, label %if.end249

if.then248:                                       ; preds = %if.end220
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end249:                                        ; preds = %if.end220
  %196 = bitcast [3 x float]* %_dif250 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %196) #4
  %197 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx251 = getelementptr inbounds %class.btVector3, %class.btVector3* %197, i32 0
  %call252 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx251)
  %arrayidx253 = getelementptr inbounds float, float* %call252, i32 0
  %198 = load float, float* %arrayidx253, align 4, !tbaa !9
  %199 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds %class.btVector3, %class.btVector3* %199, i32 2
  %call255 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx254)
  %arrayidx256 = getelementptr inbounds float, float* %call255, i32 0
  %200 = load float, float* %arrayidx256, align 4, !tbaa !9
  %sub257 = fsub float %198, %200
  %arrayidx258 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 0
  store float %sub257, float* %arrayidx258, align 4, !tbaa !9
  %201 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds %class.btVector3, %class.btVector3* %201, i32 0
  %call260 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx259)
  %arrayidx261 = getelementptr inbounds float, float* %call260, i32 1
  %202 = load float, float* %arrayidx261, align 4, !tbaa !9
  %203 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx262 = getelementptr inbounds %class.btVector3, %class.btVector3* %203, i32 2
  %call263 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx262)
  %arrayidx264 = getelementptr inbounds float, float* %call263, i32 1
  %204 = load float, float* %arrayidx264, align 4, !tbaa !9
  %sub265 = fsub float %202, %204
  %arrayidx266 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 1
  store float %sub265, float* %arrayidx266, align 4, !tbaa !9
  %205 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx267 = getelementptr inbounds %class.btVector3, %class.btVector3* %205, i32 0
  %call268 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx267)
  %arrayidx269 = getelementptr inbounds float, float* %call268, i32 2
  %206 = load float, float* %arrayidx269, align 4, !tbaa !9
  %207 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx270 = getelementptr inbounds %class.btVector3, %class.btVector3* %207, i32 2
  %call271 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx270)
  %arrayidx272 = getelementptr inbounds float, float* %call271, i32 2
  %208 = load float, float* %arrayidx272, align 4, !tbaa !9
  %sub273 = fsub float %206, %208
  %arrayidx274 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 2
  store float %sub273, float* %arrayidx274, align 4, !tbaa !9
  %arrayidx275 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 1
  %209 = load float, float* %arrayidx275, align 4, !tbaa !9
  %210 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %211 = bitcast %class.btVector4* %210 to %class.btVector3*
  %call276 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %211)
  %arrayidx277 = getelementptr inbounds float, float* %call276, i32 2
  %212 = load float, float* %arrayidx277, align 4, !tbaa !9
  %mul278 = fmul float %209, %212
  %arrayidx279 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 2
  %213 = load float, float* %arrayidx279, align 4, !tbaa !9
  %214 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %215 = bitcast %class.btVector4* %214 to %class.btVector3*
  %call280 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %215)
  %arrayidx281 = getelementptr inbounds float, float* %call280, i32 1
  %216 = load float, float* %arrayidx281, align 4, !tbaa !9
  %mul282 = fmul float %213, %216
  %sub283 = fsub float %mul278, %mul282
  %217 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call284 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %217)
  %arrayidx285 = getelementptr inbounds float, float* %call284, i32 0
  store float %sub283, float* %arrayidx285, align 4, !tbaa !9
  %arrayidx286 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 2
  %218 = load float, float* %arrayidx286, align 4, !tbaa !9
  %219 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %220 = bitcast %class.btVector4* %219 to %class.btVector3*
  %call287 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %220)
  %arrayidx288 = getelementptr inbounds float, float* %call287, i32 0
  %221 = load float, float* %arrayidx288, align 4, !tbaa !9
  %mul289 = fmul float %218, %221
  %arrayidx290 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 0
  %222 = load float, float* %arrayidx290, align 4, !tbaa !9
  %223 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %224 = bitcast %class.btVector4* %223 to %class.btVector3*
  %call291 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %224)
  %arrayidx292 = getelementptr inbounds float, float* %call291, i32 2
  %225 = load float, float* %arrayidx292, align 4, !tbaa !9
  %mul293 = fmul float %222, %225
  %sub294 = fsub float %mul289, %mul293
  %226 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call295 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %226)
  %arrayidx296 = getelementptr inbounds float, float* %call295, i32 1
  store float %sub294, float* %arrayidx296, align 4, !tbaa !9
  %arrayidx297 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 0
  %227 = load float, float* %arrayidx297, align 4, !tbaa !9
  %228 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %229 = bitcast %class.btVector4* %228 to %class.btVector3*
  %call298 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %229)
  %arrayidx299 = getelementptr inbounds float, float* %call298, i32 1
  %230 = load float, float* %arrayidx299, align 4, !tbaa !9
  %mul300 = fmul float %227, %230
  %arrayidx301 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 1
  %231 = load float, float* %arrayidx301, align 4, !tbaa !9
  %232 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4, !tbaa !2
  %233 = bitcast %class.btVector4* %232 to %class.btVector3*
  %call302 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %233)
  %arrayidx303 = getelementptr inbounds float, float* %call302, i32 0
  %234 = load float, float* %arrayidx303, align 4, !tbaa !9
  %mul304 = fmul float %231, %234
  %sub305 = fsub float %mul300, %mul304
  %235 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call306 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %235)
  %arrayidx307 = getelementptr inbounds float, float* %call306, i32 2
  store float %sub305, float* %arrayidx307, align 4, !tbaa !9
  %236 = bitcast float* %len308 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %236) #4
  %237 = bitcast float* %_pp309 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %237) #4
  %238 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call310 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %238)
  %arrayidx311 = getelementptr inbounds float, float* %call310, i32 0
  %239 = load float, float* %arrayidx311, align 4, !tbaa !9
  %240 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call312 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %240)
  %arrayidx313 = getelementptr inbounds float, float* %call312, i32 0
  %241 = load float, float* %arrayidx313, align 4, !tbaa !9
  %mul314 = fmul float %239, %241
  %242 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call315 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %242)
  %arrayidx316 = getelementptr inbounds float, float* %call315, i32 1
  %243 = load float, float* %arrayidx316, align 4, !tbaa !9
  %244 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call317 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %244)
  %arrayidx318 = getelementptr inbounds float, float* %call317, i32 1
  %245 = load float, float* %arrayidx318, align 4, !tbaa !9
  %mul319 = fmul float %243, %245
  %add320 = fadd float %mul314, %mul319
  %246 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call321 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %246)
  %arrayidx322 = getelementptr inbounds float, float* %call321, i32 2
  %247 = load float, float* %arrayidx322, align 4, !tbaa !9
  %248 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call323 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %248)
  %arrayidx324 = getelementptr inbounds float, float* %call323, i32 2
  %249 = load float, float* %arrayidx324, align 4, !tbaa !9
  %mul325 = fmul float %247, %249
  %add326 = fadd float %add320, %mul325
  store float %add326, float* %_pp309, align 4, !tbaa !9
  %250 = load float, float* %_pp309, align 4, !tbaa !9
  %cmp327 = fcmp ole float %250, 0x3E7AD7F2A0000000
  br i1 %cmp327, label %if.then328, label %if.else329

if.then328:                                       ; preds = %if.end249
  store float 0x47EFFFFFE0000000, float* %len308, align 4, !tbaa !9
  br label %if.end339

if.else329:                                       ; preds = %if.end249
  %251 = bitcast float* %_x330 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #4
  %252 = load float, float* %_pp309, align 4, !tbaa !9
  %mul331 = fmul float %252, 5.000000e-01
  store float %mul331, float* %_x330, align 4, !tbaa !9
  %253 = bitcast i32* %_y332 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %253) #4
  %254 = bitcast float* %_pp309 to i32*
  %255 = load i32, i32* %254, align 4, !tbaa !16
  %shr333 = lshr i32 %255, 1
  %sub334 = sub i32 1597463007, %shr333
  store i32 %sub334, i32* %_y332, align 4, !tbaa !16
  %256 = bitcast i32* %_y332 to float*
  %257 = load float, float* %256, align 4, !tbaa !9
  store float %257, float* %len308, align 4, !tbaa !9
  %258 = load float, float* %len308, align 4, !tbaa !9
  %259 = load float, float* %_x330, align 4, !tbaa !9
  %260 = load float, float* %len308, align 4, !tbaa !9
  %mul335 = fmul float %259, %260
  %261 = load float, float* %len308, align 4, !tbaa !9
  %mul336 = fmul float %mul335, %261
  %sub337 = fsub float 1.500000e+00, %mul336
  %mul338 = fmul float %258, %sub337
  store float %mul338, float* %len308, align 4, !tbaa !9
  %262 = bitcast i32* %_y332 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #4
  %263 = bitcast float* %_x330 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #4
  br label %if.end339

if.end339:                                        ; preds = %if.else329, %if.then328
  %264 = bitcast float* %_pp309 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #4
  %265 = load float, float* %len308, align 4, !tbaa !9
  %cmp340 = fcmp olt float %265, 0x47EFFFFFE0000000
  br i1 %cmp340, label %if.then341, label %if.end351

if.then341:                                       ; preds = %if.end339
  %266 = load float, float* %len308, align 4, !tbaa !9
  %267 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call342 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %267)
  %arrayidx343 = getelementptr inbounds float, float* %call342, i32 0
  %268 = load float, float* %arrayidx343, align 4, !tbaa !9
  %mul344 = fmul float %268, %266
  store float %mul344, float* %arrayidx343, align 4, !tbaa !9
  %269 = load float, float* %len308, align 4, !tbaa !9
  %270 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call345 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %270)
  %arrayidx346 = getelementptr inbounds float, float* %call345, i32 1
  %271 = load float, float* %arrayidx346, align 4, !tbaa !9
  %mul347 = fmul float %271, %269
  store float %mul347, float* %arrayidx346, align 4, !tbaa !9
  %272 = load float, float* %len308, align 4, !tbaa !9
  %273 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call348 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %273)
  %arrayidx349 = getelementptr inbounds float, float* %call348, i32 2
  %274 = load float, float* %arrayidx349, align 4, !tbaa !9
  %mul350 = fmul float %274, %272
  store float %mul350, float* %arrayidx349, align 4, !tbaa !9
  br label %if.end351

if.end351:                                        ; preds = %if.then341, %if.end339
  %275 = bitcast float* %len308 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #4
  %276 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx352 = getelementptr inbounds %class.btVector3, %class.btVector3* %276, i32 2
  %call353 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx352)
  %arrayidx354 = getelementptr inbounds float, float* %call353, i32 0
  %277 = load float, float* %arrayidx354, align 4, !tbaa !9
  %278 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call355 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %278)
  %arrayidx356 = getelementptr inbounds float, float* %call355, i32 0
  %279 = load float, float* %arrayidx356, align 4, !tbaa !9
  %mul357 = fmul float %277, %279
  %280 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx358 = getelementptr inbounds %class.btVector3, %class.btVector3* %280, i32 2
  %call359 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx358)
  %arrayidx360 = getelementptr inbounds float, float* %call359, i32 1
  %281 = load float, float* %arrayidx360, align 4, !tbaa !9
  %282 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call361 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %282)
  %arrayidx362 = getelementptr inbounds float, float* %call361, i32 1
  %283 = load float, float* %arrayidx362, align 4, !tbaa !9
  %mul363 = fmul float %281, %283
  %add364 = fadd float %mul357, %mul363
  %284 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4, !tbaa !2
  %arrayidx365 = getelementptr inbounds %class.btVector3, %class.btVector3* %284, i32 2
  %call366 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx365)
  %arrayidx367 = getelementptr inbounds float, float* %call366, i32 2
  %285 = load float, float* %arrayidx367, align 4, !tbaa !9
  %286 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call368 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %286)
  %arrayidx369 = getelementptr inbounds float, float* %call368, i32 2
  %287 = load float, float* %arrayidx369, align 4, !tbaa !9
  %mul370 = fmul float %285, %287
  %add371 = fadd float %add364, %mul370
  %288 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call372 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %288)
  %arrayidx373 = getelementptr inbounds float, float* %call372, i32 3
  store float %add371, float* %arrayidx373, align 4, !tbaa !9
  %289 = bitcast [3 x float]* %_dif250 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %289) #4
  %temp_points1374 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 16
  %arraydecay375 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1374, i32 0, i32 0
  %290 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %291 = load %class.btVector3*, %class.btVector3** %clip_points.addr, align 4, !tbaa !2
  %call376 = call i32 @_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* %arraydecay375, i32 %290, %class.btVector3* %291)
  store i32 %call376, i32* %clipped_count, align 4, !tbaa !16
  %292 = load i32, i32* %clipped_count, align 4, !tbaa !16
  store i32 %292, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end351, %if.then248, %if.then117
  %293 = bitcast i32* %clipped_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #4
  %294 = bitcast %class.btVector4* %edgeplane to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %294) #4
  %295 = load i32, i32* %retval, align 4
  ret i32 %295
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j(%struct.GIM_TRIANGLE_CONTACT_DATA* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane, float %margin, %class.btVector3* %points, i32 %point_count) #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %margin.addr = alloca float, align 4
  %points.addr = alloca %class.btVector3*, align 4
  %point_count.addr = alloca i32, align 4
  %agg.tmp = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %this, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4, !tbaa !2
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !9
  store %class.btVector3* %points, %class.btVector3** %points.addr, align 4, !tbaa !2
  store i32 %point_count, i32* %point_count.addr, align 4, !tbaa !16
  %this1 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 2
  %1 = bitcast %class.btVector4* %m_separating_normal to i8*
  %2 = bitcast %class.btVector4* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %4 = load float, float* %margin.addr, align 4, !tbaa !9
  %5 = load %class.btVector3*, %class.btVector3** %points.addr, align 4, !tbaa !2
  %6 = load i32, i32* %point_count.addr, align 4, !tbaa !16
  call void @_ZN25GIM_TRIANGLE_CONTACT_DATA19mergepoints_genericI22DISTANCE_PLANE_3D_FUNC9btVector4EEvRKT0_fPK9btVector3jT_(%struct.GIM_TRIANGLE_CONTACT_DATA* %this1, %class.btVector4* nonnull align 4 dereferenceable(16) %3, float %4, %class.btVector3* %5, i32 %6)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !9
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !9
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !9
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !9
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !9
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !9
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !9
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !9
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !9
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z21PLANE_CLIP_TRIANGLE3DI9btVector39btVector4EjRKT0_RKT_S7_S7_PS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, %class.btVector3* nonnull align 4 dereferenceable(16) %point2, %class.btVector3* %clipped) #2 comdat {
entry:
  %plane.addr = alloca %class.btVector4*, align 4
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %point2.addr = alloca %class.btVector3*, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %agg.tmp = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4, !tbaa !2
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4, !tbaa !2
  store %class.btVector3* %point2, %class.btVector3** %point2.addr, align 4, !tbaa !2
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %call = call i32 @_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* %4)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* %polygon_points, i32 %polygon_point_count, %class.btVector3* %clipped) #2 comdat {
entry:
  %plane.addr = alloca %class.btVector4*, align 4
  %polygon_points.addr = alloca %class.btVector3*, align 4
  %polygon_point_count.addr = alloca i32, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %agg.tmp = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  store %class.btVector3* %polygon_points, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  store i32 %polygon_point_count, i32* %polygon_point_count.addr, align 4, !tbaa !16
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %2 = load i32, i32* %polygon_point_count.addr, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %call = call i32 @_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* %1, i32 %2, %class.btVector3* %3)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, %class.btVector3* nonnull align 4 dereferenceable(16) %point2, %class.btVector3* %clipped) #2 comdat {
entry:
  %distance_func = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  %plane.addr = alloca %class.btVector4*, align 4
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %point2.addr = alloca %class.btVector3*, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count = alloca i32, align 4
  %firstdist = alloca float, align 4
  %olddist = alloca float, align 4
  %dist = alloca float, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4, !tbaa !2
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4, !tbaa !2
  store %class.btVector3* %point2, %class.btVector3** %point2.addr, align 4, !tbaa !2
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %0 = bitcast i32* %clipped_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %clipped_count, align 4, !tbaa !16
  %1 = bitcast float* %firstdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %call = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call, float* %firstdist, align 4, !tbaa !9
  %4 = load float, float* %firstdist, align 4, !tbaa !9
  %cmp = fcmp ogt float %4, 0x3E80000000000000
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %5 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %6 = load float, float* %arrayidx, align 4, !tbaa !9
  %7 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %8 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  store float %6, float* %arrayidx4, align 4, !tbaa !9
  %9 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 1
  %10 = load float, float* %arrayidx6, align 4, !tbaa !9
  %11 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %12 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 %12
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  store float %10, float* %arrayidx9, align 4, !tbaa !9
  %13 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %13)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %14 = load float, float* %arrayidx11, align 4, !tbaa !9
  %15 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %16 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 %16
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx12)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  store float %14, float* %arrayidx14, align 4, !tbaa !9
  %17 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %inc = add i32 %17, 1
  store i32 %inc, i32* %clipped_count, align 4, !tbaa !16
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %18 = bitcast float* %olddist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load float, float* %firstdist, align 4, !tbaa !9
  store float %19, float* %olddist, align 4, !tbaa !9
  %20 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %22 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %call15 = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22)
  store float %call15, float* %dist, align 4, !tbaa !9
  %23 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %25 = load float, float* %olddist, align 4, !tbaa !9
  %26 = load float, float* %dist, align 4, !tbaa !9
  %27 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, float %25, float %26, %class.btVector3* %27, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %28 = load float, float* %dist, align 4, !tbaa !9
  store float %28, float* %olddist, align 4, !tbaa !9
  %29 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %30 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4, !tbaa !2
  %call16 = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %29, %class.btVector3* nonnull align 4 dereferenceable(16) %30)
  store float %call16, float* %dist, align 4, !tbaa !9
  %31 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %32 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4, !tbaa !2
  %33 = load float, float* %olddist, align 4, !tbaa !9
  %34 = load float, float* %dist, align 4, !tbaa !9
  %35 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %31, %class.btVector3* nonnull align 4 dereferenceable(16) %32, float %33, float %34, %class.btVector3* %35, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %36 = load float, float* %dist, align 4, !tbaa !9
  store float %36, float* %olddist, align 4, !tbaa !9
  %37 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4, !tbaa !2
  %38 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %39 = load float, float* %olddist, align 4, !tbaa !9
  %40 = load float, float* %firstdist, align 4, !tbaa !9
  %41 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %37, %class.btVector3* nonnull align 4 dereferenceable(16) %38, float %39, float %40, %class.btVector3* %41, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %42 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %43 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast float* %olddist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast float* %firstdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %clipped_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  ret i32 %42
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point) #3 comdat {
entry:
  %this.addr = alloca %class.DISTANCE_PLANE_3D_FUNC*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  store %class.DISTANCE_PLANE_3D_FUNC* %this, %class.DISTANCE_PLANE_3D_FUNC** %this.addr, align 4, !tbaa !2
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  %this1 = load %class.DISTANCE_PLANE_3D_FUNC*, %class.DISTANCE_PLANE_3D_FUNC** %this.addr, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector4* %0 to %class.btVector3*
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %1)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !9
  %3 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %4 = load float, float* %arrayidx3, align 4, !tbaa !9
  %mul = fmul float %2, %4
  %5 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector4* %5 to %class.btVector3*
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !9
  %8 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !9
  %mul8 = fmul float %7, %9
  %add = fadd float %mul, %mul8
  %10 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %11 = bitcast %class.btVector4* %10 to %class.btVector3*
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 2
  %12 = load float, float* %arrayidx10, align 4, !tbaa !9
  %13 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %call11 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %13)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 2
  %14 = load float, float* %arrayidx12, align 4, !tbaa !9
  %mul13 = fmul float %12, %14
  %add14 = fadd float %add, %mul13
  %15 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector4* %15 to %class.btVector3*
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 3
  %17 = load float, float* %arrayidx16, align 4, !tbaa !9
  %sub = fsub float %add14, %17
  ret float %sub
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, float %dist0, float %dist1, %class.btVector3* %clipped, i32* nonnull align 4 dereferenceable(4) %clipped_count) #3 comdat {
entry:
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %dist0.addr = alloca float, align 4
  %dist1.addr = alloca float, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count.addr = alloca i32*, align 4
  %_prevclassif = alloca i32, align 4
  %_classif = alloca i32, align 4
  %blendfactor = alloca float, align 4
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4, !tbaa !2
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4, !tbaa !2
  store float %dist0, float* %dist0.addr, align 4, !tbaa !9
  store float %dist1, float* %dist1.addr, align 4, !tbaa !9
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  store i32* %clipped_count, i32** %clipped_count.addr, align 4, !tbaa !2
  %0 = bitcast i32* %_prevclassif to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load float, float* %dist0.addr, align 4, !tbaa !9
  %cmp = fcmp ogt float %1, 0x3E80000000000000
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %_prevclassif, align 4, !tbaa !16
  %2 = bitcast i32* %_classif to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load float, float* %dist1.addr, align 4, !tbaa !9
  %cmp1 = fcmp ogt float %3, 0x3E80000000000000
  %conv2 = zext i1 %cmp1 to i32
  store i32 %conv2, i32* %_classif, align 4, !tbaa !16
  %4 = load i32, i32* %_classif, align 4, !tbaa !16
  %5 = load i32, i32* %_prevclassif, align 4, !tbaa !16
  %cmp3 = icmp ne i32 %4, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = bitcast float* %blendfactor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load float, float* %dist0.addr, align 4, !tbaa !9
  %fneg = fneg float %7
  %8 = load float, float* %dist1.addr, align 4, !tbaa !9
  %9 = load float, float* %dist0.addr, align 4, !tbaa !9
  %sub = fsub float %8, %9
  %div = fdiv float %fneg, %sub
  store float %div, float* %blendfactor, align 4, !tbaa !9
  %10 = load float, float* %blendfactor, align 4, !tbaa !9
  %sub4 = fsub float 1.000000e+00, %10
  %11 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %12 = load float, float* %arrayidx, align 4, !tbaa !9
  %mul = fmul float %sub4, %12
  %13 = load float, float* %blendfactor, align 4, !tbaa !9
  %14 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %15 = load float, float* %arrayidx6, align 4, !tbaa !9
  %mul7 = fmul float %13, %15
  %add = fadd float %mul, %mul7
  %16 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %17 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %18 = load i32, i32* %17, align 4, !tbaa !16
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 %18
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  store float %add, float* %arrayidx10, align 4, !tbaa !9
  %19 = load float, float* %blendfactor, align 4, !tbaa !9
  %sub11 = fsub float 1.000000e+00, %19
  %20 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %20)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %21 = load float, float* %arrayidx13, align 4, !tbaa !9
  %mul14 = fmul float %sub11, %21
  %22 = load float, float* %blendfactor, align 4, !tbaa !9
  %23 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %24 = load float, float* %arrayidx16, align 4, !tbaa !9
  %mul17 = fmul float %22, %24
  %add18 = fadd float %mul14, %mul17
  %25 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %26 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %27 = load i32, i32* %26, align 4, !tbaa !16
  %arrayidx19 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 %27
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  store float %add18, float* %arrayidx21, align 4, !tbaa !9
  %28 = load float, float* %blendfactor, align 4, !tbaa !9
  %sub22 = fsub float 1.000000e+00, %28
  %29 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4, !tbaa !2
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %29)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 2
  %30 = load float, float* %arrayidx24, align 4, !tbaa !9
  %mul25 = fmul float %sub22, %30
  %31 = load float, float* %blendfactor, align 4, !tbaa !9
  %32 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %32)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  %33 = load float, float* %arrayidx27, align 4, !tbaa !9
  %mul28 = fmul float %31, %33
  %add29 = fadd float %mul25, %mul28
  %34 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %35 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %36 = load i32, i32* %35, align 4, !tbaa !16
  %arrayidx30 = getelementptr inbounds %class.btVector3, %class.btVector3* %34, i32 %36
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  store float %add29, float* %arrayidx32, align 4, !tbaa !9
  %37 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %38 = load i32, i32* %37, align 4, !tbaa !16
  %inc = add i32 %38, 1
  store i32 %inc, i32* %37, align 4, !tbaa !16
  %39 = bitcast float* %blendfactor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %40 = load i32, i32* %_classif, align 4, !tbaa !16
  %tobool = icmp ne i32 %40, 0
  br i1 %tobool, label %if.end50, label %if.then33

if.then33:                                        ; preds = %if.end
  %41 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %41)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  %42 = load float, float* %arrayidx35, align 4, !tbaa !9
  %43 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %44 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %45 = load i32, i32* %44, align 4, !tbaa !16
  %arrayidx36 = getelementptr inbounds %class.btVector3, %class.btVector3* %43, i32 %45
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx36)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 0
  store float %42, float* %arrayidx38, align 4, !tbaa !9
  %46 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 1
  %47 = load float, float* %arrayidx40, align 4, !tbaa !9
  %48 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %49 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %50 = load i32, i32* %49, align 4, !tbaa !16
  %arrayidx41 = getelementptr inbounds %class.btVector3, %class.btVector3* %48, i32 %50
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx41)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 1
  store float %47, float* %arrayidx43, align 4, !tbaa !9
  %51 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4, !tbaa !2
  %call44 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 2
  %52 = load float, float* %arrayidx45, align 4, !tbaa !9
  %53 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %54 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %55 = load i32, i32* %54, align 4, !tbaa !16
  %arrayidx46 = getelementptr inbounds %class.btVector3, %class.btVector3* %53, i32 %55
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  store float %52, float* %arrayidx48, align 4, !tbaa !9
  %56 = load i32*, i32** %clipped_count.addr, align 4, !tbaa !2
  %57 = load i32, i32* %56, align 4, !tbaa !16
  %inc49 = add i32 %57, 1
  store i32 %inc49, i32* %56, align 4, !tbaa !16
  br label %if.end50

if.end50:                                         ; preds = %if.then33, %if.end
  %58 = bitcast i32* %_classif to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i32* %_prevclassif to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* %polygon_points, i32 %polygon_point_count, %class.btVector3* %clipped) #3 comdat {
entry:
  %distance_func = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  %plane.addr = alloca %class.btVector4*, align 4
  %polygon_points.addr = alloca %class.btVector3*, align 4
  %polygon_point_count.addr = alloca i32, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count = alloca i32, align 4
  %firstdist = alloca float, align 4
  %olddist = alloca float, align 4
  %_i = alloca i32, align 4
  %dist = alloca float, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  store %class.btVector3* %polygon_points, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  store i32 %polygon_point_count, i32* %polygon_point_count.addr, align 4, !tbaa !16
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %0 = bitcast i32* %clipped_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %clipped_count, align 4, !tbaa !16
  %1 = bitcast float* %firstdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0
  %call = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  store float %call, float* %firstdist, align 4, !tbaa !9
  %4 = load float, float* %firstdist, align 4, !tbaa !9
  %cmp = fcmp ogt float %4, 0x3E80000000000000
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %5 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx1)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %6 = load float, float* %arrayidx3, align 4, !tbaa !9
  %7 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %8 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  store float %6, float* %arrayidx6, align 4, !tbaa !9
  %9 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %10 = load float, float* %arrayidx9, align 4, !tbaa !9
  %11 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %12 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 %12
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx10)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float %10, float* %arrayidx12, align 4, !tbaa !9
  %13 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %14 = load float, float* %arrayidx15, align 4, !tbaa !9
  %15 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  %16 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %arrayidx16 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 %16
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 2
  store float %14, float* %arrayidx18, align 4, !tbaa !9
  %17 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %inc = add i32 %17, 1
  store i32 %inc, i32* %clipped_count, align 4, !tbaa !16
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %18 = bitcast float* %olddist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load float, float* %firstdist, align 4, !tbaa !9
  store float %19, float* %olddist, align 4, !tbaa !9
  %20 = bitcast i32* %_i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  store i32 1, i32* %_i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %21 = load i32, i32* %_i, align 4, !tbaa !16
  %22 = load i32, i32* %polygon_point_count.addr, align 4, !tbaa !16
  %cmp19 = icmp ult i32 %21, %22
  br i1 %cmp19, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %23 = bitcast i32* %_i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %24 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %26 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %27 = load i32, i32* %_i, align 4, !tbaa !16
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %26, i32 %27
  %call21 = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %25, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  store float %call21, float* %dist, align 4, !tbaa !9
  %28 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %29 = load i32, i32* %_i, align 4, !tbaa !16
  %sub = sub i32 %29, 1
  %arrayidx22 = getelementptr inbounds %class.btVector3, %class.btVector3* %28, i32 %sub
  %30 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %31 = load i32, i32* %_i, align 4, !tbaa !16
  %arrayidx23 = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 %31
  %32 = load float, float* %olddist, align 4, !tbaa !9
  %33 = load float, float* %dist, align 4, !tbaa !9
  %34 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx22, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx23, float %32, float %33, %class.btVector3* %34, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %35 = load float, float* %dist, align 4, !tbaa !9
  store float %35, float* %olddist, align 4, !tbaa !9
  %36 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %37 = load i32, i32* %_i, align 4, !tbaa !16
  %inc24 = add i32 %37, 1
  store i32 %inc24, i32* %_i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %38 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %39 = load i32, i32* %polygon_point_count.addr, align 4, !tbaa !16
  %sub25 = sub i32 %39, 1
  %arrayidx26 = getelementptr inbounds %class.btVector3, %class.btVector3* %38, i32 %sub25
  %40 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds %class.btVector3, %class.btVector3* %40, i32 0
  %41 = load float, float* %olddist, align 4, !tbaa !9
  %42 = load float, float* %firstdist, align 4, !tbaa !9
  %43 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4, !tbaa !2
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx26, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx27, float %41, float %42, %class.btVector3* %43, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %44 = load i32, i32* %clipped_count, align 4, !tbaa !16
  %45 = bitcast float* %olddist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast float* %firstdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i32* %clipped_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  ret i32 %44
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN25GIM_TRIANGLE_CONTACT_DATA19mergepoints_genericI22DISTANCE_PLANE_3D_FUNC9btVector4EEvRKT0_fPK9btVector3jT_(%struct.GIM_TRIANGLE_CONTACT_DATA* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane, float %margin, %class.btVector3* %points, i32 %point_count) #3 comdat {
entry:
  %distance_func = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %margin.addr = alloca float, align 4
  %points.addr = alloca %class.btVector3*, align 4
  %point_count.addr = alloca i32, align 4
  %point_indices = alloca [16 x i32], align 16
  %_k = alloca i32, align 4
  %_dist = alloca float, align 4
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %this, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4, !tbaa !2
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !9
  store %class.btVector3* %points, %class.btVector3** %points.addr, align 4, !tbaa !2
  store i32 %point_count, i32* %point_count.addr, align 4, !tbaa !16
  %this1 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  store i32 0, i32* %m_point_count, align 4, !tbaa !24
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  store float -1.000000e+03, float* %m_penetration_depth, align 4, !tbaa !22
  %0 = bitcast [16 x i32]* %point_indices to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #4
  %1 = bitcast i32* %_k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %_k, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %_k, align 4, !tbaa !16
  %3 = load i32, i32* %point_count.addr, align 4, !tbaa !16
  %cmp = icmp ult i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast float* %_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %points.addr, align 4, !tbaa !2
  %7 = load i32, i32* %_k, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %call = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %fneg = fneg float %call
  %8 = load float, float* %margin.addr, align 4, !tbaa !9
  %add = fadd float %fneg, %8
  store float %add, float* %_dist, align 4, !tbaa !9
  %9 = load float, float* %_dist, align 4, !tbaa !9
  %cmp2 = fcmp oge float %9, 0.000000e+00
  br i1 %cmp2, label %if.then, label %if.end17

if.then:                                          ; preds = %for.body
  %10 = load float, float* %_dist, align 4, !tbaa !9
  %m_penetration_depth3 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  %11 = load float, float* %m_penetration_depth3, align 4, !tbaa !22
  %cmp4 = fcmp ogt float %10, %11
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.then
  %12 = load float, float* %_dist, align 4, !tbaa !9
  %m_penetration_depth6 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  store float %12, float* %m_penetration_depth6, align 4, !tbaa !22
  %13 = load i32, i32* %_k, align 4, !tbaa !16
  %arrayidx7 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 0
  store i32 %13, i32* %arrayidx7, align 16, !tbaa !16
  %m_point_count8 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  store i32 1, i32* %m_point_count8, align 4, !tbaa !24
  br label %if.end16

if.else:                                          ; preds = %if.then
  %14 = load float, float* %_dist, align 4, !tbaa !9
  %add9 = fadd float %14, 0x3E80000000000000
  %m_penetration_depth10 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  %15 = load float, float* %m_penetration_depth10, align 4, !tbaa !22
  %cmp11 = fcmp oge float %add9, %15
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.else
  %16 = load i32, i32* %_k, align 4, !tbaa !16
  %m_point_count13 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  %17 = load i32, i32* %m_point_count13, align 4, !tbaa !24
  %arrayidx14 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 %17
  store i32 %16, i32* %arrayidx14, align 4, !tbaa !16
  %m_point_count15 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  %18 = load i32, i32* %m_point_count15, align 4, !tbaa !24
  %inc = add i32 %18, 1
  store i32 %inc, i32* %m_point_count15, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.else
  br label %if.end16

if.end16:                                         ; preds = %if.end, %if.then5
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %for.body
  %19 = bitcast float* %_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end17
  %20 = load i32, i32* %_k, align 4, !tbaa !16
  %inc18 = add i32 %20, 1
  store i32 %inc18, i32* %_k, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %_k, align 4, !tbaa !16
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc26, %for.end
  %21 = load i32, i32* %_k, align 4, !tbaa !16
  %m_point_count20 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  %22 = load i32, i32* %m_point_count20, align 4, !tbaa !24
  %cmp21 = icmp ult i32 %21, %22
  br i1 %cmp21, label %for.body22, label %for.end28

for.body22:                                       ; preds = %for.cond19
  %23 = load %class.btVector3*, %class.btVector3** %points.addr, align 4, !tbaa !2
  %24 = load i32, i32* %_k, align 4, !tbaa !16
  %arrayidx23 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 %24
  %25 = load i32, i32* %arrayidx23, align 4, !tbaa !16
  %arrayidx24 = getelementptr inbounds %class.btVector3, %class.btVector3* %23, i32 %25
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 3
  %26 = load i32, i32* %_k, align 4, !tbaa !16
  %arrayidx25 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 %26
  %27 = bitcast %class.btVector3* %arrayidx25 to i8*
  %28 = bitcast %class.btVector3* %arrayidx24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !14
  br label %for.inc26

for.inc26:                                        ; preds = %for.body22
  %29 = load i32, i32* %_k, align 4, !tbaa !16
  %inc27 = add i32 %29, 1
  store i32 %inc27, i32* %_k, align 4, !tbaa !16
  br label %for.cond19

for.end28:                                        ; preds = %for.cond19
  %30 = bitcast i32* %_k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %31 = bitcast [16 x i32]* %point_indices to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %31) #4
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !8, i64 0}
!7 = !{!"_ZTS12GIM_TRIANGLE", !8, i64 0, !4, i64 4}
!8 = !{!"float", !4, i64 0}
!9 = !{!8, !8, i64 0}
!10 = !{!11, !8, i64 0}
!11 = !{!"_ZTS30GIM_TRIANGLE_CALCULATION_CACHE", !8, i64 0, !4, i64 4, !4, i64 52, !12, i64 100, !12, i64 116, !13, i64 132, !13, i64 148, !13, i64 164, !13, i64 180, !4, i64 196, !8, i64 212, !8, i64 216, !4, i64 220, !8, i64 236, !8, i64 240, !4, i64 244, !4, i64 500, !4, i64 756}
!12 = !{!"_ZTS9btVector4"}
!13 = !{!"_ZTS9btVector3", !4, i64 0}
!14 = !{i64 0, i64 16, !15}
!15 = !{!4, !4, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"int", !4, i64 0}
!18 = !{!11, !8, i64 212}
!19 = !{!11, !8, i64 216}
!20 = !{!11, !8, i64 236}
!21 = !{!11, !8, i64 240}
!22 = !{!23, !8, i64 0}
!23 = !{!"_ZTS25GIM_TRIANGLE_CONTACT_DATA", !8, i64 0, !17, i64 4, !12, i64 8, !4, i64 24}
!24 = !{!23, !17, i64 4}
