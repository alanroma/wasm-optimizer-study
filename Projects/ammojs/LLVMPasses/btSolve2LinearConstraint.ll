; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btSolve2LinearConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btSolve2LinearConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSolve2LinearConstraint = type { float, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btTypedConstraint = type opaque
%class.btVector3 = type { [4 x float] }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }

$_Z6btFabsf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZNK15btJacobianEntry14getNonDiagonalERKS_ff = comdat any

$_ZNK15btJacobianEntry11getDiagonalEv = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

define hidden void @_ZN24btSolve2LinearConstraint31resolveUnilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_(%class.btSolve2LinearConstraint* %this, %class.btRigidBody* %body1, %class.btRigidBody* %body2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %invInertiaADiag, float %invMassA, %class.btVector3* nonnull align 4 dereferenceable(16) %linvelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angvelA, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posA1, %class.btVector3* nonnull align 4 dereferenceable(16) %invInertiaBDiag, float %invMassB, %class.btVector3* nonnull align 4 dereferenceable(16) %linvelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angvelB, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posA2, float %depthA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posB1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posB2, float %depthB, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB, float* nonnull align 4 dereferenceable(4) %imp0, float* nonnull align 4 dereferenceable(4) %imp1) #0 {
entry:
  %this.addr = alloca %class.btSolve2LinearConstraint*, align 4
  %body1.addr = alloca %class.btRigidBody*, align 4
  %body2.addr = alloca %class.btRigidBody*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %invInertiaADiag.addr = alloca %class.btVector3*, align 4
  %invMassA.addr = alloca float, align 4
  %linvelA.addr = alloca %class.btVector3*, align 4
  %angvelA.addr = alloca %class.btVector3*, align 4
  %rel_posA1.addr = alloca %class.btVector3*, align 4
  %invInertiaBDiag.addr = alloca %class.btVector3*, align 4
  %invMassB.addr = alloca float, align 4
  %linvelB.addr = alloca %class.btVector3*, align 4
  %angvelB.addr = alloca %class.btVector3*, align 4
  %rel_posA2.addr = alloca %class.btVector3*, align 4
  %depthA.addr = alloca float, align 4
  %normalA.addr = alloca %class.btVector3*, align 4
  %rel_posB1.addr = alloca %class.btVector3*, align 4
  %rel_posB2.addr = alloca %class.btVector3*, align 4
  %depthB.addr = alloca float, align 4
  %normalB.addr = alloca %class.btVector3*, align 4
  %imp0.addr = alloca float*, align 4
  %imp1.addr = alloca float*, align 4
  %len = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %jacA = alloca %class.btJacobianEntry, align 4
  %jacB = alloca %class.btJacobianEntry, align 4
  %vel0 = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %vel1 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %massTerm = alloca float, align 4
  %dv0 = alloca float, align 4
  %dv1 = alloca float, align 4
  %nonDiag = alloca float, align 4
  %invDet = alloca float, align 4
  store %class.btSolve2LinearConstraint* %this, %class.btSolve2LinearConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body1, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body2, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %invInertiaADiag, %class.btVector3** %invInertiaADiag.addr, align 4, !tbaa !2
  store float %invMassA, float* %invMassA.addr, align 4, !tbaa !6
  store %class.btVector3* %linvelA, %class.btVector3** %linvelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angvelA, %class.btVector3** %angvelA.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posA1, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  store %class.btVector3* %invInertiaBDiag, %class.btVector3** %invInertiaBDiag.addr, align 4, !tbaa !2
  store float %invMassB, float* %invMassB.addr, align 4, !tbaa !6
  store %class.btVector3* %linvelB, %class.btVector3** %linvelB.addr, align 4, !tbaa !2
  store %class.btVector3* %angvelB, %class.btVector3** %angvelB.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posA2, %class.btVector3** %rel_posA2.addr, align 4, !tbaa !2
  store float %depthA, float* %depthA.addr, align 4, !tbaa !6
  store %class.btVector3* %normalA, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posB1, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posB2, %class.btVector3** %rel_posB2.addr, align 4, !tbaa !2
  store float %depthB, float* %depthB.addr, align 4, !tbaa !6
  store %class.btVector3* %normalB, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  store float* %imp0, float** %imp0.addr, align 4, !tbaa !2
  store float* %imp1, float** %imp1.addr, align 4, !tbaa !2
  %this1 = load %class.btSolve2LinearConstraint*, %class.btSolve2LinearConstraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %linvelA.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %linvelB.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %angvelB.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %angvelA.addr, align 4, !tbaa !2
  %4 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %4, align 4, !tbaa !6
  %5 = load float*, float** %imp1.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %5, align 4, !tbaa !6
  %6 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %7)
  %call2 = call float @_Z6btFabsf(float %call)
  %sub = fsub float %call2, 1.000000e+00
  store float %sub, float* %len, align 4, !tbaa !6
  %8 = load float, float* %len, align 4, !tbaa !6
  %call3 = call float @_Z6btFabsf(float %8)
  %cmp = fcmp oge float %call3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = bitcast %class.btJacobianEntry* %jacA to i8*
  call void @llvm.lifetime.start.p0i8(i64 84, i8* %9) #6
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %12 = load %class.btVector3*, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %rel_posA2.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  %15 = load %class.btVector3*, %class.btVector3** %invInertiaADiag.addr, align 4, !tbaa !2
  %16 = load float, float* %invMassA.addr, align 4, !tbaa !6
  %17 = load %class.btVector3*, %class.btVector3** %invInertiaBDiag.addr, align 4, !tbaa !2
  %18 = load float, float* %invMassB.addr, align 4, !tbaa !6
  %call4 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %jacA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15, float %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float %18)
  %19 = bitcast %class.btJacobianEntry* %jacB to i8*
  call void @llvm.lifetime.start.p0i8(i64 84, i8* %19) #6
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %22 = load %class.btVector3*, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  %23 = load %class.btVector3*, %class.btVector3** %rel_posB2.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  %25 = load %class.btVector3*, %class.btVector3** %invInertiaADiag.addr, align 4, !tbaa !2
  %26 = load float, float* %invMassA.addr, align 4, !tbaa !6
  %27 = load %class.btVector3*, %class.btVector3** %invInertiaBDiag.addr, align 4, !tbaa !2
  %28 = load float, float* %invMassB.addr, align 4, !tbaa !6
  %call5 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %jacB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %20, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25, float %26, %class.btVector3* nonnull align 4 dereferenceable(16) %27, float %28)
  %29 = bitcast float* %vel0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  %31 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #6
  %32 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #6
  %33 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp6, %class.btRigidBody* %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34)
  %35 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #6
  %36 = load %class.btRigidBody*, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  %37 = load %class.btVector3*, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp7, %class.btRigidBody* %36, %class.btVector3* nonnull align 4 dereferenceable(16) %37)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %30, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %38 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #6
  %39 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #6
  %40 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #6
  store float %call8, float* %vel0, align 4, !tbaa !6
  %41 = bitcast float* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load %class.btVector3*, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  %43 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #6
  %44 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #6
  %45 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %46 = load %class.btVector3*, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btRigidBody* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %46)
  %47 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #6
  %48 = load %class.btRigidBody*, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  %49 = load %class.btVector3*, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp11, %class.btRigidBody* %48, %class.btVector3* nonnull align 4 dereferenceable(16) %49)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %50 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #6
  %51 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #6
  %52 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #6
  store float %call12, float* %vel1, align 4, !tbaa !6
  %53 = bitcast float* %massTerm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  %54 = load float, float* %invMassA.addr, align 4, !tbaa !6
  %55 = load float, float* %invMassB.addr, align 4, !tbaa !6
  %add = fadd float %54, %55
  %div = fdiv float 1.000000e+00, %add
  store float %div, float* %massTerm, align 4, !tbaa !6
  %56 = bitcast float* %dv0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #6
  %57 = load float, float* %depthA.addr, align 4, !tbaa !6
  %m_tau = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 0
  %58 = load float, float* %m_tau, align 4, !tbaa !8
  %mul = fmul float %57, %58
  %59 = load float, float* %massTerm, align 4, !tbaa !6
  %mul13 = fmul float %mul, %59
  %60 = load float, float* %vel0, align 4, !tbaa !6
  %m_damping = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 1
  %61 = load float, float* %m_damping, align 4, !tbaa !10
  %mul14 = fmul float %60, %61
  %sub15 = fsub float %mul13, %mul14
  store float %sub15, float* %dv0, align 4, !tbaa !6
  %62 = bitcast float* %dv1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  %63 = load float, float* %depthB.addr, align 4, !tbaa !6
  %m_tau16 = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 0
  %64 = load float, float* %m_tau16, align 4, !tbaa !8
  %mul17 = fmul float %63, %64
  %65 = load float, float* %massTerm, align 4, !tbaa !6
  %mul18 = fmul float %mul17, %65
  %66 = load float, float* %vel1, align 4, !tbaa !6
  %m_damping19 = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 1
  %67 = load float, float* %m_damping19, align 4, !tbaa !10
  %mul20 = fmul float %66, %67
  %sub21 = fsub float %mul18, %mul20
  store float %sub21, float* %dv1, align 4, !tbaa !6
  %68 = bitcast float* %nonDiag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  %69 = load float, float* %invMassA.addr, align 4, !tbaa !6
  %70 = load float, float* %invMassB.addr, align 4, !tbaa !6
  %call22 = call float @_ZNK15btJacobianEntry14getNonDiagonalERKS_ff(%class.btJacobianEntry* %jacA, %class.btJacobianEntry* nonnull align 4 dereferenceable(84) %jacB, float %69, float %70)
  store float %call22, float* %nonDiag, align 4, !tbaa !6
  %71 = bitcast float* %invDet to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  %call23 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacA)
  %call24 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacB)
  %mul25 = fmul float %call23, %call24
  %72 = load float, float* %nonDiag, align 4, !tbaa !6
  %73 = load float, float* %nonDiag, align 4, !tbaa !6
  %mul26 = fmul float %72, %73
  %sub27 = fsub float %mul25, %mul26
  %div28 = fdiv float 1.000000e+00, %sub27
  store float %div28, float* %invDet, align 4, !tbaa !6
  %74 = load float, float* %dv0, align 4, !tbaa !6
  %call29 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacA)
  %mul30 = fmul float %74, %call29
  %75 = load float, float* %invDet, align 4, !tbaa !6
  %mul31 = fmul float %mul30, %75
  %76 = load float, float* %dv1, align 4, !tbaa !6
  %77 = load float, float* %nonDiag, align 4, !tbaa !6
  %fneg = fneg float %77
  %mul32 = fmul float %76, %fneg
  %78 = load float, float* %invDet, align 4, !tbaa !6
  %mul33 = fmul float %mul32, %78
  %add34 = fadd float %mul31, %mul33
  %79 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float %add34, float* %79, align 4, !tbaa !6
  %80 = load float, float* %dv1, align 4, !tbaa !6
  %call35 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacB)
  %mul36 = fmul float %80, %call35
  %81 = load float, float* %invDet, align 4, !tbaa !6
  %mul37 = fmul float %mul36, %81
  %82 = load float, float* %dv0, align 4, !tbaa !6
  %83 = load float, float* %nonDiag, align 4, !tbaa !6
  %fneg38 = fneg float %83
  %mul39 = fmul float %82, %fneg38
  %84 = load float, float* %invDet, align 4, !tbaa !6
  %mul40 = fmul float %mul39, %84
  %add41 = fadd float %mul37, %mul40
  %85 = load float*, float** %imp1.addr, align 4, !tbaa !2
  store float %add41, float* %85, align 4, !tbaa !6
  %86 = bitcast float* %invDet to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #6
  %87 = bitcast float* %nonDiag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #6
  %88 = bitcast float* %dv1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #6
  %89 = bitcast float* %dv0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #6
  %90 = bitcast float* %massTerm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #6
  %91 = bitcast float* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #6
  %92 = bitcast float* %vel0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #6
  %93 = bitcast %class.btJacobianEntry* %jacB to i8*
  call void @llvm.lifetime.end.p0i8(i64 84, i8* %93) #6
  %94 = bitcast %class.btJacobianEntry* %jacA to i8*
  call void @llvm.lifetime.end.p0i8(i64 84, i8* %94) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %95 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, float %massInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB, float %massInvB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %massInvA.addr = alloca float, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %massInvB.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  store float %massInvA, float* %massInvA.addr, align 4, !tbaa !6
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  store float %massInvB, float* %massInvB.addr, align 4, !tbaa !6
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %m_linearJointAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !11
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  %m_linearJointAxis6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_aJ7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %7 = bitcast %class.btVector3* %m_aJ7 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !11
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #6
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #6
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #6
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #6
  %14 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #6
  %m_linearJointAxis11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis11)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_bJ12 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !11
  %18 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #6
  %19 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #6
  %20 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  %21 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #6
  %22 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %23 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !11
  %25 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #6
  %26 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #6
  %27 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %28 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !11
  %30 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #6
  %31 = load float, float* %massInvA.addr, align 4, !tbaa !6
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %add = fadd float %31, %call21
  %32 = load float, float* %massInvB.addr, align 4, !tbaa !6
  %add22 = fadd float %add, %32
  %m_1MinvJt23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ24 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ24)
  %add26 = fadd float %add22, %call25
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add26, float* %m_Adiag, align 4, !tbaa !13
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %1 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

define linkonce_odr hidden float @_ZNK15btJacobianEntry14getNonDiagonalERKS_ff(%class.btJacobianEntry* %this, %class.btJacobianEntry* nonnull align 4 dereferenceable(84) %jacB, float %massInvA, float %massInvB) #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %jacB.addr = alloca %class.btJacobianEntry*, align 4
  %massInvA.addr = alloca float, align 4
  %massInvB.addr = alloca float, align 4
  %jacA = alloca %class.btJacobianEntry*, align 4
  %lin = alloca %class.btVector3, align 4
  %ang0 = alloca %class.btVector3, align 4
  %ang1 = alloca %class.btVector3, align 4
  %lin0 = alloca %class.btVector3, align 4
  %lin1 = alloca %class.btVector3, align 4
  %sum = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btJacobianEntry* %jacB, %class.btJacobianEntry** %jacB.addr, align 4, !tbaa !2
  store float %massInvA, float* %massInvA.addr, align 4, !tbaa !6
  store float %massInvB, float* %massInvB.addr, align 4, !tbaa !6
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %0 = bitcast %class.btJacobianEntry** %jacA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store %class.btJacobianEntry* %this1, %class.btJacobianEntry** %jacA, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %lin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacA, align 4, !tbaa !2
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %2, i32 0, i32 0
  %3 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacB.addr, align 4, !tbaa !2
  %m_linearJointAxis2 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %3, i32 0, i32 0
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %lin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis2)
  %4 = bitcast %class.btVector3* %ang0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #6
  %5 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacA, align 4, !tbaa !2
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %5, i32 0, i32 3
  %6 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacB.addr, align 4, !tbaa !2
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %6, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ang0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_0MinvJt, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ)
  %7 = bitcast %class.btVector3* %ang1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacA, align 4, !tbaa !2
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %8, i32 0, i32 4
  %9 = load %class.btJacobianEntry*, %class.btJacobianEntry** %jacB.addr, align 4, !tbaa !2
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %9, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ang1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_1MinvJt, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ)
  %10 = bitcast %class.btVector3* %lin0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %lin0, float* nonnull align 4 dereferenceable(4) %massInvA.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %lin)
  %11 = bitcast %class.btVector3* %lin1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %lin1, float* nonnull align 4 dereferenceable(4) %massInvB.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %lin)
  %12 = bitcast %class.btVector3* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #6
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #6
  %14 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #6
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ang0, %class.btVector3* nonnull align 4 dereferenceable(16) %ang1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %lin0)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %sum, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %lin1)
  %15 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #6
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %sum)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %17 = load float, float* %arrayidx, align 4, !tbaa !6
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %sum)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %18 = load float, float* %arrayidx5, align 4, !tbaa !6
  %add = fadd float %17, %18
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %sum)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %19 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %add, %19
  %20 = bitcast %class.btVector3* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  %21 = bitcast %class.btVector3* %lin1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #6
  %22 = bitcast %class.btVector3* %lin0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #6
  %23 = bitcast %class.btVector3* %ang1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #6
  %24 = bitcast %class.btVector3* %ang0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #6
  %25 = bitcast %class.btVector3* %lin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #6
  %26 = bitcast %class.btJacobianEntry** %jacA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  ret float %add8
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  %0 = load float, float* %m_Adiag, align 4, !tbaa !13
  ret float %0
}

define hidden void @_ZN24btSolve2LinearConstraint30resolveBilateralPairConstraintEP11btRigidBodyS1_RK11btMatrix3x3S4_RK9btVector3fS7_S7_S7_S7_fS7_S7_S7_fS7_S7_S7_fS7_RfS8_(%class.btSolve2LinearConstraint* %this, %class.btRigidBody* %body1, %class.btRigidBody* %body2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %invInertiaADiag, float %invMassA, %class.btVector3* nonnull align 4 dereferenceable(16) %linvelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angvelA, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posA1, %class.btVector3* nonnull align 4 dereferenceable(16) %invInertiaBDiag, float %invMassB, %class.btVector3* nonnull align 4 dereferenceable(16) %linvelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angvelB, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posA2, float %depthA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posB1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_posB2, float %depthB, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB, float* nonnull align 4 dereferenceable(4) %imp0, float* nonnull align 4 dereferenceable(4) %imp1) #0 {
entry:
  %this.addr = alloca %class.btSolve2LinearConstraint*, align 4
  %body1.addr = alloca %class.btRigidBody*, align 4
  %body2.addr = alloca %class.btRigidBody*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %invInertiaADiag.addr = alloca %class.btVector3*, align 4
  %invMassA.addr = alloca float, align 4
  %linvelA.addr = alloca %class.btVector3*, align 4
  %angvelA.addr = alloca %class.btVector3*, align 4
  %rel_posA1.addr = alloca %class.btVector3*, align 4
  %invInertiaBDiag.addr = alloca %class.btVector3*, align 4
  %invMassB.addr = alloca float, align 4
  %linvelB.addr = alloca %class.btVector3*, align 4
  %angvelB.addr = alloca %class.btVector3*, align 4
  %rel_posA2.addr = alloca %class.btVector3*, align 4
  %depthA.addr = alloca float, align 4
  %normalA.addr = alloca %class.btVector3*, align 4
  %rel_posB1.addr = alloca %class.btVector3*, align 4
  %rel_posB2.addr = alloca %class.btVector3*, align 4
  %depthB.addr = alloca float, align 4
  %normalB.addr = alloca %class.btVector3*, align 4
  %imp0.addr = alloca float*, align 4
  %imp1.addr = alloca float*, align 4
  %len = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %jacA = alloca %class.btJacobianEntry, align 4
  %jacB = alloca %class.btJacobianEntry, align 4
  %vel0 = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %vel1 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %dv0 = alloca float, align 4
  %dv1 = alloca float, align 4
  %nonDiag = alloca float, align 4
  %invDet = alloca float, align 4
  store %class.btSolve2LinearConstraint* %this, %class.btSolve2LinearConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body1, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body2, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %invInertiaADiag, %class.btVector3** %invInertiaADiag.addr, align 4, !tbaa !2
  store float %invMassA, float* %invMassA.addr, align 4, !tbaa !6
  store %class.btVector3* %linvelA, %class.btVector3** %linvelA.addr, align 4, !tbaa !2
  store %class.btVector3* %angvelA, %class.btVector3** %angvelA.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posA1, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  store %class.btVector3* %invInertiaBDiag, %class.btVector3** %invInertiaBDiag.addr, align 4, !tbaa !2
  store float %invMassB, float* %invMassB.addr, align 4, !tbaa !6
  store %class.btVector3* %linvelB, %class.btVector3** %linvelB.addr, align 4, !tbaa !2
  store %class.btVector3* %angvelB, %class.btVector3** %angvelB.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posA2, %class.btVector3** %rel_posA2.addr, align 4, !tbaa !2
  store float %depthA, float* %depthA.addr, align 4, !tbaa !6
  store %class.btVector3* %normalA, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posB1, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_posB2, %class.btVector3** %rel_posB2.addr, align 4, !tbaa !2
  store float %depthB, float* %depthB.addr, align 4, !tbaa !6
  store %class.btVector3* %normalB, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  store float* %imp0, float** %imp0.addr, align 4, !tbaa !2
  store float* %imp1, float** %imp1.addr, align 4, !tbaa !2
  %this1 = load %class.btSolve2LinearConstraint*, %class.btSolve2LinearConstraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %linvelA.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %linvelB.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %angvelA.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %angvelB.addr, align 4, !tbaa !2
  %4 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %4, align 4, !tbaa !6
  %5 = load float*, float** %imp1.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %5, align 4, !tbaa !6
  %6 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %7)
  %call2 = call float @_Z6btFabsf(float %call)
  %sub = fsub float %call2, 1.000000e+00
  store float %sub, float* %len, align 4, !tbaa !6
  %8 = load float, float* %len, align 4, !tbaa !6
  %call3 = call float @_Z6btFabsf(float %8)
  %cmp = fcmp oge float %call3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = bitcast %class.btJacobianEntry* %jacA to i8*
  call void @llvm.lifetime.start.p0i8(i64 84, i8* %9) #6
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %12 = load %class.btVector3*, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %rel_posA2.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  %15 = load %class.btVector3*, %class.btVector3** %invInertiaADiag.addr, align 4, !tbaa !2
  %16 = load float, float* %invMassA.addr, align 4, !tbaa !6
  %17 = load %class.btVector3*, %class.btVector3** %invInertiaBDiag.addr, align 4, !tbaa !2
  %18 = load float, float* %invMassB.addr, align 4, !tbaa !6
  %call4 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %jacA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15, float %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float %18)
  %19 = bitcast %class.btJacobianEntry* %jacB to i8*
  call void @llvm.lifetime.start.p0i8(i64 84, i8* %19) #6
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %22 = load %class.btVector3*, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  %23 = load %class.btVector3*, %class.btVector3** %rel_posB2.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  %25 = load %class.btVector3*, %class.btVector3** %invInertiaADiag.addr, align 4, !tbaa !2
  %26 = load float, float* %invMassA.addr, align 4, !tbaa !6
  %27 = load %class.btVector3*, %class.btVector3** %invInertiaBDiag.addr, align 4, !tbaa !2
  %28 = load float, float* %invMassB.addr, align 4, !tbaa !6
  %call5 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %jacB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %20, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25, float %26, %class.btVector3* nonnull align 4 dereferenceable(16) %27, float %28)
  %29 = bitcast float* %vel0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  %30 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  %31 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #6
  %32 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #6
  %33 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp6, %class.btRigidBody* %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34)
  %35 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #6
  %36 = load %class.btRigidBody*, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  %37 = load %class.btVector3*, %class.btVector3** %rel_posA1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp7, %class.btRigidBody* %36, %class.btVector3* nonnull align 4 dereferenceable(16) %37)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %30, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %38 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #6
  %39 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #6
  %40 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #6
  store float %call8, float* %vel0, align 4, !tbaa !6
  %41 = bitcast float* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  %42 = load %class.btVector3*, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  %43 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #6
  %44 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #6
  %45 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !2
  %46 = load %class.btVector3*, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btRigidBody* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %46)
  %47 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #6
  %48 = load %class.btRigidBody*, %class.btRigidBody** %body2.addr, align 4, !tbaa !2
  %49 = load %class.btVector3*, %class.btVector3** %rel_posB1.addr, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp11, %class.btRigidBody* %48, %class.btVector3* nonnull align 4 dereferenceable(16) %49)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %50 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #6
  %51 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #6
  %52 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #6
  store float %call12, float* %vel1, align 4, !tbaa !6
  %53 = bitcast float* %dv0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  %54 = load float, float* %depthA.addr, align 4, !tbaa !6
  %m_tau = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 0
  %55 = load float, float* %m_tau, align 4, !tbaa !8
  %mul = fmul float %54, %55
  %56 = load float, float* %vel0, align 4, !tbaa !6
  %m_damping = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 1
  %57 = load float, float* %m_damping, align 4, !tbaa !10
  %mul13 = fmul float %56, %57
  %sub14 = fsub float %mul, %mul13
  store float %sub14, float* %dv0, align 4, !tbaa !6
  %58 = bitcast float* %dv1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  %59 = load float, float* %depthB.addr, align 4, !tbaa !6
  %m_tau15 = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 0
  %60 = load float, float* %m_tau15, align 4, !tbaa !8
  %mul16 = fmul float %59, %60
  %61 = load float, float* %vel1, align 4, !tbaa !6
  %m_damping17 = getelementptr inbounds %class.btSolve2LinearConstraint, %class.btSolve2LinearConstraint* %this1, i32 0, i32 1
  %62 = load float, float* %m_damping17, align 4, !tbaa !10
  %mul18 = fmul float %61, %62
  %sub19 = fsub float %mul16, %mul18
  store float %sub19, float* %dv1, align 4, !tbaa !6
  %63 = bitcast float* %nonDiag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #6
  %64 = load float, float* %invMassA.addr, align 4, !tbaa !6
  %65 = load float, float* %invMassB.addr, align 4, !tbaa !6
  %call20 = call float @_ZNK15btJacobianEntry14getNonDiagonalERKS_ff(%class.btJacobianEntry* %jacA, %class.btJacobianEntry* nonnull align 4 dereferenceable(84) %jacB, float %64, float %65)
  store float %call20, float* %nonDiag, align 4, !tbaa !6
  %66 = bitcast float* %invDet to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  %call21 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacA)
  %call22 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacB)
  %mul23 = fmul float %call21, %call22
  %67 = load float, float* %nonDiag, align 4, !tbaa !6
  %68 = load float, float* %nonDiag, align 4, !tbaa !6
  %mul24 = fmul float %67, %68
  %sub25 = fsub float %mul23, %mul24
  %div = fdiv float 1.000000e+00, %sub25
  store float %div, float* %invDet, align 4, !tbaa !6
  %69 = load float, float* %dv0, align 4, !tbaa !6
  %call26 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacA)
  %mul27 = fmul float %69, %call26
  %70 = load float, float* %invDet, align 4, !tbaa !6
  %mul28 = fmul float %mul27, %70
  %71 = load float, float* %dv1, align 4, !tbaa !6
  %72 = load float, float* %nonDiag, align 4, !tbaa !6
  %fneg = fneg float %72
  %mul29 = fmul float %71, %fneg
  %73 = load float, float* %invDet, align 4, !tbaa !6
  %mul30 = fmul float %mul29, %73
  %add = fadd float %mul28, %mul30
  %74 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float %add, float* %74, align 4, !tbaa !6
  %75 = load float, float* %dv1, align 4, !tbaa !6
  %call31 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacB)
  %mul32 = fmul float %75, %call31
  %76 = load float, float* %invDet, align 4, !tbaa !6
  %mul33 = fmul float %mul32, %76
  %77 = load float, float* %dv0, align 4, !tbaa !6
  %78 = load float, float* %nonDiag, align 4, !tbaa !6
  %fneg34 = fneg float %78
  %mul35 = fmul float %77, %fneg34
  %79 = load float, float* %invDet, align 4, !tbaa !6
  %mul36 = fmul float %mul35, %79
  %add37 = fadd float %mul33, %mul36
  %80 = load float*, float** %imp1.addr, align 4, !tbaa !2
  store float %add37, float* %80, align 4, !tbaa !6
  %81 = load float*, float** %imp0.addr, align 4, !tbaa !2
  %82 = load float, float* %81, align 4, !tbaa !6
  %cmp38 = fcmp ogt float %82, 0.000000e+00
  br i1 %cmp38, label %if.then39, label %if.else49

if.then39:                                        ; preds = %if.end
  %83 = load float*, float** %imp1.addr, align 4, !tbaa !2
  %84 = load float, float* %83, align 4, !tbaa !6
  %cmp40 = fcmp ogt float %84, 0.000000e+00
  br i1 %cmp40, label %if.then41, label %if.else

if.then41:                                        ; preds = %if.then39
  br label %if.end48

if.else:                                          ; preds = %if.then39
  %85 = load float*, float** %imp1.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %85, align 4, !tbaa !6
  %86 = load float, float* %dv0, align 4, !tbaa !6
  %call42 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacA)
  %div43 = fdiv float %86, %call42
  %87 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float %div43, float* %87, align 4, !tbaa !6
  %88 = load float*, float** %imp0.addr, align 4, !tbaa !2
  %89 = load float, float* %88, align 4, !tbaa !6
  %cmp44 = fcmp ogt float %89, 0.000000e+00
  br i1 %cmp44, label %if.then45, label %if.else46

if.then45:                                        ; preds = %if.else
  br label %if.end47

if.else46:                                        ; preds = %if.else
  %90 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %90, align 4, !tbaa !6
  br label %if.end47

if.end47:                                         ; preds = %if.else46, %if.then45
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.then41
  br label %if.end62

if.else49:                                        ; preds = %if.end
  %91 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %91, align 4, !tbaa !6
  %92 = load float, float* %dv1, align 4, !tbaa !6
  %call50 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacB)
  %div51 = fdiv float %92, %call50
  %93 = load float*, float** %imp1.addr, align 4, !tbaa !2
  store float %div51, float* %93, align 4, !tbaa !6
  %94 = load float*, float** %imp1.addr, align 4, !tbaa !2
  %95 = load float, float* %94, align 4, !tbaa !6
  %cmp52 = fcmp ole float %95, 0.000000e+00
  br i1 %cmp52, label %if.then53, label %if.else60

if.then53:                                        ; preds = %if.else49
  %96 = load float*, float** %imp1.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %96, align 4, !tbaa !6
  %97 = load float, float* %dv0, align 4, !tbaa !6
  %call54 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %jacA)
  %div55 = fdiv float %97, %call54
  %98 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float %div55, float* %98, align 4, !tbaa !6
  %99 = load float*, float** %imp0.addr, align 4, !tbaa !2
  %100 = load float, float* %99, align 4, !tbaa !6
  %cmp56 = fcmp ogt float %100, 0.000000e+00
  br i1 %cmp56, label %if.then57, label %if.else58

if.then57:                                        ; preds = %if.then53
  br label %if.end59

if.else58:                                        ; preds = %if.then53
  %101 = load float*, float** %imp0.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %101, align 4, !tbaa !6
  br label %if.end59

if.end59:                                         ; preds = %if.else58, %if.then57
  br label %if.end61

if.else60:                                        ; preds = %if.else49
  br label %if.end61

if.end61:                                         ; preds = %if.else60, %if.end59
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %if.end48
  %102 = bitcast float* %invDet to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast float* %nonDiag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast float* %dv1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %105 = bitcast float* %dv0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #6
  %106 = bitcast float* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #6
  %107 = bitcast float* %vel0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #6
  %108 = bitcast %class.btJacobianEntry* %jacB to i8*
  call void @llvm.lifetime.end.p0i8(i64 84, i8* %108) #6
  %109 = bitcast %class.btJacobianEntry* %jacA to i8*
  call void @llvm.lifetime.end.p0i8(i64 84, i8* %109) #6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end62, %if.then
  %110 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !6
  %0 = load float, float* %y.addr, align 4, !tbaa !6
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !6
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !6
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !6
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !6
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !6
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !6
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !6
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !6
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !6
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !6
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !16
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !6
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !6
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !6
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !7, i64 0}
!9 = !{!"_ZTS24btSolve2LinearConstraint", !7, i64 0, !7, i64 4}
!10 = !{!9, !7, i64 4}
!11 = !{i64 0, i64 16, !12}
!12 = !{!4, !4, i64 0}
!13 = !{!14, !7, i64 80}
!14 = !{!"_ZTS15btJacobianEntry", !15, i64 0, !15, i64 16, !15, i64 32, !15, i64 48, !15, i64 64, !7, i64 80}
!15 = !{!"_ZTS9btVector3", !4, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"int", !4, i64 0}
