; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexPointCloudShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexPointCloudShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConvexPointCloudShape = type { %class.btPolyhedralConvexAabbCachingShape.base, %class.btVector3*, i32 }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btConvexPolyhedron = type opaque
%class.btVector3 = type { [4 x float] }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector36maxDotEPKS_lRf = comdat any

$_ZNK23btConvexPointCloudShape14getScaledPointEi = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN23btConvexPointCloudShapeD0Ev = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK23btConvexPointCloudShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN23btConvexPointCloudShapedlEPv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

@_ZTV23btConvexPointCloudShape = hidden unnamed_addr constant { [33 x i8*] } { [33 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btConvexPointCloudShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btConvexPointCloudShape*)* @_ZN23btConvexPointCloudShapeD0Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexPointCloudShape*, %class.btVector3*)* @_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btConvexPointCloudShape*)* @_ZNK23btConvexPointCloudShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)* @_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)* @_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btConvexPointCloudShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btConvexPointCloudShape*)* @_ZNK23btConvexPointCloudShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btConvexPointCloudShape*)* @_ZNK23btConvexPointCloudShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btConvexPointCloudShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btConvexPointCloudShape*, i32, %class.btVector3*)* @_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btConvexPointCloudShape*)* @_ZNK23btConvexPointCloudShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btConvexPointCloudShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btConvexPointCloudShape*, %class.btVector3*, float)* @_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btConvexPointCloudShape = hidden constant [26 x i8] c"23btConvexPointCloudShape\00", align 1
@_ZTI34btPolyhedralConvexAabbCachingShape = external constant i8*
@_ZTI23btConvexPointCloudShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btConvexPointCloudShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI34btPolyhedralConvexAabbCachingShape to i8*) }, align 4
@.str = private unnamed_addr constant [17 x i8] c"ConvexPointCloud\00", align 1
@.str.1 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1

define hidden void @_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3(%class.btConvexPointCloudShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %1 = bitcast %class.btConvexPointCloudShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_localScaling to i8*
  %3 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !6
  %4 = bitcast %class.btConvexPointCloudShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %4)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape*) #2

define hidden void @_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexPointCloudShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec0) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %vec0.addr = alloca %class.btVector3*, align 4
  %supVec = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %maxDot = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %lenSqr = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %rlen = alloca float, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec0, %class.btVector3** %vec0.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %supVec, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0xC3ABC16D60000000, float* %maxDot, align 4, !tbaa !8
  %8 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btVector3*, %class.btVector3** %vec0.addr, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %vec to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !6
  %12 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec)
  store float %call4, float* %lenSqr, align 4, !tbaa !8
  %13 = load float, float* %lenSqr, align 4, !tbaa !8
  %cmp = fcmp olt float %13, 0x3F1A36E2E0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %14 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %15 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %16 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %17 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load float, float* %lenSqr, align 4, !tbaa !8
  %call8 = call float @_Z6btSqrtf(float %21)
  %div = fdiv float 1.000000e+00, %call8
  store float %div, float* %rlen, align 4, !tbaa !8
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %rlen)
  %22 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_numPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 2
  %23 = load i32, i32* %m_numPoints, align 4, !tbaa !10
  %cmp10 = icmp sgt i32 %23, 0
  br i1 %cmp10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %if.end
  %24 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %m_unscaledPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 1
  %25 = load %class.btVector3*, %class.btVector3** %m_unscaledPoints, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 0
  %m_numPoints12 = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 2
  %26 = load i32, i32* %m_numPoints12, align 4, !tbaa !10
  %call13 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %vec, %class.btVector3* %arrayidx, i32 %26, float* nonnull align 4 dereferenceable(4) %maxDot)
  store i32 %call13, i32* %index, align 4, !tbaa !14
  %27 = load i32, i32* %index, align 4, !tbaa !14
  call void @_ZNK23btConvexPointCloudShape14getScaledPointEi(%class.btVector3* sret align 4 %agg.result, %class.btConvexPointCloudShape* %this1, i32 %27)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  br label %cleanup

if.end14:                                         ; preds = %if.end
  %29 = bitcast %class.btVector3* %agg.result to i8*
  %30 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !6
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end14, %if.then11
  %31 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  %33 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !8
  %0 = load float, float* %y.addr, align 4, !tbaa !8
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !8
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %maxDot = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4, !tbaa !2
  store i32 %array_count, i32* %array_count.addr, align 4, !tbaa !15
  store float* %dotOut, float** %dotOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0xC7EFFFFFE0000000, float* %maxDot, align 4, !tbaa !8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !14
  %2 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 -1, i32* %ptIndex, align 4, !tbaa !14
  store i32 0, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !14
  %4 = load i32, i32* %array_count.addr, align 4, !tbaa !15
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast float* %dot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %array.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4, !tbaa !8
  %8 = load float, float* %dot, align 4, !tbaa !8
  %9 = load float, float* %maxDot, align 4, !tbaa !8
  %cmp2 = fcmp ogt float %8, %9
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load float, float* %dot, align 4, !tbaa !8
  store float %10, float* %maxDot, align 4, !tbaa !8
  %11 = load i32, i32* %i, align 4, !tbaa !14
  store i32 %11, i32* %ptIndex, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %12 = bitcast float* %dot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load i32, i32* %i, align 4, !tbaa !14
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load float, float* %maxDot, align 4, !tbaa !8
  %15 = load float*, float** %dotOut.addr, align 4, !tbaa !2
  store float %14, float* %15, align 4, !tbaa !8
  %16 = load i32, i32* %ptIndex, align 4, !tbaa !14
  %17 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  ret i32 %16
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK23btConvexPointCloudShape14getScaledPointEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexPointCloudShape* %this, i32 %index) #4 comdat {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !14
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 1
  %0 = load %class.btVector3*, %class.btVector3** %m_unscaledPoints, align 4, !tbaa !13
  %1 = load i32, i32* %index.addr, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  %2 = bitcast %class.btConvexPointCloudShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  ret void
}

define hidden void @_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btConvexPointCloudShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %vec = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %maxDot = alloca float, align 4
  %index = alloca i32, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !14
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %j, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !14
  %2 = load i32, i32* %numVectors.addr, align 4, !tbaa !14
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %7 = load i32, i32* %j, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %8 = bitcast %class.btConvexPointCloudShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %8, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  store %class.btVector3* %ref.tmp, %class.btVector3** %vec, align 4, !tbaa !2
  %9 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  %m_unscaledPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 1
  %12 = load %class.btVector3*, %class.btVector3** %m_unscaledPoints, align 4, !tbaa !13
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0
  %m_numPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 2
  %13 = load i32, i32* %m_numPoints, align 4, !tbaa !10
  %call = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %11, %class.btVector3* %arrayidx2, i32 %13, float* nonnull align 4 dereferenceable(4) %maxDot)
  store i32 %call, i32* %index, align 4, !tbaa !14
  %14 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %15 = load i32, i32* %j, align 4, !tbaa !14
  %arrayidx3 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx3)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 3
  store float 0xC3ABC16D60000000, float* %arrayidx5, align 4, !tbaa !8
  %16 = load i32, i32* %index, align 4, !tbaa !14
  %cmp6 = icmp sle i32 0, %16
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %17 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  %18 = load i32, i32* %index, align 4, !tbaa !14
  call void @_ZNK23btConvexPointCloudShape14getScaledPointEi(%class.btVector3* sret align 4 %ref.tmp7, %class.btConvexPointCloudShape* %this1, i32 %18)
  %19 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %20 = load i32, i32* %j, align 4, !tbaa !14
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 %20
  %21 = bitcast %class.btVector3* %arrayidx8 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !6
  %23 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %24 = load float, float* %maxDot, align 4, !tbaa !8
  %25 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %26 = load i32, i32* %j, align 4, !tbaa !14
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 %26
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 3
  store float %24, float* %arrayidx11, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %27 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %31 = load i32, i32* %j, align 4, !tbaa !14
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %j, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define hidden void @_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexPointCloudShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %vecnorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %1 = bitcast %class.btConvexPointCloudShape* %this1 to void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)*** %1, align 4, !tbaa !17
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)** %vtable, i64 17
  %2 = load void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexPointCloudShape*, %class.btVector3*)** %vfn, align 4
  call void %2(%class.btVector3* sret align 4 %agg.result, %class.btConvexPointCloudShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %3 = bitcast %class.btConvexPointCloudShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to float (%class.btConvexInternalShape*)***
  %vtable2 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %4, align 4, !tbaa !17
  %vfn3 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable2, i64 12
  %5 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn3, align 4
  %call = call float %5(%class.btConvexInternalShape* %3)
  %cmp = fcmp une float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %6 = bitcast %class.btVector3* %vecnorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %vecnorm to i8*
  %9 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !6
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vecnorm)
  %cmp5 = fcmp olt float %call4, 0x3D10000000000000
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %10 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float -1.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %12 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float -1.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vecnorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %13 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vecnorm)
  %16 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %18 = bitcast %class.btConvexPointCloudShape* %this1 to %class.btConvexInternalShape*
  %19 = bitcast %class.btConvexInternalShape* %18 to float (%class.btConvexInternalShape*)***
  %vtable12 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %19, align 4, !tbaa !17
  %vfn13 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable12, i64 12
  %20 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn13, align 4
  %call14 = call float %20(%class.btConvexInternalShape* %18)
  store float %call14, float* %ref.tmp11, align 4, !tbaa !8
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %vecnorm)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #8
  %23 = bitcast %class.btVector3* %vecnorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  br label %if.end16

if.end16:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define hidden i32 @_ZNK23btConvexPointCloudShape14getNumVerticesEv(%class.btConvexPointCloudShape* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %m_numPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numPoints, align 4, !tbaa !10
  ret i32 %0
}

; Function Attrs: nounwind
define hidden i32 @_ZNK23btConvexPointCloudShape11getNumEdgesEv(%class.btConvexPointCloudShape* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_(%class.btConvexPointCloudShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !14
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  ret void
}

define hidden void @_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3(%class.btConvexPointCloudShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %i.addr = alloca i32, align 4
  %vtx.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !14
  store %class.btVector3* %vtx, %class.btVector3** %vtx.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %m_unscaledPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %m_unscaledPoints, align 4, !tbaa !13
  %2 = load i32, i32* %i.addr, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btConvexPointCloudShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %4 = load %class.btVector3*, %class.btVector3** %vtx.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !6
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  ret void
}

; Function Attrs: nounwind
define hidden i32 @_ZNK23btConvexPointCloudShape12getNumPlanesEv(%class.btConvexPointCloudShape* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i(%class.btConvexPointCloudShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 %2) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  %.addr2 = alloca i32, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4, !tbaa !2
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4, !tbaa !2
  store i32 %2, i32* %.addr2, align 4, !tbaa !14
  %this3 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden zeroext i1 @_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f(%class.btConvexPointCloudShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float %1) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca float, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4, !tbaa !2
  store float %1, float* %.addr1, align 4, !tbaa !8
  %this2 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  ret i1 false
}

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btConvexPointCloudShapeD0Ev(%class.btConvexPointCloudShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %call = call %class.btConvexPointCloudShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btConvexPointCloudShape* (%class.btConvexPointCloudShape*)*)(%class.btConvexPointCloudShape* %this1) #8
  %0 = bitcast %class.btConvexPointCloudShape* %this1 to i8*
  call void @_ZN23btConvexPointCloudShapedlEPv(i8* %0) #8
  ret void
}

declare void @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_(%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #2

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #2

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

declare void @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3(%class.btPolyhedralConvexShape*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK23btConvexPointCloudShape7getNameEv(%class.btConvexPointCloudShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !8
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !8
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !19
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !19
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !19
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !22
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #2

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #2

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !14
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btConvexPointCloudShapedlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #2

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !14
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !8
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !14
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{i64 0, i64 16, !7}
!7 = !{!4, !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !12, i64 96}
!11 = !{!"_ZTS23btConvexPointCloudShape", !3, i64 92, !12, i64 96}
!12 = !{!"int", !4, i64 0}
!13 = !{!11, !3, i64 92}
!14 = !{!12, !12, i64 0}
!15 = !{!16, !16, i64 0}
!16 = !{!"long", !4, i64 0}
!17 = !{!18, !18, i64 0}
!18 = !{!"vtable pointer", !5, i64 0}
!19 = !{!20, !9, i64 44}
!20 = !{!"_ZTS21btConvexInternalShape", !21, i64 12, !21, i64 28, !9, i64 44, !9, i64 48}
!21 = !{!"_ZTS9btVector3", !4, i64 0}
!22 = !{!23, !9, i64 44}
!23 = !{!"_ZTS25btConvexInternalShapeData", !24, i64 0, !25, i64 12, !25, i64 28, !9, i64 44, !12, i64 48}
!24 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !12, i64 4, !4, i64 8}
!25 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
