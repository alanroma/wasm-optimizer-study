; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btHashedSimplePairCache.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btHashedSimplePairCache.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btHashedSimplePairCache = type { i32 (...)**, %class.btAlignedObjectArray, i8, [3 x i8], %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSimplePair*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSimplePair = type { i32, i32, %union.anon }
%union.anon = type { i8* }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }

$_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairED2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN23btHashedSimplePairCache7getHashEjj = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN23btHashedSimplePairCache10equalsPairERK12btSimplePairii = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN23btHashedSimplePairCache16internalFindPairEiii = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE21expandNonInitializingEv = comdat any

$_ZN12btSimplePairC2Eii = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE8pop_backEv = comdat any

$_ZN23btHashedSimplePairCache18addOverlappingPairEii = comdat any

$_ZN23btHashedSimplePairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi = comdat any

@gOverlappingSimplePairs = hidden global i32 0, align 4
@gRemoveSimplePairs = hidden global i32 0, align 4
@gAddedSimplePairs = hidden global i32 0, align 4
@gFindSimplePairs = hidden global i32 0, align 4
@_ZTV23btHashedSimplePairCache = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI23btHashedSimplePairCache to i8*), i8* bitcast (%class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)* @_ZN23btHashedSimplePairCacheD1Ev to i8*), i8* bitcast (void (%class.btHashedSimplePairCache*)* @_ZN23btHashedSimplePairCacheD0Ev to i8*), i8* bitcast (i8* (%class.btHashedSimplePairCache*, i32, i32)* @_ZN23btHashedSimplePairCache21removeOverlappingPairEii to i8*), i8* bitcast (%struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)* @_ZN23btHashedSimplePairCache18addOverlappingPairEii to i8*), i8* bitcast (%struct.btSimplePair* (%class.btHashedSimplePairCache*)* @_ZN23btHashedSimplePairCache26getOverlappingPairArrayPtrEv to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS23btHashedSimplePairCache = hidden constant [26 x i8] c"23btHashedSimplePairCache\00", align 1
@_ZTI23btHashedSimplePairCache = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btHashedSimplePairCache, i32 0, i32 0) }, align 4

@_ZN23btHashedSimplePairCacheC1Ev = hidden unnamed_addr alias %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*), %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)* @_ZN23btHashedSimplePairCacheC2Ev
@_ZN23btHashedSimplePairCacheD1Ev = hidden unnamed_addr alias %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*), %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)* @_ZN23btHashedSimplePairCacheD2Ev

define hidden %class.btHashedSimplePairCache* @_ZN23btHashedSimplePairCacheC2Ev(%class.btHashedSimplePairCache* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %initialAllocatedSize = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = bitcast %class.btHashedSimplePairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV23btHashedSimplePairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %m_blockedForChanges = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 2
  store i8 0, i8* %m_blockedForChanges, align 4, !tbaa !8
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.0* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %call3 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.0* %m_next)
  %1 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store i32 2, i32* %initialAllocatedSize, align 4, !tbaa !16
  %m_overlappingPairArray4 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %2 = load i32, i32* %initialAllocatedSize, align 4, !tbaa !16
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray* %m_overlappingPairArray4, i32 %2)
  call void @_ZN23btHashedSimplePairCache10growTablesEv(%class.btHashedSimplePairCache* %this1)
  %3 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret %class.btHashedSimplePairCache* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btSimplePair*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !16
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btSimplePair** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !16
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btSimplePair*
  store %struct.btSimplePair* %3, %struct.btSimplePair** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btSimplePair*, %struct.btSimplePair** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btSimplePair* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !17
  %5 = load %struct.btSimplePair*, %struct.btSimplePair** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSimplePair* %5, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !16
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !19
  %7 = bitcast %struct.btSimplePair** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden void @_ZN23btHashedSimplePairCache10growTablesEv(%class.btHashedSimplePairCache* %this) #0 {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %pair = alloca %struct.btSimplePair*, align 4
  %indexA = alloca i32, align 4
  %indexB = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  store i32 %call, i32* %newCapacity, align 4, !tbaa !16
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %m_hashTable)
  %1 = load i32, i32* %newCapacity, align 4, !tbaa !16
  %cmp = icmp slt i32 %call2, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %m_hashTable3 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4, !tbaa !16
  %m_hashTable5 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %3 = load i32, i32* %newCapacity, align 4, !tbaa !16
  %4 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  store i32 0, i32* %ref.tmp, align 4, !tbaa !16
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.0* %m_hashTable5, i32 %3, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %5 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %6 = load i32, i32* %newCapacity, align 4, !tbaa !16
  %7 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store i32 0, i32* %ref.tmp6, align 4, !tbaa !16
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.0* %m_next, i32 %6, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %8 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %i, align 4, !tbaa !16
  %11 = load i32, i32* %newCapacity, align 4, !tbaa !16
  %cmp7 = icmp slt i32 %10, %11
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %12 = load i32, i32* %i, align 4, !tbaa !16
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable8, i32 %12)
  store i32 -1, i32* %call9, align 4, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !16
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %14 = load i32, i32* %i, align 4, !tbaa !16
  %15 = load i32, i32* %newCapacity, align 4, !tbaa !16
  %cmp11 = icmp slt i32 %14, %15
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %16 = load i32, i32* %i, align 4, !tbaa !16
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next13, i32 %16)
  store i32 -1, i32* %call14, align 4, !tbaa !16
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %17 = load i32, i32* %i, align 4, !tbaa !16
  %inc16 = add nsw i32 %17, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !16
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4, !tbaa !16
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc32, %for.end17
  %18 = load i32, i32* %i, align 4, !tbaa !16
  %19 = load i32, i32* %curHashtableSize, align 4, !tbaa !16
  %cmp19 = icmp slt i32 %18, %19
  br i1 %cmp19, label %for.body20, label %for.end34

for.body20:                                       ; preds = %for.cond18
  %20 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %m_overlappingPairArray21 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %21 = load i32, i32* %i, align 4, !tbaa !16
  %call22 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray21, i32 %21)
  store %struct.btSimplePair* %call22, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %22 = bitcast i32* %indexA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %23 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %23, i32 0, i32 0
  %24 = load i32, i32* %m_indexA, align 4, !tbaa !20
  store i32 %24, i32* %indexA, align 4, !tbaa !16
  %25 = bitcast i32* %indexB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %26, i32 0, i32 1
  %27 = load i32, i32* %m_indexB, align 4, !tbaa !22
  store i32 %27, i32* %indexB, align 4, !tbaa !16
  %28 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load i32, i32* %indexA, align 4, !tbaa !16
  %30 = load i32, i32* %indexB, align 4, !tbaa !16
  %call23 = call i32 @_ZN23btHashedSimplePairCache7getHashEjj(%class.btHashedSimplePairCache* %this1, i32 %29, i32 %30)
  %m_overlappingPairArray24 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call25 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray24)
  %sub = sub nsw i32 %call25, 1
  %and = and i32 %call23, %sub
  store i32 %and, i32* %hashValue, align 4, !tbaa !16
  %m_hashTable26 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %31 = load i32, i32* %hashValue, align 4, !tbaa !16
  %call27 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable26, i32 %31)
  %32 = load i32, i32* %call27, align 4, !tbaa !16
  %m_next28 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %33 = load i32, i32* %i, align 4, !tbaa !16
  %call29 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next28, i32 %33)
  store i32 %32, i32* %call29, align 4, !tbaa !16
  %34 = load i32, i32* %i, align 4, !tbaa !16
  %m_hashTable30 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %35 = load i32, i32* %hashValue, align 4, !tbaa !16
  %call31 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable30, i32 %35)
  store i32 %34, i32* %call31, align 4, !tbaa !16
  %36 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast i32* %indexB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast i32* %indexA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  br label %for.inc32

for.inc32:                                        ; preds = %for.body20
  %40 = load i32, i32* %i, align 4, !tbaa !16
  %inc33 = add nsw i32 %40, 1
  store i32 %inc33, i32* %i, align 4, !tbaa !16
  br label %for.cond18

for.end34:                                        ; preds = %for.cond18
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  br label %if.end

if.end:                                           ; preds = %for.end34, %entry
  %43 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden %class.btHashedSimplePairCache* @_ZN23btHashedSimplePairCacheD2Ev(%class.btHashedSimplePairCache* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = bitcast %class.btHashedSimplePairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV23btHashedSimplePairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.0* %m_next) #7
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.0* %m_hashTable) #7
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairED2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray) #7
  ret %class.btHashedSimplePairCache* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN23btHashedSimplePairCacheD0Ev(%class.btHashedSimplePairCache* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %call = call %class.btHashedSimplePairCache* @_ZN23btHashedSimplePairCacheD1Ev(%class.btHashedSimplePairCache* %this1) #7
  %0 = bitcast %class.btHashedSimplePairCache* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

define hidden void @_ZN23btHashedSimplePairCache14removeAllPairsEv(%class.btHashedSimplePairCache* %this) #0 {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %initialAllocatedSize = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.0* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.0* %m_next)
  %0 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 2, i32* %initialAllocatedSize, align 4, !tbaa !16
  %m_overlappingPairArray2 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %1 = load i32, i32* %initialAllocatedSize, align 4, !tbaa !16
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray* %m_overlappingPairArray2, i32 %1)
  call void @_ZN23btHashedSimplePairCache10growTablesEv(%class.btHashedSimplePairCache* %this1)
  %2 = bitcast i32* %initialAllocatedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

define hidden %struct.btSimplePair* @_ZN23btHashedSimplePairCache8findPairEii(%class.btHashedSimplePairCache* %this, i32 %indexA, i32 %indexB) #0 {
entry:
  %retval = alloca %struct.btSimplePair*, align 4
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  %hash = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !16
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = load i32, i32* @gFindSimplePairs, align 4, !tbaa !16
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gFindSimplePairs, align 4, !tbaa !16
  %1 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %3 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %call = call i32 @_ZN23btHashedSimplePairCache7getHashEjj(%class.btHashedSimplePairCache* %this1, i32 %2, i32 %3)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !16
  %4 = load i32, i32* %hash, align 4, !tbaa !16
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %m_hashTable)
  %cmp = icmp sge i32 %4, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btSimplePair* null, %struct.btSimplePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

if.end:                                           ; preds = %entry
  %5 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %m_hashTable4 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %6 = load i32, i32* %hash, align 4, !tbaa !16
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable4, i32 %6)
  %7 = load i32, i32* %call5, align 4, !tbaa !16
  store i32 %7, i32* %index, align 4, !tbaa !16
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %8 = load i32, i32* %index, align 4, !tbaa !16
  %cmp6 = icmp ne i32 %8, -1
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %m_overlappingPairArray7 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %9 = load i32, i32* %index, align 4, !tbaa !16
  %call8 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %9)
  %10 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %11 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %call9 = call zeroext i1 @_ZN23btHashedSimplePairCache10equalsPairERK12btSimplePairii(%class.btHashedSimplePairCache* %this1, %struct.btSimplePair* nonnull align 4 dereferenceable(12) %call8, i32 %10, i32 %11)
  %conv = zext i1 %call9 to i32
  %cmp10 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %12 = phi i1 [ false, %while.cond ], [ %cmp10, %land.rhs ]
  br i1 %12, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %13 = load i32, i32* %index, align 4, !tbaa !16
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %13)
  %14 = load i32, i32* %call11, align 4, !tbaa !16
  store i32 %14, i32* %index, align 4, !tbaa !16
  br label %while.cond

while.end:                                        ; preds = %land.end
  %15 = load i32, i32* %index, align 4, !tbaa !16
  %cmp12 = icmp eq i32 %15, -1
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %while.end
  store %struct.btSimplePair* null, %struct.btSimplePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %while.end
  %m_overlappingPairArray15 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %16 = load i32, i32* %index, align 4, !tbaa !16
  %call16 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray15, i32 %16)
  store %struct.btSimplePair* %call16, %struct.btSimplePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end14, %if.then13
  %17 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %cleanup17

cleanup17:                                        ; preds = %cleanup, %if.then
  %18 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  %19 = load %struct.btSimplePair*, %struct.btSimplePair** %retval, align 4
  ret %struct.btSimplePair* %19
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN23btHashedSimplePairCache7getHashEjj(%class.btHashedSimplePairCache* %this, i32 %indexA, i32 %indexB) #5 comdat {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  %key = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !16
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = bitcast i32* %key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %2 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %shl = shl i32 %2, 16
  %or = or i32 %1, %shl
  store i32 %or, i32* %key, align 4, !tbaa !16
  %3 = load i32, i32* %key, align 4, !tbaa !16
  %shl2 = shl i32 %3, 15
  %neg = xor i32 %shl2, -1
  %4 = load i32, i32* %key, align 4, !tbaa !16
  %add = add nsw i32 %4, %neg
  store i32 %add, i32* %key, align 4, !tbaa !16
  %5 = load i32, i32* %key, align 4, !tbaa !16
  %shr = ashr i32 %5, 10
  %6 = load i32, i32* %key, align 4, !tbaa !16
  %xor = xor i32 %6, %shr
  store i32 %xor, i32* %key, align 4, !tbaa !16
  %7 = load i32, i32* %key, align 4, !tbaa !16
  %shl3 = shl i32 %7, 3
  %8 = load i32, i32* %key, align 4, !tbaa !16
  %add4 = add nsw i32 %8, %shl3
  store i32 %add4, i32* %key, align 4, !tbaa !16
  %9 = load i32, i32* %key, align 4, !tbaa !16
  %shr5 = ashr i32 %9, 6
  %10 = load i32, i32* %key, align 4, !tbaa !16
  %xor6 = xor i32 %10, %shr5
  store i32 %xor6, i32* %key, align 4, !tbaa !16
  %11 = load i32, i32* %key, align 4, !tbaa !16
  %shl7 = shl i32 %11, 11
  %neg8 = xor i32 %shl7, -1
  %12 = load i32, i32* %key, align 4, !tbaa !16
  %add9 = add nsw i32 %12, %neg8
  store i32 %add9, i32* %key, align 4, !tbaa !16
  %13 = load i32, i32* %key, align 4, !tbaa !16
  %shr10 = ashr i32 %13, 16
  %14 = load i32, i32* %key, align 4, !tbaa !16
  %xor11 = xor i32 %14, %shr10
  store i32 %xor11, i32* %key, align 4, !tbaa !16
  %15 = load i32, i32* %key, align 4, !tbaa !16
  %16 = bitcast i32* %key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  ret i32 %15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !19
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !23
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !24
  %1 = load i32, i32* %n.addr, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN23btHashedSimplePairCache10equalsPairERK12btSimplePairii(%class.btHashedSimplePairCache* %this, %struct.btSimplePair* nonnull align 4 dereferenceable(12) %pair, i32 %indexA, i32 %indexB) #5 comdat {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %pair.addr = alloca %struct.btSimplePair*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  store %struct.btSimplePair* %pair, %struct.btSimplePair** %pair.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !16
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %pair.addr, align 4, !tbaa !2
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %0, i32 0, i32 0
  %1 = load i32, i32* %m_indexA, align 4, !tbaa !20
  %2 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %cmp = icmp eq i32 %1, %2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %3 = load %struct.btSimplePair*, %struct.btSimplePair** %pair.addr, align 4, !tbaa !2
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %3, i32 0, i32 1
  %4 = load i32, i32* %m_indexB, align 4, !tbaa !22
  %5 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %cmp2 = icmp eq i32 %4, %5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %cmp2, %land.rhs ]
  ret i1 %6
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %1 = load i32, i32* %n.addr, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %0, i32 %1
  ret %struct.btSimplePair* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.0* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !16
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !16
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !16
  %2 = load i32, i32* %curSize, align 4, !tbaa !16
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !16
  store i32 %4, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !16
  %6 = load i32, i32* %curSize, align 4, !tbaa !16
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !24
  %9 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !16
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !16
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !16
  store i32 %14, i32* %i6, align 4, !tbaa !16
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !16
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !16
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !24
  %19 = load i32, i32* %i6, align 4, !tbaa !16
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !16
  store i32 %23, i32* %21, align 4, !tbaa !16
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !16
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !16
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !16
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !23
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  ret void
}

define hidden %struct.btSimplePair* @_ZN23btHashedSimplePairCache15internalAddPairEii(%class.btHashedSimplePairCache* %this, i32 %indexA, i32 %indexB) #0 {
entry:
  %retval = alloca %struct.btSimplePair*, align 4
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  %hash = alloca i32, align 4
  %pair = alloca %struct.btSimplePair*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %mem = alloca i8*, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !16
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %2 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %call = call i32 @_ZN23btHashedSimplePairCache7getHashEjj(%class.btHashedSimplePairCache* %this1, i32 %1, i32 %2)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !16
  %3 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %5 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %6 = load i32, i32* %hash, align 4, !tbaa !16
  %call3 = call %struct.btSimplePair* @_ZN23btHashedSimplePairCache16internalFindPairEiii(%class.btHashedSimplePairCache* %this1, i32 %4, i32 %5, i32 %6)
  store %struct.btSimplePair* %call3, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %7 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %cmp = icmp ne %struct.btSimplePair* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  store %struct.btSimplePair* %8, %struct.btSimplePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %m_overlappingPairArray4 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray4)
  store i32 %call5, i32* %count, align 4, !tbaa !16
  %10 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %m_overlappingPairArray6 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray6)
  store i32 %call7, i32* %oldCapacity, align 4, !tbaa !16
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %m_overlappingPairArray8 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call9 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %m_overlappingPairArray8)
  %12 = bitcast %struct.btSimplePair* %call9 to i8*
  store i8* %12, i8** %mem, align 4, !tbaa !2
  %13 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %m_overlappingPairArray10 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray10)
  store i32 %call11, i32* %newCapacity, align 4, !tbaa !16
  %14 = load i32, i32* %oldCapacity, align 4, !tbaa !16
  %15 = load i32, i32* %newCapacity, align 4, !tbaa !16
  %cmp12 = icmp slt i32 %14, %15
  br i1 %cmp12, label %if.then13, label %if.end19

if.then13:                                        ; preds = %if.end
  call void @_ZN23btHashedSimplePairCache10growTablesEv(%class.btHashedSimplePairCache* %this1)
  %16 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %17 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %call14 = call i32 @_ZN23btHashedSimplePairCache7getHashEjj(%class.btHashedSimplePairCache* %this1, i32 %16, i32 %17)
  %m_overlappingPairArray15 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call16 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray15)
  %sub17 = sub nsw i32 %call16, 1
  %and18 = and i32 %call14, %sub17
  store i32 %and18, i32* %hash, align 4, !tbaa !16
  br label %if.end19

if.end19:                                         ; preds = %if.then13, %if.end
  %18 = load i8*, i8** %mem, align 4, !tbaa !2
  %19 = bitcast i8* %18 to %struct.btSimplePair*
  %20 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %21 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %call20 = call %struct.btSimplePair* @_ZN12btSimplePairC2Eii(%struct.btSimplePair* %19, i32 %20, i32 %21)
  store %struct.btSimplePair* %19, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %22 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %23 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %22, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %23 to i8**
  store i8* null, i8** %m_userPointer, align 4, !tbaa !25
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %24 = load i32, i32* %hash, align 4, !tbaa !16
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable, i32 %24)
  %25 = load i32, i32* %call21, align 4, !tbaa !16
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %26 = load i32, i32* %count, align 4, !tbaa !16
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %26)
  store i32 %25, i32* %call22, align 4, !tbaa !16
  %27 = load i32, i32* %count, align 4, !tbaa !16
  %m_hashTable23 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %28 = load i32, i32* %hash, align 4, !tbaa !16
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable23, i32 %28)
  store i32 %27, i32* %call24, align 4, !tbaa !16
  %29 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  store %struct.btSimplePair* %29, %struct.btSimplePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  br label %cleanup

cleanup:                                          ; preds = %if.end19, %if.then
  %34 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = load %struct.btSimplePair*, %struct.btSimplePair** %retval, align 4
  ret %struct.btSimplePair* %36
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btSimplePair* @_ZN23btHashedSimplePairCache16internalFindPairEiii(%class.btHashedSimplePairCache* %this, i32 %proxyIdA, i32 %proxyIdB, i32 %hash) #2 comdat {
entry:
  %retval = alloca %struct.btSimplePair*, align 4
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %proxyIdA.addr = alloca i32, align 4
  %proxyIdB.addr = alloca i32, align 4
  %hash.addr = alloca i32, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  store i32 %proxyIdA, i32* %proxyIdA.addr, align 4, !tbaa !16
  store i32 %proxyIdB, i32* %proxyIdB.addr, align 4, !tbaa !16
  store i32 %hash, i32* %hash.addr, align 4, !tbaa !16
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %1 = load i32, i32* %hash.addr, align 4, !tbaa !16
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable, i32 %1)
  %2 = load i32, i32* %call, align 4, !tbaa !16
  store i32 %2, i32* %index, align 4, !tbaa !16
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %index, align 4, !tbaa !16
  %cmp = icmp ne i32 %3, -1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %4 = load i32, i32* %index, align 4, !tbaa !16
  %call2 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 %4)
  %5 = load i32, i32* %proxyIdA.addr, align 4, !tbaa !16
  %6 = load i32, i32* %proxyIdB.addr, align 4, !tbaa !16
  %call3 = call zeroext i1 @_ZN23btHashedSimplePairCache10equalsPairERK12btSimplePairii(%class.btHashedSimplePairCache* %this1, %struct.btSimplePair* nonnull align 4 dereferenceable(12) %call2, i32 %5, i32 %6)
  %conv = zext i1 %call3 to i32
  %cmp4 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %7 = phi i1 [ false, %while.cond ], [ %cmp4, %land.rhs ]
  br i1 %7, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %8 = load i32, i32* %index, align 4, !tbaa !16
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %8)
  %9 = load i32, i32* %call5, align 4, !tbaa !16
  store i32 %9, i32* %index, align 4, !tbaa !16
  br label %while.cond

while.end:                                        ; preds = %land.end
  %10 = load i32, i32* %index, align 4, !tbaa !16
  %cmp6 = icmp eq i32 %10, -1
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  store %struct.btSimplePair* null, %struct.btSimplePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.end
  %m_overlappingPairArray7 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %11 = load i32, i32* %index, align 4, !tbaa !16
  %call8 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %11)
  store %struct.btSimplePair* %call8, %struct.btSimplePair** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  %13 = load %struct.btSimplePair*, %struct.btSimplePair** %retval, align 4
  ret %struct.btSimplePair* %13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !26
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !16
  %1 = load i32, i32* %sz, align 4, !tbaa !16
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !26
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !26
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %4 = load i32, i32* %sz, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %3, i32 %4
  %5 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  ret %struct.btSimplePair* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btSimplePair* @_ZN12btSimplePairC2Eii(%struct.btSimplePair* returned %this, i32 %indexA, i32 %indexB) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btSimplePair*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  store %struct.btSimplePair* %this, %struct.btSimplePair** %this.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !16
  %this1 = load %struct.btSimplePair*, %struct.btSimplePair** %this.addr, align 4
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 0
  %0 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %0, i32* %m_indexA, align 4, !tbaa !20
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 1
  %1 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  store i32 %1, i32* %m_indexB, align 4, !tbaa !22
  %2 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %2 to i8**
  store i8* null, i8** %m_userPointer, align 4, !tbaa !25
  ret %struct.btSimplePair* %this1
}

define hidden i8* @_ZN23btHashedSimplePairCache21removeOverlappingPairEii(%class.btHashedSimplePairCache* %this, i32 %indexA, i32 %indexB) unnamed_addr #0 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  %hash = alloca i32, align 4
  %pair = alloca %struct.btSimplePair*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %userData = alloca i8*, align 4
  %pairIndex = alloca i32, align 4
  %index = alloca i32, align 4
  %previous = alloca i32, align 4
  %lastPairIndex = alloca i32, align 4
  %last = alloca %struct.btSimplePair*, align 4
  %lastHash = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !16
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = load i32, i32* @gRemoveSimplePairs, align 4, !tbaa !16
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gRemoveSimplePairs, align 4, !tbaa !16
  %1 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %3 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %call = call i32 @_ZN23btHashedSimplePairCache7getHashEjj(%class.btHashedSimplePairCache* %this1, i32 %2, i32 %3)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !16
  %4 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %6 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %7 = load i32, i32* %hash, align 4, !tbaa !16
  %call3 = call %struct.btSimplePair* @_ZN23btHashedSimplePairCache16internalFindPairEiii(%class.btHashedSimplePairCache* %this1, i32 %5, i32 %6, i32 %7)
  store %struct.btSimplePair* %call3, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %8 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %cmp = icmp eq %struct.btSimplePair* %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup69

if.end:                                           ; preds = %entry
  %9 = bitcast i8** %userData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %11 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %10, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %11 to i8**
  %12 = load i8*, i8** %m_userPointer, align 4, !tbaa !25
  store i8* %12, i8** %userData, align 4, !tbaa !2
  %13 = bitcast i32* %pairIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %m_overlappingPairArray4 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray4, i32 0)
  %sub.ptr.lhs.cast = ptrtoint %struct.btSimplePair* %14 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.btSimplePair* %call5 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  store i32 %sub.ptr.div, i32* %pairIndex, align 4, !tbaa !16
  %15 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %m_hashTable = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %16 = load i32, i32* %hash, align 4, !tbaa !16
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable, i32 %16)
  %17 = load i32, i32* %call6, align 4, !tbaa !16
  store i32 %17, i32* %index, align 4, !tbaa !16
  %18 = bitcast i32* %previous to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  store i32 -1, i32* %previous, align 4, !tbaa !16
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %19 = load i32, i32* %index, align 4, !tbaa !16
  %20 = load i32, i32* %pairIndex, align 4, !tbaa !16
  %cmp7 = icmp ne i32 %19, %20
  br i1 %cmp7, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %21 = load i32, i32* %index, align 4, !tbaa !16
  store i32 %21, i32* %previous, align 4, !tbaa !16
  %m_next = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %22 = load i32, i32* %index, align 4, !tbaa !16
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next, i32 %22)
  %23 = load i32, i32* %call8, align 4, !tbaa !16
  store i32 %23, i32* %index, align 4, !tbaa !16
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = load i32, i32* %previous, align 4, !tbaa !16
  %cmp9 = icmp ne i32 %24, -1
  br i1 %cmp9, label %if.then10, label %if.else

if.then10:                                        ; preds = %while.end
  %m_next11 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %25 = load i32, i32* %pairIndex, align 4, !tbaa !16
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next11, i32 %25)
  %26 = load i32, i32* %call12, align 4, !tbaa !16
  %m_next13 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %27 = load i32, i32* %previous, align 4, !tbaa !16
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next13, i32 %27)
  store i32 %26, i32* %call14, align 4, !tbaa !16
  br label %if.end19

if.else:                                          ; preds = %while.end
  %m_next15 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %28 = load i32, i32* %pairIndex, align 4, !tbaa !16
  %call16 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next15, i32 %28)
  %29 = load i32, i32* %call16, align 4, !tbaa !16
  %m_hashTable17 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %30 = load i32, i32* %hash, align 4, !tbaa !16
  %call18 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable17, i32 %30)
  store i32 %29, i32* %call18, align 4, !tbaa !16
  br label %if.end19

if.end19:                                         ; preds = %if.else, %if.then10
  %31 = bitcast i32* %lastPairIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %m_overlappingPairArray20 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call21 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray20)
  %sub22 = sub nsw i32 %call21, 1
  store i32 %sub22, i32* %lastPairIndex, align 4, !tbaa !16
  %32 = load i32, i32* %lastPairIndex, align 4, !tbaa !16
  %33 = load i32, i32* %pairIndex, align 4, !tbaa !16
  %cmp23 = icmp eq i32 %32, %33
  br i1 %cmp23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end19
  %m_overlappingPairArray25 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray25)
  %34 = load i8*, i8** %userData, align 4, !tbaa !2
  store i8* %34, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %if.end19
  %35 = bitcast %struct.btSimplePair** %last to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %m_overlappingPairArray27 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %36 = load i32, i32* %lastPairIndex, align 4, !tbaa !16
  %call28 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray27, i32 %36)
  store %struct.btSimplePair* %call28, %struct.btSimplePair** %last, align 4, !tbaa !2
  %37 = bitcast i32* %lastHash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #7
  %38 = load %struct.btSimplePair*, %struct.btSimplePair** %last, align 4, !tbaa !2
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %38, i32 0, i32 0
  %39 = load i32, i32* %m_indexA, align 4, !tbaa !20
  %40 = load %struct.btSimplePair*, %struct.btSimplePair** %last, align 4, !tbaa !2
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %40, i32 0, i32 1
  %41 = load i32, i32* %m_indexB, align 4, !tbaa !22
  %call29 = call i32 @_ZN23btHashedSimplePairCache7getHashEjj(%class.btHashedSimplePairCache* %this1, i32 %39, i32 %41)
  %m_overlappingPairArray30 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call31 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray30)
  %sub32 = sub nsw i32 %call31, 1
  %and33 = and i32 %call29, %sub32
  store i32 %and33, i32* %lastHash, align 4, !tbaa !16
  %m_hashTable34 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %42 = load i32, i32* %lastHash, align 4, !tbaa !16
  %call35 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable34, i32 %42)
  %43 = load i32, i32* %call35, align 4, !tbaa !16
  store i32 %43, i32* %index, align 4, !tbaa !16
  store i32 -1, i32* %previous, align 4, !tbaa !16
  br label %while.cond36

while.cond36:                                     ; preds = %while.body38, %if.end26
  %44 = load i32, i32* %index, align 4, !tbaa !16
  %45 = load i32, i32* %lastPairIndex, align 4, !tbaa !16
  %cmp37 = icmp ne i32 %44, %45
  br i1 %cmp37, label %while.body38, label %while.end41

while.body38:                                     ; preds = %while.cond36
  %46 = load i32, i32* %index, align 4, !tbaa !16
  store i32 %46, i32* %previous, align 4, !tbaa !16
  %m_next39 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %47 = load i32, i32* %index, align 4, !tbaa !16
  %call40 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next39, i32 %47)
  %48 = load i32, i32* %call40, align 4, !tbaa !16
  store i32 %48, i32* %index, align 4, !tbaa !16
  br label %while.cond36

while.end41:                                      ; preds = %while.cond36
  %49 = load i32, i32* %previous, align 4, !tbaa !16
  %cmp42 = icmp ne i32 %49, -1
  br i1 %cmp42, label %if.then43, label %if.else48

if.then43:                                        ; preds = %while.end41
  %m_next44 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %50 = load i32, i32* %lastPairIndex, align 4, !tbaa !16
  %call45 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next44, i32 %50)
  %51 = load i32, i32* %call45, align 4, !tbaa !16
  %m_next46 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %52 = load i32, i32* %previous, align 4, !tbaa !16
  %call47 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next46, i32 %52)
  store i32 %51, i32* %call47, align 4, !tbaa !16
  br label %if.end53

if.else48:                                        ; preds = %while.end41
  %m_next49 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %53 = load i32, i32* %lastPairIndex, align 4, !tbaa !16
  %call50 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next49, i32 %53)
  %54 = load i32, i32* %call50, align 4, !tbaa !16
  %m_hashTable51 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %55 = load i32, i32* %lastHash, align 4, !tbaa !16
  %call52 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable51, i32 %55)
  store i32 %54, i32* %call52, align 4, !tbaa !16
  br label %if.end53

if.end53:                                         ; preds = %if.else48, %if.then43
  %m_overlappingPairArray54 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %56 = load i32, i32* %lastPairIndex, align 4, !tbaa !16
  %call55 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray54, i32 %56)
  %m_overlappingPairArray56 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %57 = load i32, i32* %pairIndex, align 4, !tbaa !16
  %call57 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray56, i32 %57)
  %58 = bitcast %struct.btSimplePair* %call57 to i8*
  %59 = bitcast %struct.btSimplePair* %call55 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 12, i1 false), !tbaa.struct !27
  %m_hashTable58 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %60 = load i32, i32* %lastHash, align 4, !tbaa !16
  %call59 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable58, i32 %60)
  %61 = load i32, i32* %call59, align 4, !tbaa !16
  %m_next60 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 5
  %62 = load i32, i32* %pairIndex, align 4, !tbaa !16
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_next60, i32 %62)
  store i32 %61, i32* %call61, align 4, !tbaa !16
  %63 = load i32, i32* %pairIndex, align 4, !tbaa !16
  %m_hashTable62 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 4
  %64 = load i32, i32* %lastHash, align 4, !tbaa !16
  %call63 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.0* %m_hashTable62, i32 %64)
  store i32 %63, i32* %call63, align 4, !tbaa !16
  %m_overlappingPairArray64 = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray64)
  %65 = load i8*, i8** %userData, align 4, !tbaa !2
  store i8* %65, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %66 = bitcast i32* %lastHash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  %67 = bitcast %struct.btSimplePair** %last to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #7
  br label %cleanup

cleanup:                                          ; preds = %if.end53, %if.then24
  %68 = bitcast i32* %lastPairIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #7
  %69 = bitcast i32* %previous to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  %70 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  %71 = bitcast i32* %pairIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = bitcast i8** %userData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  br label %cleanup69

cleanup69:                                        ; preds = %cleanup, %if.then
  %73 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  %74 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  %75 = load i8*, i8** %retval, align 4
  ret i8* %75
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE8pop_backEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !26
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !26
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !26
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %1, i32 %2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

define linkonce_odr hidden %struct.btSimplePair* @_ZN23btHashedSimplePairCache18addOverlappingPairEii(%class.btHashedSimplePairCache* %this, i32 %indexA, i32 %indexB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !16
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !16
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %0 = load i32, i32* @gAddedSimplePairs, align 4, !tbaa !16
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gAddedSimplePairs, align 4, !tbaa !16
  %1 = load i32, i32* %indexA.addr, align 4, !tbaa !16
  %2 = load i32, i32* %indexB.addr, align 4, !tbaa !16
  %call = call %struct.btSimplePair* @_ZN23btHashedSimplePairCache15internalAddPairEii(%class.btHashedSimplePairCache* %this1, i32 %1, i32 %2)
  ret %struct.btSimplePair* %call
}

define linkonce_odr hidden %struct.btSimplePair* @_ZN23btHashedSimplePairCache26getOverlappingPairArrayPtrEv(%class.btHashedSimplePairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btSimplePair* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !17
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSimplePair* null, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !26
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !19
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.0* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !24
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !23
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !29
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !16
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !16
  %call = call %struct.btSimplePair* @_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btSimplePair** null)
  %2 = bitcast %struct.btSimplePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btSimplePair* %dest) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btSimplePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !16
  store i32 %end, i32* %end.addr, align 4, !tbaa !16
  store %struct.btSimplePair* %dest, %struct.btSimplePair** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !16
  store i32 %1, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !16
  %3 = load i32, i32* %end.addr, align 4, !tbaa !16
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btSimplePair*, %struct.btSimplePair** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %4, i32 %5
  %6 = bitcast %struct.btSimplePair* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btSimplePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %9 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx2 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %8, i32 %9
  %10 = bitcast %struct.btSimplePair* %7 to i8*
  %11 = bitcast %struct.btSimplePair* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 12, i1 false), !tbaa.struct !27
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !16
  store i32 %last, i32* %last.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !16
  store i32 %1, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !16
  %3 = load i32, i32* %last.addr, align 4, !tbaa !16
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %5 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !18
  %tobool = icmp ne %struct.btSimplePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !17, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data4, align 4, !tbaa !18
  call void @_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btSimplePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSimplePair* null, %struct.btSimplePair** %m_data5, align 4, !tbaa !18
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btSimplePair* @_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btSimplePair** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btSimplePair**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !16
  store %struct.btSimplePair** %hint, %struct.btSimplePair*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !16
  %mul = mul i32 12, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btSimplePair*
  ret %struct.btSimplePair* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btSimplePair* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btSimplePair*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btSimplePair* %ptr, %struct.btSimplePair** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btSimplePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !16
  store i32 %last, i32* %last.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !16
  store i32 %1, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !16
  %3 = load i32, i32* %last.addr, align 4, !tbaa !16
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !24
  %5 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !24
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !28, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !24
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.1* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !24
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.1* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !16
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !16
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !28
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !24
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !16
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !29
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.0* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !29
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !16
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !16
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.1* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, i32* %dest) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !16
  store i32 %end, i32* %end.addr, align 4, !tbaa !16
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !16
  store i32 %1, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !16
  %3 = load i32, i32* %end.addr, align 4, !tbaa !16
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !24
  %9 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !16
  store i32 %10, i32* %7, align 4, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.1* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !16
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !16
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !16
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !16
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !13, i64 24}
!9 = !{!"_ZTS23btHashedSimplePairCache", !10, i64 4, !13, i64 24, !14, i64 28, !14, i64 48}
!10 = !{!"_ZTS20btAlignedObjectArrayI12btSimplePairE", !11, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!11 = !{!"_ZTS18btAlignedAllocatorI12btSimplePairLj16EE"}
!12 = !{!"int", !4, i64 0}
!13 = !{!"bool", !4, i64 0}
!14 = !{!"_ZTS20btAlignedObjectArrayIiE", !15, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!15 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!16 = !{!12, !12, i64 0}
!17 = !{!10, !13, i64 16}
!18 = !{!10, !3, i64 12}
!19 = !{!10, !12, i64 8}
!20 = !{!21, !12, i64 0}
!21 = !{!"_ZTS12btSimplePair", !12, i64 0, !12, i64 4, !4, i64 8}
!22 = !{!21, !12, i64 4}
!23 = !{!14, !12, i64 4}
!24 = !{!14, !3, i64 12}
!25 = !{!4, !4, i64 0}
!26 = !{!10, !12, i64 4}
!27 = !{i64 0, i64 4, !16, i64 4, i64 4, !16, i64 8, i64 4, !2, i64 8, i64 4, !16}
!28 = !{!14, !13, i64 16}
!29 = !{!14, !12, i64 8}
!30 = !{i8 0, i8 2}
