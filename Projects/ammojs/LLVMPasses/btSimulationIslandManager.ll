; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btSimulationIslandManager.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btSimulationIslandManager.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSimulationIslandManager = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, i8, [3 x i8] }>
%class.btUnionFind = type { %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btDispatcher = type { i32 (...)** }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray.4, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btIDebugDraw = type opaque
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.8 }
%class.btCollisionAlgorithm = type opaque
%union.anon.8 = type { i8* }
%class.CProfileSample = type { i8 }
%"struct.btSimulationIslandManager::IslandCallback" = type { i32 (...)** }
%class.btPersistentManifoldSortPredicate = type { i8 }

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN16btCollisionWorld12getPairCacheEv = comdat any

$_ZNK17btCollisionObject23mergesSimulationIslandsEv = comdat any

$_ZN11btUnionFind5uniteEii = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZN16btCollisionWorld23getCollisionObjectArrayEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZN17btCollisionObject12setIslandTagEi = comdat any

$_ZN17btCollisionObject14setCompanionIdEi = comdat any

$_ZN17btCollisionObject14setHitFractionEf = comdat any

$_ZN11btUnionFind4findEi = comdat any

$_ZN11btUnionFind10getElementEi = comdat any

$_ZN14CProfileSampleC2EPKc = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN25btSimulationIslandManager12getUnionFindEv = comdat any

$_ZNK11btUnionFind14getNumElementsEv = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN17btCollisionObject19setDeactivationTimeEf = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK17btCollisionObject17isKinematicObjectEv = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZN14CProfileSampleD2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_ = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_Z11getIslandIdPK20btPersistentManifold = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE4sizeEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii = comdat any

$_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_ = comdat any

@_ZTV25btSimulationIslandManager = hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI25btSimulationIslandManager to i8*), i8* bitcast (%class.btSimulationIslandManager* (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerD1Ev to i8*), i8* bitcast (void (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerD0Ev to i8*), i8* bitcast (void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)* @_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher to i8*), i8* bitcast (void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)* @_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld to i8*)] }, align 4
@.str = private unnamed_addr constant [28 x i8] c"islandUnionFindAndQuickSort\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"processIslands\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS25btSimulationIslandManager = hidden constant [28 x i8] c"25btSimulationIslandManager\00", align 1
@_ZTI25btSimulationIslandManager = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btSimulationIslandManager, i32 0, i32 0) }, align 4

@_ZN25btSimulationIslandManagerC1Ev = hidden unnamed_addr alias %class.btSimulationIslandManager* (%class.btSimulationIslandManager*), %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerC2Ev
@_ZN25btSimulationIslandManagerD1Ev = hidden unnamed_addr alias %class.btSimulationIslandManager* (%class.btSimulationIslandManager*), %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerD2Ev

define hidden %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerC2Ev(%class.btSimulationIslandManager* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast %class.btSimulationIslandManager* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV25btSimulationIslandManager, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %call = call %class.btUnionFind* @_ZN11btUnionFindC1Ev(%class.btUnionFind* %m_unionFind)
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.0* %m_islandmanifold)
  %m_islandBodies = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray.4* %m_islandBodies)
  %m_splitIslands = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 4
  store i8 1, i8* %m_splitIslands, align 4, !tbaa !8
  ret %class.btSimulationIslandManager* %this1
}

declare %class.btUnionFind* @_ZN11btUnionFindC1Ev(%class.btUnionFind* returned) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: nounwind
define hidden %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerD2Ev(%class.btSimulationIslandManager* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast %class.btSimulationIslandManager* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV25btSimulationIslandManager, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_islandBodies = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray.4* %m_islandBodies) #8
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.0* %m_islandmanifold) #8
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %call3 = call %class.btUnionFind* @_ZN11btUnionFindD1Ev(%class.btUnionFind* %m_unionFind) #8
  ret %class.btSimulationIslandManager* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
declare %class.btUnionFind* @_ZN11btUnionFindD1Ev(%class.btUnionFind* returned) unnamed_addr #3

; Function Attrs: nounwind
define hidden void @_ZN25btSimulationIslandManagerD0Ev(%class.btSimulationIslandManager* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %call = call %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerD1Ev(%class.btSimulationIslandManager* %this1) #8
  %0 = bitcast %class.btSimulationIslandManager* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

define hidden void @_ZN25btSimulationIslandManager13initUnionFindEi(%class.btSimulationIslandManager* %this, i32 %n) #0 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %n.addr = alloca i32, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !19
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %0 = load i32, i32* %n.addr, align 4, !tbaa !19
  call void @_ZN11btUnionFind5resetEi(%class.btUnionFind* %m_unionFind, i32 %0)
  ret void
}

declare void @_ZN11btUnionFind5resetEi(%class.btUnionFind*, i32) #1

define hidden void @_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this, %class.btDispatcher* %0, %class.btCollisionWorld* %colWorld) #0 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %colWorld.addr = alloca %class.btCollisionWorld*, align 4
  %pairCachePtr = alloca %class.btOverlappingPairCache*, align 4
  %numOverlappingPairs = alloca i32, align 4
  %pairPtr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  %collisionPair = alloca %struct.btBroadphasePair*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %colWorld, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %1 = bitcast %class.btOverlappingPairCache** %pairCachePtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  %call = call %class.btOverlappingPairCache* @_ZN16btCollisionWorld12getPairCacheEv(%class.btCollisionWorld* %2)
  store %class.btOverlappingPairCache* %call, %class.btOverlappingPairCache** %pairCachePtr, align 4, !tbaa !2
  %3 = bitcast i32* %numOverlappingPairs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCachePtr, align 4, !tbaa !2
  %5 = bitcast %class.btOverlappingPairCache* %4 to i32 (%class.btOverlappingPairCache*)***
  %vtable = load i32 (%class.btOverlappingPairCache*)**, i32 (%class.btOverlappingPairCache*)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btOverlappingPairCache*)*, i32 (%class.btOverlappingPairCache*)** %vtable, i64 9
  %6 = load i32 (%class.btOverlappingPairCache*)*, i32 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call2 = call i32 %6(%class.btOverlappingPairCache* %4)
  store i32 %call2, i32* %numOverlappingPairs, align 4, !tbaa !19
  %7 = load i32, i32* %numOverlappingPairs, align 4, !tbaa !19
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %8 = bitcast %struct.btBroadphasePair** %pairPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCachePtr, align 4, !tbaa !2
  %10 = bitcast %class.btOverlappingPairCache* %9 to %struct.btBroadphasePair* (%class.btOverlappingPairCache*)***
  %vtable3 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCache*)*** %10, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCache*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*)** %vtable3, i64 5
  %11 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*)** %vfn4, align 4
  %call5 = call %struct.btBroadphasePair* %11(%class.btOverlappingPairCache* %9)
  store %struct.btBroadphasePair* %call5, %struct.btBroadphasePair** %pairPtr, align 4, !tbaa !2
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %13 = load i32, i32* %i, align 4, !tbaa !19
  %14 = load i32, i32* %numOverlappingPairs, align 4, !tbaa !19
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %16 = bitcast %struct.btBroadphasePair** %collisionPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pairPtr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 %18
  store %struct.btBroadphasePair* %arrayidx, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %19 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %20, i32 0, i32 0
  %21 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !20
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %21, i32 0, i32 0
  %22 = load i8*, i8** %m_clientObject, align 4, !tbaa !22
  %23 = bitcast i8* %22 to %class.btCollisionObject*
  store %class.btCollisionObject* %23, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %24 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %25, i32 0, i32 1
  %26 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !26
  %m_clientObject6 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %26, i32 0, i32 0
  %27 = load i8*, i8** %m_clientObject6, align 4, !tbaa !22
  %28 = bitcast i8* %27 to %class.btCollisionObject*
  store %class.btCollisionObject* %28, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %29 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %tobool7 = icmp ne %class.btCollisionObject* %29, null
  br i1 %tobool7, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %30 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call8 = call zeroext i1 @_ZNK17btCollisionObject23mergesSimulationIslandsEv(%class.btCollisionObject* %30)
  br i1 %call8, label %land.lhs.true9, label %if.end

land.lhs.true9:                                   ; preds = %land.lhs.true
  %31 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %tobool10 = icmp ne %class.btCollisionObject* %31, null
  br i1 %tobool10, label %land.lhs.true11, label %if.end

land.lhs.true11:                                  ; preds = %land.lhs.true9
  %32 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call12 = call zeroext i1 @_ZNK17btCollisionObject23mergesSimulationIslandsEv(%class.btCollisionObject* %32)
  br i1 %call12, label %if.then13, label %if.end

if.then13:                                        ; preds = %land.lhs.true11
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %33 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call14 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %33)
  %34 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call15 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %34)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %m_unionFind, i32 %call14, i32 %call15)
  br label %if.end

if.end:                                           ; preds = %if.then13, %land.lhs.true11, %land.lhs.true9, %land.lhs.true, %for.body
  %35 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast %struct.btBroadphasePair** %collisionPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %38 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %38, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %39 = bitcast %struct.btBroadphasePair** %pairPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  br label %if.end16

if.end16:                                         ; preds = %for.end, %entry
  %40 = bitcast i32* %numOverlappingPairs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast %class.btOverlappingPairCache** %pairCachePtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN16btCollisionWorld12getPairCacheEv(%class.btCollisionWorld* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4, !tbaa !27
  %1 = bitcast %class.btBroadphaseInterface* %0 to %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)**, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*** %1, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vtable, i64 9
  %2 = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %2(%class.btBroadphaseInterface* %0)
  ret %class.btOverlappingPairCache* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject23mergesSimulationIslandsEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !31
  %and = and i32 %0, 7
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

define linkonce_odr hidden void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %this, i32 %p, i32 %q) #0 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %p.addr = alloca i32, align 4
  %q.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %p, i32* %p.addr, align 4, !tbaa !19
  store i32 %q, i32* %q.addr, align 4, !tbaa !19
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %p.addr, align 4, !tbaa !19
  %call = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %1)
  store i32 %call, i32* %i, align 4, !tbaa !19
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i32, i32* %q.addr, align 4, !tbaa !19
  %call2 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %3)
  store i32 %call2, i32* %j, align 4, !tbaa !19
  %4 = load i32, i32* %i, align 4, !tbaa !19
  %5 = load i32, i32* %j, align 4, !tbaa !19
  %cmp = icmp eq i32 %4, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %j, align 4, !tbaa !19
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %7 = load i32, i32* %i, align 4, !tbaa !19
  %call3 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %7)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call3, i32 0, i32 0
  store i32 %6, i32* %m_id, align 4, !tbaa !35
  %m_elements4 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %i, align 4, !tbaa !19
  %call5 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements4, i32 %8)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call5, i32 0, i32 1
  %9 = load i32, i32* %m_sz, align 4, !tbaa !37
  %m_elements6 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %10 = load i32, i32* %j, align 4, !tbaa !19
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements6, i32 %10)
  %m_sz8 = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 1
  %11 = load i32, i32* %m_sz8, align 4, !tbaa !37
  %add = add nsw i32 %11, %9
  store i32 %add, i32* %m_sz8, align 4, !tbaa !37
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4, !tbaa !38
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

define hidden void @_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher(%class.btSimulationIslandManager* %this, %class.btCollisionWorld* %colWorld, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %colWorld.addr = alloca %class.btCollisionWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %colWorld, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %index, align 4, !tbaa !19
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !19
  %3 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %3)
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %call)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %5)
  %6 = load i32, i32* %i, align 4, !tbaa !19
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %call3, i32 %6)
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %call4, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call5 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %8)
  br i1 %call5, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %10 = load i32, i32* %index, align 4, !tbaa !19
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %index, align 4, !tbaa !19
  call void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %9, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %11, i32 -1)
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %12, float 1.000000e+00)
  %13 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %14 = load i32, i32* %i, align 4, !tbaa !19
  %inc6 = add nsw i32 %14, 1
  store i32 %inc6, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = load i32, i32* %index, align 4, !tbaa !19
  call void @_ZN25btSimulationIslandManager13initUnionFindEi(%class.btSimulationIslandManager* %this1, i32 %16)
  %17 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %18 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  call void @_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this1, %class.btDispatcher* %17, %class.btCollisionWorld* %18)
  %19 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.4* %m_collisionObjects
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !39
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %1 = load i32, i32* %n.addr, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !31
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %this, i32 %tag) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %tag.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store i32 %tag, i32* %tag.addr, align 4, !tbaa !19
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i32, i32* %tag.addr, align 4, !tbaa !19
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  store i32 %0, i32* %m_islandTag1, align 4, !tbaa !38
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %this, i32 %id) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %id.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store i32 %id, i32* %id.addr, align 4, !tbaa !19
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4, !tbaa !19
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  store i32 %0, i32* %m_companionId, align 4, !tbaa !41
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %this, float %hitFraction) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %hitFraction.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store float %hitFraction, float* %hitFraction.addr, align 4, !tbaa !42
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %hitFraction.addr, align 4, !tbaa !42
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 22
  store float %0, float* %m_hitFraction, align 4, !tbaa !43
  ret void
}

define hidden void @_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld(%class.btSimulationIslandManager* %this, %class.btCollisionWorld* %colWorld) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %colWorld.addr = alloca %class.btCollisionWorld*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %colWorld, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %index, align 4, !tbaa !19
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !19
  %3 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %3)
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %call)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %5)
  %6 = load i32, i32* %i, align 4, !tbaa !19
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %call3, i32 %6)
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %call4, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call5 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %8)
  br i1 %call5, label %if.else, label %if.then

if.then:                                          ; preds = %for.body
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %10 = load i32, i32* %index, align 4, !tbaa !19
  %call6 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %m_unionFind, i32 %10)
  call void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %9, i32 %call6)
  %11 = load i32, i32* %i, align 4, !tbaa !19
  %m_unionFind7 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %12 = load i32, i32* %index, align 4, !tbaa !19
  %call8 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %m_unionFind7, i32 %12)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call8, i32 0, i32 1
  store i32 %11, i32* %m_sz, align 4, !tbaa !37
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %13, i32 -1)
  %14 = load i32, i32* %index, align 4, !tbaa !19
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %index, align 4, !tbaa !19
  br label %if.end

if.else:                                          ; preds = %for.body
  %15 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  call void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %15, i32 -1)
  %16 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %16, i32 -2)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %i, align 4, !tbaa !19
  %inc9 = add nsw i32 %18, 1
  store i32 %inc9, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this, i32 %x) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %x.addr = alloca i32, align 4
  %elementPtr = alloca %struct.btElement*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %x, i32* %x.addr, align 4, !tbaa !19
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %x.addr, align 4, !tbaa !19
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %1 = load i32, i32* %x.addr, align 4, !tbaa !19
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %1)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  %2 = load i32, i32* %m_id, align 4, !tbaa !35
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = bitcast %struct.btElement** %elementPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %4 = load i32, i32* %x.addr, align 4, !tbaa !19
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements3, i32 %4)
  %m_id5 = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  %5 = load i32, i32* %m_id5, align 4, !tbaa !35
  %call6 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements2, i32 %5)
  store %struct.btElement* %call6, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %6 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %m_id7 = getelementptr inbounds %struct.btElement, %struct.btElement* %6, i32 0, i32 0
  %7 = load i32, i32* %m_id7, align 4, !tbaa !35
  %m_elements8 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %x.addr, align 4, !tbaa !19
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements8, i32 %8)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  store i32 %7, i32* %m_id10, align 4, !tbaa !35
  %9 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %m_id11 = getelementptr inbounds %struct.btElement, %struct.btElement* %9, i32 0, i32 0
  %10 = load i32, i32* %m_id11, align 4, !tbaa !35
  store i32 %10, i32* %x.addr, align 4, !tbaa !19
  %11 = bitcast %struct.btElement** %elementPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %12 = load i32, i32* %x.addr, align 4, !tbaa !19
  ret i32 %12
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %index.addr = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !19
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %0 = load i32, i32* %index.addr, align 4, !tbaa !19
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %0)
  ret %struct.btElement* %call
}

define hidden void @_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this, %class.btDispatcher* %dispatcher, %class.btCollisionWorld* %collisionWorld) #0 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %collisionObjects = alloca %class.btAlignedObjectArray.4*, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %numElem = alloca i32, align 4
  %endIslandIndex = alloca i32, align 4
  %startIslandIndex = alloca i32, align 4
  %islandId = alloca i32, align 4
  %allSleeping = alloca i8, align 1
  %idx = alloca i32, align 4
  %i = alloca i32, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %idx41 = alloca i32, align 4
  %i45 = alloca i32, align 4
  %colObj049 = alloca %class.btCollisionObject*, align 4
  %idx65 = alloca i32, align 4
  %i69 = alloca i32, align 4
  %colObj073 = alloca %class.btCollisionObject*, align 4
  %i96 = alloca i32, align 4
  %maxNumManifolds = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %colObj0104 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #8
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0))
  %1 = bitcast %class.btAlignedObjectArray.4** %collisionObjects to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %2)
  store %class.btAlignedObjectArray.4* %call2, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %3 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %m_islandmanifold, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %call3 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  call void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind* %call3)
  %5 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %call4 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %call5 = call i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %call4)
  store i32 %call5, i32* %numElem, align 4, !tbaa !19
  %6 = bitcast i32* %endIslandIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store i32 1, i32* %endIslandIndex, align 4, !tbaa !19
  %7 = bitcast i32* %startIslandIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store i32 0, i32* %startIslandIndex, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc94, %entry
  %8 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  %9 = load i32, i32* %numElem, align 4, !tbaa !19
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end95

for.body:                                         ; preds = %for.cond
  %10 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %call6 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %11 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call6, i32 %11)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 0
  %12 = load i32, i32* %m_id, align 4, !tbaa !35
  store i32 %12, i32* %islandId, align 4, !tbaa !19
  %13 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  %add = add nsw i32 %13, 1
  store i32 %add, i32* %endIslandIndex, align 4, !tbaa !19
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %15 = load i32, i32* %numElem, align 4, !tbaa !19
  %cmp9 = icmp slt i32 %14, %15
  br i1 %cmp9, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond8
  %call10 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %16 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %call11 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call10, i32 %16)
  %m_id12 = getelementptr inbounds %struct.btElement, %struct.btElement* %call11, i32 0, i32 0
  %17 = load i32, i32* %m_id12, align 4, !tbaa !35
  %18 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp13 = icmp eq i32 %17, %18
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond8
  %19 = phi i1 [ false, %for.cond8 ], [ %cmp13, %land.rhs ]
  br i1 %19, label %for.body14, label %for.end

for.body14:                                       ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %20 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %endIslandIndex, align 4, !tbaa !19
  br label %for.cond8

for.end:                                          ; preds = %land.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %allSleeping) #8
  store i8 1, i8* %allSleeping, align 1, !tbaa !44
  %21 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  store i32 %22, i32* %idx, align 4, !tbaa !19
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc37, %for.end
  %23 = load i32, i32* %idx, align 4, !tbaa !19
  %24 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %cmp16 = icmp slt i32 %23, %24
  br i1 %cmp16, label %for.body17, label %for.end39

for.body17:                                       ; preds = %for.cond15
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %call18 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %26 = load i32, i32* %idx, align 4, !tbaa !19
  %call19 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call18, i32 %26)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call19, i32 0, i32 1
  %27 = load i32, i32* %m_sz, align 4, !tbaa !37
  store i32 %27, i32* %i, align 4, !tbaa !19
  %28 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  %29 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !19
  %call20 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %29, i32 %30)
  %31 = load %class.btCollisionObject*, %class.btCollisionObject** %call20, align 4, !tbaa !2
  store %class.btCollisionObject* %31, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %32 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call21 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %32)
  %33 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp22 = icmp ne i32 %call21, %33
  br i1 %cmp22, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body17
  %34 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call23 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %34)
  %cmp24 = icmp ne i32 %call23, -1
  br i1 %cmp24, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body17
  %35 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call25 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %35)
  %36 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp26 = icmp eq i32 %call25, %36
  br i1 %cmp26, label %if.then27, label %if.end36

if.then27:                                        ; preds = %if.end
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call28 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %37)
  %cmp29 = icmp eq i32 %call28, 1
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then27
  store i8 0, i8* %allSleeping, align 1, !tbaa !44
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %if.then27
  %38 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call32 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %38)
  %cmp33 = icmp eq i32 %call32, 4
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end31
  store i8 0, i8* %allSleeping, align 1, !tbaa !44
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end31
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.end
  %39 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  br label %for.inc37

for.inc37:                                        ; preds = %if.end36
  %41 = load i32, i32* %idx, align 4, !tbaa !19
  %inc38 = add nsw i32 %41, 1
  store i32 %inc38, i32* %idx, align 4, !tbaa !19
  br label %for.cond15

for.end39:                                        ; preds = %for.cond15
  %42 = load i8, i8* %allSleeping, align 1, !tbaa !44, !range !45
  %tobool = trunc i8 %42 to i1
  br i1 %tobool, label %if.then40, label %if.else

if.then40:                                        ; preds = %for.end39
  %43 = bitcast i32* %idx41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %44 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  store i32 %44, i32* %idx41, align 4, !tbaa !19
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc62, %if.then40
  %45 = load i32, i32* %idx41, align 4, !tbaa !19
  %46 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %cmp43 = icmp slt i32 %45, %46
  br i1 %cmp43, label %for.body44, label %for.end64

for.body44:                                       ; preds = %for.cond42
  %47 = bitcast i32* %i45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #8
  %call46 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %48 = load i32, i32* %idx41, align 4, !tbaa !19
  %call47 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call46, i32 %48)
  %m_sz48 = getelementptr inbounds %struct.btElement, %struct.btElement* %call47, i32 0, i32 1
  %49 = load i32, i32* %m_sz48, align 4, !tbaa !37
  store i32 %49, i32* %i45, align 4, !tbaa !19
  %50 = bitcast %class.btCollisionObject** %colObj049 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  %51 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %52 = load i32, i32* %i45, align 4, !tbaa !19
  %call50 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %51, i32 %52)
  %53 = load %class.btCollisionObject*, %class.btCollisionObject** %call50, align 4, !tbaa !2
  store %class.btCollisionObject* %53, %class.btCollisionObject** %colObj049, align 4, !tbaa !2
  %54 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4, !tbaa !2
  %call51 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %54)
  %55 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp52 = icmp ne i32 %call51, %55
  br i1 %cmp52, label %land.lhs.true53, label %if.end57

land.lhs.true53:                                  ; preds = %for.body44
  %56 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4, !tbaa !2
  %call54 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %56)
  %cmp55 = icmp ne i32 %call54, -1
  br i1 %cmp55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %land.lhs.true53
  br label %if.end57

if.end57:                                         ; preds = %if.then56, %land.lhs.true53, %for.body44
  %57 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4, !tbaa !2
  %call58 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %57)
  %58 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp59 = icmp eq i32 %call58, %58
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end57
  %59 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4, !tbaa !2
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %59, i32 2)
  br label %if.end61

if.end61:                                         ; preds = %if.then60, %if.end57
  %60 = bitcast %class.btCollisionObject** %colObj049 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #8
  %61 = bitcast i32* %i45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #8
  br label %for.inc62

for.inc62:                                        ; preds = %if.end61
  %62 = load i32, i32* %idx41, align 4, !tbaa !19
  %inc63 = add nsw i32 %62, 1
  store i32 %inc63, i32* %idx41, align 4, !tbaa !19
  br label %for.cond42

for.end64:                                        ; preds = %for.cond42
  %63 = bitcast i32* %idx41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #8
  br label %if.end93

if.else:                                          ; preds = %for.end39
  %64 = bitcast i32* %idx65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  %65 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  store i32 %65, i32* %idx65, align 4, !tbaa !19
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc90, %if.else
  %66 = load i32, i32* %idx65, align 4, !tbaa !19
  %67 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %cmp67 = icmp slt i32 %66, %67
  br i1 %cmp67, label %for.body68, label %for.end92

for.body68:                                       ; preds = %for.cond66
  %68 = bitcast i32* %i69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #8
  %call70 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %69 = load i32, i32* %idx65, align 4, !tbaa !19
  %call71 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call70, i32 %69)
  %m_sz72 = getelementptr inbounds %struct.btElement, %struct.btElement* %call71, i32 0, i32 1
  %70 = load i32, i32* %m_sz72, align 4, !tbaa !37
  store i32 %70, i32* %i69, align 4, !tbaa !19
  %71 = bitcast %class.btCollisionObject** %colObj073 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #8
  %72 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %73 = load i32, i32* %i69, align 4, !tbaa !19
  %call74 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %72, i32 %73)
  %74 = load %class.btCollisionObject*, %class.btCollisionObject** %call74, align 4, !tbaa !2
  store %class.btCollisionObject* %74, %class.btCollisionObject** %colObj073, align 4, !tbaa !2
  %75 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4, !tbaa !2
  %call75 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %75)
  %76 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp76 = icmp ne i32 %call75, %76
  br i1 %cmp76, label %land.lhs.true77, label %if.end81

land.lhs.true77:                                  ; preds = %for.body68
  %77 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4, !tbaa !2
  %call78 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %77)
  %cmp79 = icmp ne i32 %call78, -1
  br i1 %cmp79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %land.lhs.true77
  br label %if.end81

if.end81:                                         ; preds = %if.then80, %land.lhs.true77, %for.body68
  %78 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4, !tbaa !2
  %call82 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %78)
  %79 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp83 = icmp eq i32 %call82, %79
  br i1 %cmp83, label %if.then84, label %if.end89

if.then84:                                        ; preds = %if.end81
  %80 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4, !tbaa !2
  %call85 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %80)
  %cmp86 = icmp eq i32 %call85, 2
  br i1 %cmp86, label %if.then87, label %if.end88

if.then87:                                        ; preds = %if.then84
  %81 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4, !tbaa !2
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %81, i32 3)
  %82 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4, !tbaa !2
  call void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %82, float 0.000000e+00)
  br label %if.end88

if.end88:                                         ; preds = %if.then87, %if.then84
  br label %if.end89

if.end89:                                         ; preds = %if.end88, %if.end81
  %83 = bitcast %class.btCollisionObject** %colObj073 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #8
  %84 = bitcast i32* %i69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #8
  br label %for.inc90

for.inc90:                                        ; preds = %if.end89
  %85 = load i32, i32* %idx65, align 4, !tbaa !19
  %inc91 = add nsw i32 %85, 1
  store i32 %inc91, i32* %idx65, align 4, !tbaa !19
  br label %for.cond66

for.end92:                                        ; preds = %for.cond66
  %86 = bitcast i32* %idx65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  br label %if.end93

if.end93:                                         ; preds = %for.end92, %for.end64
  %87 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %allSleeping) #8
  %88 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  br label %for.inc94

for.inc94:                                        ; preds = %if.end93
  %89 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  store i32 %89, i32* %startIslandIndex, align 4, !tbaa !19
  br label %for.cond

for.end95:                                        ; preds = %for.cond
  %90 = bitcast i32* %i96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #8
  %91 = bitcast i32* %maxNumManifolds to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #8
  %92 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %93 = bitcast %class.btDispatcher* %92 to i32 (%class.btDispatcher*)***
  %vtable = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %93, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable, i64 9
  %94 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn, align 4
  %call97 = call i32 %94(%class.btDispatcher* %92)
  store i32 %call97, i32* %maxNumManifolds, align 4, !tbaa !19
  store i32 0, i32* %i96, align 4, !tbaa !19
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc144, %for.end95
  %95 = load i32, i32* %i96, align 4, !tbaa !19
  %96 = load i32, i32* %maxNumManifolds, align 4, !tbaa !19
  %cmp99 = icmp slt i32 %95, %96
  br i1 %cmp99, label %for.body100, label %for.end146

for.body100:                                      ; preds = %for.cond98
  %97 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #8
  %98 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %99 = load i32, i32* %i96, align 4, !tbaa !19
  %100 = bitcast %class.btDispatcher* %98 to %class.btPersistentManifold* (%class.btDispatcher*, i32)***
  %vtable101 = load %class.btPersistentManifold* (%class.btDispatcher*, i32)**, %class.btPersistentManifold* (%class.btDispatcher*, i32)*** %100, align 4, !tbaa !6
  %vfn102 = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, i32)*, %class.btPersistentManifold* (%class.btDispatcher*, i32)** %vtable101, i64 10
  %101 = load %class.btPersistentManifold* (%class.btDispatcher*, i32)*, %class.btPersistentManifold* (%class.btDispatcher*, i32)** %vfn102, align 4
  %call103 = call %class.btPersistentManifold* %101(%class.btDispatcher* %98, i32 %99)
  store %class.btPersistentManifold* %call103, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %102 = bitcast %class.btCollisionObject** %colObj0104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #8
  %103 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %call105 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %103)
  store %class.btCollisionObject* %call105, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  %104 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #8
  %105 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %call106 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %105)
  store %class.btCollisionObject* %call106, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %106 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  %tobool107 = icmp ne %class.btCollisionObject* %106, null
  br i1 %tobool107, label %land.lhs.true108, label %lor.lhs.false

land.lhs.true108:                                 ; preds = %for.body100
  %107 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  %call109 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %107)
  %cmp110 = icmp ne i32 %call109, 2
  br i1 %cmp110, label %if.then115, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true108, %for.body100
  %108 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %tobool111 = icmp ne %class.btCollisionObject* %108, null
  br i1 %tobool111, label %land.lhs.true112, label %if.end143

land.lhs.true112:                                 ; preds = %lor.lhs.false
  %109 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call113 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %109)
  %cmp114 = icmp ne i32 %call113, 2
  br i1 %cmp114, label %if.then115, label %if.end143

if.then115:                                       ; preds = %land.lhs.true112, %land.lhs.true108
  %110 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  %call116 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %110)
  br i1 %call116, label %land.lhs.true117, label %if.end124

land.lhs.true117:                                 ; preds = %if.then115
  %111 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  %call118 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %111)
  %cmp119 = icmp ne i32 %call118, 2
  br i1 %cmp119, label %if.then120, label %if.end124

if.then120:                                       ; preds = %land.lhs.true117
  %112 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  %call121 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %112)
  br i1 %call121, label %if.then122, label %if.end123

if.then122:                                       ; preds = %if.then120
  %113 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %113, i1 zeroext false)
  br label %if.end123

if.end123:                                        ; preds = %if.then122, %if.then120
  br label %if.end124

if.end124:                                        ; preds = %if.end123, %land.lhs.true117, %if.then115
  %114 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call125 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %114)
  br i1 %call125, label %land.lhs.true126, label %if.end133

land.lhs.true126:                                 ; preds = %if.end124
  %115 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call127 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %115)
  %cmp128 = icmp ne i32 %call127, 2
  br i1 %cmp128, label %if.then129, label %if.end133

if.then129:                                       ; preds = %land.lhs.true126
  %116 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call130 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %116)
  br i1 %call130, label %if.then131, label %if.end132

if.then131:                                       ; preds = %if.then129
  %117 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %117, i1 zeroext false)
  br label %if.end132

if.end132:                                        ; preds = %if.then131, %if.then129
  br label %if.end133

if.end133:                                        ; preds = %if.end132, %land.lhs.true126, %if.end124
  %m_splitIslands = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 4
  %118 = load i8, i8* %m_splitIslands, align 4, !tbaa !8, !range !45
  %tobool134 = trunc i8 %118 to i1
  br i1 %tobool134, label %if.then135, label %if.end142

if.then135:                                       ; preds = %if.end133
  %119 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %120 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4, !tbaa !2
  %121 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %122 = bitcast %class.btDispatcher* %119 to i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable136 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %122, align 4, !tbaa !6
  %vfn137 = getelementptr inbounds i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable136, i64 7
  %123 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn137, align 4
  %call138 = call zeroext i1 %123(%class.btDispatcher* %119, %class.btCollisionObject* %120, %class.btCollisionObject* %121)
  br i1 %call138, label %if.then139, label %if.end141

if.then139:                                       ; preds = %if.then135
  %m_islandmanifold140 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.0* %m_islandmanifold140, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %manifold)
  br label %if.end141

if.end141:                                        ; preds = %if.then139, %if.then135
  br label %if.end142

if.end142:                                        ; preds = %if.end141, %if.end133
  br label %if.end143

if.end143:                                        ; preds = %if.end142, %land.lhs.true112, %lor.lhs.false
  %124 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #8
  %125 = bitcast %class.btCollisionObject** %colObj0104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #8
  %126 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  br label %for.inc144

for.inc144:                                       ; preds = %if.end143
  %127 = load i32, i32* %i96, align 4, !tbaa !19
  %inc145 = add nsw i32 %127, 1
  store i32 %inc145, i32* %i96, align 4, !tbaa !19
  br label %for.cond98

for.end146:                                       ; preds = %for.cond98
  %128 = bitcast i32* %maxNumManifolds to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #8
  %129 = bitcast i32* %i96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #8
  %130 = bitcast i32* %startIslandIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #8
  %131 = bitcast i32* %endIslandIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #8
  %132 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #8
  %133 = bitcast %class.btAlignedObjectArray.4** %collisionObjects to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #8
  %call147 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %134 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %134) #8
  ret void
}

define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* returned %this, i8* %name) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  %name.addr = alloca i8*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  store i8* %name, i8** %name.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  %0 = load i8*, i8** %name.addr, align 4, !tbaa !2
  call void @_ZN15CProfileManager13Start_ProfileEPKc(i8* %0)
  ret %class.CProfileSample* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !19
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !19
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %2 = load i32, i32* %curSize, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  store i32 %4, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %curSize, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !19
  store i32 %14, i32* %i6, align 4, !tbaa !19
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !19
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data11, align 4, !tbaa !46
  %19 = load i32, i32* %i6, align 4, !tbaa !19
  %arrayidx12 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %18, i32 %19
  %20 = bitcast %class.btPersistentManifold** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btPersistentManifold**
  %22 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btPersistentManifold*, %class.btPersistentManifold** %22, align 4, !tbaa !2
  store %class.btPersistentManifold* %23, %class.btPersistentManifold** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !19
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !19
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !47
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  ret %class.btUnionFind* %m_unionFind
}

declare void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind*) #1

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %m_elements)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %0 = load i32, i32* %m_activationState1, align 4, !tbaa !48
  ret i32 %0
}

declare void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject*, i32) #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %this, float %time) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %time.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store float %time, float* %time.addr, align 4, !tbaa !42
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %time.addr, align 4, !tbaa !42
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  store float %0, float* %m_deactivationTime, align 4, !tbaa !49
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !50
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4, !tbaa !52
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !31
  %and = and i32 %0, 2
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !31
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

declare void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject*, i1 zeroext) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.0* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !19
  %1 = load i32, i32* %sz, align 4, !tbaa !19
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !47
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btPersistentManifold**
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %6, align 4, !tbaa !2
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !47
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !47
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  call void @_ZN15CProfileManager12Stop_ProfileEv()
  ret %class.CProfileSample* %this1
}

define hidden void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager* %this, %class.btDispatcher* %dispatcher, %class.btCollisionWorld* %collisionWorld, %"struct.btSimulationIslandManager::IslandCallback"* %callback) #0 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %callback.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  %collisionObjects = alloca %class.btAlignedObjectArray.4*, align 4
  %endIslandIndex = alloca i32, align 4
  %startIslandIndex = alloca i32, align 4
  %numElem = alloca i32, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %manifold = alloca %class.btPersistentManifold**, align 4
  %maxNumManifolds = alloca i32, align 4
  %numManifolds = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifoldSortPredicate, align 1
  %startManifoldIndex = alloca i32, align 4
  %endManifoldIndex = alloca i32, align 4
  %islandId = alloca i32, align 4
  %islandSleeping = alloca i8, align 1
  %i = alloca i32, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %numIslandManifolds = alloca i32, align 4
  %startManifold = alloca %class.btPersistentManifold**, align 4
  %curIslandId = alloca i32, align 4
  %ref.tmp65 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  store %"struct.btSimulationIslandManager::IslandCallback"* %callback, %"struct.btSimulationIslandManager::IslandCallback"** %callback.addr, align 4, !tbaa !2
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast %class.btAlignedObjectArray.4** %collisionObjects to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %1)
  store %class.btAlignedObjectArray.4* %call, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %3 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  call void @_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this1, %class.btDispatcher* %2, %class.btCollisionWorld* %3)
  %4 = bitcast i32* %endIslandIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 1, i32* %endIslandIndex, align 4, !tbaa !19
  %5 = bitcast i32* %startIslandIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %call2 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %call3 = call i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %call2)
  store i32 %call3, i32* %numElem, align 4, !tbaa !19
  %7 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %7) #8
  %call4 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i32 0, i32 0))
  %m_splitIslands = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 4
  %8 = load i8, i8* %m_splitIslands, align 4, !tbaa !8, !range !45
  %tobool = trunc i8 %8 to i1
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %9 = bitcast %class.btPersistentManifold*** %manifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %11 = bitcast %class.btDispatcher* %10 to %class.btPersistentManifold** (%class.btDispatcher*)***
  %vtable = load %class.btPersistentManifold** (%class.btDispatcher*)**, %class.btPersistentManifold** (%class.btDispatcher*)*** %11, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btPersistentManifold** (%class.btDispatcher*)*, %class.btPersistentManifold** (%class.btDispatcher*)** %vtable, i64 11
  %12 = load %class.btPersistentManifold** (%class.btDispatcher*)*, %class.btPersistentManifold** (%class.btDispatcher*)** %vfn, align 4
  %call5 = call %class.btPersistentManifold** %12(%class.btDispatcher* %10)
  store %class.btPersistentManifold** %call5, %class.btPersistentManifold*** %manifold, align 4, !tbaa !2
  %13 = bitcast i32* %maxNumManifolds to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %15 = bitcast %class.btDispatcher* %14 to i32 (%class.btDispatcher*)***
  %vtable6 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %15, align 4, !tbaa !6
  %vfn7 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable6, i64 9
  %16 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn7, align 4
  %call8 = call i32 %16(%class.btDispatcher* %14)
  store i32 %call8, i32* %maxNumManifolds, align 4, !tbaa !19
  %17 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %callback.addr, align 4, !tbaa !2
  %18 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %18, i32 0)
  %19 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %19)
  %20 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold, align 4, !tbaa !2
  %21 = load i32, i32* %maxNumManifolds, align 4, !tbaa !19
  %22 = bitcast %"struct.btSimulationIslandManager::IslandCallback"* %17 to void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)***
  %vtable11 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)**, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*** %22, align 4, !tbaa !6
  %vfn12 = getelementptr inbounds void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vtable11, i64 2
  %23 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vfn12, align 4
  call void %23(%"struct.btSimulationIslandManager::IslandCallback"* %17, %class.btCollisionObject** %call9, i32 %call10, %class.btPersistentManifold** %20, i32 %21, i32 -1)
  %24 = bitcast i32* %maxNumManifolds to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast %class.btPersistentManifold*** %manifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  br label %if.end68

if.else:                                          ; preds = %entry
  %26 = bitcast i32* %numManifolds to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %m_islandmanifold)
  store i32 %call13, i32* %numManifolds, align 4, !tbaa !19
  %m_islandmanifold14 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %27 = bitcast %class.btPersistentManifoldSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %27) #8
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvRKT_(%class.btAlignedObjectArray.0* %m_islandmanifold14, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %28 = bitcast %class.btPersistentManifoldSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %28) #8
  %29 = bitcast i32* %startManifoldIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  store i32 0, i32* %startManifoldIndex, align 4, !tbaa !19
  %30 = bitcast i32* %endManifoldIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store i32 1, i32* %endManifoldIndex, align 4, !tbaa !19
  store i32 0, i32* %startIslandIndex, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc66, %if.else
  %31 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  %32 = load i32, i32* %numElem, align 4, !tbaa !19
  %cmp = icmp slt i32 %31, %32
  br i1 %cmp, label %for.body, label %for.end67

for.body:                                         ; preds = %for.cond
  %33 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %call15 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %34 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  %call16 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call15, i32 %34)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call16, i32 0, i32 0
  %35 = load i32, i32* %m_id, align 4, !tbaa !35
  store i32 %35, i32* %islandId, align 4, !tbaa !19
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %islandSleeping) #8
  store i8 1, i8* %islandSleeping, align 1, !tbaa !44
  %36 = load i32, i32* %startIslandIndex, align 4, !tbaa !19
  store i32 %36, i32* %endIslandIndex, align 4, !tbaa !19
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc, %for.body
  %37 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %38 = load i32, i32* %numElem, align 4, !tbaa !19
  %cmp18 = icmp slt i32 %37, %38
  br i1 %cmp18, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond17
  %call19 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %39 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %call20 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call19, i32 %39)
  %m_id21 = getelementptr inbounds %struct.btElement, %struct.btElement* %call20, i32 0, i32 0
  %40 = load i32, i32* %m_id21, align 4, !tbaa !35
  %41 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp22 = icmp eq i32 %40, %41
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond17
  %42 = phi i1 [ false, %for.cond17 ], [ %cmp22, %land.rhs ]
  br i1 %42, label %for.body23, label %for.end

for.body23:                                       ; preds = %land.end
  %43 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %call24 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %44 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %call25 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call24, i32 %44)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call25, i32 0, i32 1
  %45 = load i32, i32* %m_sz, align 4, !tbaa !37
  store i32 %45, i32* %i, align 4, !tbaa !19
  %46 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collisionObjects, align 4, !tbaa !2
  %48 = load i32, i32* %i, align 4, !tbaa !19
  %call26 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %47, i32 %48)
  %49 = load %class.btCollisionObject*, %class.btCollisionObject** %call26, align 4, !tbaa !2
  store %class.btCollisionObject* %49, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %m_islandBodies = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray.4* %m_islandBodies, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %colObj0)
  %50 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call27 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %50)
  br i1 %call27, label %if.then28, label %if.end

if.then28:                                        ; preds = %for.body23
  store i8 0, i8* %islandSleeping, align 1, !tbaa !44
  br label %if.end

if.end:                                           ; preds = %if.then28, %for.body23
  %51 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %53 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  %inc = add nsw i32 %53, 1
  store i32 %inc, i32* %endIslandIndex, align 4, !tbaa !19
  br label %for.cond17

for.end:                                          ; preds = %land.end
  %54 = bitcast i32* %numIslandManifolds to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #8
  store i32 0, i32* %numIslandManifolds, align 4, !tbaa !19
  %55 = bitcast %class.btPersistentManifold*** %startManifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #8
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %startManifold, align 4, !tbaa !2
  %56 = load i32, i32* %startManifoldIndex, align 4, !tbaa !19
  %57 = load i32, i32* %numManifolds, align 4, !tbaa !19
  %cmp29 = icmp slt i32 %56, %57
  br i1 %cmp29, label %if.then30, label %if.end51

if.then30:                                        ; preds = %for.end
  %58 = bitcast i32* %curIslandId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #8
  %m_islandmanifold31 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %59 = load i32, i32* %startManifoldIndex, align 4, !tbaa !19
  %call32 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %m_islandmanifold31, i32 %59)
  %60 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call32, align 4, !tbaa !2
  %call33 = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %60)
  store i32 %call33, i32* %curIslandId, align 4, !tbaa !19
  %61 = load i32, i32* %curIslandId, align 4, !tbaa !19
  %62 = load i32, i32* %islandId, align 4, !tbaa !19
  %cmp34 = icmp eq i32 %61, %62
  br i1 %cmp34, label %if.then35, label %if.end50

if.then35:                                        ; preds = %if.then30
  %m_islandmanifold36 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %63 = load i32, i32* %startManifoldIndex, align 4, !tbaa !19
  %call37 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %m_islandmanifold36, i32 %63)
  store %class.btPersistentManifold** %call37, %class.btPersistentManifold*** %startManifold, align 4, !tbaa !2
  %64 = load i32, i32* %startManifoldIndex, align 4, !tbaa !19
  %add = add nsw i32 %64, 1
  store i32 %add, i32* %endManifoldIndex, align 4, !tbaa !19
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc47, %if.then35
  %65 = load i32, i32* %endManifoldIndex, align 4, !tbaa !19
  %66 = load i32, i32* %numManifolds, align 4, !tbaa !19
  %cmp39 = icmp slt i32 %65, %66
  br i1 %cmp39, label %land.rhs40, label %land.end45

land.rhs40:                                       ; preds = %for.cond38
  %67 = load i32, i32* %islandId, align 4, !tbaa !19
  %m_islandmanifold41 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %68 = load i32, i32* %endManifoldIndex, align 4, !tbaa !19
  %call42 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %m_islandmanifold41, i32 %68)
  %69 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call42, align 4, !tbaa !2
  %call43 = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %69)
  %cmp44 = icmp eq i32 %67, %call43
  br label %land.end45

land.end45:                                       ; preds = %land.rhs40, %for.cond38
  %70 = phi i1 [ false, %for.cond38 ], [ %cmp44, %land.rhs40 ]
  br i1 %70, label %for.body46, label %for.end49

for.body46:                                       ; preds = %land.end45
  br label %for.inc47

for.inc47:                                        ; preds = %for.body46
  %71 = load i32, i32* %endManifoldIndex, align 4, !tbaa !19
  %inc48 = add nsw i32 %71, 1
  store i32 %inc48, i32* %endManifoldIndex, align 4, !tbaa !19
  br label %for.cond38

for.end49:                                        ; preds = %land.end45
  %72 = load i32, i32* %endManifoldIndex, align 4, !tbaa !19
  %73 = load i32, i32* %startManifoldIndex, align 4, !tbaa !19
  %sub = sub nsw i32 %72, %73
  store i32 %sub, i32* %numIslandManifolds, align 4, !tbaa !19
  br label %if.end50

if.end50:                                         ; preds = %for.end49, %if.then30
  %74 = bitcast i32* %curIslandId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %for.end
  %75 = load i8, i8* %islandSleeping, align 1, !tbaa !44, !range !45
  %tobool52 = trunc i8 %75 to i1
  br i1 %tobool52, label %if.end60, label %if.then53

if.then53:                                        ; preds = %if.end51
  %76 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %callback.addr, align 4, !tbaa !2
  %m_islandBodies54 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call55 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.4* %m_islandBodies54, i32 0)
  %m_islandBodies56 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call57 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %m_islandBodies56)
  %77 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %startManifold, align 4, !tbaa !2
  %78 = load i32, i32* %numIslandManifolds, align 4, !tbaa !19
  %79 = load i32, i32* %islandId, align 4, !tbaa !19
  %80 = bitcast %"struct.btSimulationIslandManager::IslandCallback"* %76 to void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)***
  %vtable58 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)**, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*** %80, align 4, !tbaa !6
  %vfn59 = getelementptr inbounds void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vtable58, i64 2
  %81 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vfn59, align 4
  call void %81(%"struct.btSimulationIslandManager::IslandCallback"* %76, %class.btCollisionObject** %call55, i32 %call57, %class.btPersistentManifold** %77, i32 %78, i32 %79)
  br label %if.end60

if.end60:                                         ; preds = %if.then53, %if.end51
  %82 = load i32, i32* %numIslandManifolds, align 4, !tbaa !19
  %tobool61 = icmp ne i32 %82, 0
  br i1 %tobool61, label %if.then62, label %if.end63

if.then62:                                        ; preds = %if.end60
  %83 = load i32, i32* %endManifoldIndex, align 4, !tbaa !19
  store i32 %83, i32* %startManifoldIndex, align 4, !tbaa !19
  br label %if.end63

if.end63:                                         ; preds = %if.then62, %if.end60
  %m_islandBodies64 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %84 = bitcast %class.btCollisionObject** %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #8
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp65, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %m_islandBodies64, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp65)
  %85 = bitcast %class.btCollisionObject** %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  %86 = bitcast %class.btPersistentManifold*** %startManifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast i32* %numIslandManifolds to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %islandSleeping) #8
  %88 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  br label %for.inc66

for.inc66:                                        ; preds = %if.end63
  %89 = load i32, i32* %endIslandIndex, align 4, !tbaa !19
  store i32 %89, i32* %startIslandIndex, align 4, !tbaa !19
  br label %for.cond

for.end67:                                        ; preds = %for.cond
  %90 = bitcast i32* %endManifoldIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %91 = bitcast i32* %startManifoldIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  %92 = bitcast i32* %numManifolds to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  br label %if.end68

if.end68:                                         ; preds = %for.end67, %if.then
  %call69 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %93 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %93) #8
  %94 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  %95 = bitcast i32* %startIslandIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #8
  %96 = bitcast i32* %endIslandIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #8
  %97 = bitcast %class.btAlignedObjectArray.4** %collisionObjects to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !47
  ret i32 %0
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvRKT_(%class.btAlignedObjectArray.0* %this, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.btPersistentManifoldSortPredicate*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifoldSortPredicate* %CompareFunc, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray.4* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !19
  %1 = load i32, i32* %sz, align 4, !tbaa !19
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %2, i32 %3
  %4 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btCollisionObject**
  %6 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %6, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !39
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !39
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %lhs) #6 comdat {
entry:
  %lhs.addr = alloca %class.btPersistentManifold*, align 4
  %islandId = alloca i32, align 4
  %rcolObj0 = alloca %class.btCollisionObject*, align 4
  %rcolObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btPersistentManifold* %lhs, %class.btPersistentManifold** %lhs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btCollisionObject** %rcolObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4, !tbaa !2
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %2)
  store %class.btCollisionObject* %call, %class.btCollisionObject** %rcolObj0, align 4, !tbaa !2
  %3 = bitcast %class.btCollisionObject** %rcolObj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4, !tbaa !2
  %call1 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %4)
  store %class.btCollisionObject* %call1, %class.btCollisionObject** %rcolObj1, align 4, !tbaa !2
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4, !tbaa !2
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  %cmp = icmp sge i32 %call2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4, !tbaa !2
  %call3 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %6)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj1, align 4, !tbaa !2
  %call4 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %7)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call3, %cond.true ], [ %call4, %cond.false ]
  store i32 %cond, i32* %islandId, align 4, !tbaa !19
  %8 = load i32, i32* %islandId, align 4, !tbaa !19
  %9 = bitcast %class.btCollisionObject** %rcolObj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btCollisionObject** %rcolObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast i32* %islandId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret i32 %8
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %1 = load i32, i32* %n.addr, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionObject**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !19
  store %class.btCollisionObject** %fillData, %class.btCollisionObject*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !19
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %2 = load i32, i32* %curSize, align 4, !tbaa !19
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  store i32 %4, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %6 = load i32, i32* %curSize, align 4, !tbaa !19
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !19
  store i32 %14, i32* %i6, align 4, !tbaa !19
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !19
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %18 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data11, align 4, !tbaa !40
  %19 = load i32, i32* %i6, align 4, !tbaa !19
  %arrayidx12 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %18, i32 %19
  %20 = bitcast %class.btCollisionObject** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btCollisionObject**
  %22 = load %class.btCollisionObject**, %class.btCollisionObject*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btCollisionObject*, %class.btCollisionObject** %22, align 4, !tbaa !2
  store %class.btCollisionObject* %23, %class.btCollisionObject** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !19
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !19
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !19
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !39
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !53
  %1 = load i32, i32* %n.addr, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

declare void @_ZN15CProfileManager13Start_ProfileEPKc(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !54
  ret i32 %0
}

declare void @_ZN15CProfileManager12Stop_ProfileEv() #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !55
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !47
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !56
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.4* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !57
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !39
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !58
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.0* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !19
  store i32 %last, i32* %last.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !19
  store i32 %1, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !19
  %3 = load i32, i32* %last.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !55, !range !45
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !46
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !46
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray.4* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !19
  store i32 %last, i32* %last.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !19
  store i32 %1, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !19
  %3 = load i32, i32* %last.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.4* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !57, !range !45
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4, !tbaa !40
  call void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4, !tbaa !40
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %this, %class.btCollisionObject** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !19
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !55
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !19
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !56
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !56
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !19
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !19
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !19
  store i32 %end, i32* %end.addr, align 4, !tbaa !19
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !19
  store i32 %1, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !19
  %3 = load i32, i32* %end.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !19
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !19
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !19
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !19
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.btPersistentManifoldSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %class.btPersistentManifold*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifoldSortPredicate* %CompareFunc, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !19
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !19
  store i32 %1, i32* %i, align 4, !tbaa !19
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !19
  store i32 %3, i32* %j, align 4, !tbaa !19
  %4 = bitcast %class.btPersistentManifold** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !19
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !19
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %div
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4, !tbaa !2
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %x, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %9 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %10 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data2, align 4, !tbaa !46
  %11 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx3 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %10, i32 %11
  %12 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx3, align 4, !tbaa !2
  %13 = load %class.btPersistentManifold*, %class.btPersistentManifold** %x, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_(%class.btPersistentManifoldSortPredicate* %9, %class.btPersistentManifold* %12, %class.btPersistentManifold* %13)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %15 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %16 = load %class.btPersistentManifold*, %class.btPersistentManifold** %x, align 4, !tbaa !2
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %17 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !46
  %18 = load i32, i32* %j, align 4, !tbaa !19
  %arrayidx6 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %17, i32 %18
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx6, align 4, !tbaa !2
  %call7 = call zeroext i1 @_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_(%class.btPersistentManifoldSortPredicate* %15, %class.btPersistentManifold* %16, %class.btPersistentManifold* %19)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %20 = load i32, i32* %j, align 4, !tbaa !19
  %dec = add nsw i32 %20, -1
  store i32 %dec, i32* %j, align 4, !tbaa !19
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %21 = load i32, i32* %i, align 4, !tbaa !19
  %22 = load i32, i32* %j, align 4, !tbaa !19
  %cmp = icmp sle i32 %21, %22
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %23 = load i32, i32* %i, align 4, !tbaa !19
  %24 = load i32, i32* %j, align 4, !tbaa !19
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray.0* %this1, i32 %23, i32 %24)
  %25 = load i32, i32* %i, align 4, !tbaa !19
  %inc10 = add nsw i32 %25, 1
  store i32 %inc10, i32* %i, align 4, !tbaa !19
  %26 = load i32, i32* %j, align 4, !tbaa !19
  %dec11 = add nsw i32 %26, -1
  store i32 %dec11, i32* %j, align 4, !tbaa !19
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4, !tbaa !19
  %28 = load i32, i32* %j, align 4, !tbaa !19
  %cmp12 = icmp sle i32 %27, %28
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %29 = load i32, i32* %lo.addr, align 4, !tbaa !19
  %30 = load i32, i32* %j, align 4, !tbaa !19
  %cmp13 = icmp slt i32 %29, %30
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %31 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %lo.addr, align 4, !tbaa !19
  %33 = load i32, i32* %j, align 4, !tbaa !19
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %34 = load i32, i32* %i, align 4, !tbaa !19
  %35 = load i32, i32* %hi.addr, align 4, !tbaa !19
  %cmp16 = icmp slt i32 %34, %35
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %36 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !19
  %38 = load i32, i32* %hi.addr, align 4, !tbaa !19
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %36, i32 %37, i32 %38)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  %39 = bitcast %class.btPersistentManifold** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_(%class.btPersistentManifoldSortPredicate* %this, %class.btPersistentManifold* %lhs, %class.btPersistentManifold* %rhs) #6 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifoldSortPredicate*, align 4
  %lhs.addr = alloca %class.btPersistentManifold*, align 4
  %rhs.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifoldSortPredicate* %this, %class.btPersistentManifoldSortPredicate** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %lhs, %class.btPersistentManifold** %lhs.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %rhs, %class.btPersistentManifold** %rhs.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4, !tbaa !2
  %call = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %0)
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %rhs.addr, align 4, !tbaa !2
  %call2 = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %1)
  %cmp = icmp slt i32 %call, %call2
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btPersistentManifold*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !19
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast %class.btPersistentManifold** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !46
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4, !tbaa !2
  store %class.btPersistentManifold* %3, %class.btPersistentManifold** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data2, align 4, !tbaa !46
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !19
  %arrayidx3 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !46
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !19
  %arrayidx5 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %arrayidx5, align 4, !tbaa !2
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %10 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data6, align 4, !tbaa !46
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !19
  %arrayidx7 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %10, i32 %11
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %class.btPersistentManifold** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.4* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !58
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btCollisionObject*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !19
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %3, %class.btCollisionObject*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btCollisionObject** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !57
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btCollisionObject** %5, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !19
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !58
  %7 = bitcast %class.btCollisionObject*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !19
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !19
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !19
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !19
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !19
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !19
  store i32 %end, i32* %end.addr, align 4, !tbaa !19
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !19
  store i32 %1, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !19
  %3 = load i32, i32* %end.addr, align 4, !tbaa !19
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %4, i32 %5
  %6 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !40
  %9 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %8, i32 %9
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4, !tbaa !2
  store %class.btCollisionObject* %10, %class.btCollisionObject** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btCollisionObject*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !19
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !19
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !14, i64 64}
!9 = !{!"_ZTS25btSimulationIslandManager", !10, i64 4, !15, i64 24, !17, i64 44, !14, i64 64}
!10 = !{!"_ZTS11btUnionFind", !11, i64 0}
!11 = !{!"_ZTS20btAlignedObjectArrayI9btElementE", !12, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !14, i64 16}
!12 = !{!"_ZTS18btAlignedAllocatorI9btElementLj16EE"}
!13 = !{!"int", !4, i64 0}
!14 = !{!"bool", !4, i64 0}
!15 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !16, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !14, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!17 = !{!"_ZTS20btAlignedObjectArrayIP17btCollisionObjectE", !18, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !14, i64 16}
!18 = !{!"_ZTS18btAlignedAllocatorIP17btCollisionObjectLj16EE"}
!19 = !{!13, !13, i64 0}
!20 = !{!21, !3, i64 0}
!21 = !{!"_ZTS16btBroadphasePair", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12}
!22 = !{!23, !3, i64 0}
!23 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !24, i64 4, !24, i64 6, !3, i64 8, !13, i64 12, !25, i64 16, !25, i64 32}
!24 = !{!"short", !4, i64 0}
!25 = !{!"_ZTS9btVector3", !4, i64 0}
!26 = !{!21, !3, i64 4}
!27 = !{!28, !3, i64 68}
!28 = !{!"_ZTS16btCollisionWorld", !17, i64 4, !3, i64 24, !29, i64 28, !3, i64 68, !3, i64 72, !14, i64 76}
!29 = !{!"_ZTS16btDispatcherInfo", !30, i64 0, !13, i64 4, !13, i64 8, !30, i64 12, !14, i64 16, !3, i64 20, !14, i64 24, !14, i64 25, !14, i64 26, !30, i64 28, !14, i64 32, !30, i64 36}
!30 = !{!"float", !4, i64 0}
!31 = !{!32, !13, i64 204}
!32 = !{!"_ZTS17btCollisionObject", !33, i64 4, !33, i64 68, !25, i64 132, !25, i64 148, !25, i64 164, !13, i64 180, !30, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !13, i64 204, !13, i64 208, !13, i64 212, !13, i64 216, !30, i64 220, !30, i64 224, !30, i64 228, !30, i64 232, !13, i64 236, !4, i64 240, !30, i64 244, !30, i64 248, !30, i64 252, !13, i64 256, !13, i64 260}
!33 = !{!"_ZTS11btTransform", !34, i64 0, !25, i64 48}
!34 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!35 = !{!36, !13, i64 0}
!36 = !{!"_ZTS9btElement", !13, i64 0, !13, i64 4}
!37 = !{!36, !13, i64 4}
!38 = !{!32, !13, i64 208}
!39 = !{!17, !13, i64 4}
!40 = !{!17, !3, i64 12}
!41 = !{!32, !13, i64 212}
!42 = !{!30, !30, i64 0}
!43 = !{!32, !30, i64 244}
!44 = !{!14, !14, i64 0}
!45 = !{i8 0, i8 2}
!46 = !{!15, !3, i64 12}
!47 = !{!15, !13, i64 4}
!48 = !{!32, !13, i64 216}
!49 = !{!32, !30, i64 220}
!50 = !{!51, !3, i64 740}
!51 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !13, i64 748, !30, i64 752, !30, i64 756, !13, i64 760, !13, i64 764, !13, i64 768}
!52 = !{!51, !3, i64 744}
!53 = !{!11, !3, i64 12}
!54 = !{!11, !13, i64 4}
!55 = !{!15, !14, i64 16}
!56 = !{!15, !13, i64 8}
!57 = !{!17, !14, i64 16}
!58 = !{!17, !13, i64 8}
