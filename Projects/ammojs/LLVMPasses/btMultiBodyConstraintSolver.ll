; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraintSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraintSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiBodyConstraintSolver = type { %class.btSequentialImpulseConstraintSolver, %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18, %struct.btMultiBodyJacobianData, %class.btMultiBodyConstraint**, i32 }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.14, i32, i32, i32 }
%class.btConstraintSolver = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.0, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.8, i32, i32, i32, i32 }
%union.anon.8 = type { i8* }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btAlignedObjectArray.18 = type <{ %class.btAlignedAllocator.19, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.19 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, %class.btVector3, %class.btVector3, i32, i32, %class.btVector3, %class.btVector3, i32, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.21, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32 }
%union.anon.21 = type { i8* }
%class.btMultiBody = type <{ %class.btMultiBodyLinkCollider*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.38, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i32, float, float, i8, [3 x i8], float, i8, [3 x i8] }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btAlignedObjectArray.22 = type <{ %class.btAlignedAllocator.23, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator.23 = type { i8 }
%struct.btMultibodyLink = type { float, float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i8, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btMultiBodyLinkCollider*, i32 }
%class.btAlignedObjectArray.26 = type <{ %class.btAlignedAllocator.27, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.27 = type { i8 }
%class.btAlignedObjectArray.30 = type <{ %class.btAlignedAllocator.31, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.31 = type { i8 }
%class.btAlignedObjectArray.34 = type <{ %class.btAlignedAllocator.35, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.35 = type { i8 }
%class.btAlignedObjectArray.38 = type <{ %class.btAlignedAllocator.39, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.39 = type { i8 }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray*, i32 }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, float, %class.btAlignedObjectArray.30 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.3, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%union.anon.3 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }
%class.btIDebugDraw = type opaque
%class.CProfileSample = type { i8 }
%class.btDispatcher = type opaque

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_ = comdat any

$_ZN27btMultiBodySolverConstraintC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN23btMultiBodyLinkCollider6upcastEP17btCollisionObject = comdat any

$_ZN11btMultiBody14setCompanionIdEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyEixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN12btSolverBody30internalGetDeltaLinearVelocityEv = comdat any

$_ZN12btSolverBody31internalGetDeltaAngularVelocityEv = comdat any

$_ZN11btMultiBody13applyDeltaVeeEPKff = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody18internalGetInvMassEv = comdat any

$_ZN14CProfileSampleC2EPKc = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnAEv = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnBEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody17getWorldTransformEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK11btMultiBody14getCompanionIdEv = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZNK11btRigidBody16getAngularFactorEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK15btManifoldPoint11getDistanceEv = comdat any

$_ZNK11btMultiBody17getVelocityVectorEv = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZNK11btRigidBody15getLinearFactorEv = comdat any

$_ZN14CProfileSampleD2Ev = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv = comdat any

$_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN20btPersistentManifold15getContactPointEi = comdat any

$_ZNK20btPersistentManifold29getContactProcessingThresholdEv = comdat any

$_ZN11btRigidBody6upcastEP17btCollisionObject = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZN27btMultiBodyConstraintSolverD2Ev = comdat any

$_ZN27btMultiBodyConstraintSolverD0Ev = comdat any

$_ZN18btConstraintSolver12prepareSolveEii = comdat any

$_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw = comdat any

$_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_Z6btSqrtf = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN23btMultiBodyJacobianDataD2Ev = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_ = comdat any

$_ZN27btMultiBodyConstraintSolverdlEPv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi = comdat any

$_ZN27btMultiBodySolverConstraintnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi = comdat any

@.str = private unnamed_addr constant [32 x i8] c"setupMultiBodyContactConstraint\00", align 1
@.str.1 = private unnamed_addr constant [31 x i8] c"addMultiBodyFrictionConstraint\00", align 1
@_ZTV27btMultiBodyConstraintSolver = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI27btMultiBodyConstraintSolver to i8*), i8* bitcast (%class.btMultiBodyConstraintSolver* (%class.btMultiBodyConstraintSolver*)* @_ZN27btMultiBodyConstraintSolverD2Ev to i8*), i8* bitcast (void (%class.btMultiBodyConstraintSolver*)* @_ZN27btMultiBodyConstraintSolverD0Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*, i32, i32)* @_ZN18btConstraintSolver12prepareSolveEii to i8*), i8* bitcast (float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN27btMultiBodyConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*), i8* bitcast (void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolver5resetEv to i8*), i8* bitcast (i32 (%class.btSequentialImpulseConstraintSolver*)* @_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv to i8*), i8* bitcast (void (%class.btMultiBodyConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)* @_ZN27btMultiBodyConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo to i8*), i8* bitcast (float (%class.btMultiBodyConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN27btMultiBodyConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN27btMultiBodyConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN27btMultiBodyConstraintSolver19solveMultiBodyGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiPP21btMultiBodyConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS27btMultiBodyConstraintSolver = hidden constant [30 x i8] c"27btMultiBodyConstraintSolver\00", align 1
@_ZTI35btSequentialImpulseConstraintSolver = external constant i8*
@_ZTI27btMultiBodyConstraintSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([30 x i8], [30 x i8]* @_ZTS27btMultiBodyConstraintSolver, i32 0, i32 0), i8* bitcast (i8** @_ZTI35btSequentialImpulseConstraintSolver to i8*) }, align 4

define hidden float @_ZN27btMultiBodyConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMultiBodyConstraintSolver* %this, i32 %iteration, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %iteration.addr = alloca i32, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %val = alloca float, align 4
  %j = alloca i32, align 4
  %constraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %j5 = alloca i32, align 4
  %constraint11 = alloca %struct.btMultiBodySolverConstraint*, align 4
  %j18 = alloca i32, align 4
  %frictionConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %totalImpulse = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store i32 %iteration, i32* %iteration.addr, align 4, !tbaa !6
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  store i32 %numBodies, i32* %numBodies.addr, align 4, !tbaa !6
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !6
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  store i32 %numConstraints, i32* %numConstraints.addr, align 4, !tbaa !6
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast float* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %2 = load i32, i32* %iteration.addr, align 4, !tbaa !6
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %4 = load i32, i32* %numBodies.addr, align 4, !tbaa !6
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  %6 = load i32, i32* %numManifolds.addr, align 4, !tbaa !6
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  %8 = load i32, i32* %numConstraints.addr, align 4, !tbaa !6
  %9 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %10 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %call = call float @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %1, i32 %2, %class.btCollisionObject** %3, i32 %4, %class.btPersistentManifold** %5, i32 %6, %class.btTypedConstraint** %7, i32 %8, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %9, %class.btIDebugDraw* %10)
  store float %call, float* %val, align 4, !tbaa !8
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %m_multiBodyNonContactConstraints)
  %cmp = icmp slt i32 %12, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = bitcast %struct.btMultiBodySolverConstraint** %constraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %m_multiBodyNonContactConstraints3 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.18* %m_multiBodyNonContactConstraints3, i32 %15)
  store %struct.btMultiBodySolverConstraint* %call4, %struct.btMultiBodySolverConstraint** %constraint, align 4, !tbaa !2
  %16 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint, align 4, !tbaa !2
  call void @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %16)
  %17 = bitcast %struct.btMultiBodySolverConstraint** %constraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %19 = bitcast i32* %j5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  store i32 0, i32* %j5, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc15, %for.end
  %20 = load i32, i32* %j5, align 4, !tbaa !6
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %m_multiBodyNormalContactConstraints)
  %cmp8 = icmp slt i32 %20, %call7
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond6
  %21 = bitcast i32* %j5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  br label %for.end17

for.body10:                                       ; preds = %for.cond6
  %22 = bitcast %struct.btMultiBodySolverConstraint** %constraint11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %m_multiBodyNormalContactConstraints12 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %23 = load i32, i32* %j5, align 4, !tbaa !6
  %call13 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.18* %m_multiBodyNormalContactConstraints12, i32 %23)
  store %struct.btMultiBodySolverConstraint* %call13, %struct.btMultiBodySolverConstraint** %constraint11, align 4, !tbaa !2
  %24 = load i32, i32* %iteration.addr, align 4, !tbaa !6
  %25 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %26 = bitcast %struct.btContactSolverInfo* %25 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %26, i32 0, i32 5
  %27 = load i32, i32* %m_numIterations, align 4, !tbaa !10
  %cmp14 = icmp slt i32 %24, %27
  br i1 %cmp14, label %if.then, label %if.end

if.then:                                          ; preds = %for.body10
  %28 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint11, align 4, !tbaa !2
  call void @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %28)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body10
  %29 = bitcast %struct.btMultiBodySolverConstraint** %constraint11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  br label %for.inc15

for.inc15:                                        ; preds = %if.end
  %30 = load i32, i32* %j5, align 4, !tbaa !6
  %inc16 = add nsw i32 %30, 1
  store i32 %inc16, i32* %j5, align 4, !tbaa !6
  br label %for.cond6

for.end17:                                        ; preds = %for.cond.cleanup9
  %31 = bitcast i32* %j18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store i32 0, i32* %j18, align 4, !tbaa !6
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc37, %for.end17
  %32 = load i32, i32* %j18, align 4, !tbaa !6
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call20 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %m_multiBodyFrictionContactConstraints)
  %cmp21 = icmp slt i32 %32, %call20
  br i1 %cmp21, label %for.body23, label %for.cond.cleanup22

for.cond.cleanup22:                               ; preds = %for.cond19
  %33 = bitcast i32* %j18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  br label %for.end39

for.body23:                                       ; preds = %for.cond19
  %34 = load i32, i32* %iteration.addr, align 4, !tbaa !6
  %35 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %36 = bitcast %struct.btContactSolverInfo* %35 to %struct.btContactSolverInfoData*
  %m_numIterations24 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %36, i32 0, i32 5
  %37 = load i32, i32* %m_numIterations24, align 4, !tbaa !10
  %cmp25 = icmp slt i32 %34, %37
  br i1 %cmp25, label %if.then26, label %if.end36

if.then26:                                        ; preds = %for.body23
  %38 = bitcast %struct.btMultiBodySolverConstraint** %frictionConstraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  %m_multiBodyFrictionContactConstraints27 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %39 = load i32, i32* %j18, align 4, !tbaa !6
  %call28 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.18* %m_multiBodyFrictionContactConstraints27, i32 %39)
  store %struct.btMultiBodySolverConstraint* %call28, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4, !tbaa !2
  %40 = bitcast float* %totalImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #9
  %m_multiBodyNormalContactConstraints29 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %41 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4, !tbaa !2
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %41, i32 0, i32 21
  %42 = load i32, i32* %m_frictionIndex, align 4, !tbaa !12
  %call30 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.18* %m_multiBodyNormalContactConstraints29, i32 %42)
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %call30, i32 0, i32 11
  %43 = load float, float* %m_appliedImpulse, align 4, !tbaa !15
  store float %43, float* %totalImpulse, align 4, !tbaa !8
  %44 = load float, float* %totalImpulse, align 4, !tbaa !8
  %cmp31 = fcmp ogt float %44, 0.000000e+00
  br i1 %cmp31, label %if.then32, label %if.end35

if.then32:                                        ; preds = %if.then26
  %45 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4, !tbaa !2
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %45, i32 0, i32 12
  %46 = load float, float* %m_friction, align 4, !tbaa !16
  %47 = load float, float* %totalImpulse, align 4, !tbaa !8
  %mul = fmul float %46, %47
  %fneg = fneg float %mul
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 16
  store float %fneg, float* %m_lowerLimit, align 4, !tbaa !17
  %49 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4, !tbaa !2
  %m_friction33 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %49, i32 0, i32 12
  %50 = load float, float* %m_friction33, align 4, !tbaa !16
  %51 = load float, float* %totalImpulse, align 4, !tbaa !8
  %mul34 = fmul float %50, %51
  %52 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %52, i32 0, i32 17
  store float %mul34, float* %m_upperLimit, align 4, !tbaa !18
  %53 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4, !tbaa !2
  call void @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %53)
  br label %if.end35

if.end35:                                         ; preds = %if.then32, %if.then26
  %54 = bitcast float* %totalImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  %55 = bitcast %struct.btMultiBodySolverConstraint** %frictionConstraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #9
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %for.body23
  br label %for.inc37

for.inc37:                                        ; preds = %if.end36
  %56 = load i32, i32* %j18, align 4, !tbaa !6
  %inc38 = add nsw i32 %56, 1
  store i32 %inc38, i32* %j18, align 4, !tbaa !6
  br label %for.cond19

for.end39:                                        ; preds = %for.cond.cleanup22
  %57 = load float, float* %val, align 4, !tbaa !8
  %58 = bitcast float* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  ret float %57
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare float @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !19
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.18* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 %1
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

define hidden void @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %c) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %c.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %deltaImpulse = alloca float, align 4
  %deltaVelADotn = alloca float, align 4
  %deltaVelBDotn = alloca float, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %ndofA = alloca i32, align 4
  %ndofB = alloca i32, align 4
  %i = alloca i32, align 4
  %i22 = alloca i32, align 4
  %sum = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp106 = alloca %class.btVector3, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %c, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast float* %deltaImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %1, i32 0, i32 14
  %2 = load float, float* %m_rhs, align 4, !tbaa !24
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 0, i32 11
  %4 = load float, float* %m_appliedImpulse, align 4, !tbaa !15
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %5, i32 0, i32 15
  %6 = load float, float* %m_cfm, align 4, !tbaa !25
  %mul = fmul float %4, %6
  %sub = fsub float %2, %mul
  store float %sub, float* %deltaImpulse, align 4, !tbaa !8
  %7 = bitcast float* %deltaVelADotn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %deltaVelADotn, align 4, !tbaa !8
  %8 = bitcast float* %deltaVelBDotn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0.000000e+00, float* %deltaVelBDotn, align 4, !tbaa !8
  %9 = bitcast %struct.btSolverBody** %bodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store %struct.btSolverBody* null, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %10 = bitcast %struct.btSolverBody** %bodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store %struct.btSolverBody* null, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %11 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store i32 0, i32* %ndofA, align 4, !tbaa !6
  %12 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  store i32 0, i32* %ndofB, align 4, !tbaa !6
  %13 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %13, i32 0, i32 23
  %14 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !26
  %tobool = icmp ne %class.btMultiBody* %14, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %15 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %15, i32 0, i32 23
  %16 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA2, align 4, !tbaa !26
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %16)
  %add = add nsw i32 %call, 6
  store i32 %add, i32* %ndofA, align 4, !tbaa !6
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %ndofA, align 4, !tbaa !6
  %cmp = icmp slt i32 %18, %19
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 0
  %21 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %21, i32 0, i32 3
  %22 = load i32, i32* %m_jacAindex, align 4, !tbaa !27
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %add3 = add nsw i32 %22, %23
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians, i32 %add3)
  %24 = load float, float* %call4, align 4, !tbaa !8
  %m_data5 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data5, i32 0, i32 2
  %25 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %25, i32 0, i32 0
  %26 = load i32, i32* %m_deltaVelAindex, align 4, !tbaa !28
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %add6 = add nsw i32 %26, %27
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocities, i32 %add6)
  %28 = load float, float* %call7, align 4, !tbaa !8
  %mul8 = fmul float %24, %28
  %29 = load float, float* %deltaVelADotn, align 4, !tbaa !8
  %add9 = fadd float %29, %mul8
  store float %add9, float* %deltaVelADotn, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end

if.else:                                          ; preds = %entry
  %31 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %31, i32 0, i32 1
  %32 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %32, i32 0, i32 22
  %33 = load i32, i32* %m_solverBodyIdA, align 4, !tbaa !29
  %call10 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %33)
  store %struct.btSolverBody* %call10, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %34 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %34, i32 0, i32 2
  %35 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %35)
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call11)
  %36 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %36, i32 0, i32 1
  %37 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %37)
  %call14 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call13)
  %add15 = fadd float %call12, %call14
  %38 = load float, float* %deltaVelADotn, align 4, !tbaa !8
  %add16 = fadd float %38, %add15
  store float %add16, float* %deltaVelADotn, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  %39 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %39, i32 0, i32 26
  %40 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !30
  %tobool17 = icmp ne %class.btMultiBody* %40, null
  br i1 %tobool17, label %if.then18, label %if.else40

if.then18:                                        ; preds = %if.end
  %41 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB19 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %41, i32 0, i32 26
  %42 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB19, align 4, !tbaa !30
  %call20 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %42)
  %add21 = add nsw i32 %call20, 6
  store i32 %add21, i32* %ndofB, align 4, !tbaa !6
  %43 = bitcast i32* %i22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  store i32 0, i32* %i22, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc37, %if.then18
  %44 = load i32, i32* %i22, align 4, !tbaa !6
  %45 = load i32, i32* %ndofB, align 4, !tbaa !6
  %cmp24 = icmp slt i32 %44, %45
  br i1 %cmp24, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond23
  %46 = bitcast i32* %i22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #9
  br label %for.end39

for.body26:                                       ; preds = %for.cond23
  %m_data27 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians28 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data27, i32 0, i32 0
  %47 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %47, i32 0, i32 7
  %48 = load i32, i32* %m_jacBindex, align 4, !tbaa !31
  %49 = load i32, i32* %i22, align 4, !tbaa !6
  %add29 = add nsw i32 %48, %49
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians28, i32 %add29)
  %50 = load float, float* %call30, align 4, !tbaa !8
  %m_data31 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities32 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data31, i32 0, i32 2
  %51 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %51, i32 0, i32 4
  %52 = load i32, i32* %m_deltaVelBindex, align 4, !tbaa !32
  %53 = load i32, i32* %i22, align 4, !tbaa !6
  %add33 = add nsw i32 %52, %53
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocities32, i32 %add33)
  %54 = load float, float* %call34, align 4, !tbaa !8
  %mul35 = fmul float %50, %54
  %55 = load float, float* %deltaVelBDotn, align 4, !tbaa !8
  %add36 = fadd float %55, %mul35
  store float %add36, float* %deltaVelBDotn, align 4, !tbaa !8
  br label %for.inc37

for.inc37:                                        ; preds = %for.body26
  %56 = load i32, i32* %i22, align 4, !tbaa !6
  %inc38 = add nsw i32 %56, 1
  store i32 %inc38, i32* %i22, align 4, !tbaa !6
  br label %for.cond23

for.end39:                                        ; preds = %for.cond.cleanup25
  br label %if.end49

if.else40:                                        ; preds = %if.end
  %57 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool41 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %57, i32 0, i32 1
  %58 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %58, i32 0, i32 25
  %59 = load i32, i32* %m_solverBodyIdB, align 4, !tbaa !33
  %call42 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool41, i32 %59)
  store %struct.btSolverBody* %call42, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %60 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %60, i32 0, i32 6
  %61 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %61)
  %call44 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call43)
  %62 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %62, i32 0, i32 5
  %63 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %63)
  %call46 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call45)
  %add47 = fadd float %call44, %call46
  %64 = load float, float* %deltaVelBDotn, align 4, !tbaa !8
  %add48 = fadd float %64, %add47
  store float %add48, float* %deltaVelBDotn, align 4, !tbaa !8
  br label %if.end49

if.end49:                                         ; preds = %if.else40, %for.end39
  %65 = load float, float* %deltaVelADotn, align 4, !tbaa !8
  %66 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %66, i32 0, i32 13
  %67 = load float, float* %m_jacDiagABInv, align 4, !tbaa !34
  %mul50 = fmul float %65, %67
  %68 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %sub51 = fsub float %68, %mul50
  store float %sub51, float* %deltaImpulse, align 4, !tbaa !8
  %69 = load float, float* %deltaVelBDotn, align 4, !tbaa !8
  %70 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacDiagABInv52 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %70, i32 0, i32 13
  %71 = load float, float* %m_jacDiagABInv52, align 4, !tbaa !34
  %mul53 = fmul float %69, %71
  %72 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %sub54 = fsub float %72, %mul53
  store float %sub54, float* %deltaImpulse, align 4, !tbaa !8
  %73 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  %74 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse55 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %74, i32 0, i32 11
  %75 = load float, float* %m_appliedImpulse55, align 4, !tbaa !15
  %76 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %add56 = fadd float %75, %76
  store float %add56, float* %sum, align 4, !tbaa !8
  %77 = load float, float* %sum, align 4, !tbaa !8
  %78 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %78, i32 0, i32 16
  %79 = load float, float* %m_lowerLimit, align 4, !tbaa !17
  %cmp57 = fcmp olt float %77, %79
  br i1 %cmp57, label %if.then58, label %if.else64

if.then58:                                        ; preds = %if.end49
  %80 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_lowerLimit59 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %80, i32 0, i32 16
  %81 = load float, float* %m_lowerLimit59, align 4, !tbaa !17
  %82 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse60 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %82, i32 0, i32 11
  %83 = load float, float* %m_appliedImpulse60, align 4, !tbaa !15
  %sub61 = fsub float %81, %83
  store float %sub61, float* %deltaImpulse, align 4, !tbaa !8
  %84 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_lowerLimit62 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %84, i32 0, i32 16
  %85 = load float, float* %m_lowerLimit62, align 4, !tbaa !17
  %86 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse63 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %86, i32 0, i32 11
  store float %85, float* %m_appliedImpulse63, align 4, !tbaa !15
  br label %if.end75

if.else64:                                        ; preds = %if.end49
  %87 = load float, float* %sum, align 4, !tbaa !8
  %88 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %88, i32 0, i32 17
  %89 = load float, float* %m_upperLimit, align 4, !tbaa !18
  %cmp65 = fcmp ogt float %87, %89
  br i1 %cmp65, label %if.then66, label %if.else72

if.then66:                                        ; preds = %if.else64
  %90 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_upperLimit67 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %90, i32 0, i32 17
  %91 = load float, float* %m_upperLimit67, align 4, !tbaa !18
  %92 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse68 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %92, i32 0, i32 11
  %93 = load float, float* %m_appliedImpulse68, align 4, !tbaa !15
  %sub69 = fsub float %91, %93
  store float %sub69, float* %deltaImpulse, align 4, !tbaa !8
  %94 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_upperLimit70 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %94, i32 0, i32 17
  %95 = load float, float* %m_upperLimit70, align 4, !tbaa !18
  %96 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse71 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %96, i32 0, i32 11
  store float %95, float* %m_appliedImpulse71, align 4, !tbaa !15
  br label %if.end74

if.else72:                                        ; preds = %if.else64
  %97 = load float, float* %sum, align 4, !tbaa !8
  %98 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse73 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %98, i32 0, i32 11
  store float %97, float* %m_appliedImpulse73, align 4, !tbaa !15
  br label %if.end74

if.end74:                                         ; preds = %if.else72, %if.then66
  br label %if.end75

if.end75:                                         ; preds = %if.end74, %if.then58
  %99 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA76 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %99, i32 0, i32 23
  %100 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA76, align 4, !tbaa !26
  %tobool77 = icmp ne %class.btMultiBody* %100, null
  br i1 %tobool77, label %if.then78, label %if.else88

if.then78:                                        ; preds = %if.end75
  %m_data79 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data79, i32 0, i32 1
  %101 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacAindex80 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %101, i32 0, i32 3
  %102 = load i32, i32* %m_jacAindex80, align 4, !tbaa !27
  %call81 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse, i32 %102)
  %103 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %104 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelAindex82 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %104, i32 0, i32 0
  %105 = load i32, i32* %m_deltaVelAindex82, align 4, !tbaa !28
  %106 = load i32, i32* %ndofA, align 4, !tbaa !6
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %call81, float %103, i32 %105, i32 %106)
  %107 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA83 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %107, i32 0, i32 23
  %108 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA83, align 4, !tbaa !26
  %m_data84 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse85 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data84, i32 0, i32 1
  %109 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacAindex86 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %109, i32 0, i32 3
  %110 = load i32, i32* %m_jacAindex86, align 4, !tbaa !27
  %call87 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse85, i32 %110)
  %111 = load float, float* %deltaImpulse, align 4, !tbaa !8
  call void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %108, float* %call87, float %111)
  br label %if.end91

if.else88:                                        ; preds = %if.end75
  %112 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %113 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %113) #9
  %114 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_contactNormal189 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %114, i32 0, i32 2
  %115 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %call90 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %115)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal189, %class.btVector3* nonnull align 4 dereferenceable(16) %call90)
  %116 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %116, i32 0, i32 8
  %117 = load float, float* %deltaImpulse, align 4, !tbaa !8
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %112, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %117)
  %118 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %118) #9
  br label %if.end91

if.end91:                                         ; preds = %if.else88, %if.then78
  %119 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB92 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %119, i32 0, i32 26
  %120 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB92, align 4, !tbaa !30
  %tobool93 = icmp ne %class.btMultiBody* %120, null
  br i1 %tobool93, label %if.then94, label %if.else105

if.then94:                                        ; preds = %if.end91
  %m_data95 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse96 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data95, i32 0, i32 1
  %121 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacBindex97 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %121, i32 0, i32 7
  %122 = load i32, i32* %m_jacBindex97, align 4, !tbaa !31
  %call98 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse96, i32 %122)
  %123 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %124 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelBindex99 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %124, i32 0, i32 4
  %125 = load i32, i32* %m_deltaVelBindex99, align 4, !tbaa !32
  %126 = load i32, i32* %ndofB, align 4, !tbaa !6
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %call98, float %123, i32 %125, i32 %126)
  %127 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB100 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %127, i32 0, i32 26
  %128 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB100, align 4, !tbaa !30
  %m_data101 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse102 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data101, i32 0, i32 1
  %129 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacBindex103 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %129, i32 0, i32 7
  %130 = load i32, i32* %m_jacBindex103, align 4, !tbaa !31
  %call104 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse102, i32 %130)
  %131 = load float, float* %deltaImpulse, align 4, !tbaa !8
  call void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %128, float* %call104, float %131)
  br label %if.end109

if.else105:                                       ; preds = %if.end91
  %132 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %133 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %133) #9
  %134 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_contactNormal2107 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %134, i32 0, i32 6
  %135 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %call108 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %135)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp106, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2107, %class.btVector3* nonnull align 4 dereferenceable(16) %call108)
  %136 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %136, i32 0, i32 9
  %137 = load float, float* %deltaImpulse, align 4, !tbaa !8
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %132, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp106, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %137)
  %138 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %138) #9
  br label %if.end109

if.end109:                                        ; preds = %if.else105, %if.then94
  %139 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #9
  %140 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #9
  %141 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #9
  %142 = bitcast %struct.btSolverBody** %bodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #9
  %143 = bitcast %struct.btSolverBody** %bodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #9
  %144 = bitcast float* %deltaVelBDotn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #9
  %145 = bitcast float* %deltaVelADotn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #9
  %146 = bitcast float* %deltaImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

define hidden float @_ZN27btMultiBodyConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMultiBodyConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %ref.tmp = alloca %struct.btMultiBodySolverConstraint, align 4
  %ref.tmp2 = alloca %struct.btMultiBodySolverConstraint, align 4
  %ref.tmp4 = alloca %struct.btMultiBodySolverConstraint, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %i = alloca i32, align 4
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %val = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  store i32 %numBodies, i32* %numBodies.addr, align 4, !tbaa !6
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !6
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  store i32 %numConstraints, i32* %numConstraints.addr, align 4, !tbaa !6
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %0 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 184, i8* %0) #9
  %1 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 184, i1 false)
  %call = call %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.18* %m_multiBodyNonContactConstraints, i32 0, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %ref.tmp)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 184, i8* %2) #9
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %3 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 184, i8* %3) #9
  %4 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %4, i8 0, i32 184, i1 false)
  %call3 = call %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* %ref.tmp2)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.18* %m_multiBodyNormalContactConstraints, i32 0, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %ref.tmp2)
  %5 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 184, i8* %5) #9
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %6 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 184, i8* %6) #9
  %7 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp4 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %7, i8 0, i32 184, i1 false)
  %call5 = call %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* %ref.tmp4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.18* %m_multiBodyFrictionContactConstraints, i32 0, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %ref.tmp4)
  %8 = bitcast %struct.btMultiBodySolverConstraint* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 184, i8* %8) #9
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 0
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_jacobians, i32 0, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %m_data7 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data7, i32 0, i32 1
  %11 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse, i32 0, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %12 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %m_data9 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data9, i32 0, i32 2
  %13 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_deltaVelocities, i32 0, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %14 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %numBodies.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %19 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %20, i32 %21
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4, !tbaa !2
  %call11 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEP17btCollisionObject(%class.btCollisionObject* %22)
  store %class.btMultiBodyLinkCollider* %call11, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %23 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %23, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %24 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %m_multiBody = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %24, i32 0, i32 1
  %25 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody, align 4, !tbaa !35
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %25, i32 -1)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %26 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = bitcast float* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  %29 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %30 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %31 = load i32, i32* %numBodies.addr, align 4, !tbaa !6
  %32 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  %33 = load i32, i32* %numManifolds.addr, align 4, !tbaa !6
  %34 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  %35 = load i32, i32* %numConstraints.addr, align 4, !tbaa !6
  %36 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %37 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %call12 = call float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %29, %class.btCollisionObject** %30, i32 %31, %class.btPersistentManifold** %32, i32 %33, %class.btTypedConstraint** %34, i32 %35, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %36, %class.btIDebugDraw* %37)
  store float %call12, float* %val, align 4, !tbaa !8
  %38 = load float, float* %val, align 4, !tbaa !8
  %39 = bitcast float* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  ret float %38
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.18* %this, i32 %newsize, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint* %fillData, %struct.btMultiBodySolverConstraint** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.18* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %18 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data11, align 4, !tbaa !23
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %18, i32 %19
  %20 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx12 to i8*
  %call13 = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 184, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btMultiBodySolverConstraint*
  %22 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btMultiBodySolverConstraint* %21 to i8*
  %24 = bitcast %struct.btMultiBodySolverConstraint* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 184, i1 false), !tbaa.struct !37
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !6
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !19
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %struct.btMultiBodySolverConstraint* %this, %struct.btMultiBodySolverConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %this.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relpos1CrossNormal)
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormal1)
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 5
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relpos2CrossNormal)
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 6
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormal2)
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 8
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularComponentA)
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 9
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularComponentB)
  ret %struct.btMultiBodySolverConstraint* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store float* %fillData, float** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !39
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.30* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %18 = load float*, float** %m_data11, align 4, !tbaa !39
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 %19
  %20 = bitcast float* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float*
  %22 = load float*, float** %fillData.addr, align 4, !tbaa !2
  %23 = load float, float* %22, align 4, !tbaa !8
  store float %23, float* %21, align 4, !tbaa !8
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !42
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  ret void
}

define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #0 comdat {
entry:
  %retval = alloca %class.btMultiBodyLinkCollider*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 64
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionObject* %1 to %class.btMultiBodyLinkCollider*
  store %class.btMultiBodyLinkCollider* %2, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btMultiBodyLinkCollider* null, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %retval, align 4
  ret %class.btMultiBodyLinkCollider* %3
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %this, i32 %id) #6 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %id.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %id, i32* %id.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4, !tbaa !6
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 21
  store i32 %0, i32* %m_companionId, align 4, !tbaa !43
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #2

define hidden void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this, float* %delta_vee, float %impulse, i32 %velocityIndex, i32 %ndof) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %delta_vee.addr = alloca float*, align 4
  %impulse.addr = alloca float, align 4
  %velocityIndex.addr = alloca i32, align 4
  %ndof.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store float* %delta_vee, float** %delta_vee.addr, align 4, !tbaa !2
  store float %impulse, float* %impulse.addr, align 4, !tbaa !8
  store i32 %velocityIndex, i32* %velocityIndex.addr, align 4, !tbaa !6
  store i32 %ndof, i32* %ndof.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %ndof.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4, !tbaa !8
  %7 = load float, float* %impulse.addr, align 4, !tbaa !8
  %mul = fmul float %6, %7
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 2
  %8 = load i32, i32* %velocityIndex.addr, align 4, !tbaa !6
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %8, %9
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocities, i32 %add)
  %10 = load float, float* %call, align 4, !tbaa !8
  %add2 = fadd float %10, %mul
  store float %add2, float* %call, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !39
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.22* %links)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4, !tbaa !55
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %this) #6 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  ret %class.btVector3* %m_deltaLinearVelocity
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %this) #6 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_deltaAngularVelocity
}

define linkonce_odr hidden void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %this, float* %delta_vee, float %multiplier) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %delta_vee.addr = alloca float*, align 4
  %multiplier.addr = alloca float, align 4
  %sum = alloca float, align 4
  %i = alloca i32, align 4
  %l = alloca float, align 4
  %i10 = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store float* %delta_vee, float** %delta_vee.addr, align 4, !tbaa !2
  store float %multiplier, float* %multiplier.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %sum, align 4, !tbaa !8
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  %add = add nsw i32 6, %call
  %cmp = icmp slt i32 %2, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4, !tbaa !8
  %7 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul = fmul float %6, %7
  %8 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul3 = fmul float %mul, %10
  %11 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul4 = fmul float %mul3, %11
  %12 = load float, float* %sum, align 4, !tbaa !8
  %add5 = fadd float %12, %mul4
  store float %add5, float* %sum, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load float, float* %sum, align 4, !tbaa !8
  %call6 = call float @_Z6btSqrtf(float %15)
  store float %call6, float* %l, align 4, !tbaa !8
  %16 = load float, float* %l, align 4, !tbaa !8
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 26
  %17 = load float, float* %m_maxAppliedImpulse, align 4, !tbaa !58
  %cmp7 = fcmp ogt float %16, %17
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %m_maxAppliedImpulse8 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 26
  %18 = load float, float* %m_maxAppliedImpulse8, align 4, !tbaa !58
  %19 = load float, float* %l, align 4, !tbaa !8
  %div = fdiv float %18, %19
  %20 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul9 = fmul float %20, %div
  store float %mul9, float* %multiplier.addr, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %21 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  store i32 0, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc27, %if.end
  %22 = load i32, i32* %i10, align 4, !tbaa !6
  %call12 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this1)
  %add13 = add nsw i32 6, %call12
  %cmp14 = icmp slt i32 %22, %add13
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond11
  %23 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  br label %for.end29

for.body16:                                       ; preds = %for.cond11
  %24 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds float, float* %24, i32 %25
  %26 = load float, float* %arrayidx17, align 4, !tbaa !8
  %27 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul18 = fmul float %26, %27
  %28 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds float, float* %28, i32 %29
  %30 = load float, float* %arrayidx19, align 4, !tbaa !8
  %mul20 = fmul float %mul18, %30
  %31 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul21 = fmul float %mul20, %31
  %32 = load float, float* %sum, align 4, !tbaa !8
  %add22 = fadd float %32, %mul21
  store float %add22, float* %sum, align 4, !tbaa !8
  %33 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds float, float* %33, i32 %34
  %35 = load float, float* %arrayidx23, align 4, !tbaa !8
  %36 = load float, float* %multiplier.addr, align 4, !tbaa !8
  %mul24 = fmul float %35, %36
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %37 = load i32, i32* %i10, align 4, !tbaa !6
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_real_buf, i32 %37)
  %38 = load float, float* %call25, align 4, !tbaa !8
  %add26 = fadd float %38, %mul24
  store float %add26, float* %call25, align 4, !tbaa !8
  br label %for.inc27

for.inc27:                                        ; preds = %for.body16
  %39 = load i32, i32* %i10, align 4, !tbaa !6
  %inc28 = add nsw i32 %39, 1
  store i32 %inc28, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.end29:                                        ; preds = %for.cond.cleanup15
  %40 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #4 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4, !tbaa !8
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !59
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #9
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #9
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #9
  %10 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %this) #6 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_invMass
}

define hidden void @_ZN27btMultiBodyConstraintSolver42resolveSingleConstraintRowGenericMultiBodyERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %c) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %c.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %deltaImpulse = alloca float, align 4
  %deltaVelADotn = alloca float, align 4
  %deltaVelBDotn = alloca float, align 4
  %ndofA = alloca i32, align 4
  %ndofB = alloca i32, align 4
  %i = alloca i32, align 4
  %i15 = alloca i32, align 4
  %sum = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %c, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast float* %deltaImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %1, i32 0, i32 14
  %2 = load float, float* %m_rhs, align 4, !tbaa !24
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 0, i32 11
  %4 = load float, float* %m_appliedImpulse, align 4, !tbaa !15
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %5, i32 0, i32 15
  %6 = load float, float* %m_cfm, align 4, !tbaa !25
  %mul = fmul float %4, %6
  %sub = fsub float %2, %mul
  store float %sub, float* %deltaImpulse, align 4, !tbaa !8
  %7 = bitcast float* %deltaVelADotn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %deltaVelADotn, align 4, !tbaa !8
  %8 = bitcast float* %deltaVelBDotn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0.000000e+00, float* %deltaVelBDotn, align 4, !tbaa !8
  %9 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store i32 0, i32* %ndofA, align 4, !tbaa !6
  %10 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store i32 0, i32* %ndofB, align 4, !tbaa !6
  %11 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %11, i32 0, i32 23
  %12 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !26
  %tobool = icmp ne %class.btMultiBody* %12, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %13, i32 0, i32 23
  %14 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA2, align 4, !tbaa !26
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %14)
  %add = add nsw i32 %call, 6
  store i32 %add, i32* %ndofA, align 4, !tbaa !6
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %ndofA, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 0
  %19 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %19, i32 0, i32 3
  %20 = load i32, i32* %m_jacAindex, align 4, !tbaa !27
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %add3 = add nsw i32 %20, %21
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians, i32 %add3)
  %22 = load float, float* %call4, align 4, !tbaa !8
  %m_data5 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data5, i32 0, i32 2
  %23 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %23, i32 0, i32 0
  %24 = load i32, i32* %m_deltaVelAindex, align 4, !tbaa !28
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %add6 = add nsw i32 %24, %25
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocities, i32 %add6)
  %26 = load float, float* %call7, align 4, !tbaa !8
  %mul8 = fmul float %22, %26
  %27 = load float, float* %deltaVelADotn, align 4, !tbaa !8
  %add9 = fadd float %27, %mul8
  store float %add9, float* %deltaVelADotn, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %29 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %29, i32 0, i32 26
  %30 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !30
  %tobool10 = icmp ne %class.btMultiBody* %30, null
  br i1 %tobool10, label %if.then11, label %if.end33

if.then11:                                        ; preds = %if.end
  %31 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB12 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %31, i32 0, i32 26
  %32 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB12, align 4, !tbaa !30
  %call13 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %32)
  %add14 = add nsw i32 %call13, 6
  store i32 %add14, i32* %ndofB, align 4, !tbaa !6
  %33 = bitcast i32* %i15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  store i32 0, i32* %i15, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc30, %if.then11
  %34 = load i32, i32* %i15, align 4, !tbaa !6
  %35 = load i32, i32* %ndofB, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %34, %35
  br i1 %cmp17, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond16
  %36 = bitcast i32* %i15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  br label %for.end32

for.body19:                                       ; preds = %for.cond16
  %m_data20 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians21 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data20, i32 0, i32 0
  %37 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %37, i32 0, i32 7
  %38 = load i32, i32* %m_jacBindex, align 4, !tbaa !31
  %39 = load i32, i32* %i15, align 4, !tbaa !6
  %add22 = add nsw i32 %38, %39
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians21, i32 %add22)
  %40 = load float, float* %call23, align 4, !tbaa !8
  %m_data24 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities25 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data24, i32 0, i32 2
  %41 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %41, i32 0, i32 4
  %42 = load i32, i32* %m_deltaVelBindex, align 4, !tbaa !32
  %43 = load i32, i32* %i15, align 4, !tbaa !6
  %add26 = add nsw i32 %42, %43
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocities25, i32 %add26)
  %44 = load float, float* %call27, align 4, !tbaa !8
  %mul28 = fmul float %40, %44
  %45 = load float, float* %deltaVelBDotn, align 4, !tbaa !8
  %add29 = fadd float %45, %mul28
  store float %add29, float* %deltaVelBDotn, align 4, !tbaa !8
  br label %for.inc30

for.inc30:                                        ; preds = %for.body19
  %46 = load i32, i32* %i15, align 4, !tbaa !6
  %inc31 = add nsw i32 %46, 1
  store i32 %inc31, i32* %i15, align 4, !tbaa !6
  br label %for.cond16

for.end32:                                        ; preds = %for.cond.cleanup18
  br label %if.end33

if.end33:                                         ; preds = %for.end32, %if.end
  %47 = load float, float* %deltaVelADotn, align 4, !tbaa !8
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 13
  %49 = load float, float* %m_jacDiagABInv, align 4, !tbaa !34
  %mul34 = fmul float %47, %49
  %50 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %sub35 = fsub float %50, %mul34
  store float %sub35, float* %deltaImpulse, align 4, !tbaa !8
  %51 = load float, float* %deltaVelBDotn, align 4, !tbaa !8
  %52 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacDiagABInv36 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %52, i32 0, i32 13
  %53 = load float, float* %m_jacDiagABInv36, align 4, !tbaa !34
  %mul37 = fmul float %51, %53
  %54 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %sub38 = fsub float %54, %mul37
  store float %sub38, float* %deltaImpulse, align 4, !tbaa !8
  %55 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #9
  %56 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse39 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %56, i32 0, i32 11
  %57 = load float, float* %m_appliedImpulse39, align 4, !tbaa !15
  %58 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %add40 = fadd float %57, %58
  store float %add40, float* %sum, align 4, !tbaa !8
  %59 = load float, float* %sum, align 4, !tbaa !8
  %60 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %60, i32 0, i32 16
  %61 = load float, float* %m_lowerLimit, align 4, !tbaa !17
  %cmp41 = fcmp olt float %59, %61
  br i1 %cmp41, label %if.then42, label %if.else

if.then42:                                        ; preds = %if.end33
  %62 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_lowerLimit43 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %62, i32 0, i32 16
  %63 = load float, float* %m_lowerLimit43, align 4, !tbaa !17
  %64 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse44 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %64, i32 0, i32 11
  %65 = load float, float* %m_appliedImpulse44, align 4, !tbaa !15
  %sub45 = fsub float %63, %65
  store float %sub45, float* %deltaImpulse, align 4, !tbaa !8
  %66 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_lowerLimit46 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %66, i32 0, i32 16
  %67 = load float, float* %m_lowerLimit46, align 4, !tbaa !17
  %68 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse47 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %68, i32 0, i32 11
  store float %67, float* %m_appliedImpulse47, align 4, !tbaa !15
  br label %if.end58

if.else:                                          ; preds = %if.end33
  %69 = load float, float* %sum, align 4, !tbaa !8
  %70 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %70, i32 0, i32 17
  %71 = load float, float* %m_upperLimit, align 4, !tbaa !18
  %cmp48 = fcmp ogt float %69, %71
  br i1 %cmp48, label %if.then49, label %if.else55

if.then49:                                        ; preds = %if.else
  %72 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_upperLimit50 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %72, i32 0, i32 17
  %73 = load float, float* %m_upperLimit50, align 4, !tbaa !18
  %74 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse51 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %74, i32 0, i32 11
  %75 = load float, float* %m_appliedImpulse51, align 4, !tbaa !15
  %sub52 = fsub float %73, %75
  store float %sub52, float* %deltaImpulse, align 4, !tbaa !8
  %76 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_upperLimit53 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %76, i32 0, i32 17
  %77 = load float, float* %m_upperLimit53, align 4, !tbaa !18
  %78 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse54 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %78, i32 0, i32 11
  store float %77, float* %m_appliedImpulse54, align 4, !tbaa !15
  br label %if.end57

if.else55:                                        ; preds = %if.else
  %79 = load float, float* %sum, align 4, !tbaa !8
  %80 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_appliedImpulse56 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %80, i32 0, i32 11
  store float %79, float* %m_appliedImpulse56, align 4, !tbaa !15
  br label %if.end57

if.end57:                                         ; preds = %if.else55, %if.then49
  br label %if.end58

if.end58:                                         ; preds = %if.end57, %if.then42
  %81 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA59 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %81, i32 0, i32 23
  %82 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA59, align 4, !tbaa !26
  %tobool60 = icmp ne %class.btMultiBody* %82, null
  br i1 %tobool60, label %if.then61, label %if.end71

if.then61:                                        ; preds = %if.end58
  %m_data62 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data62, i32 0, i32 1
  %83 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacAindex63 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %83, i32 0, i32 3
  %84 = load i32, i32* %m_jacAindex63, align 4, !tbaa !27
  %call64 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse, i32 %84)
  %85 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %86 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelAindex65 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %86, i32 0, i32 0
  %87 = load i32, i32* %m_deltaVelAindex65, align 4, !tbaa !28
  %88 = load i32, i32* %ndofA, align 4, !tbaa !6
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %call64, float %85, i32 %87, i32 %88)
  %89 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyA66 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %89, i32 0, i32 23
  %90 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA66, align 4, !tbaa !26
  %m_data67 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse68 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data67, i32 0, i32 1
  %91 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacAindex69 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %91, i32 0, i32 3
  %92 = load i32, i32* %m_jacAindex69, align 4, !tbaa !27
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse68, i32 %92)
  %93 = load float, float* %deltaImpulse, align 4, !tbaa !8
  call void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %90, float* %call70, float %93)
  br label %if.end71

if.end71:                                         ; preds = %if.then61, %if.end58
  %94 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB72 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %94, i32 0, i32 26
  %95 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB72, align 4, !tbaa !30
  %tobool73 = icmp ne %class.btMultiBody* %95, null
  br i1 %tobool73, label %if.then74, label %if.end85

if.then74:                                        ; preds = %if.end71
  %m_data75 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse76 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data75, i32 0, i32 1
  %96 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacBindex77 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %96, i32 0, i32 7
  %97 = load i32, i32* %m_jacBindex77, align 4, !tbaa !31
  %call78 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse76, i32 %97)
  %98 = load float, float* %deltaImpulse, align 4, !tbaa !8
  %99 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_deltaVelBindex79 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %99, i32 0, i32 4
  %100 = load i32, i32* %m_deltaVelBindex79, align 4, !tbaa !32
  %101 = load i32, i32* %ndofB, align 4, !tbaa !6
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %call78, float %98, i32 %100, i32 %101)
  %102 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_multiBodyB80 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %102, i32 0, i32 26
  %103 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB80, align 4, !tbaa !30
  %m_data81 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse82 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data81, i32 0, i32 1
  %104 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4, !tbaa !2
  %m_jacBindex83 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %104, i32 0, i32 7
  %105 = load i32, i32* %m_jacBindex83, align 4, !tbaa !31
  %call84 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse82, i32 %105)
  %106 = load float, float* %deltaImpulse, align 4, !tbaa !8
  call void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %103, float* %call84, float %106)
  br label %if.end85

if.end85:                                         ; preds = %if.then74, %if.end71
  %107 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast float* %deltaVelBDotn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast float* %deltaVelADotn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast float* %deltaImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #9
  ret void
}

define hidden void @_ZN27btMultiBodyConstraintSolver31setupMultiBodyContactConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %solverConstraint, %class.btVector3* nonnull align 4 dereferenceable(16) %contactNormal, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %cp, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, float* nonnull align 4 dereferenceable(4) %relaxation, i1 zeroext %isFriction, float %desiredVelocity, float %cfmSlip) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %solverConstraint.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %contactNormal.addr = alloca %class.btVector3*, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %relaxation.addr = alloca float*, align 4
  %isFriction.addr = alloca i8, align 1
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %multiBodyA = alloca %class.btMultiBody*, align 4
  %multiBodyB = alloca %class.btMultiBody*, align 4
  %pos1 = alloca %class.btVector3*, align 4
  %pos2 = alloca %class.btVector3*, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ndofA = alloca i32, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %jac1 = alloca float*, align 4
  %delta = alloca float*, align 4
  %torqueAxis0 = alloca %class.btVector3, align 4
  %ref.tmp87 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca float, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp96 = alloca float, align 4
  %ndofB = alloca i32, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp130 = alloca float, align 4
  %ref.tmp137 = alloca float, align 4
  %ref.tmp139 = alloca %class.btVector3, align 4
  %torqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp163 = alloca %class.btVector3, align 4
  %ref.tmp166 = alloca %class.btVector3, align 4
  %ref.tmp168 = alloca %class.btVector3, align 4
  %ref.tmp171 = alloca float, align 4
  %ref.tmp172 = alloca float, align 4
  %ref.tmp173 = alloca float, align 4
  %ref.tmp176 = alloca %class.btVector3, align 4
  %ref.tmp177 = alloca %class.btVector3, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %jacB = alloca float*, align 4
  %jacA = alloca float*, align 4
  %lambdaA = alloca float*, align 4
  %lambdaB = alloca float*, align 4
  %ndofA180 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca float, align 4
  %l = alloca float, align 4
  %ref.tmp199 = alloca %class.btVector3, align 4
  %ndofB208 = alloca i32, align 4
  %i219 = alloca i32, align 4
  %j224 = alloca float, align 4
  %l226 = alloca float, align 4
  %ref.tmp236 = alloca %class.btVector3, align 4
  %ref.tmp237 = alloca %class.btVector3, align 4
  %i247 = alloca i32, align 4
  %d = alloca float, align 4
  %restitution = alloca float, align 4
  %penetration = alloca float, align 4
  %rel_vel = alloca float, align 4
  %ndofA278 = alloca i32, align 4
  %ndofB279 = alloca i32, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %jacA286 = alloca float*, align 4
  %i291 = alloca i32, align 4
  %ref.tmp307 = alloca %class.btVector3, align 4
  %jacB317 = alloca float*, align 4
  %i322 = alloca i32, align 4
  %ref.tmp338 = alloca %class.btVector3, align 4
  %impulse = alloca float, align 4
  %deltaV = alloca float*, align 4
  %ref.tmp371 = alloca %class.btVector3, align 4
  %ref.tmp372 = alloca %class.btVector3, align 4
  %impulse382 = alloca float, align 4
  %deltaV384 = alloca float*, align 4
  %ref.tmp393 = alloca %class.btVector3, align 4
  %ref.tmp394 = alloca %class.btVector3, align 4
  %ref.tmp395 = alloca %class.btVector3, align 4
  %ref.tmp399 = alloca %class.btVector3, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %erp = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %solverConstraint, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  store %class.btVector3* %contactNormal, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store float* %relaxation, float** %relaxation.addr, align 4, !tbaa !2
  %frombool = zext i1 %isFriction to i8
  store i8 %frombool, i8* %isFriction.addr, align 1, !tbaa !62
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4, !tbaa !8
  store float %cfmSlip, float* %cfmSlip.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #9
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0))
  %1 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos1)
  %2 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos2)
  %3 = bitcast %class.btMultiBody** %multiBodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 0, i32 23
  %5 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !26
  store %class.btMultiBody* %5, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %6 = bitcast %class.btMultiBody** %multiBodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 0, i32 26
  %8 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !30
  store %class.btMultiBody* %8, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %9 = bitcast %class.btVector3** %pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %10)
  store %class.btVector3* %call4, %class.btVector3** %pos1, align 4, !tbaa !2
  %11 = bitcast %class.btVector3** %pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %12)
  store %class.btVector3* %call5, %class.btVector3** %pos2, align 4, !tbaa !2
  %13 = bitcast %struct.btSolverBody** %bodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBody* %14, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %15 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %15, i32 0, i32 1
  %16 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %16, i32 0, i32 22
  %17 = load i32, i32* %m_solverBodyIdA, align 4, !tbaa !29
  %call6 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %17)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btSolverBody* [ null, %cond.true ], [ %call6, %cond.false ]
  store %struct.btSolverBody* %cond, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %18 = bitcast %struct.btSolverBody** %bodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool7 = icmp ne %class.btMultiBody* %19, null
  br i1 %tobool7, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  br label %cond.end12

cond.false9:                                      ; preds = %cond.end
  %20 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %20, i32 0, i32 1
  %21 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %21, i32 0, i32 25
  %22 = load i32, i32* %m_solverBodyIdB, align 4, !tbaa !33
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool10, i32 %22)
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false9, %cond.true8
  %cond13 = phi %struct.btSolverBody* [ null, %cond.true8 ], [ %call11, %cond.false9 ]
  store %struct.btSolverBody* %cond13, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %23 = bitcast %class.btRigidBody** %rb0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool14 = icmp ne %class.btMultiBody* %24, null
  br i1 %tobool14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.end12
  br label %cond.end17

cond.false16:                                     ; preds = %cond.end12
  %25 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %25, i32 0, i32 12
  %26 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !59
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true15
  %cond18 = phi %class.btRigidBody* [ null, %cond.true15 ], [ %26, %cond.false16 ]
  store %class.btRigidBody* %cond18, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %27 = bitcast %class.btRigidBody** %rb1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  %28 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool19 = icmp ne %class.btMultiBody* %28, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end23

cond.false21:                                     ; preds = %cond.end17
  %29 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %m_originalBody22 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %29, i32 0, i32 12
  %30 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody22, align 4, !tbaa !59
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false21, %cond.true20
  %cond24 = phi %class.btRigidBody* [ null, %cond.true20 ], [ %30, %cond.false21 ]
  store %class.btRigidBody* %cond24, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %31 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %tobool25 = icmp ne %struct.btSolverBody* %31, null
  br i1 %tobool25, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end23
  %32 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #9
  %33 = load %class.btVector3*, %class.btVector3** %pos1, align 4, !tbaa !2
  %34 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %34)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call26)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %call27)
  %35 = bitcast %class.btVector3* %rel_pos1 to i8*
  %36 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false), !tbaa.struct !63
  %37 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end23
  %38 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %tobool28 = icmp ne %struct.btSolverBody* %38, null
  br i1 %tobool28, label %if.then29, label %if.end33

if.then29:                                        ; preds = %if.end
  %39 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %39) #9
  %40 = load %class.btVector3*, %class.btVector3** %pos2, align 4, !tbaa !2
  %41 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %call31 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %41)
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call31)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %40, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %42 = bitcast %class.btVector3* %rel_pos2 to i8*
  %43 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false), !tbaa.struct !63
  %44 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #9
  br label %if.end33

if.end33:                                         ; preds = %if.then29, %if.end
  %45 = load float*, float** %relaxation.addr, align 4, !tbaa !2
  store float 1.000000e+00, float* %45, align 4, !tbaa !8
  %46 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool34 = icmp ne %class.btMultiBody* %46, null
  br i1 %tobool34, label %if.then35, label %if.else86

if.then35:                                        ; preds = %if.end33
  %47 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #9
  %48 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call36 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %48)
  %add = add nsw i32 %call36, 6
  store i32 %add, i32* %ndofA, align 4, !tbaa !6
  %49 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call37 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %49)
  %50 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %50, i32 0, i32 0
  store i32 %call37, i32* %m_deltaVelAindex, align 4, !tbaa !28
  %51 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex38 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %51, i32 0, i32 0
  %52 = load i32, i32* %m_deltaVelAindex38, align 4, !tbaa !28
  %cmp = icmp slt i32 %52, 0
  br i1 %cmp, label %if.then39, label %if.else

if.then39:                                        ; preds = %if.then35
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 2
  %call40 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_deltaVelocities)
  %53 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex41 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %53, i32 0, i32 0
  store i32 %call40, i32* %m_deltaVelAindex41, align 4, !tbaa !28
  %54 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %55 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex42 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %55, i32 0, i32 0
  %56 = load i32, i32* %m_deltaVelAindex42, align 4, !tbaa !28
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %54, i32 %56)
  %m_data43 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities44 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data43, i32 0, i32 2
  %m_data45 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities46 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data45, i32 0, i32 2
  %call47 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_deltaVelocities46)
  %57 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add48 = add nsw i32 %call47, %57
  %58 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #9
  store float 0.000000e+00, float* %ref.tmp49, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_deltaVelocities44, i32 %add48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %59 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  br label %if.end50

if.else:                                          ; preds = %if.then35
  br label %if.end50

if.end50:                                         ; preds = %if.else, %if.then39
  %m_data51 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data51, i32 0, i32 0
  %call52 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_jacobians)
  %60 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %60, i32 0, i32 3
  store i32 %call52, i32* %m_jacAindex, align 4, !tbaa !27
  %m_data53 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians54 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data53, i32 0, i32 0
  %m_data55 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians56 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data55, i32 0, i32 0
  %call57 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_jacobians56)
  %61 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add58 = add nsw i32 %call57, %61
  %62 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #9
  store float 0.000000e+00, float* %ref.tmp59, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_jacobians54, i32 %add58, float* nonnull align 4 dereferenceable(4) %ref.tmp59)
  %63 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #9
  %m_data60 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data60, i32 0, i32 1
  %m_data61 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse62 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data61, i32 0, i32 1
  %call63 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse62)
  %64 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add64 = add nsw i32 %call63, %64
  %65 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #9
  store float 0.000000e+00, float* %ref.tmp65, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse, i32 %add64, float* nonnull align 4 dereferenceable(4) %ref.tmp65)
  %66 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #9
  %67 = bitcast float** %jac1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  %m_data66 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians67 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data66, i32 0, i32 0
  %68 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex68 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %68, i32 0, i32 3
  %69 = load i32, i32* %m_jacAindex68, align 4, !tbaa !27
  %call69 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians67, i32 %69)
  store float* %call69, float** %jac1, align 4, !tbaa !2
  %70 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %71 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %71, i32 0, i32 24
  %72 = load i32, i32* %m_linkA, align 4, !tbaa !64
  %73 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %call70 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %73)
  %74 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  %75 = load float*, float** %jac1, align 4, !tbaa !2
  %m_data71 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data71, i32 0, i32 3
  %m_data72 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data72, i32 0, i32 4
  %m_data73 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_m = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data73, i32 0, i32 5
  call void @_ZNK11btMultiBody19fillContactJacobianEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %70, i32 %72, %class.btVector3* nonnull align 4 dereferenceable(16) %call70, %class.btVector3* nonnull align 4 dereferenceable(16) %74, float* %75, %class.btAlignedObjectArray.30* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_m)
  %76 = bitcast float** %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #9
  %m_data74 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse75 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data74, i32 0, i32 1
  %77 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex76 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %77, i32 0, i32 3
  %78 = load i32, i32* %m_jacAindex76, align 4, !tbaa !27
  %call77 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse75, i32 %78)
  store float* %call77, float** %delta, align 4, !tbaa !2
  %79 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %m_data78 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians79 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data78, i32 0, i32 0
  %80 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex80 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %80, i32 0, i32 3
  %81 = load i32, i32* %m_jacAindex80, align 4, !tbaa !27
  %call81 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians79, i32 %81)
  %82 = load float*, float** %delta, align 4, !tbaa !2
  %m_data82 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r83 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data82, i32 0, i32 3
  %m_data84 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v85 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data84, i32 0, i32 4
  call void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %79, float* %call81, float* %82, %class.btAlignedObjectArray.30* nonnull align 4 dereferenceable(17) %scratch_r83, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_v85)
  %83 = bitcast float** %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #9
  %84 = bitcast float** %jac1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #9
  %85 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #9
  br label %if.end99

if.else86:                                        ; preds = %if.end33
  %86 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %86) #9
  %87 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis0, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %87)
  %88 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %88) #9
  %89 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %tobool88 = icmp ne %class.btRigidBody* %89, null
  %90 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %90) #9
  %91 = bitcast float* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #9
  %92 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #9
  %93 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #9
  br i1 %tobool88, label %cond.true89, label %cond.false93

cond.true89:                                      ; preds = %if.else86
  %94 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %call91 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %94)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp90, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call91, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis0)
  %95 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %call92 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %95)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp87, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90, %class.btVector3* nonnull align 4 dereferenceable(16) %call92)
  br label %cond.end98

cond.false93:                                     ; preds = %if.else86
  store float 0.000000e+00, float* %ref.tmp94, align 4, !tbaa !8
  store float 0.000000e+00, float* %ref.tmp95, align 4, !tbaa !8
  store float 0.000000e+00, float* %ref.tmp96, align 4, !tbaa !8
  %call97 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp94, float* nonnull align 4 dereferenceable(4) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp96)
  br label %cond.end98

cond.end98:                                       ; preds = %cond.false93, %cond.true89
  %96 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %96, i32 0, i32 8
  %97 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %98 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %97, i8* align 4 %98, i32 16, i1 false), !tbaa.struct !63
  %99 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #9
  %100 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  %101 = bitcast float* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %102) #9
  %103 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #9
  %104 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %104, i32 0, i32 1
  %105 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %106 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %105, i8* align 4 %106, i32 16, i1 false), !tbaa.struct !63
  %107 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  %108 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %108, i32 0, i32 2
  %109 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %110 = bitcast %class.btVector3* %107 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %109, i8* align 4 %110, i32 16, i1 false), !tbaa.struct !63
  %111 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #9
  br label %if.end99

if.end99:                                         ; preds = %cond.end98, %if.end50
  %112 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool100 = icmp ne %class.btMultiBody* %112, null
  br i1 %tobool100, label %if.then101, label %if.else162

if.then101:                                       ; preds = %if.end99
  %113 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #9
  %114 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call102 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %114)
  %add103 = add nsw i32 %call102, 6
  store i32 %add103, i32* %ndofB, align 4, !tbaa !6
  %115 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call104 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %115)
  %116 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %116, i32 0, i32 4
  store i32 %call104, i32* %m_deltaVelBindex, align 4, !tbaa !32
  %117 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex105 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %117, i32 0, i32 4
  %118 = load i32, i32* %m_deltaVelBindex105, align 4, !tbaa !32
  %cmp106 = icmp slt i32 %118, 0
  br i1 %cmp106, label %if.then107, label %if.end120

if.then107:                                       ; preds = %if.then101
  %m_data108 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities109 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data108, i32 0, i32 2
  %call110 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_deltaVelocities109)
  %119 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex111 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %119, i32 0, i32 4
  store i32 %call110, i32* %m_deltaVelBindex111, align 4, !tbaa !32
  %120 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %121 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex112 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %121, i32 0, i32 4
  %122 = load i32, i32* %m_deltaVelBindex112, align 4, !tbaa !32
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %120, i32 %122)
  %m_data113 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities114 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data113, i32 0, i32 2
  %m_data115 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities116 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data115, i32 0, i32 2
  %call117 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_deltaVelocities116)
  %123 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add118 = add nsw i32 %call117, %123
  %124 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #9
  store float 0.000000e+00, float* %ref.tmp119, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_deltaVelocities114, i32 %add118, float* nonnull align 4 dereferenceable(4) %ref.tmp119)
  %125 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #9
  br label %if.end120

if.end120:                                        ; preds = %if.then107, %if.then101
  %m_data121 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians122 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data121, i32 0, i32 0
  %call123 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_jacobians122)
  %126 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %126, i32 0, i32 7
  store i32 %call123, i32* %m_jacBindex, align 4, !tbaa !31
  %m_data124 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians125 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data124, i32 0, i32 0
  %m_data126 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians127 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data126, i32 0, i32 0
  %call128 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_jacobians127)
  %127 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add129 = add nsw i32 %call128, %127
  %128 = bitcast float* %ref.tmp130 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #9
  store float 0.000000e+00, float* %ref.tmp130, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_jacobians125, i32 %add129, float* nonnull align 4 dereferenceable(4) %ref.tmp130)
  %129 = bitcast float* %ref.tmp130 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #9
  %m_data131 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse132 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data131, i32 0, i32 1
  %m_data133 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse134 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data133, i32 0, i32 1
  %call135 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse134)
  %130 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add136 = add nsw i32 %call135, %130
  %131 = bitcast float* %ref.tmp137 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #9
  store float 0.000000e+00, float* %ref.tmp137, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse132, i32 %add136, float* nonnull align 4 dereferenceable(4) %ref.tmp137)
  %132 = bitcast float* %ref.tmp137 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #9
  %133 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %134 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %134, i32 0, i32 27
  %135 = load i32, i32* %m_linkB, align 4, !tbaa !65
  %136 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %call138 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %136)
  %137 = bitcast %class.btVector3* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %137) #9
  %138 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp139, %class.btVector3* nonnull align 4 dereferenceable(16) %138)
  %m_data140 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians141 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data140, i32 0, i32 0
  %139 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex142 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %139, i32 0, i32 7
  %140 = load i32, i32* %m_jacBindex142, align 4, !tbaa !31
  %call143 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians141, i32 %140)
  %m_data144 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r145 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data144, i32 0, i32 3
  %m_data146 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v147 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data146, i32 0, i32 4
  %m_data148 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_m149 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data148, i32 0, i32 5
  call void @_ZNK11btMultiBody19fillContactJacobianEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %133, i32 %135, %class.btVector3* nonnull align 4 dereferenceable(16) %call138, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp139, float* %call143, %class.btAlignedObjectArray.30* nonnull align 4 dereferenceable(17) %scratch_r145, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_v147, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_m149)
  %141 = bitcast %class.btVector3* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #9
  %142 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %m_data150 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians151 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data150, i32 0, i32 0
  %143 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex152 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %143, i32 0, i32 7
  %144 = load i32, i32* %m_jacBindex152, align 4, !tbaa !31
  %call153 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians151, i32 %144)
  %m_data154 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse155 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data154, i32 0, i32 1
  %145 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex156 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %145, i32 0, i32 7
  %146 = load i32, i32* %m_jacBindex156, align 4, !tbaa !31
  %call157 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse155, i32 %146)
  %m_data158 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r159 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data158, i32 0, i32 3
  %m_data160 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v161 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data160, i32 0, i32 4
  call void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %142, float* %call153, float* %call157, %class.btAlignedObjectArray.30* nonnull align 4 dereferenceable(17) %scratch_r159, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_v161)
  %147 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #9
  br label %if.end178

if.else162:                                       ; preds = %if.end99
  %148 = bitcast %class.btVector3* %torqueAxis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %148) #9
  %149 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis1, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %149)
  %150 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %150) #9
  %151 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %tobool164 = icmp ne %class.btRigidBody* %151, null
  %152 = bitcast %class.btVector3* %ref.tmp166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %152) #9
  %153 = bitcast %class.btVector3* %ref.tmp168 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %153) #9
  %154 = bitcast float* %ref.tmp171 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #9
  %155 = bitcast float* %ref.tmp172 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %155) #9
  %156 = bitcast float* %ref.tmp173 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %156) #9
  br i1 %tobool164, label %cond.true165, label %cond.false170

cond.true165:                                     ; preds = %if.else162
  %157 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %call167 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %157)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp168, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp166, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call167, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp168)
  %158 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %call169 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %158)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp163, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp166, %class.btVector3* nonnull align 4 dereferenceable(16) %call169)
  br label %cond.end175

cond.false170:                                    ; preds = %if.else162
  store float 0.000000e+00, float* %ref.tmp171, align 4, !tbaa !8
  store float 0.000000e+00, float* %ref.tmp172, align 4, !tbaa !8
  store float 0.000000e+00, float* %ref.tmp173, align 4, !tbaa !8
  %call174 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp163, float* nonnull align 4 dereferenceable(4) %ref.tmp171, float* nonnull align 4 dereferenceable(4) %ref.tmp172, float* nonnull align 4 dereferenceable(4) %ref.tmp173)
  br label %cond.end175

cond.end175:                                      ; preds = %cond.false170, %cond.true165
  %159 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %159, i32 0, i32 9
  %160 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %161 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %160, i8* align 4 %161, i32 16, i1 false), !tbaa.struct !63
  %162 = bitcast float* %ref.tmp173 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #9
  %163 = bitcast float* %ref.tmp172 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #9
  %164 = bitcast float* %ref.tmp171 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #9
  %165 = bitcast %class.btVector3* %ref.tmp168 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %165) #9
  %166 = bitcast %class.btVector3* %ref.tmp166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %166) #9
  %167 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %167) #9
  %168 = bitcast %class.btVector3* %ref.tmp176 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %168) #9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp176, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  %169 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %169, i32 0, i32 5
  %170 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %171 = bitcast %class.btVector3* %ref.tmp176 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %170, i8* align 4 %171, i32 16, i1 false), !tbaa.struct !63
  %172 = bitcast %class.btVector3* %ref.tmp176 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %172) #9
  %173 = bitcast %class.btVector3* %ref.tmp177 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %173) #9
  %174 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp177, %class.btVector3* nonnull align 4 dereferenceable(16) %174)
  %175 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %175, i32 0, i32 6
  %176 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %177 = bitcast %class.btVector3* %ref.tmp177 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %176, i8* align 4 %177, i32 16, i1 false), !tbaa.struct !63
  %178 = bitcast %class.btVector3* %ref.tmp177 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %178) #9
  %179 = bitcast %class.btVector3* %torqueAxis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %179) #9
  br label %if.end178

if.end178:                                        ; preds = %cond.end175, %if.end120
  %180 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %180) #9
  %call179 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  %181 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %181) #9
  store float 0.000000e+00, float* %denom0, align 4, !tbaa !8
  %182 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %182) #9
  store float 0.000000e+00, float* %denom1, align 4, !tbaa !8
  %183 = bitcast float** %jacB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %183) #9
  store float* null, float** %jacB, align 4, !tbaa !2
  %184 = bitcast float** %jacA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %184) #9
  store float* null, float** %jacA, align 4, !tbaa !2
  %185 = bitcast float** %lambdaA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #9
  store float* null, float** %lambdaA, align 4, !tbaa !2
  %186 = bitcast float** %lambdaB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %186) #9
  store float* null, float** %lambdaB, align 4, !tbaa !2
  %187 = bitcast i32* %ndofA180 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #9
  store i32 0, i32* %ndofA180, align 4, !tbaa !6
  %188 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool181 = icmp ne %class.btMultiBody* %188, null
  br i1 %tobool181, label %if.then182, label %if.else196

if.then182:                                       ; preds = %if.end178
  %189 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call183 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %189)
  %add184 = add nsw i32 %call183, 6
  store i32 %add184, i32* %ndofA180, align 4, !tbaa !6
  %m_data185 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians186 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data185, i32 0, i32 0
  %190 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex187 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %190, i32 0, i32 3
  %191 = load i32, i32* %m_jacAindex187, align 4, !tbaa !27
  %call188 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians186, i32 %191)
  store float* %call188, float** %jacA, align 4, !tbaa !2
  %m_data189 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse190 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data189, i32 0, i32 1
  %192 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex191 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %192, i32 0, i32 3
  %193 = load i32, i32* %m_jacAindex191, align 4, !tbaa !27
  %call192 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse190, i32 %193)
  store float* %call192, float** %lambdaA, align 4, !tbaa !2
  %194 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then182
  %195 = load i32, i32* %i, align 4, !tbaa !6
  %196 = load i32, i32* %ndofA180, align 4, !tbaa !6
  %cmp193 = icmp slt i32 %195, %196
  br i1 %cmp193, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %197 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %198 = bitcast float* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #9
  %199 = load float*, float** %jacA, align 4, !tbaa !2
  %200 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %199, i32 %200
  %201 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %201, float* %j, align 4, !tbaa !8
  %202 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #9
  %203 = load float*, float** %lambdaA, align 4, !tbaa !2
  %204 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx194 = getelementptr inbounds float, float* %203, i32 %204
  %205 = load float, float* %arrayidx194, align 4, !tbaa !8
  store float %205, float* %l, align 4, !tbaa !8
  %206 = load float, float* %j, align 4, !tbaa !8
  %207 = load float, float* %l, align 4, !tbaa !8
  %mul = fmul float %206, %207
  %208 = load float, float* %denom0, align 4, !tbaa !8
  %add195 = fadd float %208, %mul
  store float %add195, float* %denom0, align 4, !tbaa !8
  %209 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #9
  %210 = bitcast float* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %211 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %211, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end205

if.else196:                                       ; preds = %if.end178
  %212 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %tobool197 = icmp ne %class.btRigidBody* %212, null
  br i1 %tobool197, label %if.then198, label %if.end204

if.then198:                                       ; preds = %if.else196
  %213 = bitcast %class.btVector3* %ref.tmp199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %213) #9
  %214 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentA200 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %214, i32 0, i32 8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp199, %class.btVector3* %m_angularComponentA200, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %215 = bitcast %class.btVector3* %vec to i8*
  %216 = bitcast %class.btVector3* %ref.tmp199 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %215, i8* align 4 %216, i32 16, i1 false), !tbaa.struct !63
  %217 = bitcast %class.btVector3* %ref.tmp199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %217) #9
  %218 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %call201 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %218)
  %219 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  %call202 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %219, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add203 = fadd float %call201, %call202
  store float %add203, float* %denom0, align 4, !tbaa !8
  br label %if.end204

if.end204:                                        ; preds = %if.then198, %if.else196
  br label %if.end205

if.end205:                                        ; preds = %if.end204, %for.end
  %220 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool206 = icmp ne %class.btMultiBody* %220, null
  br i1 %tobool206, label %if.then207, label %if.else233

if.then207:                                       ; preds = %if.end205
  %221 = bitcast i32* %ndofB208 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %221) #9
  %222 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call209 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %222)
  %add210 = add nsw i32 %call209, 6
  store i32 %add210, i32* %ndofB208, align 4, !tbaa !6
  %m_data211 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians212 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data211, i32 0, i32 0
  %223 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex213 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %223, i32 0, i32 7
  %224 = load i32, i32* %m_jacBindex213, align 4, !tbaa !31
  %call214 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians212, i32 %224)
  store float* %call214, float** %jacB, align 4, !tbaa !2
  %m_data215 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse216 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data215, i32 0, i32 1
  %225 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex217 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %225, i32 0, i32 7
  %226 = load i32, i32* %m_jacBindex217, align 4, !tbaa !31
  %call218 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse216, i32 %226)
  store float* %call218, float** %lambdaB, align 4, !tbaa !2
  %227 = bitcast i32* %i219 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %227) #9
  store i32 0, i32* %i219, align 4, !tbaa !6
  br label %for.cond220

for.cond220:                                      ; preds = %for.inc230, %if.then207
  %228 = load i32, i32* %i219, align 4, !tbaa !6
  %229 = load i32, i32* %ndofB208, align 4, !tbaa !6
  %cmp221 = icmp slt i32 %228, %229
  br i1 %cmp221, label %for.body223, label %for.cond.cleanup222

for.cond.cleanup222:                              ; preds = %for.cond220
  %230 = bitcast i32* %i219 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #9
  br label %for.end232

for.body223:                                      ; preds = %for.cond220
  %231 = bitcast float* %j224 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %231) #9
  %232 = load float*, float** %jacB, align 4, !tbaa !2
  %233 = load i32, i32* %i219, align 4, !tbaa !6
  %arrayidx225 = getelementptr inbounds float, float* %232, i32 %233
  %234 = load float, float* %arrayidx225, align 4, !tbaa !8
  store float %234, float* %j224, align 4, !tbaa !8
  %235 = bitcast float* %l226 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #9
  %236 = load float*, float** %lambdaB, align 4, !tbaa !2
  %237 = load i32, i32* %i219, align 4, !tbaa !6
  %arrayidx227 = getelementptr inbounds float, float* %236, i32 %237
  %238 = load float, float* %arrayidx227, align 4, !tbaa !8
  store float %238, float* %l226, align 4, !tbaa !8
  %239 = load float, float* %j224, align 4, !tbaa !8
  %240 = load float, float* %l226, align 4, !tbaa !8
  %mul228 = fmul float %239, %240
  %241 = load float, float* %denom1, align 4, !tbaa !8
  %add229 = fadd float %241, %mul228
  store float %add229, float* %denom1, align 4, !tbaa !8
  %242 = bitcast float* %l226 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #9
  %243 = bitcast float* %j224 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #9
  br label %for.inc230

for.inc230:                                       ; preds = %for.body223
  %244 = load i32, i32* %i219, align 4, !tbaa !6
  %inc231 = add nsw i32 %244, 1
  store i32 %inc231, i32* %i219, align 4, !tbaa !6
  br label %for.cond220

for.end232:                                       ; preds = %for.cond.cleanup222
  %245 = bitcast i32* %ndofB208 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #9
  br label %if.end243

if.else233:                                       ; preds = %if.end205
  %246 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %tobool234 = icmp ne %class.btRigidBody* %246, null
  br i1 %tobool234, label %if.then235, label %if.end242

if.then235:                                       ; preds = %if.else233
  %247 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %247) #9
  %248 = bitcast %class.btVector3* %ref.tmp237 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %248) #9
  %249 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentB238 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %249, i32 0, i32 9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp237, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB238)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp236, %class.btVector3* %ref.tmp237, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %250 = bitcast %class.btVector3* %vec to i8*
  %251 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %250, i8* align 4 %251, i32 16, i1 false), !tbaa.struct !63
  %252 = bitcast %class.btVector3* %ref.tmp237 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %252) #9
  %253 = bitcast %class.btVector3* %ref.tmp236 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %253) #9
  %254 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %call239 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %254)
  %255 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4, !tbaa !2
  %call240 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %255, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add241 = fadd float %call239, %call240
  store float %add241, float* %denom1, align 4, !tbaa !8
  br label %if.end242

if.end242:                                        ; preds = %if.then235, %if.else233
  br label %if.end243

if.end243:                                        ; preds = %if.end242, %for.end232
  %256 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool244 = icmp ne %class.btMultiBody* %256, null
  br i1 %tobool244, label %land.lhs.true, label %if.end263

land.lhs.true:                                    ; preds = %if.end243
  %257 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %258 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %cmp245 = icmp eq %class.btMultiBody* %257, %258
  br i1 %cmp245, label %if.then246, label %if.end263

if.then246:                                       ; preds = %land.lhs.true
  %259 = bitcast i32* %i247 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %259) #9
  store i32 0, i32* %i247, align 4, !tbaa !6
  br label %for.cond248

for.cond248:                                      ; preds = %for.inc260, %if.then246
  %260 = load i32, i32* %i247, align 4, !tbaa !6
  %261 = load i32, i32* %ndofA180, align 4, !tbaa !6
  %cmp249 = icmp slt i32 %260, %261
  br i1 %cmp249, label %for.body251, label %for.cond.cleanup250

for.cond.cleanup250:                              ; preds = %for.cond248
  %262 = bitcast i32* %i247 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #9
  br label %for.end262

for.body251:                                      ; preds = %for.cond248
  %263 = load float*, float** %jacB, align 4, !tbaa !2
  %264 = load i32, i32* %i247, align 4, !tbaa !6
  %arrayidx252 = getelementptr inbounds float, float* %263, i32 %264
  %265 = load float, float* %arrayidx252, align 4, !tbaa !8
  %266 = load float*, float** %lambdaA, align 4, !tbaa !2
  %267 = load i32, i32* %i247, align 4, !tbaa !6
  %arrayidx253 = getelementptr inbounds float, float* %266, i32 %267
  %268 = load float, float* %arrayidx253, align 4, !tbaa !8
  %mul254 = fmul float %265, %268
  %269 = load float, float* %denom1, align 4, !tbaa !8
  %add255 = fadd float %269, %mul254
  store float %add255, float* %denom1, align 4, !tbaa !8
  %270 = load float*, float** %jacA, align 4, !tbaa !2
  %271 = load i32, i32* %i247, align 4, !tbaa !6
  %arrayidx256 = getelementptr inbounds float, float* %270, i32 %271
  %272 = load float, float* %arrayidx256, align 4, !tbaa !8
  %273 = load float*, float** %lambdaB, align 4, !tbaa !2
  %274 = load i32, i32* %i247, align 4, !tbaa !6
  %arrayidx257 = getelementptr inbounds float, float* %273, i32 %274
  %275 = load float, float* %arrayidx257, align 4, !tbaa !8
  %mul258 = fmul float %272, %275
  %276 = load float, float* %denom1, align 4, !tbaa !8
  %add259 = fadd float %276, %mul258
  store float %add259, float* %denom1, align 4, !tbaa !8
  br label %for.inc260

for.inc260:                                       ; preds = %for.body251
  %277 = load i32, i32* %i247, align 4, !tbaa !6
  %inc261 = add nsw i32 %277, 1
  store i32 %inc261, i32* %i247, align 4, !tbaa !6
  br label %for.cond248

for.end262:                                       ; preds = %for.cond.cleanup250
  br label %if.end263

if.end263:                                        ; preds = %for.end262, %land.lhs.true, %if.end243
  %278 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %278) #9
  %279 = load float, float* %denom0, align 4, !tbaa !8
  %280 = load float, float* %denom1, align 4, !tbaa !8
  %add264 = fadd float %279, %280
  store float %add264, float* %d, align 4, !tbaa !8
  %281 = load float, float* %d, align 4, !tbaa !8
  %call265 = call float @_Z6btFabsf(float %281)
  %cmp266 = fcmp ogt float %call265, 0x3E80000000000000
  br i1 %cmp266, label %if.then267, label %if.else268

if.then267:                                       ; preds = %if.end263
  %282 = load float*, float** %relaxation.addr, align 4, !tbaa !2
  %283 = load float, float* %282, align 4, !tbaa !8
  %284 = load float, float* %d, align 4, !tbaa !8
  %div = fdiv float %283, %284
  %285 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %285, i32 0, i32 13
  store float %div, float* %m_jacDiagABInv, align 4, !tbaa !34
  br label %if.end270

if.else268:                                       ; preds = %if.end263
  %286 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv269 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %286, i32 0, i32 13
  store float 1.000000e+00, float* %m_jacDiagABInv269, align 4, !tbaa !34
  br label %if.end270

if.end270:                                        ; preds = %if.else268, %if.then267
  %287 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #9
  %288 = bitcast i32* %ndofA180 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #9
  %289 = bitcast float** %lambdaB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #9
  %290 = bitcast float** %lambdaA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #9
  %291 = bitcast float** %jacA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #9
  %292 = bitcast float** %jacB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #9
  %293 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #9
  %294 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #9
  %295 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %295) #9
  %296 = bitcast float* %restitution to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %296) #9
  store float 0.000000e+00, float* %restitution, align 4, !tbaa !8
  %297 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %297) #9
  %298 = load i8, i8* %isFriction.addr, align 1, !tbaa !62, !range !66
  %tobool271 = trunc i8 %298 to i1
  br i1 %tobool271, label %cond.true272, label %cond.false273

cond.true272:                                     ; preds = %if.end270
  br label %cond.end276

cond.false273:                                    ; preds = %if.end270
  %299 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %call274 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %299)
  %300 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %301 = bitcast %struct.btContactSolverInfo* %300 to %struct.btContactSolverInfoData*
  %m_linearSlop = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %301, i32 0, i32 14
  %302 = load float, float* %m_linearSlop, align 4, !tbaa !67
  %add275 = fadd float %call274, %302
  br label %cond.end276

cond.end276:                                      ; preds = %cond.false273, %cond.true272
  %cond277 = phi float [ 0.000000e+00, %cond.true272 ], [ %add275, %cond.false273 ]
  store float %cond277, float* %penetration, align 4, !tbaa !8
  %303 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %303) #9
  store float 0.000000e+00, float* %rel_vel, align 4, !tbaa !8
  %304 = bitcast i32* %ndofA278 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %304) #9
  store i32 0, i32* %ndofA278, align 4, !tbaa !6
  %305 = bitcast i32* %ndofB279 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %305) #9
  store i32 0, i32* %ndofB279, align 4, !tbaa !6
  %306 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %306) #9
  %call280 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %307 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %307) #9
  %call281 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %308 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool282 = icmp ne %class.btMultiBody* %308, null
  br i1 %tobool282, label %if.then283, label %if.else304

if.then283:                                       ; preds = %cond.end276
  %309 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call284 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %309)
  %add285 = add nsw i32 %call284, 6
  store i32 %add285, i32* %ndofA278, align 4, !tbaa !6
  %310 = bitcast float** %jacA286 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %310) #9
  %m_data287 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians288 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data287, i32 0, i32 0
  %311 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex289 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %311, i32 0, i32 3
  %312 = load i32, i32* %m_jacAindex289, align 4, !tbaa !27
  %call290 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians288, i32 %312)
  store float* %call290, float** %jacA286, align 4, !tbaa !2
  %313 = bitcast i32* %i291 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %313) #9
  store i32 0, i32* %i291, align 4, !tbaa !6
  br label %for.cond292

for.cond292:                                      ; preds = %for.inc301, %if.then283
  %314 = load i32, i32* %i291, align 4, !tbaa !6
  %315 = load i32, i32* %ndofA278, align 4, !tbaa !6
  %cmp293 = icmp slt i32 %314, %315
  br i1 %cmp293, label %for.body295, label %for.cond.cleanup294

for.cond.cleanup294:                              ; preds = %for.cond292
  %316 = bitcast i32* %i291 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #9
  br label %for.end303

for.body295:                                      ; preds = %for.cond292
  %317 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call296 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %317)
  %318 = load i32, i32* %i291, align 4, !tbaa !6
  %arrayidx297 = getelementptr inbounds float, float* %call296, i32 %318
  %319 = load float, float* %arrayidx297, align 4, !tbaa !8
  %320 = load float*, float** %jacA286, align 4, !tbaa !2
  %321 = load i32, i32* %i291, align 4, !tbaa !6
  %arrayidx298 = getelementptr inbounds float, float* %320, i32 %321
  %322 = load float, float* %arrayidx298, align 4, !tbaa !8
  %mul299 = fmul float %319, %322
  %323 = load float, float* %rel_vel, align 4, !tbaa !8
  %add300 = fadd float %323, %mul299
  store float %add300, float* %rel_vel, align 4, !tbaa !8
  br label %for.inc301

for.inc301:                                       ; preds = %for.body295
  %324 = load i32, i32* %i291, align 4, !tbaa !6
  %inc302 = add nsw i32 %324, 1
  store i32 %inc302, i32* %i291, align 4, !tbaa !6
  br label %for.cond292

for.end303:                                       ; preds = %for.cond.cleanup294
  %325 = bitcast float** %jacA286 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #9
  br label %if.end312

if.else304:                                       ; preds = %cond.end276
  %326 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %tobool305 = icmp ne %class.btRigidBody* %326, null
  br i1 %tobool305, label %if.then306, label %if.end311

if.then306:                                       ; preds = %if.else304
  %327 = bitcast %class.btVector3* %ref.tmp307 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %327) #9
  %328 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp307, %class.btRigidBody* %328, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %329 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal1308 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %329, i32 0, i32 2
  %call309 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp307, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1308)
  %330 = load float, float* %rel_vel, align 4, !tbaa !8
  %add310 = fadd float %330, %call309
  store float %add310, float* %rel_vel, align 4, !tbaa !8
  %331 = bitcast %class.btVector3* %ref.tmp307 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %331) #9
  br label %if.end311

if.end311:                                        ; preds = %if.then306, %if.else304
  br label %if.end312

if.end312:                                        ; preds = %if.end311, %for.end303
  %332 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool313 = icmp ne %class.btMultiBody* %332, null
  br i1 %tobool313, label %if.then314, label %if.else335

if.then314:                                       ; preds = %if.end312
  %333 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call315 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %333)
  %add316 = add nsw i32 %call315, 6
  store i32 %add316, i32* %ndofB279, align 4, !tbaa !6
  %334 = bitcast float** %jacB317 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %334) #9
  %m_data318 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians319 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data318, i32 0, i32 0
  %335 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex320 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %335, i32 0, i32 7
  %336 = load i32, i32* %m_jacBindex320, align 4, !tbaa !31
  %call321 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_jacobians319, i32 %336)
  store float* %call321, float** %jacB317, align 4, !tbaa !2
  %337 = bitcast i32* %i322 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %337) #9
  store i32 0, i32* %i322, align 4, !tbaa !6
  br label %for.cond323

for.cond323:                                      ; preds = %for.inc332, %if.then314
  %338 = load i32, i32* %i322, align 4, !tbaa !6
  %339 = load i32, i32* %ndofB279, align 4, !tbaa !6
  %cmp324 = icmp slt i32 %338, %339
  br i1 %cmp324, label %for.body326, label %for.cond.cleanup325

for.cond.cleanup325:                              ; preds = %for.cond323
  %340 = bitcast i32* %i322 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #9
  br label %for.end334

for.body326:                                      ; preds = %for.cond323
  %341 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call327 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %341)
  %342 = load i32, i32* %i322, align 4, !tbaa !6
  %arrayidx328 = getelementptr inbounds float, float* %call327, i32 %342
  %343 = load float, float* %arrayidx328, align 4, !tbaa !8
  %344 = load float*, float** %jacB317, align 4, !tbaa !2
  %345 = load i32, i32* %i322, align 4, !tbaa !6
  %arrayidx329 = getelementptr inbounds float, float* %344, i32 %345
  %346 = load float, float* %arrayidx329, align 4, !tbaa !8
  %mul330 = fmul float %343, %346
  %347 = load float, float* %rel_vel, align 4, !tbaa !8
  %add331 = fadd float %347, %mul330
  store float %add331, float* %rel_vel, align 4, !tbaa !8
  br label %for.inc332

for.inc332:                                       ; preds = %for.body326
  %348 = load i32, i32* %i322, align 4, !tbaa !6
  %inc333 = add nsw i32 %348, 1
  store i32 %inc333, i32* %i322, align 4, !tbaa !6
  br label %for.cond323

for.end334:                                       ; preds = %for.cond.cleanup325
  %349 = bitcast float** %jacB317 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %349) #9
  br label %if.end343

if.else335:                                       ; preds = %if.end312
  %350 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %tobool336 = icmp ne %class.btRigidBody* %350, null
  br i1 %tobool336, label %if.then337, label %if.end342

if.then337:                                       ; preds = %if.else335
  %351 = bitcast %class.btVector3* %ref.tmp338 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %351) #9
  %352 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp338, %class.btRigidBody* %352, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %353 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal2339 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %353, i32 0, i32 6
  %call340 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp338, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2339)
  %354 = load float, float* %rel_vel, align 4, !tbaa !8
  %add341 = fadd float %354, %call340
  store float %add341, float* %rel_vel, align 4, !tbaa !8
  %355 = bitcast %class.btVector3* %ref.tmp338 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %355) #9
  br label %if.end342

if.end342:                                        ; preds = %if.then337, %if.else335
  br label %if.end343

if.end343:                                        ; preds = %if.end342, %for.end334
  %356 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %356, i32 0, i32 6
  %357 = load float, float* %m_combinedFriction, align 4, !tbaa !68
  %358 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %358, i32 0, i32 12
  store float %357, float* %m_friction, align 4, !tbaa !16
  %359 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %360 = load float, float* %rel_vel, align 4, !tbaa !8
  %361 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %361, i32 0, i32 8
  %362 = load float, float* %m_combinedRestitution, align 4, !tbaa !70
  %call344 = call float @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff(%class.btSequentialImpulseConstraintSolver* %359, float %360, float %362)
  store float %call344, float* %restitution, align 4, !tbaa !8
  %363 = load float, float* %restitution, align 4, !tbaa !8
  %cmp345 = fcmp ole float %363, 0.000000e+00
  br i1 %cmp345, label %if.then346, label %if.end347

if.then346:                                       ; preds = %if.end343
  store float 0.000000e+00, float* %restitution, align 4, !tbaa !8
  br label %if.end347

if.end347:                                        ; preds = %if.then346, %if.end343
  %364 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %364) #9
  %365 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %365) #9
  %366 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %367 = bitcast %struct.btContactSolverInfo* %366 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %367, i32 0, i32 16
  %368 = load i32, i32* %m_solverMode, align 4, !tbaa !71
  %and = and i32 %368, 4
  %tobool348 = icmp ne i32 %and, 0
  br i1 %tobool348, label %if.then349, label %if.else405

if.then349:                                       ; preds = %if.end347
  %369 = load i8, i8* %isFriction.addr, align 1, !tbaa !62, !range !66
  %tobool350 = trunc i8 %369 to i1
  br i1 %tobool350, label %cond.true351, label %cond.false352

cond.true351:                                     ; preds = %if.then349
  br label %cond.end354

cond.false352:                                    ; preds = %if.then349
  %370 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %370, i32 0, i32 15
  %371 = load float, float* %m_appliedImpulse, align 4, !tbaa !72
  %372 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %373 = bitcast %struct.btContactSolverInfo* %372 to %struct.btContactSolverInfoData*
  %m_warmstartingFactor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %373, i32 0, i32 15
  %374 = load float, float* %m_warmstartingFactor, align 4, !tbaa !73
  %mul353 = fmul float %371, %374
  br label %cond.end354

cond.end354:                                      ; preds = %cond.false352, %cond.true351
  %cond355 = phi float [ 0.000000e+00, %cond.true351 ], [ %mul353, %cond.false352 ]
  %375 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse356 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %375, i32 0, i32 11
  store float %cond355, float* %m_appliedImpulse356, align 4, !tbaa !15
  %376 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse357 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %376, i32 0, i32 11
  %377 = load float, float* %m_appliedImpulse357, align 4, !tbaa !15
  %tobool358 = fcmp une float %377, 0.000000e+00
  br i1 %tobool358, label %if.then359, label %if.end404

if.then359:                                       ; preds = %cond.end354
  %378 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool360 = icmp ne %class.btMultiBody* %378, null
  br i1 %tobool360, label %if.then361, label %if.else368

if.then361:                                       ; preds = %if.then359
  %379 = bitcast float* %impulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %379) #9
  %380 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse362 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %380, i32 0, i32 11
  %381 = load float, float* %m_appliedImpulse362, align 4, !tbaa !15
  store float %381, float* %impulse, align 4, !tbaa !8
  %382 = bitcast float** %deltaV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %382) #9
  %m_data363 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse364 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data363, i32 0, i32 1
  %383 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex365 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %383, i32 0, i32 3
  %384 = load i32, i32* %m_jacAindex365, align 4, !tbaa !27
  %call366 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse364, i32 %384)
  store float* %call366, float** %deltaV, align 4, !tbaa !2
  %385 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %386 = load float*, float** %deltaV, align 4, !tbaa !2
  %387 = load float, float* %impulse, align 4, !tbaa !8
  call void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %385, float* %386, float %387)
  %388 = load float*, float** %deltaV, align 4, !tbaa !2
  %389 = load float, float* %impulse, align 4, !tbaa !8
  %390 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex367 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %390, i32 0, i32 0
  %391 = load i32, i32* %m_deltaVelAindex367, align 4, !tbaa !28
  %392 = load i32, i32* %ndofA278, align 4, !tbaa !6
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %388, float %389, i32 %391, i32 %392)
  %393 = bitcast float** %deltaV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #9
  %394 = bitcast float* %impulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %394) #9
  br label %if.end379

if.else368:                                       ; preds = %if.then359
  %395 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %tobool369 = icmp ne %class.btRigidBody* %395, null
  br i1 %tobool369, label %if.then370, label %if.end378

if.then370:                                       ; preds = %if.else368
  %396 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %397 = bitcast %class.btVector3* %ref.tmp371 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %397) #9
  %398 = bitcast %class.btVector3* %ref.tmp372 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %398) #9
  %399 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal1373 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %399, i32 0, i32 2
  %400 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %call374 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %400)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp372, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1373, %class.btVector3* nonnull align 4 dereferenceable(16) %call374)
  %401 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %call375 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %401)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp371, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp372, %class.btVector3* nonnull align 4 dereferenceable(16) %call375)
  %402 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentA376 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %402, i32 0, i32 8
  %403 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse377 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %403, i32 0, i32 11
  %404 = load float, float* %m_appliedImpulse377, align 4, !tbaa !15
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %396, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp371, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA376, float %404)
  %405 = bitcast %class.btVector3* %ref.tmp372 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %405) #9
  %406 = bitcast %class.btVector3* %ref.tmp371 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %406) #9
  br label %if.end378

if.end378:                                        ; preds = %if.then370, %if.else368
  br label %if.end379

if.end379:                                        ; preds = %if.end378, %if.then361
  %407 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool380 = icmp ne %class.btMultiBody* %407, null
  br i1 %tobool380, label %if.then381, label %if.else390

if.then381:                                       ; preds = %if.end379
  %408 = bitcast float* %impulse382 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %408) #9
  %409 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse383 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %409, i32 0, i32 11
  %410 = load float, float* %m_appliedImpulse383, align 4, !tbaa !15
  store float %410, float* %impulse382, align 4, !tbaa !8
  %411 = bitcast float** %deltaV384 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %411) #9
  %m_data385 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse386 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data385, i32 0, i32 1
  %412 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex387 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %412, i32 0, i32 7
  %413 = load i32, i32* %m_jacBindex387, align 4, !tbaa !31
  %call388 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse386, i32 %413)
  store float* %call388, float** %deltaV384, align 4, !tbaa !2
  %414 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %415 = load float*, float** %deltaV384, align 4, !tbaa !2
  %416 = load float, float* %impulse382, align 4, !tbaa !8
  call void @_ZN11btMultiBody13applyDeltaVeeEPKff(%class.btMultiBody* %414, float* %415, float %416)
  %417 = load float*, float** %deltaV384, align 4, !tbaa !2
  %418 = load float, float* %impulse382, align 4, !tbaa !8
  %419 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex389 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %419, i32 0, i32 4
  %420 = load i32, i32* %m_deltaVelBindex389, align 4, !tbaa !32
  %421 = load i32, i32* %ndofB279, align 4, !tbaa !6
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %417, float %418, i32 %420, i32 %421)
  %422 = bitcast float** %deltaV384 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %422) #9
  %423 = bitcast float* %impulse382 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %423) #9
  br label %if.end403

if.else390:                                       ; preds = %if.end379
  %424 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %tobool391 = icmp ne %class.btRigidBody* %424, null
  br i1 %tobool391, label %if.then392, label %if.end402

if.then392:                                       ; preds = %if.else390
  %425 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %426 = bitcast %class.btVector3* %ref.tmp393 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %426) #9
  %427 = bitcast %class.btVector3* %ref.tmp394 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %427) #9
  %428 = bitcast %class.btVector3* %ref.tmp395 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %428) #9
  %429 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal2396 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %429, i32 0, i32 6
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp395, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2396)
  %430 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %call397 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %430)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp394, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp395, %class.btVector3* nonnull align 4 dereferenceable(16) %call397)
  %431 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %call398 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %431)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp393, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp394, %class.btVector3* nonnull align 4 dereferenceable(16) %call398)
  %432 = bitcast %class.btVector3* %ref.tmp399 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %432) #9
  %433 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentB400 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %433, i32 0, i32 9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp399, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB400)
  %434 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse401 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %434, i32 0, i32 11
  %435 = load float, float* %m_appliedImpulse401, align 4, !tbaa !15
  %fneg = fneg float %435
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %425, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp393, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp399, float %fneg)
  %436 = bitcast %class.btVector3* %ref.tmp399 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %436) #9
  %437 = bitcast %class.btVector3* %ref.tmp395 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %437) #9
  %438 = bitcast %class.btVector3* %ref.tmp394 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %438) #9
  %439 = bitcast %class.btVector3* %ref.tmp393 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %439) #9
  br label %if.end402

if.end402:                                        ; preds = %if.then392, %if.else390
  br label %if.end403

if.end403:                                        ; preds = %if.end402, %if.then381
  br label %if.end404

if.end404:                                        ; preds = %if.end403, %cond.end354
  br label %if.end407

if.else405:                                       ; preds = %if.end347
  %440 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse406 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %440, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse406, align 4, !tbaa !15
  br label %if.end407

if.end407:                                        ; preds = %if.else405, %if.end404
  %441 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %441, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4, !tbaa !74
  %442 = bitcast float* %positionalError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %442) #9
  store float 0.000000e+00, float* %positionalError, align 4, !tbaa !8
  %443 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %443) #9
  %444 = load float, float* %restitution, align 4, !tbaa !8
  %445 = load float, float* %rel_vel, align 4, !tbaa !8
  %sub = fsub float %444, %445
  store float %sub, float* %velocityError, align 4, !tbaa !8
  %446 = bitcast float* %erp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %446) #9
  %447 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %448 = bitcast %struct.btContactSolverInfo* %447 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %448, i32 0, i32 9
  %449 = load float, float* %m_erp2, align 4, !tbaa !75
  store float %449, float* %erp, align 4, !tbaa !8
  %450 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %451 = bitcast %struct.btContactSolverInfo* %450 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %451, i32 0, i32 11
  %452 = load i32, i32* %m_splitImpulse, align 4, !tbaa !76
  %tobool408 = icmp ne i32 %452, 0
  br i1 %tobool408, label %lor.lhs.false, label %if.then410

lor.lhs.false:                                    ; preds = %if.end407
  %453 = load float, float* %penetration, align 4, !tbaa !8
  %454 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %455 = bitcast %struct.btContactSolverInfo* %454 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %455, i32 0, i32 12
  %456 = load float, float* %m_splitImpulsePenetrationThreshold, align 4, !tbaa !77
  %cmp409 = fcmp ogt float %453, %456
  br i1 %cmp409, label %if.then410, label %if.end411

if.then410:                                       ; preds = %lor.lhs.false, %if.end407
  %457 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %458 = bitcast %struct.btContactSolverInfo* %457 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %458, i32 0, i32 8
  %459 = load float, float* %m_erp, align 4, !tbaa !78
  store float %459, float* %erp, align 4, !tbaa !8
  br label %if.end411

if.end411:                                        ; preds = %if.then410, %lor.lhs.false
  %460 = load float, float* %penetration, align 4, !tbaa !8
  %cmp412 = fcmp ogt float %460, 0.000000e+00
  br i1 %cmp412, label %if.then413, label %if.else416

if.then413:                                       ; preds = %if.end411
  store float 0.000000e+00, float* %positionalError, align 4, !tbaa !8
  %461 = load float, float* %penetration, align 4, !tbaa !8
  %fneg414 = fneg float %461
  %462 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %463 = bitcast %struct.btContactSolverInfo* %462 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %463, i32 0, i32 3
  %464 = load float, float* %m_timeStep, align 4, !tbaa !79
  %div415 = fdiv float %fneg414, %464
  store float %div415, float* %velocityError, align 4, !tbaa !8
  br label %if.end421

if.else416:                                       ; preds = %if.end411
  %465 = load float, float* %penetration, align 4, !tbaa !8
  %fneg417 = fneg float %465
  %466 = load float, float* %erp, align 4, !tbaa !8
  %mul418 = fmul float %fneg417, %466
  %467 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %468 = bitcast %struct.btContactSolverInfo* %467 to %struct.btContactSolverInfoData*
  %m_timeStep419 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %468, i32 0, i32 3
  %469 = load float, float* %m_timeStep419, align 4, !tbaa !79
  %div420 = fdiv float %mul418, %469
  store float %div420, float* %positionalError, align 4, !tbaa !8
  br label %if.end421

if.end421:                                        ; preds = %if.else416, %if.then413
  %470 = bitcast float* %penetrationImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %470) #9
  %471 = load float, float* %positionalError, align 4, !tbaa !8
  %472 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv422 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %472, i32 0, i32 13
  %473 = load float, float* %m_jacDiagABInv422, align 4, !tbaa !34
  %mul423 = fmul float %471, %473
  store float %mul423, float* %penetrationImpulse, align 4, !tbaa !8
  %474 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %474) #9
  %475 = load float, float* %velocityError, align 4, !tbaa !8
  %476 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv424 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %476, i32 0, i32 13
  %477 = load float, float* %m_jacDiagABInv424, align 4, !tbaa !34
  %mul425 = fmul float %475, %477
  store float %mul425, float* %velocityImpulse, align 4, !tbaa !8
  %478 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %479 = bitcast %struct.btContactSolverInfo* %478 to %struct.btContactSolverInfoData*
  %m_splitImpulse426 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %479, i32 0, i32 11
  %480 = load i32, i32* %m_splitImpulse426, align 4, !tbaa !76
  %tobool427 = icmp ne i32 %480, 0
  br i1 %tobool427, label %lor.lhs.false428, label %if.then431

lor.lhs.false428:                                 ; preds = %if.end421
  %481 = load float, float* %penetration, align 4, !tbaa !8
  %482 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %483 = bitcast %struct.btContactSolverInfo* %482 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold429 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %483, i32 0, i32 12
  %484 = load float, float* %m_splitImpulsePenetrationThreshold429, align 4, !tbaa !77
  %cmp430 = fcmp ogt float %481, %484
  br i1 %cmp430, label %if.then431, label %if.else433

if.then431:                                       ; preds = %lor.lhs.false428, %if.end421
  %485 = load float, float* %penetrationImpulse, align 4, !tbaa !8
  %486 = load float, float* %velocityImpulse, align 4, !tbaa !8
  %add432 = fadd float %485, %486
  %487 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %487, i32 0, i32 14
  store float %add432, float* %m_rhs, align 4, !tbaa !24
  %488 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %488, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4, !tbaa !80
  br label %if.end436

if.else433:                                       ; preds = %lor.lhs.false428
  %489 = load float, float* %velocityImpulse, align 4, !tbaa !8
  %490 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhs434 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %490, i32 0, i32 14
  store float %489, float* %m_rhs434, align 4, !tbaa !24
  %491 = load float, float* %penetrationImpulse, align 4, !tbaa !8
  %492 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhsPenetration435 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %492, i32 0, i32 18
  store float %491, float* %m_rhsPenetration435, align 4, !tbaa !80
  br label %if.end436

if.end436:                                        ; preds = %if.else433, %if.then431
  %493 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %493, i32 0, i32 15
  store float 0.000000e+00, float* %m_cfm, align 4, !tbaa !25
  %494 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %494, i32 0, i32 16
  store float 0.000000e+00, float* %m_lowerLimit, align 4, !tbaa !17
  %495 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %495, i32 0, i32 17
  store float 1.000000e+10, float* %m_upperLimit, align 4, !tbaa !18
  %496 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %496) #9
  %497 = bitcast float* %penetrationImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %497) #9
  %498 = bitcast float* %erp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %498) #9
  %499 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %499) #9
  %500 = bitcast float* %positionalError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %500) #9
  %501 = bitcast i32* %ndofB279 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %501) #9
  %502 = bitcast i32* %ndofA278 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %502) #9
  %503 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %503) #9
  %504 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %504) #9
  %505 = bitcast float* %restitution to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %505) #9
  %506 = bitcast %class.btRigidBody** %rb1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %506) #9
  %507 = bitcast %class.btRigidBody** %rb0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %507) #9
  %508 = bitcast %struct.btSolverBody** %bodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %508) #9
  %509 = bitcast %struct.btSolverBody** %bodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %509) #9
  %510 = bitcast %class.btVector3** %pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %510) #9
  %511 = bitcast %class.btVector3** %pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %511) #9
  %512 = bitcast %class.btMultiBody** %multiBodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %512) #9
  %513 = bitcast %class.btMultiBody** %multiBodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %513) #9
  %514 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %514) #9
  %515 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %515) #9
  %call437 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #9
  %516 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %516) #9
  ret void
}

define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* returned %this, i8* %name) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  %name.addr = alloca i8*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  store i8* %name, i8** %name.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  %0 = load i8*, i8** %name.addr, align 4, !tbaa !2
  call void @_ZN15CProfileManager13Start_ProfileEPKc(i8* %0)
  ret %class.CProfileSample* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  ret %class.btVector3* %m_positionWorldOnA
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  ret %class.btVector3* %m_positionWorldOnB
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %this) #6 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 21
  %0 = load i32, i32* %m_companionId, align 4, !tbaa !43
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !42
  ret i32 %0
}

declare void @_ZNK11btMultiBody19fillContactJacobianEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float*, %class.btAlignedObjectArray.30* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17)) #2

declare void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody*, float*, float*, %class.btAlignedObjectArray.30* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17)) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !8
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !8
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !8
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !8
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !8
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !8
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  ret %class.btVector3* %m_angularFactor
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !8
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !81
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %0 = load float, float* %m_distance1, align 4, !tbaa !85
  ret float %0
}

define linkonce_odr hidden float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %m_real_buf, i32 0)
  ret float* %call
}

define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %1 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff(%class.btSequentialImpulseConstraintSolver*, float, float) #2

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_linearFactor
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  call void @_ZN15CProfileManager12Stop_ProfileEv()
  ret %class.CProfileSample* %this1
}

define hidden nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis, %class.btPersistentManifold* %manifold, i32 %frictionIndex, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %cp, %class.btCollisionObject* %colObj0, %class.btCollisionObject* %colObj1, float %relaxation, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, float %desiredVelocity, float %cfmSlip) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %frictionIndex.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %colObj0.addr = alloca %class.btCollisionObject*, align 4
  %colObj1.addr = alloca %class.btCollisionObject*, align 4
  %relaxation.addr = alloca float, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %solverConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %isFriction = alloca i8, align 1
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %fcB = alloca %class.btMultiBodyLinkCollider*, align 4
  %mbA = alloca %class.btMultiBody*, align 4
  %mbB = alloca %class.btMultiBody*, align 4
  %solverBodyIdA = alloca i32, align 4
  %solverBodyIdB = alloca i32, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  store i32 %frictionIndex, i32* %frictionIndex.addr, align 4, !tbaa !6
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %colObj0, %class.btCollisionObject** %colObj0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %colObj1, %class.btCollisionObject** %colObj1.addr, align 4, !tbaa !2
  store float %relaxation, float* %relaxation.addr, align 4, !tbaa !8
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4, !tbaa !8
  store float %cfmSlip, float* %cfmSlip.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #9
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.1, i32 0, i32 0))
  %1 = bitcast %struct.btMultiBodySolverConstraint** %solverConstraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.18* %m_multiBodyFrictionContactConstraints)
  store %struct.btMultiBodySolverConstraint* %call2, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %2 = load i32, i32* %frictionIndex.addr, align 4, !tbaa !6
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 0, i32 21
  store i32 %2, i32* %m_frictionIndex, align 4, !tbaa !12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isFriction) #9
  store i8 1, i8* %isFriction, align 1, !tbaa !62
  %4 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %5)
  %call4 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call3)
  store %class.btMultiBodyLinkCollider* %call4, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %6 = bitcast %class.btMultiBodyLinkCollider** %fcB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call5 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %7)
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call5)
  store %class.btMultiBodyLinkCollider* %call6, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %8 = bitcast %class.btMultiBody** %mbA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %9, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %m_multiBody = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %10, i32 0, i32 1
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody, align 4, !tbaa !35
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btMultiBody* [ %11, %cond.true ], [ null, %cond.false ]
  store %class.btMultiBody* %cond, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %12 = bitcast %class.btMultiBody** %mbB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %13, null
  br i1 %tobool7, label %cond.true8, label %cond.false10

cond.true8:                                       ; preds = %cond.end
  %14 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %m_multiBody9 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %14, i32 0, i32 1
  %15 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody9, align 4, !tbaa !35
  br label %cond.end11

cond.false10:                                     ; preds = %cond.end
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false10, %cond.true8
  %cond12 = phi %class.btMultiBody* [ %15, %cond.true8 ], [ null, %cond.false10 ]
  store %class.btMultiBody* %cond12, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %16 = bitcast i32* %solverBodyIdA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %tobool13 = icmp ne %class.btMultiBody* %17, null
  br i1 %tobool13, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %cond.end11
  br label %cond.end17

cond.false15:                                     ; preds = %cond.end11
  %18 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0.addr, align 4, !tbaa !2
  %20 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %21 = bitcast %struct.btContactSolverInfo* %20 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %21, i32 0, i32 3
  %22 = load float, float* %m_timeStep, align 4, !tbaa !79
  %call16 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %18, %class.btCollisionObject* nonnull align 4 dereferenceable(264) %19, float %22)
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false15, %cond.true14
  %cond18 = phi i32 [ -1, %cond.true14 ], [ %call16, %cond.false15 ]
  store i32 %cond18, i32* %solverBodyIdA, align 4, !tbaa !6
  %23 = bitcast i32* %solverBodyIdB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %tobool19 = icmp ne %class.btMultiBody* %24, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end24

cond.false21:                                     ; preds = %cond.end17
  %25 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1.addr, align 4, !tbaa !2
  %27 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %28 = bitcast %struct.btContactSolverInfo* %27 to %struct.btContactSolverInfoData*
  %m_timeStep22 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %28, i32 0, i32 3
  %29 = load float, float* %m_timeStep22, align 4, !tbaa !79
  %call23 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %25, %class.btCollisionObject* nonnull align 4 dereferenceable(264) %26, float %29)
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false21, %cond.true20
  %cond25 = phi i32 [ -1, %cond.true20 ], [ %call23, %cond.false21 ]
  store i32 %cond25, i32* %solverBodyIdB, align 4, !tbaa !6
  %30 = load i32, i32* %solverBodyIdA, align 4, !tbaa !6
  %31 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %31, i32 0, i32 22
  store i32 %30, i32* %m_solverBodyIdA, align 4, !tbaa !29
  %32 = load i32, i32* %solverBodyIdB, align 4, !tbaa !6
  %33 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %33, i32 0, i32 25
  store i32 %32, i32* %m_solverBodyIdB, align 4, !tbaa !33
  %34 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %35 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %35, i32 0, i32 23
  store %class.btMultiBody* %34, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !26
  %36 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %tobool26 = icmp ne %class.btMultiBody* %36, null
  br i1 %tobool26, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end24
  %37 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %m_link = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %37, i32 0, i32 2
  %38 = load i32, i32* %m_link, align 4, !tbaa !86
  %39 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %39, i32 0, i32 24
  store i32 %38, i32* %m_linkA, align 4, !tbaa !64
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end24
  %40 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %41 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %41, i32 0, i32 26
  store %class.btMultiBody* %40, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !30
  %42 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %tobool27 = icmp ne %class.btMultiBody* %42, null
  br i1 %tobool27, label %if.then28, label %if.end30

if.then28:                                        ; preds = %if.end
  %43 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %m_link29 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %43, i32 0, i32 2
  %44 = load i32, i32* %m_link29, align 4, !tbaa !86
  %45 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %45, i32 0, i32 27
  store i32 %44, i32* %m_linkB, align 4, !tbaa !65
  br label %if.end30

if.end30:                                         ; preds = %if.then28, %if.end
  %46 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %47 = bitcast %class.btManifoldPoint* %46 to i8*
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %49 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 19
  %m_originalContactPoint = bitcast %union.anon.21* %49 to i8**
  store i8* %47, i8** %m_originalContactPoint, align 4, !tbaa !38
  %50 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %51 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4, !tbaa !2
  %52 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %53 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %54 = load i8, i8* %isFriction, align 1, !tbaa !62, !range !66
  %tobool31 = trunc i8 %54 to i1
  %55 = load float, float* %desiredVelocity.addr, align 4, !tbaa !8
  %56 = load float, float* %cfmSlip.addr, align 4, !tbaa !8
  call void @_ZN27btMultiBodyConstraintSolver31setupMultiBodyContactConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %50, %class.btVector3* nonnull align 4 dereferenceable(16) %51, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %52, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %53, float* nonnull align 4 dereferenceable(4) %relaxation.addr, i1 zeroext %tobool31, float %55, float %56)
  %57 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %58 = bitcast i32* %solverBodyIdB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  %59 = bitcast i32* %solverBodyIdA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = bitcast %class.btMultiBody** %mbB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #9
  %61 = bitcast %class.btMultiBody** %mbA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #9
  %62 = bitcast %class.btMultiBodyLinkCollider** %fcB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #9
  %63 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isFriction) #9
  %64 = bitcast %struct.btMultiBodySolverConstraint** %solverConstraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #9
  %call32 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #9
  %65 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %65) #9
  ret %struct.btMultiBodySolverConstraint* %57
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.18* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.18* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.18* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.18* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !19
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !19
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %4 = load i32, i32* %sz, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  %5 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %colObj) #6 comdat {
entry:
  %retval = alloca %class.btMultiBodyLinkCollider*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 64
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionObject* %1 to %class.btMultiBodyLinkCollider*
  store %class.btMultiBodyLinkCollider* %2, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btMultiBodyLinkCollider* null, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %retval, align 4
  ret %class.btMultiBodyLinkCollider* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !87
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4, !tbaa !89
  ret %class.btCollisionObject* %0
}

declare i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject* nonnull align 4 dereferenceable(264), float) #2

define hidden void @_ZN27btMultiBodyConstraintSolver23convertMultiBodyContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btMultiBodyConstraintSolver* %this, %class.btPersistentManifold* %manifold, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %fcB = alloca %class.btMultiBodyLinkCollider*, align 4
  %mbA = alloca %class.btMultiBody*, align 4
  %mbB = alloca %class.btMultiBody*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %solverBodyIdA = alloca i32, align 4
  %solverBodyIdB = alloca i32, align 4
  %solverBodyA = alloca %struct.btSolverBody*, align 4
  %solverBodyB = alloca %struct.btSolverBody*, align 4
  %rollingFriction = alloca i32, align 4
  %j = alloca i32, align 4
  %cp = alloca %class.btManifoldPoint*, align 4
  %relaxation = alloca float, align 4
  %frictionIndex = alloca i32, align 4
  %solverConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %isFriction = alloca i8, align 1
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %call2 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call)
  store %class.btMultiBodyLinkCollider* %call2, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %2 = bitcast %class.btMultiBodyLinkCollider** %fcB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %3)
  %call4 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call3)
  store %class.btMultiBodyLinkCollider* %call4, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %4 = bitcast %class.btMultiBody** %mbA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %5, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %m_multiBody = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %6, i32 0, i32 1
  %7 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody, align 4, !tbaa !35
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btMultiBody* [ %7, %cond.true ], [ null, %cond.false ]
  store %class.btMultiBody* %cond, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %8 = bitcast %class.btMultiBody** %mbB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %tobool5 = icmp ne %class.btMultiBodyLinkCollider* %9, null
  br i1 %tobool5, label %cond.true6, label %cond.false8

cond.true6:                                       ; preds = %cond.end
  %10 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %m_multiBody7 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %10, i32 0, i32 1
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody7, align 4, !tbaa !35
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true6
  %cond10 = phi %class.btMultiBody* [ %11, %cond.true6 ], [ null, %cond.false8 ]
  store %class.btMultiBody* %cond10, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %12 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  store %class.btCollisionObject* null, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %13 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  store %class.btCollisionObject* null, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %14 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call11 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %14)
  store %class.btCollisionObject* %call11, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %15 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call12 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %15)
  store %class.btCollisionObject* %call12, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %16 = bitcast i32* %solverBodyIdA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %tobool13 = icmp ne %class.btMultiBody* %17, null
  br i1 %tobool13, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %cond.end9
  br label %cond.end17

cond.false15:                                     ; preds = %cond.end9
  %18 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %20 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %21 = bitcast %struct.btContactSolverInfo* %20 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %21, i32 0, i32 3
  %22 = load float, float* %m_timeStep, align 4, !tbaa !79
  %call16 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %18, %class.btCollisionObject* nonnull align 4 dereferenceable(264) %19, float %22)
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false15, %cond.true14
  %cond18 = phi i32 [ -1, %cond.true14 ], [ %call16, %cond.false15 ]
  store i32 %cond18, i32* %solverBodyIdA, align 4, !tbaa !6
  %23 = bitcast i32* %solverBodyIdB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %tobool19 = icmp ne %class.btMultiBody* %24, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end24

cond.false21:                                     ; preds = %cond.end17
  %25 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %27 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %28 = bitcast %struct.btContactSolverInfo* %27 to %struct.btContactSolverInfoData*
  %m_timeStep22 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %28, i32 0, i32 3
  %29 = load float, float* %m_timeStep22, align 4, !tbaa !79
  %call23 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %25, %class.btCollisionObject* nonnull align 4 dereferenceable(264) %26, float %29)
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false21, %cond.true20
  %cond25 = phi i32 [ -1, %cond.true20 ], [ %call23, %cond.false21 ]
  store i32 %cond25, i32* %solverBodyIdB, align 4, !tbaa !6
  %30 = bitcast %struct.btSolverBody** %solverBodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %31 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %tobool26 = icmp ne %class.btMultiBody* %31, null
  br i1 %tobool26, label %cond.true27, label %cond.false28

cond.true27:                                      ; preds = %cond.end24
  br label %cond.end30

cond.false28:                                     ; preds = %cond.end24
  %32 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %32, i32 0, i32 1
  %33 = load i32, i32* %solverBodyIdA, align 4, !tbaa !6
  %call29 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %33)
  br label %cond.end30

cond.end30:                                       ; preds = %cond.false28, %cond.true27
  %cond31 = phi %struct.btSolverBody* [ null, %cond.true27 ], [ %call29, %cond.false28 ]
  store %struct.btSolverBody* %cond31, %struct.btSolverBody** %solverBodyA, align 4, !tbaa !2
  %34 = bitcast %struct.btSolverBody** %solverBodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %tobool32 = icmp ne %class.btMultiBody* %35, null
  br i1 %tobool32, label %cond.true33, label %cond.false34

cond.true33:                                      ; preds = %cond.end30
  br label %cond.end37

cond.false34:                                     ; preds = %cond.end30
  %36 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool35 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %36, i32 0, i32 1
  %37 = load i32, i32* %solverBodyIdB, align 4, !tbaa !6
  %call36 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool35, i32 %37)
  br label %cond.end37

cond.end37:                                       ; preds = %cond.false34, %cond.true33
  %cond38 = phi %struct.btSolverBody* [ null, %cond.true33 ], [ %call36, %cond.false34 ]
  store %struct.btSolverBody* %cond38, %struct.btSolverBody** %solverBodyB, align 4, !tbaa !2
  %38 = bitcast i32* %rollingFriction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  store i32 1, i32* %rollingFriction, align 4, !tbaa !6
  %39 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end37
  %40 = load i32, i32* %j, align 4, !tbaa !6
  %41 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call39 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %41)
  %cmp = icmp slt i32 %40, %call39
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %42 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %43 = bitcast %class.btManifoldPoint** %cp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  %44 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %call40 = call nonnull align 4 dereferenceable(184) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %44, i32 %45)
  store %class.btManifoldPoint* %call40, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %46 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %call41 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %46)
  %47 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %call42 = call float @_ZNK20btPersistentManifold29getContactProcessingThresholdEv(%class.btPersistentManifold* %47)
  %cmp43 = fcmp ole float %call41, %call42
  br i1 %cmp43, label %if.then, label %if.end92

if.then:                                          ; preds = %for.body
  %48 = bitcast float* %relaxation to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #9
  %49 = bitcast i32* %frictionIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call44 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %m_multiBodyNormalContactConstraints)
  store i32 %call44, i32* %frictionIndex, align 4, !tbaa !6
  %50 = bitcast %struct.btMultiBodySolverConstraint** %solverConstraint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #9
  %m_multiBodyNormalContactConstraints45 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call46 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.18* %m_multiBodyNormalContactConstraints45)
  store %struct.btMultiBodySolverConstraint* %call46, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %51 = bitcast %class.btRigidBody** %rb0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #9
  %52 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %call47 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %52)
  store %class.btRigidBody* %call47, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %53 = bitcast %class.btRigidBody** %rb1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #9
  %54 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %call48 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %54)
  store %class.btRigidBody* %call48, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %55 = load i32, i32* %solverBodyIdA, align 4, !tbaa !6
  %56 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %56, i32 0, i32 22
  store i32 %55, i32* %m_solverBodyIdA, align 4, !tbaa !29
  %57 = load i32, i32* %solverBodyIdB, align 4, !tbaa !6
  %58 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %58, i32 0, i32 25
  store i32 %57, i32* %m_solverBodyIdB, align 4, !tbaa !33
  %59 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %60 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %60, i32 0, i32 23
  store %class.btMultiBody* %59, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !26
  %61 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4, !tbaa !2
  %tobool49 = icmp ne %class.btMultiBody* %61, null
  br i1 %tobool49, label %if.then50, label %if.end

if.then50:                                        ; preds = %if.then
  %62 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %m_link = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %62, i32 0, i32 2
  %63 = load i32, i32* %m_link, align 4, !tbaa !86
  %64 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %64, i32 0, i32 24
  store i32 %63, i32* %m_linkA, align 4, !tbaa !64
  br label %if.end

if.end:                                           ; preds = %if.then50, %if.then
  %65 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %66 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %66, i32 0, i32 26
  store %class.btMultiBody* %65, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !30
  %67 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4, !tbaa !2
  %tobool51 = icmp ne %class.btMultiBody* %67, null
  br i1 %tobool51, label %if.then52, label %if.end54

if.then52:                                        ; preds = %if.end
  %68 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %m_link53 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %68, i32 0, i32 2
  %69 = load i32, i32* %m_link53, align 4, !tbaa !86
  %70 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %70, i32 0, i32 27
  store i32 %69, i32* %m_linkB, align 4, !tbaa !65
  br label %if.end54

if.end54:                                         ; preds = %if.then52, %if.end
  %71 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %72 = bitcast %class.btManifoldPoint* %71 to i8*
  %73 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %74 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %73, i32 0, i32 19
  %m_originalContactPoint = bitcast %union.anon.21* %74 to i8**
  store i8* %72, i8** %m_originalContactPoint, align 4, !tbaa !38
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isFriction) #9
  store i8 0, i8* %isFriction, align 1, !tbaa !62
  %75 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %76 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %76, i32 0, i32 4
  %77 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %78 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %79 = load i8, i8* %isFriction, align 1, !tbaa !62, !range !66
  %tobool55 = trunc i8 %79 to i1
  call void @_ZN27btMultiBodyConstraintSolver31setupMultiBodyContactConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %75, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %77, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %78, float* nonnull align 4 dereferenceable(4) %relaxation, i1 zeroext %tobool55, float 0.000000e+00, float 0.000000e+00)
  %80 = load i32, i32* %frictionIndex, align 4, !tbaa !6
  %81 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %81, i32 0, i32 21
  store i32 %80, i32* %m_frictionIndex, align 4, !tbaa !12
  %82 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %83 = bitcast %struct.btContactSolverInfo* %82 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %83, i32 0, i32 16
  %84 = load i32, i32* %m_solverMode, align 4, !tbaa !71
  %and = and i32 %84, 32
  %tobool56 = icmp ne i32 %and, 0
  br i1 %tobool56, label %lor.lhs.false, label %if.then58

lor.lhs.false:                                    ; preds = %if.end54
  %85 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionInitialized = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %85, i32 0, i32 14
  %86 = load i8, i8* %m_lateralFrictionInitialized, align 4, !tbaa !90, !range !66
  %tobool57 = trunc i8 %86 to i1
  br i1 %tobool57, label %if.else, label %if.then58

if.then58:                                        ; preds = %lor.lhs.false, %if.end54
  %87 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_normalWorldOnB59 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %87, i32 0, i32 4
  %88 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %88, i32 0, i32 23
  %89 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %89, i32 0, i32 24
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB59, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2)
  %90 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %91 = bitcast %struct.btContactSolverInfo* %90 to %struct.btContactSolverInfoData*
  %m_solverMode60 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %91, i32 0, i32 16
  %92 = load i32, i32* %m_solverMode60, align 4, !tbaa !71
  %and61 = and i32 %92, 16
  %tobool62 = icmp ne i32 %and61, 0
  br i1 %tobool62, label %if.then63, label %if.end68

if.then63:                                        ; preds = %if.then58
  %93 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %94 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir264 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %94, i32 0, i32 24
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %93, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir264, i32 1)
  %95 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %96 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir265 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %96, i32 0, i32 24
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %95, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir265, i32 1)
  %97 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir266 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %97, i32 0, i32 24
  %98 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %99 = load i32, i32* %frictionIndex, align 4, !tbaa !6
  %100 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %101 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %102 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %103 = load float, float* %relaxation, align 4, !tbaa !8
  %104 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %call67 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir266, %class.btPersistentManifold* %98, i32 %99, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %100, %class.btCollisionObject* %101, %class.btCollisionObject* %102, float %103, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %104, float 0.000000e+00, float 0.000000e+00)
  br label %if.end68

if.end68:                                         ; preds = %if.then63, %if.then58
  %105 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %106 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir169 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %106, i32 0, i32 23
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %105, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir169, i32 1)
  %107 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %108 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir170 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %108, i32 0, i32 23
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %107, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir170, i32 1)
  %109 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir171 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %109, i32 0, i32 23
  %110 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %111 = load i32, i32* %frictionIndex, align 4, !tbaa !6
  %112 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %113 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %114 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %115 = load float, float* %relaxation, align 4, !tbaa !8
  %116 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %call72 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir171, %class.btPersistentManifold* %110, i32 %111, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %112, %class.btCollisionObject* %113, %class.btCollisionObject* %114, float %115, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %116, float 0.000000e+00, float 0.000000e+00)
  %117 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %118 = bitcast %struct.btContactSolverInfo* %117 to %struct.btContactSolverInfoData*
  %m_solverMode73 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %118, i32 0, i32 16
  %119 = load i32, i32* %m_solverMode73, align 4, !tbaa !71
  %and74 = and i32 %119, 16
  %tobool75 = icmp ne i32 %and74, 0
  br i1 %tobool75, label %land.lhs.true, label %if.end81

land.lhs.true:                                    ; preds = %if.end68
  %120 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %121 = bitcast %struct.btContactSolverInfo* %120 to %struct.btContactSolverInfoData*
  %m_solverMode76 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %121, i32 0, i32 16
  %122 = load i32, i32* %m_solverMode76, align 4, !tbaa !71
  %and77 = and i32 %122, 64
  %tobool78 = icmp ne i32 %and77, 0
  br i1 %tobool78, label %if.then79, label %if.end81

if.then79:                                        ; preds = %land.lhs.true
  %123 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionInitialized80 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %123, i32 0, i32 14
  store i8 1, i8* %m_lateralFrictionInitialized80, align 4, !tbaa !90
  br label %if.end81

if.end81:                                         ; preds = %if.then79, %land.lhs.true, %if.end68
  br label %if.end91

if.else:                                          ; preds = %lor.lhs.false
  %124 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir182 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %124, i32 0, i32 23
  %125 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %126 = load i32, i32* %frictionIndex, align 4, !tbaa !6
  %127 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %128 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %129 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %130 = load float, float* %relaxation, align 4, !tbaa !8
  %131 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %132 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %132, i32 0, i32 18
  %133 = load float, float* %m_contactMotion1, align 4, !tbaa !91
  %134 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_contactCFM1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %134, i32 0, i32 20
  %135 = load float, float* %m_contactCFM1, align 4, !tbaa !92
  %call83 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir182, %class.btPersistentManifold* %125, i32 %126, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %127, %class.btCollisionObject* %128, %class.btCollisionObject* %129, float %130, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %131, float %133, float %135)
  %136 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %137 = bitcast %struct.btContactSolverInfo* %136 to %struct.btContactSolverInfoData*
  %m_solverMode84 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %137, i32 0, i32 16
  %138 = load i32, i32* %m_solverMode84, align 4, !tbaa !71
  %and85 = and i32 %138, 16
  %tobool86 = icmp ne i32 %and85, 0
  br i1 %tobool86, label %if.then87, label %if.end90

if.then87:                                        ; preds = %if.else
  %139 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_lateralFrictionDir288 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %139, i32 0, i32 24
  %140 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4, !tbaa !2
  %141 = load i32, i32* %frictionIndex, align 4, !tbaa !6
  %142 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %143 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4, !tbaa !2
  %144 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4, !tbaa !2
  %145 = load float, float* %relaxation, align 4, !tbaa !8
  %146 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %147 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %147, i32 0, i32 19
  %148 = load float, float* %m_contactMotion2, align 4, !tbaa !93
  %149 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4, !tbaa !2
  %m_contactCFM2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %149, i32 0, i32 21
  %150 = load float, float* %m_contactCFM2, align 4, !tbaa !94
  %call89 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir288, %class.btPersistentManifold* %140, i32 %141, %class.btManifoldPoint* nonnull align 4 dereferenceable(184) %142, %class.btCollisionObject* %143, %class.btCollisionObject* %144, float %145, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %146, float %148, float %150)
  br label %if.end90

if.end90:                                         ; preds = %if.then87, %if.else
  %151 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %151, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !15
  %152 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4, !tbaa !2
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %152, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4, !tbaa !74
  br label %if.end91

if.end91:                                         ; preds = %if.end90, %if.end81
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isFriction) #9
  %153 = bitcast %class.btRigidBody** %rb1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #9
  %154 = bitcast %class.btRigidBody** %rb0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #9
  %155 = bitcast %struct.btMultiBodySolverConstraint** %solverConstraint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #9
  %156 = bitcast i32* %frictionIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #9
  %157 = bitcast float* %relaxation to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #9
  br label %if.end92

if.end92:                                         ; preds = %if.end91, %for.body
  %158 = bitcast %class.btManifoldPoint** %cp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end92
  %159 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %159, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %160 = bitcast i32* %rollingFriction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #9
  %161 = bitcast %struct.btSolverBody** %solverBodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #9
  %162 = bitcast %struct.btSolverBody** %solverBodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #9
  %163 = bitcast i32* %solverBodyIdB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #9
  %164 = bitcast i32* %solverBodyIdA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #9
  %165 = bitcast %class.btCollisionObject** %colObj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #9
  %166 = bitcast %class.btCollisionObject** %colObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #9
  %167 = bitcast %class.btMultiBody** %mbB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #9
  %168 = bitcast %class.btMultiBody** %mbA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #9
  %169 = bitcast %class.btMultiBodyLinkCollider** %fcB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #9
  %170 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4, !tbaa !95
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %this, i32 %index) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  ret %class.btManifoldPoint* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK20btPersistentManifold29getContactProcessingThresholdEv(%class.btPersistentManifold* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 6
  %0 = load float, float* %m_contactProcessingThreshold, align 4, !tbaa !96
  ret float %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #6 comdat {
entry:
  %retval = alloca %class.btRigidBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionObject* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btRigidBody* null, %class.btRigidBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btRigidBody*, %class.btRigidBody** %retval, align 4
  ret %class.btRigidBody* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #4 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %4 = load float, float* %arrayidx3, align 4, !tbaa !8
  %5 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %6 = load float, float* %arrayidx5, align 4, !tbaa !8
  %mul = fmul float %4, %6
  %7 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %10 = load float, float* %arrayidx9, align 4, !tbaa !8
  %mul10 = fmul float %8, %10
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4, !tbaa !8
  %11 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load float, float* %a, align 4, !tbaa !8
  %call11 = call float @_Z6btSqrtf(float %12)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4, !tbaa !8
  %14 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %15 = load float, float* %arrayidx15, align 4, !tbaa !8
  %fneg = fneg float %15
  %16 = load float, float* %k, align 4, !tbaa !8
  %mul16 = fmul float %fneg, %16
  %17 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4, !tbaa !8
  %18 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %19 = load float, float* %arrayidx20, align 4, !tbaa !8
  %20 = load float, float* %k, align 4, !tbaa !8
  %mul21 = fmul float %19, %20
  %21 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4, !tbaa !8
  %22 = load float, float* %a, align 4, !tbaa !8
  %23 = load float, float* %k, align 4, !tbaa !8
  %mul24 = fmul float %22, %23
  %24 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4, !tbaa !8
  %25 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %26 = load float, float* %arrayidx28, align 4, !tbaa !8
  %fneg29 = fneg float %26
  %27 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %28 = load float, float* %arrayidx31, align 4, !tbaa !8
  %mul32 = fmul float %fneg29, %28
  %29 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %29)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4, !tbaa !8
  %30 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %31 = load float, float* %arrayidx36, align 4, !tbaa !8
  %32 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %33 = load float, float* %arrayidx38, align 4, !tbaa !8
  %mul39 = fmul float %31, %33
  %34 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %34)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4, !tbaa !8
  %35 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %37 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %39 = load float, float* %arrayidx44, align 4, !tbaa !8
  %40 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %41 = load float, float* %arrayidx46, align 4, !tbaa !8
  %mul47 = fmul float %39, %41
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %43 = load float, float* %arrayidx49, align 4, !tbaa !8
  %44 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %44)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %45 = load float, float* %arrayidx51, align 4, !tbaa !8
  %mul52 = fmul float %43, %45
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4, !tbaa !8
  %46 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #9
  %47 = load float, float* %a42, align 4, !tbaa !8
  %call55 = call float @_Z6btSqrtf(float %47)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4, !tbaa !8
  %48 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %49 = load float, float* %arrayidx58, align 4, !tbaa !8
  %fneg59 = fneg float %49
  %50 = load float, float* %k54, align 4, !tbaa !8
  %mul60 = fmul float %fneg59, %50
  %51 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4, !tbaa !8
  %52 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %52)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %53 = load float, float* %arrayidx64, align 4, !tbaa !8
  %54 = load float, float* %k54, align 4, !tbaa !8
  %mul65 = fmul float %53, %54
  %55 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4, !tbaa !8
  %56 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %56)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4, !tbaa !8
  %57 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %57)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %58 = load float, float* %arrayidx71, align 4, !tbaa !8
  %fneg72 = fneg float %58
  %59 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %59)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %60 = load float, float* %arrayidx74, align 4, !tbaa !8
  %mul75 = fmul float %fneg72, %60
  %61 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %61)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4, !tbaa !8
  %62 = load %class.btVector3*, %class.btVector3** %n.addr, align 4, !tbaa !2
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %62)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %63 = load float, float* %arrayidx79, align 4, !tbaa !8
  %64 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %64)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %65 = load float, float* %arrayidx81, align 4, !tbaa !8
  %mul82 = fmul float %63, %65
  %66 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %66)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4, !tbaa !8
  %67 = load float, float* %a42, align 4, !tbaa !8
  %68 = load float, float* %k54, align 4, !tbaa !8
  %mul85 = fmul float %67, %68
  %69 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %69)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4, !tbaa !8
  %70 = bitcast float* %k54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  %71 = bitcast float* %a42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject*, %class.btVector3* nonnull align 4 dereferenceable(16), i32) #2

define hidden void @_ZN27btMultiBodyConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo(%class.btMultiBodyConstraintSolver* %this, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %i = alloca i32, align 4
  %manifold2 = alloca %class.btPersistentManifold*, align 4
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %fcB = alloca %class.btMultiBodyLinkCollider*, align 4
  %i7 = alloca i32, align 4
  %c = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !6
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %numManifolds.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %class.btPersistentManifold** %manifold2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %6, i32 %7
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4, !tbaa !2
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %manifold2, align 4, !tbaa !2
  %9 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold2, align 4, !tbaa !2
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %10)
  %call3 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call)
  store %class.btMultiBodyLinkCollider* %call3, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %11 = bitcast %class.btMultiBodyLinkCollider** %fcB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold2, align 4, !tbaa !2
  %call4 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %12)
  %call5 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call4)
  store %class.btMultiBodyLinkCollider* %call5, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %13, null
  br i1 %tobool, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body
  %14 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4, !tbaa !2
  %tobool6 = icmp ne %class.btMultiBodyLinkCollider* %14, null
  br i1 %tobool6, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %15 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %16 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold2, align 4, !tbaa !2
  %17 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  call void @_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %15, %class.btPersistentManifold* %16, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %17)
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %for.body
  %18 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold2, align 4, !tbaa !2
  %19 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  call void @_ZN27btMultiBodyConstraintSolver23convertMultiBodyContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btMultiBodyConstraintSolver* %this1, %class.btPersistentManifold* %18, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %19)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %20 = bitcast %class.btMultiBodyLinkCollider** %fcB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast %class.btMultiBodyLinkCollider** %fcA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast %class.btPersistentManifold** %manifold2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %24 = bitcast i32* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  store i32 0, i32* %i7, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc16, %for.end
  %25 = load i32, i32* %i7, align 4, !tbaa !6
  %m_tmpNumMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 6
  %26 = load i32, i32* %m_tmpNumMultiBodyConstraints, align 4, !tbaa !97
  %cmp9 = icmp slt i32 %25, %26
  br i1 %cmp9, label %for.body11, label %for.cond.cleanup10

for.cond.cleanup10:                               ; preds = %for.cond8
  %27 = bitcast i32* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  br label %for.end18

for.body11:                                       ; preds = %for.cond8
  %28 = bitcast %class.btMultiBodyConstraint** %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  %m_tmpMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 5
  %29 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_tmpMultiBodyConstraints, align 4, !tbaa !100
  %30 = load i32, i32* %i7, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %29, i32 %30
  %31 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx12, align 4, !tbaa !2
  store %class.btMultiBodyConstraint* %31, %class.btMultiBodyConstraint** %c, align 4, !tbaa !2
  %32 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %32, i32 0, i32 1
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_solverBodyPool = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 6
  store %class.btAlignedObjectArray* %m_tmpSolverBodyPool, %class.btAlignedObjectArray** %m_solverBodyPool, align 4, !tbaa !101
  %33 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_fixedBodyId = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %33, i32 0, i32 11
  %34 = load i32, i32* %m_fixedBodyId, align 4, !tbaa !102
  %m_data13 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_fixedBodyId14 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data13, i32 0, i32 7
  store i32 %34, i32* %m_fixedBodyId14, align 4, !tbaa !111
  %35 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %c, align 4, !tbaa !2
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %m_data15 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %36 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %37 = bitcast %class.btMultiBodyConstraint* %35 to void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.18*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)***
  %vtable = load void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.18*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)**, void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.18*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)*** %37, align 4, !tbaa !112
  %vfn = getelementptr inbounds void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.18*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)*, void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.18*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)** %vtable, i64 4
  %38 = load void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.18*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)*, void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.18*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)** %vfn, align 4
  call void %38(%class.btMultiBodyConstraint* %35, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %m_multiBodyNonContactConstraints, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %m_data15, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %36)
  %39 = bitcast %class.btMultiBodyConstraint** %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  br label %for.inc16

for.inc16:                                        ; preds = %for.body11
  %40 = load i32, i32* %i7, align 4, !tbaa !6
  %inc17 = add nsw i32 %40, 1
  store i32 %inc17, i32* %i7, align 4, !tbaa !6
  br label %for.cond8

for.end18:                                        ; preds = %for.cond.cleanup10
  %41 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold*, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84)) #2

define hidden float @_ZN27btMultiBodyConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btMultiBodyConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifold, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %info, %class.btIDebugDraw* %debugDrawer, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifold.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %info.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  store i32 %numBodies, i32* %numBodies.addr, align 4, !tbaa !6
  store %class.btPersistentManifold** %manifold, %class.btPersistentManifold*** %manifold.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !6
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  store i32 %numConstraints, i32* %numConstraints.addr, align 4, !tbaa !6
  store %struct.btContactSolverInfo* %info, %struct.btContactSolverInfo** %info.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %2 = load i32, i32* %numBodies.addr, align 4, !tbaa !6
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold.addr, align 4, !tbaa !2
  %4 = load i32, i32* %numManifolds.addr, align 4, !tbaa !6
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  %6 = load i32, i32* %numConstraints.addr, align 4, !tbaa !6
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %info.addr, align 4, !tbaa !2
  %8 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %9 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %call = call float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver* %0, %class.btCollisionObject** %1, i32 %2, %class.btPersistentManifold** %3, i32 %4, %class.btTypedConstraint** %5, i32 %6, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %7, %class.btIDebugDraw* %8, %class.btDispatcher* %9)
  ret float %call
}

declare float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*, %class.btDispatcher*) unnamed_addr #2

define hidden void @_ZN27btMultiBodyConstraintSolver19solveMultiBodyGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiPP21btMultiBodyConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btMultiBodyConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifold, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %class.btMultiBodyConstraint** %multiBodyConstraints, i32 %numMultiBodyConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %info, %class.btIDebugDraw* %debugDrawer, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifold.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %multiBodyConstraints.addr = alloca %class.btMultiBodyConstraint**, align 4
  %numMultiBodyConstraints.addr = alloca i32, align 4
  %info.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  store i32 %numBodies, i32* %numBodies.addr, align 4, !tbaa !6
  store %class.btPersistentManifold** %manifold, %class.btPersistentManifold*** %manifold.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !6
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  store i32 %numConstraints, i32* %numConstraints.addr, align 4, !tbaa !6
  store %class.btMultiBodyConstraint** %multiBodyConstraints, %class.btMultiBodyConstraint*** %multiBodyConstraints.addr, align 4, !tbaa !2
  store i32 %numMultiBodyConstraints, i32* %numMultiBodyConstraints.addr, align 4, !tbaa !6
  store %struct.btContactSolverInfo* %info, %struct.btContactSolverInfo** %info.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %multiBodyConstraints.addr, align 4, !tbaa !2
  %m_tmpMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 5
  store %class.btMultiBodyConstraint** %0, %class.btMultiBodyConstraint*** %m_tmpMultiBodyConstraints, align 4, !tbaa !100
  %1 = load i32, i32* %numMultiBodyConstraints.addr, align 4, !tbaa !6
  %m_tmpNumMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 6
  store i32 %1, i32* %m_tmpNumMultiBodyConstraints, align 4, !tbaa !97
  %2 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %4 = load i32, i32* %numBodies.addr, align 4, !tbaa !6
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold.addr, align 4, !tbaa !2
  %6 = load i32, i32* %numManifolds.addr, align 4, !tbaa !6
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  %8 = load i32, i32* %numConstraints.addr, align 4, !tbaa !6
  %9 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %info.addr, align 4, !tbaa !2
  %10 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %11 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %call = call float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver* %2, %class.btCollisionObject** %3, i32 %4, %class.btPersistentManifold** %5, i32 %6, %class.btTypedConstraint** %7, i32 %8, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %9, %class.btIDebugDraw* %10, %class.btDispatcher* %11)
  %m_tmpMultiBodyConstraints2 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 5
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_tmpMultiBodyConstraints2, align 4, !tbaa !100
  %m_tmpNumMultiBodyConstraints3 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 6
  store i32 0, i32* %m_tmpNumMultiBodyConstraints3, align 4, !tbaa !97
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btMultiBodyConstraintSolver* @_ZN27btMultiBodyConstraintSolverD2Ev(%class.btMultiBodyConstraintSolver* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraintSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV27btMultiBodyConstraintSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !112
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %call = call %struct.btMultiBodyJacobianData* @_ZN23btMultiBodyJacobianDataD2Ev(%struct.btMultiBodyJacobianData* %m_data) #9
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call2 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.18* %m_multiBodyFrictionContactConstraints) #9
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.18* %m_multiBodyNormalContactConstraints) #9
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.18* %m_multiBodyNonContactConstraints) #9
  %1 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %call5 = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* %1) #9
  ret %class.btMultiBodyConstraintSolver* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN27btMultiBodyConstraintSolverD0Ev(%class.btMultiBodyConstraintSolver* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %call = call %class.btMultiBodyConstraintSolver* @_ZN27btMultiBodyConstraintSolverD2Ev(%class.btMultiBodyConstraintSolver* %this1) #9
  %0 = bitcast %class.btMultiBodyConstraintSolver* %this1 to i8*
  call void @_ZN27btMultiBodyConstraintSolverdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18btConstraintSolver12prepareSolveEii(%class.btConstraintSolver* %this, i32 %0, i32 %1) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca i32, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !6
  store i32 %1, i32* %.addr1, align 4, !tbaa !6
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw(%class.btConstraintSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %0, %class.btIDebugDraw* %1) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr1 = alloca %class.btIDebugDraw*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %1, %class.btIDebugDraw** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver5resetEv(%class.btSequentialImpulseConstraintSolver*) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv(%class.btSequentialImpulseConstraintSolver* %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  ret i32 1
}

declare void @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #2

declare float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84)) unnamed_addr #2

declare float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #2

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  %0 = load i32, i32* %m_internalType, align 4, !tbaa !114
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.22* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !116
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !8
  %0 = load float, float* %y.addr, align 4, !tbaa !8
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

declare void @_ZN15CProfileManager13Start_ProfileEPKc(i8*) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.30* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !39
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

declare void @_ZN15CProfileManager12Stop_ProfileEv() #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btMultiBodyJacobianData* @_ZN23btMultiBodyJacobianDataD2Ev(%struct.btMultiBodyJacobianData* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  store %struct.btMultiBodyJacobianData* %this, %struct.btMultiBodyJacobianData** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %this.addr, align 4
  %scratch_m = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.38* %scratch_m) #9
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 4
  %call2 = call %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.34* %scratch_v) #9
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.30* %scratch_r) #9
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 2
  %call4 = call %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.30* %m_deltaVelocities) #9
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 1
  %call5 = call %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.30* %m_deltaVelocitiesUnitImpulse) #9
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 0
  %call6 = call %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.30* %m_jacobians) #9
  ret %struct.btMultiBodyJacobianData* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.18* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE5clearEv(%class.btAlignedObjectArray.18* %this1)
  ret %class.btAlignedObjectArray.18* %this1
}

; Function Attrs: nounwind
declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #8

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.38* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.38* %this1)
  ret %class.btAlignedObjectArray.38* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.34* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.34* %this1)
  ret %class.btAlignedObjectArray.34* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.30* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.30* %this1)
  ret %class.btAlignedObjectArray.30* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.38* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.38* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.38* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.38* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.38* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.38* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !117
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.38* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !118
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.38* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4, !tbaa !117
  %tobool = icmp ne %class.btMatrix3x3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !119, !range !66
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data4, align 4, !tbaa !117
  call void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.39* %m_allocator, %class.btMatrix3x3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data5, align 4, !tbaa !117
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.38* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !119
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data, align 4, !tbaa !117
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !118
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !120
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.39* %this, %class.btMatrix3x3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.39*, align 4
  %ptr.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedAllocator.39* %this, %class.btAlignedAllocator.39** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %ptr, %class.btMatrix3x3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.39*, %class.btAlignedAllocator.39** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btMatrix3x3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.34* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.34* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.34* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.34* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !121
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.34* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !122
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.34* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !121
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !123, !range !66
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !121
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.35* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !121
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.34* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !123
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !121
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !122
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !124
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.35* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.35*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.35* %this, %class.btAlignedAllocator.35** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.35*, %class.btAlignedAllocator.35** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.30* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.30* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.30* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.30* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.30* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4, !tbaa !39
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.30* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !39
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !125, !range !66
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4, !tbaa !39
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.31* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4, !tbaa !39
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.30* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !125
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4, !tbaa !39
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !42
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !126
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.31* %this, float* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.31*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.31* %this, %class.btAlignedAllocator.31** %this.addr, align 4, !tbaa !2
  store float* %ptr, float** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.31*, %class.btAlignedAllocator.31** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE5clearEv(%class.btAlignedObjectArray.18* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE4initEv(%class.btAlignedObjectArray.18* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.18* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.18* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %tobool = icmp ne %struct.btMultiBodySolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !127, !range !66
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data4, align 4, !tbaa !23
  call void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.19* %m_allocator, %struct.btMultiBodySolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data5, align 4, !tbaa !23
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE4initEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !127
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !19
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !128
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.19* %this, %struct.btMultiBodySolverConstraint* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %ptr.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %ptr, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btMultiBodySolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN27btMultiBodyConstraintSolverdlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.18* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.18* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.18* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btMultiBodySolverConstraint*
  store %struct.btMultiBodySolverConstraint* %3, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call3, %struct.btMultiBodySolverConstraint* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !127
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* %5, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !128
  %7 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !129
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !128
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.18* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.19* %m_allocator, i32 %1, %struct.btMultiBodySolverConstraint** null)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.18* %this, i32 %start, i32 %end, %struct.btMultiBodySolverConstraint* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint* %dest, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  %6 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 184, i8* %6)
  %7 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !23
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 %9
  %10 = bitcast %struct.btMultiBodySolverConstraint* %7 to i8*
  %11 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 184, i1 false), !tbaa.struct !37
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.19* %this, i32 %n, %struct.btMultiBodySolverConstraint** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultiBodySolverConstraint**, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint** %hint, %struct.btMultiBodySolverConstraint*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 184, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  ret %struct.btMultiBodySolverConstraint* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.30* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.30* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.30* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float*
  store float* %3, float** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  %4 = load float*, float** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.30* %this1, i32 0, i32 %call3, float* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.30* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.30* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !125
  %5 = load float*, float** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  store float* %5, float** %m_data, align 4, !tbaa !39
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !126
  %7 = bitcast float** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.30* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !126
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.30* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.31* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.30* %this, i32 %start, i32 %end, float* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store float* %dest, float** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = bitcast float* %arrayidx to i8*
  %7 = bitcast i8* %6 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !39
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !8
  store float %10, float* %7, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.31* %this, i32 %n, float** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.31*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.31* %this, %class.btAlignedAllocator.31** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store float** %hint, float*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.31*, %class.btAlignedAllocator.31** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.18* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !7, i64 20}
!11 = !{!"_ZTS23btContactSolverInfoData", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !7, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !9, i64 40, !7, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !9, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !9, i64 76, !9, i64 80}
!12 = !{!13, !7, i64 156}
!13 = !{!"_ZTS27btMultiBodySolverConstraint", !7, i64 0, !14, i64 4, !14, i64 20, !7, i64 36, !7, i64 40, !14, i64 44, !14, i64 60, !7, i64 76, !14, i64 80, !14, i64 96, !9, i64 112, !9, i64 116, !9, i64 120, !9, i64 124, !9, i64 128, !9, i64 132, !9, i64 136, !9, i64 140, !9, i64 144, !4, i64 148, !7, i64 152, !7, i64 156, !7, i64 160, !3, i64 164, !7, i64 168, !7, i64 172, !3, i64 176, !7, i64 180}
!14 = !{!"_ZTS9btVector3", !4, i64 0}
!15 = !{!13, !9, i64 116}
!16 = !{!13, !9, i64 120}
!17 = !{!13, !9, i64 136}
!18 = !{!13, !9, i64 140}
!19 = !{!20, !7, i64 4}
!20 = !{!"_ZTS20btAlignedObjectArrayI27btMultiBodySolverConstraintE", !21, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!21 = !{!"_ZTS18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE"}
!22 = !{!"bool", !4, i64 0}
!23 = !{!20, !3, i64 12}
!24 = !{!13, !9, i64 128}
!25 = !{!13, !9, i64 132}
!26 = !{!13, !3, i64 164}
!27 = !{!13, !7, i64 36}
!28 = !{!13, !7, i64 0}
!29 = !{!13, !7, i64 160}
!30 = !{!13, !3, i64 176}
!31 = !{!13, !7, i64 76}
!32 = !{!13, !7, i64 40}
!33 = !{!13, !7, i64 172}
!34 = !{!13, !9, i64 124}
!35 = !{!36, !3, i64 264}
!36 = !{!"_ZTS23btMultiBodyLinkCollider", !3, i64 264, !7, i64 268}
!37 = !{i64 0, i64 4, !6, i64 4, i64 16, !38, i64 20, i64 16, !38, i64 36, i64 4, !6, i64 40, i64 4, !6, i64 44, i64 16, !38, i64 60, i64 16, !38, i64 76, i64 4, !6, i64 80, i64 16, !38, i64 96, i64 16, !38, i64 112, i64 4, !8, i64 116, i64 4, !8, i64 120, i64 4, !8, i64 124, i64 4, !8, i64 128, i64 4, !8, i64 132, i64 4, !8, i64 136, i64 4, !8, i64 140, i64 4, !8, i64 144, i64 4, !8, i64 148, i64 4, !2, i64 148, i64 4, !8, i64 152, i64 4, !6, i64 156, i64 4, !6, i64 160, i64 4, !6, i64 164, i64 4, !2, i64 168, i64 4, !6, i64 172, i64 4, !6, i64 176, i64 4, !2, i64 180, i64 4, !6}
!38 = !{!4, !4, i64 0}
!39 = !{!40, !3, i64 12}
!40 = !{!"_ZTS20btAlignedObjectArrayIfE", !41, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!41 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!42 = !{!40, !7, i64 4}
!43 = !{!44, !7, i64 388}
!44 = !{!"_ZTS11btMultiBody", !3, i64 0, !14, i64 4, !45, i64 20, !9, i64 36, !14, i64 40, !14, i64 56, !14, i64 72, !46, i64 88, !48, i64 108, !40, i64 128, !50, i64 148, !52, i64 168, !54, i64 188, !54, i64 236, !54, i64 284, !54, i64 332, !22, i64 380, !22, i64 381, !22, i64 382, !9, i64 384, !7, i64 388, !9, i64 392, !9, i64 396, !22, i64 400, !9, i64 404, !22, i64 408}
!45 = !{!"_ZTS12btQuaternion"}
!46 = !{!"_ZTS20btAlignedObjectArrayI15btMultibodyLinkE", !47, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!47 = !{!"_ZTS18btAlignedAllocatorI15btMultibodyLinkLj16EE"}
!48 = !{!"_ZTS20btAlignedObjectArrayIP23btMultiBodyLinkColliderE", !49, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!49 = !{!"_ZTS18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE"}
!50 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !51, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!51 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!52 = !{!"_ZTS20btAlignedObjectArrayI11btMatrix3x3E", !53, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!53 = !{!"_ZTS18btAlignedAllocatorI11btMatrix3x3Lj16EE"}
!54 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!55 = !{!56, !3, i64 12}
!56 = !{!"_ZTS20btAlignedObjectArrayI12btSolverBodyE", !57, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!57 = !{!"_ZTS18btAlignedAllocatorI12btSolverBodyLj16EE"}
!58 = !{!44, !9, i64 404}
!59 = !{!60, !3, i64 240}
!60 = !{!"_ZTS12btSolverBody", !61, i64 0, !14, i64 64, !14, i64 80, !14, i64 96, !14, i64 112, !14, i64 128, !14, i64 144, !14, i64 160, !14, i64 176, !14, i64 192, !14, i64 208, !14, i64 224, !3, i64 240}
!61 = !{!"_ZTS11btTransform", !54, i64 0, !14, i64 48}
!62 = !{!22, !22, i64 0}
!63 = !{i64 0, i64 16, !38}
!64 = !{!13, !7, i64 168}
!65 = !{!13, !7, i64 180}
!66 = !{i8 0, i8 2}
!67 = !{!11, !9, i64 56}
!68 = !{!69, !9, i64 84}
!69 = !{!"_ZTS15btManifoldPoint", !14, i64 0, !14, i64 16, !14, i64 32, !14, i64 48, !14, i64 64, !9, i64 80, !9, i64 84, !9, i64 88, !9, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !3, i64 112, !22, i64 116, !9, i64 120, !9, i64 124, !9, i64 128, !9, i64 132, !9, i64 136, !9, i64 140, !9, i64 144, !7, i64 148, !14, i64 152, !14, i64 168}
!70 = !{!69, !9, i64 92}
!71 = !{!11, !7, i64 64}
!72 = !{!69, !9, i64 120}
!73 = !{!11, !9, i64 60}
!74 = !{!13, !9, i64 112}
!75 = !{!11, !9, i64 36}
!76 = !{!11, !7, i64 44}
!77 = !{!11, !9, i64 48}
!78 = !{!11, !9, i64 32}
!79 = !{!11, !9, i64 12}
!80 = !{!13, !9, i64 144}
!81 = !{!82, !9, i64 344}
!82 = !{!"_ZTS11btRigidBody", !54, i64 264, !14, i64 312, !14, i64 328, !9, i64 344, !14, i64 348, !14, i64 364, !14, i64 380, !14, i64 396, !14, i64 412, !14, i64 428, !9, i64 444, !9, i64 448, !22, i64 452, !9, i64 456, !9, i64 460, !9, i64 464, !9, i64 468, !9, i64 472, !9, i64 476, !3, i64 480, !83, i64 484, !7, i64 504, !7, i64 508, !14, i64 512, !14, i64 528, !14, i64 544, !14, i64 560, !14, i64 576, !14, i64 592, !7, i64 608, !7, i64 612}
!83 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !84, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!84 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!85 = !{!69, !9, i64 80}
!86 = !{!36, !7, i64 268}
!87 = !{!88, !3, i64 740}
!88 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !7, i64 748, !9, i64 752, !9, i64 756, !7, i64 760, !7, i64 764, !7, i64 768}
!89 = !{!88, !3, i64 744}
!90 = !{!69, !22, i64 116}
!91 = !{!69, !9, i64 132}
!92 = !{!69, !9, i64 140}
!93 = !{!69, !9, i64 136}
!94 = !{!69, !9, i64 144}
!95 = !{!88, !7, i64 748}
!96 = !{!88, !9, i64 756}
!97 = !{!98, !7, i64 388}
!98 = !{!"_ZTS27btMultiBodyConstraintSolver", !20, i64 196, !20, i64 216, !20, i64 236, !99, i64 256, !3, i64 384, !7, i64 388}
!99 = !{!"_ZTS23btMultiBodyJacobianData", !40, i64 0, !40, i64 20, !40, i64 40, !40, i64 60, !50, i64 80, !52, i64 100, !3, i64 120, !7, i64 124}
!100 = !{!98, !3, i64 384}
!101 = !{!98, !3, i64 376}
!102 = !{!103, !7, i64 188}
!103 = !{!"_ZTS35btSequentialImpulseConstraintSolver", !56, i64 4, !104, i64 24, !104, i64 44, !104, i64 64, !104, i64 84, !106, i64 104, !106, i64 124, !106, i64 144, !108, i64 164, !7, i64 184, !7, i64 188, !110, i64 192}
!104 = !{!"_ZTS20btAlignedObjectArrayI18btSolverConstraintE", !105, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!105 = !{!"_ZTS18btAlignedAllocatorI18btSolverConstraintLj16EE"}
!106 = !{!"_ZTS20btAlignedObjectArrayIiE", !107, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!107 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!108 = !{!"_ZTS20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE", !109, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !22, i64 16}
!109 = !{!"_ZTS18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE"}
!110 = !{!"long", !4, i64 0}
!111 = !{!98, !7, i64 380}
!112 = !{!113, !113, i64 0}
!113 = !{!"vtable pointer", !5, i64 0}
!114 = !{!115, !7, i64 236}
!115 = !{!"_ZTS17btCollisionObject", !61, i64 4, !61, i64 68, !14, i64 132, !14, i64 148, !14, i64 164, !7, i64 180, !9, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !7, i64 204, !7, i64 208, !7, i64 212, !7, i64 216, !9, i64 220, !9, i64 224, !9, i64 228, !9, i64 232, !7, i64 236, !4, i64 240, !9, i64 244, !9, i64 248, !9, i64 252, !7, i64 256, !7, i64 260}
!116 = !{!46, !7, i64 4}
!117 = !{!52, !3, i64 12}
!118 = !{!52, !7, i64 4}
!119 = !{!52, !22, i64 16}
!120 = !{!52, !7, i64 8}
!121 = !{!50, !3, i64 12}
!122 = !{!50, !7, i64 4}
!123 = !{!50, !22, i64 16}
!124 = !{!50, !7, i64 8}
!125 = !{!40, !22, i64 16}
!126 = !{!40, !7, i64 8}
!127 = !{!20, !22, i64 16}
!128 = !{!20, !7, i64 8}
!129 = !{!110, !110, i64 0}
