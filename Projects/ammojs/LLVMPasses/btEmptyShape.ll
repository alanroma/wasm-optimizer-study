; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btEmptyShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btEmptyShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btEmptyShape = type { %class.btConcaveShape, %class.btVector3 }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque
%class.btTriangleCallback = type { i32 (...)** }

$_ZN9btVector3C2Ev = comdat any

$_ZN12btEmptyShapedlEPv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN12btEmptyShape15setLocalScalingERK9btVector3 = comdat any

$_ZNK12btEmptyShape15getLocalScalingEv = comdat any

$_ZNK12btEmptyShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btConcaveShape9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ = comdat any

@_ZTV12btEmptyShape = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI12btEmptyShape to i8*), i8* bitcast (%class.btEmptyShape* (%class.btEmptyShape*)* @_ZN12btEmptyShapeD1Ev to i8*), i8* bitcast (void (%class.btEmptyShape*)* @_ZN12btEmptyShapeD0Ev to i8*), i8* bitcast (void (%class.btEmptyShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btEmptyShape*, %class.btVector3*)* @_ZN12btEmptyShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btEmptyShape*)* @_ZNK12btEmptyShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btEmptyShape*, float, %class.btVector3*)* @_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btEmptyShape*)* @_ZNK12btEmptyShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConcaveShape*, float)* @_ZN14btConcaveShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btEmptyShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS12btEmptyShape = hidden constant [15 x i8] c"12btEmptyShape\00", align 1
@_ZTI14btConcaveShape = external constant i8*
@_ZTI12btEmptyShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btEmptyShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI14btConcaveShape to i8*) }, align 4
@.str = private unnamed_addr constant [6 x i8] c"Empty\00", align 1

@_ZN12btEmptyShapeC1Ev = hidden unnamed_addr alias %class.btEmptyShape* (%class.btEmptyShape*), %class.btEmptyShape* (%class.btEmptyShape*)* @_ZN12btEmptyShapeC2Ev
@_ZN12btEmptyShapeD1Ev = hidden unnamed_addr alias %class.btEmptyShape* (%class.btEmptyShape*), %class.btEmptyShape* (%class.btEmptyShape*)* @_ZN12btEmptyShapeD2Ev

define hidden %class.btEmptyShape* @_ZN12btEmptyShapeC2Ev(%class.btEmptyShape* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  %0 = bitcast %class.btEmptyShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* %0)
  %1 = bitcast %class.btEmptyShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV12btEmptyShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_localScaling = getelementptr inbounds %class.btEmptyShape, %class.btEmptyShape* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localScaling)
  %2 = bitcast %class.btEmptyShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 27, i32* %m_shapeType, align 4, !tbaa !8
  ret %class.btEmptyShape* %this1
}

declare %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
declare %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* returned) unnamed_addr #3

; Function Attrs: nounwind
define hidden %class.btEmptyShape* @_ZN12btEmptyShapeD2Ev(%class.btEmptyShape* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  %0 = bitcast %class.btEmptyShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* %0) #7
  ret %class.btEmptyShape* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN12btEmptyShapeD0Ev(%class.btEmptyShape* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  %call = call %class.btEmptyShape* @_ZN12btEmptyShapeD1Ev(%class.btEmptyShape* %this1) #7
  %0 = bitcast %class.btEmptyShape* %this1 to i8*
  call void @_ZN12btEmptyShapedlEPv(i8* %0) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN12btEmptyShapedlEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZNK12btEmptyShape7getAabbERK11btTransformR9btVector3S4_(%class.btEmptyShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %margin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #7
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast %class.btEmptyShape* %this1 to %class.btConcaveShape*
  %3 = bitcast %class.btConcaveShape* %2 to float (%class.btConcaveShape*)***
  %vtable = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable, i64 12
  %4 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn, align 4
  %call = call float %4(%class.btConcaveShape* %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !11
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = bitcast %class.btEmptyShape* %this1 to %class.btConcaveShape*
  %7 = bitcast %class.btConcaveShape* %6 to float (%class.btConcaveShape*)***
  %vtable3 = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %7, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable3, i64 12
  %8 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn4, align 4
  %call5 = call float %8(%class.btConcaveShape* %6)
  store float %call5, float* %ref.tmp2, align 4, !tbaa !11
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = bitcast %class.btEmptyShape* %this1 to %class.btConcaveShape*
  %11 = bitcast %class.btConcaveShape* %10 to float (%class.btConcaveShape*)***
  %vtable7 = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %11, align 4, !tbaa !6
  %vfn8 = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable7, i64 12
  %12 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn8, align 4
  %call9 = call float %12(%class.btConcaveShape* %10)
  store float %call9, float* %ref.tmp6, align 4, !tbaa !11
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %margin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  %15 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #7
  %17 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %17)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %margin)
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %19 = bitcast %class.btVector3* %18 to i8*
  %20 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false), !tbaa.struct !13
  %21 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #7
  %22 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #7
  %23 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %23)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %margin)
  %24 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %25 = bitcast %class.btVector3* %24 to i8*
  %26 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !13
  %27 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #7
  %28 = bitcast %class.btVector3* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !11
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !11
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !11
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !11
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !11
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !11
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !11
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !11
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !11
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !11
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !11
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !11
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !11
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !11
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !11
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !11
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !11
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !11
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !11
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !11
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !11
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !11
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !11
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZNK12btEmptyShape21calculateLocalInertiaEfR9btVector3(%class.btEmptyShape* %this, float %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  %.addr = alloca float, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  store float %0, float* %.addr, align 4, !tbaa !11
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12btEmptyShape15setLocalScalingERK9btVector3(%class.btEmptyShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %m_localScaling = getelementptr inbounds %class.btEmptyShape, %class.btEmptyShape* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_localScaling to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !13
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btEmptyShape15getLocalScalingEv(%class.btEmptyShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btEmptyShape, %class.btEmptyShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK12btEmptyShape7getNameEv(%class.btEmptyShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !11
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !11
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !11
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN14btConcaveShape9setMarginEf(%class.btConcaveShape* %this, float %collisionMargin) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  store float %collisionMargin, float* %collisionMargin.addr, align 4, !tbaa !11
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = load float, float* %collisionMargin.addr, align 4, !tbaa !11
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4, !tbaa !15
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !15
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK12btEmptyShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btEmptyShape* %this, %class.btTriangleCallback* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btEmptyShape*, align 4
  %.addr = alloca %class.btTriangleCallback*, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  %.addr2 = alloca %class.btVector3*, align 4
  store %class.btEmptyShape* %this, %class.btEmptyShape** %this.addr, align 4, !tbaa !2
  store %class.btTriangleCallback* %0, %class.btTriangleCallback** %.addr, align 4, !tbaa !2
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4, !tbaa !2
  store %class.btVector3* %2, %class.btVector3** %.addr2, align 4, !tbaa !2
  %this3 = load %class.btEmptyShape*, %class.btEmptyShape** %this.addr, align 4
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !10, i64 4}
!9 = !{!"_ZTS16btCollisionShape", !10, i64 4, !3, i64 8}
!10 = !{!"int", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"float", !4, i64 0}
!13 = !{i64 0, i64 16, !14}
!14 = !{!4, !4, i64 0}
!15 = !{!16, !12, i64 12}
!16 = !{!"_ZTS14btConcaveShape", !12, i64 12}
