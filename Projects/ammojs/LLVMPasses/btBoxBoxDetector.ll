; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btBoxBoxDetector.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btBoxBoxDetector.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btBoxBoxDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btBoxShape*, %class.btBoxShape* }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%class.btBoxShape = type { %class.btPolyhedralConvexShape }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btConvexPolyhedron = type opaque
%struct.dContactGeom = type opaque
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btIDebugDraw = type opaque

$_ZN36btDiscreteCollisionDetectorInterfaceC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_Z6btFabsf = comdat any

$_Z7btAtan2ff = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZngRK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK10btBoxShape24getHalfExtentsWithMarginEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN16btBoxBoxDetectorD0Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD0Ev = comdat any

$_ZNK10btBoxShape27getHalfExtentsWithoutMarginEv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZTS36btDiscreteCollisionDetectorInterface = comdat any

$_ZTI36btDiscreteCollisionDetectorInterface = comdat any

$_ZTV36btDiscreteCollisionDetectorInterface = comdat any

@_ZTV16btBoxBoxDetector = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btBoxBoxDetector to i8*), i8* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to i8*), i8* bitcast (void (%struct.btBoxBoxDetector*)* @_ZN16btBoxBoxDetectorD0Ev to i8*), i8* bitcast (void (%struct.btBoxBoxDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btIDebugDraw*, i1)* @_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btBoxBoxDetector = hidden constant [19 x i8] c"16btBoxBoxDetector\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant [39 x i8] c"36btDiscreteCollisionDetectorInterface\00", comdat, align 1
@_ZTI36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36btDiscreteCollisionDetectorInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI16btBoxBoxDetector = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btBoxBoxDetector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*) }, align 4
@_ZTV36btDiscreteCollisionDetectorInterface = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*), i8* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to i8*), i8* bitcast (void (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN16btBoxBoxDetectorC1EPK10btBoxShapeS2_ = hidden unnamed_addr alias %struct.btBoxBoxDetector* (%struct.btBoxBoxDetector*, %class.btBoxShape*, %class.btBoxShape*), %struct.btBoxBoxDetector* (%struct.btBoxBoxDetector*, %class.btBoxShape*, %class.btBoxShape*)* @_ZN16btBoxBoxDetectorC2EPK10btBoxShapeS2_

; Function Attrs: nounwind
define hidden %struct.btBoxBoxDetector* @_ZN16btBoxBoxDetectorC2EPK10btBoxShapeS2_(%struct.btBoxBoxDetector* returned %this, %class.btBoxShape* %box1, %class.btBoxShape* %box2) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.btBoxBoxDetector*, align 4
  %box1.addr = alloca %class.btBoxShape*, align 4
  %box2.addr = alloca %class.btBoxShape*, align 4
  store %struct.btBoxBoxDetector* %this, %struct.btBoxBoxDetector** %this.addr, align 4, !tbaa !2
  store %class.btBoxShape* %box1, %class.btBoxShape** %box1.addr, align 4, !tbaa !2
  store %class.btBoxShape* %box2, %class.btBoxShape** %box2.addr, align 4, !tbaa !2
  %this1 = load %struct.btBoxBoxDetector*, %struct.btBoxBoxDetector** %this.addr, align 4
  %0 = bitcast %struct.btBoxBoxDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #9
  %1 = bitcast %struct.btBoxBoxDetector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV16btBoxBoxDetector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_box1 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this1, i32 0, i32 1
  %2 = load %class.btBoxShape*, %class.btBoxShape** %box1.addr, align 4, !tbaa !2
  store %class.btBoxShape* %2, %class.btBoxShape** %m_box1, align 4, !tbaa !8
  %m_box2 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this1, i32 0, i32 2
  %3 = load %class.btBoxShape*, %class.btBoxShape** %box2.addr, align 4, !tbaa !2
  store %class.btBoxShape* %3, %class.btBoxShape** %m_box2, align 4, !tbaa !10
  ret %struct.btBoxBoxDetector* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %0 = bitcast %struct.btDiscreteCollisionDetectorInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36btDiscreteCollisionDetectorInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

define hidden void @_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_(%class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %ua, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %ub, float* %alpha, float* %beta) #2 {
entry:
  %pa.addr = alloca %class.btVector3*, align 4
  %ua.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  %ub.addr = alloca %class.btVector3*, align 4
  %alpha.addr = alloca float*, align 4
  %beta.addr = alloca float*, align 4
  %p = alloca %class.btVector3, align 4
  %uaub = alloca float, align 4
  %q1 = alloca float, align 4
  %q2 = alloca float, align 4
  %d = alloca float, align 4
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %ua, %class.btVector3** %ua.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  store %class.btVector3* %ub, %class.btVector3** %ub.addr, align 4, !tbaa !2
  store float* %alpha, float** %alpha.addr, align 4, !tbaa !2
  store float* %beta, float** %beta.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %1 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %1)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %4 = load float, float* %arrayidx3, align 4, !tbaa !11
  %sub = fsub float %2, %4
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  store float %sub, float* %arrayidx5, align 4, !tbaa !11
  %5 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !11
  %7 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %8 = load float, float* %arrayidx9, align 4, !tbaa !11
  %sub10 = fsub float %6, %8
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float %sub10, float* %arrayidx12, align 4, !tbaa !11
  %9 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  %10 = load float, float* %arrayidx14, align 4, !tbaa !11
  %11 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %12 = load float, float* %arrayidx16, align 4, !tbaa !11
  %sub17 = fsub float %10, %12
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  store float %sub17, float* %arrayidx19, align 4, !tbaa !11
  %13 = bitcast float* %uaub to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load %class.btVector3*, %class.btVector3** %ua.addr, align 4, !tbaa !2
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %15 = load %class.btVector3*, %class.btVector3** %ub.addr, align 4, !tbaa !2
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %call22 = call float @_ZL4dDOTPKfS0_(float* %call20, float* %call21)
  store float %call22, float* %uaub, align 4, !tbaa !11
  %16 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btVector3*, %class.btVector3** %ua.addr, align 4, !tbaa !2
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %17)
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call25 = call float @_ZL4dDOTPKfS0_(float* %call23, float* %call24)
  store float %call25, float* %q1, align 4, !tbaa !11
  %18 = bitcast float* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = load %class.btVector3*, %class.btVector3** %ub.addr, align 4, !tbaa !2
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %19)
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call28 = call float @_ZL4dDOTPKfS0_(float* %call26, float* %call27)
  %fneg = fneg float %call28
  store float %fneg, float* %q2, align 4, !tbaa !11
  %20 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %21 = load float, float* %uaub, align 4, !tbaa !11
  %22 = load float, float* %uaub, align 4, !tbaa !11
  %mul = fmul float %21, %22
  %sub29 = fsub float 1.000000e+00, %mul
  store float %sub29, float* %d, align 4, !tbaa !11
  %23 = load float, float* %d, align 4, !tbaa !11
  %cmp = fcmp ole float %23, 0x3F1A36E2E0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %24 = load float*, float** %alpha.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %24, align 4, !tbaa !11
  %25 = load float*, float** %beta.addr, align 4, !tbaa !2
  store float 0.000000e+00, float* %25, align 4, !tbaa !11
  br label %if.end

if.else:                                          ; preds = %entry
  %26 = load float, float* %d, align 4, !tbaa !11
  %div = fdiv float 1.000000e+00, %26
  store float %div, float* %d, align 4, !tbaa !11
  %27 = load float, float* %q1, align 4, !tbaa !11
  %28 = load float, float* %uaub, align 4, !tbaa !11
  %29 = load float, float* %q2, align 4, !tbaa !11
  %mul30 = fmul float %28, %29
  %add = fadd float %27, %mul30
  %30 = load float, float* %d, align 4, !tbaa !11
  %mul31 = fmul float %add, %30
  %31 = load float*, float** %alpha.addr, align 4, !tbaa !2
  store float %mul31, float* %31, align 4, !tbaa !11
  %32 = load float, float* %uaub, align 4, !tbaa !11
  %33 = load float, float* %q1, align 4, !tbaa !11
  %mul32 = fmul float %32, %33
  %34 = load float, float* %q2, align 4, !tbaa !11
  %add33 = fadd float %mul32, %34
  %35 = load float, float* %d, align 4, !tbaa !11
  %mul34 = fmul float %add33, %35
  %36 = load float*, float** %beta.addr, align 4, !tbaa !2
  store float %mul34, float* %36, align 4, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %37 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast float* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  %39 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  %40 = bitcast float* %uaub to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define internal float @_ZL4dDOTPKfS0_(float* %a, float* %b) #0 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !11
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4, !tbaa !11
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 1
  %5 = load float, float* %arrayidx2, align 4, !tbaa !11
  %6 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !11
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 2
  %9 = load float, float* %arrayidx5, align 4, !tbaa !11
  %10 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 2
  %11 = load float, float* %arrayidx6, align 4, !tbaa !11
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

define hidden void @_Z11cullPoints2iPfiiPi(i32 %n, float* %p, i32 %m, i32 %i0, i32* %iret) #2 {
entry:
  %n.addr = alloca i32, align 4
  %p.addr = alloca float*, align 4
  %m.addr = alloca i32, align 4
  %i0.addr = alloca i32, align 4
  %iret.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %a = alloca float, align 4
  %cx = alloca float, align 4
  %cy = alloca float, align 4
  %q = alloca float, align 4
  %A = alloca [8 x float], align 16
  %avail = alloca [8 x i32], align 16
  %maxdiff = alloca float, align 4
  %diff = alloca float, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !13
  store float* %p, float** %p.addr, align 4, !tbaa !2
  store i32 %m, i32* %m.addr, align 4, !tbaa !13
  store i32 %i0, i32* %i0.addr, align 4, !tbaa !13
  store i32* %iret, i32** %iret.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = bitcast float* %cx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %cy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load i32, i32* %n.addr, align 4, !tbaa !13
  %cmp = icmp eq i32 %6, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %7, i32 0
  %8 = load float, float* %arrayidx, align 4, !tbaa !11
  store float %8, float* %cx, align 4, !tbaa !11
  %9 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %9, i32 1
  %10 = load float, float* %arrayidx1, align 4, !tbaa !11
  store float %10, float* %cy, align 4, !tbaa !11
  br label %if.end78

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %n.addr, align 4, !tbaa !13
  %cmp2 = icmp eq i32 %11, 2
  br i1 %cmp2, label %if.then3, label %if.else10

if.then3:                                         ; preds = %if.else
  %12 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %12, i32 0
  %13 = load float, float* %arrayidx4, align 4, !tbaa !11
  %14 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %14, i32 2
  %15 = load float, float* %arrayidx5, align 4, !tbaa !11
  %add = fadd float %13, %15
  %mul = fmul float 5.000000e-01, %add
  store float %mul, float* %cx, align 4, !tbaa !11
  %16 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %16, i32 1
  %17 = load float, float* %arrayidx6, align 4, !tbaa !11
  %18 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %18, i32 3
  %19 = load float, float* %arrayidx7, align 4, !tbaa !11
  %add8 = fadd float %17, %19
  %mul9 = fmul float 5.000000e-01, %add8
  store float %mul9, float* %cy, align 4, !tbaa !11
  br label %if.end77

if.else10:                                        ; preds = %if.else
  store float 0.000000e+00, float* %a, align 4, !tbaa !11
  store float 0.000000e+00, float* %cx, align 4, !tbaa !11
  store float 0.000000e+00, float* %cy, align 4, !tbaa !11
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else10
  %20 = load i32, i32* %i, align 4, !tbaa !13
  %21 = load i32, i32* %n.addr, align 4, !tbaa !13
  %sub = sub nsw i32 %21, 1
  %cmp11 = icmp slt i32 %20, %sub
  br i1 %cmp11, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load float*, float** %p.addr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !13
  %mul12 = mul nsw i32 %23, 2
  %arrayidx13 = getelementptr inbounds float, float* %22, i32 %mul12
  %24 = load float, float* %arrayidx13, align 4, !tbaa !11
  %25 = load float*, float** %p.addr, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !13
  %mul14 = mul nsw i32 %26, 2
  %add15 = add nsw i32 %mul14, 3
  %arrayidx16 = getelementptr inbounds float, float* %25, i32 %add15
  %27 = load float, float* %arrayidx16, align 4, !tbaa !11
  %mul17 = fmul float %24, %27
  %28 = load float*, float** %p.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !13
  %mul18 = mul nsw i32 %29, 2
  %add19 = add nsw i32 %mul18, 2
  %arrayidx20 = getelementptr inbounds float, float* %28, i32 %add19
  %30 = load float, float* %arrayidx20, align 4, !tbaa !11
  %31 = load float*, float** %p.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !13
  %mul21 = mul nsw i32 %32, 2
  %add22 = add nsw i32 %mul21, 1
  %arrayidx23 = getelementptr inbounds float, float* %31, i32 %add22
  %33 = load float, float* %arrayidx23, align 4, !tbaa !11
  %mul24 = fmul float %30, %33
  %sub25 = fsub float %mul17, %mul24
  store float %sub25, float* %q, align 4, !tbaa !11
  %34 = load float, float* %q, align 4, !tbaa !11
  %35 = load float, float* %a, align 4, !tbaa !11
  %add26 = fadd float %35, %34
  store float %add26, float* %a, align 4, !tbaa !11
  %36 = load float, float* %q, align 4, !tbaa !11
  %37 = load float*, float** %p.addr, align 4, !tbaa !2
  %38 = load i32, i32* %i, align 4, !tbaa !13
  %mul27 = mul nsw i32 %38, 2
  %arrayidx28 = getelementptr inbounds float, float* %37, i32 %mul27
  %39 = load float, float* %arrayidx28, align 4, !tbaa !11
  %40 = load float*, float** %p.addr, align 4, !tbaa !2
  %41 = load i32, i32* %i, align 4, !tbaa !13
  %mul29 = mul nsw i32 %41, 2
  %add30 = add nsw i32 %mul29, 2
  %arrayidx31 = getelementptr inbounds float, float* %40, i32 %add30
  %42 = load float, float* %arrayidx31, align 4, !tbaa !11
  %add32 = fadd float %39, %42
  %mul33 = fmul float %36, %add32
  %43 = load float, float* %cx, align 4, !tbaa !11
  %add34 = fadd float %43, %mul33
  store float %add34, float* %cx, align 4, !tbaa !11
  %44 = load float, float* %q, align 4, !tbaa !11
  %45 = load float*, float** %p.addr, align 4, !tbaa !2
  %46 = load i32, i32* %i, align 4, !tbaa !13
  %mul35 = mul nsw i32 %46, 2
  %add36 = add nsw i32 %mul35, 1
  %arrayidx37 = getelementptr inbounds float, float* %45, i32 %add36
  %47 = load float, float* %arrayidx37, align 4, !tbaa !11
  %48 = load float*, float** %p.addr, align 4, !tbaa !2
  %49 = load i32, i32* %i, align 4, !tbaa !13
  %mul38 = mul nsw i32 %49, 2
  %add39 = add nsw i32 %mul38, 3
  %arrayidx40 = getelementptr inbounds float, float* %48, i32 %add39
  %50 = load float, float* %arrayidx40, align 4, !tbaa !11
  %add41 = fadd float %47, %50
  %mul42 = fmul float %44, %add41
  %51 = load float, float* %cy, align 4, !tbaa !11
  %add43 = fadd float %51, %mul42
  store float %add43, float* %cy, align 4, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %53 = load float*, float** %p.addr, align 4, !tbaa !2
  %54 = load i32, i32* %n.addr, align 4, !tbaa !13
  %mul44 = mul nsw i32 %54, 2
  %sub45 = sub nsw i32 %mul44, 2
  %arrayidx46 = getelementptr inbounds float, float* %53, i32 %sub45
  %55 = load float, float* %arrayidx46, align 4, !tbaa !11
  %56 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds float, float* %56, i32 1
  %57 = load float, float* %arrayidx47, align 4, !tbaa !11
  %mul48 = fmul float %55, %57
  %58 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds float, float* %58, i32 0
  %59 = load float, float* %arrayidx49, align 4, !tbaa !11
  %60 = load float*, float** %p.addr, align 4, !tbaa !2
  %61 = load i32, i32* %n.addr, align 4, !tbaa !13
  %mul50 = mul nsw i32 %61, 2
  %sub51 = sub nsw i32 %mul50, 1
  %arrayidx52 = getelementptr inbounds float, float* %60, i32 %sub51
  %62 = load float, float* %arrayidx52, align 4, !tbaa !11
  %mul53 = fmul float %59, %62
  %sub54 = fsub float %mul48, %mul53
  store float %sub54, float* %q, align 4, !tbaa !11
  %63 = load float, float* %a, align 4, !tbaa !11
  %64 = load float, float* %q, align 4, !tbaa !11
  %add55 = fadd float %63, %64
  %call = call float @_Z6btFabsf(float %add55)
  %cmp56 = fcmp ogt float %call, 0x3E80000000000000
  br i1 %cmp56, label %if.then57, label %if.else60

if.then57:                                        ; preds = %for.end
  %65 = load float, float* %a, align 4, !tbaa !11
  %66 = load float, float* %q, align 4, !tbaa !11
  %add58 = fadd float %65, %66
  %mul59 = fmul float 3.000000e+00, %add58
  %div = fdiv float 1.000000e+00, %mul59
  store float %div, float* %a, align 4, !tbaa !11
  br label %if.end

if.else60:                                        ; preds = %for.end
  store float 0x43ABC16D60000000, float* %a, align 4, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %if.else60, %if.then57
  %67 = load float, float* %a, align 4, !tbaa !11
  %68 = load float, float* %cx, align 4, !tbaa !11
  %69 = load float, float* %q, align 4, !tbaa !11
  %70 = load float*, float** %p.addr, align 4, !tbaa !2
  %71 = load i32, i32* %n.addr, align 4, !tbaa !13
  %mul61 = mul nsw i32 %71, 2
  %sub62 = sub nsw i32 %mul61, 2
  %arrayidx63 = getelementptr inbounds float, float* %70, i32 %sub62
  %72 = load float, float* %arrayidx63, align 4, !tbaa !11
  %73 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds float, float* %73, i32 0
  %74 = load float, float* %arrayidx64, align 4, !tbaa !11
  %add65 = fadd float %72, %74
  %mul66 = fmul float %69, %add65
  %add67 = fadd float %68, %mul66
  %mul68 = fmul float %67, %add67
  store float %mul68, float* %cx, align 4, !tbaa !11
  %75 = load float, float* %a, align 4, !tbaa !11
  %76 = load float, float* %cy, align 4, !tbaa !11
  %77 = load float, float* %q, align 4, !tbaa !11
  %78 = load float*, float** %p.addr, align 4, !tbaa !2
  %79 = load i32, i32* %n.addr, align 4, !tbaa !13
  %mul69 = mul nsw i32 %79, 2
  %sub70 = sub nsw i32 %mul69, 1
  %arrayidx71 = getelementptr inbounds float, float* %78, i32 %sub70
  %80 = load float, float* %arrayidx71, align 4, !tbaa !11
  %81 = load float*, float** %p.addr, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds float, float* %81, i32 1
  %82 = load float, float* %arrayidx72, align 4, !tbaa !11
  %add73 = fadd float %80, %82
  %mul74 = fmul float %77, %add73
  %add75 = fadd float %76, %mul74
  %mul76 = fmul float %75, %add75
  store float %mul76, float* %cy, align 4, !tbaa !11
  br label %if.end77

if.end77:                                         ; preds = %if.end, %if.then3
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %if.then
  %83 = bitcast [8 x float]* %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %83) #9
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc91, %if.end78
  %84 = load i32, i32* %i, align 4, !tbaa !13
  %85 = load i32, i32* %n.addr, align 4, !tbaa !13
  %cmp80 = icmp slt i32 %84, %85
  br i1 %cmp80, label %for.body81, label %for.end93

for.body81:                                       ; preds = %for.cond79
  %86 = load float*, float** %p.addr, align 4, !tbaa !2
  %87 = load i32, i32* %i, align 4, !tbaa !13
  %mul82 = mul nsw i32 %87, 2
  %add83 = add nsw i32 %mul82, 1
  %arrayidx84 = getelementptr inbounds float, float* %86, i32 %add83
  %88 = load float, float* %arrayidx84, align 4, !tbaa !11
  %89 = load float, float* %cy, align 4, !tbaa !11
  %sub85 = fsub float %88, %89
  %90 = load float*, float** %p.addr, align 4, !tbaa !2
  %91 = load i32, i32* %i, align 4, !tbaa !13
  %mul86 = mul nsw i32 %91, 2
  %arrayidx87 = getelementptr inbounds float, float* %90, i32 %mul86
  %92 = load float, float* %arrayidx87, align 4, !tbaa !11
  %93 = load float, float* %cx, align 4, !tbaa !11
  %sub88 = fsub float %92, %93
  %call89 = call float @_Z7btAtan2ff(float %sub85, float %sub88)
  %94 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx90 = getelementptr inbounds [8 x float], [8 x float]* %A, i32 0, i32 %94
  store float %call89, float* %arrayidx90, align 4, !tbaa !11
  br label %for.inc91

for.inc91:                                        ; preds = %for.body81
  %95 = load i32, i32* %i, align 4, !tbaa !13
  %inc92 = add nsw i32 %95, 1
  store i32 %inc92, i32* %i, align 4, !tbaa !13
  br label %for.cond79

for.end93:                                        ; preds = %for.cond79
  %96 = bitcast [8 x i32]* %avail to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %96) #9
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond94

for.cond94:                                       ; preds = %for.inc98, %for.end93
  %97 = load i32, i32* %i, align 4, !tbaa !13
  %98 = load i32, i32* %n.addr, align 4, !tbaa !13
  %cmp95 = icmp slt i32 %97, %98
  br i1 %cmp95, label %for.body96, label %for.end100

for.body96:                                       ; preds = %for.cond94
  %99 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx97 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %99
  store i32 1, i32* %arrayidx97, align 4, !tbaa !13
  br label %for.inc98

for.inc98:                                        ; preds = %for.body96
  %100 = load i32, i32* %i, align 4, !tbaa !13
  %inc99 = add nsw i32 %100, 1
  store i32 %inc99, i32* %i, align 4, !tbaa !13
  br label %for.cond94

for.end100:                                       ; preds = %for.cond94
  %101 = load i32, i32* %i0.addr, align 4, !tbaa !13
  %arrayidx101 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %101
  store i32 0, i32* %arrayidx101, align 4, !tbaa !13
  %102 = load i32, i32* %i0.addr, align 4, !tbaa !13
  %103 = load i32*, i32** %iret.addr, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %103, i32 0
  store i32 %102, i32* %arrayidx102, align 4, !tbaa !13
  %104 = load i32*, i32** %iret.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %104, i32 1
  store i32* %incdec.ptr, i32** %iret.addr, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !13
  br label %for.cond103

for.cond103:                                      ; preds = %for.inc136, %for.end100
  %105 = load i32, i32* %j, align 4, !tbaa !13
  %106 = load i32, i32* %m.addr, align 4, !tbaa !13
  %cmp104 = icmp slt i32 %105, %106
  br i1 %cmp104, label %for.body105, label %for.end138

for.body105:                                      ; preds = %for.cond103
  %107 = load i32, i32* %j, align 4, !tbaa !13
  %conv = sitofp i32 %107 to float
  %108 = load i32, i32* %m.addr, align 4, !tbaa !13
  %conv106 = sitofp i32 %108 to float
  %div107 = fdiv float 0x401921FB60000000, %conv106
  %mul108 = fmul float %conv, %div107
  %109 = load i32, i32* %i0.addr, align 4, !tbaa !13
  %arrayidx109 = getelementptr inbounds [8 x float], [8 x float]* %A, i32 0, i32 %109
  %110 = load float, float* %arrayidx109, align 4, !tbaa !11
  %add110 = fadd float %mul108, %110
  store float %add110, float* %a, align 4, !tbaa !11
  %111 = load float, float* %a, align 4, !tbaa !11
  %cmp111 = fcmp ogt float %111, 0x400921FB60000000
  br i1 %cmp111, label %if.then112, label %if.end114

if.then112:                                       ; preds = %for.body105
  %112 = load float, float* %a, align 4, !tbaa !11
  %sub113 = fsub float %112, 0x401921FB60000000
  store float %sub113, float* %a, align 4, !tbaa !11
  br label %if.end114

if.end114:                                        ; preds = %if.then112, %for.body105
  %113 = bitcast float* %maxdiff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #9
  store float 1.000000e+09, float* %maxdiff, align 4, !tbaa !11
  %114 = bitcast float* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #9
  %115 = load i32, i32* %i0.addr, align 4, !tbaa !13
  %116 = load i32*, i32** %iret.addr, align 4, !tbaa !2
  store i32 %115, i32* %116, align 4, !tbaa !13
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc131, %if.end114
  %117 = load i32, i32* %i, align 4, !tbaa !13
  %118 = load i32, i32* %n.addr, align 4, !tbaa !13
  %cmp116 = icmp slt i32 %117, %118
  br i1 %cmp116, label %for.body117, label %for.end133

for.body117:                                      ; preds = %for.cond115
  %119 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx118 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %119
  %120 = load i32, i32* %arrayidx118, align 4, !tbaa !13
  %tobool = icmp ne i32 %120, 0
  br i1 %tobool, label %if.then119, label %if.end130

if.then119:                                       ; preds = %for.body117
  %121 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx120 = getelementptr inbounds [8 x float], [8 x float]* %A, i32 0, i32 %121
  %122 = load float, float* %arrayidx120, align 4, !tbaa !11
  %123 = load float, float* %a, align 4, !tbaa !11
  %sub121 = fsub float %122, %123
  %call122 = call float @_Z6btFabsf(float %sub121)
  store float %call122, float* %diff, align 4, !tbaa !11
  %124 = load float, float* %diff, align 4, !tbaa !11
  %cmp123 = fcmp ogt float %124, 0x400921FB60000000
  br i1 %cmp123, label %if.then124, label %if.end126

if.then124:                                       ; preds = %if.then119
  %125 = load float, float* %diff, align 4, !tbaa !11
  %sub125 = fsub float 0x401921FB60000000, %125
  store float %sub125, float* %diff, align 4, !tbaa !11
  br label %if.end126

if.end126:                                        ; preds = %if.then124, %if.then119
  %126 = load float, float* %diff, align 4, !tbaa !11
  %127 = load float, float* %maxdiff, align 4, !tbaa !11
  %cmp127 = fcmp olt float %126, %127
  br i1 %cmp127, label %if.then128, label %if.end129

if.then128:                                       ; preds = %if.end126
  %128 = load float, float* %diff, align 4, !tbaa !11
  store float %128, float* %maxdiff, align 4, !tbaa !11
  %129 = load i32, i32* %i, align 4, !tbaa !13
  %130 = load i32*, i32** %iret.addr, align 4, !tbaa !2
  store i32 %129, i32* %130, align 4, !tbaa !13
  br label %if.end129

if.end129:                                        ; preds = %if.then128, %if.end126
  br label %if.end130

if.end130:                                        ; preds = %if.end129, %for.body117
  br label %for.inc131

for.inc131:                                       ; preds = %if.end130
  %131 = load i32, i32* %i, align 4, !tbaa !13
  %inc132 = add nsw i32 %131, 1
  store i32 %inc132, i32* %i, align 4, !tbaa !13
  br label %for.cond115

for.end133:                                       ; preds = %for.cond115
  %132 = load i32*, i32** %iret.addr, align 4, !tbaa !2
  %133 = load i32, i32* %132, align 4, !tbaa !13
  %arrayidx134 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %133
  store i32 0, i32* %arrayidx134, align 4, !tbaa !13
  %134 = load i32*, i32** %iret.addr, align 4, !tbaa !2
  %incdec.ptr135 = getelementptr inbounds i32, i32* %134, i32 1
  store i32* %incdec.ptr135, i32** %iret.addr, align 4, !tbaa !2
  %135 = bitcast float* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #9
  %136 = bitcast float* %maxdiff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #9
  br label %for.inc136

for.inc136:                                       ; preds = %for.end133
  %137 = load i32, i32* %j, align 4, !tbaa !13
  %inc137 = add nsw i32 %137, 1
  store i32 %inc137, i32* %j, align 4, !tbaa !13
  br label %for.cond103

for.end138:                                       ; preds = %for.cond103
  %138 = bitcast [8 x i32]* %avail to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %138) #9
  %139 = bitcast [8 x float]* %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %139) #9
  %140 = bitcast float* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #9
  %141 = bitcast float* %cy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #9
  %142 = bitcast float* %cx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #9
  %143 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #9
  %144 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #9
  %145 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !11
  %0 = load float, float* %x.addr, align 4, !tbaa !11
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !11
  store float %y, float* %y.addr, align 4, !tbaa !11
  %0 = load float, float* %x.addr, align 4, !tbaa !11
  %1 = load float, float* %y.addr, align 4, !tbaa !11
  %call = call float @atan2f(float %0, float %1) #10
  ret float %call
}

define hidden i32 @_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %p1, float* %R1, %class.btVector3* nonnull align 4 dereferenceable(16) %side1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2, float* %R2, %class.btVector3* nonnull align 4 dereferenceable(16) %side2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* %depth, i32* %return_code, i32 %maxc, %struct.dContactGeom* %0, i32 %1, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output) #2 {
entry:
  %retval = alloca i32, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %R1.addr = alloca float*, align 4
  %side1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  %R2.addr = alloca float*, align 4
  %side2.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float*, align 4
  %return_code.addr = alloca i32*, align 4
  %maxc.addr = alloca i32, align 4
  %.addr = alloca %struct.dContactGeom*, align 4
  %.addr1 = alloca i32, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %fudge_factor = alloca float, align 4
  %p = alloca %class.btVector3, align 4
  %pp = alloca %class.btVector3, align 4
  %normalC = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %normalR = alloca float*, align 4
  %A = alloca [3 x float], align 4
  %B = alloca [3 x float], align 4
  %R11 = alloca float, align 4
  %R12 = alloca float, align 4
  %R13 = alloca float, align 4
  %R21 = alloca float, align 4
  %R22 = alloca float, align 4
  %R23 = alloca float, align 4
  %R31 = alloca float, align 4
  %R32 = alloca float, align 4
  %R33 = alloca float, align 4
  %Q11 = alloca float, align 4
  %Q12 = alloca float, align 4
  %Q13 = alloca float, align 4
  %Q21 = alloca float, align 4
  %Q22 = alloca float, align 4
  %Q23 = alloca float, align 4
  %Q31 = alloca float, align 4
  %Q32 = alloca float, align 4
  %Q33 = alloca float, align 4
  %s = alloca float, align 4
  %s2 = alloca float, align 4
  %l = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %invert_normal = alloca i32, align 4
  %code = alloca i32, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %fudge2 = alloca float, align 4
  %pa = alloca %class.btVector3, align 4
  %sign = alloca float, align 4
  %pb = alloca %class.btVector3, align 4
  %alpha = alloca float, align 4
  %beta = alloca float, align 4
  %ua = alloca %class.btVector3, align 4
  %ub = alloca %class.btVector3, align 4
  %pointInWorld = alloca %class.btVector3, align 4
  %ref.tmp917 = alloca %class.btVector3, align 4
  %Ra = alloca float*, align 4
  %Rb = alloca float*, align 4
  %pa920 = alloca float*, align 4
  %pb921 = alloca float*, align 4
  %Sa = alloca float*, align 4
  %Sb = alloca float*, align 4
  %normal2 = alloca %class.btVector3, align 4
  %nr = alloca %class.btVector3, align 4
  %anr = alloca %class.btVector3, align 4
  %lanr = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %codeN = alloca i32, align 4
  %code1 = alloca i32, align 4
  %code2 = alloca i32, align 4
  %quad = alloca [8 x float], align 16
  %c1 = alloca float, align 4
  %c2 = alloca float, align 4
  %m11 = alloca float, align 4
  %m12 = alloca float, align 4
  %m21 = alloca float, align 4
  %m22 = alloca float, align 4
  %k1 = alloca float, align 4
  %k2 = alloca float, align 4
  %k3 = alloca float, align 4
  %k4 = alloca float, align 4
  %rect = alloca [2 x float], align 4
  %ret = alloca [16 x float], align 16
  %n = alloca i32, align 4
  %point = alloca [24 x float], align 16
  %dep = alloca [8 x float], align 16
  %det1 = alloca float, align 4
  %cnum = alloca i32, align 4
  %k11147 = alloca float, align 4
  %k21158 = alloca float, align 4
  %pointInWorld1233 = alloca %class.btVector3, align 4
  %ref.tmp1248 = alloca %class.btVector3, align 4
  %pointInWorld1260 = alloca %class.btVector3, align 4
  %ref.tmp1280 = alloca %class.btVector3, align 4
  %i1 = alloca i32, align 4
  %maxdepth = alloca float, align 4
  %iret = alloca [8 x i32], align 16
  %posInWorld = alloca %class.btVector3, align 4
  %ref.tmp1324 = alloca %class.btVector3, align 4
  %ref.tmp1331 = alloca %class.btVector3, align 4
  %ref.tmp1332 = alloca %class.btVector3, align 4
  %ref.tmp1333 = alloca %class.btVector3, align 4
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4, !tbaa !2
  store float* %R1, float** %R1.addr, align 4, !tbaa !2
  store %class.btVector3* %side1, %class.btVector3** %side1.addr, align 4, !tbaa !2
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4, !tbaa !2
  store float* %R2, float** %R2.addr, align 4, !tbaa !2
  store %class.btVector3* %side2, %class.btVector3** %side2.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  store float* %depth, float** %depth.addr, align 4, !tbaa !2
  store i32* %return_code, i32** %return_code.addr, align 4, !tbaa !2
  store i32 %maxc, i32* %maxc.addr, align 4, !tbaa !13
  store %struct.dContactGeom* %0, %struct.dContactGeom** %.addr, align 4, !tbaa !2
  store i32 %1, i32* %.addr1, align 4, !tbaa !13
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %2 = bitcast float* %fudge_factor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0x3FF0CCCCC0000000, float* %fudge_factor, align 4, !tbaa !11
  %3 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %4 = bitcast %class.btVector3* %pp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pp)
  %5 = bitcast %class.btVector3* %normalC to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !11
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !11
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !11
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %normalC, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %9 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float** %normalR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  store float* null, float** %normalR, align 4, !tbaa !2
  %13 = bitcast [3 x float]* %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %13) #9
  %14 = bitcast [3 x float]* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %14) #9
  %15 = bitcast float* %R11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %R12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %R13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %18 = bitcast float* %R21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = bitcast float* %R22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = bitcast float* %R23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %21 = bitcast float* %R31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %R32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %R33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %Q11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %Q12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %Q13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %Q21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  %28 = bitcast float* %Q22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  %29 = bitcast float* %Q23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = bitcast float* %Q31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %Q32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %Q33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %s2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #9
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #9
  %37 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = bitcast i32* %invert_normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  %39 = bitcast i32* %code to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #9
  %41 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %42 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %42)
  %43 = bitcast %class.btVector3* %p to i8*
  %44 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !15
  %45 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #9
  %46 = load float*, float** %R1.addr, align 4, !tbaa !2
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call8 = call float @_ZL6dDOT41PKfS0_(float* %46, float* %call7)
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx = getelementptr inbounds float, float* %call9, i32 0
  store float %call8, float* %arrayidx, align 4, !tbaa !11
  %47 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds float, float* %47, i32 1
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call11 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr, float* %call10)
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  store float %call11, float* %arrayidx13, align 4, !tbaa !11
  %48 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr14 = getelementptr inbounds float, float* %48, i32 2
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call16 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr14, float* %call15)
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 2
  store float %call16, float* %arrayidx18, align 4, !tbaa !11
  %49 = load %class.btVector3*, %class.btVector3** %side1.addr, align 4, !tbaa !2
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %49)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 0
  %50 = load float, float* %arrayidx20, align 4, !tbaa !11
  %mul = fmul float %50, 5.000000e-01
  %arrayidx21 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  store float %mul, float* %arrayidx21, align 4, !tbaa !11
  %51 = load %class.btVector3*, %class.btVector3** %side1.addr, align 4, !tbaa !2
  %call22 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %52 = load float, float* %arrayidx23, align 4, !tbaa !11
  %mul24 = fmul float %52, 5.000000e-01
  %arrayidx25 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  store float %mul24, float* %arrayidx25, align 4, !tbaa !11
  %53 = load %class.btVector3*, %class.btVector3** %side1.addr, align 4, !tbaa !2
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %53)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  %54 = load float, float* %arrayidx27, align 4, !tbaa !11
  %mul28 = fmul float %54, 5.000000e-01
  %arrayidx29 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  store float %mul28, float* %arrayidx29, align 4, !tbaa !11
  %55 = load %class.btVector3*, %class.btVector3** %side2.addr, align 4, !tbaa !2
  %call30 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %55)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 0
  %56 = load float, float* %arrayidx31, align 4, !tbaa !11
  %mul32 = fmul float %56, 5.000000e-01
  %arrayidx33 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  store float %mul32, float* %arrayidx33, align 4, !tbaa !11
  %57 = load %class.btVector3*, %class.btVector3** %side2.addr, align 4, !tbaa !2
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %57)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %58 = load float, float* %arrayidx35, align 4, !tbaa !11
  %mul36 = fmul float %58, 5.000000e-01
  %arrayidx37 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  store float %mul36, float* %arrayidx37, align 4, !tbaa !11
  %59 = load %class.btVector3*, %class.btVector3** %side2.addr, align 4, !tbaa !2
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %59)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %60 = load float, float* %arrayidx39, align 4, !tbaa !11
  %mul40 = fmul float %60, 5.000000e-01
  %arrayidx41 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  store float %mul40, float* %arrayidx41, align 4, !tbaa !11
  %61 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr42 = getelementptr inbounds float, float* %61, i32 0
  %62 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr43 = getelementptr inbounds float, float* %62, i32 0
  %call44 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr42, float* %add.ptr43)
  store float %call44, float* %R11, align 4, !tbaa !11
  %63 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr45 = getelementptr inbounds float, float* %63, i32 0
  %64 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr46 = getelementptr inbounds float, float* %64, i32 1
  %call47 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr45, float* %add.ptr46)
  store float %call47, float* %R12, align 4, !tbaa !11
  %65 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr48 = getelementptr inbounds float, float* %65, i32 0
  %66 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr49 = getelementptr inbounds float, float* %66, i32 2
  %call50 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr48, float* %add.ptr49)
  store float %call50, float* %R13, align 4, !tbaa !11
  %67 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr51 = getelementptr inbounds float, float* %67, i32 1
  %68 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr52 = getelementptr inbounds float, float* %68, i32 0
  %call53 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr51, float* %add.ptr52)
  store float %call53, float* %R21, align 4, !tbaa !11
  %69 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds float, float* %69, i32 1
  %70 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr55 = getelementptr inbounds float, float* %70, i32 1
  %call56 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr54, float* %add.ptr55)
  store float %call56, float* %R22, align 4, !tbaa !11
  %71 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr57 = getelementptr inbounds float, float* %71, i32 1
  %72 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr58 = getelementptr inbounds float, float* %72, i32 2
  %call59 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr57, float* %add.ptr58)
  store float %call59, float* %R23, align 4, !tbaa !11
  %73 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr60 = getelementptr inbounds float, float* %73, i32 2
  %74 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr61 = getelementptr inbounds float, float* %74, i32 0
  %call62 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr60, float* %add.ptr61)
  store float %call62, float* %R31, align 4, !tbaa !11
  %75 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr63 = getelementptr inbounds float, float* %75, i32 2
  %76 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr64 = getelementptr inbounds float, float* %76, i32 1
  %call65 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr63, float* %add.ptr64)
  store float %call65, float* %R32, align 4, !tbaa !11
  %77 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds float, float* %77, i32 2
  %78 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr67 = getelementptr inbounds float, float* %78, i32 2
  %call68 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr66, float* %add.ptr67)
  store float %call68, float* %R33, align 4, !tbaa !11
  %79 = load float, float* %R11, align 4, !tbaa !11
  %call69 = call float @_Z6btFabsf(float %79)
  store float %call69, float* %Q11, align 4, !tbaa !11
  %80 = load float, float* %R12, align 4, !tbaa !11
  %call70 = call float @_Z6btFabsf(float %80)
  store float %call70, float* %Q12, align 4, !tbaa !11
  %81 = load float, float* %R13, align 4, !tbaa !11
  %call71 = call float @_Z6btFabsf(float %81)
  store float %call71, float* %Q13, align 4, !tbaa !11
  %82 = load float, float* %R21, align 4, !tbaa !11
  %call72 = call float @_Z6btFabsf(float %82)
  store float %call72, float* %Q21, align 4, !tbaa !11
  %83 = load float, float* %R22, align 4, !tbaa !11
  %call73 = call float @_Z6btFabsf(float %83)
  store float %call73, float* %Q22, align 4, !tbaa !11
  %84 = load float, float* %R23, align 4, !tbaa !11
  %call74 = call float @_Z6btFabsf(float %84)
  store float %call74, float* %Q23, align 4, !tbaa !11
  %85 = load float, float* %R31, align 4, !tbaa !11
  %call75 = call float @_Z6btFabsf(float %85)
  store float %call75, float* %Q31, align 4, !tbaa !11
  %86 = load float, float* %R32, align 4, !tbaa !11
  %call76 = call float @_Z6btFabsf(float %86)
  store float %call76, float* %Q32, align 4, !tbaa !11
  %87 = load float, float* %R33, align 4, !tbaa !11
  %call77 = call float @_Z6btFabsf(float %87)
  store float %call77, float* %Q33, align 4, !tbaa !11
  store float 0xC7EFFFFFE0000000, float* %s, align 4, !tbaa !11
  store i32 0, i32* %invert_normal, align 4, !tbaa !13
  store i32 0, i32* %code, align 4, !tbaa !13
  %call78 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 0
  %88 = load float, float* %arrayidx79, align 4, !tbaa !11
  %call80 = call float @_Z6btFabsf(float %88)
  %arrayidx81 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %89 = load float, float* %arrayidx81, align 4, !tbaa !11
  %arrayidx82 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %90 = load float, float* %arrayidx82, align 4, !tbaa !11
  %91 = load float, float* %Q11, align 4, !tbaa !11
  %mul83 = fmul float %90, %91
  %add = fadd float %89, %mul83
  %arrayidx84 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %92 = load float, float* %arrayidx84, align 4, !tbaa !11
  %93 = load float, float* %Q12, align 4, !tbaa !11
  %mul85 = fmul float %92, %93
  %add86 = fadd float %add, %mul85
  %arrayidx87 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %94 = load float, float* %arrayidx87, align 4, !tbaa !11
  %95 = load float, float* %Q13, align 4, !tbaa !11
  %mul88 = fmul float %94, %95
  %add89 = fadd float %add86, %mul88
  %sub = fsub float %call80, %add89
  store float %sub, float* %s2, align 4, !tbaa !11
  %96 = load float, float* %s2, align 4, !tbaa !11
  %cmp = fcmp ogt float %96, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1376

if.end:                                           ; preds = %entry
  %97 = load float, float* %s2, align 4, !tbaa !11
  %98 = load float, float* %s, align 4, !tbaa !11
  %cmp90 = fcmp ogt float %97, %98
  br i1 %cmp90, label %if.then91, label %if.end96

if.then91:                                        ; preds = %if.end
  %99 = load float, float* %s2, align 4, !tbaa !11
  store float %99, float* %s, align 4, !tbaa !11
  %100 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr92 = getelementptr inbounds float, float* %100, i32 0
  store float* %add.ptr92, float** %normalR, align 4, !tbaa !2
  %call93 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 0
  %101 = load float, float* %arrayidx94, align 4, !tbaa !11
  %cmp95 = fcmp olt float %101, 0.000000e+00
  %conv = zext i1 %cmp95 to i32
  store i32 %conv, i32* %invert_normal, align 4, !tbaa !13
  store i32 1, i32* %code, align 4, !tbaa !13
  br label %if.end96

if.end96:                                         ; preds = %if.then91, %if.end
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 1
  %102 = load float, float* %arrayidx98, align 4, !tbaa !11
  %call99 = call float @_Z6btFabsf(float %102)
  %arrayidx100 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %103 = load float, float* %arrayidx100, align 4, !tbaa !11
  %arrayidx101 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %104 = load float, float* %arrayidx101, align 4, !tbaa !11
  %105 = load float, float* %Q21, align 4, !tbaa !11
  %mul102 = fmul float %104, %105
  %add103 = fadd float %103, %mul102
  %arrayidx104 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %106 = load float, float* %arrayidx104, align 4, !tbaa !11
  %107 = load float, float* %Q22, align 4, !tbaa !11
  %mul105 = fmul float %106, %107
  %add106 = fadd float %add103, %mul105
  %arrayidx107 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %108 = load float, float* %arrayidx107, align 4, !tbaa !11
  %109 = load float, float* %Q23, align 4, !tbaa !11
  %mul108 = fmul float %108, %109
  %add109 = fadd float %add106, %mul108
  %sub110 = fsub float %call99, %add109
  store float %sub110, float* %s2, align 4, !tbaa !11
  %110 = load float, float* %s2, align 4, !tbaa !11
  %cmp111 = fcmp ogt float %110, 0.000000e+00
  br i1 %cmp111, label %if.then112, label %if.end113

if.then112:                                       ; preds = %if.end96
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1376

if.end113:                                        ; preds = %if.end96
  %111 = load float, float* %s2, align 4, !tbaa !11
  %112 = load float, float* %s, align 4, !tbaa !11
  %cmp114 = fcmp ogt float %111, %112
  br i1 %cmp114, label %if.then115, label %if.end121

if.then115:                                       ; preds = %if.end113
  %113 = load float, float* %s2, align 4, !tbaa !11
  store float %113, float* %s, align 4, !tbaa !11
  %114 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr116 = getelementptr inbounds float, float* %114, i32 1
  store float* %add.ptr116, float** %normalR, align 4, !tbaa !2
  %call117 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 1
  %115 = load float, float* %arrayidx118, align 4, !tbaa !11
  %cmp119 = fcmp olt float %115, 0.000000e+00
  %conv120 = zext i1 %cmp119 to i32
  store i32 %conv120, i32* %invert_normal, align 4, !tbaa !13
  store i32 2, i32* %code, align 4, !tbaa !13
  br label %if.end121

if.end121:                                        ; preds = %if.then115, %if.end113
  %call122 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx123 = getelementptr inbounds float, float* %call122, i32 2
  %116 = load float, float* %arrayidx123, align 4, !tbaa !11
  %call124 = call float @_Z6btFabsf(float %116)
  %arrayidx125 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %117 = load float, float* %arrayidx125, align 4, !tbaa !11
  %arrayidx126 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %118 = load float, float* %arrayidx126, align 4, !tbaa !11
  %119 = load float, float* %Q31, align 4, !tbaa !11
  %mul127 = fmul float %118, %119
  %add128 = fadd float %117, %mul127
  %arrayidx129 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %120 = load float, float* %arrayidx129, align 4, !tbaa !11
  %121 = load float, float* %Q32, align 4, !tbaa !11
  %mul130 = fmul float %120, %121
  %add131 = fadd float %add128, %mul130
  %arrayidx132 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %122 = load float, float* %arrayidx132, align 4, !tbaa !11
  %123 = load float, float* %Q33, align 4, !tbaa !11
  %mul133 = fmul float %122, %123
  %add134 = fadd float %add131, %mul133
  %sub135 = fsub float %call124, %add134
  store float %sub135, float* %s2, align 4, !tbaa !11
  %124 = load float, float* %s2, align 4, !tbaa !11
  %cmp136 = fcmp ogt float %124, 0.000000e+00
  br i1 %cmp136, label %if.then137, label %if.end138

if.then137:                                       ; preds = %if.end121
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1376

if.end138:                                        ; preds = %if.end121
  %125 = load float, float* %s2, align 4, !tbaa !11
  %126 = load float, float* %s, align 4, !tbaa !11
  %cmp139 = fcmp ogt float %125, %126
  br i1 %cmp139, label %if.then140, label %if.end146

if.then140:                                       ; preds = %if.end138
  %127 = load float, float* %s2, align 4, !tbaa !11
  store float %127, float* %s, align 4, !tbaa !11
  %128 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr141 = getelementptr inbounds float, float* %128, i32 2
  store float* %add.ptr141, float** %normalR, align 4, !tbaa !2
  %call142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 2
  %129 = load float, float* %arrayidx143, align 4, !tbaa !11
  %cmp144 = fcmp olt float %129, 0.000000e+00
  %conv145 = zext i1 %cmp144 to i32
  store i32 %conv145, i32* %invert_normal, align 4, !tbaa !13
  store i32 3, i32* %code, align 4, !tbaa !13
  br label %if.end146

if.end146:                                        ; preds = %if.then140, %if.end138
  %130 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr147 = getelementptr inbounds float, float* %130, i32 0
  %call148 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call149 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr147, float* %call148)
  %call150 = call float @_Z6btFabsf(float %call149)
  %arrayidx151 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %131 = load float, float* %arrayidx151, align 4, !tbaa !11
  %132 = load float, float* %Q11, align 4, !tbaa !11
  %mul152 = fmul float %131, %132
  %arrayidx153 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %133 = load float, float* %arrayidx153, align 4, !tbaa !11
  %134 = load float, float* %Q21, align 4, !tbaa !11
  %mul154 = fmul float %133, %134
  %add155 = fadd float %mul152, %mul154
  %arrayidx156 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %135 = load float, float* %arrayidx156, align 4, !tbaa !11
  %136 = load float, float* %Q31, align 4, !tbaa !11
  %mul157 = fmul float %135, %136
  %add158 = fadd float %add155, %mul157
  %arrayidx159 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %137 = load float, float* %arrayidx159, align 4, !tbaa !11
  %add160 = fadd float %add158, %137
  %sub161 = fsub float %call150, %add160
  store float %sub161, float* %s2, align 4, !tbaa !11
  %138 = load float, float* %s2, align 4, !tbaa !11
  %cmp162 = fcmp ogt float %138, 0.000000e+00
  br i1 %cmp162, label %if.then163, label %if.end164

if.then163:                                       ; preds = %if.end146
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1376

if.end164:                                        ; preds = %if.end146
  %139 = load float, float* %s2, align 4, !tbaa !11
  %140 = load float, float* %s, align 4, !tbaa !11
  %cmp165 = fcmp ogt float %139, %140
  br i1 %cmp165, label %if.then166, label %if.end173

if.then166:                                       ; preds = %if.end164
  %141 = load float, float* %s2, align 4, !tbaa !11
  store float %141, float* %s, align 4, !tbaa !11
  %142 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr167 = getelementptr inbounds float, float* %142, i32 0
  store float* %add.ptr167, float** %normalR, align 4, !tbaa !2
  %143 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr168 = getelementptr inbounds float, float* %143, i32 0
  %call169 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call170 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr168, float* %call169)
  %cmp171 = fcmp olt float %call170, 0.000000e+00
  %conv172 = zext i1 %cmp171 to i32
  store i32 %conv172, i32* %invert_normal, align 4, !tbaa !13
  store i32 4, i32* %code, align 4, !tbaa !13
  br label %if.end173

if.end173:                                        ; preds = %if.then166, %if.end164
  %144 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr174 = getelementptr inbounds float, float* %144, i32 1
  %call175 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call176 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr174, float* %call175)
  %call177 = call float @_Z6btFabsf(float %call176)
  %arrayidx178 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %145 = load float, float* %arrayidx178, align 4, !tbaa !11
  %146 = load float, float* %Q12, align 4, !tbaa !11
  %mul179 = fmul float %145, %146
  %arrayidx180 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %147 = load float, float* %arrayidx180, align 4, !tbaa !11
  %148 = load float, float* %Q22, align 4, !tbaa !11
  %mul181 = fmul float %147, %148
  %add182 = fadd float %mul179, %mul181
  %arrayidx183 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %149 = load float, float* %arrayidx183, align 4, !tbaa !11
  %150 = load float, float* %Q32, align 4, !tbaa !11
  %mul184 = fmul float %149, %150
  %add185 = fadd float %add182, %mul184
  %arrayidx186 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %151 = load float, float* %arrayidx186, align 4, !tbaa !11
  %add187 = fadd float %add185, %151
  %sub188 = fsub float %call177, %add187
  store float %sub188, float* %s2, align 4, !tbaa !11
  %152 = load float, float* %s2, align 4, !tbaa !11
  %cmp189 = fcmp ogt float %152, 0.000000e+00
  br i1 %cmp189, label %if.then190, label %if.end191

if.then190:                                       ; preds = %if.end173
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1376

if.end191:                                        ; preds = %if.end173
  %153 = load float, float* %s2, align 4, !tbaa !11
  %154 = load float, float* %s, align 4, !tbaa !11
  %cmp192 = fcmp ogt float %153, %154
  br i1 %cmp192, label %if.then193, label %if.end200

if.then193:                                       ; preds = %if.end191
  %155 = load float, float* %s2, align 4, !tbaa !11
  store float %155, float* %s, align 4, !tbaa !11
  %156 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr194 = getelementptr inbounds float, float* %156, i32 1
  store float* %add.ptr194, float** %normalR, align 4, !tbaa !2
  %157 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr195 = getelementptr inbounds float, float* %157, i32 1
  %call196 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call197 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr195, float* %call196)
  %cmp198 = fcmp olt float %call197, 0.000000e+00
  %conv199 = zext i1 %cmp198 to i32
  store i32 %conv199, i32* %invert_normal, align 4, !tbaa !13
  store i32 5, i32* %code, align 4, !tbaa !13
  br label %if.end200

if.end200:                                        ; preds = %if.then193, %if.end191
  %158 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr201 = getelementptr inbounds float, float* %158, i32 2
  %call202 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call203 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr201, float* %call202)
  %call204 = call float @_Z6btFabsf(float %call203)
  %arrayidx205 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %159 = load float, float* %arrayidx205, align 4, !tbaa !11
  %160 = load float, float* %Q13, align 4, !tbaa !11
  %mul206 = fmul float %159, %160
  %arrayidx207 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %161 = load float, float* %arrayidx207, align 4, !tbaa !11
  %162 = load float, float* %Q23, align 4, !tbaa !11
  %mul208 = fmul float %161, %162
  %add209 = fadd float %mul206, %mul208
  %arrayidx210 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %163 = load float, float* %arrayidx210, align 4, !tbaa !11
  %164 = load float, float* %Q33, align 4, !tbaa !11
  %mul211 = fmul float %163, %164
  %add212 = fadd float %add209, %mul211
  %arrayidx213 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %165 = load float, float* %arrayidx213, align 4, !tbaa !11
  %add214 = fadd float %add212, %165
  %sub215 = fsub float %call204, %add214
  store float %sub215, float* %s2, align 4, !tbaa !11
  %166 = load float, float* %s2, align 4, !tbaa !11
  %cmp216 = fcmp ogt float %166, 0.000000e+00
  br i1 %cmp216, label %if.then217, label %if.end218

if.then217:                                       ; preds = %if.end200
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1376

if.end218:                                        ; preds = %if.end200
  %167 = load float, float* %s2, align 4, !tbaa !11
  %168 = load float, float* %s, align 4, !tbaa !11
  %cmp219 = fcmp ogt float %167, %168
  br i1 %cmp219, label %if.then220, label %if.end227

if.then220:                                       ; preds = %if.end218
  %169 = load float, float* %s2, align 4, !tbaa !11
  store float %169, float* %s, align 4, !tbaa !11
  %170 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr221 = getelementptr inbounds float, float* %170, i32 2
  store float* %add.ptr221, float** %normalR, align 4, !tbaa !2
  %171 = load float*, float** %R2.addr, align 4, !tbaa !2
  %add.ptr222 = getelementptr inbounds float, float* %171, i32 2
  %call223 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call224 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr222, float* %call223)
  %cmp225 = fcmp olt float %call224, 0.000000e+00
  %conv226 = zext i1 %cmp225 to i32
  store i32 %conv226, i32* %invert_normal, align 4, !tbaa !13
  store i32 6, i32* %code, align 4, !tbaa !13
  br label %if.end227

if.end227:                                        ; preds = %if.then220, %if.end218
  %172 = bitcast float* %fudge2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %172) #9
  store float 0x3EE4F8B580000000, float* %fudge2, align 4, !tbaa !11
  %173 = load float, float* %fudge2, align 4, !tbaa !11
  %174 = load float, float* %Q11, align 4, !tbaa !11
  %add228 = fadd float %174, %173
  store float %add228, float* %Q11, align 4, !tbaa !11
  %175 = load float, float* %fudge2, align 4, !tbaa !11
  %176 = load float, float* %Q12, align 4, !tbaa !11
  %add229 = fadd float %176, %175
  store float %add229, float* %Q12, align 4, !tbaa !11
  %177 = load float, float* %fudge2, align 4, !tbaa !11
  %178 = load float, float* %Q13, align 4, !tbaa !11
  %add230 = fadd float %178, %177
  store float %add230, float* %Q13, align 4, !tbaa !11
  %179 = load float, float* %fudge2, align 4, !tbaa !11
  %180 = load float, float* %Q21, align 4, !tbaa !11
  %add231 = fadd float %180, %179
  store float %add231, float* %Q21, align 4, !tbaa !11
  %181 = load float, float* %fudge2, align 4, !tbaa !11
  %182 = load float, float* %Q22, align 4, !tbaa !11
  %add232 = fadd float %182, %181
  store float %add232, float* %Q22, align 4, !tbaa !11
  %183 = load float, float* %fudge2, align 4, !tbaa !11
  %184 = load float, float* %Q23, align 4, !tbaa !11
  %add233 = fadd float %184, %183
  store float %add233, float* %Q23, align 4, !tbaa !11
  %185 = load float, float* %fudge2, align 4, !tbaa !11
  %186 = load float, float* %Q31, align 4, !tbaa !11
  %add234 = fadd float %186, %185
  store float %add234, float* %Q31, align 4, !tbaa !11
  %187 = load float, float* %fudge2, align 4, !tbaa !11
  %188 = load float, float* %Q32, align 4, !tbaa !11
  %add235 = fadd float %188, %187
  store float %add235, float* %Q32, align 4, !tbaa !11
  %189 = load float, float* %fudge2, align 4, !tbaa !11
  %190 = load float, float* %Q33, align 4, !tbaa !11
  %add236 = fadd float %190, %189
  store float %add236, float* %Q33, align 4, !tbaa !11
  %call237 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx238 = getelementptr inbounds float, float* %call237, i32 2
  %191 = load float, float* %arrayidx238, align 4, !tbaa !11
  %192 = load float, float* %R21, align 4, !tbaa !11
  %mul239 = fmul float %191, %192
  %call240 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 1
  %193 = load float, float* %arrayidx241, align 4, !tbaa !11
  %194 = load float, float* %R31, align 4, !tbaa !11
  %mul242 = fmul float %193, %194
  %sub243 = fsub float %mul239, %mul242
  %call244 = call float @_Z6btFabsf(float %sub243)
  %arrayidx245 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %195 = load float, float* %arrayidx245, align 4, !tbaa !11
  %196 = load float, float* %Q31, align 4, !tbaa !11
  %mul246 = fmul float %195, %196
  %arrayidx247 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %197 = load float, float* %arrayidx247, align 4, !tbaa !11
  %198 = load float, float* %Q21, align 4, !tbaa !11
  %mul248 = fmul float %197, %198
  %add249 = fadd float %mul246, %mul248
  %arrayidx250 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %199 = load float, float* %arrayidx250, align 4, !tbaa !11
  %200 = load float, float* %Q13, align 4, !tbaa !11
  %mul251 = fmul float %199, %200
  %add252 = fadd float %add249, %mul251
  %arrayidx253 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %201 = load float, float* %arrayidx253, align 4, !tbaa !11
  %202 = load float, float* %Q12, align 4, !tbaa !11
  %mul254 = fmul float %201, %202
  %add255 = fadd float %add252, %mul254
  %sub256 = fsub float %call244, %add255
  store float %sub256, float* %s2, align 4, !tbaa !11
  %203 = load float, float* %s2, align 4, !tbaa !11
  %cmp257 = fcmp ogt float %203, 0x3E80000000000000
  br i1 %cmp257, label %if.then258, label %if.end259

if.then258:                                       ; preds = %if.end227
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end259:                                        ; preds = %if.end227
  %204 = load float, float* %R31, align 4, !tbaa !11
  %fneg = fneg float %204
  %205 = load float, float* %R31, align 4, !tbaa !11
  %fneg260 = fneg float %205
  %mul261 = fmul float %fneg, %fneg260
  %add262 = fadd float 0.000000e+00, %mul261
  %206 = load float, float* %R21, align 4, !tbaa !11
  %207 = load float, float* %R21, align 4, !tbaa !11
  %mul263 = fmul float %206, %207
  %add264 = fadd float %add262, %mul263
  %call265 = call float @_Z6btSqrtf(float %add264)
  store float %call265, float* %l, align 4, !tbaa !11
  %208 = load float, float* %l, align 4, !tbaa !11
  %cmp266 = fcmp ogt float %208, 0x3E80000000000000
  br i1 %cmp266, label %if.then267, label %if.end291

if.then267:                                       ; preds = %if.end259
  %209 = load float, float* %l, align 4, !tbaa !11
  %210 = load float, float* %s2, align 4, !tbaa !11
  %div = fdiv float %210, %209
  store float %div, float* %s2, align 4, !tbaa !11
  %211 = load float, float* %s2, align 4, !tbaa !11
  %mul268 = fmul float %211, 0x3FF0CCCCC0000000
  %212 = load float, float* %s, align 4, !tbaa !11
  %cmp269 = fcmp ogt float %mul268, %212
  br i1 %cmp269, label %if.then270, label %if.end290

if.then270:                                       ; preds = %if.then267
  %213 = load float, float* %s2, align 4, !tbaa !11
  store float %213, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %214 = load float, float* %l, align 4, !tbaa !11
  %div271 = fdiv float 0.000000e+00, %214
  %call272 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx273 = getelementptr inbounds float, float* %call272, i32 0
  store float %div271, float* %arrayidx273, align 4, !tbaa !11
  %215 = load float, float* %R31, align 4, !tbaa !11
  %fneg274 = fneg float %215
  %216 = load float, float* %l, align 4, !tbaa !11
  %div275 = fdiv float %fneg274, %216
  %call276 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx277 = getelementptr inbounds float, float* %call276, i32 1
  store float %div275, float* %arrayidx277, align 4, !tbaa !11
  %217 = load float, float* %R21, align 4, !tbaa !11
  %218 = load float, float* %l, align 4, !tbaa !11
  %div278 = fdiv float %217, %218
  %call279 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx280 = getelementptr inbounds float, float* %call279, i32 2
  store float %div278, float* %arrayidx280, align 4, !tbaa !11
  %call281 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx282 = getelementptr inbounds float, float* %call281, i32 2
  %219 = load float, float* %arrayidx282, align 4, !tbaa !11
  %220 = load float, float* %R21, align 4, !tbaa !11
  %mul283 = fmul float %219, %220
  %call284 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx285 = getelementptr inbounds float, float* %call284, i32 1
  %221 = load float, float* %arrayidx285, align 4, !tbaa !11
  %222 = load float, float* %R31, align 4, !tbaa !11
  %mul286 = fmul float %221, %222
  %sub287 = fsub float %mul283, %mul286
  %cmp288 = fcmp olt float %sub287, 0.000000e+00
  %conv289 = zext i1 %cmp288 to i32
  store i32 %conv289, i32* %invert_normal, align 4, !tbaa !13
  store i32 7, i32* %code, align 4, !tbaa !13
  br label %if.end290

if.end290:                                        ; preds = %if.then270, %if.then267
  br label %if.end291

if.end291:                                        ; preds = %if.end290, %if.end259
  %call292 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx293 = getelementptr inbounds float, float* %call292, i32 2
  %223 = load float, float* %arrayidx293, align 4, !tbaa !11
  %224 = load float, float* %R22, align 4, !tbaa !11
  %mul294 = fmul float %223, %224
  %call295 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx296 = getelementptr inbounds float, float* %call295, i32 1
  %225 = load float, float* %arrayidx296, align 4, !tbaa !11
  %226 = load float, float* %R32, align 4, !tbaa !11
  %mul297 = fmul float %225, %226
  %sub298 = fsub float %mul294, %mul297
  %call299 = call float @_Z6btFabsf(float %sub298)
  %arrayidx300 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %227 = load float, float* %arrayidx300, align 4, !tbaa !11
  %228 = load float, float* %Q32, align 4, !tbaa !11
  %mul301 = fmul float %227, %228
  %arrayidx302 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %229 = load float, float* %arrayidx302, align 4, !tbaa !11
  %230 = load float, float* %Q22, align 4, !tbaa !11
  %mul303 = fmul float %229, %230
  %add304 = fadd float %mul301, %mul303
  %arrayidx305 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %231 = load float, float* %arrayidx305, align 4, !tbaa !11
  %232 = load float, float* %Q13, align 4, !tbaa !11
  %mul306 = fmul float %231, %232
  %add307 = fadd float %add304, %mul306
  %arrayidx308 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %233 = load float, float* %arrayidx308, align 4, !tbaa !11
  %234 = load float, float* %Q11, align 4, !tbaa !11
  %mul309 = fmul float %233, %234
  %add310 = fadd float %add307, %mul309
  %sub311 = fsub float %call299, %add310
  store float %sub311, float* %s2, align 4, !tbaa !11
  %235 = load float, float* %s2, align 4, !tbaa !11
  %cmp312 = fcmp ogt float %235, 0x3E80000000000000
  br i1 %cmp312, label %if.then313, label %if.end314

if.then313:                                       ; preds = %if.end291
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end314:                                        ; preds = %if.end291
  %236 = load float, float* %R32, align 4, !tbaa !11
  %fneg315 = fneg float %236
  %237 = load float, float* %R32, align 4, !tbaa !11
  %fneg316 = fneg float %237
  %mul317 = fmul float %fneg315, %fneg316
  %add318 = fadd float 0.000000e+00, %mul317
  %238 = load float, float* %R22, align 4, !tbaa !11
  %239 = load float, float* %R22, align 4, !tbaa !11
  %mul319 = fmul float %238, %239
  %add320 = fadd float %add318, %mul319
  %call321 = call float @_Z6btSqrtf(float %add320)
  store float %call321, float* %l, align 4, !tbaa !11
  %240 = load float, float* %l, align 4, !tbaa !11
  %cmp322 = fcmp ogt float %240, 0x3E80000000000000
  br i1 %cmp322, label %if.then323, label %if.end348

if.then323:                                       ; preds = %if.end314
  %241 = load float, float* %l, align 4, !tbaa !11
  %242 = load float, float* %s2, align 4, !tbaa !11
  %div324 = fdiv float %242, %241
  store float %div324, float* %s2, align 4, !tbaa !11
  %243 = load float, float* %s2, align 4, !tbaa !11
  %mul325 = fmul float %243, 0x3FF0CCCCC0000000
  %244 = load float, float* %s, align 4, !tbaa !11
  %cmp326 = fcmp ogt float %mul325, %244
  br i1 %cmp326, label %if.then327, label %if.end347

if.then327:                                       ; preds = %if.then323
  %245 = load float, float* %s2, align 4, !tbaa !11
  store float %245, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %246 = load float, float* %l, align 4, !tbaa !11
  %div328 = fdiv float 0.000000e+00, %246
  %call329 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx330 = getelementptr inbounds float, float* %call329, i32 0
  store float %div328, float* %arrayidx330, align 4, !tbaa !11
  %247 = load float, float* %R32, align 4, !tbaa !11
  %fneg331 = fneg float %247
  %248 = load float, float* %l, align 4, !tbaa !11
  %div332 = fdiv float %fneg331, %248
  %call333 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx334 = getelementptr inbounds float, float* %call333, i32 1
  store float %div332, float* %arrayidx334, align 4, !tbaa !11
  %249 = load float, float* %R22, align 4, !tbaa !11
  %250 = load float, float* %l, align 4, !tbaa !11
  %div335 = fdiv float %249, %250
  %call336 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx337 = getelementptr inbounds float, float* %call336, i32 2
  store float %div335, float* %arrayidx337, align 4, !tbaa !11
  %call338 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx339 = getelementptr inbounds float, float* %call338, i32 2
  %251 = load float, float* %arrayidx339, align 4, !tbaa !11
  %252 = load float, float* %R22, align 4, !tbaa !11
  %mul340 = fmul float %251, %252
  %call341 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx342 = getelementptr inbounds float, float* %call341, i32 1
  %253 = load float, float* %arrayidx342, align 4, !tbaa !11
  %254 = load float, float* %R32, align 4, !tbaa !11
  %mul343 = fmul float %253, %254
  %sub344 = fsub float %mul340, %mul343
  %cmp345 = fcmp olt float %sub344, 0.000000e+00
  %conv346 = zext i1 %cmp345 to i32
  store i32 %conv346, i32* %invert_normal, align 4, !tbaa !13
  store i32 8, i32* %code, align 4, !tbaa !13
  br label %if.end347

if.end347:                                        ; preds = %if.then327, %if.then323
  br label %if.end348

if.end348:                                        ; preds = %if.end347, %if.end314
  %call349 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx350 = getelementptr inbounds float, float* %call349, i32 2
  %255 = load float, float* %arrayidx350, align 4, !tbaa !11
  %256 = load float, float* %R23, align 4, !tbaa !11
  %mul351 = fmul float %255, %256
  %call352 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx353 = getelementptr inbounds float, float* %call352, i32 1
  %257 = load float, float* %arrayidx353, align 4, !tbaa !11
  %258 = load float, float* %R33, align 4, !tbaa !11
  %mul354 = fmul float %257, %258
  %sub355 = fsub float %mul351, %mul354
  %call356 = call float @_Z6btFabsf(float %sub355)
  %arrayidx357 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %259 = load float, float* %arrayidx357, align 4, !tbaa !11
  %260 = load float, float* %Q33, align 4, !tbaa !11
  %mul358 = fmul float %259, %260
  %arrayidx359 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %261 = load float, float* %arrayidx359, align 4, !tbaa !11
  %262 = load float, float* %Q23, align 4, !tbaa !11
  %mul360 = fmul float %261, %262
  %add361 = fadd float %mul358, %mul360
  %arrayidx362 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %263 = load float, float* %arrayidx362, align 4, !tbaa !11
  %264 = load float, float* %Q12, align 4, !tbaa !11
  %mul363 = fmul float %263, %264
  %add364 = fadd float %add361, %mul363
  %arrayidx365 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %265 = load float, float* %arrayidx365, align 4, !tbaa !11
  %266 = load float, float* %Q11, align 4, !tbaa !11
  %mul366 = fmul float %265, %266
  %add367 = fadd float %add364, %mul366
  %sub368 = fsub float %call356, %add367
  store float %sub368, float* %s2, align 4, !tbaa !11
  %267 = load float, float* %s2, align 4, !tbaa !11
  %cmp369 = fcmp ogt float %267, 0x3E80000000000000
  br i1 %cmp369, label %if.then370, label %if.end371

if.then370:                                       ; preds = %if.end348
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end371:                                        ; preds = %if.end348
  %268 = load float, float* %R33, align 4, !tbaa !11
  %fneg372 = fneg float %268
  %269 = load float, float* %R33, align 4, !tbaa !11
  %fneg373 = fneg float %269
  %mul374 = fmul float %fneg372, %fneg373
  %add375 = fadd float 0.000000e+00, %mul374
  %270 = load float, float* %R23, align 4, !tbaa !11
  %271 = load float, float* %R23, align 4, !tbaa !11
  %mul376 = fmul float %270, %271
  %add377 = fadd float %add375, %mul376
  %call378 = call float @_Z6btSqrtf(float %add377)
  store float %call378, float* %l, align 4, !tbaa !11
  %272 = load float, float* %l, align 4, !tbaa !11
  %cmp379 = fcmp ogt float %272, 0x3E80000000000000
  br i1 %cmp379, label %if.then380, label %if.end405

if.then380:                                       ; preds = %if.end371
  %273 = load float, float* %l, align 4, !tbaa !11
  %274 = load float, float* %s2, align 4, !tbaa !11
  %div381 = fdiv float %274, %273
  store float %div381, float* %s2, align 4, !tbaa !11
  %275 = load float, float* %s2, align 4, !tbaa !11
  %mul382 = fmul float %275, 0x3FF0CCCCC0000000
  %276 = load float, float* %s, align 4, !tbaa !11
  %cmp383 = fcmp ogt float %mul382, %276
  br i1 %cmp383, label %if.then384, label %if.end404

if.then384:                                       ; preds = %if.then380
  %277 = load float, float* %s2, align 4, !tbaa !11
  store float %277, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %278 = load float, float* %l, align 4, !tbaa !11
  %div385 = fdiv float 0.000000e+00, %278
  %call386 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx387 = getelementptr inbounds float, float* %call386, i32 0
  store float %div385, float* %arrayidx387, align 4, !tbaa !11
  %279 = load float, float* %R33, align 4, !tbaa !11
  %fneg388 = fneg float %279
  %280 = load float, float* %l, align 4, !tbaa !11
  %div389 = fdiv float %fneg388, %280
  %call390 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx391 = getelementptr inbounds float, float* %call390, i32 1
  store float %div389, float* %arrayidx391, align 4, !tbaa !11
  %281 = load float, float* %R23, align 4, !tbaa !11
  %282 = load float, float* %l, align 4, !tbaa !11
  %div392 = fdiv float %281, %282
  %call393 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx394 = getelementptr inbounds float, float* %call393, i32 2
  store float %div392, float* %arrayidx394, align 4, !tbaa !11
  %call395 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx396 = getelementptr inbounds float, float* %call395, i32 2
  %283 = load float, float* %arrayidx396, align 4, !tbaa !11
  %284 = load float, float* %R23, align 4, !tbaa !11
  %mul397 = fmul float %283, %284
  %call398 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx399 = getelementptr inbounds float, float* %call398, i32 1
  %285 = load float, float* %arrayidx399, align 4, !tbaa !11
  %286 = load float, float* %R33, align 4, !tbaa !11
  %mul400 = fmul float %285, %286
  %sub401 = fsub float %mul397, %mul400
  %cmp402 = fcmp olt float %sub401, 0.000000e+00
  %conv403 = zext i1 %cmp402 to i32
  store i32 %conv403, i32* %invert_normal, align 4, !tbaa !13
  store i32 9, i32* %code, align 4, !tbaa !13
  br label %if.end404

if.end404:                                        ; preds = %if.then384, %if.then380
  br label %if.end405

if.end405:                                        ; preds = %if.end404, %if.end371
  %call406 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx407 = getelementptr inbounds float, float* %call406, i32 0
  %287 = load float, float* %arrayidx407, align 4, !tbaa !11
  %288 = load float, float* %R31, align 4, !tbaa !11
  %mul408 = fmul float %287, %288
  %call409 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx410 = getelementptr inbounds float, float* %call409, i32 2
  %289 = load float, float* %arrayidx410, align 4, !tbaa !11
  %290 = load float, float* %R11, align 4, !tbaa !11
  %mul411 = fmul float %289, %290
  %sub412 = fsub float %mul408, %mul411
  %call413 = call float @_Z6btFabsf(float %sub412)
  %arrayidx414 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %291 = load float, float* %arrayidx414, align 4, !tbaa !11
  %292 = load float, float* %Q31, align 4, !tbaa !11
  %mul415 = fmul float %291, %292
  %arrayidx416 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %293 = load float, float* %arrayidx416, align 4, !tbaa !11
  %294 = load float, float* %Q11, align 4, !tbaa !11
  %mul417 = fmul float %293, %294
  %add418 = fadd float %mul415, %mul417
  %arrayidx419 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %295 = load float, float* %arrayidx419, align 4, !tbaa !11
  %296 = load float, float* %Q23, align 4, !tbaa !11
  %mul420 = fmul float %295, %296
  %add421 = fadd float %add418, %mul420
  %arrayidx422 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %297 = load float, float* %arrayidx422, align 4, !tbaa !11
  %298 = load float, float* %Q22, align 4, !tbaa !11
  %mul423 = fmul float %297, %298
  %add424 = fadd float %add421, %mul423
  %sub425 = fsub float %call413, %add424
  store float %sub425, float* %s2, align 4, !tbaa !11
  %299 = load float, float* %s2, align 4, !tbaa !11
  %cmp426 = fcmp ogt float %299, 0x3E80000000000000
  br i1 %cmp426, label %if.then427, label %if.end428

if.then427:                                       ; preds = %if.end405
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end428:                                        ; preds = %if.end405
  %300 = load float, float* %R31, align 4, !tbaa !11
  %301 = load float, float* %R31, align 4, !tbaa !11
  %mul429 = fmul float %300, %301
  %add430 = fadd float %mul429, 0.000000e+00
  %302 = load float, float* %R11, align 4, !tbaa !11
  %fneg431 = fneg float %302
  %303 = load float, float* %R11, align 4, !tbaa !11
  %fneg432 = fneg float %303
  %mul433 = fmul float %fneg431, %fneg432
  %add434 = fadd float %add430, %mul433
  %call435 = call float @_Z6btSqrtf(float %add434)
  store float %call435, float* %l, align 4, !tbaa !11
  %304 = load float, float* %l, align 4, !tbaa !11
  %cmp436 = fcmp ogt float %304, 0x3E80000000000000
  br i1 %cmp436, label %if.then437, label %if.end462

if.then437:                                       ; preds = %if.end428
  %305 = load float, float* %l, align 4, !tbaa !11
  %306 = load float, float* %s2, align 4, !tbaa !11
  %div438 = fdiv float %306, %305
  store float %div438, float* %s2, align 4, !tbaa !11
  %307 = load float, float* %s2, align 4, !tbaa !11
  %mul439 = fmul float %307, 0x3FF0CCCCC0000000
  %308 = load float, float* %s, align 4, !tbaa !11
  %cmp440 = fcmp ogt float %mul439, %308
  br i1 %cmp440, label %if.then441, label %if.end461

if.then441:                                       ; preds = %if.then437
  %309 = load float, float* %s2, align 4, !tbaa !11
  store float %309, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %310 = load float, float* %R31, align 4, !tbaa !11
  %311 = load float, float* %l, align 4, !tbaa !11
  %div442 = fdiv float %310, %311
  %call443 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx444 = getelementptr inbounds float, float* %call443, i32 0
  store float %div442, float* %arrayidx444, align 4, !tbaa !11
  %312 = load float, float* %l, align 4, !tbaa !11
  %div445 = fdiv float 0.000000e+00, %312
  %call446 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx447 = getelementptr inbounds float, float* %call446, i32 1
  store float %div445, float* %arrayidx447, align 4, !tbaa !11
  %313 = load float, float* %R11, align 4, !tbaa !11
  %fneg448 = fneg float %313
  %314 = load float, float* %l, align 4, !tbaa !11
  %div449 = fdiv float %fneg448, %314
  %call450 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx451 = getelementptr inbounds float, float* %call450, i32 2
  store float %div449, float* %arrayidx451, align 4, !tbaa !11
  %call452 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx453 = getelementptr inbounds float, float* %call452, i32 0
  %315 = load float, float* %arrayidx453, align 4, !tbaa !11
  %316 = load float, float* %R31, align 4, !tbaa !11
  %mul454 = fmul float %315, %316
  %call455 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx456 = getelementptr inbounds float, float* %call455, i32 2
  %317 = load float, float* %arrayidx456, align 4, !tbaa !11
  %318 = load float, float* %R11, align 4, !tbaa !11
  %mul457 = fmul float %317, %318
  %sub458 = fsub float %mul454, %mul457
  %cmp459 = fcmp olt float %sub458, 0.000000e+00
  %conv460 = zext i1 %cmp459 to i32
  store i32 %conv460, i32* %invert_normal, align 4, !tbaa !13
  store i32 10, i32* %code, align 4, !tbaa !13
  br label %if.end461

if.end461:                                        ; preds = %if.then441, %if.then437
  br label %if.end462

if.end462:                                        ; preds = %if.end461, %if.end428
  %call463 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx464 = getelementptr inbounds float, float* %call463, i32 0
  %319 = load float, float* %arrayidx464, align 4, !tbaa !11
  %320 = load float, float* %R32, align 4, !tbaa !11
  %mul465 = fmul float %319, %320
  %call466 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx467 = getelementptr inbounds float, float* %call466, i32 2
  %321 = load float, float* %arrayidx467, align 4, !tbaa !11
  %322 = load float, float* %R12, align 4, !tbaa !11
  %mul468 = fmul float %321, %322
  %sub469 = fsub float %mul465, %mul468
  %call470 = call float @_Z6btFabsf(float %sub469)
  %arrayidx471 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %323 = load float, float* %arrayidx471, align 4, !tbaa !11
  %324 = load float, float* %Q32, align 4, !tbaa !11
  %mul472 = fmul float %323, %324
  %arrayidx473 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %325 = load float, float* %arrayidx473, align 4, !tbaa !11
  %326 = load float, float* %Q12, align 4, !tbaa !11
  %mul474 = fmul float %325, %326
  %add475 = fadd float %mul472, %mul474
  %arrayidx476 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %327 = load float, float* %arrayidx476, align 4, !tbaa !11
  %328 = load float, float* %Q23, align 4, !tbaa !11
  %mul477 = fmul float %327, %328
  %add478 = fadd float %add475, %mul477
  %arrayidx479 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %329 = load float, float* %arrayidx479, align 4, !tbaa !11
  %330 = load float, float* %Q21, align 4, !tbaa !11
  %mul480 = fmul float %329, %330
  %add481 = fadd float %add478, %mul480
  %sub482 = fsub float %call470, %add481
  store float %sub482, float* %s2, align 4, !tbaa !11
  %331 = load float, float* %s2, align 4, !tbaa !11
  %cmp483 = fcmp ogt float %331, 0x3E80000000000000
  br i1 %cmp483, label %if.then484, label %if.end485

if.then484:                                       ; preds = %if.end462
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end485:                                        ; preds = %if.end462
  %332 = load float, float* %R32, align 4, !tbaa !11
  %333 = load float, float* %R32, align 4, !tbaa !11
  %mul486 = fmul float %332, %333
  %add487 = fadd float %mul486, 0.000000e+00
  %334 = load float, float* %R12, align 4, !tbaa !11
  %fneg488 = fneg float %334
  %335 = load float, float* %R12, align 4, !tbaa !11
  %fneg489 = fneg float %335
  %mul490 = fmul float %fneg488, %fneg489
  %add491 = fadd float %add487, %mul490
  %call492 = call float @_Z6btSqrtf(float %add491)
  store float %call492, float* %l, align 4, !tbaa !11
  %336 = load float, float* %l, align 4, !tbaa !11
  %cmp493 = fcmp ogt float %336, 0x3E80000000000000
  br i1 %cmp493, label %if.then494, label %if.end519

if.then494:                                       ; preds = %if.end485
  %337 = load float, float* %l, align 4, !tbaa !11
  %338 = load float, float* %s2, align 4, !tbaa !11
  %div495 = fdiv float %338, %337
  store float %div495, float* %s2, align 4, !tbaa !11
  %339 = load float, float* %s2, align 4, !tbaa !11
  %mul496 = fmul float %339, 0x3FF0CCCCC0000000
  %340 = load float, float* %s, align 4, !tbaa !11
  %cmp497 = fcmp ogt float %mul496, %340
  br i1 %cmp497, label %if.then498, label %if.end518

if.then498:                                       ; preds = %if.then494
  %341 = load float, float* %s2, align 4, !tbaa !11
  store float %341, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %342 = load float, float* %R32, align 4, !tbaa !11
  %343 = load float, float* %l, align 4, !tbaa !11
  %div499 = fdiv float %342, %343
  %call500 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx501 = getelementptr inbounds float, float* %call500, i32 0
  store float %div499, float* %arrayidx501, align 4, !tbaa !11
  %344 = load float, float* %l, align 4, !tbaa !11
  %div502 = fdiv float 0.000000e+00, %344
  %call503 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx504 = getelementptr inbounds float, float* %call503, i32 1
  store float %div502, float* %arrayidx504, align 4, !tbaa !11
  %345 = load float, float* %R12, align 4, !tbaa !11
  %fneg505 = fneg float %345
  %346 = load float, float* %l, align 4, !tbaa !11
  %div506 = fdiv float %fneg505, %346
  %call507 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx508 = getelementptr inbounds float, float* %call507, i32 2
  store float %div506, float* %arrayidx508, align 4, !tbaa !11
  %call509 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx510 = getelementptr inbounds float, float* %call509, i32 0
  %347 = load float, float* %arrayidx510, align 4, !tbaa !11
  %348 = load float, float* %R32, align 4, !tbaa !11
  %mul511 = fmul float %347, %348
  %call512 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx513 = getelementptr inbounds float, float* %call512, i32 2
  %349 = load float, float* %arrayidx513, align 4, !tbaa !11
  %350 = load float, float* %R12, align 4, !tbaa !11
  %mul514 = fmul float %349, %350
  %sub515 = fsub float %mul511, %mul514
  %cmp516 = fcmp olt float %sub515, 0.000000e+00
  %conv517 = zext i1 %cmp516 to i32
  store i32 %conv517, i32* %invert_normal, align 4, !tbaa !13
  store i32 11, i32* %code, align 4, !tbaa !13
  br label %if.end518

if.end518:                                        ; preds = %if.then498, %if.then494
  br label %if.end519

if.end519:                                        ; preds = %if.end518, %if.end485
  %call520 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx521 = getelementptr inbounds float, float* %call520, i32 0
  %351 = load float, float* %arrayidx521, align 4, !tbaa !11
  %352 = load float, float* %R33, align 4, !tbaa !11
  %mul522 = fmul float %351, %352
  %call523 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx524 = getelementptr inbounds float, float* %call523, i32 2
  %353 = load float, float* %arrayidx524, align 4, !tbaa !11
  %354 = load float, float* %R13, align 4, !tbaa !11
  %mul525 = fmul float %353, %354
  %sub526 = fsub float %mul522, %mul525
  %call527 = call float @_Z6btFabsf(float %sub526)
  %arrayidx528 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %355 = load float, float* %arrayidx528, align 4, !tbaa !11
  %356 = load float, float* %Q33, align 4, !tbaa !11
  %mul529 = fmul float %355, %356
  %arrayidx530 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %357 = load float, float* %arrayidx530, align 4, !tbaa !11
  %358 = load float, float* %Q13, align 4, !tbaa !11
  %mul531 = fmul float %357, %358
  %add532 = fadd float %mul529, %mul531
  %arrayidx533 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %359 = load float, float* %arrayidx533, align 4, !tbaa !11
  %360 = load float, float* %Q22, align 4, !tbaa !11
  %mul534 = fmul float %359, %360
  %add535 = fadd float %add532, %mul534
  %arrayidx536 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %361 = load float, float* %arrayidx536, align 4, !tbaa !11
  %362 = load float, float* %Q21, align 4, !tbaa !11
  %mul537 = fmul float %361, %362
  %add538 = fadd float %add535, %mul537
  %sub539 = fsub float %call527, %add538
  store float %sub539, float* %s2, align 4, !tbaa !11
  %363 = load float, float* %s2, align 4, !tbaa !11
  %cmp540 = fcmp ogt float %363, 0x3E80000000000000
  br i1 %cmp540, label %if.then541, label %if.end542

if.then541:                                       ; preds = %if.end519
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end542:                                        ; preds = %if.end519
  %364 = load float, float* %R33, align 4, !tbaa !11
  %365 = load float, float* %R33, align 4, !tbaa !11
  %mul543 = fmul float %364, %365
  %add544 = fadd float %mul543, 0.000000e+00
  %366 = load float, float* %R13, align 4, !tbaa !11
  %fneg545 = fneg float %366
  %367 = load float, float* %R13, align 4, !tbaa !11
  %fneg546 = fneg float %367
  %mul547 = fmul float %fneg545, %fneg546
  %add548 = fadd float %add544, %mul547
  %call549 = call float @_Z6btSqrtf(float %add548)
  store float %call549, float* %l, align 4, !tbaa !11
  %368 = load float, float* %l, align 4, !tbaa !11
  %cmp550 = fcmp ogt float %368, 0x3E80000000000000
  br i1 %cmp550, label %if.then551, label %if.end576

if.then551:                                       ; preds = %if.end542
  %369 = load float, float* %l, align 4, !tbaa !11
  %370 = load float, float* %s2, align 4, !tbaa !11
  %div552 = fdiv float %370, %369
  store float %div552, float* %s2, align 4, !tbaa !11
  %371 = load float, float* %s2, align 4, !tbaa !11
  %mul553 = fmul float %371, 0x3FF0CCCCC0000000
  %372 = load float, float* %s, align 4, !tbaa !11
  %cmp554 = fcmp ogt float %mul553, %372
  br i1 %cmp554, label %if.then555, label %if.end575

if.then555:                                       ; preds = %if.then551
  %373 = load float, float* %s2, align 4, !tbaa !11
  store float %373, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %374 = load float, float* %R33, align 4, !tbaa !11
  %375 = load float, float* %l, align 4, !tbaa !11
  %div556 = fdiv float %374, %375
  %call557 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx558 = getelementptr inbounds float, float* %call557, i32 0
  store float %div556, float* %arrayidx558, align 4, !tbaa !11
  %376 = load float, float* %l, align 4, !tbaa !11
  %div559 = fdiv float 0.000000e+00, %376
  %call560 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx561 = getelementptr inbounds float, float* %call560, i32 1
  store float %div559, float* %arrayidx561, align 4, !tbaa !11
  %377 = load float, float* %R13, align 4, !tbaa !11
  %fneg562 = fneg float %377
  %378 = load float, float* %l, align 4, !tbaa !11
  %div563 = fdiv float %fneg562, %378
  %call564 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx565 = getelementptr inbounds float, float* %call564, i32 2
  store float %div563, float* %arrayidx565, align 4, !tbaa !11
  %call566 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx567 = getelementptr inbounds float, float* %call566, i32 0
  %379 = load float, float* %arrayidx567, align 4, !tbaa !11
  %380 = load float, float* %R33, align 4, !tbaa !11
  %mul568 = fmul float %379, %380
  %call569 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx570 = getelementptr inbounds float, float* %call569, i32 2
  %381 = load float, float* %arrayidx570, align 4, !tbaa !11
  %382 = load float, float* %R13, align 4, !tbaa !11
  %mul571 = fmul float %381, %382
  %sub572 = fsub float %mul568, %mul571
  %cmp573 = fcmp olt float %sub572, 0.000000e+00
  %conv574 = zext i1 %cmp573 to i32
  store i32 %conv574, i32* %invert_normal, align 4, !tbaa !13
  store i32 12, i32* %code, align 4, !tbaa !13
  br label %if.end575

if.end575:                                        ; preds = %if.then555, %if.then551
  br label %if.end576

if.end576:                                        ; preds = %if.end575, %if.end542
  %call577 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx578 = getelementptr inbounds float, float* %call577, i32 1
  %383 = load float, float* %arrayidx578, align 4, !tbaa !11
  %384 = load float, float* %R11, align 4, !tbaa !11
  %mul579 = fmul float %383, %384
  %call580 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx581 = getelementptr inbounds float, float* %call580, i32 0
  %385 = load float, float* %arrayidx581, align 4, !tbaa !11
  %386 = load float, float* %R21, align 4, !tbaa !11
  %mul582 = fmul float %385, %386
  %sub583 = fsub float %mul579, %mul582
  %call584 = call float @_Z6btFabsf(float %sub583)
  %arrayidx585 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %387 = load float, float* %arrayidx585, align 4, !tbaa !11
  %388 = load float, float* %Q21, align 4, !tbaa !11
  %mul586 = fmul float %387, %388
  %arrayidx587 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %389 = load float, float* %arrayidx587, align 4, !tbaa !11
  %390 = load float, float* %Q11, align 4, !tbaa !11
  %mul588 = fmul float %389, %390
  %add589 = fadd float %mul586, %mul588
  %arrayidx590 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %391 = load float, float* %arrayidx590, align 4, !tbaa !11
  %392 = load float, float* %Q33, align 4, !tbaa !11
  %mul591 = fmul float %391, %392
  %add592 = fadd float %add589, %mul591
  %arrayidx593 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %393 = load float, float* %arrayidx593, align 4, !tbaa !11
  %394 = load float, float* %Q32, align 4, !tbaa !11
  %mul594 = fmul float %393, %394
  %add595 = fadd float %add592, %mul594
  %sub596 = fsub float %call584, %add595
  store float %sub596, float* %s2, align 4, !tbaa !11
  %395 = load float, float* %s2, align 4, !tbaa !11
  %cmp597 = fcmp ogt float %395, 0x3E80000000000000
  br i1 %cmp597, label %if.then598, label %if.end599

if.then598:                                       ; preds = %if.end576
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end599:                                        ; preds = %if.end576
  %396 = load float, float* %R21, align 4, !tbaa !11
  %fneg600 = fneg float %396
  %397 = load float, float* %R21, align 4, !tbaa !11
  %fneg601 = fneg float %397
  %mul602 = fmul float %fneg600, %fneg601
  %398 = load float, float* %R11, align 4, !tbaa !11
  %399 = load float, float* %R11, align 4, !tbaa !11
  %mul603 = fmul float %398, %399
  %add604 = fadd float %mul602, %mul603
  %add605 = fadd float %add604, 0.000000e+00
  %call606 = call float @_Z6btSqrtf(float %add605)
  store float %call606, float* %l, align 4, !tbaa !11
  %400 = load float, float* %l, align 4, !tbaa !11
  %cmp607 = fcmp ogt float %400, 0x3E80000000000000
  br i1 %cmp607, label %if.then608, label %if.end633

if.then608:                                       ; preds = %if.end599
  %401 = load float, float* %l, align 4, !tbaa !11
  %402 = load float, float* %s2, align 4, !tbaa !11
  %div609 = fdiv float %402, %401
  store float %div609, float* %s2, align 4, !tbaa !11
  %403 = load float, float* %s2, align 4, !tbaa !11
  %mul610 = fmul float %403, 0x3FF0CCCCC0000000
  %404 = load float, float* %s, align 4, !tbaa !11
  %cmp611 = fcmp ogt float %mul610, %404
  br i1 %cmp611, label %if.then612, label %if.end632

if.then612:                                       ; preds = %if.then608
  %405 = load float, float* %s2, align 4, !tbaa !11
  store float %405, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %406 = load float, float* %R21, align 4, !tbaa !11
  %fneg613 = fneg float %406
  %407 = load float, float* %l, align 4, !tbaa !11
  %div614 = fdiv float %fneg613, %407
  %call615 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx616 = getelementptr inbounds float, float* %call615, i32 0
  store float %div614, float* %arrayidx616, align 4, !tbaa !11
  %408 = load float, float* %R11, align 4, !tbaa !11
  %409 = load float, float* %l, align 4, !tbaa !11
  %div617 = fdiv float %408, %409
  %call618 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx619 = getelementptr inbounds float, float* %call618, i32 1
  store float %div617, float* %arrayidx619, align 4, !tbaa !11
  %410 = load float, float* %l, align 4, !tbaa !11
  %div620 = fdiv float 0.000000e+00, %410
  %call621 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx622 = getelementptr inbounds float, float* %call621, i32 2
  store float %div620, float* %arrayidx622, align 4, !tbaa !11
  %call623 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx624 = getelementptr inbounds float, float* %call623, i32 1
  %411 = load float, float* %arrayidx624, align 4, !tbaa !11
  %412 = load float, float* %R11, align 4, !tbaa !11
  %mul625 = fmul float %411, %412
  %call626 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx627 = getelementptr inbounds float, float* %call626, i32 0
  %413 = load float, float* %arrayidx627, align 4, !tbaa !11
  %414 = load float, float* %R21, align 4, !tbaa !11
  %mul628 = fmul float %413, %414
  %sub629 = fsub float %mul625, %mul628
  %cmp630 = fcmp olt float %sub629, 0.000000e+00
  %conv631 = zext i1 %cmp630 to i32
  store i32 %conv631, i32* %invert_normal, align 4, !tbaa !13
  store i32 13, i32* %code, align 4, !tbaa !13
  br label %if.end632

if.end632:                                        ; preds = %if.then612, %if.then608
  br label %if.end633

if.end633:                                        ; preds = %if.end632, %if.end599
  %call634 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx635 = getelementptr inbounds float, float* %call634, i32 1
  %415 = load float, float* %arrayidx635, align 4, !tbaa !11
  %416 = load float, float* %R12, align 4, !tbaa !11
  %mul636 = fmul float %415, %416
  %call637 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx638 = getelementptr inbounds float, float* %call637, i32 0
  %417 = load float, float* %arrayidx638, align 4, !tbaa !11
  %418 = load float, float* %R22, align 4, !tbaa !11
  %mul639 = fmul float %417, %418
  %sub640 = fsub float %mul636, %mul639
  %call641 = call float @_Z6btFabsf(float %sub640)
  %arrayidx642 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %419 = load float, float* %arrayidx642, align 4, !tbaa !11
  %420 = load float, float* %Q22, align 4, !tbaa !11
  %mul643 = fmul float %419, %420
  %arrayidx644 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %421 = load float, float* %arrayidx644, align 4, !tbaa !11
  %422 = load float, float* %Q12, align 4, !tbaa !11
  %mul645 = fmul float %421, %422
  %add646 = fadd float %mul643, %mul645
  %arrayidx647 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %423 = load float, float* %arrayidx647, align 4, !tbaa !11
  %424 = load float, float* %Q33, align 4, !tbaa !11
  %mul648 = fmul float %423, %424
  %add649 = fadd float %add646, %mul648
  %arrayidx650 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %425 = load float, float* %arrayidx650, align 4, !tbaa !11
  %426 = load float, float* %Q31, align 4, !tbaa !11
  %mul651 = fmul float %425, %426
  %add652 = fadd float %add649, %mul651
  %sub653 = fsub float %call641, %add652
  store float %sub653, float* %s2, align 4, !tbaa !11
  %427 = load float, float* %s2, align 4, !tbaa !11
  %cmp654 = fcmp ogt float %427, 0x3E80000000000000
  br i1 %cmp654, label %if.then655, label %if.end656

if.then655:                                       ; preds = %if.end633
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end656:                                        ; preds = %if.end633
  %428 = load float, float* %R22, align 4, !tbaa !11
  %fneg657 = fneg float %428
  %429 = load float, float* %R22, align 4, !tbaa !11
  %fneg658 = fneg float %429
  %mul659 = fmul float %fneg657, %fneg658
  %430 = load float, float* %R12, align 4, !tbaa !11
  %431 = load float, float* %R12, align 4, !tbaa !11
  %mul660 = fmul float %430, %431
  %add661 = fadd float %mul659, %mul660
  %add662 = fadd float %add661, 0.000000e+00
  %call663 = call float @_Z6btSqrtf(float %add662)
  store float %call663, float* %l, align 4, !tbaa !11
  %432 = load float, float* %l, align 4, !tbaa !11
  %cmp664 = fcmp ogt float %432, 0x3E80000000000000
  br i1 %cmp664, label %if.then665, label %if.end690

if.then665:                                       ; preds = %if.end656
  %433 = load float, float* %l, align 4, !tbaa !11
  %434 = load float, float* %s2, align 4, !tbaa !11
  %div666 = fdiv float %434, %433
  store float %div666, float* %s2, align 4, !tbaa !11
  %435 = load float, float* %s2, align 4, !tbaa !11
  %mul667 = fmul float %435, 0x3FF0CCCCC0000000
  %436 = load float, float* %s, align 4, !tbaa !11
  %cmp668 = fcmp ogt float %mul667, %436
  br i1 %cmp668, label %if.then669, label %if.end689

if.then669:                                       ; preds = %if.then665
  %437 = load float, float* %s2, align 4, !tbaa !11
  store float %437, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %438 = load float, float* %R22, align 4, !tbaa !11
  %fneg670 = fneg float %438
  %439 = load float, float* %l, align 4, !tbaa !11
  %div671 = fdiv float %fneg670, %439
  %call672 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx673 = getelementptr inbounds float, float* %call672, i32 0
  store float %div671, float* %arrayidx673, align 4, !tbaa !11
  %440 = load float, float* %R12, align 4, !tbaa !11
  %441 = load float, float* %l, align 4, !tbaa !11
  %div674 = fdiv float %440, %441
  %call675 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx676 = getelementptr inbounds float, float* %call675, i32 1
  store float %div674, float* %arrayidx676, align 4, !tbaa !11
  %442 = load float, float* %l, align 4, !tbaa !11
  %div677 = fdiv float 0.000000e+00, %442
  %call678 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx679 = getelementptr inbounds float, float* %call678, i32 2
  store float %div677, float* %arrayidx679, align 4, !tbaa !11
  %call680 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx681 = getelementptr inbounds float, float* %call680, i32 1
  %443 = load float, float* %arrayidx681, align 4, !tbaa !11
  %444 = load float, float* %R12, align 4, !tbaa !11
  %mul682 = fmul float %443, %444
  %call683 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx684 = getelementptr inbounds float, float* %call683, i32 0
  %445 = load float, float* %arrayidx684, align 4, !tbaa !11
  %446 = load float, float* %R22, align 4, !tbaa !11
  %mul685 = fmul float %445, %446
  %sub686 = fsub float %mul682, %mul685
  %cmp687 = fcmp olt float %sub686, 0.000000e+00
  %conv688 = zext i1 %cmp687 to i32
  store i32 %conv688, i32* %invert_normal, align 4, !tbaa !13
  store i32 14, i32* %code, align 4, !tbaa !13
  br label %if.end689

if.end689:                                        ; preds = %if.then669, %if.then665
  br label %if.end690

if.end690:                                        ; preds = %if.end689, %if.end656
  %call691 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx692 = getelementptr inbounds float, float* %call691, i32 1
  %447 = load float, float* %arrayidx692, align 4, !tbaa !11
  %448 = load float, float* %R13, align 4, !tbaa !11
  %mul693 = fmul float %447, %448
  %call694 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx695 = getelementptr inbounds float, float* %call694, i32 0
  %449 = load float, float* %arrayidx695, align 4, !tbaa !11
  %450 = load float, float* %R23, align 4, !tbaa !11
  %mul696 = fmul float %449, %450
  %sub697 = fsub float %mul693, %mul696
  %call698 = call float @_Z6btFabsf(float %sub697)
  %arrayidx699 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %451 = load float, float* %arrayidx699, align 4, !tbaa !11
  %452 = load float, float* %Q23, align 4, !tbaa !11
  %mul700 = fmul float %451, %452
  %arrayidx701 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %453 = load float, float* %arrayidx701, align 4, !tbaa !11
  %454 = load float, float* %Q13, align 4, !tbaa !11
  %mul702 = fmul float %453, %454
  %add703 = fadd float %mul700, %mul702
  %arrayidx704 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %455 = load float, float* %arrayidx704, align 4, !tbaa !11
  %456 = load float, float* %Q32, align 4, !tbaa !11
  %mul705 = fmul float %455, %456
  %add706 = fadd float %add703, %mul705
  %arrayidx707 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %457 = load float, float* %arrayidx707, align 4, !tbaa !11
  %458 = load float, float* %Q31, align 4, !tbaa !11
  %mul708 = fmul float %457, %458
  %add709 = fadd float %add706, %mul708
  %sub710 = fsub float %call698, %add709
  store float %sub710, float* %s2, align 4, !tbaa !11
  %459 = load float, float* %s2, align 4, !tbaa !11
  %cmp711 = fcmp ogt float %459, 0x3E80000000000000
  br i1 %cmp711, label %if.then712, label %if.end713

if.then712:                                       ; preds = %if.end690
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end713:                                        ; preds = %if.end690
  %460 = load float, float* %R23, align 4, !tbaa !11
  %fneg714 = fneg float %460
  %461 = load float, float* %R23, align 4, !tbaa !11
  %fneg715 = fneg float %461
  %mul716 = fmul float %fneg714, %fneg715
  %462 = load float, float* %R13, align 4, !tbaa !11
  %463 = load float, float* %R13, align 4, !tbaa !11
  %mul717 = fmul float %462, %463
  %add718 = fadd float %mul716, %mul717
  %add719 = fadd float %add718, 0.000000e+00
  %call720 = call float @_Z6btSqrtf(float %add719)
  store float %call720, float* %l, align 4, !tbaa !11
  %464 = load float, float* %l, align 4, !tbaa !11
  %cmp721 = fcmp ogt float %464, 0x3E80000000000000
  br i1 %cmp721, label %if.then722, label %if.end747

if.then722:                                       ; preds = %if.end713
  %465 = load float, float* %l, align 4, !tbaa !11
  %466 = load float, float* %s2, align 4, !tbaa !11
  %div723 = fdiv float %466, %465
  store float %div723, float* %s2, align 4, !tbaa !11
  %467 = load float, float* %s2, align 4, !tbaa !11
  %mul724 = fmul float %467, 0x3FF0CCCCC0000000
  %468 = load float, float* %s, align 4, !tbaa !11
  %cmp725 = fcmp ogt float %mul724, %468
  br i1 %cmp725, label %if.then726, label %if.end746

if.then726:                                       ; preds = %if.then722
  %469 = load float, float* %s2, align 4, !tbaa !11
  store float %469, float* %s, align 4, !tbaa !11
  store float* null, float** %normalR, align 4, !tbaa !2
  %470 = load float, float* %R23, align 4, !tbaa !11
  %fneg727 = fneg float %470
  %471 = load float, float* %l, align 4, !tbaa !11
  %div728 = fdiv float %fneg727, %471
  %call729 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx730 = getelementptr inbounds float, float* %call729, i32 0
  store float %div728, float* %arrayidx730, align 4, !tbaa !11
  %472 = load float, float* %R13, align 4, !tbaa !11
  %473 = load float, float* %l, align 4, !tbaa !11
  %div731 = fdiv float %472, %473
  %call732 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx733 = getelementptr inbounds float, float* %call732, i32 1
  store float %div731, float* %arrayidx733, align 4, !tbaa !11
  %474 = load float, float* %l, align 4, !tbaa !11
  %div734 = fdiv float 0.000000e+00, %474
  %call735 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx736 = getelementptr inbounds float, float* %call735, i32 2
  store float %div734, float* %arrayidx736, align 4, !tbaa !11
  %call737 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx738 = getelementptr inbounds float, float* %call737, i32 1
  %475 = load float, float* %arrayidx738, align 4, !tbaa !11
  %476 = load float, float* %R13, align 4, !tbaa !11
  %mul739 = fmul float %475, %476
  %call740 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx741 = getelementptr inbounds float, float* %call740, i32 0
  %477 = load float, float* %arrayidx741, align 4, !tbaa !11
  %478 = load float, float* %R23, align 4, !tbaa !11
  %mul742 = fmul float %477, %478
  %sub743 = fsub float %mul739, %mul742
  %cmp744 = fcmp olt float %sub743, 0.000000e+00
  %conv745 = zext i1 %cmp744 to i32
  store i32 %conv745, i32* %invert_normal, align 4, !tbaa !13
  store i32 15, i32* %code, align 4, !tbaa !13
  br label %if.end746

if.end746:                                        ; preds = %if.then726, %if.then722
  br label %if.end747

if.end747:                                        ; preds = %if.end746, %if.end713
  %479 = load i32, i32* %code, align 4, !tbaa !13
  %tobool = icmp ne i32 %479, 0
  br i1 %tobool, label %if.end749, label %if.then748

if.then748:                                       ; preds = %if.end747
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1375

if.end749:                                        ; preds = %if.end747
  %480 = load float*, float** %normalR, align 4, !tbaa !2
  %tobool750 = icmp ne float* %480, null
  br i1 %tobool750, label %if.then751, label %if.else

if.then751:                                       ; preds = %if.end749
  %481 = load float*, float** %normalR, align 4, !tbaa !2
  %arrayidx752 = getelementptr inbounds float, float* %481, i32 0
  %482 = load float, float* %arrayidx752, align 4, !tbaa !11
  %483 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call753 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %483)
  %arrayidx754 = getelementptr inbounds float, float* %call753, i32 0
  store float %482, float* %arrayidx754, align 4, !tbaa !11
  %484 = load float*, float** %normalR, align 4, !tbaa !2
  %arrayidx755 = getelementptr inbounds float, float* %484, i32 4
  %485 = load float, float* %arrayidx755, align 4, !tbaa !11
  %486 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call756 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %486)
  %arrayidx757 = getelementptr inbounds float, float* %call756, i32 1
  store float %485, float* %arrayidx757, align 4, !tbaa !11
  %487 = load float*, float** %normalR, align 4, !tbaa !2
  %arrayidx758 = getelementptr inbounds float, float* %487, i32 8
  %488 = load float, float* %arrayidx758, align 4, !tbaa !11
  %489 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call759 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %489)
  %arrayidx760 = getelementptr inbounds float, float* %call759, i32 2
  store float %488, float* %arrayidx760, align 4, !tbaa !11
  br label %if.end775

if.else:                                          ; preds = %if.end749
  %490 = load float*, float** %R1.addr, align 4, !tbaa !2
  %call761 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %call762 = call float @_ZL4dDOTPKfS0_(float* %490, float* %call761)
  %491 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call763 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %491)
  %arrayidx764 = getelementptr inbounds float, float* %call763, i32 0
  store float %call762, float* %arrayidx764, align 4, !tbaa !11
  %492 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr765 = getelementptr inbounds float, float* %492, i32 4
  %call766 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %call767 = call float @_ZL4dDOTPKfS0_(float* %add.ptr765, float* %call766)
  %493 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call768 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %493)
  %arrayidx769 = getelementptr inbounds float, float* %call768, i32 1
  store float %call767, float* %arrayidx769, align 4, !tbaa !11
  %494 = load float*, float** %R1.addr, align 4, !tbaa !2
  %add.ptr770 = getelementptr inbounds float, float* %494, i32 8
  %call771 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %call772 = call float @_ZL4dDOTPKfS0_(float* %add.ptr770, float* %call771)
  %495 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call773 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %495)
  %arrayidx774 = getelementptr inbounds float, float* %call773, i32 2
  store float %call772, float* %arrayidx774, align 4, !tbaa !11
  br label %if.end775

if.end775:                                        ; preds = %if.else, %if.then751
  %496 = load i32, i32* %invert_normal, align 4, !tbaa !13
  %tobool776 = icmp ne i32 %496, 0
  br i1 %tobool776, label %if.then777, label %if.end793

if.then777:                                       ; preds = %if.end775
  %497 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call778 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %497)
  %arrayidx779 = getelementptr inbounds float, float* %call778, i32 0
  %498 = load float, float* %arrayidx779, align 4, !tbaa !11
  %fneg780 = fneg float %498
  %499 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call781 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %499)
  %arrayidx782 = getelementptr inbounds float, float* %call781, i32 0
  store float %fneg780, float* %arrayidx782, align 4, !tbaa !11
  %500 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call783 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %500)
  %arrayidx784 = getelementptr inbounds float, float* %call783, i32 1
  %501 = load float, float* %arrayidx784, align 4, !tbaa !11
  %fneg785 = fneg float %501
  %502 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call786 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %502)
  %arrayidx787 = getelementptr inbounds float, float* %call786, i32 1
  store float %fneg785, float* %arrayidx787, align 4, !tbaa !11
  %503 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call788 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %503)
  %arrayidx789 = getelementptr inbounds float, float* %call788, i32 2
  %504 = load float, float* %arrayidx789, align 4, !tbaa !11
  %fneg790 = fneg float %504
  %505 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call791 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %505)
  %arrayidx792 = getelementptr inbounds float, float* %call791, i32 2
  store float %fneg790, float* %arrayidx792, align 4, !tbaa !11
  br label %if.end793

if.end793:                                        ; preds = %if.then777, %if.end775
  %506 = load float, float* %s, align 4, !tbaa !11
  %fneg794 = fneg float %506
  %507 = load float*, float** %depth.addr, align 4, !tbaa !2
  store float %fneg794, float* %507, align 4, !tbaa !11
  %508 = load i32, i32* %code, align 4, !tbaa !13
  %cmp795 = icmp sgt i32 %508, 6
  br i1 %cmp795, label %if.then796, label %if.end919

if.then796:                                       ; preds = %if.end793
  %509 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %509) #9
  %call797 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  %510 = bitcast float* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %510) #9
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then796
  %511 = load i32, i32* %i, align 4, !tbaa !13
  %cmp798 = icmp slt i32 %511, 3
  br i1 %cmp798, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %512 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %call799 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %512)
  %513 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx800 = getelementptr inbounds float, float* %call799, i32 %513
  %514 = load float, float* %arrayidx800, align 4, !tbaa !11
  %call801 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pa)
  %515 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx802 = getelementptr inbounds float, float* %call801, i32 %515
  store float %514, float* %arrayidx802, align 4, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %516 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add nsw i32 %516, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond803

for.cond803:                                      ; preds = %for.inc825, %for.end
  %517 = load i32, i32* %j, align 4, !tbaa !13
  %cmp804 = icmp slt i32 %517, 3
  br i1 %cmp804, label %for.body805, label %for.end827

for.body805:                                      ; preds = %for.cond803
  %518 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call806 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %518)
  %519 = load float*, float** %R1.addr, align 4, !tbaa !2
  %520 = load i32, i32* %j, align 4, !tbaa !13
  %add.ptr807 = getelementptr inbounds float, float* %519, i32 %520
  %call808 = call float @_ZL6dDOT14PKfS0_(float* %call806, float* %add.ptr807)
  %cmp809 = fcmp ogt float %call808, 0.000000e+00
  %521 = zext i1 %cmp809 to i64
  %cond = select i1 %cmp809, float 1.000000e+00, float -1.000000e+00
  store float %cond, float* %sign, align 4, !tbaa !11
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond810

for.cond810:                                      ; preds = %for.inc822, %for.body805
  %522 = load i32, i32* %i, align 4, !tbaa !13
  %cmp811 = icmp slt i32 %522, 3
  br i1 %cmp811, label %for.body812, label %for.end824

for.body812:                                      ; preds = %for.cond810
  %523 = load float, float* %sign, align 4, !tbaa !11
  %524 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx813 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 %524
  %525 = load float, float* %arrayidx813, align 4, !tbaa !11
  %mul814 = fmul float %523, %525
  %526 = load float*, float** %R1.addr, align 4, !tbaa !2
  %527 = load i32, i32* %i, align 4, !tbaa !13
  %mul815 = mul nsw i32 %527, 4
  %528 = load i32, i32* %j, align 4, !tbaa !13
  %add816 = add nsw i32 %mul815, %528
  %arrayidx817 = getelementptr inbounds float, float* %526, i32 %add816
  %529 = load float, float* %arrayidx817, align 4, !tbaa !11
  %mul818 = fmul float %mul814, %529
  %call819 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pa)
  %530 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx820 = getelementptr inbounds float, float* %call819, i32 %530
  %531 = load float, float* %arrayidx820, align 4, !tbaa !11
  %add821 = fadd float %531, %mul818
  store float %add821, float* %arrayidx820, align 4, !tbaa !11
  br label %for.inc822

for.inc822:                                       ; preds = %for.body812
  %532 = load i32, i32* %i, align 4, !tbaa !13
  %inc823 = add nsw i32 %532, 1
  store i32 %inc823, i32* %i, align 4, !tbaa !13
  br label %for.cond810

for.end824:                                       ; preds = %for.cond810
  br label %for.inc825

for.inc825:                                       ; preds = %for.end824
  %533 = load i32, i32* %j, align 4, !tbaa !13
  %inc826 = add nsw i32 %533, 1
  store i32 %inc826, i32* %j, align 4, !tbaa !13
  br label %for.cond803

for.end827:                                       ; preds = %for.cond803
  %534 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %534) #9
  %call828 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond829

for.cond829:                                      ; preds = %for.inc836, %for.end827
  %535 = load i32, i32* %i, align 4, !tbaa !13
  %cmp830 = icmp slt i32 %535, 3
  br i1 %cmp830, label %for.body831, label %for.end838

for.body831:                                      ; preds = %for.cond829
  %536 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %call832 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %536)
  %537 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx833 = getelementptr inbounds float, float* %call832, i32 %537
  %538 = load float, float* %arrayidx833, align 4, !tbaa !11
  %call834 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pb)
  %539 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx835 = getelementptr inbounds float, float* %call834, i32 %539
  store float %538, float* %arrayidx835, align 4, !tbaa !11
  br label %for.inc836

for.inc836:                                       ; preds = %for.body831
  %540 = load i32, i32* %i, align 4, !tbaa !13
  %inc837 = add nsw i32 %540, 1
  store i32 %inc837, i32* %i, align 4, !tbaa !13
  br label %for.cond829

for.end838:                                       ; preds = %for.cond829
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond839

for.cond839:                                      ; preds = %for.inc862, %for.end838
  %541 = load i32, i32* %j, align 4, !tbaa !13
  %cmp840 = icmp slt i32 %541, 3
  br i1 %cmp840, label %for.body841, label %for.end864

for.body841:                                      ; preds = %for.cond839
  %542 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call842 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %542)
  %543 = load float*, float** %R2.addr, align 4, !tbaa !2
  %544 = load i32, i32* %j, align 4, !tbaa !13
  %add.ptr843 = getelementptr inbounds float, float* %543, i32 %544
  %call844 = call float @_ZL6dDOT14PKfS0_(float* %call842, float* %add.ptr843)
  %cmp845 = fcmp ogt float %call844, 0.000000e+00
  %545 = zext i1 %cmp845 to i64
  %cond846 = select i1 %cmp845, float -1.000000e+00, float 1.000000e+00
  store float %cond846, float* %sign, align 4, !tbaa !11
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond847

for.cond847:                                      ; preds = %for.inc859, %for.body841
  %546 = load i32, i32* %i, align 4, !tbaa !13
  %cmp848 = icmp slt i32 %546, 3
  br i1 %cmp848, label %for.body849, label %for.end861

for.body849:                                      ; preds = %for.cond847
  %547 = load float, float* %sign, align 4, !tbaa !11
  %548 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx850 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 %548
  %549 = load float, float* %arrayidx850, align 4, !tbaa !11
  %mul851 = fmul float %547, %549
  %550 = load float*, float** %R2.addr, align 4, !tbaa !2
  %551 = load i32, i32* %i, align 4, !tbaa !13
  %mul852 = mul nsw i32 %551, 4
  %552 = load i32, i32* %j, align 4, !tbaa !13
  %add853 = add nsw i32 %mul852, %552
  %arrayidx854 = getelementptr inbounds float, float* %550, i32 %add853
  %553 = load float, float* %arrayidx854, align 4, !tbaa !11
  %mul855 = fmul float %mul851, %553
  %call856 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pb)
  %554 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx857 = getelementptr inbounds float, float* %call856, i32 %554
  %555 = load float, float* %arrayidx857, align 4, !tbaa !11
  %add858 = fadd float %555, %mul855
  store float %add858, float* %arrayidx857, align 4, !tbaa !11
  br label %for.inc859

for.inc859:                                       ; preds = %for.body849
  %556 = load i32, i32* %i, align 4, !tbaa !13
  %inc860 = add nsw i32 %556, 1
  store i32 %inc860, i32* %i, align 4, !tbaa !13
  br label %for.cond847

for.end861:                                       ; preds = %for.cond847
  br label %for.inc862

for.inc862:                                       ; preds = %for.end861
  %557 = load i32, i32* %j, align 4, !tbaa !13
  %inc863 = add nsw i32 %557, 1
  store i32 %inc863, i32* %j, align 4, !tbaa !13
  br label %for.cond839

for.end864:                                       ; preds = %for.cond839
  %558 = bitcast float* %alpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %558) #9
  %559 = bitcast float* %beta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %559) #9
  %560 = bitcast %class.btVector3* %ua to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %560) #9
  %call865 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ua)
  %561 = bitcast %class.btVector3* %ub to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %561) #9
  %call866 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ub)
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond867

for.cond867:                                      ; preds = %for.inc877, %for.end864
  %562 = load i32, i32* %i, align 4, !tbaa !13
  %cmp868 = icmp slt i32 %562, 3
  br i1 %cmp868, label %for.body869, label %for.end879

for.body869:                                      ; preds = %for.cond867
  %563 = load float*, float** %R1.addr, align 4, !tbaa !2
  %564 = load i32, i32* %code, align 4, !tbaa !13
  %sub870 = sub nsw i32 %564, 7
  %div871 = sdiv i32 %sub870, 3
  %565 = load i32, i32* %i, align 4, !tbaa !13
  %mul872 = mul nsw i32 %565, 4
  %add873 = add nsw i32 %div871, %mul872
  %arrayidx874 = getelementptr inbounds float, float* %563, i32 %add873
  %566 = load float, float* %arrayidx874, align 4, !tbaa !11
  %call875 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ua)
  %567 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx876 = getelementptr inbounds float, float* %call875, i32 %567
  store float %566, float* %arrayidx876, align 4, !tbaa !11
  br label %for.inc877

for.inc877:                                       ; preds = %for.body869
  %568 = load i32, i32* %i, align 4, !tbaa !13
  %inc878 = add nsw i32 %568, 1
  store i32 %inc878, i32* %i, align 4, !tbaa !13
  br label %for.cond867

for.end879:                                       ; preds = %for.cond867
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond880

for.cond880:                                      ; preds = %for.inc889, %for.end879
  %569 = load i32, i32* %i, align 4, !tbaa !13
  %cmp881 = icmp slt i32 %569, 3
  br i1 %cmp881, label %for.body882, label %for.end891

for.body882:                                      ; preds = %for.cond880
  %570 = load float*, float** %R2.addr, align 4, !tbaa !2
  %571 = load i32, i32* %code, align 4, !tbaa !13
  %sub883 = sub nsw i32 %571, 7
  %rem = srem i32 %sub883, 3
  %572 = load i32, i32* %i, align 4, !tbaa !13
  %mul884 = mul nsw i32 %572, 4
  %add885 = add nsw i32 %rem, %mul884
  %arrayidx886 = getelementptr inbounds float, float* %570, i32 %add885
  %573 = load float, float* %arrayidx886, align 4, !tbaa !11
  %call887 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ub)
  %574 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx888 = getelementptr inbounds float, float* %call887, i32 %574
  store float %573, float* %arrayidx888, align 4, !tbaa !11
  br label %for.inc889

for.inc889:                                       ; preds = %for.body882
  %575 = load i32, i32* %i, align 4, !tbaa !13
  %inc890 = add nsw i32 %575, 1
  store i32 %inc890, i32* %i, align 4, !tbaa !13
  br label %for.cond880

for.end891:                                       ; preds = %for.cond880
  call void @_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_(%class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %ua, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %ub, float* %alpha, float* %beta)
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond892

for.cond892:                                      ; preds = %for.inc901, %for.end891
  %576 = load i32, i32* %i, align 4, !tbaa !13
  %cmp893 = icmp slt i32 %576, 3
  br i1 %cmp893, label %for.body894, label %for.end903

for.body894:                                      ; preds = %for.cond892
  %call895 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ua)
  %577 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx896 = getelementptr inbounds float, float* %call895, i32 %577
  %578 = load float, float* %arrayidx896, align 4, !tbaa !11
  %579 = load float, float* %alpha, align 4, !tbaa !11
  %mul897 = fmul float %578, %579
  %call898 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pa)
  %580 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx899 = getelementptr inbounds float, float* %call898, i32 %580
  %581 = load float, float* %arrayidx899, align 4, !tbaa !11
  %add900 = fadd float %581, %mul897
  store float %add900, float* %arrayidx899, align 4, !tbaa !11
  br label %for.inc901

for.inc901:                                       ; preds = %for.body894
  %582 = load i32, i32* %i, align 4, !tbaa !13
  %inc902 = add nsw i32 %582, 1
  store i32 %inc902, i32* %i, align 4, !tbaa !13
  br label %for.cond892

for.end903:                                       ; preds = %for.cond892
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond904

for.cond904:                                      ; preds = %for.inc913, %for.end903
  %583 = load i32, i32* %i, align 4, !tbaa !13
  %cmp905 = icmp slt i32 %583, 3
  br i1 %cmp905, label %for.body906, label %for.end915

for.body906:                                      ; preds = %for.cond904
  %call907 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ub)
  %584 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx908 = getelementptr inbounds float, float* %call907, i32 %584
  %585 = load float, float* %arrayidx908, align 4, !tbaa !11
  %586 = load float, float* %beta, align 4, !tbaa !11
  %mul909 = fmul float %585, %586
  %call910 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pb)
  %587 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx911 = getelementptr inbounds float, float* %call910, i32 %587
  %588 = load float, float* %arrayidx911, align 4, !tbaa !11
  %add912 = fadd float %588, %mul909
  store float %add912, float* %arrayidx911, align 4, !tbaa !11
  br label %for.inc913

for.inc913:                                       ; preds = %for.body906
  %589 = load i32, i32* %i, align 4, !tbaa !13
  %inc914 = add nsw i32 %589, 1
  store i32 %inc914, i32* %i, align 4, !tbaa !13
  br label %for.cond904

for.end915:                                       ; preds = %for.cond904
  %590 = bitcast %class.btVector3* %pointInWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %590) #9
  %call916 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointInWorld)
  %591 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %592 = bitcast %class.btVector3* %ref.tmp917 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %592) #9
  %593 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp917, %class.btVector3* nonnull align 4 dereferenceable(16) %593)
  %594 = load float*, float** %depth.addr, align 4, !tbaa !2
  %595 = load float, float* %594, align 4, !tbaa !11
  %fneg918 = fneg float %595
  %596 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %591 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %596, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %597 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %597(%"struct.btDiscreteCollisionDetectorInterface::Result"* %591, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp917, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, float %fneg918)
  %598 = bitcast %class.btVector3* %ref.tmp917 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %598) #9
  %599 = load i32, i32* %code, align 4, !tbaa !13
  %600 = load i32*, i32** %return_code.addr, align 4, !tbaa !2
  store i32 %599, i32* %600, align 4, !tbaa !13
  %601 = bitcast %class.btVector3* %pointInWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %601) #9
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %602 = bitcast %class.btVector3* %ub to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %602) #9
  %603 = bitcast %class.btVector3* %ua to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %603) #9
  %604 = bitcast float* %beta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %604) #9
  %605 = bitcast float* %alpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %605) #9
  %606 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %606) #9
  %607 = bitcast float* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %607) #9
  %608 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %608) #9
  br label %cleanup1375

if.end919:                                        ; preds = %if.end793
  %609 = bitcast float** %Ra to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %609) #9
  %610 = bitcast float** %Rb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %610) #9
  %611 = bitcast float** %pa920 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %611) #9
  %612 = bitcast float** %pb921 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %612) #9
  %613 = bitcast float** %Sa to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %613) #9
  %614 = bitcast float** %Sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %614) #9
  %615 = load i32, i32* %code, align 4, !tbaa !13
  %cmp922 = icmp sle i32 %615, 3
  br i1 %cmp922, label %if.then923, label %if.else927

if.then923:                                       ; preds = %if.end919
  %616 = load float*, float** %R1.addr, align 4, !tbaa !2
  store float* %616, float** %Ra, align 4, !tbaa !2
  %617 = load float*, float** %R2.addr, align 4, !tbaa !2
  store float* %617, float** %Rb, align 4, !tbaa !2
  %618 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %call924 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %618)
  store float* %call924, float** %pa920, align 4, !tbaa !2
  %619 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %call925 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %619)
  store float* %call925, float** %pb921, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  store float* %arraydecay, float** %Sa, align 4, !tbaa !2
  %arraydecay926 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  store float* %arraydecay926, float** %Sb, align 4, !tbaa !2
  br label %if.end932

if.else927:                                       ; preds = %if.end919
  %620 = load float*, float** %R2.addr, align 4, !tbaa !2
  store float* %620, float** %Ra, align 4, !tbaa !2
  %621 = load float*, float** %R1.addr, align 4, !tbaa !2
  store float* %621, float** %Rb, align 4, !tbaa !2
  %622 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %call928 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %622)
  store float* %call928, float** %pa920, align 4, !tbaa !2
  %623 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %call929 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %623)
  store float* %call929, float** %pb921, align 4, !tbaa !2
  %arraydecay930 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  store float* %arraydecay930, float** %Sa, align 4, !tbaa !2
  %arraydecay931 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  store float* %arraydecay931, float** %Sb, align 4, !tbaa !2
  br label %if.end932

if.end932:                                        ; preds = %if.else927, %if.then923
  %624 = bitcast %class.btVector3* %normal2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %624) #9
  %call933 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal2)
  %625 = bitcast %class.btVector3* %nr to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %625) #9
  %call934 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nr)
  %626 = bitcast %class.btVector3* %anr to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %626) #9
  %call935 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %anr)
  %627 = load i32, i32* %code, align 4, !tbaa !13
  %cmp936 = icmp sle i32 %627, 3
  br i1 %cmp936, label %if.then937, label %if.else950

if.then937:                                       ; preds = %if.end932
  %628 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call938 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %628)
  %arrayidx939 = getelementptr inbounds float, float* %call938, i32 0
  %629 = load float, float* %arrayidx939, align 4, !tbaa !11
  %call940 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx941 = getelementptr inbounds float, float* %call940, i32 0
  store float %629, float* %arrayidx941, align 4, !tbaa !11
  %630 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call942 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %630)
  %arrayidx943 = getelementptr inbounds float, float* %call942, i32 1
  %631 = load float, float* %arrayidx943, align 4, !tbaa !11
  %call944 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx945 = getelementptr inbounds float, float* %call944, i32 1
  store float %631, float* %arrayidx945, align 4, !tbaa !11
  %632 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call946 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %632)
  %arrayidx947 = getelementptr inbounds float, float* %call946, i32 2
  %633 = load float, float* %arrayidx947, align 4, !tbaa !11
  %call948 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx949 = getelementptr inbounds float, float* %call948, i32 2
  store float %633, float* %arrayidx949, align 4, !tbaa !11
  br label %if.end966

if.else950:                                       ; preds = %if.end932
  %634 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call951 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %634)
  %arrayidx952 = getelementptr inbounds float, float* %call951, i32 0
  %635 = load float, float* %arrayidx952, align 4, !tbaa !11
  %fneg953 = fneg float %635
  %call954 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx955 = getelementptr inbounds float, float* %call954, i32 0
  store float %fneg953, float* %arrayidx955, align 4, !tbaa !11
  %636 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call956 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %636)
  %arrayidx957 = getelementptr inbounds float, float* %call956, i32 1
  %637 = load float, float* %arrayidx957, align 4, !tbaa !11
  %fneg958 = fneg float %637
  %call959 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx960 = getelementptr inbounds float, float* %call959, i32 1
  store float %fneg958, float* %arrayidx960, align 4, !tbaa !11
  %638 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call961 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %638)
  %arrayidx962 = getelementptr inbounds float, float* %call961, i32 2
  %639 = load float, float* %arrayidx962, align 4, !tbaa !11
  %fneg963 = fneg float %639
  %call964 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx965 = getelementptr inbounds float, float* %call964, i32 2
  store float %fneg963, float* %arrayidx965, align 4, !tbaa !11
  br label %if.end966

if.end966:                                        ; preds = %if.else950, %if.then937
  %640 = load float*, float** %Rb, align 4, !tbaa !2
  %call967 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %call968 = call float @_ZL6dDOT41PKfS0_(float* %640, float* %call967)
  %call969 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx970 = getelementptr inbounds float, float* %call969, i32 0
  store float %call968, float* %arrayidx970, align 4, !tbaa !11
  %641 = load float*, float** %Rb, align 4, !tbaa !2
  %add.ptr971 = getelementptr inbounds float, float* %641, i32 1
  %call972 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %call973 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr971, float* %call972)
  %call974 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx975 = getelementptr inbounds float, float* %call974, i32 1
  store float %call973, float* %arrayidx975, align 4, !tbaa !11
  %642 = load float*, float** %Rb, align 4, !tbaa !2
  %add.ptr976 = getelementptr inbounds float, float* %642, i32 2
  %call977 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %call978 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr976, float* %call977)
  %call979 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx980 = getelementptr inbounds float, float* %call979, i32 2
  store float %call978, float* %arrayidx980, align 4, !tbaa !11
  %call981 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx982 = getelementptr inbounds float, float* %call981, i32 0
  %643 = load float, float* %arrayidx982, align 4, !tbaa !11
  %call983 = call float @_Z6btFabsf(float %643)
  %call984 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx985 = getelementptr inbounds float, float* %call984, i32 0
  store float %call983, float* %arrayidx985, align 4, !tbaa !11
  %call986 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx987 = getelementptr inbounds float, float* %call986, i32 1
  %644 = load float, float* %arrayidx987, align 4, !tbaa !11
  %call988 = call float @_Z6btFabsf(float %644)
  %call989 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx990 = getelementptr inbounds float, float* %call989, i32 1
  store float %call988, float* %arrayidx990, align 4, !tbaa !11
  %call991 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx992 = getelementptr inbounds float, float* %call991, i32 2
  %645 = load float, float* %arrayidx992, align 4, !tbaa !11
  %call993 = call float @_Z6btFabsf(float %645)
  %call994 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx995 = getelementptr inbounds float, float* %call994, i32 2
  store float %call993, float* %arrayidx995, align 4, !tbaa !11
  %646 = bitcast i32* %lanr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %646) #9
  %647 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %647) #9
  %648 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %648) #9
  %call996 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx997 = getelementptr inbounds float, float* %call996, i32 1
  %649 = load float, float* %arrayidx997, align 4, !tbaa !11
  %call998 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx999 = getelementptr inbounds float, float* %call998, i32 0
  %650 = load float, float* %arrayidx999, align 4, !tbaa !11
  %cmp1000 = fcmp ogt float %649, %650
  br i1 %cmp1000, label %if.then1001, label %if.else1010

if.then1001:                                      ; preds = %if.end966
  %call1002 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1003 = getelementptr inbounds float, float* %call1002, i32 1
  %651 = load float, float* %arrayidx1003, align 4, !tbaa !11
  %call1004 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1005 = getelementptr inbounds float, float* %call1004, i32 2
  %652 = load float, float* %arrayidx1005, align 4, !tbaa !11
  %cmp1006 = fcmp ogt float %651, %652
  br i1 %cmp1006, label %if.then1007, label %if.else1008

if.then1007:                                      ; preds = %if.then1001
  store i32 0, i32* %a1, align 4, !tbaa !13
  store i32 1, i32* %lanr, align 4, !tbaa !13
  store i32 2, i32* %a2, align 4, !tbaa !13
  br label %if.end1009

if.else1008:                                      ; preds = %if.then1001
  store i32 0, i32* %a1, align 4, !tbaa !13
  store i32 1, i32* %a2, align 4, !tbaa !13
  store i32 2, i32* %lanr, align 4, !tbaa !13
  br label %if.end1009

if.end1009:                                       ; preds = %if.else1008, %if.then1007
  br label %if.end1019

if.else1010:                                      ; preds = %if.end966
  %call1011 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1012 = getelementptr inbounds float, float* %call1011, i32 0
  %653 = load float, float* %arrayidx1012, align 4, !tbaa !11
  %call1013 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1014 = getelementptr inbounds float, float* %call1013, i32 2
  %654 = load float, float* %arrayidx1014, align 4, !tbaa !11
  %cmp1015 = fcmp ogt float %653, %654
  br i1 %cmp1015, label %if.then1016, label %if.else1017

if.then1016:                                      ; preds = %if.else1010
  store i32 0, i32* %lanr, align 4, !tbaa !13
  store i32 1, i32* %a1, align 4, !tbaa !13
  store i32 2, i32* %a2, align 4, !tbaa !13
  br label %if.end1018

if.else1017:                                      ; preds = %if.else1010
  store i32 0, i32* %a1, align 4, !tbaa !13
  store i32 1, i32* %a2, align 4, !tbaa !13
  store i32 2, i32* %lanr, align 4, !tbaa !13
  br label %if.end1018

if.end1018:                                       ; preds = %if.else1017, %if.then1016
  br label %if.end1019

if.end1019:                                       ; preds = %if.end1018, %if.end1009
  %655 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %655) #9
  %call1020 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %center)
  %call1021 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %656 = load i32, i32* %lanr, align 4, !tbaa !13
  %arrayidx1022 = getelementptr inbounds float, float* %call1021, i32 %656
  %657 = load float, float* %arrayidx1022, align 4, !tbaa !11
  %cmp1023 = fcmp olt float %657, 0.000000e+00
  br i1 %cmp1023, label %if.then1024, label %if.else1042

if.then1024:                                      ; preds = %if.end1019
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond1025

for.cond1025:                                     ; preds = %for.inc1039, %if.then1024
  %658 = load i32, i32* %i, align 4, !tbaa !13
  %cmp1026 = icmp slt i32 %658, 3
  br i1 %cmp1026, label %for.body1027, label %for.end1041

for.body1027:                                     ; preds = %for.cond1025
  %659 = load float*, float** %pb921, align 4, !tbaa !2
  %660 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1028 = getelementptr inbounds float, float* %659, i32 %660
  %661 = load float, float* %arrayidx1028, align 4, !tbaa !11
  %662 = load float*, float** %pa920, align 4, !tbaa !2
  %663 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1029 = getelementptr inbounds float, float* %662, i32 %663
  %664 = load float, float* %arrayidx1029, align 4, !tbaa !11
  %sub1030 = fsub float %661, %664
  %665 = load float*, float** %Sb, align 4, !tbaa !2
  %666 = load i32, i32* %lanr, align 4, !tbaa !13
  %arrayidx1031 = getelementptr inbounds float, float* %665, i32 %666
  %667 = load float, float* %arrayidx1031, align 4, !tbaa !11
  %668 = load float*, float** %Rb, align 4, !tbaa !2
  %669 = load i32, i32* %i, align 4, !tbaa !13
  %mul1032 = mul nsw i32 %669, 4
  %670 = load i32, i32* %lanr, align 4, !tbaa !13
  %add1033 = add nsw i32 %mul1032, %670
  %arrayidx1034 = getelementptr inbounds float, float* %668, i32 %add1033
  %671 = load float, float* %arrayidx1034, align 4, !tbaa !11
  %mul1035 = fmul float %667, %671
  %add1036 = fadd float %sub1030, %mul1035
  %call1037 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %672 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1038 = getelementptr inbounds float, float* %call1037, i32 %672
  store float %add1036, float* %arrayidx1038, align 4, !tbaa !11
  br label %for.inc1039

for.inc1039:                                      ; preds = %for.body1027
  %673 = load i32, i32* %i, align 4, !tbaa !13
  %inc1040 = add nsw i32 %673, 1
  store i32 %inc1040, i32* %i, align 4, !tbaa !13
  br label %for.cond1025

for.end1041:                                      ; preds = %for.cond1025
  br label %if.end1060

if.else1042:                                      ; preds = %if.end1019
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond1043

for.cond1043:                                     ; preds = %for.inc1057, %if.else1042
  %674 = load i32, i32* %i, align 4, !tbaa !13
  %cmp1044 = icmp slt i32 %674, 3
  br i1 %cmp1044, label %for.body1045, label %for.end1059

for.body1045:                                     ; preds = %for.cond1043
  %675 = load float*, float** %pb921, align 4, !tbaa !2
  %676 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1046 = getelementptr inbounds float, float* %675, i32 %676
  %677 = load float, float* %arrayidx1046, align 4, !tbaa !11
  %678 = load float*, float** %pa920, align 4, !tbaa !2
  %679 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1047 = getelementptr inbounds float, float* %678, i32 %679
  %680 = load float, float* %arrayidx1047, align 4, !tbaa !11
  %sub1048 = fsub float %677, %680
  %681 = load float*, float** %Sb, align 4, !tbaa !2
  %682 = load i32, i32* %lanr, align 4, !tbaa !13
  %arrayidx1049 = getelementptr inbounds float, float* %681, i32 %682
  %683 = load float, float* %arrayidx1049, align 4, !tbaa !11
  %684 = load float*, float** %Rb, align 4, !tbaa !2
  %685 = load i32, i32* %i, align 4, !tbaa !13
  %mul1050 = mul nsw i32 %685, 4
  %686 = load i32, i32* %lanr, align 4, !tbaa !13
  %add1051 = add nsw i32 %mul1050, %686
  %arrayidx1052 = getelementptr inbounds float, float* %684, i32 %add1051
  %687 = load float, float* %arrayidx1052, align 4, !tbaa !11
  %mul1053 = fmul float %683, %687
  %sub1054 = fsub float %sub1048, %mul1053
  %call1055 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %688 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1056 = getelementptr inbounds float, float* %call1055, i32 %688
  store float %sub1054, float* %arrayidx1056, align 4, !tbaa !11
  br label %for.inc1057

for.inc1057:                                      ; preds = %for.body1045
  %689 = load i32, i32* %i, align 4, !tbaa !13
  %inc1058 = add nsw i32 %689, 1
  store i32 %inc1058, i32* %i, align 4, !tbaa !13
  br label %for.cond1043

for.end1059:                                      ; preds = %for.cond1043
  br label %if.end1060

if.end1060:                                       ; preds = %for.end1059, %for.end1041
  %690 = bitcast i32* %codeN to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %690) #9
  %691 = bitcast i32* %code1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %691) #9
  %692 = bitcast i32* %code2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %692) #9
  %693 = load i32, i32* %code, align 4, !tbaa !13
  %cmp1061 = icmp sle i32 %693, 3
  br i1 %cmp1061, label %if.then1062, label %if.else1064

if.then1062:                                      ; preds = %if.end1060
  %694 = load i32, i32* %code, align 4, !tbaa !13
  %sub1063 = sub nsw i32 %694, 1
  store i32 %sub1063, i32* %codeN, align 4, !tbaa !13
  br label %if.end1066

if.else1064:                                      ; preds = %if.end1060
  %695 = load i32, i32* %code, align 4, !tbaa !13
  %sub1065 = sub nsw i32 %695, 4
  store i32 %sub1065, i32* %codeN, align 4, !tbaa !13
  br label %if.end1066

if.end1066:                                       ; preds = %if.else1064, %if.then1062
  %696 = load i32, i32* %codeN, align 4, !tbaa !13
  %cmp1067 = icmp eq i32 %696, 0
  br i1 %cmp1067, label %if.then1068, label %if.else1069

if.then1068:                                      ; preds = %if.end1066
  store i32 1, i32* %code1, align 4, !tbaa !13
  store i32 2, i32* %code2, align 4, !tbaa !13
  br label %if.end1074

if.else1069:                                      ; preds = %if.end1066
  %697 = load i32, i32* %codeN, align 4, !tbaa !13
  %cmp1070 = icmp eq i32 %697, 1
  br i1 %cmp1070, label %if.then1071, label %if.else1072

if.then1071:                                      ; preds = %if.else1069
  store i32 0, i32* %code1, align 4, !tbaa !13
  store i32 2, i32* %code2, align 4, !tbaa !13
  br label %if.end1073

if.else1072:                                      ; preds = %if.else1069
  store i32 0, i32* %code1, align 4, !tbaa !13
  store i32 1, i32* %code2, align 4, !tbaa !13
  br label %if.end1073

if.end1073:                                       ; preds = %if.else1072, %if.then1071
  br label %if.end1074

if.end1074:                                       ; preds = %if.end1073, %if.then1068
  %698 = bitcast [8 x float]* %quad to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %698) #9
  %699 = bitcast float* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %699) #9
  %700 = bitcast float* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %700) #9
  %701 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %701) #9
  %702 = bitcast float* %m12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %702) #9
  %703 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %703) #9
  %704 = bitcast float* %m22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %704) #9
  %call1075 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %705 = load float*, float** %Ra, align 4, !tbaa !2
  %706 = load i32, i32* %code1, align 4, !tbaa !13
  %add.ptr1076 = getelementptr inbounds float, float* %705, i32 %706
  %call1077 = call float @_ZL6dDOT14PKfS0_(float* %call1075, float* %add.ptr1076)
  store float %call1077, float* %c1, align 4, !tbaa !11
  %call1078 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %707 = load float*, float** %Ra, align 4, !tbaa !2
  %708 = load i32, i32* %code2, align 4, !tbaa !13
  %add.ptr1079 = getelementptr inbounds float, float* %707, i32 %708
  %call1080 = call float @_ZL6dDOT14PKfS0_(float* %call1078, float* %add.ptr1079)
  store float %call1080, float* %c2, align 4, !tbaa !11
  %709 = load float*, float** %Ra, align 4, !tbaa !2
  %710 = load i32, i32* %code1, align 4, !tbaa !13
  %add.ptr1081 = getelementptr inbounds float, float* %709, i32 %710
  %711 = load float*, float** %Rb, align 4, !tbaa !2
  %712 = load i32, i32* %a1, align 4, !tbaa !13
  %add.ptr1082 = getelementptr inbounds float, float* %711, i32 %712
  %call1083 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1081, float* %add.ptr1082)
  store float %call1083, float* %m11, align 4, !tbaa !11
  %713 = load float*, float** %Ra, align 4, !tbaa !2
  %714 = load i32, i32* %code1, align 4, !tbaa !13
  %add.ptr1084 = getelementptr inbounds float, float* %713, i32 %714
  %715 = load float*, float** %Rb, align 4, !tbaa !2
  %716 = load i32, i32* %a2, align 4, !tbaa !13
  %add.ptr1085 = getelementptr inbounds float, float* %715, i32 %716
  %call1086 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1084, float* %add.ptr1085)
  store float %call1086, float* %m12, align 4, !tbaa !11
  %717 = load float*, float** %Ra, align 4, !tbaa !2
  %718 = load i32, i32* %code2, align 4, !tbaa !13
  %add.ptr1087 = getelementptr inbounds float, float* %717, i32 %718
  %719 = load float*, float** %Rb, align 4, !tbaa !2
  %720 = load i32, i32* %a1, align 4, !tbaa !13
  %add.ptr1088 = getelementptr inbounds float, float* %719, i32 %720
  %call1089 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1087, float* %add.ptr1088)
  store float %call1089, float* %m21, align 4, !tbaa !11
  %721 = load float*, float** %Ra, align 4, !tbaa !2
  %722 = load i32, i32* %code2, align 4, !tbaa !13
  %add.ptr1090 = getelementptr inbounds float, float* %721, i32 %722
  %723 = load float*, float** %Rb, align 4, !tbaa !2
  %724 = load i32, i32* %a2, align 4, !tbaa !13
  %add.ptr1091 = getelementptr inbounds float, float* %723, i32 %724
  %call1092 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1090, float* %add.ptr1091)
  store float %call1092, float* %m22, align 4, !tbaa !11
  %725 = bitcast float* %k1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %725) #9
  %726 = load float, float* %m11, align 4, !tbaa !11
  %727 = load float*, float** %Sb, align 4, !tbaa !2
  %728 = load i32, i32* %a1, align 4, !tbaa !13
  %arrayidx1093 = getelementptr inbounds float, float* %727, i32 %728
  %729 = load float, float* %arrayidx1093, align 4, !tbaa !11
  %mul1094 = fmul float %726, %729
  store float %mul1094, float* %k1, align 4, !tbaa !11
  %730 = bitcast float* %k2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %730) #9
  %731 = load float, float* %m21, align 4, !tbaa !11
  %732 = load float*, float** %Sb, align 4, !tbaa !2
  %733 = load i32, i32* %a1, align 4, !tbaa !13
  %arrayidx1095 = getelementptr inbounds float, float* %732, i32 %733
  %734 = load float, float* %arrayidx1095, align 4, !tbaa !11
  %mul1096 = fmul float %731, %734
  store float %mul1096, float* %k2, align 4, !tbaa !11
  %735 = bitcast float* %k3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %735) #9
  %736 = load float, float* %m12, align 4, !tbaa !11
  %737 = load float*, float** %Sb, align 4, !tbaa !2
  %738 = load i32, i32* %a2, align 4, !tbaa !13
  %arrayidx1097 = getelementptr inbounds float, float* %737, i32 %738
  %739 = load float, float* %arrayidx1097, align 4, !tbaa !11
  %mul1098 = fmul float %736, %739
  store float %mul1098, float* %k3, align 4, !tbaa !11
  %740 = bitcast float* %k4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %740) #9
  %741 = load float, float* %m22, align 4, !tbaa !11
  %742 = load float*, float** %Sb, align 4, !tbaa !2
  %743 = load i32, i32* %a2, align 4, !tbaa !13
  %arrayidx1099 = getelementptr inbounds float, float* %742, i32 %743
  %744 = load float, float* %arrayidx1099, align 4, !tbaa !11
  %mul1100 = fmul float %741, %744
  store float %mul1100, float* %k4, align 4, !tbaa !11
  %745 = load float, float* %c1, align 4, !tbaa !11
  %746 = load float, float* %k1, align 4, !tbaa !11
  %sub1101 = fsub float %745, %746
  %747 = load float, float* %k3, align 4, !tbaa !11
  %sub1102 = fsub float %sub1101, %747
  %arrayidx1103 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 0
  store float %sub1102, float* %arrayidx1103, align 16, !tbaa !11
  %748 = load float, float* %c2, align 4, !tbaa !11
  %749 = load float, float* %k2, align 4, !tbaa !11
  %sub1104 = fsub float %748, %749
  %750 = load float, float* %k4, align 4, !tbaa !11
  %sub1105 = fsub float %sub1104, %750
  %arrayidx1106 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 1
  store float %sub1105, float* %arrayidx1106, align 4, !tbaa !11
  %751 = load float, float* %c1, align 4, !tbaa !11
  %752 = load float, float* %k1, align 4, !tbaa !11
  %sub1107 = fsub float %751, %752
  %753 = load float, float* %k3, align 4, !tbaa !11
  %add1108 = fadd float %sub1107, %753
  %arrayidx1109 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 2
  store float %add1108, float* %arrayidx1109, align 8, !tbaa !11
  %754 = load float, float* %c2, align 4, !tbaa !11
  %755 = load float, float* %k2, align 4, !tbaa !11
  %sub1110 = fsub float %754, %755
  %756 = load float, float* %k4, align 4, !tbaa !11
  %add1111 = fadd float %sub1110, %756
  %arrayidx1112 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 3
  store float %add1111, float* %arrayidx1112, align 4, !tbaa !11
  %757 = load float, float* %c1, align 4, !tbaa !11
  %758 = load float, float* %k1, align 4, !tbaa !11
  %add1113 = fadd float %757, %758
  %759 = load float, float* %k3, align 4, !tbaa !11
  %add1114 = fadd float %add1113, %759
  %arrayidx1115 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 4
  store float %add1114, float* %arrayidx1115, align 16, !tbaa !11
  %760 = load float, float* %c2, align 4, !tbaa !11
  %761 = load float, float* %k2, align 4, !tbaa !11
  %add1116 = fadd float %760, %761
  %762 = load float, float* %k4, align 4, !tbaa !11
  %add1117 = fadd float %add1116, %762
  %arrayidx1118 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 5
  store float %add1117, float* %arrayidx1118, align 4, !tbaa !11
  %763 = load float, float* %c1, align 4, !tbaa !11
  %764 = load float, float* %k1, align 4, !tbaa !11
  %add1119 = fadd float %763, %764
  %765 = load float, float* %k3, align 4, !tbaa !11
  %sub1120 = fsub float %add1119, %765
  %arrayidx1121 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 6
  store float %sub1120, float* %arrayidx1121, align 8, !tbaa !11
  %766 = load float, float* %c2, align 4, !tbaa !11
  %767 = load float, float* %k2, align 4, !tbaa !11
  %add1122 = fadd float %766, %767
  %768 = load float, float* %k4, align 4, !tbaa !11
  %sub1123 = fsub float %add1122, %768
  %arrayidx1124 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 7
  store float %sub1123, float* %arrayidx1124, align 4, !tbaa !11
  %769 = bitcast float* %k4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %769) #9
  %770 = bitcast float* %k3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %770) #9
  %771 = bitcast float* %k2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %771) #9
  %772 = bitcast float* %k1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %772) #9
  %773 = bitcast [2 x float]* %rect to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %773) #9
  %774 = load float*, float** %Sa, align 4, !tbaa !2
  %775 = load i32, i32* %code1, align 4, !tbaa !13
  %arrayidx1125 = getelementptr inbounds float, float* %774, i32 %775
  %776 = load float, float* %arrayidx1125, align 4, !tbaa !11
  %arrayidx1126 = getelementptr inbounds [2 x float], [2 x float]* %rect, i32 0, i32 0
  store float %776, float* %arrayidx1126, align 4, !tbaa !11
  %777 = load float*, float** %Sa, align 4, !tbaa !2
  %778 = load i32, i32* %code2, align 4, !tbaa !13
  %arrayidx1127 = getelementptr inbounds float, float* %777, i32 %778
  %779 = load float, float* %arrayidx1127, align 4, !tbaa !11
  %arrayidx1128 = getelementptr inbounds [2 x float], [2 x float]* %rect, i32 0, i32 1
  store float %779, float* %arrayidx1128, align 4, !tbaa !11
  %780 = bitcast [16 x float]* %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %780) #9
  %781 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %781) #9
  %arraydecay1129 = getelementptr inbounds [2 x float], [2 x float]* %rect, i32 0, i32 0
  %arraydecay1130 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 0
  %arraydecay1131 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 0
  %call1132 = call i32 @_ZL18intersectRectQuad2PfS_S_(float* %arraydecay1129, float* %arraydecay1130, float* %arraydecay1131)
  store i32 %call1132, i32* %n, align 4, !tbaa !13
  %782 = load i32, i32* %n, align 4, !tbaa !13
  %cmp1133 = icmp slt i32 %782, 1
  br i1 %cmp1133, label %if.then1134, label %if.end1135

if.then1134:                                      ; preds = %if.end1074
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup1349

if.end1135:                                       ; preds = %if.end1074
  %783 = bitcast [24 x float]* %point to i8*
  call void @llvm.lifetime.start.p0i8(i64 96, i8* %783) #9
  %784 = bitcast [8 x float]* %dep to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %784) #9
  %785 = bitcast float* %det1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %785) #9
  %786 = load float, float* %m11, align 4, !tbaa !11
  %787 = load float, float* %m22, align 4, !tbaa !11
  %mul1136 = fmul float %786, %787
  %788 = load float, float* %m12, align 4, !tbaa !11
  %789 = load float, float* %m21, align 4, !tbaa !11
  %mul1137 = fmul float %788, %789
  %sub1138 = fsub float %mul1136, %mul1137
  %div1139 = fdiv float 1.000000e+00, %sub1138
  store float %div1139, float* %det1, align 4, !tbaa !11
  %790 = load float, float* %det1, align 4, !tbaa !11
  %791 = load float, float* %m11, align 4, !tbaa !11
  %mul1140 = fmul float %791, %790
  store float %mul1140, float* %m11, align 4, !tbaa !11
  %792 = load float, float* %det1, align 4, !tbaa !11
  %793 = load float, float* %m12, align 4, !tbaa !11
  %mul1141 = fmul float %793, %792
  store float %mul1141, float* %m12, align 4, !tbaa !11
  %794 = load float, float* %det1, align 4, !tbaa !11
  %795 = load float, float* %m21, align 4, !tbaa !11
  %mul1142 = fmul float %795, %794
  store float %mul1142, float* %m21, align 4, !tbaa !11
  %796 = load float, float* %det1, align 4, !tbaa !11
  %797 = load float, float* %m22, align 4, !tbaa !11
  %mul1143 = fmul float %797, %796
  store float %mul1143, float* %m22, align 4, !tbaa !11
  %798 = bitcast i32* %cnum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %798) #9
  store i32 0, i32* %cnum, align 4, !tbaa !13
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond1144

for.cond1144:                                     ; preds = %for.inc1214, %if.end1135
  %799 = load i32, i32* %j, align 4, !tbaa !13
  %800 = load i32, i32* %n, align 4, !tbaa !13
  %cmp1145 = icmp slt i32 %799, %800
  br i1 %cmp1145, label %for.body1146, label %for.end1216

for.body1146:                                     ; preds = %for.cond1144
  %801 = bitcast float* %k11147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %801) #9
  %802 = load float, float* %m22, align 4, !tbaa !11
  %803 = load i32, i32* %j, align 4, !tbaa !13
  %mul1148 = mul nsw i32 %803, 2
  %arrayidx1149 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1148
  %804 = load float, float* %arrayidx1149, align 4, !tbaa !11
  %805 = load float, float* %c1, align 4, !tbaa !11
  %sub1150 = fsub float %804, %805
  %mul1151 = fmul float %802, %sub1150
  %806 = load float, float* %m12, align 4, !tbaa !11
  %807 = load i32, i32* %j, align 4, !tbaa !13
  %mul1152 = mul nsw i32 %807, 2
  %add1153 = add nsw i32 %mul1152, 1
  %arrayidx1154 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1153
  %808 = load float, float* %arrayidx1154, align 4, !tbaa !11
  %809 = load float, float* %c2, align 4, !tbaa !11
  %sub1155 = fsub float %808, %809
  %mul1156 = fmul float %806, %sub1155
  %sub1157 = fsub float %mul1151, %mul1156
  store float %sub1157, float* %k11147, align 4, !tbaa !11
  %810 = bitcast float* %k21158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %810) #9
  %811 = load float, float* %m21, align 4, !tbaa !11
  %fneg1159 = fneg float %811
  %812 = load i32, i32* %j, align 4, !tbaa !13
  %mul1160 = mul nsw i32 %812, 2
  %arrayidx1161 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1160
  %813 = load float, float* %arrayidx1161, align 4, !tbaa !11
  %814 = load float, float* %c1, align 4, !tbaa !11
  %sub1162 = fsub float %813, %814
  %mul1163 = fmul float %fneg1159, %sub1162
  %815 = load float, float* %m11, align 4, !tbaa !11
  %816 = load i32, i32* %j, align 4, !tbaa !13
  %mul1164 = mul nsw i32 %816, 2
  %add1165 = add nsw i32 %mul1164, 1
  %arrayidx1166 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1165
  %817 = load float, float* %arrayidx1166, align 4, !tbaa !11
  %818 = load float, float* %c2, align 4, !tbaa !11
  %sub1167 = fsub float %817, %818
  %mul1168 = fmul float %815, %sub1167
  %add1169 = fadd float %mul1163, %mul1168
  store float %add1169, float* %k21158, align 4, !tbaa !11
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond1170

for.cond1170:                                     ; preds = %for.inc1188, %for.body1146
  %819 = load i32, i32* %i, align 4, !tbaa !13
  %cmp1171 = icmp slt i32 %819, 3
  br i1 %cmp1171, label %for.body1172, label %for.end1190

for.body1172:                                     ; preds = %for.cond1170
  %call1173 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %820 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1174 = getelementptr inbounds float, float* %call1173, i32 %820
  %821 = load float, float* %arrayidx1174, align 4, !tbaa !11
  %822 = load float, float* %k11147, align 4, !tbaa !11
  %823 = load float*, float** %Rb, align 4, !tbaa !2
  %824 = load i32, i32* %i, align 4, !tbaa !13
  %mul1175 = mul nsw i32 %824, 4
  %825 = load i32, i32* %a1, align 4, !tbaa !13
  %add1176 = add nsw i32 %mul1175, %825
  %arrayidx1177 = getelementptr inbounds float, float* %823, i32 %add1176
  %826 = load float, float* %arrayidx1177, align 4, !tbaa !11
  %mul1178 = fmul float %822, %826
  %add1179 = fadd float %821, %mul1178
  %827 = load float, float* %k21158, align 4, !tbaa !11
  %828 = load float*, float** %Rb, align 4, !tbaa !2
  %829 = load i32, i32* %i, align 4, !tbaa !13
  %mul1180 = mul nsw i32 %829, 4
  %830 = load i32, i32* %a2, align 4, !tbaa !13
  %add1181 = add nsw i32 %mul1180, %830
  %arrayidx1182 = getelementptr inbounds float, float* %828, i32 %add1181
  %831 = load float, float* %arrayidx1182, align 4, !tbaa !11
  %mul1183 = fmul float %827, %831
  %add1184 = fadd float %add1179, %mul1183
  %832 = load i32, i32* %cnum, align 4, !tbaa !13
  %mul1185 = mul nsw i32 %832, 3
  %833 = load i32, i32* %i, align 4, !tbaa !13
  %add1186 = add nsw i32 %mul1185, %833
  %arrayidx1187 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1186
  store float %add1184, float* %arrayidx1187, align 4, !tbaa !11
  br label %for.inc1188

for.inc1188:                                      ; preds = %for.body1172
  %834 = load i32, i32* %i, align 4, !tbaa !13
  %inc1189 = add nsw i32 %834, 1
  store i32 %inc1189, i32* %i, align 4, !tbaa !13
  br label %for.cond1170

for.end1190:                                      ; preds = %for.cond1170
  %835 = load float*, float** %Sa, align 4, !tbaa !2
  %836 = load i32, i32* %codeN, align 4, !tbaa !13
  %arrayidx1191 = getelementptr inbounds float, float* %835, i32 %836
  %837 = load float, float* %arrayidx1191, align 4, !tbaa !11
  %call1192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arraydecay1193 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 0
  %838 = load i32, i32* %cnum, align 4, !tbaa !13
  %mul1194 = mul nsw i32 %838, 3
  %add.ptr1195 = getelementptr inbounds float, float* %arraydecay1193, i32 %mul1194
  %call1196 = call float @_ZL4dDOTPKfS0_(float* %call1192, float* %add.ptr1195)
  %sub1197 = fsub float %837, %call1196
  %839 = load i32, i32* %cnum, align 4, !tbaa !13
  %arrayidx1198 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %839
  store float %sub1197, float* %arrayidx1198, align 4, !tbaa !11
  %840 = load i32, i32* %cnum, align 4, !tbaa !13
  %arrayidx1199 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %840
  %841 = load float, float* %arrayidx1199, align 4, !tbaa !11
  %cmp1200 = fcmp oge float %841, 0.000000e+00
  br i1 %cmp1200, label %if.then1201, label %if.end1213

if.then1201:                                      ; preds = %for.end1190
  %842 = load i32, i32* %j, align 4, !tbaa !13
  %mul1202 = mul nsw i32 %842, 2
  %arrayidx1203 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1202
  %843 = load float, float* %arrayidx1203, align 4, !tbaa !11
  %844 = load i32, i32* %cnum, align 4, !tbaa !13
  %mul1204 = mul nsw i32 %844, 2
  %arrayidx1205 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1204
  store float %843, float* %arrayidx1205, align 4, !tbaa !11
  %845 = load i32, i32* %j, align 4, !tbaa !13
  %mul1206 = mul nsw i32 %845, 2
  %add1207 = add nsw i32 %mul1206, 1
  %arrayidx1208 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1207
  %846 = load float, float* %arrayidx1208, align 4, !tbaa !11
  %847 = load i32, i32* %cnum, align 4, !tbaa !13
  %mul1209 = mul nsw i32 %847, 2
  %add1210 = add nsw i32 %mul1209, 1
  %arrayidx1211 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1210
  store float %846, float* %arrayidx1211, align 4, !tbaa !11
  %848 = load i32, i32* %cnum, align 4, !tbaa !13
  %inc1212 = add nsw i32 %848, 1
  store i32 %inc1212, i32* %cnum, align 4, !tbaa !13
  br label %if.end1213

if.end1213:                                       ; preds = %if.then1201, %for.end1190
  %849 = bitcast float* %k21158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %849) #9
  %850 = bitcast float* %k11147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %850) #9
  br label %for.inc1214

for.inc1214:                                      ; preds = %if.end1213
  %851 = load i32, i32* %j, align 4, !tbaa !13
  %inc1215 = add nsw i32 %851, 1
  store i32 %inc1215, i32* %j, align 4, !tbaa !13
  br label %for.cond1144

for.end1216:                                      ; preds = %for.cond1144
  %852 = load i32, i32* %cnum, align 4, !tbaa !13
  %cmp1217 = icmp slt i32 %852, 1
  br i1 %cmp1217, label %if.then1218, label %if.end1219

if.then1218:                                      ; preds = %for.end1216
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end1219:                                       ; preds = %for.end1216
  %853 = load i32, i32* %maxc.addr, align 4, !tbaa !13
  %854 = load i32, i32* %cnum, align 4, !tbaa !13
  %cmp1220 = icmp sgt i32 %853, %854
  br i1 %cmp1220, label %if.then1221, label %if.end1222

if.then1221:                                      ; preds = %if.end1219
  %855 = load i32, i32* %cnum, align 4, !tbaa !13
  store i32 %855, i32* %maxc.addr, align 4, !tbaa !13
  br label %if.end1222

if.end1222:                                       ; preds = %if.then1221, %if.end1219
  %856 = load i32, i32* %maxc.addr, align 4, !tbaa !13
  %cmp1223 = icmp slt i32 %856, 1
  br i1 %cmp1223, label %if.then1224, label %if.end1225

if.then1224:                                      ; preds = %if.end1222
  store i32 1, i32* %maxc.addr, align 4, !tbaa !13
  br label %if.end1225

if.end1225:                                       ; preds = %if.then1224, %if.end1222
  %857 = load i32, i32* %cnum, align 4, !tbaa !13
  %858 = load i32, i32* %maxc.addr, align 4, !tbaa !13
  %cmp1226 = icmp sle i32 %857, %858
  br i1 %cmp1226, label %if.then1227, label %if.else1289

if.then1227:                                      ; preds = %if.end1225
  %859 = load i32, i32* %code, align 4, !tbaa !13
  %cmp1228 = icmp slt i32 %859, 4
  br i1 %cmp1228, label %if.then1229, label %if.else1256

if.then1229:                                      ; preds = %if.then1227
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond1230

for.cond1230:                                     ; preds = %for.inc1253, %if.then1229
  %860 = load i32, i32* %j, align 4, !tbaa !13
  %861 = load i32, i32* %cnum, align 4, !tbaa !13
  %cmp1231 = icmp slt i32 %860, %861
  br i1 %cmp1231, label %for.body1232, label %for.end1255

for.body1232:                                     ; preds = %for.cond1230
  %862 = bitcast %class.btVector3* %pointInWorld1233 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %862) #9
  %call1234 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointInWorld1233)
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond1235

for.cond1235:                                     ; preds = %for.inc1245, %for.body1232
  %863 = load i32, i32* %i, align 4, !tbaa !13
  %cmp1236 = icmp slt i32 %863, 3
  br i1 %cmp1236, label %for.body1237, label %for.end1247

for.body1237:                                     ; preds = %for.cond1235
  %864 = load i32, i32* %j, align 4, !tbaa !13
  %mul1238 = mul nsw i32 %864, 3
  %865 = load i32, i32* %i, align 4, !tbaa !13
  %add1239 = add nsw i32 %mul1238, %865
  %arrayidx1240 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1239
  %866 = load float, float* %arrayidx1240, align 4, !tbaa !11
  %867 = load float*, float** %pa920, align 4, !tbaa !2
  %868 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1241 = getelementptr inbounds float, float* %867, i32 %868
  %869 = load float, float* %arrayidx1241, align 4, !tbaa !11
  %add1242 = fadd float %866, %869
  %call1243 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pointInWorld1233)
  %870 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1244 = getelementptr inbounds float, float* %call1243, i32 %870
  store float %add1242, float* %arrayidx1244, align 4, !tbaa !11
  br label %for.inc1245

for.inc1245:                                      ; preds = %for.body1237
  %871 = load i32, i32* %i, align 4, !tbaa !13
  %inc1246 = add nsw i32 %871, 1
  store i32 %inc1246, i32* %i, align 4, !tbaa !13
  br label %for.cond1235

for.end1247:                                      ; preds = %for.cond1235
  %872 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %873 = bitcast %class.btVector3* %ref.tmp1248 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %873) #9
  %874 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1248, %class.btVector3* nonnull align 4 dereferenceable(16) %874)
  %875 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx1249 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %875
  %876 = load float, float* %arrayidx1249, align 4, !tbaa !11
  %fneg1250 = fneg float %876
  %877 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %872 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1251 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %877, align 4, !tbaa !6
  %vfn1252 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1251, i64 4
  %878 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1252, align 4
  call void %878(%"struct.btDiscreteCollisionDetectorInterface::Result"* %872, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1248, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld1233, float %fneg1250)
  %879 = bitcast %class.btVector3* %ref.tmp1248 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %879) #9
  %880 = bitcast %class.btVector3* %pointInWorld1233 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %880) #9
  br label %for.inc1253

for.inc1253:                                      ; preds = %for.end1247
  %881 = load i32, i32* %j, align 4, !tbaa !13
  %inc1254 = add nsw i32 %881, 1
  store i32 %inc1254, i32* %j, align 4, !tbaa !13
  br label %for.cond1230

for.end1255:                                      ; preds = %for.cond1230
  br label %if.end1288

if.else1256:                                      ; preds = %if.then1227
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond1257

for.cond1257:                                     ; preds = %for.inc1285, %if.else1256
  %882 = load i32, i32* %j, align 4, !tbaa !13
  %883 = load i32, i32* %cnum, align 4, !tbaa !13
  %cmp1258 = icmp slt i32 %882, %883
  br i1 %cmp1258, label %for.body1259, label %for.end1287

for.body1259:                                     ; preds = %for.cond1257
  %884 = bitcast %class.btVector3* %pointInWorld1260 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %884) #9
  %call1261 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointInWorld1260)
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond1262

for.cond1262:                                     ; preds = %for.inc1277, %for.body1259
  %885 = load i32, i32* %i, align 4, !tbaa !13
  %cmp1263 = icmp slt i32 %885, 3
  br i1 %cmp1263, label %for.body1264, label %for.end1279

for.body1264:                                     ; preds = %for.cond1262
  %886 = load i32, i32* %j, align 4, !tbaa !13
  %mul1265 = mul nsw i32 %886, 3
  %887 = load i32, i32* %i, align 4, !tbaa !13
  %add1266 = add nsw i32 %mul1265, %887
  %arrayidx1267 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1266
  %888 = load float, float* %arrayidx1267, align 4, !tbaa !11
  %889 = load float*, float** %pa920, align 4, !tbaa !2
  %890 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1268 = getelementptr inbounds float, float* %889, i32 %890
  %891 = load float, float* %arrayidx1268, align 4, !tbaa !11
  %add1269 = fadd float %888, %891
  %892 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call1270 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %892)
  %893 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1271 = getelementptr inbounds float, float* %call1270, i32 %893
  %894 = load float, float* %arrayidx1271, align 4, !tbaa !11
  %895 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx1272 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %895
  %896 = load float, float* %arrayidx1272, align 4, !tbaa !11
  %mul1273 = fmul float %894, %896
  %sub1274 = fsub float %add1269, %mul1273
  %call1275 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pointInWorld1260)
  %897 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1276 = getelementptr inbounds float, float* %call1275, i32 %897
  store float %sub1274, float* %arrayidx1276, align 4, !tbaa !11
  br label %for.inc1277

for.inc1277:                                      ; preds = %for.body1264
  %898 = load i32, i32* %i, align 4, !tbaa !13
  %inc1278 = add nsw i32 %898, 1
  store i32 %inc1278, i32* %i, align 4, !tbaa !13
  br label %for.cond1262

for.end1279:                                      ; preds = %for.cond1262
  %899 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %900 = bitcast %class.btVector3* %ref.tmp1280 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %900) #9
  %901 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1280, %class.btVector3* nonnull align 4 dereferenceable(16) %901)
  %902 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx1281 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %902
  %903 = load float, float* %arrayidx1281, align 4, !tbaa !11
  %fneg1282 = fneg float %903
  %904 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %899 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1283 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %904, align 4, !tbaa !6
  %vfn1284 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1283, i64 4
  %905 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1284, align 4
  call void %905(%"struct.btDiscreteCollisionDetectorInterface::Result"* %899, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1280, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld1260, float %fneg1282)
  %906 = bitcast %class.btVector3* %ref.tmp1280 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %906) #9
  %907 = bitcast %class.btVector3* %pointInWorld1260 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %907) #9
  br label %for.inc1285

for.inc1285:                                      ; preds = %for.end1279
  %908 = load i32, i32* %j, align 4, !tbaa !13
  %inc1286 = add nsw i32 %908, 1
  store i32 %inc1286, i32* %j, align 4, !tbaa !13
  br label %for.cond1257

for.end1287:                                      ; preds = %for.cond1257
  br label %if.end1288

if.end1288:                                       ; preds = %for.end1287, %for.end1255
  br label %if.end1345

if.else1289:                                      ; preds = %if.end1225
  %909 = bitcast i32* %i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %909) #9
  store i32 0, i32* %i1, align 4, !tbaa !13
  %910 = bitcast float* %maxdepth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %910) #9
  %arrayidx1290 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 0
  %911 = load float, float* %arrayidx1290, align 16, !tbaa !11
  store float %911, float* %maxdepth, align 4, !tbaa !11
  store i32 1, i32* %i, align 4, !tbaa !13
  br label %for.cond1291

for.cond1291:                                     ; preds = %for.inc1299, %if.else1289
  %912 = load i32, i32* %i, align 4, !tbaa !13
  %913 = load i32, i32* %cnum, align 4, !tbaa !13
  %cmp1292 = icmp slt i32 %912, %913
  br i1 %cmp1292, label %for.body1293, label %for.end1301

for.body1293:                                     ; preds = %for.cond1291
  %914 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1294 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %914
  %915 = load float, float* %arrayidx1294, align 4, !tbaa !11
  %916 = load float, float* %maxdepth, align 4, !tbaa !11
  %cmp1295 = fcmp ogt float %915, %916
  br i1 %cmp1295, label %if.then1296, label %if.end1298

if.then1296:                                      ; preds = %for.body1293
  %917 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1297 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %917
  %918 = load float, float* %arrayidx1297, align 4, !tbaa !11
  store float %918, float* %maxdepth, align 4, !tbaa !11
  %919 = load i32, i32* %i, align 4, !tbaa !13
  store i32 %919, i32* %i1, align 4, !tbaa !13
  br label %if.end1298

if.end1298:                                       ; preds = %if.then1296, %for.body1293
  br label %for.inc1299

for.inc1299:                                      ; preds = %if.end1298
  %920 = load i32, i32* %i, align 4, !tbaa !13
  %inc1300 = add nsw i32 %920, 1
  store i32 %inc1300, i32* %i, align 4, !tbaa !13
  br label %for.cond1291

for.end1301:                                      ; preds = %for.cond1291
  %921 = bitcast [8 x i32]* %iret to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %921) #9
  %922 = load i32, i32* %cnum, align 4, !tbaa !13
  %arraydecay1302 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 0
  %923 = load i32, i32* %maxc.addr, align 4, !tbaa !13
  %924 = load i32, i32* %i1, align 4, !tbaa !13
  %arraydecay1303 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 0
  call void @_Z11cullPoints2iPfiiPi(i32 %922, float* %arraydecay1302, i32 %923, i32 %924, i32* %arraydecay1303)
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond1304

for.cond1304:                                     ; preds = %for.inc1342, %for.end1301
  %925 = load i32, i32* %j, align 4, !tbaa !13
  %926 = load i32, i32* %maxc.addr, align 4, !tbaa !13
  %cmp1305 = icmp slt i32 %925, %926
  br i1 %cmp1305, label %for.body1306, label %for.end1344

for.body1306:                                     ; preds = %for.cond1304
  %927 = bitcast %class.btVector3* %posInWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %927) #9
  %call1307 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %posInWorld)
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond1308

for.cond1308:                                     ; preds = %for.inc1319, %for.body1306
  %928 = load i32, i32* %i, align 4, !tbaa !13
  %cmp1309 = icmp slt i32 %928, 3
  br i1 %cmp1309, label %for.body1310, label %for.end1321

for.body1310:                                     ; preds = %for.cond1308
  %929 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx1311 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %929
  %930 = load i32, i32* %arrayidx1311, align 4, !tbaa !13
  %mul1312 = mul nsw i32 %930, 3
  %931 = load i32, i32* %i, align 4, !tbaa !13
  %add1313 = add nsw i32 %mul1312, %931
  %arrayidx1314 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1313
  %932 = load float, float* %arrayidx1314, align 4, !tbaa !11
  %933 = load float*, float** %pa920, align 4, !tbaa !2
  %934 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1315 = getelementptr inbounds float, float* %933, i32 %934
  %935 = load float, float* %arrayidx1315, align 4, !tbaa !11
  %add1316 = fadd float %932, %935
  %call1317 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %posInWorld)
  %936 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx1318 = getelementptr inbounds float, float* %call1317, i32 %936
  store float %add1316, float* %arrayidx1318, align 4, !tbaa !11
  br label %for.inc1319

for.inc1319:                                      ; preds = %for.body1310
  %937 = load i32, i32* %i, align 4, !tbaa !13
  %inc1320 = add nsw i32 %937, 1
  store i32 %inc1320, i32* %i, align 4, !tbaa !13
  br label %for.cond1308

for.end1321:                                      ; preds = %for.cond1308
  %938 = load i32, i32* %code, align 4, !tbaa !13
  %cmp1322 = icmp slt i32 %938, 4
  br i1 %cmp1322, label %if.then1323, label %if.else1330

if.then1323:                                      ; preds = %for.end1321
  %939 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %940 = bitcast %class.btVector3* %ref.tmp1324 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %940) #9
  %941 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1324, %class.btVector3* nonnull align 4 dereferenceable(16) %941)
  %942 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx1325 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %942
  %943 = load i32, i32* %arrayidx1325, align 4, !tbaa !13
  %arrayidx1326 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %943
  %944 = load float, float* %arrayidx1326, align 4, !tbaa !11
  %fneg1327 = fneg float %944
  %945 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %939 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1328 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %945, align 4, !tbaa !6
  %vfn1329 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1328, i64 4
  %946 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1329, align 4
  call void %946(%"struct.btDiscreteCollisionDetectorInterface::Result"* %939, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1324, %class.btVector3* nonnull align 4 dereferenceable(16) %posInWorld, float %fneg1327)
  %947 = bitcast %class.btVector3* %ref.tmp1324 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %947) #9
  br label %if.end1341

if.else1330:                                      ; preds = %for.end1321
  %948 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %949 = bitcast %class.btVector3* %ref.tmp1331 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %949) #9
  %950 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1331, %class.btVector3* nonnull align 4 dereferenceable(16) %950)
  %951 = bitcast %class.btVector3* %ref.tmp1332 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %951) #9
  %952 = bitcast %class.btVector3* %ref.tmp1333 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %952) #9
  %953 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %954 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx1334 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %954
  %955 = load i32, i32* %arrayidx1334, align 4, !tbaa !13
  %arrayidx1335 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %955
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1333, %class.btVector3* nonnull align 4 dereferenceable(16) %953, float* nonnull align 4 dereferenceable(4) %arrayidx1335)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1332, %class.btVector3* nonnull align 4 dereferenceable(16) %posInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1333)
  %956 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx1336 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %956
  %957 = load i32, i32* %arrayidx1336, align 4, !tbaa !13
  %arrayidx1337 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %957
  %958 = load float, float* %arrayidx1337, align 4, !tbaa !11
  %fneg1338 = fneg float %958
  %959 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %948 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1339 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %959, align 4, !tbaa !6
  %vfn1340 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1339, i64 4
  %960 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1340, align 4
  call void %960(%"struct.btDiscreteCollisionDetectorInterface::Result"* %948, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1331, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1332, float %fneg1338)
  %961 = bitcast %class.btVector3* %ref.tmp1333 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %961) #9
  %962 = bitcast %class.btVector3* %ref.tmp1332 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %962) #9
  %963 = bitcast %class.btVector3* %ref.tmp1331 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %963) #9
  br label %if.end1341

if.end1341:                                       ; preds = %if.else1330, %if.then1323
  %964 = bitcast %class.btVector3* %posInWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %964) #9
  br label %for.inc1342

for.inc1342:                                      ; preds = %if.end1341
  %965 = load i32, i32* %j, align 4, !tbaa !13
  %inc1343 = add nsw i32 %965, 1
  store i32 %inc1343, i32* %j, align 4, !tbaa !13
  br label %for.cond1304

for.end1344:                                      ; preds = %for.cond1304
  %966 = load i32, i32* %maxc.addr, align 4, !tbaa !13
  store i32 %966, i32* %cnum, align 4, !tbaa !13
  %967 = bitcast [8 x i32]* %iret to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %967) #9
  %968 = bitcast float* %maxdepth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %968) #9
  %969 = bitcast i32* %i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %969) #9
  br label %if.end1345

if.end1345:                                       ; preds = %for.end1344, %if.end1288
  %970 = load i32, i32* %code, align 4, !tbaa !13
  %971 = load i32*, i32** %return_code.addr, align 4, !tbaa !2
  store i32 %970, i32* %971, align 4, !tbaa !13
  %972 = load i32, i32* %cnum, align 4, !tbaa !13
  store i32 %972, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end1345, %if.then1218
  %973 = bitcast i32* %cnum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %973) #9
  %974 = bitcast float* %det1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %974) #9
  %975 = bitcast [8 x float]* %dep to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %975) #9
  %976 = bitcast [24 x float]* %point to i8*
  call void @llvm.lifetime.end.p0i8(i64 96, i8* %976) #9
  br label %cleanup1349

cleanup1349:                                      ; preds = %cleanup, %if.then1134
  %977 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %977) #9
  %978 = bitcast [16 x float]* %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %978) #9
  %979 = bitcast [2 x float]* %rect to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %979) #9
  %980 = bitcast float* %m22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %980) #9
  %981 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %981) #9
  %982 = bitcast float* %m12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %982) #9
  %983 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %983) #9
  %984 = bitcast float* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %984) #9
  %985 = bitcast float* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %985) #9
  %986 = bitcast [8 x float]* %quad to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %986) #9
  %987 = bitcast i32* %code2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %987) #9
  %988 = bitcast i32* %code1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %988) #9
  %989 = bitcast i32* %codeN to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %989) #9
  %990 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %990) #9
  %991 = bitcast i32* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %991) #9
  %992 = bitcast i32* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %992) #9
  %993 = bitcast i32* %lanr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %993) #9
  %994 = bitcast %class.btVector3* %anr to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %994) #9
  %995 = bitcast %class.btVector3* %nr to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %995) #9
  %996 = bitcast %class.btVector3* %normal2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %996) #9
  %997 = bitcast float** %Sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %997) #9
  %998 = bitcast float** %Sa to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %998) #9
  %999 = bitcast float** %pb921 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %999) #9
  %1000 = bitcast float** %pa920 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1000) #9
  %1001 = bitcast float** %Rb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1001) #9
  %1002 = bitcast float** %Ra to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1002) #9
  br label %cleanup1375

cleanup1375:                                      ; preds = %cleanup1349, %for.end915, %if.then748, %if.then712, %if.then655, %if.then598, %if.then541, %if.then484, %if.then427, %if.then370, %if.then313, %if.then258
  %1003 = bitcast float* %fudge2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1003) #9
  br label %cleanup1376

cleanup1376:                                      ; preds = %cleanup1375, %if.then217, %if.then190, %if.then163, %if.then137, %if.then112, %if.then
  %1004 = bitcast i32* %code to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1004) #9
  %1005 = bitcast i32* %invert_normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1005) #9
  %1006 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1006) #9
  %1007 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1007) #9
  %1008 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1008) #9
  %1009 = bitcast float* %s2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1009) #9
  %1010 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1010) #9
  %1011 = bitcast float* %Q33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1011) #9
  %1012 = bitcast float* %Q32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1012) #9
  %1013 = bitcast float* %Q31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1013) #9
  %1014 = bitcast float* %Q23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1014) #9
  %1015 = bitcast float* %Q22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1015) #9
  %1016 = bitcast float* %Q21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1016) #9
  %1017 = bitcast float* %Q13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1017) #9
  %1018 = bitcast float* %Q12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1018) #9
  %1019 = bitcast float* %Q11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1019) #9
  %1020 = bitcast float* %R33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1020) #9
  %1021 = bitcast float* %R32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1021) #9
  %1022 = bitcast float* %R31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1022) #9
  %1023 = bitcast float* %R23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1023) #9
  %1024 = bitcast float* %R22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1024) #9
  %1025 = bitcast float* %R21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1025) #9
  %1026 = bitcast float* %R13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1026) #9
  %1027 = bitcast float* %R12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1027) #9
  %1028 = bitcast float* %R11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1028) #9
  %1029 = bitcast [3 x float]* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %1029) #9
  %1030 = bitcast [3 x float]* %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %1030) #9
  %1031 = bitcast float** %normalR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1031) #9
  %1032 = bitcast %class.btVector3* %normalC to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %1032) #9
  %1033 = bitcast %class.btVector3* %pp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %1033) #9
  %1034 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %1034) #9
  %1035 = bitcast float* %fudge_factor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1035) #9
  %1036 = load i32, i32* %retval, align 4
  ret i32 %1036
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !11
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !11
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !11
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !11
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !11
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !11
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !11
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !11
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !11
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !11
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !11
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !11
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !11
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !11
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !11
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: nounwind
define internal float @_ZL6dDOT41PKfS0_(float* %a, float* %b) #0 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !11
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4, !tbaa !11
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 4
  %5 = load float, float* %arrayidx2, align 4, !tbaa !11
  %6 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !11
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 8
  %9 = load float, float* %arrayidx5, align 4, !tbaa !11
  %10 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 2
  %11 = load float, float* %arrayidx6, align 4, !tbaa !11
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: nounwind
define internal float @_ZL6dDOT44PKfS0_(float* %a, float* %b) #0 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !11
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4, !tbaa !11
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 4
  %5 = load float, float* %arrayidx2, align 4, !tbaa !11
  %6 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 4
  %7 = load float, float* %arrayidx3, align 4, !tbaa !11
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 8
  %9 = load float, float* %arrayidx5, align 4, !tbaa !11
  %10 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 8
  %11 = load float, float* %arrayidx6, align 4, !tbaa !11
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !11
  %0 = load float, float* %y.addr, align 4, !tbaa !11
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind
define internal float @_ZL6dDOT14PKfS0_(float* %a, float* %b) #0 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !11
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4, !tbaa !11
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 1
  %5 = load float, float* %arrayidx2, align 4, !tbaa !11
  %6 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 4
  %7 = load float, float* %arrayidx3, align 4, !tbaa !11
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 2
  %9 = load float, float* %arrayidx5, align 4, !tbaa !11
  %10 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 8
  %11 = load float, float* %arrayidx6, align 4, !tbaa !11
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !11
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !11
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !11
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !11
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !11
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !11
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: nounwind
define internal i32 @_ZL18intersectRectQuad2PfS_S_(float* %h, float* %p, float* %ret) #0 {
entry:
  %retval = alloca i32, align 4
  %h.addr = alloca float*, align 4
  %p.addr = alloca float*, align 4
  %ret.addr = alloca float*, align 4
  %nq = alloca i32, align 4
  %nr = alloca i32, align 4
  %buffer = alloca [16 x float], align 16
  %q = alloca float*, align 4
  %r = alloca float*, align 4
  %dir = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sign = alloca i32, align 4
  %pq = alloca float*, align 4
  %pr = alloca float*, align 4
  %i = alloca i32, align 4
  %nextq = alloca float*, align 4
  store float* %h, float** %h.addr, align 4, !tbaa !2
  store float* %p, float** %p.addr, align 4, !tbaa !2
  store float* %ret, float** %ret.addr, align 4, !tbaa !2
  %0 = bitcast i32* %nq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 4, i32* %nq, align 4, !tbaa !13
  %1 = bitcast i32* %nr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %nr, align 4, !tbaa !13
  %2 = bitcast [16 x float]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #9
  %3 = bitcast float** %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load float*, float** %p.addr, align 4, !tbaa !2
  store float* %4, float** %q, align 4, !tbaa !2
  %5 = bitcast float** %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load float*, float** %ret.addr, align 4, !tbaa !2
  store float* %6, float** %r, align 4, !tbaa !2
  %7 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store i32 0, i32* %dir, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc78, %entry
  %8 = load i32, i32* %dir, align 4, !tbaa !13
  %cmp = icmp sle i32 %8, 1
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup80

for.body:                                         ; preds = %for.cond
  %9 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store i32 -1, i32* %sign, align 4, !tbaa !13
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc73, %for.body
  %10 = load i32, i32* %sign, align 4, !tbaa !13
  %cmp2 = icmp sle i32 %10, 1
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup75

for.body4:                                        ; preds = %for.cond1
  %11 = bitcast float** %pq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load float*, float** %q, align 4, !tbaa !2
  store float* %12, float** %pq, align 4, !tbaa !2
  %13 = bitcast float** %pr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load float*, float** %r, align 4, !tbaa !2
  store float* %14, float** %pr, align 4, !tbaa !2
  store i32 0, i32* %nr, align 4, !tbaa !13
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = load i32, i32* %nq, align 4, !tbaa !13
  store i32 %16, i32* %i, align 4, !tbaa !13
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body4
  %17 = load i32, i32* %i, align 4, !tbaa !13
  %cmp6 = icmp sgt i32 %17, 0
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup62

for.body8:                                        ; preds = %for.cond5
  %18 = load i32, i32* %sign, align 4, !tbaa !13
  %conv = sitofp i32 %18 to float
  %19 = load float*, float** %pq, align 4, !tbaa !2
  %20 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds float, float* %19, i32 %20
  %21 = load float, float* %arrayidx, align 4, !tbaa !11
  %mul = fmul float %conv, %21
  %22 = load float*, float** %h.addr, align 4, !tbaa !2
  %23 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx9 = getelementptr inbounds float, float* %22, i32 %23
  %24 = load float, float* %arrayidx9, align 4, !tbaa !11
  %cmp10 = fcmp olt float %mul, %24
  br i1 %cmp10, label %if.then, label %if.end16

if.then:                                          ; preds = %for.body8
  %25 = load float*, float** %pq, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds float, float* %25, i32 0
  %26 = load float, float* %arrayidx11, align 4, !tbaa !11
  %27 = load float*, float** %pr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds float, float* %27, i32 0
  store float %26, float* %arrayidx12, align 4, !tbaa !11
  %28 = load float*, float** %pq, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds float, float* %28, i32 1
  %29 = load float, float* %arrayidx13, align 4, !tbaa !11
  %30 = load float*, float** %pr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %30, i32 1
  store float %29, float* %arrayidx14, align 4, !tbaa !11
  %31 = load float*, float** %pr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds float, float* %31, i32 2
  store float* %add.ptr, float** %pr, align 4, !tbaa !2
  %32 = load i32, i32* %nr, align 4, !tbaa !13
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %nr, align 4, !tbaa !13
  %33 = load i32, i32* %nr, align 4, !tbaa !13
  %and = and i32 %33, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then
  %34 = load float*, float** %r, align 4, !tbaa !2
  store float* %34, float** %q, align 4, !tbaa !2
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup62

if.end:                                           ; preds = %if.then
  br label %if.end16

if.end16:                                         ; preds = %if.end, %for.body8
  %35 = bitcast float** %nextq to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #9
  %36 = load i32, i32* %i, align 4, !tbaa !13
  %cmp17 = icmp sgt i32 %36, 1
  br i1 %cmp17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end16
  %37 = load float*, float** %pq, align 4, !tbaa !2
  %add.ptr18 = getelementptr inbounds float, float* %37, i32 2
  br label %cond.end

cond.false:                                       ; preds = %if.end16
  %38 = load float*, float** %q, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %add.ptr18, %cond.true ], [ %38, %cond.false ]
  store float* %cond, float** %nextq, align 4, !tbaa !2
  %39 = load i32, i32* %sign, align 4, !tbaa !13
  %conv19 = sitofp i32 %39 to float
  %40 = load float*, float** %pq, align 4, !tbaa !2
  %41 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx20 = getelementptr inbounds float, float* %40, i32 %41
  %42 = load float, float* %arrayidx20, align 4, !tbaa !11
  %mul21 = fmul float %conv19, %42
  %43 = load float*, float** %h.addr, align 4, !tbaa !2
  %44 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx22 = getelementptr inbounds float, float* %43, i32 %44
  %45 = load float, float* %arrayidx22, align 4, !tbaa !11
  %cmp23 = fcmp olt float %mul21, %45
  %conv24 = zext i1 %cmp23 to i32
  %46 = load i32, i32* %sign, align 4, !tbaa !13
  %conv25 = sitofp i32 %46 to float
  %47 = load float*, float** %nextq, align 4, !tbaa !2
  %48 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx26 = getelementptr inbounds float, float* %47, i32 %48
  %49 = load float, float* %arrayidx26, align 4, !tbaa !11
  %mul27 = fmul float %conv25, %49
  %50 = load float*, float** %h.addr, align 4, !tbaa !2
  %51 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx28 = getelementptr inbounds float, float* %50, i32 %51
  %52 = load float, float* %arrayidx28, align 4, !tbaa !11
  %cmp29 = fcmp olt float %mul27, %52
  %conv30 = zext i1 %cmp29 to i32
  %xor = xor i32 %conv24, %conv30
  %tobool31 = icmp ne i32 %xor, 0
  br i1 %tobool31, label %if.then32, label %if.end60

if.then32:                                        ; preds = %cond.end
  %53 = load float*, float** %pq, align 4, !tbaa !2
  %54 = load i32, i32* %dir, align 4, !tbaa !13
  %sub = sub nsw i32 1, %54
  %arrayidx33 = getelementptr inbounds float, float* %53, i32 %sub
  %55 = load float, float* %arrayidx33, align 4, !tbaa !11
  %56 = load float*, float** %nextq, align 4, !tbaa !2
  %57 = load i32, i32* %dir, align 4, !tbaa !13
  %sub34 = sub nsw i32 1, %57
  %arrayidx35 = getelementptr inbounds float, float* %56, i32 %sub34
  %58 = load float, float* %arrayidx35, align 4, !tbaa !11
  %59 = load float*, float** %pq, align 4, !tbaa !2
  %60 = load i32, i32* %dir, align 4, !tbaa !13
  %sub36 = sub nsw i32 1, %60
  %arrayidx37 = getelementptr inbounds float, float* %59, i32 %sub36
  %61 = load float, float* %arrayidx37, align 4, !tbaa !11
  %sub38 = fsub float %58, %61
  %62 = load float*, float** %nextq, align 4, !tbaa !2
  %63 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx39 = getelementptr inbounds float, float* %62, i32 %63
  %64 = load float, float* %arrayidx39, align 4, !tbaa !11
  %65 = load float*, float** %pq, align 4, !tbaa !2
  %66 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx40 = getelementptr inbounds float, float* %65, i32 %66
  %67 = load float, float* %arrayidx40, align 4, !tbaa !11
  %sub41 = fsub float %64, %67
  %div = fdiv float %sub38, %sub41
  %68 = load i32, i32* %sign, align 4, !tbaa !13
  %conv42 = sitofp i32 %68 to float
  %69 = load float*, float** %h.addr, align 4, !tbaa !2
  %70 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx43 = getelementptr inbounds float, float* %69, i32 %70
  %71 = load float, float* %arrayidx43, align 4, !tbaa !11
  %mul44 = fmul float %conv42, %71
  %72 = load float*, float** %pq, align 4, !tbaa !2
  %73 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx45 = getelementptr inbounds float, float* %72, i32 %73
  %74 = load float, float* %arrayidx45, align 4, !tbaa !11
  %sub46 = fsub float %mul44, %74
  %mul47 = fmul float %div, %sub46
  %add = fadd float %55, %mul47
  %75 = load float*, float** %pr, align 4, !tbaa !2
  %76 = load i32, i32* %dir, align 4, !tbaa !13
  %sub48 = sub nsw i32 1, %76
  %arrayidx49 = getelementptr inbounds float, float* %75, i32 %sub48
  store float %add, float* %arrayidx49, align 4, !tbaa !11
  %77 = load i32, i32* %sign, align 4, !tbaa !13
  %conv50 = sitofp i32 %77 to float
  %78 = load float*, float** %h.addr, align 4, !tbaa !2
  %79 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx51 = getelementptr inbounds float, float* %78, i32 %79
  %80 = load float, float* %arrayidx51, align 4, !tbaa !11
  %mul52 = fmul float %conv50, %80
  %81 = load float*, float** %pr, align 4, !tbaa !2
  %82 = load i32, i32* %dir, align 4, !tbaa !13
  %arrayidx53 = getelementptr inbounds float, float* %81, i32 %82
  store float %mul52, float* %arrayidx53, align 4, !tbaa !11
  %83 = load float*, float** %pr, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds float, float* %83, i32 2
  store float* %add.ptr54, float** %pr, align 4, !tbaa !2
  %84 = load i32, i32* %nr, align 4, !tbaa !13
  %inc55 = add nsw i32 %84, 1
  store i32 %inc55, i32* %nr, align 4, !tbaa !13
  %85 = load i32, i32* %nr, align 4, !tbaa !13
  %and56 = and i32 %85, 8
  %tobool57 = icmp ne i32 %and56, 0
  br i1 %tobool57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.then32
  %86 = load float*, float** %r, align 4, !tbaa !2
  store float* %86, float** %q, align 4, !tbaa !2
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end59:                                         ; preds = %if.then32
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %cond.end
  %87 = load float*, float** %pq, align 4, !tbaa !2
  %add.ptr61 = getelementptr inbounds float, float* %87, i32 2
  store float* %add.ptr61, float** %pq, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then58, %if.end60
  %88 = bitcast float** %nextq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup62 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %89 = load i32, i32* %i, align 4, !tbaa !13
  %dec = add nsw i32 %89, -1
  store i32 %dec, i32* %i, align 4, !tbaa !13
  br label %for.cond5

cleanup62:                                        ; preds = %if.then15, %cleanup, %for.cond.cleanup7
  %90 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  %cleanup.dest63 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest63, label %cleanup69 [
    i32 8, label %for.end
  ]

for.end:                                          ; preds = %cleanup62
  %91 = load float*, float** %r, align 4, !tbaa !2
  store float* %91, float** %q, align 4, !tbaa !2
  %92 = load float*, float** %q, align 4, !tbaa !2
  %93 = load float*, float** %ret.addr, align 4, !tbaa !2
  %cmp64 = icmp eq float* %92, %93
  br i1 %cmp64, label %cond.true65, label %cond.false66

cond.true65:                                      ; preds = %for.end
  %arraydecay = getelementptr inbounds [16 x float], [16 x float]* %buffer, i32 0, i32 0
  br label %cond.end67

cond.false66:                                     ; preds = %for.end
  %94 = load float*, float** %ret.addr, align 4, !tbaa !2
  br label %cond.end67

cond.end67:                                       ; preds = %cond.false66, %cond.true65
  %cond68 = phi float* [ %arraydecay, %cond.true65 ], [ %94, %cond.false66 ]
  store float* %cond68, float** %r, align 4, !tbaa !2
  %95 = load i32, i32* %nr, align 4, !tbaa !13
  store i32 %95, i32* %nq, align 4, !tbaa !13
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup69

cleanup69:                                        ; preds = %cond.end67, %cleanup62
  %96 = bitcast float** %pr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #9
  %97 = bitcast float** %pq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #9
  %cleanup.dest71 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest71, label %cleanup75 [
    i32 0, label %cleanup.cont72
  ]

cleanup.cont72:                                   ; preds = %cleanup69
  br label %for.inc73

for.inc73:                                        ; preds = %cleanup.cont72
  %98 = load i32, i32* %sign, align 4, !tbaa !13
  %add74 = add nsw i32 %98, 2
  store i32 %add74, i32* %sign, align 4, !tbaa !13
  br label %for.cond1

cleanup75:                                        ; preds = %cleanup69, %for.cond.cleanup3
  %99 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #9
  %cleanup.dest76 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest76, label %cleanup80 [
    i32 5, label %for.end77
  ]

for.end77:                                        ; preds = %cleanup75
  br label %for.inc78

for.inc78:                                        ; preds = %for.end77
  %100 = load i32, i32* %dir, align 4, !tbaa !13
  %inc79 = add nsw i32 %100, 1
  store i32 %inc79, i32* %dir, align 4, !tbaa !13
  br label %for.cond

cleanup80:                                        ; preds = %cleanup75, %for.cond.cleanup
  %101 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %cleanup.dest81 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest81, label %cleanup88 [
    i32 2, label %for.end82
    i32 11, label %done
  ]

for.end82:                                        ; preds = %cleanup80
  br label %done

done:                                             ; preds = %for.end82, %cleanup80
  %102 = load float*, float** %q, align 4, !tbaa !2
  %103 = load float*, float** %ret.addr, align 4, !tbaa !2
  %cmp83 = icmp ne float* %102, %103
  br i1 %cmp83, label %if.then84, label %if.end87

if.then84:                                        ; preds = %done
  %104 = load float*, float** %ret.addr, align 4, !tbaa !2
  %105 = bitcast float* %104 to i8*
  %106 = load float*, float** %q, align 4, !tbaa !2
  %107 = bitcast float* %106 to i8*
  %108 = load i32, i32* %nr, align 4, !tbaa !13
  %mul85 = mul nsw i32 %108, 2
  %mul86 = mul i32 %mul85, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %105, i8* align 4 %107, i32 %mul86, i1 false)
  br label %if.end87

if.end87:                                         ; preds = %if.then84, %done
  %109 = load i32, i32* %nr, align 4, !tbaa !13
  store i32 %109, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup88

cleanup88:                                        ; preds = %if.end87, %cleanup80
  %110 = bitcast float** %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast float** %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast [16 x float]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %112) #9
  %113 = bitcast i32* %nr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  %114 = bitcast i32* %nq to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #9
  %115 = load i32, i32* %retval, align 4
  ret i32 %115
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !11
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !11
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !11
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !11
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !11
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !11
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !11
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !11
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !11
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

define hidden void @_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%struct.btBoxBoxDetector* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output, %class.btIDebugDraw* %0, i1 zeroext %1) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btBoxBoxDetector*, align 4
  %input.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %.addr = alloca %class.btIDebugDraw*, align 4
  %.addr1 = alloca i8, align 1
  %transformA = alloca %class.btTransform*, align 4
  %transformB = alloca %class.btTransform*, align 4
  %skip = alloca i32, align 4
  %contact = alloca %struct.dContactGeom*, align 4
  %R1 = alloca [12 x float], align 16
  %R2 = alloca [12 x float], align 16
  %j = alloca i32, align 4
  %normal = alloca %class.btVector3, align 4
  %depth = alloca float, align 4
  %return_code = alloca i32, align 4
  %maxc = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca float, align 4
  %ref.tmp43 = alloca %class.btVector3, align 4
  store %struct.btBoxBoxDetector* %this, %struct.btBoxBoxDetector** %this.addr, align 4, !tbaa !2
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %.addr, align 4, !tbaa !2
  %frombool = zext i1 %1 to i8
  store i8 %frombool, i8* %.addr1, align 1, !tbaa !17
  %this2 = load %struct.btBoxBoxDetector*, %struct.btBoxBoxDetector** %this.addr, align 4
  %2 = bitcast %class.btTransform** %transformA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %3, i32 0, i32 0
  store %class.btTransform* %m_transformA, %class.btTransform** %transformA, align 4, !tbaa !2
  %4 = bitcast %class.btTransform** %transformB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4, !tbaa !2
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %5, i32 0, i32 1
  store %class.btTransform* %m_transformB, %class.btTransform** %transformB, align 4, !tbaa !2
  %6 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store i32 0, i32* %skip, align 4, !tbaa !13
  %7 = bitcast %struct.dContactGeom** %contact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store %struct.dContactGeom* null, %struct.dContactGeom** %contact, align 4, !tbaa !2
  %8 = bitcast [12 x float]* %R1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %8) #9
  %9 = bitcast [12 x float]* %R2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %9) #9
  %10 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %j, align 4, !tbaa !13
  %cmp = icmp slt i32 %11, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %class.btTransform*, %class.btTransform** %transformA, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %13)
  %14 = load i32, i32* %j, align 4, !tbaa !13
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call, i32 %14)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call3)
  %15 = load float, float* %call4, align 4, !tbaa !11
  %16 = load i32, i32* %j, align 4, !tbaa !13
  %mul = mul nsw i32 4, %16
  %add = add nsw i32 0, %mul
  %arrayidx = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 %add
  store float %15, float* %arrayidx, align 4, !tbaa !11
  %17 = load %class.btTransform*, %class.btTransform** %transformB, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %17)
  %18 = load i32, i32* %j, align 4, !tbaa !13
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call5, i32 %18)
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call6)
  %19 = load float, float* %call7, align 4, !tbaa !11
  %20 = load i32, i32* %j, align 4, !tbaa !13
  %mul8 = mul nsw i32 4, %20
  %add9 = add nsw i32 0, %mul8
  %arrayidx10 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 %add9
  store float %19, float* %arrayidx10, align 4, !tbaa !11
  %21 = load %class.btTransform*, %class.btTransform** %transformA, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %21)
  %22 = load i32, i32* %j, align 4, !tbaa !13
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call11, i32 %22)
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call12)
  %23 = load float, float* %call13, align 4, !tbaa !11
  %24 = load i32, i32* %j, align 4, !tbaa !13
  %mul14 = mul nsw i32 4, %24
  %add15 = add nsw i32 1, %mul14
  %arrayidx16 = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 %add15
  store float %23, float* %arrayidx16, align 4, !tbaa !11
  %25 = load %class.btTransform*, %class.btTransform** %transformB, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %25)
  %26 = load i32, i32* %j, align 4, !tbaa !13
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call17, i32 %26)
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call18)
  %27 = load float, float* %call19, align 4, !tbaa !11
  %28 = load i32, i32* %j, align 4, !tbaa !13
  %mul20 = mul nsw i32 4, %28
  %add21 = add nsw i32 1, %mul20
  %arrayidx22 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 %add21
  store float %27, float* %arrayidx22, align 4, !tbaa !11
  %29 = load %class.btTransform*, %class.btTransform** %transformA, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %29)
  %30 = load i32, i32* %j, align 4, !tbaa !13
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call23, i32 %30)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call24)
  %31 = load float, float* %call25, align 4, !tbaa !11
  %32 = load i32, i32* %j, align 4, !tbaa !13
  %mul26 = mul nsw i32 4, %32
  %add27 = add nsw i32 2, %mul26
  %arrayidx28 = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 %add27
  store float %31, float* %arrayidx28, align 4, !tbaa !11
  %33 = load %class.btTransform*, %class.btTransform** %transformB, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %33)
  %34 = load i32, i32* %j, align 4, !tbaa !13
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call29, i32 %34)
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call30)
  %35 = load float, float* %call31, align 4, !tbaa !11
  %36 = load i32, i32* %j, align 4, !tbaa !13
  %mul32 = mul nsw i32 4, %36
  %add33 = add nsw i32 2, %mul32
  %arrayidx34 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 %add33
  store float %35, float* %arrayidx34, align 4, !tbaa !11
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %37 = load i32, i32* %j, align 4, !tbaa !13
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %38 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #9
  %call35 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  %39 = bitcast float* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = bitcast i32* %return_code to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #9
  %41 = bitcast i32* %maxc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #9
  store i32 4, i32* %maxc, align 4, !tbaa !13
  %42 = load %class.btTransform*, %class.btTransform** %transformA, align 4, !tbaa !2
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %42)
  %arraydecay = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 0
  %43 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #9
  %44 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  store float 2.000000e+00, float* %ref.tmp37, align 4, !tbaa !11
  %45 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #9
  %m_box1 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this2, i32 0, i32 1
  %46 = load %class.btBoxShape*, %class.btBoxShape** %m_box1, align 4, !tbaa !8
  call void @_ZNK10btBoxShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %ref.tmp38, %class.btBoxShape* %46)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  %47 = load %class.btTransform*, %class.btTransform** %transformB, align 4, !tbaa !2
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %47)
  %arraydecay40 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 0
  %48 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #9
  %49 = bitcast float* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  store float 2.000000e+00, float* %ref.tmp42, align 4, !tbaa !11
  %50 = bitcast %class.btVector3* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #9
  %m_box2 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this2, i32 0, i32 2
  %51 = load %class.btBoxShape*, %class.btBoxShape** %m_box2, align 4, !tbaa !10
  call void @_ZNK10btBoxShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %ref.tmp43, %class.btBoxShape* %51)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, float* nonnull align 4 dereferenceable(4) %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp43)
  %52 = load i32, i32* %maxc, align 4, !tbaa !13
  %53 = load %struct.dContactGeom*, %struct.dContactGeom** %contact, align 4, !tbaa !2
  %54 = load i32, i32* %skip, align 4, !tbaa !13
  %55 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4, !tbaa !2
  %call44 = call i32 @_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %call36, float* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call39, float* %arraydecay40, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* %depth, i32* %return_code, i32 %52, %struct.dContactGeom* %53, i32 %54, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %55)
  %56 = bitcast %class.btVector3* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #9
  %57 = bitcast float* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  %58 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #9
  %59 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #9
  %60 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #9
  %61 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #9
  %62 = bitcast i32* %maxc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #9
  %63 = bitcast i32* %return_code to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #9
  %64 = bitcast float* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #9
  %65 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #9
  %66 = bitcast [12 x float]* %R2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %66) #9
  %67 = bitcast [12 x float]* %R1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %67) #9
  %68 = bitcast %struct.dContactGeom** %contact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #9
  %69 = bitcast i32* %skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #9
  %70 = bitcast %class.btTransform** %transformB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  %71 = bitcast %class.btTransform** %transformA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !13
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

define linkonce_odr hidden void @_ZNK10btBoxShape24getHalfExtentsWithMarginEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btBoxShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btBoxShape*, align 4
  %margin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btBoxShape* %this, %class.btBoxShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBoxShape*, %class.btBoxShape** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK10btBoxShape27getHalfExtentsWithoutMarginEv(%class.btBoxShape* %this1)
  %0 = bitcast %class.btVector3* %agg.result to i8*
  %1 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false), !tbaa.struct !15
  %2 = bitcast %class.btVector3* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %5 = bitcast %class.btConvexInternalShape* %4 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %6 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call2 = call float %6(%class.btConvexInternalShape* %4)
  store float %call2, float* %ref.tmp, align 4, !tbaa !11
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %9 = bitcast %class.btConvexInternalShape* %8 to float (%class.btConvexInternalShape*)***
  %vtable4 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %9, align 4, !tbaa !6
  %vfn5 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable4, i64 12
  %10 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn5, align 4
  %call6 = call float %10(%class.btConvexInternalShape* %8)
  store float %call6, float* %ref.tmp3, align 4, !tbaa !11
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %13 = bitcast %class.btConvexInternalShape* %12 to float (%class.btConvexInternalShape*)***
  %vtable8 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %13, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable8, i64 12
  %14 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn9, align 4
  %call10 = call float %14(%class.btConvexInternalShape* %12)
  store float %call10, float* %ref.tmp7, align 4, !tbaa !11
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %margin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %15 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %margin)
  %18 = bitcast %class.btVector3* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btBoxBoxDetectorD0Ev(%struct.btBoxBoxDetector* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btBoxBoxDetector*, align 4
  store %struct.btBoxBoxDetector* %this, %struct.btBoxBoxDetector** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBoxBoxDetector*, %struct.btBoxBoxDetector** %this.addr, align 4
  %call = call %struct.btBoxBoxDetector* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to %struct.btBoxBoxDetector* (%struct.btBoxBoxDetector*)*)(%struct.btBoxBoxDetector* %this1) #9
  %0 = bitcast %struct.btBoxBoxDetector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev(%struct.btDiscreteCollisionDetectorInterface* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  call void @llvm.trap() #12
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #5

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #7

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK10btBoxShape27getHalfExtentsWithoutMarginEv(%class.btBoxShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btBoxShape*, align 4
  store %class.btBoxShape* %this, %class.btBoxShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBoxShape*, %class.btBoxShape** %this.addr, align 4
  %0 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  ret %class.btVector3* %m_implicitShapeDimensions
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !11
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !11
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !11
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !11
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !11
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !11
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !11
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !11
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !11
  ret %class.btVector3* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { cold noreturn nounwind }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { nounwind readnone }
attributes #11 = { builtin nounwind }
attributes #12 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"_ZTS16btBoxBoxDetector", !3, i64 4, !3, i64 8}
!10 = !{!9, !3, i64 8}
!11 = !{!12, !12, i64 0}
!12 = !{!"float", !4, i64 0}
!13 = !{!14, !14, i64 0}
!14 = !{!"int", !4, i64 0}
!15 = !{i64 0, i64 16, !16}
!16 = !{!4, !4, i64 0}
!17 = !{!18, !18, i64 0}
!18 = !{!"bool", !4, i64 0}
