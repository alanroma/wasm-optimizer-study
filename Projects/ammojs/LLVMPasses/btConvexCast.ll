; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btConvexCast.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btConvexCast.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConvexCast = type { i32 (...)** }

@_ZTV12btConvexCast = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI12btConvexCast to i8*), i8* bitcast (%class.btConvexCast* (%class.btConvexCast*)* @_ZN12btConvexCastD1Ev to i8*), i8* bitcast (void (%class.btConvexCast*)* @_ZN12btConvexCastD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS12btConvexCast = hidden constant [15 x i8] c"12btConvexCast\00", align 1
@_ZTI12btConvexCast = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btConvexCast, i32 0, i32 0) }, align 4

@_ZN12btConvexCastD1Ev = hidden unnamed_addr alias %class.btConvexCast* (%class.btConvexCast*), %class.btConvexCast* (%class.btConvexCast*)* @_ZN12btConvexCastD2Ev

; Function Attrs: nounwind
define hidden %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexCast*, align 4
  store %class.btConvexCast* %this, %class.btConvexCast** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexCast*, %class.btConvexCast** %this.addr, align 4
  ret %class.btConvexCast* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN12btConvexCastD0Ev(%class.btConvexCast* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexCast*, align 4
  store %class.btConvexCast* %this, %class.btConvexCast** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexCast*, %class.btConvexCast** %this.addr, align 4
  call void @llvm.trap() #2
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #1

declare void @__cxa_pure_virtual() unnamed_addr

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { cold noreturn nounwind }
attributes #2 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
