; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btMultiSapBroadphase.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btMultiSapBroadphase.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiSapBroadphase = type { %class.btBroadphaseInterface, %class.btAlignedObjectArray, %class.btSimpleBroadphase*, %class.btOverlappingPairCache*, %class.btQuantizedBvh*, i8, %struct.btOverlapFilterCallback*, i32, %class.btAlignedObjectArray.12 }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btBroadphaseInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSimpleBroadphase = type { %class.btBroadphaseInterface, i32, i32, i32, %struct.btSimpleBroadphaseProxy*, i8*, i32, %class.btOverlappingPairCache*, i8, i32 }
%struct.btSimpleBroadphaseProxy = type { %struct.btBroadphaseProxy, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, i32, %class.btAlignedObjectArray.8, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %"struct.btMultiSapBroadphase::btMultiSapProxy"**, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%"struct.btMultiSapBroadphase::btMultiSapProxy" = type { %struct.btBroadphaseProxy, %class.btAlignedObjectArray.15, %class.btVector3, %class.btVector3, i32 }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, %"struct.btMultiSapBroadphase::btBridgeProxy"**, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%"struct.btMultiSapBroadphase::btBridgeProxy" = type { %struct.btBroadphaseProxy*, %class.btBroadphaseInterface* }
%class.btSortedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.20, i8, i8, %struct.btOverlapFilterCallback*, %class.btOverlappingPairCallback* }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon }
%class.btCollisionAlgorithm = type opaque
%union.anon = type { i8* }
%struct.btMultiSapOverlapFilterCallback = type { %struct.btOverlapFilterCallback }
%class.btDispatcher = type opaque
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%struct.MyNodeOverlapCallback = type { %class.btNodeOverlapCallback, %class.btMultiSapBroadphase*, %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %class.btDispatcher* }
%class.btNodeOverlapCallback = type { i32 (...)** }
%class.btMultiSapBroadphasePairSortPredicate = type { i8 }

$_ZN21btBroadphaseInterfaceC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceED2Ev = comdat any

$_ZN14btQuantizedBvhnwEm = comdat any

$_ZN14btQuantizedBvh16getLeafNodeArrayEv = comdat any

$_ZNK20btAlignedObjectArrayIP21btBroadphaseInterfaceE4sizeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceEixEi = comdat any

$_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_ = comdat any

$_ZN17btBroadphaseProxynwEmPv = comdat any

$_ZN20btMultiSapBroadphase15btMultiSapProxyC2ERK9btVector3S3_iPvss = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE9push_backERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE9push_backERKS2_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEixEi = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8pop_backEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI37btMultiSapBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN16btBroadphasePairC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv = comdat any

$_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv = comdat any

$_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_ = comdat any

$_ZN21btBroadphaseInterfaceD2Ev = comdat any

$_ZN21btBroadphaseInterfaceD0Ev = comdat any

$_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher = comdat any

$_ZN23btOverlapFilterCallbackC2Ev = comdat any

$_ZN23btOverlapFilterCallbackD2Ev = comdat any

$_ZN23btOverlapFilterCallbackD0Ev = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN17btBroadphaseProxyC2ERK9btVector3S2_PvssS3_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEC2Ev = comdat any

$_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4initEv = comdat any

$_ZN21btNodeOverlapCallbackC2Ev = comdat any

$_ZN21btNodeOverlapCallbackD2Ev = comdat any

$_ZN21btNodeOverlapCallbackD0Ev = comdat any

$_ZN20btMultiSapBroadphase18getBroadphaseArrayEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN18btAlignedAllocatorIP21btBroadphaseInterfaceLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE4initEv = comdat any

$_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP21btBroadphaseInterfaceLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EE10deallocateEPS2_ = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi = comdat any

$_ZN18btQuantizedBvhNodenwEmPv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4copyEiiPS2_ = comdat any

$_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EE8allocateEiPPKS2_ = comdat any

$_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4copyEiiPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EE8allocateEiPPKS2_ = comdat any

$_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EE10deallocateEPS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZNK37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZTS21btBroadphaseInterface = comdat any

$_ZTI21btBroadphaseInterface = comdat any

$_ZTV21btBroadphaseInterface = comdat any

$_ZTS23btOverlapFilterCallback = comdat any

$_ZTI23btOverlapFilterCallback = comdat any

$_ZTV23btOverlapFilterCallback = comdat any

$_ZTS21btNodeOverlapCallback = comdat any

$_ZTI21btNodeOverlapCallback = comdat any

$_ZTV21btNodeOverlapCallback = comdat any

@_ZTV20btMultiSapBroadphase = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btMultiSapBroadphase to i8*), i8* bitcast (%class.btMultiSapBroadphase* (%class.btMultiSapBroadphase*)* @_ZN20btMultiSapBroadphaseD1Ev to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*)* @_ZN20btMultiSapBroadphaseD0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btMultiSapBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)* @_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_ to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_ to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_ to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*, %class.btDispatcher*)* @_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)* @_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)* @_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*, %class.btVector3*, %class.btVector3*)* @_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_ to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*, %class.btDispatcher*)* @_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btMultiSapBroadphase*)* @_ZN20btMultiSapBroadphase10printStatsEv to i8*)] }, align 4
@stopUpdating = hidden global i8 0, align 1
@gOverlappingPairs = external global i32, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS20btMultiSapBroadphase = hidden constant [23 x i8] c"20btMultiSapBroadphase\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btBroadphaseInterface = linkonce_odr hidden constant [24 x i8] c"21btBroadphaseInterface\00", comdat, align 1
@_ZTI21btBroadphaseInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btBroadphaseInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI20btMultiSapBroadphase = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btMultiSapBroadphase, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, align 4
@_ZTV21btBroadphaseInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*), i8* bitcast (%class.btBroadphaseInterface* (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD2Ev to i8*), i8* bitcast (void (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btBroadphaseInterface*, %class.btDispatcher*)* @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback to i8*), i8* bitcast (%struct.btOverlapFilterCallback* (%struct.btOverlapFilterCallback*)* @_ZN23btOverlapFilterCallbackD2Ev to i8*), i8* bitcast (void (%struct.btMultiSapOverlapFilterCallback*)* @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev to i8*), i8* bitcast (i1 (%struct.btMultiSapOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_ to i8*)] }, align 4
@_ZTSZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback = internal constant [88 x i8] c"ZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback\00", align 1
@_ZTS23btOverlapFilterCallback = linkonce_odr hidden constant [26 x i8] c"23btOverlapFilterCallback\00", comdat, align 1
@_ZTI23btOverlapFilterCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btOverlapFilterCallback, i32 0, i32 0) }, comdat, align 4
@_ZTIZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([88 x i8], [88 x i8]* @_ZTSZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI23btOverlapFilterCallback to i8*) }, align 4
@_ZTV23btOverlapFilterCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI23btOverlapFilterCallback to i8*), i8* bitcast (%struct.btOverlapFilterCallback* (%struct.btOverlapFilterCallback*)* @_ZN23btOverlapFilterCallbackD2Ev to i8*), i8* bitcast (void (%struct.btOverlapFilterCallback*)* @_ZN23btOverlapFilterCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback to i8*), i8* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback*)* @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback*, i32, i32)* @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii to i8*)] }, align 4
@_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback = internal constant [108 x i8] c"ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback\00", align 1
@_ZTS21btNodeOverlapCallback = linkonce_odr hidden constant [24 x i8] c"21btNodeOverlapCallback\00", comdat, align 1
@_ZTI21btNodeOverlapCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btNodeOverlapCallback, i32 0, i32 0) }, comdat, align 4
@_ZTIZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([108 x i8], [108 x i8]* @_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*) }, align 4
@_ZTV21btNodeOverlapCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*), i8* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN20btMultiSapBroadphaseD1Ev = hidden unnamed_addr alias %class.btMultiSapBroadphase* (%class.btMultiSapBroadphase*), %class.btMultiSapBroadphase* (%class.btMultiSapBroadphase*)* @_ZN20btMultiSapBroadphaseD2Ev

define hidden %class.btMultiSapBroadphase* @_ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache(%class.btMultiSapBroadphase* returned %this, i32 %0, %class.btOverlappingPairCache* %pairCache) unnamed_addr #0 {
entry:
  %retval = alloca %class.btMultiSapBroadphase*, align 4
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %.addr = alloca i32, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %mem = alloca i8*, align 4
  %mem9 = alloca i8*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !6
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  store %class.btMultiSapBroadphase* %this1, %class.btMultiSapBroadphase** %retval, align 4
  %1 = bitcast %class.btMultiSapBroadphase* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %1) #9
  %2 = bitcast %class.btMultiSapBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btMultiSapBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !8
  %m_sapBroadphases = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceEC2Ev(%class.btAlignedObjectArray* %m_sapBroadphases)
  %m_overlappingPairs = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %3, %class.btOverlappingPairCache** %m_overlappingPairs, align 4, !tbaa !10
  %m_optimizedAabbTree = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  store %class.btQuantizedBvh* null, %class.btQuantizedBvh** %m_optimizedAabbTree, align 4, !tbaa !17
  %m_ownsPairCache = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsPairCache, align 4, !tbaa !18
  %m_invalidPair = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 7
  store i32 0, i32* %m_invalidPair, align 4, !tbaa !19
  %m_multiSapProxies = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 8
  %call3 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEEC2Ev(%class.btAlignedObjectArray.12* %m_multiSapProxies)
  %m_overlappingPairs4 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_overlappingPairs4, align 4, !tbaa !10
  %tobool = icmp ne %class.btOverlappingPairCache* %4, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %m_ownsPairCache5 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsPairCache5, align 4, !tbaa !18
  %5 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %call6 = call i8* @_Z22btAlignedAllocInternalmi(i32 36, i32 16)
  store i8* %call6, i8** %mem, align 4, !tbaa !2
  %6 = load i8*, i8** %mem, align 4, !tbaa !2
  %7 = bitcast i8* %6 to %class.btSortedOverlappingPairCache*
  %call7 = call %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheC1Ev(%class.btSortedOverlappingPairCache* %7)
  %8 = bitcast %class.btSortedOverlappingPairCache* %7 to %class.btOverlappingPairCache*
  %m_overlappingPairs8 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  store %class.btOverlappingPairCache* %8, %class.btOverlappingPairCache** %m_overlappingPairs8, align 4, !tbaa !10
  %9 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = bitcast i8** %mem9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %call10 = call i8* @_Z22btAlignedAllocInternalmi(i32 4, i32 16)
  store i8* %call10, i8** %mem9, align 4, !tbaa !2
  %11 = load i8*, i8** %mem9, align 4, !tbaa !2
  %12 = bitcast i8* %11 to %struct.btMultiSapOverlapFilterCallback*
  %13 = bitcast %struct.btMultiSapOverlapFilterCallback* %12 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %13, i8 0, i32 4, i1 false)
  %call11 = call %struct.btMultiSapOverlapFilterCallback* @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackC2Ev(%struct.btMultiSapOverlapFilterCallback* %12) #9
  %14 = bitcast %struct.btMultiSapOverlapFilterCallback* %12 to %struct.btOverlapFilterCallback*
  %m_filterCallback = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 6
  store %struct.btOverlapFilterCallback* %14, %struct.btOverlapFilterCallback** %m_filterCallback, align 4, !tbaa !20
  %m_overlappingPairs12 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  %15 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_overlappingPairs12, align 4, !tbaa !10
  %m_filterCallback13 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 6
  %16 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_filterCallback13, align 4, !tbaa !20
  %17 = bitcast %class.btOverlappingPairCache* %15 to void (%class.btOverlappingPairCache*, %struct.btOverlapFilterCallback*)***
  %vtable = load void (%class.btOverlappingPairCache*, %struct.btOverlapFilterCallback*)**, void (%class.btOverlappingPairCache*, %struct.btOverlapFilterCallback*)*** %17, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btOverlapFilterCallback*)*, void (%class.btOverlappingPairCache*, %struct.btOverlapFilterCallback*)** %vtable, i64 11
  %18 = load void (%class.btOverlappingPairCache*, %struct.btOverlapFilterCallback*)*, void (%class.btOverlappingPairCache*, %struct.btOverlapFilterCallback*)** %vfn, align 4
  call void %18(%class.btOverlappingPairCache* %15, %struct.btOverlapFilterCallback* %16)
  %19 = bitcast i8** %mem9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %retval, align 4
  ret %class.btMultiSapBroadphase* %20
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  %0 = bitcast %class.btBroadphaseInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV21btBroadphaseInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btBroadphaseInterface* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP21btBroadphaseInterfaceLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEEC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

declare %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheC1Ev(%class.btSortedOverlappingPairCache* returned) unnamed_addr #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: inlinehint nounwind
define internal %struct.btMultiSapOverlapFilterCallback* @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackC2Ev(%struct.btMultiSapOverlapFilterCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btMultiSapOverlapFilterCallback*, align 4
  store %struct.btMultiSapOverlapFilterCallback* %this, %struct.btMultiSapOverlapFilterCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMultiSapOverlapFilterCallback*, %struct.btMultiSapOverlapFilterCallback** %this.addr, align 4
  %0 = bitcast %struct.btMultiSapOverlapFilterCallback* %this1 to %struct.btOverlapFilterCallback*
  %call = call %struct.btOverlapFilterCallback* @_ZN23btOverlapFilterCallbackC2Ev(%struct.btOverlapFilterCallback* %0) #9
  %1 = bitcast %struct.btMultiSapOverlapFilterCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  ret %struct.btMultiSapOverlapFilterCallback* %this1
}

; Function Attrs: nounwind
define hidden %class.btMultiSapBroadphase* @_ZN20btMultiSapBroadphaseD2Ev(%class.btMultiSapBroadphase* returned %this) unnamed_addr #5 {
entry:
  %retval = alloca %class.btMultiSapBroadphase*, align 4
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  store %class.btMultiSapBroadphase* %this1, %class.btMultiSapBroadphase** %retval, align 4
  %0 = bitcast %class.btMultiSapBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btMultiSapBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_ownsPairCache = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsPairCache, align 4, !tbaa !18, !range !21
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_overlappingPairs = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_overlappingPairs, align 4, !tbaa !10
  %3 = bitcast %class.btOverlappingPairCache* %2 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %4 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %4(%class.btOverlappingPairCache* %2) #9
  %m_overlappingPairs2 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_overlappingPairs2, align 4, !tbaa !10
  %6 = bitcast %class.btOverlappingPairCache* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_multiSapProxies = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 8
  %call3 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEED2Ev(%class.btAlignedObjectArray.12* %m_multiSapProxies) #9
  %m_sapBroadphases = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceED2Ev(%class.btAlignedObjectArray* %m_sapBroadphases) #9
  %7 = bitcast %class.btMultiSapBroadphase* %this1 to %class.btBroadphaseInterface*
  %call5 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %7) #9
  %8 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %retval, align 4
  ret %class.btMultiSapBroadphase* %8
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN20btMultiSapBroadphaseD0Ev(%class.btMultiSapBroadphase* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

define hidden void @_ZN20btMultiSapBroadphase9buildTreeERK9btVector3S2_(%class.btMultiSapBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMax) #0 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %bvhAabbMin.addr = alloca %class.btVector3*, align 4
  %bvhAabbMax.addr = alloca %class.btVector3*, align 4
  %nodes = alloca %class.btAlignedObjectArray.4*, align 4
  %i = alloca i32, align 4
  %node = alloca %struct.btQuantizedBvhNode, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %partId = alloca i32, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %bvhAabbMin, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %bvhAabbMax, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %call = call i8* @_ZN14btQuantizedBvhnwEm(i32 172)
  %0 = bitcast i8* %call to %class.btQuantizedBvh*
  %call2 = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhC1Ev(%class.btQuantizedBvh* %0)
  %m_optimizedAabbTree = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  store %class.btQuantizedBvh* %0, %class.btQuantizedBvh** %m_optimizedAabbTree, align 4, !tbaa !17
  %m_optimizedAabbTree3 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  %1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedAabbTree3, align 4, !tbaa !17
  %2 = load %class.btVector3*, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  call void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float 1.000000e+00)
  %4 = bitcast %class.btAlignedObjectArray.4** %nodes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %m_optimizedAabbTree4 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  %5 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedAabbTree4, align 4, !tbaa !17
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN14btQuantizedBvh16getLeafNodeArrayEv(%class.btQuantizedBvh* %5)
  store %class.btAlignedObjectArray.4* %call5, %class.btAlignedObjectArray.4** %nodes, align 4, !tbaa !2
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %m_sapBroadphases = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 1
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP21btBroadphaseInterfaceE4sizeEv(%class.btAlignedObjectArray* %m_sapBroadphases)
  %cmp = icmp slt i32 %7, %call6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = bitcast %struct.btQuantizedBvhNode* %node to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %10 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %11 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %m_sapBroadphases9 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 1
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(4) %class.btBroadphaseInterface** @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceEixEi(%class.btAlignedObjectArray* %m_sapBroadphases9, i32 %12)
  %13 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %call10, align 4, !tbaa !2
  %14 = bitcast %class.btBroadphaseInterface* %13 to void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)**, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)*** %14, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)** %vtable, i64 11
  %15 = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %15(%class.btBroadphaseInterface* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %m_optimizedAabbTree11 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  %16 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedAabbTree11, align 4, !tbaa !17
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %16, i16* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, i32 0)
  %m_optimizedAabbTree12 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  %17 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedAabbTree12, align 4, !tbaa !17
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %17, i16* %arrayidx13, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 1)
  %18 = bitcast i32* %partId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  store i32 0, i32* %partId, align 4, !tbaa !6
  %19 = load i32, i32* %partId, align 4, !tbaa !6
  %shl = shl i32 %19, 21
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %or = or i32 %shl, %20
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 2
  store i32 %or, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !22
  %21 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %nodes, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray.4* %21, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %node)
  %22 = bitcast i32* %partId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #9
  %24 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #9
  %25 = bitcast %struct.btQuantizedBvhNode* %node to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_optimizedAabbTree14 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  %27 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedAabbTree14, align 4, !tbaa !17
  call void @_ZN14btQuantizedBvh13buildInternalEv(%class.btQuantizedBvh* %27)
  %28 = bitcast %class.btAlignedObjectArray.4** %nodes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN14btQuantizedBvhnwEm(i32 %sizeInBytes) #7 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4, !tbaa !24
  %0 = load i32, i32* %sizeInBytes.addr, align 4, !tbaa !24
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

declare %class.btQuantizedBvh* @_ZN14btQuantizedBvhC1Ev(%class.btQuantizedBvh* returned) unnamed_addr #3

declare void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float) #3

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN14btQuantizedBvh16getLeafNodeArrayEv(%class.btQuantizedBvh* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  ret %class.btAlignedObjectArray.4* %m_quantizedLeafNodes
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP21btBroadphaseInterfaceE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !26
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btBroadphaseInterface** @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface**, %class.btBroadphaseInterface*** %m_data, align 4, !tbaa !27
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %0, i32 %1
  ret %class.btBroadphaseInterface** %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #7 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %out.addr = alloca i16*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i16* %out, i16** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store i32 %isMax, i32* %isMax.addr, align 4, !tbaa !6
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #9
  %4 = load i32, i32* %isMax.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %5 = load float, float* %call, align 4, !tbaa !28
  %add = fadd float %5, 1.000000e+00
  %conv = fptoui float %add to i16
  %conv2 = zext i16 %conv to i32
  %or = or i32 %conv2, 1
  %conv3 = trunc i32 %or to i16
  %6 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 0
  store i16 %conv3, i16* %arrayidx, align 2, !tbaa !30
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %7 = load float, float* %call4, align 4, !tbaa !28
  %add5 = fadd float %7, 1.000000e+00
  %conv6 = fptoui float %add5 to i16
  %conv7 = zext i16 %conv6 to i32
  %or8 = or i32 %conv7, 1
  %conv9 = trunc i32 %or8 to i16
  %8 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %8, i32 1
  store i16 %conv9, i16* %arrayidx10, align 2, !tbaa !30
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %9 = load float, float* %call11, align 4, !tbaa !28
  %add12 = fadd float %9, 1.000000e+00
  %conv13 = fptoui float %add12 to i16
  %conv14 = zext i16 %conv13 to i32
  %or15 = or i32 %conv14, 1
  %conv16 = trunc i32 %or15 to i16
  %10 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i16, i16* %10, i32 2
  store i16 %conv16, i16* %arrayidx17, align 2, !tbaa !30
  br label %if.end

if.else:                                          ; preds = %entry
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %11 = load float, float* %call18, align 4, !tbaa !28
  %conv19 = fptoui float %11 to i16
  %conv20 = zext i16 %conv19 to i32
  %and = and i32 %conv20, 65534
  %conv21 = trunc i32 %and to i16
  %12 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i16, i16* %12, i32 0
  store i16 %conv21, i16* %arrayidx22, align 2, !tbaa !30
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %13 = load float, float* %call23, align 4, !tbaa !28
  %conv24 = fptoui float %13 to i16
  %conv25 = zext i16 %conv24 to i32
  %and26 = and i32 %conv25, 65534
  %conv27 = trunc i32 %and26 to i16
  %14 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i16, i16* %14, i32 1
  store i16 %conv27, i16* %arrayidx28, align 2, !tbaa !30
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %15 = load float, float* %call29, align 4, !tbaa !28
  %conv30 = fptoui float %15 to i16
  %conv31 = zext i16 %conv30 to i32
  %and32 = and i32 %conv31, 65534
  %conv33 = trunc i32 %and32 to i16
  %16 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i16, i16* %16, i32 2
  store i16 %conv33, i16* %arrayidx34, align 2, !tbaa !30
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray.4* %this, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %_Val, %struct.btQuantizedBvhNode** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !35
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %2, i32 %3
  %4 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call5 = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %struct.btQuantizedBvhNode*
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btQuantizedBvhNode* %5 to i8*
  %8 = bitcast %struct.btQuantizedBvhNode* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !36
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !35
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !35
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

declare void @_ZN14btQuantizedBvh13buildInternalEv(%class.btQuantizedBvh*) #3

define hidden %struct.btBroadphaseProxy* @_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_(%class.btMultiSapBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, %class.btDispatcher* %dispatcher, i8* %0) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %.addr = alloca i8*, align 4
  %mem = alloca i8*, align 4
  %proxy = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i32 %shapeType, i32* %shapeType.addr, align 4, !tbaa !6
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %1 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 104, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %2 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %proxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load i8*, i8** %mem, align 4, !tbaa !2
  %call2 = call i8* @_ZN17btBroadphaseProxynwEmPv(i32 104, i8* %3)
  %4 = bitcast i8* %call2 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %7 = load i32, i32* %shapeType.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %9 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  %10 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  %call3 = call %"struct.btMultiSapBroadphase::btMultiSapProxy"* @_ZN20btMultiSapBroadphase15btMultiSapProxyC2ERK9btVector3S3_iPvss(%"struct.btMultiSapBroadphase::btMultiSapProxy"* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, i32 %7, i8* %8, i16 signext %9, i16 signext %10)
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %4, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %proxy, align 4, !tbaa !2
  %m_multiSapProxies = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 8
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE9push_backERKS2_(%class.btAlignedObjectArray.12* %m_multiSapProxies, %"struct.btMultiSapBroadphase::btMultiSapProxy"** nonnull align 4 dereferenceable(4) %proxy)
  %11 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %proxy, align 4, !tbaa !2
  %12 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %11 to %struct.btBroadphaseProxy*
  %13 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %15 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %16 = bitcast %class.btMultiSapBroadphase* %this1 to void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable = load void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %16, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable, i64 4
  %17 = load void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btMultiSapBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn, align 4
  call void %17(%class.btMultiSapBroadphase* %this1, %struct.btBroadphaseProxy* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btDispatcher* %15)
  %18 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %proxy, align 4, !tbaa !2
  %19 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %18 to %struct.btBroadphaseProxy*
  %20 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %proxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  ret %struct.btBroadphaseProxy* %19
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN17btBroadphaseProxynwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !24
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

define linkonce_odr hidden %"struct.btMultiSapBroadphase::btMultiSapProxy"* @_ZN20btMultiSapBroadphase15btMultiSapProxyC2ERK9btVector3S3_iPvss(%"struct.btMultiSapBroadphase::btMultiSapProxy"* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i32 %shapeType, i32* %shapeType.addr, align 4, !tbaa !6
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  %this1 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %this.addr, align 4
  %0 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1 to %struct.btBroadphaseProxy*
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %4 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  %5 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_PvssS3_(%struct.btBroadphaseProxy* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i8* %3, i16 signext %4, i16 signext %5, i8* null)
  %m_bridgeProxies = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.15* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEC2Ev(%class.btAlignedObjectArray.15* %m_bridgeProxies)
  %m_aabbMin = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1, i32 0, i32 2
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %m_aabbMin to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !38
  %m_aabbMax = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1, i32 0, i32 3
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %m_aabbMax to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !38
  %m_shapeType = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1, i32 0, i32 4
  %12 = load i32, i32* %shapeType.addr, align 4, !tbaa !6
  store i32 %12, i32* %m_shapeType, align 4, !tbaa !39
  %13 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1 to i8*
  %14 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1 to %struct.btBroadphaseProxy*
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %14, i32 0, i32 3
  store i8* %13, i8** %m_multiSapParentProxy, align 4, !tbaa !44
  ret %"struct.btMultiSapBroadphase::btMultiSapProxy"* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE9push_backERKS2_(%class.btAlignedObjectArray.12* %this, %"struct.btMultiSapBroadphase::btMultiSapProxy"** nonnull align 4 dereferenceable(4) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"** %_Val, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data, align 4, !tbaa !46
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !47
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %2, i32 %3
  %4 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %arrayidx to i8*
  %5 = bitcast i8* %4 to %"struct.btMultiSapBroadphase::btMultiSapProxy"**
  %6 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %_Val.addr, align 4, !tbaa !2
  %7 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %6, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %7, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !47
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !47
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%class.btMultiSapBroadphase* %this, %struct.btBroadphaseProxy* %0, %class.btDispatcher* %1) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4, !tbaa !2
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  ret void
}

define hidden void @_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface(%class.btMultiSapBroadphase* %this, %"struct.btMultiSapBroadphase::btMultiSapProxy"* %parentMultiSapProxy, %struct.btBroadphaseProxy* %childProxy, %class.btBroadphaseInterface* %childBroadphase) #0 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %parentMultiSapProxy.addr = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %childProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %childBroadphase.addr = alloca %class.btBroadphaseInterface*, align 4
  %mem = alloca i8*, align 4
  %bridgeProxyRef = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %parentMultiSapProxy, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %parentMultiSapProxy.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %childProxy, %struct.btBroadphaseProxy** %childProxy.addr, align 4, !tbaa !2
  store %class.btBroadphaseInterface* %childBroadphase, %class.btBroadphaseInterface** %childBroadphase.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %1 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i8*, i8** %mem, align 4, !tbaa !2
  %3 = bitcast i8* %2 to %"struct.btMultiSapBroadphase::btBridgeProxy"*
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %3, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %childProxy.addr, align 4, !tbaa !2
  %5 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef, align 4, !tbaa !2
  %m_childProxy = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %5, i32 0, i32 0
  store %struct.btBroadphaseProxy* %4, %struct.btBroadphaseProxy** %m_childProxy, align 4, !tbaa !48
  %6 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %childBroadphase.addr, align 4, !tbaa !2
  %7 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef, align 4, !tbaa !2
  %m_childBroadphase = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %7, i32 0, i32 1
  store %class.btBroadphaseInterface* %6, %class.btBroadphaseInterface** %m_childBroadphase, align 4, !tbaa !50
  %8 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %parentMultiSapProxy.addr, align 4, !tbaa !2
  %m_bridgeProxies = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %8, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE9push_backERKS2_(%class.btAlignedObjectArray.15* %m_bridgeProxies, %"struct.btMultiSapBroadphase::btBridgeProxy"** nonnull align 4 dereferenceable(4) %bridgeProxyRef)
  %9 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE9push_backERKS2_(%class.btAlignedObjectArray.15* %this, %"struct.btMultiSapBroadphase::btBridgeProxy"** nonnull align 4 dereferenceable(4) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %_Val.addr = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btBridgeProxy"** %_Val, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8capacityEv(%class.btAlignedObjectArray.15* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE9allocSizeEi(%class.btAlignedObjectArray.15* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE7reserveEi(%class.btAlignedObjectArray.15* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %2 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !52
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %2, i32 %3
  %4 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx to i8*
  %5 = bitcast i8* %4 to %"struct.btMultiSapBroadphase::btBridgeProxy"**
  %6 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %_Val.addr, align 4, !tbaa !2
  %7 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %6, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %7, %"struct.btMultiSapBroadphase::btBridgeProxy"** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !52
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !52
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

define hidden zeroext i1 @_Z23boxIsContainedWithinBoxRK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %amin, %class.btVector3* nonnull align 4 dereferenceable(16) %amax, %class.btVector3* nonnull align 4 dereferenceable(16) %bmin, %class.btVector3* nonnull align 4 dereferenceable(16) %bmax) #0 {
entry:
  %amin.addr = alloca %class.btVector3*, align 4
  %amax.addr = alloca %class.btVector3*, align 4
  %bmin.addr = alloca %class.btVector3*, align 4
  %bmax.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %amin, %class.btVector3** %amin.addr, align 4, !tbaa !2
  store %class.btVector3* %amax, %class.btVector3** %amax.addr, align 4, !tbaa !2
  store %class.btVector3* %bmin, %class.btVector3** %bmin.addr, align 4, !tbaa !2
  store %class.btVector3* %bmax, %class.btVector3** %bmax.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %amin.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !28
  %2 = load %class.btVector3*, %class.btVector3** %bmin.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !28
  %cmp = fcmp oge float %1, %3
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %amax.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !28
  %6 = load %class.btVector3*, %class.btVector3** %bmax.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4, !tbaa !28
  %cmp4 = fcmp ole float %5, %7
  br i1 %cmp4, label %land.lhs.true5, label %land.end

land.lhs.true5:                                   ; preds = %land.lhs.true
  %8 = load %class.btVector3*, %class.btVector3** %amin.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4, !tbaa !28
  %10 = load %class.btVector3*, %class.btVector3** %bmin.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %10)
  %11 = load float, float* %call7, align 4, !tbaa !28
  %cmp8 = fcmp oge float %9, %11
  br i1 %cmp8, label %land.lhs.true9, label %land.end

land.lhs.true9:                                   ; preds = %land.lhs.true5
  %12 = load %class.btVector3*, %class.btVector3** %amax.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %12)
  %13 = load float, float* %call10, align 4, !tbaa !28
  %14 = load %class.btVector3*, %class.btVector3** %bmax.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %14)
  %15 = load float, float* %call11, align 4, !tbaa !28
  %cmp12 = fcmp ole float %13, %15
  br i1 %cmp12, label %land.lhs.true13, label %land.end

land.lhs.true13:                                  ; preds = %land.lhs.true9
  %16 = load %class.btVector3*, %class.btVector3** %amin.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %16)
  %17 = load float, float* %call14, align 4, !tbaa !28
  %18 = load %class.btVector3*, %class.btVector3** %bmin.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %18)
  %19 = load float, float* %call15, align 4, !tbaa !28
  %cmp16 = fcmp oge float %17, %19
  br i1 %cmp16, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true13
  %20 = load %class.btVector3*, %class.btVector3** %amax.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %20)
  %21 = load float, float* %call17, align 4, !tbaa !28
  %22 = load %class.btVector3*, %class.btVector3** %bmax.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %22)
  %23 = load float, float* %call18, align 4, !tbaa !28
  %cmp19 = fcmp ole float %21, %23
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true13, %land.lhs.true9, %land.lhs.true5, %land.lhs.true, %entry
  %24 = phi i1 [ false, %land.lhs.true13 ], [ false, %land.lhs.true9 ], [ false, %land.lhs.true5 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp19, %land.rhs ]
  ret i1 %24
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: nounwind
define hidden void @_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_(%class.btMultiSapBroadphase* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %multiProxy = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %0 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %2, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %3 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_aabbMin = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %3, i32 0, i32 2
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !38
  %7 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_aabbMax = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %7, i32 0, i32 3
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !38
  %11 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

define hidden void @_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_(%class.btMultiSapBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %m_multiSapProxies = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 8
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv(%class.btAlignedObjectArray.12* %m_multiSapProxies)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  %4 = bitcast %struct.btBroadphaseRayCallback* %3 to %struct.btBroadphaseAabbCallback*
  %m_multiSapProxies2 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 8
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.btMultiSapBroadphase::btMultiSapProxy"** @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEEixEi(%class.btAlignedObjectArray.12* %m_multiSapProxies2, i32 %5)
  %6 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %call3, align 4, !tbaa !2
  %7 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %6 to %struct.btBroadphaseProxy*
  %8 = bitcast %struct.btBroadphaseAabbCallback* %4 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %8, align 4, !tbaa !8
  %vfn = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %9 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call4 = call zeroext i1 %9(%struct.btBroadphaseAabbCallback* %4, %struct.btBroadphaseProxy* %7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !47
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btMultiSapBroadphase::btMultiSapProxy"** @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data, align 4, !tbaa !46
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %0, i32 %1
  ret %"struct.btMultiSapBroadphase::btMultiSapProxy"** %arrayidx
}

define hidden void @_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher(%class.btMultiSapBroadphase* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %multiProxy = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %myNodeCallback = alloca %struct.MyNodeOverlapCallback, align 4
  %i = alloca i32, align 4
  %worldAabbMin = alloca %class.btVector3, align 4
  %worldAabbMax = alloca %class.btVector3, align 4
  %overlapsBroadphase = alloca i8, align 1
  %bridgeProxy = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"*, align 4
  %childProxy = alloca %struct.btBroadphaseProxy*, align 4
  %bridgeProxyRef = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %0 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %2, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_aabbMin = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %4, i32 0, i32 2
  %5 = bitcast %class.btVector3* %m_aabbMin to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !38
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %8 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_aabbMax = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %8, i32 0, i32 3
  %9 = bitcast %class.btVector3* %m_aabbMax to i8*
  %10 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !38
  %11 = bitcast %struct.MyNodeOverlapCallback* %myNodeCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %12 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %13 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %call = call %struct.MyNodeOverlapCallback* @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackC2EPS_PNS_15btMultiSapProxyES6_(%struct.MyNodeOverlapCallback* %myNodeCallback, %class.btMultiSapBroadphase* %this1, %"struct.btMultiSapBroadphase::btMultiSapProxy"* %12, %class.btDispatcher* %13)
  %m_optimizedAabbTree = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  %14 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedAabbTree, align 4, !tbaa !17
  %tobool = icmp ne %class.btQuantizedBvh* %14, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_optimizedAabbTree2 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 4
  %15 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedAabbTree2, align 4, !tbaa !17
  %16 = bitcast %struct.MyNodeOverlapCallback* %myNodeCallback to %class.btNodeOverlapCallback*
  %17 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %18 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %15, %class.btNodeOverlapCallback* %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %18)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %21, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %m_bridgeProxies)
  %cmp = icmp slt i32 %20, %call3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = bitcast %class.btVector3* %worldAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #9
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %worldAabbMin)
  %23 = bitcast %class.btVector3* %worldAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %worldAabbMax)
  %24 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies6 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %24, i32 0, i32 1
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.btMultiSapBroadphase::btBridgeProxy"** @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEixEi(%class.btAlignedObjectArray.15* %m_bridgeProxies6, i32 %25)
  %26 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %call7, align 4, !tbaa !2
  %m_childBroadphase = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %26, i32 0, i32 1
  %27 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_childBroadphase, align 4, !tbaa !50
  %28 = bitcast %class.btBroadphaseInterface* %27 to void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)**, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)*** %28, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)** %vtable, i64 11
  %29 = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %29(%class.btBroadphaseInterface* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %overlapsBroadphase) #9
  %30 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_aabbMin8 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %30, i32 0, i32 2
  %31 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_aabbMax9 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %31, i32 0, i32 3
  %call10 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax9)
  %frombool = zext i1 %call10 to i8
  store i8 %frombool, i8* %overlapsBroadphase, align 1, !tbaa !53
  %32 = load i8, i8* %overlapsBroadphase, align 1, !tbaa !53, !range !21
  %tobool11 = trunc i8 %32 to i1
  br i1 %tobool11, label %if.end22, label %if.then12

if.then12:                                        ; preds = %for.body
  %33 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %34 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies13 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %34, i32 0, i32 1
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %call14 = call nonnull align 4 dereferenceable(4) %"struct.btMultiSapBroadphase::btBridgeProxy"** @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEixEi(%class.btAlignedObjectArray.15* %m_bridgeProxies13, i32 %35)
  %36 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %call14, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %36, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxy, align 4, !tbaa !2
  %37 = bitcast %struct.btBroadphaseProxy** %childProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxy, align 4, !tbaa !2
  %m_childProxy = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %38, i32 0, i32 0
  %39 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_childProxy, align 4, !tbaa !48
  store %struct.btBroadphaseProxy* %39, %struct.btBroadphaseProxy** %childProxy, align 4, !tbaa !2
  %40 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxy, align 4, !tbaa !2
  %m_childBroadphase15 = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %40, i32 0, i32 1
  %41 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_childBroadphase15, align 4, !tbaa !50
  %42 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %childProxy, align 4, !tbaa !2
  %43 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %44 = bitcast %class.btBroadphaseInterface* %41 to void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable16 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %44, align 4, !tbaa !8
  %vfn17 = getelementptr inbounds void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable16, i64 3
  %45 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn17, align 4
  call void %45(%class.btBroadphaseInterface* %41, %struct.btBroadphaseProxy* %42, %class.btDispatcher* %43)
  %46 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies18 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %46, i32 0, i32 1
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %48 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies19 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %48, i32 0, i32 1
  %call20 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %m_bridgeProxies19)
  %sub = sub nsw i32 %call20, 1
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4swapEii(%class.btAlignedObjectArray.15* %m_bridgeProxies18, i32 %47, i32 %sub)
  %49 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies21 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %49, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8pop_backEv(%class.btAlignedObjectArray.15* %m_bridgeProxies21)
  %50 = bitcast %struct.btBroadphaseProxy** %childProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #9
  %51 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #9
  br label %if.end22

if.end22:                                         ; preds = %if.then12, %for.body
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %overlapsBroadphase) #9
  %52 = bitcast %class.btVector3* %worldAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #9
  %53 = bitcast %class.btVector3* %worldAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end22
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc34, %for.end
  %55 = load i32, i32* %i, align 4, !tbaa !6
  %56 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies24 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %56, i32 0, i32 1
  %call25 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %m_bridgeProxies24)
  %cmp26 = icmp slt i32 %55, %call25
  br i1 %cmp26, label %for.body27, label %for.end36

for.body27:                                       ; preds = %for.cond23
  %57 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #9
  %58 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy, align 4, !tbaa !2
  %m_bridgeProxies28 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %58, i32 0, i32 1
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %call29 = call nonnull align 4 dereferenceable(4) %"struct.btMultiSapBroadphase::btBridgeProxy"** @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEixEi(%class.btAlignedObjectArray.15* %m_bridgeProxies28, i32 %59)
  %60 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %call29, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %60, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef, align 4, !tbaa !2
  %61 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef, align 4, !tbaa !2
  %m_childBroadphase30 = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %61, i32 0, i32 1
  %62 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_childBroadphase30, align 4, !tbaa !50
  %63 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef, align 4, !tbaa !2
  %m_childProxy31 = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %63, i32 0, i32 0
  %64 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_childProxy31, align 4, !tbaa !48
  %65 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %66 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %67 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %68 = bitcast %class.btBroadphaseInterface* %62 to void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable32 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %68, align 4, !tbaa !8
  %vfn33 = getelementptr inbounds void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable32, i64 4
  %69 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn33, align 4
  call void %69(%class.btBroadphaseInterface* %62, %struct.btBroadphaseProxy* %64, %class.btVector3* nonnull align 4 dereferenceable(16) %65, %class.btVector3* nonnull align 4 dereferenceable(16) %66, %class.btDispatcher* %67)
  %70 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %bridgeProxyRef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  br label %for.inc34

for.inc34:                                        ; preds = %for.body27
  %71 = load i32, i32* %i, align 4, !tbaa !6
  %inc35 = add nsw i32 %71, 1
  store i32 %inc35, i32* %i, align 4, !tbaa !6
  br label %for.cond23

for.end36:                                        ; preds = %for.cond23
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #9
  %call37 = call %struct.MyNodeOverlapCallback* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback* (%struct.MyNodeOverlapCallback*)*)(%struct.MyNodeOverlapCallback* %myNodeCallback) #9
  %73 = bitcast %struct.MyNodeOverlapCallback* %myNodeCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #9
  %74 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #9
  ret void
}

; Function Attrs: nounwind
define internal %struct.MyNodeOverlapCallback* @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackC2EPS_PNS_15btMultiSapProxyES6_(%struct.MyNodeOverlapCallback* returned %this, %class.btMultiSapBroadphase* %multiSap, %"struct.btMultiSapBroadphase::btMultiSapProxy"* %multiProxy, %class.btDispatcher* %dispatcher) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  %multiSap.addr = alloca %class.btMultiSapBroadphase*, align 4
  %multiProxy.addr = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  store %class.btMultiSapBroadphase* %multiSap, %class.btMultiSapBroadphase** %multiSap.addr, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %multiProxy, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* %0) #9
  %1 = bitcast %struct.MyNodeOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_multiSap = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %2 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %multiSap.addr, align 4, !tbaa !2
  store %class.btMultiSapBroadphase* %2, %class.btMultiSapBroadphase** %m_multiSap, align 4, !tbaa !54
  %m_multiProxy = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %3 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiProxy.addr, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %3, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy, align 4, !tbaa !56
  %m_dispatcher = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 3
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !57
  ret %struct.MyNodeOverlapCallback* %this1
}

declare void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh*, %class.btNodeOverlapCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !52
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btMultiSapBroadphase::btBridgeProxy"** @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEixEi(%class.btAlignedObjectArray.15* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %0 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %0, i32 %1
  ret %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #1 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %overlap) #9
  store i8 1, i8* %overlap, align 1, !tbaa !53
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !28
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !28
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !28
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4, !tbaa !28
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1, !tbaa !53, !range !21
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1, !tbaa !53
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4, !tbaa !28
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4, !tbaa !28
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4, !tbaa !28
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4, !tbaa !28
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1, !tbaa !53, !range !21
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1, !tbaa !53
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4, !tbaa !28
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4, !tbaa !28
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4, !tbaa !28
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4, !tbaa !28
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1, !tbaa !53, !range !21
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1, !tbaa !53
  %27 = load i8, i8* %overlap, align 1, !tbaa !53, !range !21
  %tobool31 = trunc i8 %27 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %overlap) #9
  ret i1 %tobool31
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4swapEii(%class.btAlignedObjectArray.15* %this, i32 %index0, i32 %index1) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !6
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %1 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %1, i32 %2
  %3 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %3, %"struct.btMultiSapBroadphase::btBridgeProxy"** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %4 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data2, align 4, !tbaa !51
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %4, i32 %5
  %6 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %7 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data4, align 4, !tbaa !51
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %7, i32 %8
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %6, %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx5, align 4, !tbaa !2
  %9 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %10 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data6, align 4, !tbaa !51
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %10, i32 %11
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %9, %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8pop_backEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !52
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !52
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %1 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !52
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %1, i32 %2
  ret void
}

define hidden void @_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher(%class.btMultiSapBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray.20*, align 4
  %ref.tmp = alloca %class.btMultiSapBroadphasePairSortPredicate, align 1
  %ref.tmp12 = alloca %struct.btBroadphasePair, align 4
  %i = alloca i32, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %aProxy0 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %aProxy1 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %bProxy0 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %bProxy1 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %hasOverlap = alloca i8, align 1
  %ref.tmp69 = alloca %class.btMultiSapBroadphasePairSortPredicate, align 1
  %ref.tmp73 = alloca %struct.btBroadphasePair, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %0 = load i8, i8* @stopUpdating, align 1, !tbaa !53, !range !21
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.end76, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %1 = bitcast %class.btMultiSapBroadphase* %this1 to %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)**, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*** %1, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)** %vtable, i64 9
  %2 = load %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %2(%class.btMultiSapBroadphase* %this1)
  %3 = bitcast %class.btOverlappingPairCache* %call to i1 (%class.btOverlappingPairCache*)***
  %vtable2 = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %3, align 4, !tbaa !8
  %vfn3 = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable2, i64 14
  %4 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn3, align 4
  %call4 = call zeroext i1 %4(%class.btOverlappingPairCache* %call)
  br i1 %call4, label %if.then, label %if.end76

if.then:                                          ; preds = %land.lhs.true
  %5 = bitcast %class.btAlignedObjectArray.20** %overlappingPairArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = bitcast %class.btMultiSapBroadphase* %this1 to %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)***
  %vtable5 = load %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)**, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*** %6, align 4, !tbaa !8
  %vfn6 = getelementptr inbounds %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)** %vtable5, i64 9
  %7 = load %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)** %vfn6, align 4
  %call7 = call %class.btOverlappingPairCache* %7(%class.btMultiSapBroadphase* %this1)
  %8 = bitcast %class.btOverlappingPairCache* %call7 to %class.btAlignedObjectArray.20* (%class.btOverlappingPairCache*)***
  %vtable8 = load %class.btAlignedObjectArray.20* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray.20* (%class.btOverlappingPairCache*)*** %8, align 4, !tbaa !8
  %vfn9 = getelementptr inbounds %class.btAlignedObjectArray.20* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.20* (%class.btOverlappingPairCache*)** %vtable8, i64 7
  %9 = load %class.btAlignedObjectArray.20* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.20* (%class.btOverlappingPairCache*)** %vfn9, align 4
  %call10 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.20* %9(%class.btOverlappingPairCache* %call7)
  store %class.btAlignedObjectArray.20* %call10, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %10 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %11 = bitcast %class.btMultiSapBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %11) #9
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI37btMultiSapBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.20* %10, %class.btMultiSapBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %12 = bitcast %class.btMultiSapBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %12) #9
  %13 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %14 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %14)
  %m_invalidPair = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 7
  %15 = load i32, i32* %m_invalidPair, align 4, !tbaa !19
  %sub = sub nsw i32 %call11, %15
  %16 = bitcast %struct.btBroadphasePair* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #9
  %call13 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp12)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.20* %13, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %17 = bitcast %struct.btBroadphasePair* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  %m_invalidPair14 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 7
  store i32 0, i32* %m_invalidPair14, align 4, !tbaa !19
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #9
  %call15 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !58
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !60
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !61
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %call16 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %21)
  %cmp = icmp slt i32 %20, %call16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %call17 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.20* %23, i32 %24)
  store %struct.btBroadphasePair* %call17, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %25 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy018 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %26, i32 0, i32 0
  %27 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy018, align 4, !tbaa !58
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %27, null
  br i1 %tobool19, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy020 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy020, align 4, !tbaa !58
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %29, i32 0, i32 3
  %30 = load i8*, i8** %m_multiSapParentProxy, align 4, !tbaa !44
  %31 = bitcast i8* %30 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %31, %cond.true ], [ null, %cond.false ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0, align 4, !tbaa !2
  %32 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  %33 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %33, i32 0, i32 1
  %34 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4, !tbaa !60
  %tobool22 = icmp ne %struct.btBroadphaseProxy* %34, null
  br i1 %tobool22, label %cond.true23, label %cond.false26

cond.true23:                                      ; preds = %cond.end
  %35 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy124 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %35, i32 0, i32 1
  %36 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy124, align 4, !tbaa !60
  %m_multiSapParentProxy25 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %36, i32 0, i32 3
  %37 = load i8*, i8** %m_multiSapParentProxy25, align 4, !tbaa !44
  %38 = bitcast i8* %37 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end27

cond.false26:                                     ; preds = %cond.end
  br label %cond.end27

cond.end27:                                       ; preds = %cond.false26, %cond.true23
  %cond28 = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %38, %cond.true23 ], [ null, %cond.false26 ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond28, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1, align 4, !tbaa !2
  %39 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %m_pProxy029 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  %40 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy029, align 4, !tbaa !58
  %tobool30 = icmp ne %struct.btBroadphaseProxy* %40, null
  br i1 %tobool30, label %cond.true31, label %cond.false34

cond.true31:                                      ; preds = %cond.end27
  %m_pProxy032 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  %41 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy032, align 4, !tbaa !58
  %m_multiSapParentProxy33 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %41, i32 0, i32 3
  %42 = load i8*, i8** %m_multiSapParentProxy33, align 4, !tbaa !44
  %43 = bitcast i8* %42 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end35

cond.false34:                                     ; preds = %cond.end27
  br label %cond.end35

cond.end35:                                       ; preds = %cond.false34, %cond.true31
  %cond36 = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %43, %cond.true31 ], [ null, %cond.false34 ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond36, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0, align 4, !tbaa !2
  %44 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  %m_pProxy137 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  %45 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy137, align 4, !tbaa !60
  %tobool38 = icmp ne %struct.btBroadphaseProxy* %45, null
  br i1 %tobool38, label %cond.true39, label %cond.false42

cond.true39:                                      ; preds = %cond.end35
  %m_pProxy140 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  %46 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy140, align 4, !tbaa !60
  %m_multiSapParentProxy41 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %46, i32 0, i32 3
  %47 = load i8*, i8** %m_multiSapParentProxy41, align 4, !tbaa !44
  %48 = bitcast i8* %47 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end43

cond.false42:                                     ; preds = %cond.end35
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false42, %cond.true39
  %cond44 = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %48, %cond.true39 ], [ null, %cond.false42 ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond44, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isDuplicate) #9
  %49 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0, align 4, !tbaa !2
  %50 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0, align 4, !tbaa !2
  %cmp45 = icmp eq %"struct.btMultiSapBroadphase::btMultiSapProxy"* %49, %50
  br i1 %cmp45, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %cond.end43
  %51 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1, align 4, !tbaa !2
  %52 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1, align 4, !tbaa !2
  %cmp46 = icmp eq %"struct.btMultiSapBroadphase::btMultiSapProxy"* %51, %52
  br label %land.end

land.end:                                         ; preds = %land.rhs, %cond.end43
  %53 = phi i1 [ false, %cond.end43 ], [ %cmp46, %land.rhs ]
  %frombool = zext i1 %53 to i8
  store i8 %frombool, i8* %isDuplicate, align 1, !tbaa !53
  %54 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %55 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %56 = bitcast %struct.btBroadphasePair* %54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false), !tbaa.struct !62
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %needsRemoval) #9
  store i8 0, i8* %needsRemoval, align 1, !tbaa !53
  %57 = load i8, i8* %isDuplicate, align 1, !tbaa !53, !range !21
  %tobool47 = trunc i8 %57 to i1
  br i1 %tobool47, label %if.else55, label %if.then48

if.then48:                                        ; preds = %land.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasOverlap) #9
  %58 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy049 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %58, i32 0, i32 0
  %59 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy049, align 4, !tbaa !58
  %60 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy150 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %60, i32 0, i32 1
  %61 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy150, align 4, !tbaa !60
  %call51 = call zeroext i1 @_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_(%class.btMultiSapBroadphase* %this1, %struct.btBroadphaseProxy* %59, %struct.btBroadphaseProxy* %61)
  %frombool52 = zext i1 %call51 to i8
  store i8 %frombool52, i8* %hasOverlap, align 1, !tbaa !53
  %62 = load i8, i8* %hasOverlap, align 1, !tbaa !53, !range !21
  %tobool53 = trunc i8 %62 to i1
  br i1 %tobool53, label %if.then54, label %if.else

if.then54:                                        ; preds = %if.then48
  store i8 0, i8* %needsRemoval, align 1, !tbaa !53
  br label %if.end

if.else:                                          ; preds = %if.then48
  store i8 1, i8* %needsRemoval, align 1, !tbaa !53
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then54
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasOverlap) #9
  br label %if.end56

if.else55:                                        ; preds = %land.end
  store i8 1, i8* %needsRemoval, align 1, !tbaa !53
  br label %if.end56

if.end56:                                         ; preds = %if.else55, %if.end
  %63 = load i8, i8* %needsRemoval, align 1, !tbaa !53, !range !21
  %tobool57 = trunc i8 %63 to i1
  br i1 %tobool57, label %if.then58, label %if.end67

if.then58:                                        ; preds = %if.end56
  %64 = bitcast %class.btMultiSapBroadphase* %this1 to %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)***
  %vtable59 = load %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)**, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*** %64, align 4, !tbaa !8
  %vfn60 = getelementptr inbounds %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)** %vtable59, i64 9
  %65 = load %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)*, %class.btOverlappingPairCache* (%class.btMultiSapBroadphase*)** %vfn60, align 4
  %call61 = call %class.btOverlappingPairCache* %65(%class.btMultiSapBroadphase* %this1)
  %66 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %67 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %68 = bitcast %class.btOverlappingPairCache* %call61 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable62 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %68, align 4, !tbaa !8
  %vfn63 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable62, i64 8
  %69 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn63, align 4
  call void %69(%class.btOverlappingPairCache* %call61, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %66, %class.btDispatcher* %67)
  %70 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy064 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %70, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy064, align 4, !tbaa !58
  %71 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy165 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %71, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy165, align 4, !tbaa !60
  %m_invalidPair66 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 7
  %72 = load i32, i32* %m_invalidPair66, align 4, !tbaa !19
  %inc = add nsw i32 %72, 1
  store i32 %inc, i32* %m_invalidPair66, align 4, !tbaa !19
  %73 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !6
  %dec = add nsw i32 %73, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4, !tbaa !6
  br label %if.end67

if.end67:                                         ; preds = %if.then58, %if.end56
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %needsRemoval) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isDuplicate) #9
  %74 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #9
  %75 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #9
  %76 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #9
  %77 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #9
  %78 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end67
  %79 = load i32, i32* %i, align 4, !tbaa !6
  %inc68 = add nsw i32 %79, 1
  store i32 %inc68, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %80 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %81 = bitcast %class.btMultiSapBroadphasePairSortPredicate* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %81) #9
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI37btMultiSapBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.20* %80, %class.btMultiSapBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp69)
  %82 = bitcast %class.btMultiSapBroadphasePairSortPredicate* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %82) #9
  %83 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %84 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %overlappingPairArray, align 4, !tbaa !2
  %call70 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %84)
  %m_invalidPair71 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 7
  %85 = load i32, i32* %m_invalidPair71, align 4, !tbaa !19
  %sub72 = sub nsw i32 %call70, %85
  %86 = bitcast %struct.btBroadphasePair* %ref.tmp73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %86) #9
  %call74 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp73)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.20* %83, i32 %sub72, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp73)
  %87 = bitcast %struct.btBroadphasePair* %ref.tmp73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #9
  %m_invalidPair75 = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 7
  store i32 0, i32* %m_invalidPair75, align 4, !tbaa !19
  %88 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #9
  %89 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #9
  %90 = bitcast %class.btAlignedObjectArray.20** %overlappingPairArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  br label %if.end76

if.end76:                                         ; preds = %for.end, %land.lhs.true, %entry
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI37btMultiSapBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.20* %this, %class.btMultiSapBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %CompareFunc.addr = alloca %class.btMultiSapBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store %class.btMultiSapBroadphasePairSortPredicate* %CompareFunc, %class.btMultiSapBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btMultiSapBroadphasePairSortPredicate*, %class.btMultiSapBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.20* %this1, %class.btMultiSapBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.20* %this, i32 %newsize, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btBroadphasePair*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %struct.btBroadphasePair* %fillData, %struct.btBroadphasePair** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end18

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.20* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc15, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end17

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %18 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data11, align 4, !tbaa !63
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %18, i32 %19
  %20 = bitcast %struct.btBroadphasePair* %arrayidx12 to i8*
  %call13 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btBroadphasePair*
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %fillData.addr, align 4, !tbaa !2
  %call14 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %21, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %22)
  br label %for.inc15

for.inc15:                                        ; preds = %for.body10
  %23 = load i32, i32* %i6, align 4, !tbaa !6
  %inc16 = add nsw i32 %23, 1
  store i32 %inc16, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end17:                                        ; preds = %for.cond.cleanup9
  br label %if.end18

if.end18:                                         ; preds = %for.end17, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !66
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !66
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !58
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !60
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !61
  %0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %0 to i8**
  store i8* null, i8** %m_internalInfo1, align 4, !tbaa !37
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.20* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

define hidden zeroext i1 @_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_(%class.btMultiSapBroadphase* %this, %struct.btBroadphaseProxy* %childProxy0, %struct.btBroadphaseProxy* %childProxy1) #0 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %childProxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %childProxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %multiSapProxy0 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %multiSapProxy1 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %childProxy0, %struct.btBroadphaseProxy** %childProxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %childProxy1, %struct.btBroadphaseProxy** %childProxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %0 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %childProxy0.addr, align 4, !tbaa !2
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 3
  %2 = load i8*, i8** %m_multiSapParentProxy, align 4, !tbaa !44
  %3 = bitcast i8* %2 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %3, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy0, align 4, !tbaa !2
  %4 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %childProxy1.addr, align 4, !tbaa !2
  %m_multiSapParentProxy2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 3
  %6 = load i8*, i8** %m_multiSapParentProxy2, align 4, !tbaa !44
  %7 = bitcast i8* %6 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %7, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy1, align 4, !tbaa !2
  %8 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy0, align 4, !tbaa !2
  %m_aabbMin = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %8, i32 0, i32 2
  %9 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy0, align 4, !tbaa !2
  %m_aabbMax = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %9, i32 0, i32 3
  %10 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy1, align 4, !tbaa !2
  %m_aabbMin3 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %10, i32 0, i32 2
  %11 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy1, align 4, !tbaa !2
  %m_aabbMax4 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %11, i32 0, i32 3
  %call = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax4)
  %12 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %multiSapProxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret i1 %call
}

; Function Attrs: nounwind
define hidden void @_ZN20btMultiSapBroadphase10printStatsEv(%class.btMultiSapBroadphase* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher(%class.btMultiSapBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv(%class.btMultiSapBroadphase* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %m_overlappingPairs = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_overlappingPairs, align 4, !tbaa !10
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv(%class.btMultiSapBroadphase* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %m_overlappingPairs = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 3
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_overlappingPairs, align 4, !tbaa !10
  ret %class.btOverlappingPairCache* %0
}

define linkonce_odr hidden void @_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_(%class.btMultiSapBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp, align 4, !tbaa !28
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp2, align 4, !tbaa !28
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp3, align 4, !tbaa !28
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !28
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4, !tbaa !28
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 0x43ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !28
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %7, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btBroadphaseInterfaceD0Ev(%class.btBroadphaseInterface* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher(%class.btBroadphaseInterface* %this, %class.btDispatcher* %dispatcher) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btOverlapFilterCallback* @_ZN23btOverlapFilterCallbackC2Ev(%struct.btOverlapFilterCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %struct.btOverlapFilterCallback* %this, %struct.btOverlapFilterCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %this.addr, align 4
  %0 = bitcast %struct.btOverlapFilterCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btOverlapFilterCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %struct.btOverlapFilterCallback* %this1
}

; Function Attrs: nounwind
define internal void @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev(%struct.btMultiSapOverlapFilterCallback* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.btMultiSapOverlapFilterCallback*, align 4
  store %struct.btMultiSapOverlapFilterCallback* %this, %struct.btMultiSapOverlapFilterCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMultiSapOverlapFilterCallback*, %struct.btMultiSapOverlapFilterCallback** %this.addr, align 4
  %call = call %struct.btMultiSapOverlapFilterCallback* bitcast (%struct.btOverlapFilterCallback* (%struct.btOverlapFilterCallback*)* @_ZN23btOverlapFilterCallbackD2Ev to %struct.btMultiSapOverlapFilterCallback* (%struct.btMultiSapOverlapFilterCallback*)*)(%struct.btMultiSapOverlapFilterCallback* %this1) #9
  %0 = bitcast %struct.btMultiSapOverlapFilterCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nounwind
define internal zeroext i1 @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_(%struct.btMultiSapOverlapFilterCallback* %this, %struct.btBroadphaseProxy* %childProxy0, %struct.btBroadphaseProxy* %childProxy1) unnamed_addr #5 {
entry:
  %this.addr = alloca %struct.btMultiSapOverlapFilterCallback*, align 4
  %childProxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %childProxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %multiProxy0 = alloca %struct.btBroadphaseProxy*, align 4
  %multiProxy1 = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %struct.btMultiSapOverlapFilterCallback* %this, %struct.btMultiSapOverlapFilterCallback** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %childProxy0, %struct.btBroadphaseProxy** %childProxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %childProxy1, %struct.btBroadphaseProxy** %childProxy1.addr, align 4, !tbaa !2
  %this1 = load %struct.btMultiSapOverlapFilterCallback*, %struct.btMultiSapOverlapFilterCallback** %this.addr, align 4
  %0 = bitcast %struct.btBroadphaseProxy** %multiProxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %childProxy0.addr, align 4, !tbaa !2
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 3
  %2 = load i8*, i8** %m_multiSapParentProxy, align 4, !tbaa !44
  %3 = bitcast i8* %2 to %struct.btBroadphaseProxy*
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %multiProxy0, align 4, !tbaa !2
  %4 = bitcast %struct.btBroadphaseProxy** %multiProxy1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %childProxy1.addr, align 4, !tbaa !2
  %m_multiSapParentProxy2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 3
  %6 = load i8*, i8** %m_multiSapParentProxy2, align 4, !tbaa !44
  %7 = bitcast i8* %6 to %struct.btBroadphaseProxy*
  store %struct.btBroadphaseProxy* %7, %struct.btBroadphaseProxy** %multiProxy1, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %collides) #9
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %multiProxy0, align 4, !tbaa !2
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 1
  %9 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !67
  %conv = sext i16 %9 to i32
  %10 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %multiProxy1, align 4, !tbaa !2
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 2
  %11 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !68
  %conv3 = sext i16 %11 to i32
  %and = and i32 %conv, %conv3
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1, !tbaa !53
  %12 = load i8, i8* %collides, align 1, !tbaa !53, !range !21
  %tobool = trunc i8 %12 to i1
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %multiProxy1, align 4, !tbaa !2
  %m_collisionFilterGroup4 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 1
  %14 = load i16, i16* %m_collisionFilterGroup4, align 4, !tbaa !67
  %conv5 = sext i16 %14 to i32
  %15 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %multiProxy0, align 4, !tbaa !2
  %m_collisionFilterMask6 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %15, i32 0, i32 2
  %16 = load i16, i16* %m_collisionFilterMask6, align 2, !tbaa !68
  %conv7 = sext i16 %16 to i32
  %and8 = and i32 %conv5, %conv7
  %tobool9 = icmp ne i32 %and8, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %17 = phi i1 [ false, %entry ], [ %tobool9, %land.rhs ]
  %frombool10 = zext i1 %17 to i8
  store i8 %frombool10, i8* %collides, align 1, !tbaa !53
  %18 = load i8, i8* %collides, align 1, !tbaa !53, !range !21
  %tobool11 = trunc i8 %18 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %collides) #9
  %19 = bitcast %struct.btBroadphaseProxy** %multiProxy1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast %struct.btBroadphaseProxy** %multiProxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  ret i1 %tobool11
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btOverlapFilterCallback* @_ZN23btOverlapFilterCallbackD2Ev(%struct.btOverlapFilterCallback* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %struct.btOverlapFilterCallback* %this, %struct.btOverlapFilterCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %this.addr, align 4
  ret %struct.btOverlapFilterCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN23btOverlapFilterCallbackD0Ev(%struct.btOverlapFilterCallback* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %struct.btOverlapFilterCallback* %this, %struct.btOverlapFilterCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !28
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !28
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !28
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !28
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !28
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !28
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !28
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !28
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !28
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !28
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !28
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !28
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !28
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !28
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !28
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !28
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !28
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !28
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !28
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !28
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !28
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !28
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !28
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !28
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !28
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_PvssS3_(%struct.btBroadphaseProxy* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, i8* %multiSapParentProxy) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %multiSapParentProxy.addr = alloca i8*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  store i8* %multiSapParentProxy, i8** %multiSapParentProxy.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  %0 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  store i8* %0, i8** %m_clientObject, align 4, !tbaa !69
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 1
  %1 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !30
  store i16 %1, i16* %m_collisionFilterGroup, align 4, !tbaa !67
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 2
  %2 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !30
  store i16 %2, i16* %m_collisionFilterMask, align 2, !tbaa !68
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %m_aabbMin to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !38
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 6
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %m_aabbMax to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !38
  %9 = load i8*, i8** %multiSapParentProxy.addr, align 4, !tbaa !2
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 3
  store i8* %9, i8** %m_multiSapParentProxy, align 4, !tbaa !44
  ret %struct.btBroadphaseProxy* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.15* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEC2Ev(%class.btAlignedObjectArray.15* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.16* @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EEC2Ev(%class.btAlignedAllocator.16* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4initEv(%class.btAlignedObjectArray.15* %this1)
  ret %class.btAlignedObjectArray.15* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.16* @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EEC2Ev(%class.btAlignedAllocator.16* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.16*, align 4
  store %class.btAlignedAllocator.16* %this, %class.btAlignedAllocator.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.16*, %class.btAlignedAllocator.16** %this.addr, align 4
  ret %class.btAlignedAllocator.16* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4initEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !70
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  store %"struct.btMultiSapBroadphase::btBridgeProxy"** null, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !52
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !71
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast %class.btNodeOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV21btNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btNodeOverlapCallback* %this1
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev(%struct.MyNodeOverlapCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %call = call %struct.MyNodeOverlapCallback* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to %struct.MyNodeOverlapCallback* (%struct.MyNodeOverlapCallback*)*)(%struct.MyNodeOverlapCallback* %this1) #9
  %0 = bitcast %struct.MyNodeOverlapCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

define internal void @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii(%struct.MyNodeOverlapCallback* %this, i32 %0, i32 %broadphaseIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  %.addr = alloca i32, align 4
  %broadphaseIndex.addr = alloca i32, align 4
  %childBroadphase = alloca %class.btBroadphaseInterface*, align 4
  %containingBroadphaseIndex = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %childProxy = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !6
  store i32 %broadphaseIndex, i32* %broadphaseIndex.addr, align 4, !tbaa !6
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %1 = bitcast %class.btBroadphaseInterface** %childBroadphase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_multiSap = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %2 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %m_multiSap, align 4, !tbaa !54
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN20btMultiSapBroadphase18getBroadphaseArrayEv(%class.btMultiSapBroadphase* %2)
  %3 = load i32, i32* %broadphaseIndex.addr, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(4) %class.btBroadphaseInterface** @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceEixEi(%class.btAlignedObjectArray* %call, i32 %3)
  %4 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %call2, align 4, !tbaa !2
  store %class.btBroadphaseInterface* %4, %class.btBroadphaseInterface** %childBroadphase, align 4, !tbaa !2
  %5 = bitcast i32* %containingBroadphaseIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store i32 -1, i32* %containingBroadphaseIndex, align 4, !tbaa !6
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %m_multiProxy = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %8 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy, align 4, !tbaa !56
  %m_bridgeProxies = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %8, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %m_bridgeProxies)
  %cmp = icmp slt i32 %7, %call3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %m_multiProxy4 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %9 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy4, align 4, !tbaa !56
  %m_bridgeProxies5 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %9, i32 0, i32 1
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(4) %"struct.btMultiSapBroadphase::btBridgeProxy"** @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEEixEi(%class.btAlignedObjectArray.15* %m_bridgeProxies5, i32 %10)
  %11 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %call6, align 4, !tbaa !2
  %m_childBroadphase = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy", %"struct.btMultiSapBroadphase::btBridgeProxy"* %11, i32 0, i32 1
  %12 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_childBroadphase, align 4, !tbaa !50
  %13 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %childBroadphase, align 4, !tbaa !2
  %cmp7 = icmp eq %class.btBroadphaseInterface* %12, %13
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %14, i32* %containingBroadphaseIndex, align 4, !tbaa !6
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  br label %for.end

for.end:                                          ; preds = %cleanup
  %17 = load i32, i32* %containingBroadphaseIndex, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %17, 0
  br i1 %cmp8, label %if.then9, label %if.end20

if.then9:                                         ; preds = %for.end
  %18 = bitcast %struct.btBroadphaseProxy** %childProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %childBroadphase, align 4, !tbaa !2
  %m_multiProxy10 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %20 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy10, align 4, !tbaa !56
  %m_aabbMin = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %20, i32 0, i32 2
  %m_multiProxy11 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %21 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy11, align 4, !tbaa !56
  %m_aabbMax = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %21, i32 0, i32 3
  %m_multiProxy12 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %22 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy12, align 4, !tbaa !56
  %m_shapeType = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy", %"struct.btMultiSapBroadphase::btMultiSapProxy"* %22, i32 0, i32 4
  %23 = load i32, i32* %m_shapeType, align 4, !tbaa !39
  %m_multiProxy13 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %24 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy13, align 4, !tbaa !56
  %25 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %24 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 0
  %26 = load i8*, i8** %m_clientObject, align 4, !tbaa !69
  %m_multiProxy14 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %27 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy14, align 4, !tbaa !56
  %28 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %27 to %struct.btBroadphaseProxy*
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %28, i32 0, i32 1
  %29 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !67
  %m_multiProxy15 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %30 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy15, align 4, !tbaa !56
  %31 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %30 to %struct.btBroadphaseProxy*
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %31, i32 0, i32 2
  %32 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !68
  %m_dispatcher = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 3
  %33 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !57
  %m_multiProxy16 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %34 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy16, align 4, !tbaa !56
  %35 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"* %34 to i8*
  %36 = bitcast %class.btBroadphaseInterface* %19 to %struct.btBroadphaseProxy* (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)***
  %vtable = load %struct.btBroadphaseProxy* (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)**, %struct.btBroadphaseProxy* (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*** %36, align 4, !tbaa !8
  %vfn = getelementptr inbounds %struct.btBroadphaseProxy* (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*, %struct.btBroadphaseProxy* (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)** %vtable, i64 2
  %37 = load %struct.btBroadphaseProxy* (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*, %struct.btBroadphaseProxy* (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)** %vfn, align 4
  %call17 = call %struct.btBroadphaseProxy* %37(%class.btBroadphaseInterface* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax, i32 %23, i8* %26, i16 signext %29, i16 signext %32, %class.btDispatcher* %33, i8* %35)
  store %struct.btBroadphaseProxy* %call17, %struct.btBroadphaseProxy** %childProxy, align 4, !tbaa !2
  %m_multiSap18 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %38 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %m_multiSap18, align 4, !tbaa !54
  %m_multiProxy19 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %39 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %m_multiProxy19, align 4, !tbaa !56
  %40 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %childProxy, align 4, !tbaa !2
  %41 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %childBroadphase, align 4, !tbaa !2
  call void @_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface(%class.btMultiSapBroadphase* %38, %"struct.btMultiSapBroadphase::btMultiSapProxy"* %39, %struct.btBroadphaseProxy* %40, %class.btBroadphaseInterface* %41)
  %42 = bitcast %struct.btBroadphaseProxy** %childProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  br label %if.end20

if.end20:                                         ; preds = %if.then9, %for.end
  %43 = bitcast i32* %containingBroadphaseIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #9
  %44 = bitcast %class.btBroadphaseInterface** %childBroadphase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackD2Ev(%class.btNodeOverlapCallback* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  ret %class.btNodeOverlapCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btNodeOverlapCallbackD0Ev(%class.btNodeOverlapCallback* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN20btMultiSapBroadphase18getBroadphaseArrayEv(%class.btMultiSapBroadphase* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btMultiSapBroadphase*, align 4
  store %class.btMultiSapBroadphase* %this, %class.btMultiSapBroadphase** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphase*, %class.btMultiSapBroadphase** %this.addr, align 4
  %m_sapBroadphases = getelementptr inbounds %class.btMultiSapBroadphase, %class.btMultiSapBroadphase* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray* %m_sapBroadphases
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !28
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !28
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !28
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !28
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !28
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !28
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !28
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP21btBroadphaseInterfaceLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !72
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btBroadphaseInterface** null, %class.btBroadphaseInterface*** %m_data, align 4, !tbaa !27
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !26
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !73
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !74
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"** null, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data, align 4, !tbaa !46
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !47
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !75
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE5clearEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btBroadphaseInterfaceE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btBroadphaseInterface**, %class.btBroadphaseInterface*** %m_data, align 4, !tbaa !27
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceE10deallocateEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface**, %class.btBroadphaseInterface*** %m_data, align 4, !tbaa !27
  %tobool = icmp ne %class.btBroadphaseInterface** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !72, !range !21
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btBroadphaseInterface**, %class.btBroadphaseInterface*** %m_data4, align 4, !tbaa !27
  call void @_ZN18btAlignedAllocatorIP21btBroadphaseInterfaceLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btBroadphaseInterface** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btBroadphaseInterface** null, %class.btBroadphaseInterface*** %m_data5, align 4, !tbaa !27
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP21btBroadphaseInterfaceLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btBroadphaseInterface** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btBroadphaseInterface**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btBroadphaseInterface** %ptr, %class.btBroadphaseInterface*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btBroadphaseInterface**, %class.btBroadphaseInterface*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btBroadphaseInterface** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE5clearEv(%class.btAlignedObjectArray.12* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %4 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data, align 4, !tbaa !46
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE10deallocateEv(%class.btAlignedObjectArray.12* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data, align 4, !tbaa !46
  %tobool = icmp ne %"struct.btMultiSapBroadphase::btMultiSapProxy"** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !74, !range !21
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data4, align 4, !tbaa !46
  call void @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EE10deallocateEPS2_(%class.btAlignedAllocator.13* %m_allocator, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"** null, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data5, align 4, !tbaa !46
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EE10deallocateEPS2_(%class.btAlignedAllocator.13* %this, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"** %ptr, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !35
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !76
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btQuantizedBvhNode** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btQuantizedBvhNode*
  store %struct.btQuantizedBvhNode* %3, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %struct.btQuantizedBvhNode* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !77
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* %5, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !76
  %7 = bitcast %struct.btQuantizedBvhNode** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !24
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %struct.btQuantizedBvhNode** null)
  %2 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %struct.btQuantizedBvhNode* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btQuantizedBvhNode* %dest, %struct.btQuantizedBvhNode** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 %5
  %6 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 %9
  %10 = bitcast %struct.btQuantizedBvhNode* %7 to i8*
  %11 = bitcast %struct.btQuantizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !36
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.4* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %tobool = icmp ne %struct.btQuantizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !77, !range !21
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data4, align 4, !tbaa !32
  call void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %m_allocator, %struct.btQuantizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data5, align 4, !tbaa !32
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %this, i32 %n, %struct.btQuantizedBvhNode** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btQuantizedBvhNode**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btQuantizedBvhNode** %hint, %struct.btQuantizedBvhNode*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  ret %struct.btQuantizedBvhNode* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %this, %struct.btQuantizedBvhNode* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %ptr, %struct.btQuantizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btQuantizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !75
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"**, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %"struct.btMultiSapBroadphase::btMultiSapProxy"**
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"** %3, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %4 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4copyEiiPS2_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !74
  %5 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"** %5, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data, align 4, !tbaa !46
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !75
  %7 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %"struct.btMultiSapBroadphase::btMultiSapProxy"** @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** null)
  %2 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4copyEiiPS2_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"** %dest, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %4, i32 %5
  %6 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %arrayidx to i8*
  %7 = bitcast i8* %6 to %"struct.btMultiSapBroadphase::btMultiSapProxy"**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %8 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"**, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %m_data, align 4, !tbaa !46
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %8, i32 %9
  %10 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %arrayidx2, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %10, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define linkonce_odr hidden %"struct.btMultiSapBroadphase::btMultiSapProxy"** @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.13* %this, i32 %n, %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"***, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"*** %hint, %"struct.btMultiSapBroadphase::btMultiSapProxy"**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btMultiSapBroadphase::btMultiSapProxy"**
  ret %"struct.btMultiSapBroadphase::btMultiSapProxy"** %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8capacityEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !71
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE7reserveEi(%class.btAlignedObjectArray.15* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"**, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8capacityEv(%class.btAlignedObjectArray.15* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8allocateEi(%class.btAlignedObjectArray.15* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %"struct.btMultiSapBroadphase::btBridgeProxy"**
  store %"struct.btMultiSapBroadphase::btBridgeProxy"** %3, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  %4 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4copyEiiPS2_(%class.btAlignedObjectArray.15* %this1, i32 0, i32 %call3, %"struct.btMultiSapBroadphase::btBridgeProxy"** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE7destroyEii(%class.btAlignedObjectArray.15* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE10deallocateEv(%class.btAlignedObjectArray.15* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !70
  %5 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  store %"struct.btMultiSapBroadphase::btBridgeProxy"** %5, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !71
  %7 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE9allocSizeEi(%class.btAlignedObjectArray.15* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE8allocateEi(%class.btAlignedObjectArray.15* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %"struct.btMultiSapBroadphase::btBridgeProxy"** @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.16* %m_allocator, i32 %1, %"struct.btMultiSapBroadphase::btBridgeProxy"*** null)
  %2 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4copyEiiPS2_(%class.btAlignedObjectArray.15* %this, i32 %start, i32 %end, %"struct.btMultiSapBroadphase::btBridgeProxy"** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %"struct.btMultiSapBroadphase::btBridgeProxy"** %dest, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %4, i32 %5
  %6 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx to i8*
  %7 = bitcast i8* %6 to %"struct.btMultiSapBroadphase::btBridgeProxy"**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %8 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %8, i32 %9
  %10 = load %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %arrayidx2, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btBridgeProxy"* %10, %"struct.btMultiSapBroadphase::btBridgeProxy"** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE7destroyEii(%class.btAlignedObjectArray.15* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %4 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %"struct.btMultiSapBroadphase::btBridgeProxy"*, %"struct.btMultiSapBroadphase::btBridgeProxy"** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE10deallocateEv(%class.btAlignedObjectArray.15* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %0 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data, align 4, !tbaa !51
  %tobool = icmp ne %"struct.btMultiSapBroadphase::btBridgeProxy"** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !70, !range !21
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %2 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data4, align 4, !tbaa !51
  call void @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EE10deallocateEPS2_(%class.btAlignedAllocator.16* %m_allocator, %"struct.btMultiSapBroadphase::btBridgeProxy"** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  store %"struct.btMultiSapBroadphase::btBridgeProxy"** null, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %m_data5, align 4, !tbaa !51
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %"struct.btMultiSapBroadphase::btBridgeProxy"** @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.16* %this, i32 %n, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.16*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"***, align 4
  store %class.btAlignedAllocator.16* %this, %class.btAlignedAllocator.16** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %"struct.btMultiSapBroadphase::btBridgeProxy"*** %hint, %"struct.btMultiSapBroadphase::btBridgeProxy"**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.16*, %class.btAlignedAllocator.16** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btMultiSapBroadphase::btBridgeProxy"**
  ret %"struct.btMultiSapBroadphase::btBridgeProxy"** %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EE10deallocateEPS2_(%class.btAlignedAllocator.16* %this, %"struct.btMultiSapBroadphase::btBridgeProxy"** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.16*, align 4
  %ptr.addr = alloca %"struct.btMultiSapBroadphase::btBridgeProxy"**, align 4
  store %class.btAlignedAllocator.16* %this, %class.btAlignedAllocator.16** %this.addr, align 4, !tbaa !2
  store %"struct.btMultiSapBroadphase::btBridgeProxy"** %ptr, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.16*, %class.btAlignedAllocator.16** %this.addr, align 4
  %0 = load %"struct.btMultiSapBroadphase::btBridgeProxy"**, %"struct.btMultiSapBroadphase::btBridgeProxy"*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %"struct.btMultiSapBroadphase::btBridgeProxy"** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.20* %this, %class.btMultiSapBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %CompareFunc.addr = alloca %class.btMultiSapBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store %class.btMultiSapBroadphasePairSortPredicate* %CompareFunc, %class.btMultiSapBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !6
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !6
  store i32 %3, i32* %j, align 4, !tbaa !6
  %4 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !6
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %8 = load %class.btMultiSapBroadphasePairSortPredicate*, %class.btMultiSapBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !63
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %call4 = call zeroext i1 @_ZNK37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btMultiSapBroadphasePairSortPredicate* %8, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %12 = load %class.btMultiSapBroadphasePairSortPredicate*, %class.btMultiSapBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !63
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 %14
  %call8 = call zeroext i1 @_ZNK37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btMultiSapBroadphasePairSortPredicate* %12, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %cmp = icmp sle i32 %16, %17
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %j, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.20* %this1, i32 %18, i32 %19)
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %inc11 = add nsw i32 %20, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %dec12 = add nsw i32 %21, -1
  store i32 %dec12, i32* %j, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %cmp13 = icmp sle i32 %22, %23
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %24 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %24, %25
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %26 = load %class.btMultiSapBroadphasePairSortPredicate*, %class.btMultiSapBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %27 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %28 = load i32, i32* %j, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.20* %this1, %class.btMultiSapBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %26, i32 %27, i32 %28)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %30 = load i32, i32* %hi.addr, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %29, %30
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %31 = load %class.btMultiSapBroadphasePairSortPredicate*, %class.btMultiSapBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %33 = load i32, i32* %hi.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.20* %this1, %class.btMultiSapBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  %34 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #9
  %35 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !58
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !58
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4, !tbaa !60
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !60
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4, !tbaa !61
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !61
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4, !tbaa !37
  store i8* %9, i8** %m_internalInfo1, align 4, !tbaa !37
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btMultiSapBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b1) #5 comdat {
entry:
  %this.addr = alloca %class.btMultiSapBroadphasePairSortPredicate*, align 4
  %a1.addr = alloca %struct.btBroadphasePair*, align 4
  %b1.addr = alloca %struct.btBroadphasePair*, align 4
  %aProxy0 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %aProxy1 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %bProxy0 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  %bProxy1 = alloca %"struct.btMultiSapBroadphase::btMultiSapProxy"*, align 4
  store %class.btMultiSapBroadphasePairSortPredicate* %this, %class.btMultiSapBroadphasePairSortPredicate** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %a1, %struct.btBroadphasePair** %a1.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %b1, %struct.btBroadphasePair** %b1.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiSapBroadphasePairSortPredicate*, %class.btMultiSapBroadphasePairSortPredicate** %this.addr, align 4
  %0 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a1.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 0, i32 0
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !58
  %tobool = icmp ne %struct.btBroadphaseProxy* %2, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a1.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 0
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !58
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 3
  %5 = load i8*, i8** %m_multiSapParentProxy, align 4, !tbaa !44
  %6 = bitcast i8* %5 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %6, %cond.true ], [ null, %cond.false ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0, align 4, !tbaa !2
  %7 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a1.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 0, i32 1
  %9 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !60
  %tobool3 = icmp ne %struct.btBroadphaseProxy* %9, null
  br i1 %tobool3, label %cond.true4, label %cond.false7

cond.true4:                                       ; preds = %cond.end
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a1.addr, align 4, !tbaa !2
  %m_pProxy15 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 0, i32 1
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy15, align 4, !tbaa !60
  %m_multiSapParentProxy6 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 3
  %12 = load i8*, i8** %m_multiSapParentProxy6, align 4, !tbaa !44
  %13 = bitcast i8* %12 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true4
  %cond9 = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %13, %cond.true4 ], [ null, %cond.false7 ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond9, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1, align 4, !tbaa !2
  %14 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b1.addr, align 4, !tbaa !2
  %m_pProxy010 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 0
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy010, align 4, !tbaa !58
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %16, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end8
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b1.addr, align 4, !tbaa !2
  %m_pProxy013 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 0, i32 0
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy013, align 4, !tbaa !58
  %m_multiSapParentProxy14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 3
  %19 = load i8*, i8** %m_multiSapParentProxy14, align 4, !tbaa !44
  %20 = bitcast i8* %19 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end8
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %20, %cond.true12 ], [ null, %cond.false15 ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond17, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0, align 4, !tbaa !2
  %21 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b1.addr, align 4, !tbaa !2
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %22, i32 0, i32 1
  %23 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4, !tbaa !60
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %23, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %24 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b1.addr, align 4, !tbaa !2
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %24, i32 0, i32 1
  %25 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4, !tbaa !60
  %m_multiSapParentProxy22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 3
  %26 = load i8*, i8** %m_multiSapParentProxy22, align 4, !tbaa !44
  %27 = bitcast i8* %26 to %"struct.btMultiSapBroadphase::btMultiSapProxy"*
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi %"struct.btMultiSapBroadphase::btMultiSapProxy"* [ %27, %cond.true20 ], [ null, %cond.false23 ]
  store %"struct.btMultiSapBroadphase::btMultiSapProxy"* %cond25, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1, align 4, !tbaa !2
  %28 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0, align 4, !tbaa !2
  %29 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0, align 4, !tbaa !2
  %cmp = icmp ugt %"struct.btMultiSapBroadphase::btMultiSapProxy"* %28, %29
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %30 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0, align 4, !tbaa !2
  %31 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0, align 4, !tbaa !2
  %cmp26 = icmp eq %"struct.btMultiSapBroadphase::btMultiSapProxy"* %30, %31
  br i1 %cmp26, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %32 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1, align 4, !tbaa !2
  %33 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1, align 4, !tbaa !2
  %cmp27 = icmp ugt %"struct.btMultiSapBroadphase::btMultiSapProxy"* %32, %33
  br i1 %cmp27, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %34 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0, align 4, !tbaa !2
  %35 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0, align 4, !tbaa !2
  %cmp28 = icmp eq %"struct.btMultiSapBroadphase::btMultiSapProxy"* %34, %35
  br i1 %cmp28, label %land.lhs.true29, label %land.end

land.lhs.true29:                                  ; preds = %lor.rhs
  %36 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1, align 4, !tbaa !2
  %37 = load %"struct.btMultiSapBroadphase::btMultiSapProxy"*, %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1, align 4, !tbaa !2
  %cmp30 = icmp eq %"struct.btMultiSapBroadphase::btMultiSapProxy"* %36, %37
  br i1 %cmp30, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true29
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a1.addr, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 2
  %39 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !61
  %40 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b1.addr, align 4, !tbaa !2
  %m_algorithm31 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %40, i32 0, i32 2
  %41 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm31, align 4, !tbaa !61
  %cmp32 = icmp ugt %class.btCollisionAlgorithm* %39, %41
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true29, %lor.rhs
  %42 = phi i1 [ false, %land.lhs.true29 ], [ false, %lor.rhs ], [ %cmp32, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %43 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %42, %land.end ]
  %44 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #9
  %45 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %bProxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #9
  %46 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #9
  %47 = bitcast %"struct.btMultiSapBroadphase::btMultiSapProxy"** %aProxy0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #9
  ret i1 %43
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.20* %this, i32 %index0, i32 %index1) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !6
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !63
  %4 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !63
  %6 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %6
  %7 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %8 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !62
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !63
  %10 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %11 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %12 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !62
  %13 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.20* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.20* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.20* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %3, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.20* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !78
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %5, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !79
  %7 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !24
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !79
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.20* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.21* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.20* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %7, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.20* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.20* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !63
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !78, !range !21
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !63
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.21* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4, !tbaa !63
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.21* %this, i32 %n, %struct.btBroadphasePair** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.21* %this, %struct.btBroadphasePair* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { noreturn nounwind }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !3, i64 28}
!11 = !{!"_ZTS20btMultiSapBroadphase", !12, i64 4, !3, i64 24, !3, i64 28, !3, i64 32, !14, i64 36, !3, i64 40, !7, i64 44, !15, i64 48}
!12 = !{!"_ZTS20btAlignedObjectArrayIP21btBroadphaseInterfaceE", !13, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!13 = !{!"_ZTS18btAlignedAllocatorIP21btBroadphaseInterfaceLj16EE"}
!14 = !{!"bool", !4, i64 0}
!15 = !{!"_ZTS20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE", !16, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorIPN20btMultiSapBroadphase15btMultiSapProxyELj16EE"}
!17 = !{!11, !3, i64 32}
!18 = !{!11, !14, i64 36}
!19 = !{!11, !7, i64 44}
!20 = !{!11, !3, i64 40}
!21 = !{i8 0, i8 2}
!22 = !{!23, !7, i64 12}
!23 = !{!"_ZTS18btQuantizedBvhNode", !4, i64 0, !4, i64 6, !7, i64 12}
!24 = !{!25, !25, i64 0}
!25 = !{!"long", !4, i64 0}
!26 = !{!12, !7, i64 4}
!27 = !{!12, !3, i64 12}
!28 = !{!29, !29, i64 0}
!29 = !{!"float", !4, i64 0}
!30 = !{!31, !31, i64 0}
!31 = !{!"short", !4, i64 0}
!32 = !{!33, !3, i64 12}
!33 = !{!"_ZTS20btAlignedObjectArrayI18btQuantizedBvhNodeE", !34, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!34 = !{!"_ZTS18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE"}
!35 = !{!33, !7, i64 4}
!36 = !{i64 0, i64 6, !37, i64 6, i64 6, !37, i64 12, i64 4, !6}
!37 = !{!4, !4, i64 0}
!38 = !{i64 0, i64 16, !37}
!39 = !{!40, !7, i64 100}
!40 = !{!"_ZTSN20btMultiSapBroadphase15btMultiSapProxyE", !41, i64 48, !43, i64 68, !43, i64 84, !7, i64 100}
!41 = !{!"_ZTS20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE", !42, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!42 = !{!"_ZTS18btAlignedAllocatorIPN20btMultiSapBroadphase13btBridgeProxyELj16EE"}
!43 = !{!"_ZTS9btVector3", !4, i64 0}
!44 = !{!45, !3, i64 8}
!45 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !31, i64 4, !31, i64 6, !3, i64 8, !7, i64 12, !43, i64 16, !43, i64 32}
!46 = !{!15, !3, i64 12}
!47 = !{!15, !7, i64 4}
!48 = !{!49, !3, i64 0}
!49 = !{!"_ZTSN20btMultiSapBroadphase13btBridgeProxyE", !3, i64 0, !3, i64 4}
!50 = !{!49, !3, i64 4}
!51 = !{!41, !3, i64 12}
!52 = !{!41, !7, i64 4}
!53 = !{!14, !14, i64 0}
!54 = !{!55, !3, i64 4}
!55 = !{!"_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback", !3, i64 4, !3, i64 8, !3, i64 12}
!56 = !{!55, !3, i64 8}
!57 = !{!55, !3, i64 12}
!58 = !{!59, !3, i64 0}
!59 = !{!"_ZTS16btBroadphasePair", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12}
!60 = !{!59, !3, i64 4}
!61 = !{!59, !3, i64 8}
!62 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 12, i64 4, !6}
!63 = !{!64, !3, i64 12}
!64 = !{!"_ZTS20btAlignedObjectArrayI16btBroadphasePairE", !65, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !14, i64 16}
!65 = !{!"_ZTS18btAlignedAllocatorI16btBroadphasePairLj16EE"}
!66 = !{!64, !7, i64 4}
!67 = !{!45, !31, i64 4}
!68 = !{!45, !31, i64 6}
!69 = !{!45, !3, i64 0}
!70 = !{!41, !14, i64 16}
!71 = !{!41, !7, i64 8}
!72 = !{!12, !14, i64 16}
!73 = !{!12, !7, i64 8}
!74 = !{!15, !14, i64 16}
!75 = !{!15, !7, i64 8}
!76 = !{!33, !7, i64 8}
!77 = !{!33, !14, i64 16}
!78 = !{!64, !14, i64 16}
!79 = !{!64, !7, i64 8}
