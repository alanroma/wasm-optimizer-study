; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConeShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConeShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConeShape = type { %class.btConvexInternalShape, float, float, float, [3 x i32] }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btConeShapeZ = type { %class.btConeShape }
%class.btConeShapeX = type { %class.btConeShape }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque
%struct.btConeShapeData = type { %struct.btConvexInternalShapeData, i32, [4 x i8] }
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN9btVector3C2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN11btConeShapeD0Ev = comdat any

$_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3 = comdat any

$_ZNK11btConeShape7getNameEv = comdat any

$_ZNK11btConeShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK11btConeShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK11btConeShape9serializeEPvP12btSerializer = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZN12btConeShapeZD0Ev = comdat any

$_ZNK12btConeShapeZ7getNameEv = comdat any

$_ZNK12btConeShapeZ38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN12btConeShapeXD0Ev = comdat any

$_ZNK12btConeShapeX7getNameEv = comdat any

$_ZNK12btConeShapeX38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btConeShapedlEPv = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZTV12btConeShapeZ = comdat any

$_ZTV12btConeShapeX = comdat any

$_ZTS12btConeShapeZ = comdat any

$_ZTI12btConeShapeZ = comdat any

$_ZTS12btConeShapeX = comdat any

$_ZTI12btConeShapeX = comdat any

@_ZTV11btConeShape = hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI11btConeShape to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btConeShape*)* @_ZN11btConeShapeD0Ev to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConeShape*, %class.btVector3*)* @_ZN11btConeShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btConeShape*, float, %class.btVector3*)* @_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btConeShape*)* @_ZNK11btConeShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShape*)* @_ZNK11btConeShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConeShape*)* @_ZNK11btConeShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConeShape*, i8*, %class.btSerializer*)* @_ZNK11btConeShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShape*, %class.btVector3*)* @_ZNK11btConeShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShape*, %class.btVector3*)* @_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btConeShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, align 4
@_ZTV12btConeShapeZ = linkonce_odr hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI12btConeShapeZ to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btConeShapeZ*)* @_ZN12btConeShapeZD0Ev to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConeShape*, %class.btVector3*)* @_ZN11btConeShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btConeShape*, float, %class.btVector3*)* @_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btConeShapeZ*)* @_ZNK12btConeShapeZ7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShapeZ*)* @_ZNK12btConeShapeZ38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConeShape*)* @_ZNK11btConeShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConeShape*, i8*, %class.btSerializer*)* @_ZNK11btConeShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShape*, %class.btVector3*)* @_ZNK11btConeShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShape*, %class.btVector3*)* @_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btConeShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, comdat, align 4
@_ZTV12btConeShapeX = linkonce_odr hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI12btConeShapeX to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btConeShapeX*)* @_ZN12btConeShapeXD0Ev to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConeShape*, %class.btVector3*)* @_ZN11btConeShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btConeShape*, float, %class.btVector3*)* @_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btConeShapeX*)* @_ZNK12btConeShapeX7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShapeX*)* @_ZNK12btConeShapeX38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConeShape*)* @_ZNK11btConeShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConeShape*, i8*, %class.btSerializer*)* @_ZNK11btConeShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShape*, %class.btVector3*)* @_ZNK11btConeShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btConeShape*, %class.btVector3*)* @_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btConeShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS11btConeShape = hidden constant [14 x i8] c"11btConeShape\00", align 1
@_ZTI21btConvexInternalShape = external constant i8*
@_ZTI11btConeShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @_ZTS11btConeShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btConvexInternalShape to i8*) }, align 4
@_ZTS12btConeShapeZ = linkonce_odr hidden constant [15 x i8] c"12btConeShapeZ\00", comdat, align 1
@_ZTI12btConeShapeZ = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btConeShapeZ, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI11btConeShape to i8*) }, comdat, align 4
@_ZTS12btConeShapeX = linkonce_odr hidden constant [15 x i8] c"12btConeShapeX\00", comdat, align 1
@_ZTI12btConeShapeX = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btConeShapeX, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI11btConeShape to i8*) }, comdat, align 4
@.str = private unnamed_addr constant [5 x i8] c"Cone\00", align 1
@.str.1 = private unnamed_addr constant [16 x i8] c"btConeShapeData\00", align 1
@.str.2 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@.str.3 = private unnamed_addr constant [6 x i8] c"ConeZ\00", align 1
@.str.4 = private unnamed_addr constant [6 x i8] c"ConeX\00", align 1

@_ZN11btConeShapeC1Eff = hidden unnamed_addr alias %class.btConeShape* (%class.btConeShape*, float, float), %class.btConeShape* (%class.btConeShape*, float, float)* @_ZN11btConeShapeC2Eff
@_ZN12btConeShapeZC1Eff = hidden unnamed_addr alias %class.btConeShapeZ* (%class.btConeShapeZ*, float, float), %class.btConeShapeZ* (%class.btConeShapeZ*, float, float)* @_ZN12btConeShapeZC2Eff
@_ZN12btConeShapeXC1Eff = hidden unnamed_addr alias %class.btConeShapeX* (%class.btConeShapeX*, float, float), %class.btConeShapeX* (%class.btConeShapeX*, float, float)* @_ZN12btConeShapeXC2Eff

define hidden %class.btConeShape* @_ZN11btConeShapeC2Eff(%class.btConeShape* returned %this, float %radius, float %height) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  %halfExtents = alloca %class.btVector3, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btConeShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV11btConeShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_radius = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %2 = load float, float* %radius.addr, align 4, !tbaa !6
  store float %2, float* %m_radius, align 4, !tbaa !10
  %m_height = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %3 = load float, float* %height.addr, align 4, !tbaa !6
  store float %3, float* %m_height, align 4, !tbaa !12
  %4 = bitcast %class.btConeShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %4, i32 0, i32 1
  store i32 11, i32* %m_shapeType, align 4, !tbaa !13
  call void @_ZN11btConeShape14setConeUpIndexEi(%class.btConeShape* %this1, i32 1)
  %5 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %halfExtents)
  %m_radius3 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %6 = load float, float* %m_radius3, align 4, !tbaa !10
  %m_radius4 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %7 = load float, float* %m_radius4, align 4, !tbaa !10
  %m_radius5 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %8 = load float, float* %m_radius5, align 4, !tbaa !10
  %mul = fmul float %7, %8
  %m_height6 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_height6, align 4, !tbaa !12
  %m_height7 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %10 = load float, float* %m_height7, align 4, !tbaa !12
  %mul8 = fmul float %9, %10
  %add = fadd float %mul, %mul8
  %call9 = call float @_Z6btSqrtf(float %add)
  %div = fdiv float %6, %call9
  %m_sinAngle = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 1
  store float %div, float* %m_sinAngle, align 4, !tbaa !16
  %11 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  ret %class.btConeShape* %this1
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #1

define hidden void @_ZN11btConeShape14setConeUpIndexEi(%class.btConeShape* %this, i32 %upIndex) #0 {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %upIndex.addr = alloca i32, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store i32 %upIndex, i32* %upIndex.addr, align 4, !tbaa !17
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = load i32, i32* %upIndex.addr, align 4, !tbaa !17
  switch i32 %0, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb6
    i32 2, label %sw.bb13
  ]

sw.bb:                                            ; preds = %entry
  %m_coneIndices = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices, i32 0, i32 0
  store i32 1, i32* %arrayidx, align 4, !tbaa !17
  %m_coneIndices2 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices2, i32 0, i32 1
  store i32 0, i32* %arrayidx3, align 4, !tbaa !17
  %m_coneIndices4 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices4, i32 0, i32 2
  store i32 2, i32* %arrayidx5, align 4, !tbaa !17
  br label %sw.epilog

sw.bb6:                                           ; preds = %entry
  %m_coneIndices7 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx8 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices7, i32 0, i32 0
  store i32 0, i32* %arrayidx8, align 4, !tbaa !17
  %m_coneIndices9 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx10 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices9, i32 0, i32 1
  store i32 1, i32* %arrayidx10, align 4, !tbaa !17
  %m_coneIndices11 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices11, i32 0, i32 2
  store i32 2, i32* %arrayidx12, align 4, !tbaa !17
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry
  %m_coneIndices14 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices14, i32 0, i32 0
  store i32 0, i32* %arrayidx15, align 4, !tbaa !17
  %m_coneIndices16 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices16, i32 0, i32 1
  store i32 2, i32* %arrayidx17, align 4, !tbaa !17
  %m_coneIndices18 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices18, i32 0, i32 2
  store i32 1, i32* %arrayidx19, align 4, !tbaa !17
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb13, %sw.bb6, %sw.bb
  %m_radius = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %1 = load float, float* %m_radius, align 4, !tbaa !10
  %2 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 2
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_implicitShapeDimensions)
  %m_coneIndices20 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices20, i32 0, i32 0
  %3 = load i32, i32* %arrayidx21, align 4, !tbaa !17
  %arrayidx22 = getelementptr inbounds float, float* %call, i32 %3
  store float %1, float* %arrayidx22, align 4, !tbaa !6
  %m_height = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %4 = load float, float* %m_height, align 4, !tbaa !12
  %5 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions23 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %5, i32 0, i32 2
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_implicitShapeDimensions23)
  %m_coneIndices25 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices25, i32 0, i32 1
  %6 = load i32, i32* %arrayidx26, align 4, !tbaa !17
  %arrayidx27 = getelementptr inbounds float, float* %call24, i32 %6
  store float %4, float* %arrayidx27, align 4, !tbaa !6
  %m_radius28 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %7 = load float, float* %m_radius28, align 4, !tbaa !10
  %8 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions29 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %8, i32 0, i32 2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_implicitShapeDimensions29)
  %m_coneIndices31 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices31, i32 0, i32 2
  %9 = load i32, i32* %arrayidx32, align 4, !tbaa !17
  %arrayidx33 = getelementptr inbounds float, float* %call30, i32 %9
  store float %7, float* %arrayidx33, align 4, !tbaa !6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !6
  %0 = load float, float* %y.addr, align 4, !tbaa !6
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define hidden %class.btConeShapeZ* @_ZN12btConeShapeZC2Eff(%class.btConeShapeZ* returned %this, float %radius, float %height) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeShapeZ*, align 4
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  store %class.btConeShapeZ* %this, %class.btConeShapeZ** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %this1 = load %class.btConeShapeZ*, %class.btConeShapeZ** %this.addr, align 4
  %0 = bitcast %class.btConeShapeZ* %this1 to %class.btConeShape*
  %1 = load float, float* %radius.addr, align 4, !tbaa !6
  %2 = load float, float* %height.addr, align 4, !tbaa !6
  %call = call %class.btConeShape* @_ZN11btConeShapeC2Eff(%class.btConeShape* %0, float %1, float %2)
  %3 = bitcast %class.btConeShapeZ* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV12btConeShapeZ, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !8
  %4 = bitcast %class.btConeShapeZ* %this1 to %class.btConeShape*
  call void @_ZN11btConeShape14setConeUpIndexEi(%class.btConeShape* %4, i32 2)
  ret %class.btConeShapeZ* %this1
}

define hidden %class.btConeShapeX* @_ZN12btConeShapeXC2Eff(%class.btConeShapeX* returned %this, float %radius, float %height) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeShapeX*, align 4
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  store %class.btConeShapeX* %this, %class.btConeShapeX** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %this1 = load %class.btConeShapeX*, %class.btConeShapeX** %this.addr, align 4
  %0 = bitcast %class.btConeShapeX* %this1 to %class.btConeShape*
  %1 = load float, float* %radius.addr, align 4, !tbaa !6
  %2 = load float, float* %height.addr, align 4, !tbaa !6
  %call = call %class.btConeShape* @_ZN11btConeShapeC2Eff(%class.btConeShape* %0, float %1, float %2)
  %3 = bitcast %class.btConeShapeX* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV12btConeShapeX, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !8
  %4 = bitcast %class.btConeShapeX* %this1 to %class.btConeShape*
  call void @_ZN11btConeShape14setConeUpIndexEi(%class.btConeShape* %4, i32 0)
  ret %class.btConeShapeX* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define hidden void @_ZNK11btConeShape16coneLocalSupportERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #0 {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %halfHeight = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %s = alloca float, align 4
  %d = alloca float, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = bitcast float* %halfHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_height = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %1 = load float, float* %m_height, align 4, !tbaa !12
  %mul = fmul float %1, 5.000000e-01
  store float %mul, float* %halfHeight, align 4, !tbaa !6
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %m_coneIndices = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices, i32 0, i32 1
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !17
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %3
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %5)
  %m_sinAngle = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 1
  %6 = load float, float* %m_sinAngle, align 4, !tbaa !16
  %mul4 = fmul float %call3, %6
  %cmp = fcmp ogt float %4, %mul4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices7 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx8 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices7, i32 0, i32 0
  %7 = load i32, i32* %arrayidx8, align 4, !tbaa !17
  %arrayidx9 = getelementptr inbounds float, float* %call6, i32 %7
  store float 0.000000e+00, float* %arrayidx9, align 4, !tbaa !6
  %8 = load float, float* %halfHeight, align 4, !tbaa !6
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices11 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices11, i32 0, i32 1
  %9 = load i32, i32* %arrayidx12, align 4, !tbaa !17
  %arrayidx13 = getelementptr inbounds float, float* %call10, i32 %9
  store float %8, float* %arrayidx13, align 4, !tbaa !6
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices15 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx16 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices15, i32 0, i32 2
  %10 = load i32, i32* %arrayidx16, align 4, !tbaa !17
  %arrayidx17 = getelementptr inbounds float, float* %call14, i32 %10
  store float 0.000000e+00, float* %arrayidx17, align 4, !tbaa !6
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup77

if.else:                                          ; preds = %entry
  %11 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %m_coneIndices19 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices19, i32 0, i32 0
  %13 = load i32, i32* %arrayidx20, align 4, !tbaa !17
  %arrayidx21 = getelementptr inbounds float, float* %call18, i32 %13
  %14 = load float, float* %arrayidx21, align 4, !tbaa !6
  %15 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call22 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %m_coneIndices23 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices23, i32 0, i32 0
  %16 = load i32, i32* %arrayidx24, align 4, !tbaa !17
  %arrayidx25 = getelementptr inbounds float, float* %call22, i32 %16
  %17 = load float, float* %arrayidx25, align 4, !tbaa !6
  %mul26 = fmul float %14, %17
  %18 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %m_coneIndices28 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices28, i32 0, i32 2
  %19 = load i32, i32* %arrayidx29, align 4, !tbaa !17
  %arrayidx30 = getelementptr inbounds float, float* %call27, i32 %19
  %20 = load float, float* %arrayidx30, align 4, !tbaa !6
  %21 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %21)
  %m_coneIndices32 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx33 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices32, i32 0, i32 2
  %22 = load i32, i32* %arrayidx33, align 4, !tbaa !17
  %arrayidx34 = getelementptr inbounds float, float* %call31, i32 %22
  %23 = load float, float* %arrayidx34, align 4, !tbaa !6
  %mul35 = fmul float %20, %23
  %add = fadd float %mul26, %mul35
  %call36 = call float @_Z6btSqrtf(float %add)
  store float %call36, float* %s, align 4, !tbaa !6
  %24 = load float, float* %s, align 4, !tbaa !6
  %cmp37 = fcmp ogt float %24, 0x3E80000000000000
  br i1 %cmp37, label %if.then38, label %if.else62

if.then38:                                        ; preds = %if.else
  %25 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %m_radius = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %26 = load float, float* %m_radius, align 4, !tbaa !10
  %27 = load float, float* %s, align 4, !tbaa !6
  %div = fdiv float %26, %27
  store float %div, float* %d, align 4, !tbaa !6
  %call39 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %28 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call40 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %m_coneIndices41 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices41, i32 0, i32 0
  %29 = load i32, i32* %arrayidx42, align 4, !tbaa !17
  %arrayidx43 = getelementptr inbounds float, float* %call40, i32 %29
  %30 = load float, float* %arrayidx43, align 4, !tbaa !6
  %31 = load float, float* %d, align 4, !tbaa !6
  %mul44 = fmul float %30, %31
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices46 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx47 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices46, i32 0, i32 0
  %32 = load i32, i32* %arrayidx47, align 4, !tbaa !17
  %arrayidx48 = getelementptr inbounds float, float* %call45, i32 %32
  store float %mul44, float* %arrayidx48, align 4, !tbaa !6
  %33 = load float, float* %halfHeight, align 4, !tbaa !6
  %fneg = fneg float %33
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices50 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx51 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices50, i32 0, i32 1
  %34 = load i32, i32* %arrayidx51, align 4, !tbaa !17
  %arrayidx52 = getelementptr inbounds float, float* %call49, i32 %34
  store float %fneg, float* %arrayidx52, align 4, !tbaa !6
  %35 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call53 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %m_coneIndices54 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx55 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices54, i32 0, i32 2
  %36 = load i32, i32* %arrayidx55, align 4, !tbaa !17
  %arrayidx56 = getelementptr inbounds float, float* %call53, i32 %36
  %37 = load float, float* %arrayidx56, align 4, !tbaa !6
  %38 = load float, float* %d, align 4, !tbaa !6
  %mul57 = fmul float %37, %38
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices59 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx60 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices59, i32 0, i32 2
  %39 = load i32, i32* %arrayidx60, align 4, !tbaa !17
  %arrayidx61 = getelementptr inbounds float, float* %call58, i32 %39
  store float %mul57, float* %arrayidx61, align 4, !tbaa !6
  store i32 1, i32* %cleanup.dest.slot, align 4
  %40 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  br label %cleanup

if.else62:                                        ; preds = %if.else
  %call63 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %call64 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices65 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx66 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices65, i32 0, i32 0
  %41 = load i32, i32* %arrayidx66, align 4, !tbaa !17
  %arrayidx67 = getelementptr inbounds float, float* %call64, i32 %41
  store float 0.000000e+00, float* %arrayidx67, align 4, !tbaa !6
  %42 = load float, float* %halfHeight, align 4, !tbaa !6
  %fneg68 = fneg float %42
  %call69 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices70 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx71 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices70, i32 0, i32 1
  %43 = load i32, i32* %arrayidx71, align 4, !tbaa !17
  %arrayidx72 = getelementptr inbounds float, float* %call69, i32 %43
  store float %fneg68, float* %arrayidx72, align 4, !tbaa !6
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %m_coneIndices74 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx75 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices74, i32 0, i32 2
  %44 = load i32, i32* %arrayidx75, align 4, !tbaa !17
  %arrayidx76 = getelementptr inbounds float, float* %call73, i32 %44
  store float 0.000000e+00, float* %arrayidx76, align 4, !tbaa !6
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else62, %if.then38
  %45 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  br label %cleanup77

cleanup77:                                        ; preds = %cleanup, %if.then
  %46 = bitcast float* %halfHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define hidden void @_ZNK11btConeShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  call void @_ZNK11btConeShape16coneLocalSupportERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btConeShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define hidden void @_ZNK11btConeShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btConeShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %vec = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !17
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !17
  %2 = load i32, i32* %numVectors.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  store %class.btVector3* %arrayidx, %class.btVector3** %vec, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  call void @_ZNK11btConeShape16coneLocalSupportERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btConeShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %9 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !18
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #8
  %14 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

define hidden void @_ZNK11btConeShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %vecnorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  call void @_ZNK11btConeShape16coneLocalSupportERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btConeShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %2 = bitcast %class.btConvexInternalShape* %1 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %3 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %3(%class.btConvexInternalShape* %1)
  %cmp = fcmp une float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %4 = bitcast %class.btVector3* %vecnorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %vecnorm to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !18
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vecnorm)
  %cmp3 = fcmp olt float %call2, 0x3D10000000000000
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float -1.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float -1.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vecnorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vecnorm)
  %14 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %17 = bitcast %class.btConvexInternalShape* %16 to float (%class.btConvexInternalShape*)***
  %vtable10 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %17, align 4, !tbaa !8
  %vfn11 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable10, i64 12
  %18 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn11, align 4
  %call12 = call float %18(%class.btConvexInternalShape* %16)
  store float %call12, float* %ref.tmp9, align 4, !tbaa !6
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %vecnorm)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8)
  %19 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %vecnorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #8
  br label %if.end14

if.end14:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !6
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

define hidden void @_ZN11btConeShape15setLocalScalingERK9btVector3(%class.btConeShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %axis = alloca i32, align 4
  %r1 = alloca i32, align 4
  %r2 = alloca i32, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_coneIndices = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices, i32 0, i32 1
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !17
  store i32 %1, i32* %axis, align 4, !tbaa !17
  %2 = bitcast i32* %r1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_coneIndices2 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices2, i32 0, i32 0
  %3 = load i32, i32* %arrayidx3, align 4, !tbaa !17
  store i32 %3, i32* %r1, align 4, !tbaa !17
  %4 = bitcast i32* %r2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_coneIndices4 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices4, i32 0, i32 2
  %5 = load i32, i32* %arrayidx5, align 4, !tbaa !17
  store i32 %5, i32* %r2, align 4, !tbaa !17
  %6 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %7 = load i32, i32* %axis, align 4, !tbaa !17
  %arrayidx6 = getelementptr inbounds float, float* %call, i32 %7
  %8 = load float, float* %arrayidx6, align 4, !tbaa !6
  %9 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %9, i32 0, i32 1
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localScaling)
  %10 = load i32, i32* %axis, align 4, !tbaa !17
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %10
  %11 = load float, float* %arrayidx8, align 4, !tbaa !6
  %div = fdiv float %8, %11
  %m_height = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %12 = load float, float* %m_height, align 4, !tbaa !12
  %mul = fmul float %12, %div
  store float %mul, float* %m_height, align 4, !tbaa !12
  %13 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %13)
  %14 = load i32, i32* %r1, align 4, !tbaa !17
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %14
  %15 = load float, float* %arrayidx10, align 4, !tbaa !6
  %16 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling11 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %16, i32 0, i32 1
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localScaling11)
  %17 = load i32, i32* %r1, align 4, !tbaa !17
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 %17
  %18 = load float, float* %arrayidx13, align 4, !tbaa !6
  %div14 = fdiv float %15, %18
  %19 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %19)
  %20 = load i32, i32* %r2, align 4, !tbaa !17
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 %20
  %21 = load float, float* %arrayidx16, align 4, !tbaa !6
  %22 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling17 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %22, i32 0, i32 1
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localScaling17)
  %23 = load i32, i32* %r2, align 4, !tbaa !17
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %23
  %24 = load float, float* %arrayidx19, align 4, !tbaa !6
  %div20 = fdiv float %21, %24
  %add = fadd float %div14, %div20
  %div21 = fdiv float %add, 2.000000e+00
  %m_radius = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %25 = load float, float* %m_radius, align 4, !tbaa !10
  %mul22 = fmul float %25, %div21
  store float %mul22, float* %m_radius, align 4, !tbaa !10
  %m_radius23 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %26 = load float, float* %m_radius23, align 4, !tbaa !10
  %m_radius24 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %27 = load float, float* %m_radius24, align 4, !tbaa !10
  %m_radius25 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 2
  %28 = load float, float* %m_radius25, align 4, !tbaa !10
  %mul26 = fmul float %27, %28
  %m_height27 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %29 = load float, float* %m_height27, align 4, !tbaa !12
  %m_height28 = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 3
  %30 = load float, float* %m_height28, align 4, !tbaa !12
  %mul29 = fmul float %29, %30
  %add30 = fadd float %mul26, %mul29
  %call31 = call float @_Z6btSqrtf(float %add30)
  %div32 = fdiv float %26, %call31
  %m_sinAngle = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 1
  store float %div32, float* %m_sinAngle, align 4, !tbaa !16
  %31 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %32 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %32)
  %33 = bitcast i32* %r2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast i32* %r1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  ret void
}

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btConeShapeD0Ev(%class.btConeShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %call = call %class.btConeShape* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btConeShape* (%class.btConeShape*)*)(%class.btConeShape* %this1) #8
  %0 = bitcast %class.btConeShape* %this1 to i8*
  call void @_ZN11btConeShapedlEPv(i8* %0) #8
  ret void
}

define linkonce_odr hidden void @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_(%class.btConvexInternalShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %4 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btConvexInternalShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

define linkonce_odr hidden void @_ZNK11btConeShape21calculateLocalInertiaEfR9btVector3(%class.btConeShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %identity = alloca %class.btTransform, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %margin = alloca float, align 4
  %lx = alloca float, align 4
  %ly = alloca float, align 4
  %lz = alloca float, align 4
  %x2 = alloca float, align 4
  %y2 = alloca float, align 4
  %z2 = alloca float, align 4
  %scaledmass = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !6
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = bitcast %class.btTransform* %identity to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %identity)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %identity)
  %1 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %2 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %3 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexInternalShape* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %identity, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %6 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin)
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 5.000000e-01, float* %ref.tmp4, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %9 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %13 = bitcast %class.btConvexInternalShape* %12 to float (%class.btConvexInternalShape*)***
  %vtable5 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %13, align 4, !tbaa !8
  %vfn6 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable5, i64 12
  %14 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn6, align 4
  %call7 = call float %14(%class.btConvexInternalShape* %12)
  store float %call7, float* %margin, align 4, !tbaa !6
  %15 = bitcast float* %lx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %halfExtents)
  %16 = load float, float* %call8, align 4, !tbaa !6
  %17 = load float, float* %margin, align 4, !tbaa !6
  %add = fadd float %16, %17
  %mul = fmul float 2.000000e+00, %add
  store float %mul, float* %lx, align 4, !tbaa !6
  %18 = bitcast float* %ly to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %halfExtents)
  %19 = load float, float* %call9, align 4, !tbaa !6
  %20 = load float, float* %margin, align 4, !tbaa !6
  %add10 = fadd float %19, %20
  %mul11 = fmul float 2.000000e+00, %add10
  store float %mul11, float* %ly, align 4, !tbaa !6
  %21 = bitcast float* %lz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %halfExtents)
  %22 = load float, float* %call12, align 4, !tbaa !6
  %23 = load float, float* %margin, align 4, !tbaa !6
  %add13 = fadd float %22, %23
  %mul14 = fmul float 2.000000e+00, %add13
  store float %mul14, float* %lz, align 4, !tbaa !6
  %24 = bitcast float* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load float, float* %lx, align 4, !tbaa !6
  %26 = load float, float* %lx, align 4, !tbaa !6
  %mul15 = fmul float %25, %26
  store float %mul15, float* %x2, align 4, !tbaa !6
  %27 = bitcast float* %y2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load float, float* %ly, align 4, !tbaa !6
  %29 = load float, float* %ly, align 4, !tbaa !6
  %mul16 = fmul float %28, %29
  store float %mul16, float* %y2, align 4, !tbaa !6
  %30 = bitcast float* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %31 = load float, float* %lz, align 4, !tbaa !6
  %32 = load float, float* %lz, align 4, !tbaa !6
  %mul17 = fmul float %31, %32
  store float %mul17, float* %z2, align 4, !tbaa !6
  %33 = bitcast float* %scaledmass to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %34 = load float, float* %mass.addr, align 4, !tbaa !6
  %mul18 = fmul float %34, 0x3FB5555540000000
  store float %mul18, float* %scaledmass, align 4, !tbaa !6
  %35 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #8
  %36 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #8
  %37 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load float, float* %y2, align 4, !tbaa !6
  %39 = load float, float* %z2, align 4, !tbaa !6
  %add22 = fadd float %38, %39
  store float %add22, float* %ref.tmp21, align 4, !tbaa !6
  %40 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #8
  %41 = load float, float* %x2, align 4, !tbaa !6
  %42 = load float, float* %z2, align 4, !tbaa !6
  %add24 = fadd float %41, %42
  store float %add24, float* %ref.tmp23, align 4, !tbaa !6
  %43 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %44 = load float, float* %x2, align 4, !tbaa !6
  %45 = load float, float* %y2, align 4, !tbaa !6
  %add26 = fadd float %44, %45
  store float %add26, float* %ref.tmp25, align 4, !tbaa !6
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, float* nonnull align 4 dereferenceable(4) %scaledmass, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %46 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %47 = bitcast %class.btVector3* %46 to i8*
  %48 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false), !tbaa.struct !18
  %49 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %51 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  %52 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #8
  %53 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #8
  %54 = bitcast float* %scaledmass to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #8
  %55 = bitcast float* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #8
  %56 = bitcast float* %y2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #8
  %57 = bitcast float* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  %58 = bitcast float* %lz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  %59 = bitcast float* %ly to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #8
  %60 = bitcast float* %lx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #8
  %61 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #8
  %62 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #8
  %63 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %63) #8
  %64 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast %class.btTransform* %identity to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %65) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK11btConeShape7getNameEv(%class.btConeShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK11btConeShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !6
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !6
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !20
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !20
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK11btConeShape28calculateSerializeBufferSizeEv(%class.btConeShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  ret i32 60
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK11btConeShape9serializeEPvP12btSerializer(%class.btConeShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConeShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConeShapeData*, align 4
  store %class.btConeShape* %this, %class.btConeShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShape*, %class.btConeShape** %this.addr, align 4
  %0 = bitcast %struct.btConeShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConeShapeData*
  store %struct.btConeShapeData* %2, %struct.btConeShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConeShape* %this1 to %class.btConvexInternalShape*
  %4 = load %struct.btConeShapeData*, %struct.btConeShapeData** %shapeData, align 4, !tbaa !2
  %m_convexInternalShapeData = getelementptr inbounds %struct.btConeShapeData, %struct.btConeShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btConvexInternalShapeData* %m_convexInternalShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %3, i8* %5, %class.btSerializer* %6)
  %m_coneIndices = getelementptr inbounds %class.btConeShape, %class.btConeShape* %this1, i32 0, i32 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_coneIndices, i32 0, i32 1
  %7 = load i32, i32* %arrayidx, align 4, !tbaa !17
  %8 = load %struct.btConeShapeData*, %struct.btConeShapeData** %shapeData, align 4, !tbaa !2
  %m_upIndex = getelementptr inbounds %struct.btConeShapeData, %struct.btConeShapeData* %8, i32 0, i32 1
  store i32 %7, i32* %m_upIndex, align 4, !tbaa !23
  %9 = bitcast %struct.btConeShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !17
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN12btConeShapeZD0Ev(%class.btConeShapeZ* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConeShapeZ*, align 4
  store %class.btConeShapeZ* %this, %class.btConeShapeZ** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShapeZ*, %class.btConeShapeZ** %this.addr, align 4
  %call = call %class.btConeShapeZ* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btConeShapeZ* (%class.btConeShapeZ*)*)(%class.btConeShapeZ* %this1) #8
  %0 = bitcast %class.btConeShapeZ* %this1 to i8*
  call void @_ZN11btConeShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK12btConeShapeZ7getNameEv(%class.btConeShapeZ* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConeShapeZ*, align 4
  store %class.btConeShapeZ* %this, %class.btConeShapeZ** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShapeZ*, %class.btConeShapeZ** %this.addr, align 4
  ret i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.3, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK12btConeShapeZ38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeShapeZ* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btConeShapeZ*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btConeShapeZ* %this, %class.btConeShapeZ** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShapeZ*, %class.btConeShapeZ** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN12btConeShapeXD0Ev(%class.btConeShapeX* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConeShapeX*, align 4
  store %class.btConeShapeX* %this, %class.btConeShapeX** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShapeX*, %class.btConeShapeX** %this.addr, align 4
  %call = call %class.btConeShapeX* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btConeShapeX* (%class.btConeShapeX*)*)(%class.btConeShapeX* %this1) #8
  %0 = bitcast %class.btConeShapeX* %this1 to i8*
  call void @_ZN11btConeShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK12btConeShapeX7getNameEv(%class.btConeShapeX* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConeShapeX*, align 4
  store %class.btConeShapeX* %this, %class.btConeShapeX** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShapeX*, %class.btConeShapeX** %this.addr, align 4
  ret i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.4, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK12btConeShapeX38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeShapeX* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btConeShapeX*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btConeShapeX* %this, %class.btConeShapeX** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConeShapeX*, %class.btConeShapeX** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !6
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !6
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !6
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !6
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !6
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !6
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !6
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btConeShapedlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !6
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !20
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !28
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !17
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !6
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !7, i64 56}
!11 = !{!"_ZTS11btConeShape", !7, i64 52, !7, i64 56, !7, i64 60, !4, i64 64}
!12 = !{!11, !7, i64 60}
!13 = !{!14, !15, i64 4}
!14 = !{!"_ZTS16btCollisionShape", !15, i64 4, !3, i64 8}
!15 = !{!"int", !4, i64 0}
!16 = !{!11, !7, i64 52}
!17 = !{!15, !15, i64 0}
!18 = !{i64 0, i64 16, !19}
!19 = !{!4, !4, i64 0}
!20 = !{!21, !7, i64 44}
!21 = !{!"_ZTS21btConvexInternalShape", !22, i64 12, !22, i64 28, !7, i64 44, !7, i64 48}
!22 = !{!"_ZTS9btVector3", !4, i64 0}
!23 = !{!24, !15, i64 52}
!24 = !{!"_ZTS15btConeShapeData", !25, i64 0, !15, i64 52, !4, i64 56}
!25 = !{!"_ZTS25btConvexInternalShapeData", !26, i64 0, !27, i64 12, !27, i64 28, !7, i64 44, !15, i64 48}
!26 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !15, i64 4, !4, i64 8}
!27 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!28 = !{!25, !7, i64 44}
