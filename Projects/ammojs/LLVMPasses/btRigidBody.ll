; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Dynamics/btRigidBody.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Dynamics/btRigidBody.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%"struct.btRigidBody::btRigidBodyConstructionInfo" = type { float, %class.btMotionState*, %class.btTransform, %class.btCollisionShape*, %class.btVector3, float, float, float, float, float, float, float, i8, float, float, float, float }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btSerializer = type { i32 (...)** }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev = comdat any

$_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3 = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ = comdat any

$_ZN11btRigidBody14getMotionStateEv = comdat any

$_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_ = comdat any

$_ZNK11btRigidBody17getCollisionShapeEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z9btClampedIfERKT_S2_S2_S2_ = comdat any

$_Z5btPowff = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZN11btRigidBody17applyCentralForceERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x36scaledERK9btVector3 = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btRigidBody22getInvInertiaDiagLocalEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZNK17btCollisionObject17isKinematicObjectEv = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZN11btRigidBody6upcastEPK17btCollisionObject = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZNK17btTypedConstraint9isEnabledEv = comdat any

$_ZNK17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZNK17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_ = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZN11btRigidBodyD2Ev = comdat any

$_ZN11btRigidBodyD0Ev = comdat any

$_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_Z5btSinf = comdat any

$_Z5btCosf = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z6btAcosf = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN17btCollisionObjectdlEPv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv = comdat any

@gDeactivationTime = hidden global float 2.000000e+00, align 4
@gDisableDeactivation = hidden global i8 0, align 1
@_ZTV11btRigidBody = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI11btRigidBody to i8*), i8* bitcast (i1 (%class.btRigidBody*, %class.btCollisionObject*)* @_ZNK11btRigidBody24checkCollideWithOverrideEPK17btCollisionObject to i8*), i8* bitcast (%class.btRigidBody* (%class.btRigidBody*)* @_ZN11btRigidBodyD2Ev to i8*), i8* bitcast (void (%class.btRigidBody*)* @_ZN11btRigidBodyD0Ev to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btCollisionShape*)* @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape to i8*), i8* bitcast (i32 (%class.btRigidBody*)* @_ZNK11btRigidBody28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btRigidBody*, i8*, %class.btSerializer*)* @_ZNK11btRigidBody9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btRigidBody*, %class.btSerializer*)* @_ZNK11btRigidBody21serializeSingleObjectEP12btSerializer to i8*)] }, align 4
@_ZL8uniqueId = internal global i32 0, align 4
@.str = private unnamed_addr constant [21 x i8] c"btRigidBodyFloatData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS11btRigidBody = hidden constant [14 x i8] c"11btRigidBody\00", align 1
@_ZTI17btCollisionObject = external constant i8*
@_ZTI11btRigidBody = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @_ZTS11btRigidBody, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btCollisionObject to i8*) }, align 4

@_ZN11btRigidBodyC1ERKNS_27btRigidBodyConstructionInfoE = hidden unnamed_addr alias %class.btRigidBody* (%class.btRigidBody*, %"struct.btRigidBody::btRigidBodyConstructionInfo"*), %class.btRigidBody* (%class.btRigidBody*, %"struct.btRigidBody::btRigidBodyConstructionInfo"*)* @_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE
@_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3 = hidden unnamed_addr alias %class.btRigidBody* (%class.btRigidBody*, float, %class.btMotionState*, %class.btCollisionShape*, %class.btVector3*), %class.btRigidBody* (%class.btRigidBody*, float, %class.btMotionState*, %class.btCollisionShape*, %class.btVector3*)* @_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3

define hidden %class.btRigidBody* @_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* returned %this, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(140) %constructionInfo) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %constructionInfo.addr = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo"*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %"struct.btRigidBody::btRigidBodyConstructionInfo"* %constructionInfo, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* %0)
  %1 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_invInertiaTensorWorld)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearVelocity)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularVelocity)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearFactor)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity)
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity_acceleration)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invInertiaLocal)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalForce)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalTorque)
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call11 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray* %m_constraintRefs)
  %m_deltaLinearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 24
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 25
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaAngularVelocity)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularFactor)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invMass)
  %m_pushVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 28
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pushVelocity)
  %m_turnVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 29
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_turnVelocity)
  %2 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  call void @_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* %this1, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(140) %2)
  ret %class.btRigidBody* %this1
}

declare %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* returned) unnamed_addr #1

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define hidden void @_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* %this, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(140) %constructionInfo) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %constructionInfo.addr = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo"*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp42 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %"struct.btRigidBody::btRigidBodyConstructionInfo"* %constructionInfo, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 20
  store i32 2, i32* %m_internalType, align 4, !tbaa !8
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !15
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !15
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !15
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !15
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  store float 1.000000e+00, float* %ref.tmp7, align 4, !tbaa !15
  %14 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  store float 1.000000e+00, float* %ref.tmp8, align 4, !tbaa !15
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularFactor, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %16 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  %18 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %19 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  store float 1.000000e+00, float* %ref.tmp10, align 4, !tbaa !15
  %20 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store float 1.000000e+00, float* %ref.tmp11, align 4, !tbaa !15
  %21 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  store float 1.000000e+00, float* %ref.tmp12, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearFactor, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %22 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %25 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !15
  %26 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #9
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !15
  %27 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %28 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %31 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !15
  %32 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !15
  %33 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_gravity_acceleration, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %34 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %37 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !15
  %38 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  store float 0.000000e+00, float* %ref.tmp20, align 4, !tbaa !15
  %39 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  store float 0.000000e+00, float* %ref.tmp21, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalForce, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %40 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %43 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  store float 0.000000e+00, float* %ref.tmp22, align 4, !tbaa !15
  %44 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !15
  %45 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #9
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalTorque, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %46 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_linearDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %46, i32 0, i32 5
  %47 = load float, float* %m_linearDamping, align 4, !tbaa !16
  %48 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_angularDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %48, i32 0, i32 6
  %49 = load float, float* %m_angularDamping, align 4, !tbaa !19
  call void @_ZN11btRigidBody10setDampingEff(%class.btRigidBody* %this1, float %47, float %49)
  %50 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #9
  %51 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #9
  %52 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #9
  %53 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_linearSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %53, i32 0, i32 10
  %54 = load float, float* %m_linearSleepingThreshold, align 4, !tbaa !20
  %m_linearSleepingThreshold25 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 18
  store float %54, float* %m_linearSleepingThreshold25, align 4, !tbaa !21
  %55 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_angularSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %55, i32 0, i32 11
  %56 = load float, float* %m_angularSleepingThreshold, align 4, !tbaa !25
  %m_angularSleepingThreshold26 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 19
  store float %56, float* %m_angularSleepingThreshold26, align 4, !tbaa !26
  %57 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_motionState = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %57, i32 0, i32 1
  %58 = load %class.btMotionState*, %class.btMotionState** %m_motionState, align 4, !tbaa !27
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  store %class.btMotionState* %58, %class.btMotionState** %m_optionalMotionState, align 4, !tbaa !28
  %m_contactSolverType = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 30
  store i32 0, i32* %m_contactSolverType, align 4, !tbaa !29
  %m_frictionSolverType = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 31
  store i32 0, i32* %m_frictionSolverType, align 4, !tbaa !30
  %59 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_additionalDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %59, i32 0, i32 12
  %60 = load i8, i8* %m_additionalDamping, align 4, !tbaa !31, !range !32
  %tobool = trunc i8 %60 to i1
  %m_additionalDamping27 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 13
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %m_additionalDamping27, align 4, !tbaa !33
  %61 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_additionalDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %61, i32 0, i32 13
  %62 = load float, float* %m_additionalDampingFactor, align 4, !tbaa !34
  %m_additionalDampingFactor28 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  store float %62, float* %m_additionalDampingFactor28, align 4, !tbaa !35
  %63 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %63, i32 0, i32 14
  %64 = load float, float* %m_additionalLinearDampingThresholdSqr, align 4, !tbaa !36
  %m_additionalLinearDampingThresholdSqr29 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 15
  store float %64, float* %m_additionalLinearDampingThresholdSqr29, align 4, !tbaa !37
  %65 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %65, i32 0, i32 15
  %66 = load float, float* %m_additionalAngularDampingThresholdSqr, align 4, !tbaa !38
  %m_additionalAngularDampingThresholdSqr30 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 16
  store float %66, float* %m_additionalAngularDampingThresholdSqr30, align 4, !tbaa !39
  %67 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_additionalAngularDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %67, i32 0, i32 16
  %68 = load float, float* %m_additionalAngularDampingFactor, align 4, !tbaa !40
  %m_additionalAngularDampingFactor31 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 17
  store float %68, float* %m_additionalAngularDampingFactor31, align 4, !tbaa !41
  %m_optionalMotionState32 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %69 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState32, align 4, !tbaa !28
  %tobool33 = icmp ne %class.btMotionState* %69, null
  br i1 %tobool33, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_optionalMotionState34 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %70 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState34, align 4, !tbaa !28
  %71 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %71, i32 0, i32 1
  %72 = bitcast %class.btMotionState* %70 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %72, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 2
  %73 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %73(%class.btMotionState* %70, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform)
  br label %if.end

if.else:                                          ; preds = %entry
  %74 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_startWorldTransform = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %74, i32 0, i32 2
  %75 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform35 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %75, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform35, %class.btTransform* nonnull align 4 dereferenceable(64) %m_startWorldTransform)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %76 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform36 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %76, i32 0, i32 1
  %77 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %77, i32 0, i32 2
  %call37 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform36)
  %78 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %78, i32 0, i32 3
  %79 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #9
  store float 0.000000e+00, float* %ref.tmp38, align 4, !tbaa !15
  %80 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #9
  store float 0.000000e+00, float* %ref.tmp39, align 4, !tbaa !15
  %81 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #9
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_interpolationLinearVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %82 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #9
  %83 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #9
  %84 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #9
  %85 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %85, i32 0, i32 4
  %86 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #9
  store float 0.000000e+00, float* %ref.tmp41, align 4, !tbaa !15
  %87 = bitcast float* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #9
  store float 0.000000e+00, float* %ref.tmp42, align 4, !tbaa !15
  %88 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #9
  store float 0.000000e+00, float* %ref.tmp43, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_interpolationAngularVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp41, float* nonnull align 4 dereferenceable(4) %ref.tmp42, float* nonnull align 4 dereferenceable(4) %ref.tmp43)
  %89 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #9
  %90 = bitcast float* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #9
  %91 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  %92 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_friction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %92, i32 0, i32 7
  %93 = load float, float* %m_friction, align 4, !tbaa !42
  %94 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_friction44 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %94, i32 0, i32 17
  store float %93, float* %m_friction44, align 4, !tbaa !43
  %95 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_rollingFriction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %95, i32 0, i32 8
  %96 = load float, float* %m_rollingFriction, align 4, !tbaa !44
  %97 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_rollingFriction45 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %97, i32 0, i32 19
  store float %96, float* %m_rollingFriction45, align 4, !tbaa !45
  %98 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_restitution = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %98, i32 0, i32 9
  %99 = load float, float* %m_restitution, align 4, !tbaa !46
  %100 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_restitution46 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %100, i32 0, i32 18
  store float %99, float* %m_restitution46, align 4, !tbaa !47
  %101 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %102 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_collisionShape = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %102, i32 0, i32 3
  %103 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !48
  %104 = bitcast %class.btCollisionObject* %101 to void (%class.btCollisionObject*, %class.btCollisionShape*)***
  %vtable47 = load void (%class.btCollisionObject*, %class.btCollisionShape*)**, void (%class.btCollisionObject*, %class.btCollisionShape*)*** %104, align 4, !tbaa !6
  %vfn48 = getelementptr inbounds void (%class.btCollisionObject*, %class.btCollisionShape*)*, void (%class.btCollisionObject*, %class.btCollisionShape*)** %vtable47, i64 3
  %105 = load void (%class.btCollisionObject*, %class.btCollisionShape*)*, void (%class.btCollisionObject*, %class.btCollisionShape*)** %vfn48, align 4
  call void %105(%class.btCollisionObject* %101, %class.btCollisionShape* %103)
  %106 = load i32, i32* @_ZL8uniqueId, align 4, !tbaa !49
  %inc = add nsw i32 %106, 1
  store i32 %inc, i32* @_ZL8uniqueId, align 4, !tbaa !49
  %m_debugBodyId = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 23
  store i32 %106, i32* %m_debugBodyId, align 4, !tbaa !50
  %107 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_mass = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %107, i32 0, i32 0
  %108 = load float, float* %m_mass, align 4, !tbaa !51
  %109 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4, !tbaa !2
  %m_localInertia = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %109, i32 0, i32 4
  call void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody* %this1, float %108, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localInertia)
  call void @_ZN11btRigidBody19updateInertiaTensorEv(%class.btRigidBody* %this1)
  %m_rigidbodyFlags = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 22
  store i32 0, i32* %m_rigidbodyFlags, align 4, !tbaa !52
  %m_deltaLinearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 24
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 25
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_deltaAngularVelocity)
  %110 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %110) #9
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %m_linearFactor50 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, float* nonnull align 4 dereferenceable(4) %m_inverseMass, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor50)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %111 = bitcast %class.btVector3* %m_invMass to i8*
  %112 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %111, i8* align 4 %112, i32 16, i1 false), !tbaa.struct !53
  %113 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %113) #9
  %m_pushVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 28
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_pushVelocity)
  %m_turnVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 29
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_turnVelocity)
  ret void
}

define hidden %class.btRigidBody* @_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* returned %this, float %mass, %class.btMotionState* %motionState, %class.btCollisionShape* %collisionShape, %class.btVector3* nonnull align 4 dereferenceable(16) %localInertia) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %mass.addr = alloca float, align 4
  %motionState.addr = alloca %class.btMotionState*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  %localInertia.addr = alloca %class.btVector3*, align 4
  %cinfo = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo", align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !15
  store %class.btMotionState* %motionState, %class.btMotionState** %motionState.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  store %class.btVector3* %localInertia, %class.btVector3** %localInertia.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* %0)
  %1 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_invInertiaTensorWorld)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearVelocity)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularVelocity)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearFactor)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity)
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity_acceleration)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invInertiaLocal)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalForce)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalTorque)
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call11 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray* %m_constraintRefs)
  %m_deltaLinearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 24
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 25
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaAngularVelocity)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularFactor)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invMass)
  %m_pushVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 28
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pushVelocity)
  %m_turnVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 29
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_turnVelocity)
  %2 = bitcast %"struct.btRigidBody::btRigidBodyConstructionInfo"* %cinfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 140, i8* %2) #9
  %3 = load float, float* %mass.addr, align 4, !tbaa !15
  %4 = load %class.btMotionState*, %class.btMotionState** %motionState.addr, align 4, !tbaa !2
  %5 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %localInertia.addr, align 4, !tbaa !2
  %call18 = call %"struct.btRigidBody::btRigidBodyConstructionInfo"* @_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%"struct.btRigidBody::btRigidBodyConstructionInfo"* %cinfo, float %3, %class.btMotionState* %4, %class.btCollisionShape* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  call void @_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* %this1, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(140) %cinfo)
  %7 = bitcast %"struct.btRigidBody::btRigidBodyConstructionInfo"* %cinfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 140, i8* %7) #9
  ret %class.btRigidBody* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

define linkonce_odr hidden %"struct.btRigidBody::btRigidBodyConstructionInfo"* @_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%"struct.btRigidBody::btRigidBodyConstructionInfo"* returned %this, float %mass, %class.btMotionState* %motionState, %class.btCollisionShape* %collisionShape, %class.btVector3* nonnull align 4 dereferenceable(16) %localInertia) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo"*, align 4
  %mass.addr = alloca float, align 4
  %motionState.addr = alloca %class.btMotionState*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  %localInertia.addr = alloca %class.btVector3*, align 4
  store %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !15
  store %class.btMotionState* %motionState, %class.btMotionState** %motionState.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  store %class.btVector3* %localInertia, %class.btVector3** %localInertia.addr, align 4, !tbaa !2
  %this1 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %this.addr, align 4
  %m_mass = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 0
  %0 = load float, float* %mass.addr, align 4, !tbaa !15
  store float %0, float* %m_mass, align 4, !tbaa !51
  %m_motionState = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 1
  %1 = load %class.btMotionState*, %class.btMotionState** %motionState.addr, align 4, !tbaa !2
  store %class.btMotionState* %1, %class.btMotionState** %m_motionState, align 4, !tbaa !27
  %m_startWorldTransform = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 2
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_startWorldTransform)
  %m_collisionShape = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 3
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !48
  %m_localInertia = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %localInertia.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %m_localInertia to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !53
  %m_linearDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_linearDamping, align 4, !tbaa !16
  %m_angularDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_angularDamping, align 4, !tbaa !19
  %m_friction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 7
  store float 5.000000e-01, float* %m_friction, align 4, !tbaa !42
  %m_rollingFriction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_rollingFriction, align 4, !tbaa !44
  %m_restitution = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_restitution, align 4, !tbaa !46
  %m_linearSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 10
  store float 0x3FE99999A0000000, float* %m_linearSleepingThreshold, align 4, !tbaa !20
  %m_angularSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 11
  store float 1.000000e+00, float* %m_angularSleepingThreshold, align 4, !tbaa !25
  %m_additionalDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 12
  store i8 0, i8* %m_additionalDamping, align 4, !tbaa !31
  %m_additionalDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 13
  store float 0x3F747AE140000000, float* %m_additionalDampingFactor, align 4, !tbaa !34
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 14
  store float 0x3F847AE140000000, float* %m_additionalLinearDampingThresholdSqr, align 4, !tbaa !36
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 15
  store float 0x3F847AE140000000, float* %m_additionalAngularDampingThresholdSqr, align 4, !tbaa !38
  %m_additionalAngularDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 16
  store float 0x3F847AE140000000, float* %m_additionalAngularDampingFactor, align 4, !tbaa !40
  %m_startWorldTransform2 = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 2
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_startWorldTransform2)
  ret %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !15
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !15
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !15
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !15
  ret void
}

define hidden void @_ZN11btRigidBody10setDampingEff(%class.btRigidBody* %this, float %lin_damping, float %ang_damping) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %lin_damping.addr = alloca float, align 4
  %ang_damping.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %lin_damping, float* %lin_damping.addr, align 4, !tbaa !15
  store float %ang_damping, float* %ang_damping.addr, align 4, !tbaa !15
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !15
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !15
  %call = call nonnull align 4 dereferenceable(4) float* @_Z9btClampedIfERKT_S2_S2_S2_(float* nonnull align 4 dereferenceable(4) %lin_damping.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %2 = load float, float* %call, align 4, !tbaa !15
  %m_linearDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  store float %2, float* %m_linearDamping, align 4, !tbaa !55
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !15
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 1.000000e+00, float* %ref.tmp4, align 4, !tbaa !15
  %call5 = call nonnull align 4 dereferenceable(4) float* @_Z9btClampedIfERKT_S2_S2_S2_(float* nonnull align 4 dereferenceable(4) %ang_damping.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %7 = load float, float* %call5, align 4, !tbaa !15
  %m_angularDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  store float %7, float* %m_angularDamping, align 4, !tbaa !56
  %8 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !53
  ret %class.btTransform* %this1
}

define hidden void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !15
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load float, float* %mass.addr, align 4, !tbaa !15
  %cmp = fcmp oeq float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %1, i32 0, i32 12
  %2 = load i32, i32* %m_collisionFlags, align 4, !tbaa !57
  %or = or i32 %2, 1
  store i32 %or, i32* %m_collisionFlags, align 4, !tbaa !57
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_inverseMass, align 4, !tbaa !58
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionFlags2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %3, i32 0, i32 12
  %4 = load i32, i32* %m_collisionFlags2, align 4, !tbaa !57
  %and = and i32 %4, -2
  store i32 %and, i32* %m_collisionFlags2, align 4, !tbaa !57
  %5 = load float, float* %mass.addr, align 4, !tbaa !15
  %div = fdiv float 1.000000e+00, %5
  %m_inverseMass3 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  store float %div, float* %m_inverseMass3, align 4, !tbaa !58
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #9
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %mass.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity_acceleration)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %7 = bitcast %class.btVector3* %m_gravity to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !53
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #9
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %10 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %11)
  %12 = load float, float* %call, align 4, !tbaa !15
  %cmp5 = fcmp une float %12, 0.000000e+00
  br i1 %cmp5, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %13 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4, !tbaa !15
  %div7 = fdiv float 1.000000e+00, %14
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div7, %cond.true ], [ 0.000000e+00, %cond.false ]
  store float %cond, float* %ref.tmp4, align 4, !tbaa !15
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %16)
  %17 = load float, float* %call9, align 4, !tbaa !15
  %cmp10 = fcmp une float %17, 0.000000e+00
  br i1 %cmp10, label %cond.true11, label %cond.false14

cond.true11:                                      ; preds = %cond.end
  %18 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call12, align 4, !tbaa !15
  %div13 = fdiv float 1.000000e+00, %19
  br label %cond.end15

cond.false14:                                     ; preds = %cond.end
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false14, %cond.true11
  %cond16 = phi float [ %div13, %cond.true11 ], [ 0.000000e+00, %cond.false14 ]
  store float %cond16, float* %ref.tmp8, align 4, !tbaa !15
  %20 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %21 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %21)
  %22 = load float, float* %call18, align 4, !tbaa !15
  %cmp19 = fcmp une float %22, 0.000000e+00
  br i1 %cmp19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end15
  %23 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %23)
  %24 = load float, float* %call21, align 4, !tbaa !15
  %div22 = fdiv float 1.000000e+00, %24
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end15
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi float [ %div22, %cond.true20 ], [ 0.000000e+00, %cond.false23 ]
  store float %cond25, float* %ref.tmp17, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_invInertiaLocal, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %25 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %28 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #9
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %m_inverseMass27 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor, float* nonnull align 4 dereferenceable(4) %m_inverseMass27)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %29 = bitcast %class.btVector3* %m_invMass to i8*
  %30 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !53
  %31 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #9
  ret void
}

define hidden void @_ZN11btRigidBody19updateInertiaTensorEv(%class.btRigidBody* %this) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp2 = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btMatrix3x3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #9
  %1 = bitcast %class.btMatrix3x3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #9
  %2 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %2, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  call void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* sret align 4 %ref.tmp2, %class.btMatrix3x3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %m_invInertiaLocal)
  %3 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %3) #9
  %4 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform4 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %4, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform4)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp3, %class.btMatrix3x3* %call5)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp3)
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_invInertiaTensorWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %5 = bitcast %class.btMatrix3x3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %5) #9
  %6 = bitcast %class.btMatrix3x3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #9
  %7 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %7) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !15
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !15
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

define hidden void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %this, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !15
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %1 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %2 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, float %1, %class.btTransform* nonnull align 4 dereferenceable(64) %2)
  ret void
}

define linkonce_odr hidden void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %curTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %linvel, %class.btVector3* nonnull align 4 dereferenceable(16) %angvel, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #0 comdat {
entry:
  %curTrans.addr = alloca %class.btTransform*, align 4
  %linvel.addr = alloca %class.btVector3*, align 4
  %angvel.addr = alloca %class.btVector3*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %fAngle = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca float, align 4
  %orn0 = alloca %class.btQuaternion, align 4
  %predictedOrn = alloca %class.btQuaternion, align 4
  store %class.btTransform* %curTrans, %class.btTransform** %curTrans.addr, align 4, !tbaa !2
  store %class.btVector3* %linvel, %class.btVector3** %linvel.addr, align 4, !tbaa !2
  store %class.btVector3* %angvel, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !15
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  %0 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %linvel.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %4, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  %7 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %8 = bitcast float* %fAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %9)
  store float %call3, float* %fAngle, align 4, !tbaa !15
  %10 = load float, float* %fAngle, align 4, !tbaa !15
  %11 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %mul = fmul float %10, %11
  %cmp = fcmp ogt float %mul, 0x3FE921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %div = fdiv float 0x3FE921FB60000000, %12
  store float %div, float* %fAngle, align 4, !tbaa !15
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load float, float* %fAngle, align 4, !tbaa !15
  %cmp4 = fcmp olt float %13, 0x3F50624DE0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %14 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #9
  %15 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  %16 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %mul8 = fmul float 5.000000e-01, %17
  %18 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %19 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %mul9 = fmul float %18, %19
  %20 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %mul10 = fmul float %mul9, %20
  %mul11 = fmul float %mul10, 0x3F95555560000000
  %21 = load float, float* %fAngle, align 4, !tbaa !15
  %mul12 = fmul float %mul11, %21
  %22 = load float, float* %fAngle, align 4, !tbaa !15
  %mul13 = fmul float %mul12, %22
  %sub = fsub float %mul8, %mul13
  store float %sub, float* %ref.tmp7, align 4, !tbaa !15
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %15, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %23 = bitcast %class.btVector3* %axis to i8*
  %24 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !53
  %25 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #9
  br label %if.end20

if.else:                                          ; preds = %if.end
  %27 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #9
  %28 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4, !tbaa !2
  %29 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load float, float* %fAngle, align 4, !tbaa !15
  %mul16 = fmul float 5.000000e-01, %30
  %31 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %mul17 = fmul float %mul16, %31
  %call18 = call float @_Z5btSinf(float %mul17)
  %32 = load float, float* %fAngle, align 4, !tbaa !15
  %div19 = fdiv float %call18, %32
  store float %div19, float* %ref.tmp15, align 4, !tbaa !15
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %28, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %33 = bitcast %class.btVector3* %axis to i8*
  %34 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false), !tbaa.struct !53
  %35 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #9
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then5
  %37 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #9
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %axis)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %axis)
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %axis)
  %38 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  %39 = load float, float* %fAngle, align 4, !tbaa !15
  %40 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %mul25 = fmul float %39, %40
  %mul26 = fmul float %mul25, 5.000000e-01
  %call27 = call float @_Z5btCosf(float %mul26)
  store float %call27, float* %ref.tmp24, align 4, !tbaa !15
  %call28 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %dorn, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %41 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast %class.btQuaternion* %orn0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #9
  %43 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %orn0, %class.btTransform* %43)
  %44 = bitcast %class.btQuaternion* %predictedOrn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #9
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %predictedOrn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %predictedOrn)
  %45 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4, !tbaa !2
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %45, %class.btQuaternion* nonnull align 4 dereferenceable(16) %predictedOrn)
  %46 = bitcast %class.btQuaternion* %predictedOrn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #9
  %47 = bitcast %class.btQuaternion* %orn0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #9
  %48 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #9
  %49 = bitcast float* %fAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #9
  %50 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #9
  ret void
}

define hidden void @_ZN11btRigidBody18saveKinematicStateEf(%class.btRigidBody* %this, float %timeStep) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel = alloca %class.btVector3, align 4
  %angVel = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !15
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end12

if.then:                                          ; preds = %entry
  %call = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this1)
  %tobool = icmp ne %class.btMotionState* %call, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %call3 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this1)
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %1, i32 0, i32 1
  %2 = bitcast %class.btMotionState* %call3 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 2
  %3 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %3(%class.btMotionState* %call3, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %4 = bitcast %class.btVector3* %linVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVel)
  %5 = bitcast %class.btVector3* %angVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVel)
  %6 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %6, i32 0, i32 2
  %7 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform6 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %7, i32 0, i32 1
  %8 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %m_interpolationWorldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform6, float %8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity)
  %m_linearVelocity7 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %9 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %9, i32 0, i32 3
  %10 = bitcast %class.btVector3* %m_interpolationLinearVelocity to i8*
  %11 = bitcast %class.btVector3* %m_linearVelocity7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !53
  %m_angularVelocity8 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %12 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %12, i32 0, i32 4
  %13 = bitcast %class.btVector3* %m_interpolationAngularVelocity to i8*
  %14 = bitcast %class.btVector3* %m_angularVelocity8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !53
  %15 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform9 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %15, i32 0, i32 1
  %16 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform10 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %16, i32 0, i32 2
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform10, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform9)
  %17 = bitcast %class.btVector3* %angVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  %18 = bitcast %class.btVector3* %linVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #9
  br label %if.end12

if.end12:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %0 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState, align 4, !tbaa !28
  ret %class.btMotionState* %0
}

define linkonce_odr hidden void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, float %timeStep, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #0 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel.addr = alloca %class.btVector3*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !15
  store %class.btVector3* %linVel, %class.btVector3** %linVel.addr, align 4, !tbaa !2
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %4 = load %class.btVector3*, %class.btVector3** %linVel.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !53
  %7 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #9
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %10 = bitcast float* %angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %12 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  call void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %11, %class.btTransform* nonnull align 4 dereferenceable(64) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  %13 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #9
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #9
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %15 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector3* %15 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !53
  %18 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #9
  %19 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #9
  %20 = bitcast float* %angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #9
  ret void
}

define hidden void @_ZNK11btRigidBody7getAabbER9btVector3S1_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %call = call %class.btCollisionShape* @_ZNK11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %this1)
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = bitcast %class.btCollisionShape* %call to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %4 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btCollisionShape* %call, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 9
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !59
  ret %class.btCollisionShape* %1
}

define hidden void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %acceleration) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %acceleration.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %acceleration, %class.btVector3** %acceleration.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !58
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btVector3*, %class.btVector3** %acceleration.addr, align 4, !tbaa !2
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_inverseMass3 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %4 = load float, float* %m_inverseMass3, align 4, !tbaa !58
  %div = fdiv float 1.000000e+00, %4
  store float %div, float* %ref.tmp2, align 4, !tbaa !15
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %5 = bitcast %class.btVector3* %m_gravity to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !53
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %class.btVector3*, %class.btVector3** %acceleration.addr, align 4, !tbaa !2
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %10 = bitcast %class.btVector3* %m_gravity_acceleration to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !53
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !15
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !15
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !15
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !15
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !15
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !15
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !15
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !15
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !15
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z9btClampedIfERKT_S2_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %lb, float* nonnull align 4 dereferenceable(4) %ub) #2 comdat {
entry:
  %a.addr = alloca float*, align 4
  %lb.addr = alloca float*, align 4
  %ub.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %lb, float** %lb.addr, align 4, !tbaa !2
  store float* %ub, float** %ub.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %2 = load float*, float** %lb.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %lb.addr, align 4, !tbaa !2
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %ub.addr, align 4, !tbaa !2
  %6 = load float, float* %5, align 4, !tbaa !15
  %7 = load float*, float** %a.addr, align 4, !tbaa !2
  %8 = load float, float* %7, align 4, !tbaa !15
  %cmp1 = fcmp olt float %6, %8
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %9 = load float*, float** %ub.addr, align 4, !tbaa !2
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %10 = load float*, float** %a.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond-lvalue = phi float* [ %9, %cond.true2 ], [ %10, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond-lvalue5 = phi float* [ %4, %cond.true ], [ %cond-lvalue, %cond.end ]
  ret float* %cond-lvalue5
}

define hidden void @_ZN11btRigidBody12applyDampingEf(%class.btRigidBody* %this, float %timeStep) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %timeStep.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %speed = alloca float, align 4
  %dampVel = alloca float, align 4
  %dir = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %angSpeed = alloca float, align 4
  %angDampVel = alloca float, align 4
  %dir42 = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !15
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_linearDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  %1 = load float, float* %m_linearDamping, align 4, !tbaa !55
  %sub = fsub float 1.000000e+00, %1
  %2 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %call = call float @_Z5btPowff(float %sub, float %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !15
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_linearVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %m_angularDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  %5 = load float, float* %m_angularDamping, align 4, !tbaa !56
  %sub4 = fsub float 1.000000e+00, %5
  %6 = load float, float* %timeStep.addr, align 4, !tbaa !15
  %call5 = call float @_Z5btPowff(float %sub4, float %6)
  store float %call5, float* %ref.tmp3, align 4, !tbaa !15
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_angularVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %m_additionalDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 13
  %8 = load i8, i8* %m_additionalDamping, align 4, !tbaa !33, !range !32
  %tobool = trunc i8 %8 to i1
  br i1 %tobool, label %if.then, label %if.end54

if.then:                                          ; preds = %entry
  %m_angularVelocity7 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call8 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_angularVelocity7)
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 16
  %9 = load float, float* %m_additionalAngularDampingThresholdSqr, align 4, !tbaa !39
  %cmp = fcmp olt float %call8, %9
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %m_linearVelocity9 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call10 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_linearVelocity9)
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 15
  %10 = load float, float* %m_additionalLinearDampingThresholdSqr, align 4, !tbaa !37
  %cmp11 = fcmp olt float %call10, %10
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %land.lhs.true
  %m_additionalDampingFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  %m_angularVelocity13 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_angularVelocity13, float* nonnull align 4 dereferenceable(4) %m_additionalDampingFactor)
  %m_additionalDampingFactor15 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  %m_linearVelocity16 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_linearVelocity16, float* nonnull align 4 dereferenceable(4) %m_additionalDampingFactor15)
  br label %if.end

if.end:                                           ; preds = %if.then12, %land.lhs.true, %if.then
  %11 = bitcast float* %speed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_linearVelocity18 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call19 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_linearVelocity18)
  store float %call19, float* %speed, align 4, !tbaa !15
  %12 = load float, float* %speed, align 4, !tbaa !15
  %m_linearDamping20 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  %13 = load float, float* %m_linearDamping20, align 4, !tbaa !55
  %cmp21 = fcmp olt float %12, %13
  br i1 %cmp21, label %if.then22, label %if.end34

if.then22:                                        ; preds = %if.end
  %14 = bitcast float* %dampVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  store float 0x3F747AE140000000, float* %dampVel, align 4, !tbaa !15
  %15 = load float, float* %speed, align 4, !tbaa !15
  %16 = load float, float* %dampVel, align 4, !tbaa !15
  %cmp23 = fcmp ogt float %15, %16
  br i1 %cmp23, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then22
  %17 = bitcast %class.btVector3* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #9
  %m_linearVelocity25 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %dir, %class.btVector3* %m_linearVelocity25)
  %18 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #9
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, float* nonnull align 4 dereferenceable(4) %dampVel)
  %m_linearVelocity27 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_linearVelocity27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26)
  %19 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #9
  %20 = bitcast %class.btVector3* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #9
  br label %if.end33

if.else:                                          ; preds = %if.then22
  %m_linearVelocity29 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %21 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  store float 0.000000e+00, float* %ref.tmp30, align 4, !tbaa !15
  %22 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  store float 0.000000e+00, float* %ref.tmp31, align 4, !tbaa !15
  %23 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  store float 0.000000e+00, float* %ref.tmp32, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearVelocity29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %24 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  br label %if.end33

if.end33:                                         ; preds = %if.else, %if.then24
  %27 = bitcast float* %dampVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.end
  %28 = bitcast float* %angSpeed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  %m_angularVelocity35 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call36 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_angularVelocity35)
  store float %call36, float* %angSpeed, align 4, !tbaa !15
  %29 = load float, float* %angSpeed, align 4, !tbaa !15
  %m_angularDamping37 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  %30 = load float, float* %m_angularDamping37, align 4, !tbaa !56
  %cmp38 = fcmp olt float %29, %30
  br i1 %cmp38, label %if.then39, label %if.end53

if.then39:                                        ; preds = %if.end34
  %31 = bitcast float* %angDampVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 0x3F747AE140000000, float* %angDampVel, align 4, !tbaa !15
  %32 = load float, float* %angSpeed, align 4, !tbaa !15
  %33 = load float, float* %angDampVel, align 4, !tbaa !15
  %cmp40 = fcmp ogt float %32, %33
  br i1 %cmp40, label %if.then41, label %if.else47

if.then41:                                        ; preds = %if.then39
  %34 = bitcast %class.btVector3* %dir42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #9
  %m_angularVelocity43 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %dir42, %class.btVector3* %m_angularVelocity43)
  %35 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #9
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp44, %class.btVector3* nonnull align 4 dereferenceable(16) %dir42, float* nonnull align 4 dereferenceable(4) %angDampVel)
  %m_angularVelocity45 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_angularVelocity45, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp44)
  %36 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #9
  %37 = bitcast %class.btVector3* %dir42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #9
  br label %if.end52

if.else47:                                        ; preds = %if.then39
  %m_angularVelocity48 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %38 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  store float 0.000000e+00, float* %ref.tmp49, align 4, !tbaa !15
  %39 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  store float 0.000000e+00, float* %ref.tmp50, align 4, !tbaa !15
  %40 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #9
  store float 0.000000e+00, float* %ref.tmp51, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularVelocity48, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  %41 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  %43 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #9
  br label %if.end52

if.end52:                                         ; preds = %if.else47, %if.then41
  %44 = bitcast float* %angDampVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #9
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end34
  %45 = bitcast float* %angSpeed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #9
  %46 = bitcast float* %speed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #9
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btPowff(float %x, float %y) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !15
  store float %y, float* %y.addr, align 4, !tbaa !15
  %0 = load float, float* %x.addr, align 4, !tbaa !15
  %1 = load float, float* %y.addr, align 4, !tbaa !15
  %2 = call float @llvm.pow.f32(float %0, float %1)
  ret float %2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !15
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !15
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !15
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !15
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !15
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !15
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !15
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !53
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !53
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !15
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !15
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !15
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !15
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !15
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !15
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !15
  ret %class.btVector3* %this1
}

define hidden void @_ZN11btRigidBody12applyGravityEv(%class.btRigidBody* %this) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  call void @_ZN11btRigidBody17applyCentralForceERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !57
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

define linkonce_odr hidden void @_ZN11btRigidBody17applyCentralForceERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %force) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %force.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %force, %class.btVector3** %force.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %force.addr, align 4, !tbaa !2
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_totalForce, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

define hidden void @_ZN11btRigidBody18proceedToTransformERK11btTransform(%class.btRigidBody* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %newTrans) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %newTrans.addr = alloca %class.btTransform*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %newTrans, %class.btTransform** %newTrans.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %newTrans.addr, align 4, !tbaa !2
  call void @_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform(%class.btRigidBody* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  ret void
}

define hidden void @_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform(%class.btRigidBody* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %xform) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %xform.addr = alloca %class.btTransform*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %xform, %class.btTransform** %xform.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %1, i32 0, i32 1
  %2 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %2, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4, !tbaa !2
  %4 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform3 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %4, i32 0, i32 2
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform3, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this1)
  %5 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %5, i32 0, i32 3
  %6 = bitcast %class.btVector3* %m_interpolationLinearVelocity to i8*
  %7 = bitcast %class.btVector3* %call5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !53
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  %8 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %8, i32 0, i32 4
  %9 = bitcast %class.btVector3* %m_interpolationAngularVelocity to i8*
  %10 = bitcast %class.btVector3* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !53
  %11 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4, !tbaa !2
  %12 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform7 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %12, i32 0, i32 1
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform7, %class.btTransform* nonnull align 4 dereferenceable(64) %11)
  call void @_ZN11btRigidBody19updateInertiaTensorEv(%class.btRigidBody* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !15
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !15
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !15
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !15
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !15
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !15
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !15
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !15
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !15
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

define linkonce_odr hidden void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %s) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %s.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %s, %class.btVector3** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !15
  %3 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call2, align 4, !tbaa !15
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !15
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %6 = load float, float* %call6, align 4, !tbaa !15
  %7 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %7)
  %8 = load float, float* %call7, align 4, !tbaa !15
  %mul8 = fmul float %6, %8
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !15
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 0
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx11)
  %10 = load float, float* %call12, align 4, !tbaa !15
  %11 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %12 = load float, float* %call13, align 4, !tbaa !15
  %mul14 = fmul float %10, %12
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !15
  %13 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %m_el16 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el16, i32 0, i32 1
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx17)
  %14 = load float, float* %call18, align 4, !tbaa !15
  %15 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %15)
  %16 = load float, float* %call19, align 4, !tbaa !15
  %mul20 = fmul float %14, %16
  store float %mul20, float* %ref.tmp15, align 4, !tbaa !15
  %17 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %m_el22 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el22, i32 0, i32 1
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx23)
  %18 = load float, float* %call24, align 4, !tbaa !15
  %19 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %19)
  %20 = load float, float* %call25, align 4, !tbaa !15
  %mul26 = fmul float %18, %20
  store float %mul26, float* %ref.tmp21, align 4, !tbaa !15
  %21 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 1
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx29)
  %22 = load float, float* %call30, align 4, !tbaa !15
  %23 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %23)
  %24 = load float, float* %call31, align 4, !tbaa !15
  %mul32 = fmul float %22, %24
  store float %mul32, float* %ref.tmp27, align 4, !tbaa !15
  %25 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx35)
  %26 = load float, float* %call36, align 4, !tbaa !15
  %27 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %27)
  %28 = load float, float* %call37, align 4, !tbaa !15
  %mul38 = fmul float %26, %28
  store float %mul38, float* %ref.tmp33, align 4, !tbaa !15
  %29 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %m_el40 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx41 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el40, i32 0, i32 2
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx41)
  %30 = load float, float* %call42, align 4, !tbaa !15
  %31 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %31)
  %32 = load float, float* %call43, align 4, !tbaa !15
  %mul44 = fmul float %30, %32
  store float %mul44, float* %ref.tmp39, align 4, !tbaa !15
  %33 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %m_el46 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx47 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el46, i32 0, i32 2
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx47)
  %34 = load float, float* %call48, align 4, !tbaa !15
  %35 = load %class.btVector3*, %class.btVector3** %s.addr, align 4, !tbaa !2
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %35)
  %36 = load float, float* %call49, align 4, !tbaa !15
  %mul50 = fmul float %34, %36
  store float %mul50, float* %ref.tmp45, align 4, !tbaa !15
  %call51 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %37 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  %39 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  %40 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  %43 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #9
  %44 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #9
  %45 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !53
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !53
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !53
  ret %class.btMatrix3x3* %this1
}

define hidden void @_ZNK11btRigidBody22computeGyroscopicForceEf(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, float %maxGyroscopicForce) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %maxGyroscopicForce.addr = alloca float, align 4
  %inertiaLocal = alloca %class.btVector3, align 4
  %inertiaTensorWorld = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp20 = alloca %class.btMatrix3x3, align 4
  %tmp = alloca %class.btVector3, align 4
  %l2 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %maxGyroscopicForce, float* %maxGyroscopicForce.addr, align 4, !tbaa !15
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %inertiaLocal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %inertiaLocal)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this1)
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call2)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !15
  %div = fdiv float 1.000000e+00, %1
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %inertiaLocal)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  store float %div, float* %arrayidx5, align 4, !tbaa !15
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this1)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 1
  %2 = load float, float* %arrayidx8, align 4, !tbaa !15
  %div9 = fdiv float 1.000000e+00, %2
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %inertiaLocal)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  store float %div9, float* %arrayidx11, align 4, !tbaa !15
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this1)
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call12)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  %3 = load float, float* %arrayidx14, align 4, !tbaa !15
  %div15 = fdiv float 1.000000e+00, %3
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %inertiaLocal)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 2
  store float %div15, float* %arrayidx17, align 4, !tbaa !15
  %4 = bitcast %class.btMatrix3x3* %inertiaTensorWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %4) #9
  %5 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %5) #9
  %6 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call18 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %6)
  %call19 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call18)
  call void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call19, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaLocal)
  %7 = bitcast %class.btMatrix3x3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %7) #9
  %8 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %8)
  %call22 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call21)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp20, %class.btMatrix3x3* %call22)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %inertiaTensorWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp20)
  %9 = bitcast %class.btMatrix3x3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %9) #9
  %10 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %10) #9
  %11 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %call23)
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %call24, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp)
  %12 = bitcast float* %l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %call25 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %agg.result)
  store float %call25, float* %l2, align 4, !tbaa !15
  %13 = load float, float* %l2, align 4, !tbaa !15
  %14 = load float, float* %maxGyroscopicForce.addr, align 4, !tbaa !15
  %15 = load float, float* %maxGyroscopicForce.addr, align 4, !tbaa !15
  %mul = fmul float %14, %15
  %cmp = fcmp ogt float %13, %mul
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load float, float* %l2, align 4, !tbaa !15
  %call27 = call float @_Z6btSqrtf(float %17)
  %div28 = fdiv float 1.000000e+00, %call27
  %18 = load float, float* %maxGyroscopicForce.addr, align 4, !tbaa !15
  %mul29 = fmul float %div28, %18
  store float %mul29, float* %ref.tmp26, align 4, !tbaa !15
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %19 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %20 = bitcast float* %l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #9
  %22 = bitcast %class.btMatrix3x3* %inertiaTensorWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %22) #9
  %23 = bitcast %class.btVector3* %inertiaLocal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  ret %class.btVector3* %m_invInertiaLocal
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !15
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !15
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !15
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !15
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !15
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !15
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !15
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !15
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !15
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !15
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !15
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !15
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !15
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !15
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !15
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !15
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !15
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !15
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !15
  %0 = load float, float* %y.addr, align 4, !tbaa !15
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

define hidden void @_ZN11btRigidBody19integrateVelocitiesEf(%class.btRigidBody* %this, float %step) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %step.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %angvel = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store float %step, float* %step.addr, align 4, !tbaa !15
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %3 = load float, float* %m_inverseMass, align 4, !tbaa !58
  %4 = load float, float* %step.addr, align 4, !tbaa !15
  %mul = fmul float %3, %4
  store float %mul, float* %ref.tmp2, align 4, !tbaa !15
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_totalForce, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  %7 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %8 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #9
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_totalTorque)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %step.addr)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #9
  %10 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #9
  %11 = bitcast float* %angvel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_angularVelocity7 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call8 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_angularVelocity7)
  store float %call8, float* %angvel, align 4, !tbaa !15
  %12 = load float, float* %angvel, align 4, !tbaa !15
  %13 = load float, float* %step.addr, align 4, !tbaa !15
  %mul9 = fmul float %12, %13
  %cmp = fcmp ogt float %mul9, 0x3FF921FB60000000
  br i1 %cmp, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.end
  %14 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load float, float* %step.addr, align 4, !tbaa !15
  %div = fdiv float 0x3FF921FB60000000, %15
  %16 = load float, float* %angvel, align 4, !tbaa !15
  %div12 = fdiv float %div, %16
  store float %div12, float* %ref.tmp11, align 4, !tbaa !15
  %m_angularVelocity13 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_angularVelocity13, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %17 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %if.end15

if.end15:                                         ; preds = %if.then10, %if.end
  %18 = bitcast float* %angvel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  br label %return

return:                                           ; preds = %if.end15, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !15
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !15
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !15
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !15
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !15
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !15
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !15
  ret %class.btVector3* %this1
}

define hidden void @_ZNK11btRigidBody14getOrientationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btRigidBody* %this) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_worldTransform)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %call2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %trace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4, !tbaa !15
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %2 = load float, float* %call4, align 4, !tbaa !15
  %add = fadd float %1, %2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %3 = load float, float* %call7, align 4, !tbaa !15
  %add8 = fadd float %add, %3
  store float %add8, float* %trace, align 4, !tbaa !15
  %4 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %5 = load float, float* %trace, align 4, !tbaa !15
  %cmp = fcmp ogt float %5, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load float, float* %trace, align 4, !tbaa !15
  %add9 = fadd float %7, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4, !tbaa !15
  %8 = load float, float* %s, align 4, !tbaa !15
  %mul = fmul float %8, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4, !tbaa !15
  %9 = load float, float* %s, align 4, !tbaa !15
  %div = fdiv float 5.000000e-01, %9
  store float %div, float* %s, align 4, !tbaa !15
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %10 = load float, float* %call14, align 4, !tbaa !15
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %11 = load float, float* %call17, align 4, !tbaa !15
  %sub = fsub float %10, %11
  %12 = load float, float* %s, align 4, !tbaa !15
  %mul18 = fmul float %sub, %12
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16, !tbaa !15
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %13 = load float, float* %call22, align 4, !tbaa !15
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %14 = load float, float* %call25, align 4, !tbaa !15
  %sub26 = fsub float %13, %14
  %15 = load float, float* %s, align 4, !tbaa !15
  %mul27 = fmul float %sub26, %15
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4, !tbaa !15
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %16 = load float, float* %call31, align 4, !tbaa !15
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %17 = load float, float* %call34, align 4, !tbaa !15
  %sub35 = fsub float %16, %17
  %18 = load float, float* %s, align 4, !tbaa !15
  %mul36 = fmul float %sub35, %18
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8, !tbaa !15
  %19 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %21 = load float, float* %call40, align 4, !tbaa !15
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %22 = load float, float* %call43, align 4, !tbaa !15
  %cmp44 = fcmp olt float %21, %22
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %23 = load float, float* %call47, align 4, !tbaa !15
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %24 = load float, float* %call50, align 4, !tbaa !15
  %cmp51 = fcmp olt float %23, %24
  %25 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %26 = load float, float* %call54, align 4, !tbaa !15
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %27 = load float, float* %call57, align 4, !tbaa !15
  %cmp58 = fcmp olt float %26, %27
  %28 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4, !tbaa !49
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load i32, i32* %i, align 4, !tbaa !49
  %add61 = add nsw i32 %30, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4, !tbaa !49
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  %32 = load i32, i32* %i, align 4, !tbaa !49
  %add62 = add nsw i32 %32, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4, !tbaa !49
  %33 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %34 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %34
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %35 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %35
  %36 = load float, float* %arrayidx68, align 4, !tbaa !15
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %37 = load i32, i32* %j, align 4, !tbaa !49
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %37
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %38 = load i32, i32* %j, align 4, !tbaa !49
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %38
  %39 = load float, float* %arrayidx72, align 4, !tbaa !15
  %sub73 = fsub float %36, %39
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %40 = load i32, i32* %k, align 4, !tbaa !49
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %40
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %41 = load i32, i32* %k, align 4, !tbaa !49
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %41
  %42 = load float, float* %arrayidx77, align 4, !tbaa !15
  %sub78 = fsub float %sub73, %42
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4, !tbaa !15
  %43 = load float, float* %s64, align 4, !tbaa !15
  %mul81 = fmul float %43, 5.000000e-01
  %44 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %44
  store float %mul81, float* %arrayidx82, align 4, !tbaa !15
  %45 = load float, float* %s64, align 4, !tbaa !15
  %div83 = fdiv float 5.000000e-01, %45
  store float %div83, float* %s64, align 4, !tbaa !15
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %46 = load i32, i32* %k, align 4, !tbaa !49
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %46
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %47 = load i32, i32* %j, align 4, !tbaa !49
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %47
  %48 = load float, float* %arrayidx87, align 4, !tbaa !15
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %49 = load i32, i32* %j, align 4, !tbaa !49
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %49
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %50 = load i32, i32* %k, align 4, !tbaa !49
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %50
  %51 = load float, float* %arrayidx91, align 4, !tbaa !15
  %sub92 = fsub float %48, %51
  %52 = load float, float* %s64, align 4, !tbaa !15
  %mul93 = fmul float %sub92, %52
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4, !tbaa !15
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %j, align 4, !tbaa !49
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %53
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %54 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %54
  %55 = load float, float* %arrayidx98, align 4, !tbaa !15
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %56
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %57 = load i32, i32* %j, align 4, !tbaa !49
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %57
  %58 = load float, float* %arrayidx102, align 4, !tbaa !15
  %add103 = fadd float %55, %58
  %59 = load float, float* %s64, align 4, !tbaa !15
  %mul104 = fmul float %add103, %59
  %60 = load i32, i32* %j, align 4, !tbaa !49
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul104, float* %arrayidx105, align 4, !tbaa !15
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %61 = load i32, i32* %k, align 4, !tbaa !49
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %61
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %62 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %62
  %63 = load float, float* %arrayidx109, align 4, !tbaa !15
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %64 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %64
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %65 = load i32, i32* %k, align 4, !tbaa !49
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %65
  %66 = load float, float* %arrayidx113, align 4, !tbaa !15
  %add114 = fadd float %63, %66
  %67 = load float, float* %s64, align 4, !tbaa !15
  %mul115 = fmul float %add114, %67
  %68 = load i32, i32* %k, align 4, !tbaa !49
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %68
  store float %mul115, float* %arrayidx116, align 4, !tbaa !15
  %69 = bitcast float* %s64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #9
  %70 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #9
  %71 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #9
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #9
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %74, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  %75 = bitcast [4 x float]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %75) #9
  %76 = bitcast float* %trace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !57
  %and = and i32 %0, 2
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

define hidden zeroext i1 @_ZNK11btRigidBody24checkCollideWithOverrideEPK17btCollisionObject(%class.btRigidBody* %this, %class.btCollisionObject* %co) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btRigidBody*, align 4
  %co.addr = alloca %class.btCollisionObject*, align 4
  %otherRb = alloca %class.btRigidBody*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %c = alloca %class.btTypedConstraint*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %co, %class.btCollisionObject** %co.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody** %otherRb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %co.addr, align 4, !tbaa !2
  %call = call %class.btRigidBody* @_ZN11btRigidBody6upcastEPK17btCollisionObject(%class.btCollisionObject* %1)
  store %class.btRigidBody* %call, %class.btRigidBody** %otherRb, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %otherRb, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store i32 0, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %i, align 4, !tbaa !49
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %m_constraintRefs)
  %cmp = icmp slt i32 %4, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

for.body:                                         ; preds = %for.cond
  %5 = bitcast %class.btTypedConstraint** %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_constraintRefs3 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %6 = load i32, i32* %i, align 4, !tbaa !49
  %call4 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray* %m_constraintRefs3, i32 %6)
  %7 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call4, align 4, !tbaa !2
  store %class.btTypedConstraint* %7, %class.btTypedConstraint** %c, align 4, !tbaa !2
  %8 = load %class.btTypedConstraint*, %class.btTypedConstraint** %c, align 4, !tbaa !2
  %call5 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %8)
  br i1 %call5, label %if.then6, label %if.end13

if.then6:                                         ; preds = %for.body
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %c, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %9)
  %10 = load %class.btRigidBody*, %class.btRigidBody** %otherRb, align 4, !tbaa !2
  %cmp8 = icmp eq %class.btRigidBody* %call7, %10
  br i1 %cmp8, label %if.then11, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then6
  %11 = load %class.btTypedConstraint*, %class.btTypedConstraint** %c, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %11)
  %12 = load %class.btRigidBody*, %class.btRigidBody** %otherRb, align 4, !tbaa !2
  %cmp10 = icmp eq %class.btRigidBody* %call9, %12
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %lor.lhs.false, %if.then6
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %lor.lhs.false
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end13, %if.then11
  %13 = bitcast %class.btTypedConstraint** %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup14 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %14 = load i32, i32* %i, align 4, !tbaa !49
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !49
  br label %for.cond

cleanup14:                                        ; preds = %cleanup, %for.cond.cleanup
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %cleanup.dest15 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest15, label %cleanup16 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup14
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

cleanup16:                                        ; preds = %for.end, %cleanup14, %if.then
  %16 = bitcast %class.btRigidBody** %otherRb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBody6upcastEPK17btCollisionObject(%class.btCollisionObject* %colObj) #0 comdat {
entry:
  %retval = alloca %class.btRigidBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionObject* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btRigidBody* null, %class.btRigidBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btRigidBody*, %class.btRigidBody** %retval, align 4
  ret %class.btRigidBody* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !60
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !49
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %1 = load i32, i32* %n.addr, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_isEnabled, align 4, !tbaa !62, !range !32
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !64
  ret %class.btRigidBody* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !65
  ret %class.btRigidBody* %0
}

define hidden void @_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint(%class.btRigidBody* %this, %class.btTypedConstraint* %c) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %c.addr = alloca %class.btTypedConstraint*, align 4
  %index = alloca i32, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint* %c, %class.btTypedConstraint** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %m_constraintRefs, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %c.addr)
  store i32 %call, i32* %index, align 4, !tbaa !49
  %1 = load i32, i32* %index, align 4, !tbaa !49
  %m_constraintRefs2 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %m_constraintRefs2)
  %cmp = icmp eq i32 %1, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_constraintRefs4 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray* %m_constraintRefs4, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %c.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %2, i32 0, i32 25
  store i32 1, i32* %m_checkCollideWith, align 4, !tbaa !66
  %3 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %key) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %class.btTypedConstraint**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint** %key, %class.btTypedConstraint*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !49
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !49
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %4 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4, !tbaa !2
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %key.addr, align 4, !tbaa !2
  %7 = load %class.btTypedConstraint*, %class.btTypedConstraint** %6, align 4, !tbaa !2
  %cmp3 = icmp eq %class.btTypedConstraint* %5, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !49
  store i32 %8, i32* %index, align 4, !tbaa !49
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !49
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %10 = load i32, i32* %index, align 4, !tbaa !49
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret i32 %10
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %_Val) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btTypedConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint** %_Val, %class.btTypedConstraint*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !49
  %1 = load i32, i32* %sz, align 4, !tbaa !49
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %2, i32 %3
  %4 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btTypedConstraint**
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btTypedConstraint*, %class.btTypedConstraint** %6, align 4, !tbaa !2
  store %class.btTypedConstraint* %7, %class.btTypedConstraint** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !60
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !60
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  ret void
}

define hidden void @_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint(%class.btRigidBody* %this, %class.btTypedConstraint* %c) #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %c.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint* %c, %class.btTypedConstraint** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_(%class.btAlignedObjectArray* %m_constraintRefs, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %c.addr)
  %m_constraintRefs2 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %m_constraintRefs2)
  %cmp = icmp sgt i32 %call, 0
  %conv = zext i1 %cmp to i32
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 25
  store i32 %conv, i32* %m_checkCollideWith, align 4, !tbaa !66
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_(%class.btAlignedObjectArray* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %class.btTypedConstraint**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint** %key, %class.btTypedConstraint*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray* %this1, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %1)
  store i32 %call, i32* %findIndex, align 4, !tbaa !49
  %2 = load i32, i32* %findIndex, align 4, !tbaa !49
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %findIndex, align 4, !tbaa !49
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call3, 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray* %this1, i32 %3, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv(%class.btAlignedObjectArray* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret void
}

; Function Attrs: nounwind
define hidden i32 @_ZNK11btRigidBody28calculateSerializeBufferSizeEv(%class.btRigidBody* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %sz = alloca i32, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 488, i32* %sz, align 4, !tbaa !49
  %1 = load i32, i32* %sz, align 4, !tbaa !49
  %2 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  ret i32 %1
}

define hidden i8* @_ZNK11btRigidBody9serializeEPvP12btSerializer(%class.btRigidBody* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %rbd = alloca %struct.btRigidBodyFloatData*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %struct.btRigidBodyFloatData** %rbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btRigidBodyFloatData*
  store %struct.btRigidBodyFloatData* %2, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %3 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %4 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_collisionObjectData = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionObjectFloatData* %m_collisionObjectData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK17btCollisionObject9serializeEPvP12btSerializer(%class.btCollisionObject* %3, i8* %5, %class.btSerializer* %6)
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %7 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_invInertiaTensorWorld2 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %7, i32 0, i32 1
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_invInertiaTensorWorld, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld2)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %8 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_linearVelocity3 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %8, i32 0, i32 2
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_linearVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearVelocity3)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %9 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_angularVelocity4 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %9, i32 0, i32 3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_angularVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_angularVelocity4)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %10 = load float, float* %m_inverseMass, align 4, !tbaa !58
  %11 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_inverseMass5 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %11, i32 0, i32 11
  store float %10, float* %m_inverseMass5, align 4, !tbaa !67
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %12 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_angularFactor6 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %12, i32 0, i32 4
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_angularFactor, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_angularFactor6)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %13 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_linearFactor7 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %13, i32 0, i32 5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_linearFactor, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearFactor7)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %14 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_gravity8 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %14, i32 0, i32 6
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_gravity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_gravity8)
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %15 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_gravity_acceleration9 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %15, i32 0, i32 7
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_gravity_acceleration, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_gravity_acceleration9)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %16 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_invInertiaLocal10 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %16, i32 0, i32 8
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_invInertiaLocal, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_invInertiaLocal10)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %17 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_totalForce11 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %17, i32 0, i32 9
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_totalForce, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_totalForce11)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %18 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_totalTorque12 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %18, i32 0, i32 10
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_totalTorque, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_totalTorque12)
  %m_linearDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  %19 = load float, float* %m_linearDamping, align 4, !tbaa !55
  %20 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_linearDamping13 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %20, i32 0, i32 12
  store float %19, float* %m_linearDamping13, align 4, !tbaa !73
  %m_angularDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  %21 = load float, float* %m_angularDamping, align 4, !tbaa !56
  %22 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_angularDamping14 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %22, i32 0, i32 13
  store float %21, float* %m_angularDamping14, align 4, !tbaa !74
  %m_additionalDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 13
  %23 = load i8, i8* %m_additionalDamping, align 4, !tbaa !33, !range !32
  %tobool = trunc i8 %23 to i1
  %conv = zext i1 %tobool to i32
  %24 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_additionalDamping15 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %24, i32 0, i32 20
  store i32 %conv, i32* %m_additionalDamping15, align 4, !tbaa !75
  %m_additionalDampingFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  %25 = load float, float* %m_additionalDampingFactor, align 4, !tbaa !35
  %26 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_additionalDampingFactor16 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %26, i32 0, i32 14
  store float %25, float* %m_additionalDampingFactor16, align 4, !tbaa !76
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 15
  %27 = load float, float* %m_additionalLinearDampingThresholdSqr, align 4, !tbaa !37
  %28 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_additionalLinearDampingThresholdSqr17 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %28, i32 0, i32 15
  store float %27, float* %m_additionalLinearDampingThresholdSqr17, align 4, !tbaa !77
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 16
  %29 = load float, float* %m_additionalAngularDampingThresholdSqr, align 4, !tbaa !39
  %30 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_additionalAngularDampingThresholdSqr18 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %30, i32 0, i32 16
  store float %29, float* %m_additionalAngularDampingThresholdSqr18, align 4, !tbaa !78
  %m_additionalAngularDampingFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 17
  %31 = load float, float* %m_additionalAngularDampingFactor, align 4, !tbaa !41
  %32 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_additionalAngularDampingFactor19 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %32, i32 0, i32 17
  store float %31, float* %m_additionalAngularDampingFactor19, align 4, !tbaa !79
  %m_linearSleepingThreshold = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 18
  %33 = load float, float* %m_linearSleepingThreshold, align 4, !tbaa !21
  %34 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_linearSleepingThreshold20 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %34, i32 0, i32 18
  store float %33, float* %m_linearSleepingThreshold20, align 4, !tbaa !80
  %m_angularSleepingThreshold = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 19
  %35 = load float, float* %m_angularSleepingThreshold, align 4, !tbaa !26
  %36 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4, !tbaa !2
  %m_angularSleepingThreshold21 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %36, i32 0, i32 19
  store float %35, float* %m_angularSleepingThreshold21, align 4, !tbaa !81
  %37 = bitcast %struct.btRigidBodyFloatData** %rbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  ret i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0)
}

declare i8* @_ZNK17btCollisionObject9serializeEPvP12btSerializer(%class.btCollisionObject*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !49
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %3
  %4 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !49
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !49
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !15
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !15
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !49
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define hidden void @_ZNK11btRigidBody21serializeSingleObjectEP12btSerializer(%class.btRigidBody* %this, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %2 = bitcast %class.btRigidBody* %this1 to i32 (%class.btRigidBody*)***
  %vtable = load i32 (%class.btRigidBody*)**, i32 (%class.btRigidBody*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btRigidBody*)*, i32 (%class.btRigidBody*)** %vtable, i64 4
  %3 = load i32 (%class.btRigidBody*)*, i32 (%class.btRigidBody*)** %vfn, align 4
  %call = call i32 %3(%class.btRigidBody* %this1)
  %4 = bitcast %class.btSerializer* %1 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable2 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %4, align 4, !tbaa !6
  %vfn3 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable2, i64 4
  %5 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn3, align 4
  %call4 = call %class.btChunk* %5(%class.btSerializer* %1, i32 %call, i32 1)
  store %class.btChunk* %call4, %class.btChunk** %chunk, align 4, !tbaa !2
  %6 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %7, i32 0, i32 2
  %8 = load i8*, i8** %m_oldPtr, align 4, !tbaa !82
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %10 = bitcast %class.btRigidBody* %this1 to i8* (%class.btRigidBody*, i8*, %class.btSerializer*)***
  %vtable5 = load i8* (%class.btRigidBody*, i8*, %class.btSerializer*)**, i8* (%class.btRigidBody*, i8*, %class.btSerializer*)*** %10, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds i8* (%class.btRigidBody*, i8*, %class.btSerializer*)*, i8* (%class.btRigidBody*, i8*, %class.btSerializer*)** %vtable5, i64 5
  %11 = load i8* (%class.btRigidBody*, i8*, %class.btSerializer*)*, i8* (%class.btRigidBody*, i8*, %class.btSerializer*)** %vfn6, align 4
  %call7 = call i8* %11(%class.btRigidBody* %this1, i8* %8, %class.btSerializer* %9)
  store i8* %call7, i8** %structType, align 4, !tbaa !2
  %12 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %13 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %14 = load i8*, i8** %structType, align 4, !tbaa !2
  %15 = bitcast %class.btRigidBody* %this1 to i8*
  %16 = bitcast %class.btSerializer* %12 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable8 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %16, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable8, i64 5
  %17 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn9, align 4
  call void %17(%class.btSerializer* %12, %class.btChunk* %13, i8* %14, i32 1497645650, i8* %15)
  %18 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  %19 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray* %m_constraintRefs) #9
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call2 = call %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* %1) #9
  ret %class.btRigidBody* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btRigidBodyD0Ev(%class.btRigidBody* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %call = call %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* %this1) #9
  %0 = bitcast %class.btRigidBody* %this1 to i8*
  call void @_ZN17btCollisionObjectdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape(%class.btCollisionObject* %this, %class.btCollisionShape* %collisionShape) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !84
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !84
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !59
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %m_rootCollisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 11
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_rootCollisionShape, align 4, !tbaa !85
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !15
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !15
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !15
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !15
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !15
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !15
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !15
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !15
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !15
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !15
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !15
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !15
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !53
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !15
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !15
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !15
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !15
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !15
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !15
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !15
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !15
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !15
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btSinf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !15
  %0 = load float, float* %x.addr, align 4, !tbaa !15
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !15
  %0 = load float, float* %x.addr, align 4, !tbaa !15
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #4 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !15
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call1, align 4, !tbaa !15
  %mul = fmul float %3, %6
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = load float, float* %call2, align 4, !tbaa !15
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !15
  %mul4 = fmul float %9, %12
  %add = fadd float %mul, %mul4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %14)
  %15 = load float, float* %call5, align 4, !tbaa !15
  %16 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %17 = bitcast %class.btQuaternion* %16 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %17)
  %18 = load float, float* %call6, align 4, !tbaa !15
  %mul7 = fmul float %15, %18
  %add8 = fadd float %add, %mul7
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %20)
  %21 = load float, float* %call9, align 4, !tbaa !15
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %23)
  %24 = load float, float* %call10, align 4, !tbaa !15
  %mul11 = fmul float %21, %24
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4, !tbaa !15
  %25 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4, !tbaa !15
  %29 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %30 = bitcast %class.btQuaternion* %29 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %30)
  %31 = load float, float* %call14, align 4, !tbaa !15
  %mul15 = fmul float %28, %31
  %32 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %33 = bitcast %class.btQuaternion* %32 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %33)
  %34 = load float, float* %call16, align 4, !tbaa !15
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %36)
  %37 = load float, float* %call17, align 4, !tbaa !15
  %mul18 = fmul float %34, %37
  %add19 = fadd float %mul15, %mul18
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call20, align 4, !tbaa !15
  %41 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %42 = bitcast %class.btQuaternion* %41 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %42)
  %43 = load float, float* %call21, align 4, !tbaa !15
  %mul22 = fmul float %40, %43
  %add23 = fadd float %add19, %mul22
  %44 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %45 = bitcast %class.btQuaternion* %44 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %45)
  %46 = load float, float* %call24, align 4, !tbaa !15
  %47 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %48 = bitcast %class.btQuaternion* %47 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %48)
  %49 = load float, float* %call25, align 4, !tbaa !15
  %mul26 = fmul float %46, %49
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4, !tbaa !15
  %50 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #9
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %52)
  %53 = load float, float* %call29, align 4, !tbaa !15
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call30, align 4, !tbaa !15
  %mul31 = fmul float %53, %56
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %58)
  %59 = load float, float* %call32, align 4, !tbaa !15
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %61)
  %62 = load float, float* %call33, align 4, !tbaa !15
  %mul34 = fmul float %59, %62
  %add35 = fadd float %mul31, %mul34
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %64)
  %65 = load float, float* %call36, align 4, !tbaa !15
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call37, align 4, !tbaa !15
  %mul38 = fmul float %65, %68
  %add39 = fadd float %add35, %mul38
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %70)
  %71 = load float, float* %call40, align 4, !tbaa !15
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call41, align 4, !tbaa !15
  %mul42 = fmul float %71, %74
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4, !tbaa !15
  %75 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #9
  %76 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %77 = bitcast %class.btQuaternion* %76 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %77)
  %78 = load float, float* %call45, align 4, !tbaa !15
  %79 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %80 = bitcast %class.btQuaternion* %79 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %80)
  %81 = load float, float* %call46, align 4, !tbaa !15
  %mul47 = fmul float %78, %81
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %83)
  %84 = load float, float* %call48, align 4, !tbaa !15
  %85 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %86 = bitcast %class.btQuaternion* %85 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %86)
  %87 = load float, float* %call49, align 4, !tbaa !15
  %mul50 = fmul float %84, %87
  %sub51 = fsub float %mul47, %mul50
  %88 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %89 = bitcast %class.btQuaternion* %88 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %89)
  %90 = load float, float* %call52, align 4, !tbaa !15
  %91 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %92 = bitcast %class.btQuaternion* %91 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %92)
  %93 = load float, float* %call53, align 4, !tbaa !15
  %mul54 = fmul float %90, %93
  %sub55 = fsub float %sub51, %mul54
  %94 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4, !tbaa !2
  %95 = bitcast %class.btQuaternion* %94 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %95)
  %96 = load float, float* %call56, align 4, !tbaa !15
  %97 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4, !tbaa !2
  %98 = bitcast %class.btQuaternion* %97 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %98)
  %99 = load float, float* %call57, align 4, !tbaa !15
  %mul58 = fmul float %96, %99
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4, !tbaa !15
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  %100 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  %101 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #9
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !15
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #9
  ret %class.btQuaternion* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !15
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !15
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !15
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !15
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !15
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !15
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !15
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !15
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !15
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !15
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !15
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btQuaternion* %call
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #5 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !15
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !15
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !15
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !15
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !15
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !15
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !15
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !15
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4, !tbaa !15
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !15
  %4 = load float*, float** %s.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !15
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !15
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4, !tbaa !15
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !15
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4, !tbaa !15
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4, !tbaa !15
  %12 = load float*, float** %s.addr, align 4, !tbaa !2
  %13 = load float, float* %12, align 4, !tbaa !15
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4, !tbaa !15
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4, !tbaa !15
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !15
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load float, float* %d, align 4, !tbaa !15
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !15
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !15
  %8 = load float, float* %s, align 4, !tbaa !15
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !15
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !15
  %13 = load float, float* %s, align 4, !tbaa !15
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !15
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !15
  %18 = load float, float* %s, align 4, !tbaa !15
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !15
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !15
  %23 = load float, float* %xs, align 4, !tbaa !15
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !15
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !15
  %28 = load float, float* %ys, align 4, !tbaa !15
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !15
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !15
  %33 = load float, float* %zs, align 4, !tbaa !15
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !15
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !15
  %38 = load float, float* %xs, align 4, !tbaa !15
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !15
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !15
  %43 = load float, float* %ys, align 4, !tbaa !15
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !15
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !15
  %48 = load float, float* %zs, align 4, !tbaa !15
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !15
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !15
  %53 = load float, float* %ys, align 4, !tbaa !15
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !15
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #9
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !15
  %58 = load float, float* %zs, align 4, !tbaa !15
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !15
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #9
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !15
  %63 = load float, float* %zs, align 4, !tbaa !15
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !15
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #9
  %65 = load float, float* %yy, align 4, !tbaa !15
  %66 = load float, float* %zz, align 4, !tbaa !15
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !15
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  %68 = load float, float* %xy, align 4, !tbaa !15
  %69 = load float, float* %wz, align 4, !tbaa !15
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !15
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #9
  %71 = load float, float* %xz, align 4, !tbaa !15
  %72 = load float, float* %wy, align 4, !tbaa !15
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !15
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  %74 = load float, float* %xy, align 4, !tbaa !15
  %75 = load float, float* %wz, align 4, !tbaa !15
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !15
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #9
  %77 = load float, float* %xx, align 4, !tbaa !15
  %78 = load float, float* %zz, align 4, !tbaa !15
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !15
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #9
  %80 = load float, float* %yz, align 4, !tbaa !15
  %81 = load float, float* %wx, align 4, !tbaa !15
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !15
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #9
  %83 = load float, float* %xz, align 4, !tbaa !15
  %84 = load float, float* %wy, align 4, !tbaa !15
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !15
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #9
  %86 = load float, float* %yz, align 4, !tbaa !15
  %87 = load float, float* %wx, align 4, !tbaa !15
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !15
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #9
  %89 = load float, float* %xx, align 4, !tbaa !15
  %90 = load float, float* %yy, align 4, !tbaa !15
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !15
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #9
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #9
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #9
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #9
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #9
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #9
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #9
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #9
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #9
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #9
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #9
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #9
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #9
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !15
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !15
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !15
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !15
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !15
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !15
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !15
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !15
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !15
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !15
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

define linkonce_odr hidden void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle) #0 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %angle.addr = alloca float*, align 4
  %dmat = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store float* %angle, float** %angle.addr, align 4, !tbaa !2
  %0 = bitcast %class.btMatrix3x3* %dmat to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #9
  %1 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  %2 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %2) #9
  %3 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %3)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call1)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %dmat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %4 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %4) #9
  %5 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %call2 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %dorn)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %dmat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %dorn)
  %call4 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %dorn)
  %6 = load float*, float** %angle.addr, align 4, !tbaa !2
  store float %call4, float* %6, align 4, !tbaa !15
  %7 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %8 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %8)
  %9 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %9)
  %10 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %10)
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %call6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8)
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !53
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx = getelementptr inbounds float, float* %call10, i32 3
  store float 0.000000e+00, float* %arrayidx, align 4, !tbaa !15
  %16 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %17)
  store float %call11, float* %len, align 4, !tbaa !15
  %18 = load float, float* %len, align 4, !tbaa !15
  %cmp = fcmp olt float %18, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %19 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #9
  %20 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store float 1.000000e+00, float* %ref.tmp13, align 4, !tbaa !15
  %21 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !15
  %22 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !15
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %23 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %23 to i8*
  %25 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !53
  %26 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %28 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %31 = load float, float* %len, align 4, !tbaa !15
  %call18 = call float @_Z6btSqrtf(float %31)
  store float %call18, float* %ref.tmp17, align 4, !tbaa !15
  %32 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %32, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %33 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %34 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast %class.btQuaternion* %dorn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #9
  %36 = bitcast %class.btMatrix3x3* %dmat to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %36) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4, !tbaa !15
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !15
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !15
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %det to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4, !tbaa !15
  %9 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load float, float* %det, align 4, !tbaa !15
  %div = fdiv float 1.000000e+00, %10
  store float %div, float* %s, align 4, !tbaa !15
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %12 = load float, float* %call10, align 4, !tbaa !15
  %13 = load float, float* %s, align 4, !tbaa !15
  %mul = fmul float %12, %13
  store float %mul, float* %ref.tmp9, align 4, !tbaa !15
  %14 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %15 = load float, float* %s, align 4, !tbaa !15
  %mul13 = fmul float %call12, %15
  store float %mul13, float* %ref.tmp11, align 4, !tbaa !15
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %17 = load float, float* %s, align 4, !tbaa !15
  %mul16 = fmul float %call15, %17
  store float %mul16, float* %ref.tmp14, align 4, !tbaa !15
  %18 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %19 = load float, float* %call18, align 4, !tbaa !15
  %20 = load float, float* %s, align 4, !tbaa !15
  %mul19 = fmul float %19, %20
  store float %mul19, float* %ref.tmp17, align 4, !tbaa !15
  %21 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %22 = load float, float* %s, align 4, !tbaa !15
  %mul22 = fmul float %call21, %22
  store float %mul22, float* %ref.tmp20, align 4, !tbaa !15
  %23 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %24 = load float, float* %s, align 4, !tbaa !15
  %mul25 = fmul float %call24, %24
  store float %mul25, float* %ref.tmp23, align 4, !tbaa !15
  %25 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %26 = load float, float* %call27, align 4, !tbaa !15
  %27 = load float, float* %s, align 4, !tbaa !15
  %mul28 = fmul float %26, %27
  store float %mul28, float* %ref.tmp26, align 4, !tbaa !15
  %28 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %29 = load float, float* %s, align 4, !tbaa !15
  %mul31 = fmul float %call30, %29
  store float %mul31, float* %ref.tmp29, align 4, !tbaa !15
  %30 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %31 = load float, float* %s, align 4, !tbaa !15
  %mul34 = fmul float %call33, %31
  store float %mul34, float* %ref.tmp32, align 4, !tbaa !15
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %32 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  %37 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  %39 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  %40 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast float* %det to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  %43 = bitcast %class.btVector3* %co to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #9
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4, !tbaa !15
  %call = call float @_Z6btAcosf(float %2)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4, !tbaa !15
  %3 = load float, float* %s, align 4, !tbaa !15
  %4 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret float %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !15
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !15
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btVector3* %call
}

define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %r1, i32* %r1.addr, align 4, !tbaa !49
  store i32 %c1, i32* %c1.addr, align 4, !tbaa !49
  store i32 %r2, i32* %r2.addr, align 4, !tbaa !49
  store i32 %c2, i32* %c2.addr, align 4, !tbaa !49
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4, !tbaa !49
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4, !tbaa !15
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4, !tbaa !49
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4, !tbaa !49
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4, !tbaa !15
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4, !tbaa !49
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4, !tbaa !49
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4, !tbaa !15
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4, !tbaa !49
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4, !tbaa !49
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4, !tbaa !15
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !49
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !15
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !15
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !15
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !15
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !15
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !15
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btAcosf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !15
  %0 = load float, float* %x.addr, align 4, !tbaa !15
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4, !tbaa !15
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4, !tbaa !15
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4, !tbaa !15
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4, !tbaa !15
  %call = call float @acosf(float %2) #10
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #7

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #6

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !15
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #9
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !15
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !15
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !15
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !15
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !15
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !15
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !15
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !15
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !15
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !15
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !15
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !15
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !15
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !15
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !15
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !15
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !15
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !15
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !15
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !15
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !15
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !15
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !15
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !15
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !15
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !15
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !15
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #2 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !15
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !15
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !15
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !15
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !15
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  %0 = load i32, i32* %m_internalType, align 4, !tbaa !8
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned) unnamed_addr #8

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !49
  store i32 %last, i32* %last.addr, align 4, !tbaa !49
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !49
  store i32 %1, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !49
  %3 = load i32, i32* %last.addr, align 4, !tbaa !49
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %5 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !49
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !86, !range !32
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4, !tbaa !61
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4, !tbaa !61
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !86
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !60
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !87
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btTypedConstraint** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btCollisionObjectdlEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !87
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !49
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !49
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btTypedConstraint*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !49
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btTypedConstraint**
  store %class.btTypedConstraint** %3, %class.btTypedConstraint*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btTypedConstraint** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !86
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btTypedConstraint** %5, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !49
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !87
  %7 = bitcast %class.btTypedConstraint*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !49
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !49
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !49
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !49
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !49
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !49
  %call = call %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btTypedConstraint*** null)
  %2 = bitcast %class.btTypedConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btTypedConstraint** %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btTypedConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !49
  store i32 %end, i32* %end.addr, align 4, !tbaa !49
  store %class.btTypedConstraint** %dest, %class.btTypedConstraint*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !49
  store i32 %1, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !49
  %3 = load i32, i32* %end.addr, align 4, !tbaa !49
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %4, i32 %5
  %6 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btTypedConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %9 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx2 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %8, i32 %9
  %10 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx2, align 4, !tbaa !2
  store %class.btTypedConstraint* %10, %class.btTypedConstraint** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !49
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define linkonce_odr hidden %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btTypedConstraint*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btTypedConstraint***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !49
  store %class.btTypedConstraint*** %hint, %class.btTypedConstraint**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !49
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btTypedConstraint**
  ret %class.btTypedConstraint** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !49
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !49
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4, !tbaa !2
  store %class.btTypedConstraint* %3, %class.btTypedConstraint** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4, !tbaa !61
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !49
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %4, i32 %5
  %6 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4, !tbaa !61
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !49
  %arrayidx5 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  store %class.btTypedConstraint* %6, %class.btTypedConstraint** %arrayidx5, align 4, !tbaa !2
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %10 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data6, align 4, !tbaa !61
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !49
  %arrayidx7 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %10, i32 %11
  store %class.btTypedConstraint* %9, %class.btTypedConstraint** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %class.btTypedConstraint** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !60
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !60
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !61
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !60
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !13, i64 236}
!9 = !{!"_ZTS17btCollisionObject", !10, i64 4, !10, i64 68, !12, i64 132, !12, i64 148, !12, i64 164, !13, i64 180, !14, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !13, i64 204, !13, i64 208, !13, i64 212, !13, i64 216, !14, i64 220, !14, i64 224, !14, i64 228, !14, i64 232, !13, i64 236, !4, i64 240, !14, i64 244, !14, i64 248, !14, i64 252, !13, i64 256, !13, i64 260}
!10 = !{!"_ZTS11btTransform", !11, i64 0, !12, i64 48}
!11 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!"int", !4, i64 0}
!14 = !{!"float", !4, i64 0}
!15 = !{!14, !14, i64 0}
!16 = !{!17, !14, i64 92}
!17 = !{!"_ZTSN11btRigidBody27btRigidBodyConstructionInfoE", !14, i64 0, !3, i64 4, !10, i64 8, !3, i64 72, !12, i64 76, !14, i64 92, !14, i64 96, !14, i64 100, !14, i64 104, !14, i64 108, !14, i64 112, !14, i64 116, !18, i64 120, !14, i64 124, !14, i64 128, !14, i64 132, !14, i64 136}
!18 = !{!"bool", !4, i64 0}
!19 = !{!17, !14, i64 96}
!20 = !{!17, !14, i64 112}
!21 = !{!22, !14, i64 472}
!22 = !{!"_ZTS11btRigidBody", !11, i64 264, !12, i64 312, !12, i64 328, !14, i64 344, !12, i64 348, !12, i64 364, !12, i64 380, !12, i64 396, !12, i64 412, !12, i64 428, !14, i64 444, !14, i64 448, !18, i64 452, !14, i64 456, !14, i64 460, !14, i64 464, !14, i64 468, !14, i64 472, !14, i64 476, !3, i64 480, !23, i64 484, !13, i64 504, !13, i64 508, !12, i64 512, !12, i64 528, !12, i64 544, !12, i64 560, !12, i64 576, !12, i64 592, !13, i64 608, !13, i64 612}
!23 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !24, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !18, i64 16}
!24 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!25 = !{!17, !14, i64 116}
!26 = !{!22, !14, i64 476}
!27 = !{!17, !3, i64 4}
!28 = !{!22, !3, i64 480}
!29 = !{!22, !13, i64 608}
!30 = !{!22, !13, i64 612}
!31 = !{!17, !18, i64 120}
!32 = !{i8 0, i8 2}
!33 = !{!22, !18, i64 452}
!34 = !{!17, !14, i64 124}
!35 = !{!22, !14, i64 456}
!36 = !{!17, !14, i64 128}
!37 = !{!22, !14, i64 460}
!38 = !{!17, !14, i64 132}
!39 = !{!22, !14, i64 464}
!40 = !{!17, !14, i64 136}
!41 = !{!22, !14, i64 468}
!42 = !{!17, !14, i64 100}
!43 = !{!9, !14, i64 224}
!44 = !{!17, !14, i64 104}
!45 = !{!9, !14, i64 232}
!46 = !{!17, !14, i64 108}
!47 = !{!9, !14, i64 228}
!48 = !{!17, !3, i64 72}
!49 = !{!13, !13, i64 0}
!50 = !{!22, !13, i64 508}
!51 = !{!17, !14, i64 0}
!52 = !{!22, !13, i64 504}
!53 = !{i64 0, i64 16, !54}
!54 = !{!4, !4, i64 0}
!55 = !{!22, !14, i64 444}
!56 = !{!22, !14, i64 448}
!57 = !{!9, !13, i64 204}
!58 = !{!22, !14, i64 344}
!59 = !{!9, !3, i64 192}
!60 = !{!23, !13, i64 4}
!61 = !{!23, !3, i64 12}
!62 = !{!63, !18, i64 20}
!63 = !{!"_ZTS17btTypedConstraint", !13, i64 8, !4, i64 12, !14, i64 16, !18, i64 20, !18, i64 21, !13, i64 24, !3, i64 28, !3, i64 32, !14, i64 36, !14, i64 40, !3, i64 44}
!64 = !{!63, !3, i64 28}
!65 = !{!63, !3, i64 32}
!66 = !{!9, !13, i64 256}
!67 = !{!68, !14, i64 448}
!68 = !{!"_ZTS20btRigidBodyFloatData", !69, i64 0, !71, i64 256, !72, i64 304, !72, i64 320, !72, i64 336, !72, i64 352, !72, i64 368, !72, i64 384, !72, i64 400, !72, i64 416, !72, i64 432, !14, i64 448, !14, i64 452, !14, i64 456, !14, i64 460, !14, i64 464, !14, i64 468, !14, i64 472, !14, i64 476, !14, i64 480, !13, i64 484}
!69 = !{!"_ZTS26btCollisionObjectFloatData", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !70, i64 16, !70, i64 80, !72, i64 144, !72, i64 160, !72, i64 176, !14, i64 192, !14, i64 196, !14, i64 200, !14, i64 204, !14, i64 208, !14, i64 212, !14, i64 216, !14, i64 220, !13, i64 224, !13, i64 228, !13, i64 232, !13, i64 236, !13, i64 240, !13, i64 244, !13, i64 248, !4, i64 252}
!70 = !{!"_ZTS20btTransformFloatData", !71, i64 0, !72, i64 48}
!71 = !{!"_ZTS20btMatrix3x3FloatData", !4, i64 0}
!72 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!73 = !{!68, !14, i64 452}
!74 = !{!68, !14, i64 456}
!75 = !{!68, !13, i64 484}
!76 = !{!68, !14, i64 460}
!77 = !{!68, !14, i64 464}
!78 = !{!68, !14, i64 468}
!79 = !{!68, !14, i64 472}
!80 = !{!68, !14, i64 476}
!81 = !{!68, !14, i64 480}
!82 = !{!83, !3, i64 8}
!83 = !{!"_ZTS7btChunk", !13, i64 0, !13, i64 4, !3, i64 8, !13, i64 12, !13, i64 16}
!84 = !{!9, !13, i64 260}
!85 = !{!9, !3, i64 200}
!86 = !{!23, !18, i64 16}
!87 = !{!23, !13, i64 8}
