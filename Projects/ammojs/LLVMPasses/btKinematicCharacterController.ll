; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Character/btKinematicCharacterController.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Character/btKinematicCharacterController.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVector3 = type { [4 x float] }
%class.btKinematicCharacterController = type <{ %class.btCharacterControllerInterface, float, %class.btPairCachingGhostObject*, %class.btConvexShape*, float, float, float, float, float, float, float, float, float, float, float, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btAlignedObjectArray.9, i8, [3 x i8], %class.btVector3, i8, i8, i8, i8, float, i32, i8, i8, i8, i8 }>
%class.btCharacterControllerInterface = type { %class.btActionInterface }
%class.btActionInterface = type { i32 (...)** }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btPairCachingGhostObject = type { %class.btGhostObject, %class.btHashedOverlappingPairCache* }
%class.btGhostObject = type { %class.btCollisionObject, %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.0, %struct.btOverlapFilterCallback*, i8, [3 x i8], %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btOverlappingPairCallback* }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.3 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%union.anon.3 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btIDebugDraw = type { i32 (...)** }
%class.btKinematicClosestNotMeConvexResultCallback = type { %"struct.btCollisionWorld::ClosestConvexResultCallback", %class.btCollisionObject*, %class.btVector3, float }
%"struct.btCollisionWorld::ClosestConvexResultCallback" = type { %"struct.btCollisionWorld::ConvexResultCallback", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btCollisionObject* }
%"struct.btCollisionWorld::ConvexResultCallback" = type { i32 (...)**, float, i16, i16 }
%"struct.btCollisionWorld::LocalConvexResult" = type { %class.btCollisionObject*, %"struct.btCollisionWorld::LocalShapeInfo"*, %class.btVector3, %class.btVector3, float }
%"struct.btCollisionWorld::LocalShapeInfo" = type { i32, i32 }

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN30btCharacterControllerInterfaceC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z9btRadiansf = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN30btKinematicCharacterControllerdlEPv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN16btCollisionWorld13getBroadphaseEv = comdat any

$_ZN17btCollisionObject19getBroadphaseHandleEv = comdat any

$_ZN16btCollisionWorld13getDispatcherEv = comdat any

$_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv = comdat any

$_ZN16btCollisionWorld15getDispatchInfoEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN20btPersistentManifold15getContactPointEi = comdat any

$_ZNK15btManifoldPoint11getDistanceEv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN17btCollisionObject17setWorldTransformERK11btTransform = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f = comdat any

$_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv = comdat any

$_ZN9btVector315setInterpolate3ERKS_S1_f = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_Z6btFabsf = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_Z5btCosf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf = comdat any

$_ZN17btActionInterfaceC2Ev = comdat any

$_ZN17btActionInterfaceD2Ev = comdat any

$_ZN30btCharacterControllerInterfaceD0Ev = comdat any

$_ZN17btActionInterfaceD0Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_ = comdat any

$_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev = comdat any

$_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy = comdat any

$_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackC2Ev = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackD2Ev = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackD0Ev = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZTS30btCharacterControllerInterface = comdat any

$_ZTS17btActionInterface = comdat any

$_ZTI17btActionInterface = comdat any

$_ZTI30btCharacterControllerInterface = comdat any

$_ZTV30btCharacterControllerInterface = comdat any

$_ZTV17btActionInterface = comdat any

$_ZTV43btKinematicClosestNotMeConvexResultCallback = comdat any

$_ZTS43btKinematicClosestNotMeConvexResultCallback = comdat any

$_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTSN16btCollisionWorld20ConvexResultCallbackE = comdat any

$_ZTIN16btCollisionWorld20ConvexResultCallbackE = comdat any

$_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTI43btKinematicClosestNotMeConvexResultCallback = comdat any

$_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTVN16btCollisionWorld20ConvexResultCallbackE = comdat any

@_ZTV30btKinematicCharacterController = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btKinematicCharacterController to i8*), i8* bitcast (%class.btKinematicCharacterController* (%class.btKinematicCharacterController*)* @_ZN30btKinematicCharacterControllerD1Ev to i8*), i8* bitcast (void (%class.btKinematicCharacterController*)* @_ZN30btKinematicCharacterControllerD0Ev to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)* @_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btIDebugDraw*)* @_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*)* @_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3 to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*, float)* @_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)* @_ZN30btKinematicCharacterController5resetEP16btCollisionWorld to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*)* @_ZN30btKinematicCharacterController4warpERK9btVector3 to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)* @_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)* @_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf to i8*), i8* bitcast (i1 (%class.btKinematicCharacterController*)* @_ZNK30btKinematicCharacterController7canJumpEv to i8*), i8* bitcast (void (%class.btKinematicCharacterController*)* @_ZN30btKinematicCharacterController4jumpEv to i8*), i8* bitcast (i1 (%class.btKinematicCharacterController*)* @_ZNK30btKinematicCharacterController8onGroundEv to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, i1)* @_ZN30btKinematicCharacterController16setUpInterpolateEb to i8*)] }, align 4
@_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection = internal global [3 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection = internal global i32 0, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS30btKinematicCharacterController = hidden constant [33 x i8] c"30btKinematicCharacterController\00", align 1
@_ZTS30btCharacterControllerInterface = linkonce_odr hidden constant [33 x i8] c"30btCharacterControllerInterface\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS17btActionInterface = linkonce_odr hidden constant [20 x i8] c"17btActionInterface\00", comdat, align 1
@_ZTI17btActionInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btActionInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI30btCharacterControllerInterface = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCharacterControllerInterface, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*) }, comdat, align 4
@_ZTI30btKinematicCharacterController = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btKinematicCharacterController, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btCharacterControllerInterface to i8*) }, align 4
@_ZTV30btCharacterControllerInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btCharacterControllerInterface to i8*), i8* bitcast (%class.btActionInterface* (%class.btActionInterface*)* @_ZN17btActionInterfaceD2Ev to i8*), i8* bitcast (void (%class.btCharacterControllerInterface*)* @_ZN30btCharacterControllerInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV17btActionInterface = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*), i8* bitcast (%class.btActionInterface* (%class.btActionInterface*)* @_ZN17btActionInterfaceD2Ev to i8*), i8* bitcast (void (%class.btActionInterface*)* @_ZN17btActionInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV43btKinematicClosestNotMeConvexResultCallback = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI43btKinematicClosestNotMeConvexResultCallback to i8*), i8* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%class.btKinematicClosestNotMeConvexResultCallback*)* @_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%class.btKinematicClosestNotMeConvexResultCallback*, %"struct.btCollisionWorld::LocalConvexResult"*, i1)* @_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb to i8*)] }, comdat, align 4
@_ZTS43btKinematicClosestNotMeConvexResultCallback = linkonce_odr hidden constant [46 x i8] c"43btKinematicClosestNotMeConvexResultCallback\00", comdat, align 1
@_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden constant [50 x i8] c"N16btCollisionWorld27ClosestConvexResultCallbackE\00", comdat, align 1
@_ZTSN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden constant [43 x i8] c"N16btCollisionWorld20ConvexResultCallbackE\00", comdat, align 1
@_ZTIN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([43 x i8], [43 x i8]* @_ZTSN16btCollisionWorld20ConvexResultCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([50 x i8], [50 x i8]* @_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld20ConvexResultCallbackE to i8*) }, comdat, align 4
@_ZTI43btKinematicClosestNotMeConvexResultCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([46 x i8], [46 x i8]* @_ZTS43btKinematicClosestNotMeConvexResultCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE to i8*) }, comdat, align 4
@_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ClosestConvexResultCallback"*)* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::LocalConvexResult"*, i1)* @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb to i8*)] }, comdat, align 4
@_ZTVN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld20ConvexResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN30btKinematicCharacterControllerC1EP24btPairCachingGhostObjectP13btConvexShapefi = hidden unnamed_addr alias %class.btKinematicCharacterController* (%class.btKinematicCharacterController*, %class.btPairCachingGhostObject*, %class.btConvexShape*, float, i32), %class.btKinematicCharacterController* (%class.btKinematicCharacterController*, %class.btPairCachingGhostObject*, %class.btConvexShape*, float, i32)* @_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi
@_ZN30btKinematicCharacterControllerD1Ev = hidden unnamed_addr alias %class.btKinematicCharacterController* (%class.btKinematicCharacterController*), %class.btKinematicCharacterController* (%class.btKinematicCharacterController*)* @_ZN30btKinematicCharacterControllerD2Ev

define hidden void @_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %direction.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %direction, %class.btVector3** %direction.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %ref.tmp2, align 4, !tbaa !6
  %5 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define hidden void @_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %direction.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %magnitude = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %direction, %class.btVector3** %direction.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast float* %magnitude to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %magnitude, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float* nonnull align 4 dereferenceable(4) %magnitude)
  %4 = bitcast float* %magnitude to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !6
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !6
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !6
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

define hidden void @_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %direction.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %direction, %class.btVector3** %direction.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  call void @_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_(%class.btVector3* sret align 4 %ref.tmp, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #6
  ret void
}

define hidden %class.btKinematicCharacterController* @_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi(%class.btKinematicCharacterController* returned %this, %class.btPairCachingGhostObject* %ghostObject, %class.btConvexShape* %convexShape, float %stepHeight, i32 %upAxis) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %ghostObject.addr = alloca %class.btPairCachingGhostObject*, align 4
  %convexShape.addr = alloca %class.btConvexShape*, align 4
  %stepHeight.addr = alloca float, align 4
  %upAxis.addr = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btPairCachingGhostObject* %ghostObject, %class.btPairCachingGhostObject** %ghostObject.addr, align 4, !tbaa !2
  store %class.btConvexShape* %convexShape, %class.btConvexShape** %convexShape.addr, align 4, !tbaa !2
  store float %stepHeight, float* %stepHeight.addr, align 4, !tbaa !6
  store i32 %upAxis, i32* %upAxis.addr, align 4, !tbaa !8
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btKinematicCharacterController* %this1 to %class.btCharacterControllerInterface*
  %call = call %class.btCharacterControllerInterface* @_ZN30btCharacterControllerInterfaceC2Ev(%class.btCharacterControllerInterface* %0)
  %1 = bitcast %class.btKinematicCharacterController* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV30btKinematicCharacterController, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !10
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_walkDirection)
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 16
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalizedDirection)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_currentPosition)
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_targetPosition)
  %m_manifoldArray = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %call6 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.9* %m_manifoldArray)
  %m_touchingNormal = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_touchingNormal)
  %2 = load i32, i32* %upAxis.addr, align 4, !tbaa !8
  %m_upAxis = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  store i32 %2, i32* %m_upAxis, align 4, !tbaa !12
  %m_addedMargin = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 14
  store float 0x3F947AE140000000, float* %m_addedMargin, align 4, !tbaa !18
  %m_walkDirection8 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_walkDirection8, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %6 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  store i8 1, i8* %m_useGhostObjectSweepTest, align 2, !tbaa !19
  %9 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %ghostObject.addr, align 4, !tbaa !2
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  store %class.btPairCachingGhostObject* %9, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %10 = load float, float* %stepHeight.addr, align 4, !tbaa !6
  %m_stepHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  store float %10, float* %m_stepHeight, align 4, !tbaa !21
  %m_turnAngle = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 12
  store float 0.000000e+00, float* %m_turnAngle, align 4, !tbaa !22
  %11 = load %class.btConvexShape*, %class.btConvexShape** %convexShape.addr, align 4, !tbaa !2
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  store %class.btConvexShape* %11, %class.btConvexShape** %m_convexShape, align 4, !tbaa !23
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  store i8 1, i8* %m_useWalkDirection, align 1, !tbaa !24
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 28
  store float 0.000000e+00, float* %m_velocityTimeInterval, align 4, !tbaa !25
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_verticalVelocity, align 4, !tbaa !26
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalOffset, align 4, !tbaa !27
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 11
  store float 0x403D666660000000, float* %m_gravity, align 4, !tbaa !28
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  store float 5.500000e+01, float* %m_fallSpeed, align 4, !tbaa !29
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  store float 1.000000e+01, float* %m_jumpSpeed, align 4, !tbaa !30
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  store i8 0, i8* %m_wasOnGround, align 4, !tbaa !31
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  store i8 0, i8* %m_wasJumping, align 1, !tbaa !32
  %m_interpolateUp = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 30
  store i8 1, i8* %m_interpolateUp, align 4, !tbaa !33
  %call11 = call float @_Z9btRadiansf(float 4.500000e+01)
  call void @_ZN30btKinematicCharacterController11setMaxSlopeEf(%class.btKinematicCharacterController* %this1, float %call11)
  %m_currentStepOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_currentStepOffset, align 4, !tbaa !34
  %full_drop = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 31
  store i8 0, i8* %full_drop, align 1, !tbaa !35
  %bounce_fix = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  store i8 0, i8* %bounce_fix, align 2, !tbaa !36
  ret %class.btKinematicCharacterController* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCharacterControllerInterface* @_ZN30btCharacterControllerInterfaceC2Ev(%class.btCharacterControllerInterface* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCharacterControllerInterface*, align 4
  store %class.btCharacterControllerInterface* %this, %class.btCharacterControllerInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCharacterControllerInterface*, %class.btCharacterControllerInterface** %this.addr, align 4
  %0 = bitcast %class.btCharacterControllerInterface* %this1 to %class.btActionInterface*
  %call = call %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* %0) #6
  %1 = bitcast %class.btCharacterControllerInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV30btCharacterControllerInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !10
  ret %class.btCharacterControllerInterface* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret void
}

define hidden void @_ZN30btKinematicCharacterController11setMaxSlopeEf(%class.btKinematicCharacterController* %this, float %slopeRadians) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %slopeRadians.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store float %slopeRadians, float* %slopeRadians.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %slopeRadians.addr, align 4, !tbaa !6
  %m_maxSlopeRadians = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 9
  store float %0, float* %m_maxSlopeRadians, align 4, !tbaa !37
  %1 = load float, float* %slopeRadians.addr, align 4, !tbaa !6
  %call = call float @_Z5btCosf(float %1)
  %m_maxSlopeCosine = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 10
  store float %call, float* %m_maxSlopeCosine, align 4, !tbaa !38
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z9btRadiansf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %mul = fmul float %0, 0x3F91DF46A0000000
  ret float %mul
}

; Function Attrs: nounwind
define hidden %class.btKinematicCharacterController* @_ZN30btKinematicCharacterControllerD2Ev(%class.btKinematicCharacterController* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btKinematicCharacterController* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV30btKinematicCharacterController, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !10
  %m_manifoldArray = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %call = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.9* %m_manifoldArray) #6
  %1 = bitcast %class.btKinematicCharacterController* %this1 to %class.btCharacterControllerInterface*
  %call2 = call %class.btCharacterControllerInterface* bitcast (%class.btActionInterface* (%class.btActionInterface*)* @_ZN17btActionInterfaceD2Ev to %class.btCharacterControllerInterface* (%class.btCharacterControllerInterface*)*)(%class.btCharacterControllerInterface* %1) #6
  ret %class.btKinematicCharacterController* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN30btKinematicCharacterControllerD0Ev(%class.btKinematicCharacterController* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %call = call %class.btKinematicCharacterController* @_ZN30btKinematicCharacterControllerD1Ev(%class.btKinematicCharacterController* %this1) #6
  %0 = bitcast %class.btKinematicCharacterController* %this1 to i8*
  call void @_ZN30btKinematicCharacterControllerdlEPv(i8* %0) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN30btKinematicCharacterControllerdlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: nounwind
define hidden %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this) #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %0 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  ret %class.btPairCachingGhostObject* %0
}

define hidden zeroext i1 @_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %minAabb = alloca %class.btVector3, align 4
  %maxAabb = alloca %class.btVector3, align 4
  %penetration = alloca i8, align 1
  %maxPen = alloca float, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %collisionPair = alloca %struct.btBroadphasePair*, align 4
  %obj0 = alloca %class.btCollisionObject*, align 4
  %obj1 = alloca %class.btCollisionObject*, align 4
  %j = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %directionSign = alloca float, align 4
  %p = alloca i32, align 4
  %pt = alloca %class.btManifoldPoint*, align 4
  %dist = alloca float, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  %ref.tmp67 = alloca %class.btVector3, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp69 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca float, align 4
  %newTrans = alloca %class.btTransform, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btVector3* %minAabb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minAabb)
  %1 = bitcast %class.btVector3* %maxAabb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %maxAabb)
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %2 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4, !tbaa !23
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %3 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %4 = bitcast %class.btPairCachingGhostObject* %3 to %class.btCollisionObject*
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %4)
  %5 = bitcast %class.btConvexShape* %2 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %5, align 4, !tbaa !10
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %6 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %6(%class.btConvexShape* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %minAabb, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAabb)
  %7 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call4 = call %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %7)
  %m_ghostObject5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %8 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject5, align 4, !tbaa !20
  %9 = bitcast %class.btPairCachingGhostObject* %8 to %class.btCollisionObject*
  %call6 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %9)
  %10 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call7 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %10)
  %11 = bitcast %class.btBroadphaseInterface* %call4 to void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable8 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %11, align 4, !tbaa !10
  %vfn9 = getelementptr inbounds void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable8, i64 4
  %12 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn9, align 4
  call void %12(%class.btBroadphaseInterface* %call4, %struct.btBroadphaseProxy* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %minAabb, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAabb, %class.btDispatcher* %call7)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %penetration) #6
  store i8 0, i8* %penetration, align 1, !tbaa !39
  %13 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call10 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %13)
  %m_ghostObject11 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %14 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject11, align 4, !tbaa !20
  %call12 = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %14)
  %15 = bitcast %class.btHashedOverlappingPairCache* %call12 to %class.btOverlappingPairCache*
  %16 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %16)
  %17 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call14 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %17)
  %18 = bitcast %class.btDispatcher* %call10 to void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)***
  %vtable15 = load void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)**, void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)*** %18, align 4, !tbaa !10
  %vfn16 = getelementptr inbounds void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)*, void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)** %vtable15, i64 8
  %19 = load void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)*, void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)** %vfn16, align 4
  call void %19(%class.btDispatcher* %call10, %class.btOverlappingPairCache* %15, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %call13, %class.btDispatcher* %call14)
  %m_ghostObject17 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %20 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject17, align 4, !tbaa !20
  %21 = bitcast %class.btPairCachingGhostObject* %20 to %class.btCollisionObject*
  %call18 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %21)
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call18)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %22 = bitcast %class.btVector3* %m_currentPosition to i8*
  %23 = bitcast %class.btVector3* %call19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false), !tbaa.struct !40
  %24 = bitcast float* %maxPen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store float 0.000000e+00, float* %maxPen, align 4, !tbaa !6
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc80, %entry
  %26 = load i32, i32* %i, align 4, !tbaa !8
  %m_ghostObject20 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %27 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject20, align 4, !tbaa !20
  %call21 = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %27)
  %28 = bitcast %class.btHashedOverlappingPairCache* %call21 to i32 (%class.btHashedOverlappingPairCache*)***
  %vtable22 = load i32 (%class.btHashedOverlappingPairCache*)**, i32 (%class.btHashedOverlappingPairCache*)*** %28, align 4, !tbaa !10
  %vfn23 = getelementptr inbounds i32 (%class.btHashedOverlappingPairCache*)*, i32 (%class.btHashedOverlappingPairCache*)** %vtable22, i64 9
  %29 = load i32 (%class.btHashedOverlappingPairCache*)*, i32 (%class.btHashedOverlappingPairCache*)** %vfn23, align 4
  %call24 = call i32 %29(%class.btHashedOverlappingPairCache* %call21)
  %cmp = icmp slt i32 %26, %call24
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  br label %for.end83

for.body:                                         ; preds = %for.cond
  %m_manifoldArray = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %31 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %m_manifoldArray, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  %32 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #6
  %33 = bitcast %struct.btBroadphasePair** %collisionPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  %m_ghostObject25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %34 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject25, align 4, !tbaa !20
  %call26 = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %34)
  %35 = bitcast %class.btHashedOverlappingPairCache* %call26 to %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)***
  %vtable27 = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*** %35, align 4, !tbaa !10
  %vfn28 = getelementptr inbounds %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vtable27, i64 7
  %36 = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vfn28, align 4
  %call29 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.0* %36(%class.btHashedOverlappingPairCache* %call26)
  %37 = load i32, i32* %i, align 4, !tbaa !8
  %call30 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.0* %call29, i32 %37)
  store %struct.btBroadphasePair* %call30, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %38 = bitcast %class.btCollisionObject** %obj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %39 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %39, i32 0, i32 0
  %40 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !42
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %40, i32 0, i32 0
  %41 = load i8*, i8** %m_clientObject, align 4, !tbaa !44
  %42 = bitcast i8* %41 to %class.btCollisionObject*
  store %class.btCollisionObject* %42, %class.btCollisionObject** %obj0, align 4, !tbaa !2
  %43 = bitcast %class.btCollisionObject** %obj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  %44 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %44, i32 0, i32 1
  %45 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !47
  %m_clientObject31 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %45, i32 0, i32 0
  %46 = load i8*, i8** %m_clientObject31, align 4, !tbaa !44
  %47 = bitcast i8* %46 to %class.btCollisionObject*
  store %class.btCollisionObject* %47, %class.btCollisionObject** %obj1, align 4, !tbaa !2
  %48 = load %class.btCollisionObject*, %class.btCollisionObject** %obj0, align 4, !tbaa !2
  %tobool = icmp ne %class.btCollisionObject* %48, null
  br i1 %tobool, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %for.body
  %49 = load %class.btCollisionObject*, %class.btCollisionObject** %obj0, align 4, !tbaa !2
  %call32 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %49)
  br i1 %call32, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %land.lhs.true, %for.body
  %50 = load %class.btCollisionObject*, %class.btCollisionObject** %obj1, align 4, !tbaa !2
  %tobool33 = icmp ne %class.btCollisionObject* %50, null
  br i1 %tobool33, label %land.lhs.true34, label %if.end

land.lhs.true34:                                  ; preds = %lor.lhs.false
  %51 = load %class.btCollisionObject*, %class.btCollisionObject** %obj1, align 4, !tbaa !2
  %call35 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %51)
  br i1 %call35, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true34, %land.lhs.true
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true34, %lor.lhs.false
  %52 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %52, i32 0, i32 2
  %53 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !48
  %tobool36 = icmp ne %class.btCollisionAlgorithm* %53, null
  br i1 %tobool36, label %if.then37, label %if.end42

if.then37:                                        ; preds = %if.end
  %54 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4, !tbaa !2
  %m_algorithm38 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %54, i32 0, i32 2
  %55 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm38, align 4, !tbaa !48
  %m_manifoldArray39 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %56 = bitcast %class.btCollisionAlgorithm* %55 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.9*)***
  %vtable40 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.9*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.9*)*** %56, align 4, !tbaa !10
  %vfn41 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.9*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.9*)** %vtable40, i64 4
  %57 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.9*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.9*)** %vfn41, align 4
  call void %57(%class.btCollisionAlgorithm* %55, %class.btAlignedObjectArray.9* nonnull align 4 dereferenceable(17) %m_manifoldArray39)
  br label %if.end42

if.end42:                                         ; preds = %if.then37, %if.end
  %58 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  store i32 0, i32* %j, align 4, !tbaa !8
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc75, %if.end42
  %59 = load i32, i32* %j, align 4, !tbaa !8
  %m_manifoldArray44 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %call45 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_manifoldArray44)
  %cmp46 = icmp slt i32 %59, %call45
  br i1 %cmp46, label %for.body48, label %for.cond.cleanup47

for.cond.cleanup47:                               ; preds = %for.cond43
  store i32 5, i32* %cleanup.dest.slot, align 4
  %60 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  br label %for.end77

for.body48:                                       ; preds = %for.cond43
  %61 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  %m_manifoldArray49 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %62 = load i32, i32* %j, align 4, !tbaa !8
  %call50 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %m_manifoldArray49, i32 %62)
  %63 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call50, align 4, !tbaa !2
  store %class.btPersistentManifold* %63, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %64 = bitcast float* %directionSign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #6
  %65 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %call51 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %65)
  %m_ghostObject52 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %66 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject52, align 4, !tbaa !20
  %67 = bitcast %class.btPairCachingGhostObject* %66 to %class.btCollisionObject*
  %cmp53 = icmp eq %class.btCollisionObject* %call51, %67
  %68 = zext i1 %cmp53 to i64
  %cond = select i1 %cmp53, float -1.000000e+00, float 1.000000e+00
  store float %cond, float* %directionSign, align 4, !tbaa !6
  %69 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  store i32 0, i32* %p, align 4, !tbaa !8
  br label %for.cond54

for.cond54:                                       ; preds = %for.inc, %for.body48
  %70 = load i32, i32* %p, align 4, !tbaa !8
  %71 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %call55 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %71)
  %cmp56 = icmp slt i32 %70, %call55
  br i1 %cmp56, label %for.body58, label %for.cond.cleanup57

for.cond.cleanup57:                               ; preds = %for.cond54
  store i32 8, i32* %cleanup.dest.slot, align 4
  %72 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #6
  br label %for.end

for.body58:                                       ; preds = %for.cond54
  %73 = bitcast %class.btManifoldPoint** %pt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4, !tbaa !2
  %75 = load i32, i32* %p, align 4, !tbaa !8
  %call59 = call nonnull align 4 dereferenceable(184) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %74, i32 %75)
  store %class.btManifoldPoint* %call59, %class.btManifoldPoint** %pt, align 4, !tbaa !2
  %76 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #6
  %77 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4, !tbaa !2
  %call60 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %77)
  store float %call60, float* %dist, align 4, !tbaa !6
  %78 = load float, float* %dist, align 4, !tbaa !6
  %conv = fpext float %78 to double
  %cmp61 = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp61, label %if.then62, label %if.else

if.then62:                                        ; preds = %for.body58
  %79 = load float, float* %dist, align 4, !tbaa !6
  %80 = load float, float* %maxPen, align 4, !tbaa !6
  %cmp63 = fcmp olt float %79, %80
  br i1 %cmp63, label %if.then64, label %if.end66

if.then64:                                        ; preds = %if.then62
  %81 = load float, float* %dist, align 4, !tbaa !6
  store float %81, float* %maxPen, align 4, !tbaa !6
  %82 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %82) #6
  %83 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4, !tbaa !2
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %83, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB, float* nonnull align 4 dereferenceable(4) %directionSign)
  %m_touchingNormal = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %84 = bitcast %class.btVector3* %m_touchingNormal to i8*
  %85 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 16, i1 false), !tbaa.struct !40
  %86 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #6
  br label %if.end66

if.end66:                                         ; preds = %if.then64, %if.then62
  %87 = bitcast %class.btVector3* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %87) #6
  %88 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %88) #6
  %89 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %89) #6
  %90 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4, !tbaa !2
  %m_normalWorldOnB70 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %90, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp69, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB70, float* nonnull align 4 dereferenceable(4) %directionSign)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp69, float* nonnull align 4 dereferenceable(4) %dist)
  %91 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #6
  store float 0x3FC99999A0000000, float* %ref.tmp71, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp67, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp71)
  %m_currentPosition72 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call73 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_currentPosition72, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp67)
  %92 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #6
  %93 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #6
  %94 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #6
  %95 = bitcast %class.btVector3* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #6
  store i8 1, i8* %penetration, align 1, !tbaa !39
  br label %if.end74

if.else:                                          ; preds = %for.body58
  br label %if.end74

if.end74:                                         ; preds = %if.else, %if.end66
  %96 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #6
  %97 = bitcast %class.btManifoldPoint** %pt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #6
  br label %for.inc

for.inc:                                          ; preds = %if.end74
  %98 = load i32, i32* %p, align 4, !tbaa !8
  %inc = add nsw i32 %98, 1
  store i32 %inc, i32* %p, align 4, !tbaa !8
  br label %for.cond54

for.end:                                          ; preds = %for.cond.cleanup57
  %99 = bitcast float* %directionSign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #6
  %100 = bitcast %class.btPersistentManifold** %manifold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #6
  br label %for.inc75

for.inc75:                                        ; preds = %for.end
  %101 = load i32, i32* %j, align 4, !tbaa !8
  %inc76 = add nsw i32 %101, 1
  store i32 %inc76, i32* %j, align 4, !tbaa !8
  br label %for.cond43

for.end77:                                        ; preds = %for.cond.cleanup47
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end77, %if.then
  %102 = bitcast %class.btCollisionObject** %obj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #6
  %103 = bitcast %class.btCollisionObject** %obj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #6
  %104 = bitcast %struct.btBroadphasePair** %collisionPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc80
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc80

for.inc80:                                        ; preds = %cleanup.cont, %cleanup
  %105 = load i32, i32* %i, align 4, !tbaa !8
  %inc81 = add nsw i32 %105, 1
  store i32 %inc81, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end83:                                        ; preds = %for.cond.cleanup
  %106 = bitcast %class.btTransform* %newTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %106) #6
  %m_ghostObject84 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %107 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject84, align 4, !tbaa !20
  %108 = bitcast %class.btPairCachingGhostObject* %107 to %class.btCollisionObject*
  %call85 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %108)
  %call86 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %newTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call85)
  %m_currentPosition87 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %newTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition87)
  %m_ghostObject88 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %109 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject88, align 4, !tbaa !20
  %110 = bitcast %class.btPairCachingGhostObject* %109 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %110, %class.btTransform* nonnull align 4 dereferenceable(64) %newTrans)
  %111 = load i8, i8* %penetration, align 1, !tbaa !39, !range !49
  %tobool89 = trunc i8 %111 to i1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %112 = bitcast %class.btTransform* %newTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %112) #6
  %113 = bitcast float* %maxPen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %penetration) #6
  %114 = bitcast %class.btVector3* %maxAabb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %114) #6
  %115 = bitcast %class.btVector3* %minAabb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %115) #6
  ret i1 %tobool89

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4, !tbaa !50
  ret %class.btBroadphaseInterface* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4, !tbaa !55
  ret %struct.btBroadphaseProxy* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 2
  %0 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !59
  ret %class.btDispatcher* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btPairCachingGhostObject*, align 4
  store %class.btPairCachingGhostObject* %this, %class.btPairCachingGhostObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %this.addr, align 4
  %m_hashPairCache = getelementptr inbounds %class.btPairCachingGhostObject, %class.btPairCachingGhostObject* %this1, i32 0, i32 1
  %0 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %m_hashPairCache, align 4, !tbaa !60
  ret %class.btHashedOverlappingPairCache* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 3
  ret %struct.btDispatcherInfo* %m_dispatchInfo
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !8
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !8
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %2 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  store i32 %4, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %6 = load i32, i32* %curSize, align 4, !tbaa !8
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !62
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %curSize, align 4, !tbaa !8
  store i32 %14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !8
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data11, align 4, !tbaa !62
  %19 = load i32, i32* %i6, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %18, i32 %19
  %20 = bitcast %class.btPersistentManifold** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btPersistentManifold**
  %22 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btPersistentManifold*, %class.btPersistentManifold** %22, align 4, !tbaa !2
  store %class.btPersistentManifold* %23, %class.btPersistentManifold** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !8
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !8
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !63
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !64
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !67
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !63
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !62
  %1 = load i32, i32* %n.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !68
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4, !tbaa !70
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %this, i32 %index) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !8
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  ret %class.btManifoldPoint* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %0 = load float, float* %m_distance1, align 4, !tbaa !71
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !6
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !40
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !40
  ret void
}

define linkonce_odr hidden void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %worldTrans.addr = alloca %class.btTransform*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTrans, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !73
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !73
  %1 = load %class.btTransform*, %class.btTransform** %worldTrans.addr, align 4, !tbaa !2
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  ret void
}

define hidden void @_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %world) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %world.addr = alloca %class.btCollisionWorld*, align 4
  %start = alloca %class.btTransform, align 4
  %end = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %callback = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %world, %class.btCollisionWorld** %world.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btTransform* %start to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #6
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %start)
  %1 = bitcast %class.btTransform* %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %1) #6
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %call4 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %4 = load i32, i32* %m_upAxis, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %call4, i32 %4
  %5 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %m_stepHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %6 = load float, float* %m_stepHeight, align 4, !tbaa !21
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %7 = load float, float* %m_verticalOffset, align 4, !tbaa !27
  %cmp = fcmp ogt float %7, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_verticalOffset6 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %8 = load float, float* %m_verticalOffset6, align 4, !tbaa !27
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %8, %cond.true ], [ 0.000000e+00, %cond.false ]
  %add = fadd float %6, %cond
  store float %add, float* %ref.tmp5, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %9 = bitcast %class.btVector3* %m_targetPosition to i8*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !40
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #6
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #6
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %start)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end)
  %14 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #6
  %m_currentPosition8 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %15 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #6
  %call10 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis11 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %16 = load i32, i32* %m_upAxis11, align 4, !tbaa !12
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %call10, i32 %16
  %17 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %18 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4, !tbaa !23
  %19 = bitcast %class.btConvexShape* %18 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %19, align 4, !tbaa !10
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %20 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call14 = call float %20(%class.btConvexShape* %18)
  %m_addedMargin = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 14
  %21 = load float, float* %m_addedMargin, align 4, !tbaa !18
  %add15 = fadd float %call14, %21
  store float %add15, float* %ref.tmp13, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %start, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %22 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #6
  %24 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #6
  %m_targetPosition16 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition16)
  %25 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %25) #6
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %26 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %27 = bitcast %class.btPairCachingGhostObject* %26 to %class.btCollisionObject*
  %28 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #6
  %call18 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis19 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %29 = load i32, i32* %m_upAxis19, align 4, !tbaa !12
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %call18, i32 %29
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  %call21 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback, %class.btCollisionObject* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17, float 0x3FE6A09020000000)
  %30 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #6
  %call22 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %31 = bitcast %class.btPairCachingGhostObject* %call22 to %class.btCollisionObject*
  %call23 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %31)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call23, i32 0, i32 1
  %32 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !74
  %33 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup24 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %33, i32 0, i32 2
  store i16 %32, i16* %m_collisionFilterGroup24, align 4, !tbaa !75
  %call25 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %34 = bitcast %class.btPairCachingGhostObject* %call25 to %class.btCollisionObject*
  %call26 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %34)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call26, i32 0, i32 2
  %35 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !77
  %36 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask27 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %36, i32 0, i32 3
  store i16 %35, i16* %m_collisionFilterMask27, align 2, !tbaa !78
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %37 = load i8, i8* %m_useGhostObjectSweepTest, align 2, !tbaa !19, !range !49
  %tobool = trunc i8 %37 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %m_ghostObject28 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %38 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject28, align 4, !tbaa !20
  %39 = bitcast %class.btPairCachingGhostObject* %38 to %class.btGhostObject*
  %m_convexShape29 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %40 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape29, align 4, !tbaa !23
  %41 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %42 = load %class.btCollisionWorld*, %class.btCollisionWorld** %world.addr, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %42)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call30, i32 0, i32 9
  %43 = load float, float* %m_allowedCcdPenetration, align 4, !tbaa !79
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %39, %class.btConvexShape* %40, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %41, float %43)
  br label %if.end

if.else:                                          ; preds = %cond.end
  %44 = load %class.btCollisionWorld*, %class.btCollisionWorld** %world.addr, align 4, !tbaa !2
  %m_convexShape31 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %45 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape31, align 4, !tbaa !23
  %46 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %44, %class.btConvexShape* %45, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %46, float 0.000000e+00)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %47 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call32 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %47)
  br i1 %call32, label %if.then33, label %if.else55

if.then33:                                        ; preds = %if.end
  %48 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %48, i32 0, i32 3
  %call34 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis35 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %49 = load i32, i32* %m_upAxis35, align 4, !tbaa !12
  %arrayidx36 = getelementptr inbounds %class.btVector3, %class.btVector3* %call34, i32 %49
  %call37 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_hitNormalWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx36)
  %conv = fpext float %call37 to double
  %cmp38 = fcmp ogt double %conv, 0.000000e+00
  br i1 %cmp38, label %if.then39, label %if.end53

if.then39:                                        ; preds = %if.then33
  %m_stepHeight40 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %50 = load float, float* %m_stepHeight40, align 4, !tbaa !21
  %51 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %51, i32 0, i32 1
  %52 = load float, float* %m_closestHitFraction, align 4, !tbaa !80
  %mul = fmul float %50, %52
  %m_currentStepOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  store float %mul, float* %m_currentStepOffset, align 4, !tbaa !34
  %m_interpolateUp = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 30
  %53 = load i8, i8* %m_interpolateUp, align 4, !tbaa !33, !range !49
  %tobool41 = trunc i8 %53 to i1
  %conv42 = zext i1 %tobool41 to i32
  %cmp43 = icmp eq i32 %conv42, 1
  br i1 %cmp43, label %if.then44, label %if.else49

if.then44:                                        ; preds = %if.then39
  %m_currentPosition45 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_currentPosition46 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition47 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %54 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction48 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %54, i32 0, i32 1
  %55 = load float, float* %m_closestHitFraction48, align 4, !tbaa !80
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition45, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition46, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition47, float %55)
  br label %if.end52

if.else49:                                        ; preds = %if.then39
  %m_targetPosition50 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %m_currentPosition51 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %56 = bitcast %class.btVector3* %m_currentPosition51 to i8*
  %57 = bitcast %class.btVector3* %m_targetPosition50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 16, i1 false), !tbaa.struct !40
  br label %if.end52

if.end52:                                         ; preds = %if.else49, %if.then44
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.then33
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_verticalVelocity, align 4, !tbaa !26
  %m_verticalOffset54 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalOffset54, align 4, !tbaa !27
  br label %if.end60

if.else55:                                        ; preds = %if.end
  %m_stepHeight56 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %58 = load float, float* %m_stepHeight56, align 4, !tbaa !21
  %m_currentStepOffset57 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  store float %58, float* %m_currentStepOffset57, align 4, !tbaa !34
  %m_targetPosition58 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %m_currentPosition59 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %59 = bitcast %class.btVector3* %m_currentPosition59 to i8*
  %60 = bitcast %class.btVector3* %m_targetPosition58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 16, i1 false), !tbaa.struct !40
  br label %if.end60

if.end60:                                         ; preds = %if.else55, %if.end53
  %call61 = call %class.btKinematicClosestNotMeConvexResultCallback* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to %class.btKinematicClosestNotMeConvexResultCallback* (%class.btKinematicClosestNotMeConvexResultCallback*)*)(%class.btKinematicClosestNotMeConvexResultCallback* %callback) #6
  %61 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %61) #6
  %62 = bitcast %class.btTransform* %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %62) #6
  %63 = bitcast %class.btTransform* %start to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %63) #6
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

define hidden %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv() #0 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !81

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection) #6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 1.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !6
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !6
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %12 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #6
  %20 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  call void @__cxa_guard_release(i32* @_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection) #6
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !6
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

define linkonce_odr hidden %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* returned %this, %class.btCollisionObject* %me, %class.btVector3* nonnull align 4 dereferenceable(16) %up, float %minSlopeDot) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btKinematicClosestNotMeConvexResultCallback*, align 4
  %me.addr = alloca %class.btCollisionObject*, align 4
  %up.addr = alloca %class.btVector3*, align 4
  %minSlopeDot.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btKinematicClosestNotMeConvexResultCallback* %this, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %me, %class.btCollisionObject** %me.addr, align 4, !tbaa !2
  store %class.btVector3* %up, %class.btVector3** %up.addr, align 4, !tbaa !2
  store float %minSlopeDot, float* %minSlopeDot.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicClosestNotMeConvexResultCallback*, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !6
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %call10 = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %9 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #6
  %13 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  %17 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV43btKinematicClosestNotMeConvexResultCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %17, align 4, !tbaa !10
  %m_me = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %me.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %18, %class.btCollisionObject** %m_me, align 4, !tbaa !82
  %m_up = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 2
  %19 = load %class.btVector3*, %class.btVector3** %up.addr, align 4, !tbaa !2
  %20 = bitcast %class.btVector3* %m_up to i8*
  %21 = bitcast %class.btVector3* %19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !40
  %m_minSlopeDot = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 3
  %22 = load float, float* %minSlopeDot.addr, align 4, !tbaa !6
  store float %22, float* %m_minSlopeDot, align 4, !tbaa !84
  ret %class.btKinematicClosestNotMeConvexResultCallback* %this1
}

declare void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject*, %class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12), float) #5

declare void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld*, %class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12), float) #5

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 1
  %0 = load float, float* %m_closestHitFraction, align 4, !tbaa !80
  %cmp = fcmp olt float %0, 1.000000e+00
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, float %rt) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %rt.addr = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store float %rt, float* %rt.addr, align 4, !tbaa !6
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load float, float* %rt.addr, align 4, !tbaa !6
  %sub = fsub float 1.000000e+00, %1
  store float %sub, float* %s, align 4, !tbaa !6
  %2 = load float, float* %s, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %4 = load float, float* %arrayidx, align 4, !tbaa !6
  %mul = fmul float %2, %4
  %5 = load float, float* %rt.addr, align 4, !tbaa !6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 0
  store float %add, float* %arrayidx6, align 4, !tbaa !6
  %8 = load float, float* %s, align 4, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %10 = load float, float* %arrayidx8, align 4, !tbaa !6
  %mul9 = fmul float %8, %10
  %11 = load float, float* %rt.addr, align 4, !tbaa !6
  %12 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %13 = load float, float* %arrayidx11, align 4, !tbaa !6
  %mul12 = fmul float %11, %13
  %add13 = fadd float %mul9, %mul12
  %m_floats14 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 1
  store float %add13, float* %arrayidx15, align 4, !tbaa !6
  %14 = load float, float* %s, align 4, !tbaa !6
  %15 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %m_floats16 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x float], [4 x float]* %m_floats16, i32 0, i32 2
  %16 = load float, float* %arrayidx17, align 4, !tbaa !6
  %mul18 = fmul float %14, %16
  %17 = load float, float* %rt.addr, align 4, !tbaa !6
  %18 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %19 = load float, float* %arrayidx20, align 4, !tbaa !6
  %mul21 = fmul float %17, %19
  %add22 = fadd float %mul18, %mul21
  %m_floats23 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 2
  store float %add22, float* %arrayidx24, align 4, !tbaa !6
  %20 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  ret void
}

define hidden void @_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %hitNormal, float %tangentMag, float %normalMag) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %hitNormal.addr = alloca %class.btVector3*, align 4
  %tangentMag.addr = alloca float, align 4
  %normalMag.addr = alloca float, align 4
  %movementDirection = alloca %class.btVector3, align 4
  %movementLength = alloca float, align 4
  %reflectDir = alloca %class.btVector3, align 4
  %parallelDir = alloca %class.btVector3, align 4
  %perpindicularDir = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %perpComponent = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %hitNormal, %class.btVector3** %hitNormal.addr, align 4, !tbaa !2
  store float %tangentMag, float* %tangentMag.addr, align 4, !tbaa !6
  store float %normalMag, float* %normalMag.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btVector3* %movementDirection to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %movementDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition)
  %1 = bitcast float* %movementLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %movementDirection)
  store float %call, float* %movementLength, align 4, !tbaa !6
  %2 = load float, float* %movementLength, align 4, !tbaa !6
  %cmp = fcmp ogt float %2, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %movementDirection)
  %3 = bitcast %class.btVector3* %reflectDir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = load %class.btVector3*, %class.btVector3** %hitNormal.addr, align 4, !tbaa !2
  call void @_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_(%class.btVector3* sret align 4 %reflectDir, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %movementDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %reflectDir)
  %5 = bitcast %class.btVector3* %parallelDir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %parallelDir)
  %6 = bitcast %class.btVector3* %perpindicularDir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #6
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %perpindicularDir)
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #6
  %8 = load %class.btVector3*, %class.btVector3** %hitNormal.addr, align 4, !tbaa !2
  call void @_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_(%class.btVector3* sret align 4 %ref.tmp, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %reflectDir, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %9 = bitcast %class.btVector3* %parallelDir to i8*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !40
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #6
  %12 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #6
  %13 = load %class.btVector3*, %class.btVector3** %hitNormal.addr, align 4, !tbaa !2
  call void @_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_(%class.btVector3* sret align 4 %ref.tmp6, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %reflectDir, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %14 = bitcast %class.btVector3* %perpindicularDir to i8*
  %15 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !40
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  %m_currentPosition7 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition8 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %17 = bitcast %class.btVector3* %m_targetPosition8 to i8*
  %18 = bitcast %class.btVector3* %m_currentPosition7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false), !tbaa.struct !40
  %19 = load float, float* %normalMag.addr, align 4, !tbaa !6
  %conv = fpext float %19 to double
  %cmp9 = fcmp une double %conv, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then
  %20 = bitcast %class.btVector3* %perpComponent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #6
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load float, float* %normalMag.addr, align 4, !tbaa !6
  %23 = load float, float* %movementLength, align 4, !tbaa !6
  %mul = fmul float %22, %23
  store float %mul, float* %ref.tmp11, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %perpComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %perpindicularDir, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %24 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #6
  %m_targetPosition12 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_targetPosition12, %class.btVector3* nonnull align 4 dereferenceable(16) %perpComponent)
  %25 = bitcast %class.btVector3* %perpComponent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #6
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then
  %26 = bitcast %class.btVector3* %perpindicularDir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #6
  %27 = bitcast %class.btVector3* %parallelDir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #6
  %28 = bitcast %class.btVector3* %reflectDir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #6
  br label %if.end14

if.else:                                          ; preds = %entry
  br label %if.end14

if.end14:                                         ; preds = %if.else, %if.end
  %29 = bitcast float* %movementLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #6
  %30 = bitcast %class.btVector3* %movementDirection to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #6
  ret %class.btVector3* %call2
}

define hidden void @_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %walkMove) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %walkMove.addr = alloca %class.btVector3*, align 4
  %start = alloca %class.btTransform, align 4
  %end = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %fraction = alloca float, align 4
  %distance2 = alloca float, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %maxIter = alloca i32, align 4
  %sweepDirNegative = alloca %class.btVector3, align 4
  %callback = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %margin = alloca float, align 4
  %hitDistance = alloca float, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %currentDir = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %walkMove, %class.btVector3** %walkMove.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btTransform* %start to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #6
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %start)
  %1 = bitcast %class.btTransform* %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %1) #6
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #6
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %3 = load %class.btVector3*, %class.btVector3** %walkMove.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %4 = bitcast %class.btVector3* %m_targetPosition to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !40
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #6
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %start)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end)
  %7 = bitcast float* %fraction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 1.000000e+00, float* %fraction, align 4, !tbaa !6
  %8 = bitcast float* %distance2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #6
  %m_currentPosition4 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition5)
  %call6 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp3)
  %10 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #6
  store float %call6, float* %distance2, align 4, !tbaa !6
  %m_touchingContact = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %11 = load i8, i8* %m_touchingContact, align 4, !tbaa !85, !range !49
  %tobool = trunc i8 %11 to i1
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 16
  %m_touchingNormal = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_normalizedDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %m_touchingNormal)
  %cmp = fcmp ogt float %call7, 0.000000e+00
  br i1 %cmp, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  %12 = bitcast i32* %maxIter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store i32 10, i32* %maxIter, align 4, !tbaa !8
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont68, %if.end9
  %13 = load float, float* %fraction, align 4, !tbaa !6
  %cmp10 = fcmp ogt float %13, 0x3F847AE140000000
  br i1 %cmp10, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %14 = load i32, i32* %maxIter, align 4, !tbaa !8
  %dec = add nsw i32 %14, -1
  store i32 %dec, i32* %maxIter, align 4, !tbaa !8
  %cmp11 = icmp sgt i32 %14, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %15 = phi i1 [ false, %while.cond ], [ %cmp11, %land.rhs ]
  br i1 %15, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_currentPosition12 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %start, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition12)
  %m_targetPosition13 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition13)
  %16 = bitcast %class.btVector3* %sweepDirNegative to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #6
  %m_currentPosition14 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition15 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %sweepDirNegative, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition15)
  %17 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %17) #6
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %18 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %19 = bitcast %class.btPairCachingGhostObject* %18 to %class.btCollisionObject*
  %call16 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback, %class.btCollisionObject* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %sweepDirNegative, float 0.000000e+00)
  %call17 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %20 = bitcast %class.btPairCachingGhostObject* %call17 to %class.btCollisionObject*
  %call18 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %20)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call18, i32 0, i32 1
  %21 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !74
  %22 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup19 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %22, i32 0, i32 2
  store i16 %21, i16* %m_collisionFilterGroup19, align 4, !tbaa !75
  %call20 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %23 = bitcast %class.btPairCachingGhostObject* %call20 to %class.btCollisionObject*
  %call21 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %23)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call21, i32 0, i32 2
  %24 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !77
  %25 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask22 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %25, i32 0, i32 3
  store i16 %24, i16* %m_collisionFilterMask22, align 2, !tbaa !78
  %26 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %27 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4, !tbaa !23
  %28 = bitcast %class.btConvexShape* %27 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %28, align 4, !tbaa !10
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %29 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call23 = call float %29(%class.btConvexShape* %27)
  store float %call23, float* %margin, align 4, !tbaa !6
  %m_convexShape24 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %30 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape24, align 4, !tbaa !23
  %31 = load float, float* %margin, align 4, !tbaa !6
  %m_addedMargin = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 14
  %32 = load float, float* %m_addedMargin, align 4, !tbaa !18
  %add = fadd float %31, %32
  %33 = bitcast %class.btConvexShape* %30 to void (%class.btConvexShape*, float)***
  %vtable25 = load void (%class.btConvexShape*, float)**, void (%class.btConvexShape*, float)*** %33, align 4, !tbaa !10
  %vfn26 = getelementptr inbounds void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vtable25, i64 11
  %34 = load void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vfn26, align 4
  call void %34(%class.btConvexShape* %30, float %add)
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %35 = load i8, i8* %m_useGhostObjectSweepTest, align 2, !tbaa !19, !range !49
  %tobool27 = trunc i8 %35 to i1
  br i1 %tobool27, label %if.then28, label %if.else

if.then28:                                        ; preds = %while.body
  %m_ghostObject29 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %36 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject29, align 4, !tbaa !20
  %37 = bitcast %class.btPairCachingGhostObject* %36 to %class.btGhostObject*
  %m_convexShape30 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %38 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape30, align 4, !tbaa !23
  %39 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %40 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call31 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %40)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call31, i32 0, i32 9
  %41 = load float, float* %m_allowedCcdPenetration, align 4, !tbaa !79
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %37, %class.btConvexShape* %38, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %39, float %41)
  br label %if.end35

if.else:                                          ; preds = %while.body
  %42 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %m_convexShape32 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %43 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape32, align 4, !tbaa !23
  %44 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %45 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %45)
  %m_allowedCcdPenetration34 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call33, i32 0, i32 9
  %46 = load float, float* %m_allowedCcdPenetration34, align 4, !tbaa !79
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %42, %class.btConvexShape* %43, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %44, float %46)
  br label %if.end35

if.end35:                                         ; preds = %if.else, %if.then28
  %m_convexShape36 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %47 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape36, align 4, !tbaa !23
  %48 = load float, float* %margin, align 4, !tbaa !6
  %49 = bitcast %class.btConvexShape* %47 to void (%class.btConvexShape*, float)***
  %vtable37 = load void (%class.btConvexShape*, float)**, void (%class.btConvexShape*, float)*** %49, align 4, !tbaa !10
  %vfn38 = getelementptr inbounds void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vtable37, i64 11
  %50 = load void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vfn38, align 4
  call void %50(%class.btConvexShape* %47, float %48)
  %51 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %51, i32 0, i32 1
  %52 = load float, float* %m_closestHitFraction, align 4, !tbaa !80
  %53 = load float, float* %fraction, align 4, !tbaa !6
  %sub = fsub float %53, %52
  store float %sub, float* %fraction, align 4, !tbaa !6
  %54 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call39 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %54)
  br i1 %call39, label %if.then40, label %if.else58

if.then40:                                        ; preds = %if.end35
  %55 = bitcast float* %hitDistance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #6
  %56 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #6
  %57 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %57, i32 0, i32 4
  %m_currentPosition42 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitPointWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition42)
  %call43 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp41)
  store float %call43, float* %hitDistance, align 4, !tbaa !6
  %58 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #6
  %59 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %59, i32 0, i32 3
  call void @_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff(%class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalWorld, float 0.000000e+00, float 1.000000e+00)
  %60 = bitcast %class.btVector3* %currentDir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #6
  %m_targetPosition44 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %m_currentPosition45 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %currentDir, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition44, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition45)
  %call46 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %currentDir)
  store float %call46, float* %distance2, align 4, !tbaa !6
  %61 = load float, float* %distance2, align 4, !tbaa !6
  %cmp47 = fcmp ogt float %61, 0x3E80000000000000
  br i1 %cmp47, label %if.then48, label %if.else55

if.then48:                                        ; preds = %if.then40
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %currentDir)
  %m_normalizedDirection50 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 16
  %call51 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %currentDir, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalizedDirection50)
  %cmp52 = fcmp ole float %call51, 0.000000e+00
  br i1 %cmp52, label %if.then53, label %if.end54

if.then53:                                        ; preds = %if.then48
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end54:                                         ; preds = %if.then48
  br label %if.end56

if.else55:                                        ; preds = %if.then40
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end56:                                         ; preds = %if.end54
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end56, %if.else55, %if.then53
  %62 = bitcast %class.btVector3* %currentDir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #6
  %63 = bitcast float* %hitDistance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup62 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end61

if.else58:                                        ; preds = %if.end35
  %m_targetPosition59 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %m_currentPosition60 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %64 = bitcast %class.btVector3* %m_currentPosition60 to i8*
  %65 = bitcast %class.btVector3* %m_targetPosition59 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %64, i8* align 4 %65, i32 16, i1 false), !tbaa.struct !40
  br label %if.end61

if.end61:                                         ; preds = %if.else58, %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup62

cleanup62:                                        ; preds = %if.end61, %cleanup
  %66 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %call64 = call %class.btKinematicClosestNotMeConvexResultCallback* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to %class.btKinematicClosestNotMeConvexResultCallback* (%class.btKinematicClosestNotMeConvexResultCallback*)*)(%class.btKinematicClosestNotMeConvexResultCallback* %callback) #6
  %67 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %67) #6
  %68 = bitcast %class.btVector3* %sweepDirNegative to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #6
  %cleanup.dest67 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest67, label %unreachable [
    i32 0, label %cleanup.cont68
    i32 3, label %while.end
  ]

cleanup.cont68:                                   ; preds = %cleanup62
  br label %while.cond

while.end:                                        ; preds = %cleanup62, %land.end
  %69 = bitcast i32* %maxIter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast float* %distance2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  %71 = bitcast float* %fraction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  %72 = bitcast %class.btTransform* %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %72) #6
  %73 = bitcast %class.btTransform* %start to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %73) #6
  ret void

unreachable:                                      ; preds = %cleanup62
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

define hidden void @_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, float %dt) #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %dt.addr = alloca float, align 4
  %start = alloca %class.btTransform, align 4
  %end = alloca %class.btTransform, align 4
  %end_double = alloca %class.btTransform, align 4
  %runonce = alloca i8, align 1
  %orig_position = alloca %class.btVector3, align 4
  %downVelocity = alloca float, align 4
  %step_drop = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %callback = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %callback2 = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %downVelocity2 = alloca float, align 4
  %has_hit = alloca i8, align 1
  %step_drop101 = alloca %class.btVector3, align 4
  %ref.tmp105 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %fraction = alloca float, align 4
  %ref.tmp175 = alloca %class.btVector3, align 4
  %ref.tmp179 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  store float %dt, float* %dt.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btTransform* %start to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #6
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %start)
  %1 = bitcast %class.btTransform* %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %1) #6
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end)
  %2 = bitcast %class.btTransform* %end_double to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #6
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end_double)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %runonce) #6
  store i8 0, i8* %runonce, align 1, !tbaa !39
  %3 = bitcast %class.btVector3* %orig_position to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %4 = bitcast %class.btVector3* %orig_position to i8*
  %5 = bitcast %class.btVector3* %m_targetPosition to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !40
  %6 = bitcast float* %downVelocity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %7 = load float, float* %m_verticalVelocity, align 4, !tbaa !26
  %cmp = fcmp olt float %7, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_verticalVelocity4 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %8 = load float, float* %m_verticalVelocity4, align 4, !tbaa !26
  %fneg = fneg float %8
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ 0.000000e+00, %cond.false ]
  %9 = load float, float* %dt.addr, align 4, !tbaa !6
  %mul = fmul float %cond, %9
  store float %mul, float* %downVelocity, align 4, !tbaa !6
  %10 = load float, float* %downVelocity, align 4, !tbaa !6
  %conv = fpext float %10 to double
  %cmp5 = fcmp ogt double %conv, 0.000000e+00
  br i1 %cmp5, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end
  %11 = load float, float* %downVelocity, align 4, !tbaa !6
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %12 = load float, float* %m_fallSpeed, align 4, !tbaa !29
  %cmp6 = fcmp ogt float %11, %12
  br i1 %cmp6, label %land.lhs.true7, label %if.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %13 = load i8, i8* %m_wasOnGround, align 4, !tbaa !31, !range !49
  %tobool = trunc i8 %13 to i1
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true7
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  %14 = load i8, i8* %m_wasJumping, align 1, !tbaa !32, !range !49
  %tobool8 = trunc i8 %14 to i1
  br i1 %tobool8, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %land.lhs.true7
  %m_fallSpeed9 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %15 = load float, float* %m_fallSpeed9, align 4, !tbaa !29
  store float %15, float* %downVelocity, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false, %land.lhs.true, %cond.end
  %16 = bitcast %class.btVector3* %step_drop to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #6
  %call10 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %17 = load i32, i32* %m_upAxis, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %call10, i32 %17
  %18 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %m_currentStepOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  %19 = load float, float* %m_currentStepOffset, align 4, !tbaa !34
  %20 = load float, float* %downVelocity, align 4, !tbaa !6
  %add = fadd float %19, %20
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %step_drop, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %21 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %m_targetPosition11 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_targetPosition11, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  %22 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %22) #6
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %23 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %24 = bitcast %class.btPairCachingGhostObject* %23 to %class.btCollisionObject*
  %call13 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis14 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %25 = load i32, i32* %m_upAxis14, align 4, !tbaa !12
  %arrayidx15 = getelementptr inbounds %class.btVector3, %class.btVector3* %call13, i32 %25
  %m_maxSlopeCosine = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 10
  %26 = load float, float* %m_maxSlopeCosine, align 4, !tbaa !38
  %call16 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback, %class.btCollisionObject* %24, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15, float %26)
  %call17 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %27 = bitcast %class.btPairCachingGhostObject* %call17 to %class.btCollisionObject*
  %call18 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %27)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call18, i32 0, i32 1
  %28 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !74
  %29 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup19 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %29, i32 0, i32 2
  store i16 %28, i16* %m_collisionFilterGroup19, align 4, !tbaa !75
  %call20 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %30 = bitcast %class.btPairCachingGhostObject* %call20 to %class.btCollisionObject*
  %call21 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %30)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call21, i32 0, i32 2
  %31 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !77
  %32 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask22 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %32, i32 0, i32 3
  store i16 %31, i16* %m_collisionFilterMask22, align 2, !tbaa !78
  %33 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %33) #6
  %m_ghostObject23 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %34 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject23, align 4, !tbaa !20
  %35 = bitcast %class.btPairCachingGhostObject* %34 to %class.btCollisionObject*
  %call24 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %36 = load i32, i32* %m_upAxis25, align 4, !tbaa !12
  %arrayidx26 = getelementptr inbounds %class.btVector3, %class.btVector3* %call24, i32 %36
  %m_maxSlopeCosine27 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 10
  %37 = load float, float* %m_maxSlopeCosine27, align 4, !tbaa !38
  %call28 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback2, %class.btCollisionObject* %35, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx26, float %37)
  %call29 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %38 = bitcast %class.btPairCachingGhostObject* %call29 to %class.btCollisionObject*
  %call30 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %38)
  %m_collisionFilterGroup31 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call30, i32 0, i32 1
  %39 = load i16, i16* %m_collisionFilterGroup31, align 4, !tbaa !74
  %40 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup32 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %40, i32 0, i32 2
  store i16 %39, i16* %m_collisionFilterGroup32, align 4, !tbaa !75
  %call33 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %41 = bitcast %class.btPairCachingGhostObject* %call33 to %class.btCollisionObject*
  %call34 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %41)
  %m_collisionFilterMask35 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call34, i32 0, i32 2
  %42 = load i16, i16* %m_collisionFilterMask35, align 2, !tbaa !77
  %43 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask36 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %43, i32 0, i32 3
  store i16 %42, i16* %m_collisionFilterMask36, align 2, !tbaa !78
  br label %while.cond

while.cond:                                       ; preds = %cleanup, %if.end
  br label %while.body

while.body:                                       ; preds = %while.cond
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %start)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end_double)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %start, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition)
  %m_targetPosition37 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition37)
  %44 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #6
  %m_targetPosition39 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp38, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition39, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end_double, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  %45 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #6
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %46 = load i8, i8* %m_useGhostObjectSweepTest, align 2, !tbaa !19, !range !49
  %tobool40 = trunc i8 %46 to i1
  br i1 %tobool40, label %if.then41, label %if.else

if.then41:                                        ; preds = %while.body
  %m_ghostObject42 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %47 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject42, align 4, !tbaa !20
  %48 = bitcast %class.btPairCachingGhostObject* %47 to %class.btGhostObject*
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %49 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4, !tbaa !23
  %50 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %51 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %51)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call43, i32 0, i32 9
  %52 = load float, float* %m_allowedCcdPenetration, align 4, !tbaa !79
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %48, %class.btConvexShape* %49, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %50, float %52)
  %53 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call44 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %53)
  br i1 %call44, label %if.end50, label %if.then45

if.then45:                                        ; preds = %if.then41
  %m_ghostObject46 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %54 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject46, align 4, !tbaa !20
  %55 = bitcast %class.btPairCachingGhostObject* %54 to %class.btGhostObject*
  %m_convexShape47 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %56 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape47, align 4, !tbaa !23
  %57 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %58 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call48 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %58)
  %m_allowedCcdPenetration49 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call48, i32 0, i32 9
  %59 = load float, float* %m_allowedCcdPenetration49, align 4, !tbaa !79
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %55, %class.btConvexShape* %56, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end_double, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %57, float %59)
  br label %if.end50

if.end50:                                         ; preds = %if.then45, %if.then41
  br label %if.end60

if.else:                                          ; preds = %while.body
  %60 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %m_convexShape51 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %61 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape51, align 4, !tbaa !23
  %62 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %63 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call52 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %63)
  %m_allowedCcdPenetration53 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call52, i32 0, i32 9
  %64 = load float, float* %m_allowedCcdPenetration53, align 4, !tbaa !79
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %60, %class.btConvexShape* %61, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %62, float %64)
  %65 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call54 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %65)
  br i1 %call54, label %if.end59, label %if.then55

if.then55:                                        ; preds = %if.else
  %66 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %m_convexShape56 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %67 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape56, align 4, !tbaa !23
  %68 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %69 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call57 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %69)
  %m_allowedCcdPenetration58 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call57, i32 0, i32 9
  %70 = load float, float* %m_allowedCcdPenetration58, align 4, !tbaa !79
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %66, %class.btConvexShape* %67, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end_double, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(12) %68, float %70)
  br label %if.end59

if.end59:                                         ; preds = %if.then55, %if.else
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %if.end50
  %71 = bitcast float* %downVelocity2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  %m_verticalVelocity61 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %72 = load float, float* %m_verticalVelocity61, align 4, !tbaa !26
  %cmp62 = fcmp olt float %72, 0.000000e+00
  br i1 %cmp62, label %cond.true63, label %cond.false66

cond.true63:                                      ; preds = %if.end60
  %m_verticalVelocity64 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %73 = load float, float* %m_verticalVelocity64, align 4, !tbaa !26
  %fneg65 = fneg float %73
  br label %cond.end67

cond.false66:                                     ; preds = %if.end60
  br label %cond.end67

cond.end67:                                       ; preds = %cond.false66, %cond.true63
  %cond68 = phi float [ %fneg65, %cond.true63 ], [ 0.000000e+00, %cond.false66 ]
  %74 = load float, float* %dt.addr, align 4, !tbaa !6
  %mul69 = fmul float %cond68, %74
  store float %mul69, float* %downVelocity2, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %has_hit) #6
  store i8 0, i8* %has_hit, align 1, !tbaa !39
  %bounce_fix = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  %75 = load i8, i8* %bounce_fix, align 2, !tbaa !36, !range !49
  %tobool70 = trunc i8 %75 to i1
  %conv71 = zext i1 %tobool70 to i32
  %cmp72 = icmp eq i32 %conv71, 1
  br i1 %cmp72, label %if.then73, label %if.else76

if.then73:                                        ; preds = %cond.end67
  %76 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call74 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %76)
  br i1 %call74, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then73
  %77 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call75 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %77)
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.then73
  %78 = phi i1 [ true, %if.then73 ], [ %call75, %lor.rhs ]
  %frombool = zext i1 %78 to i8
  store i8 %frombool, i8* %has_hit, align 1, !tbaa !39
  br label %if.end79

if.else76:                                        ; preds = %cond.end67
  %79 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call77 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %79)
  %frombool78 = zext i1 %call77 to i8
  store i8 %frombool78, i8* %has_hit, align 1, !tbaa !39
  br label %if.end79

if.end79:                                         ; preds = %if.else76, %lor.end
  %80 = load float, float* %downVelocity2, align 4, !tbaa !6
  %conv80 = fpext float %80 to double
  %cmp81 = fcmp ogt double %conv80, 0.000000e+00
  br i1 %cmp81, label %land.lhs.true82, label %if.end110

land.lhs.true82:                                  ; preds = %if.end79
  %81 = load float, float* %downVelocity2, align 4, !tbaa !6
  %m_stepHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %82 = load float, float* %m_stepHeight, align 4, !tbaa !21
  %cmp83 = fcmp olt float %81, %82
  br i1 %cmp83, label %land.lhs.true84, label %if.end110

land.lhs.true84:                                  ; preds = %land.lhs.true82
  %83 = load i8, i8* %has_hit, align 1, !tbaa !39, !range !49
  %tobool85 = trunc i8 %83 to i1
  %conv86 = zext i1 %tobool85 to i32
  %cmp87 = icmp eq i32 %conv86, 1
  br i1 %cmp87, label %land.lhs.true88, label %if.end110

land.lhs.true88:                                  ; preds = %land.lhs.true84
  %84 = load i8, i8* %runonce, align 1, !tbaa !39, !range !49
  %tobool89 = trunc i8 %84 to i1
  %conv90 = zext i1 %tobool89 to i32
  %cmp91 = icmp eq i32 %conv90, 0
  br i1 %cmp91, label %land.lhs.true92, label %if.end110

land.lhs.true92:                                  ; preds = %land.lhs.true88
  %m_wasOnGround93 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %85 = load i8, i8* %m_wasOnGround93, align 4, !tbaa !31, !range !49
  %tobool94 = trunc i8 %85 to i1
  br i1 %tobool94, label %if.then98, label %lor.lhs.false95

lor.lhs.false95:                                  ; preds = %land.lhs.true92
  %m_wasJumping96 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  %86 = load i8, i8* %m_wasJumping96, align 1, !tbaa !32, !range !49
  %tobool97 = trunc i8 %86 to i1
  br i1 %tobool97, label %if.end110, label %if.then98

if.then98:                                        ; preds = %lor.lhs.false95, %land.lhs.true92
  %m_targetPosition99 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %87 = bitcast %class.btVector3* %m_targetPosition99 to i8*
  %88 = bitcast %class.btVector3* %orig_position to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %87, i8* align 4 %88, i32 16, i1 false), !tbaa.struct !40
  %m_stepHeight100 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %89 = load float, float* %m_stepHeight100, align 4, !tbaa !21
  store float %89, float* %downVelocity, align 4, !tbaa !6
  %90 = bitcast %class.btVector3* %step_drop101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %90) #6
  %call102 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis103 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %91 = load i32, i32* %m_upAxis103, align 4, !tbaa !12
  %arrayidx104 = getelementptr inbounds %class.btVector3, %class.btVector3* %call102, i32 %91
  %92 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  %m_currentStepOffset106 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  %93 = load float, float* %m_currentStepOffset106, align 4, !tbaa !34
  %94 = load float, float* %downVelocity, align 4, !tbaa !6
  %add107 = fadd float %93, %94
  store float %add107, float* %ref.tmp105, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %step_drop101, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx104, float* nonnull align 4 dereferenceable(4) %ref.tmp105)
  %95 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #6
  %m_targetPosition108 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call109 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_targetPosition108, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop101)
  store i8 1, i8* %runonce, align 1, !tbaa !39
  store i32 2, i32* %cleanup.dest.slot, align 4
  %96 = bitcast %class.btVector3* %step_drop101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #6
  br label %cleanup

if.end110:                                        ; preds = %lor.lhs.false95, %land.lhs.true88, %land.lhs.true84, %land.lhs.true82, %if.end79
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end110, %if.then98
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %has_hit) #6
  %97 = bitcast float* %downVelocity2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %while.cond
    i32 3, label %while.end
  ]

while.end:                                        ; preds = %cleanup
  %98 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call112 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %98)
  br i1 %call112, label %if.then117, label %lor.lhs.false113

lor.lhs.false113:                                 ; preds = %while.end
  %99 = load i8, i8* %runonce, align 1, !tbaa !39, !range !49
  %tobool114 = trunc i8 %99 to i1
  %conv115 = zext i1 %tobool114 to i32
  %cmp116 = icmp eq i32 %conv115, 1
  br i1 %cmp116, label %if.then117, label %if.else147

if.then117:                                       ; preds = %lor.lhs.false113, %while.end
  %100 = bitcast float* %fraction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  %m_currentPosition118 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call119 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_currentPosition118)
  %101 = load float, float* %call119, align 4, !tbaa !6
  %102 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %102, i32 0, i32 4
  %call120 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_hitPointWorld)
  %103 = load float, float* %call120, align 4, !tbaa !6
  %sub = fsub float %101, %103
  %div = fdiv float %sub, 2.000000e+00
  store float %div, float* %fraction, align 4, !tbaa !6
  %bounce_fix121 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  %104 = load i8, i8* %bounce_fix121, align 2, !tbaa !36, !range !49
  %tobool122 = trunc i8 %104 to i1
  %conv123 = zext i1 %tobool122 to i32
  %cmp124 = icmp eq i32 %conv123, 1
  br i1 %cmp124, label %if.then125, label %if.else138

if.then125:                                       ; preds = %if.then117
  %full_drop = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 31
  %105 = load i8, i8* %full_drop, align 1, !tbaa !35, !range !49
  %tobool126 = trunc i8 %105 to i1
  %conv127 = zext i1 %tobool126 to i32
  %cmp128 = icmp eq i32 %conv127, 1
  br i1 %cmp128, label %if.then129, label %if.else133

if.then129:                                       ; preds = %if.then125
  %m_currentPosition130 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_currentPosition131 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition132 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %106 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %106, i32 0, i32 1
  %107 = load float, float* %m_closestHitFraction, align 4, !tbaa !80
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition130, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition131, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition132, float %107)
  br label %if.end137

if.else133:                                       ; preds = %if.then125
  %m_currentPosition134 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_currentPosition135 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition136 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %108 = load float, float* %fraction, align 4, !tbaa !6
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition134, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition135, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition136, float %108)
  br label %if.end137

if.end137:                                        ; preds = %if.else133, %if.then129
  br label %if.end143

if.else138:                                       ; preds = %if.then117
  %m_currentPosition139 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_currentPosition140 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition141 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %109 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction142 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %109, i32 0, i32 1
  %110 = load float, float* %m_closestHitFraction142, align 4, !tbaa !80
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition139, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition140, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition141, float %110)
  br label %if.end143

if.end143:                                        ; preds = %if.else138, %if.end137
  %full_drop144 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 31
  store i8 0, i8* %full_drop144, align 1, !tbaa !35
  %m_verticalVelocity145 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_verticalVelocity145, align 4, !tbaa !26
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalOffset, align 4, !tbaa !27
  %m_wasJumping146 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  store i8 0, i8* %m_wasJumping146, align 1, !tbaa !32
  %111 = bitcast float* %fraction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  br label %if.end188

if.else147:                                       ; preds = %lor.lhs.false113
  %full_drop148 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 31
  store i8 1, i8* %full_drop148, align 1, !tbaa !35
  %bounce_fix149 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  %112 = load i8, i8* %bounce_fix149, align 2, !tbaa !36, !range !49
  %tobool150 = trunc i8 %112 to i1
  %conv151 = zext i1 %tobool150 to i32
  %cmp152 = icmp eq i32 %conv151, 1
  br i1 %cmp152, label %if.then153, label %if.end185

if.then153:                                       ; preds = %if.else147
  %m_verticalVelocity154 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %113 = load float, float* %m_verticalVelocity154, align 4, !tbaa !26
  %cmp155 = fcmp olt float %113, 0.000000e+00
  br i1 %cmp155, label %cond.true156, label %cond.false159

cond.true156:                                     ; preds = %if.then153
  %m_verticalVelocity157 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %114 = load float, float* %m_verticalVelocity157, align 4, !tbaa !26
  %fneg158 = fneg float %114
  br label %cond.end160

cond.false159:                                    ; preds = %if.then153
  br label %cond.end160

cond.end160:                                      ; preds = %cond.false159, %cond.true156
  %cond161 = phi float [ %fneg158, %cond.true156 ], [ 0.000000e+00, %cond.false159 ]
  %115 = load float, float* %dt.addr, align 4, !tbaa !6
  %mul162 = fmul float %cond161, %115
  store float %mul162, float* %downVelocity, align 4, !tbaa !6
  %116 = load float, float* %downVelocity, align 4, !tbaa !6
  %m_fallSpeed163 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %117 = load float, float* %m_fallSpeed163, align 4, !tbaa !29
  %cmp164 = fcmp ogt float %116, %117
  br i1 %cmp164, label %land.lhs.true165, label %if.end184

land.lhs.true165:                                 ; preds = %cond.end160
  %m_wasOnGround166 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %118 = load i8, i8* %m_wasOnGround166, align 4, !tbaa !31, !range !49
  %tobool167 = trunc i8 %118 to i1
  br i1 %tobool167, label %if.then171, label %lor.lhs.false168

lor.lhs.false168:                                 ; preds = %land.lhs.true165
  %m_wasJumping169 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  %119 = load i8, i8* %m_wasJumping169, align 1, !tbaa !32, !range !49
  %tobool170 = trunc i8 %119 to i1
  br i1 %tobool170, label %if.end184, label %if.then171

if.then171:                                       ; preds = %lor.lhs.false168, %land.lhs.true165
  %m_targetPosition172 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call173 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_targetPosition172, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  %m_fallSpeed174 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %120 = load float, float* %m_fallSpeed174, align 4, !tbaa !29
  store float %120, float* %downVelocity, align 4, !tbaa !6
  %121 = bitcast %class.btVector3* %ref.tmp175 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %121) #6
  %call176 = call %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv()
  %m_upAxis177 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %122 = load i32, i32* %m_upAxis177, align 4, !tbaa !12
  %arrayidx178 = getelementptr inbounds %class.btVector3, %class.btVector3* %call176, i32 %122
  %123 = bitcast float* %ref.tmp179 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #6
  %m_currentStepOffset180 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  %124 = load float, float* %m_currentStepOffset180, align 4, !tbaa !34
  %125 = load float, float* %downVelocity, align 4, !tbaa !6
  %add181 = fadd float %124, %125
  store float %add181, float* %ref.tmp179, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp175, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx178, float* nonnull align 4 dereferenceable(4) %ref.tmp179)
  %126 = bitcast %class.btVector3* %step_drop to i8*
  %127 = bitcast %class.btVector3* %ref.tmp175 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %126, i8* align 4 %127, i32 16, i1 false), !tbaa.struct !40
  %128 = bitcast float* %ref.tmp179 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #6
  %129 = bitcast %class.btVector3* %ref.tmp175 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %129) #6
  %m_targetPosition182 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call183 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_targetPosition182, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  br label %if.end184

if.end184:                                        ; preds = %if.then171, %lor.lhs.false168, %cond.end160
  br label %if.end185

if.end185:                                        ; preds = %if.end184, %if.else147
  %m_targetPosition186 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %m_currentPosition187 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %130 = bitcast %class.btVector3* %m_currentPosition187 to i8*
  %131 = bitcast %class.btVector3* %m_targetPosition186 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %130, i8* align 4 %131, i32 16, i1 false), !tbaa.struct !40
  br label %if.end188

if.end188:                                        ; preds = %if.end185, %if.end143
  %call189 = call %class.btKinematicClosestNotMeConvexResultCallback* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to %class.btKinematicClosestNotMeConvexResultCallback* (%class.btKinematicClosestNotMeConvexResultCallback*)*)(%class.btKinematicClosestNotMeConvexResultCallback* %callback2) #6
  %132 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %132) #6
  %call190 = call %class.btKinematicClosestNotMeConvexResultCallback* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to %class.btKinematicClosestNotMeConvexResultCallback* (%class.btKinematicClosestNotMeConvexResultCallback*)*)(%class.btKinematicClosestNotMeConvexResultCallback* %callback) #6
  %133 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %133) #6
  %134 = bitcast %class.btVector3* %step_drop to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %134) #6
  %135 = bitcast float* %downVelocity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #6
  %136 = bitcast %class.btVector3* %orig_position to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %136) #6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %runonce) #6
  %137 = bitcast %class.btTransform* %end_double to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %137) #6
  %138 = bitcast %class.btTransform* %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %138) #6
  %139 = bitcast %class.btTransform* %start to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %139) #6
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !6
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

define hidden void @_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %walkDirection) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %walkDirection.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %walkDirection, %class.btVector3** %walkDirection.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  store i8 1, i8* %m_useWalkDirection, align 1, !tbaa !24
  %0 = load %class.btVector3*, %class.btVector3** %walkDirection.addr, align 4, !tbaa !2
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %1 = bitcast %class.btVector3* %m_walkDirection to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !40
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %m_walkDirection2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  call void @_ZL19getNormalizedVectorRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection2)
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 16
  %4 = bitcast %class.btVector3* %m_normalizedDirection to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !40
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #6
  ret void
}

define internal void @_ZL19getNormalizedVectorRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #0 {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0)
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %agg.result)
  %cmp = fcmp olt float %call, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden void @_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity, float %timeInterval) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  %timeInterval.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4, !tbaa !2
  store float %timeInterval, float* %timeInterval.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  store i8 0, i8* %m_useWalkDirection, align 1, !tbaa !24
  %0 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4, !tbaa !2
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %1 = bitcast %class.btVector3* %m_walkDirection to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !40
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %m_walkDirection2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  call void @_ZL19getNormalizedVectorRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection2)
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 16
  %4 = bitcast %class.btVector3* %m_normalizedDirection to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !40
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #6
  %7 = load float, float* %timeInterval.addr, align 4, !tbaa !6
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 28
  %8 = load float, float* %m_velocityTimeInterval, align 4, !tbaa !25
  %add = fadd float %8, %7
  store float %add, float* %m_velocityTimeInterval, align 4, !tbaa !25
  ret void
}

define hidden void @_ZN30btKinematicCharacterController5resetEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %cache = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_verticalVelocity, align 4, !tbaa !26
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalOffset, align 4, !tbaa !27
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  store i8 0, i8* %m_wasOnGround, align 4, !tbaa !31
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  store i8 0, i8* %m_wasJumping, align 1, !tbaa !32
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_walkDirection, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 28
  store float 0.000000e+00, float* %m_velocityTimeInterval, align 4, !tbaa !25
  %6 = bitcast %class.btHashedOverlappingPairCache** %cache to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %7 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %call = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %7)
  store %class.btHashedOverlappingPairCache* %call, %class.btHashedOverlappingPairCache** %cache, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4, !tbaa !2
  %9 = bitcast %class.btHashedOverlappingPairCache* %8 to %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)***
  %vtable = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*** %9, align 4, !tbaa !10
  %vfn = getelementptr inbounds %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vtable, i64 7
  %10 = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vfn, align 4
  %call4 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.0* %10(%class.btHashedOverlappingPairCache* %8)
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.0* %call4)
  %cmp = icmp sgt i32 %call5, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4, !tbaa !2
  %12 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4, !tbaa !2
  %13 = bitcast %class.btHashedOverlappingPairCache* %12 to %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)***
  %vtable6 = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*** %13, align 4, !tbaa !10
  %vfn7 = getelementptr inbounds %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vtable6, i64 7
  %14 = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vfn7, align 4
  %call8 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.0* %14(%class.btHashedOverlappingPairCache* %12)
  %call9 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.0* %call8, i32 0)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call9, i32 0, i32 0
  %15 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !42
  %16 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4, !tbaa !2
  %17 = bitcast %class.btHashedOverlappingPairCache* %16 to %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)***
  %vtable10 = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*** %17, align 4, !tbaa !10
  %vfn11 = getelementptr inbounds %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vtable10, i64 7
  %18 = load %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.0* (%class.btHashedOverlappingPairCache*)** %vfn11, align 4
  %call12 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.0* %18(%class.btHashedOverlappingPairCache* %16)
  %call13 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.0* %call12, i32 0)
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call13, i32 0, i32 1
  %19 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !47
  %20 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call14 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %20)
  %21 = bitcast %class.btHashedOverlappingPairCache* %11 to i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable15 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %21, align 4, !tbaa !10
  %vfn16 = getelementptr inbounds i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable15, i64 3
  %22 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn16, align 4
  %call17 = call i8* %22(%class.btHashedOverlappingPairCache* %11, %struct.btBroadphaseProxy* %15, %struct.btBroadphaseProxy* %19, %class.btDispatcher* %call14)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = bitcast %class.btHashedOverlappingPairCache** %cache to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !86
  ret i32 %0
}

define hidden void @_ZN30btKinematicCharacterController4warpERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  %xform = alloca %class.btTransform, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btTransform* %xform to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #6
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xform)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %xform)
  %1 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %xform, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %2 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %3 = bitcast %class.btPairCachingGhostObject* %2 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %xform)
  %4 = bitcast %class.btTransform* %xform to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %4) #6
  ret void
}

define hidden void @_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %numPenetrationLoops = alloca i32, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast i32* %numPenetrationLoops to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %numPenetrationLoops, align 4, !tbaa !8
  %m_touchingContact = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  store i8 0, i8* %m_touchingContact, align 4, !tbaa !85
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %1)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %numPenetrationLoops, align 4, !tbaa !8
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %numPenetrationLoops, align 4, !tbaa !8
  %m_touchingContact2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  store i8 1, i8* %m_touchingContact2, align 4, !tbaa !85
  %3 = load i32, i32* %numPenetrationLoops, align 4, !tbaa !8
  %cmp = icmp sgt i32 %3, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  br label %while.end

if.end:                                           ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %if.then, %while.cond
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %4 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %5 = bitcast %class.btPairCachingGhostObject* %4 to %class.btCollisionObject*
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %5)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call3)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %6 = bitcast %class.btVector3* %m_currentPosition to i8*
  %7 = bitcast %class.btVector3* %call4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !40
  %m_currentPosition5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %8 = bitcast %class.btVector3* %m_targetPosition to i8*
  %9 = bitcast %class.btVector3* %m_currentPosition5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !40
  %10 = bitcast i32* %numPenetrationLoops to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  ret void
}

define hidden void @_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, float %dt) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %dt.addr = alloca float, align 4
  %xform = alloca %class.btTransform, align 4
  %dtMoving = alloca float, align 4
  %move = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  store float %dt, float* %dt.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  %0 = load i8, i8* %m_useWalkDirection, align 1, !tbaa !24, !range !49
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 28
  %1 = load float, float* %m_velocityTimeInterval, align 4, !tbaa !25
  %conv = fpext float %1 to double
  %cmp = fcmp ole double %conv, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*)***
  %vtable = load i1 (%class.btKinematicCharacterController*)**, i1 (%class.btKinematicCharacterController*)*** %2, align 4, !tbaa !10
  %vfn = getelementptr inbounds i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vtable, i64 12
  %3 = load i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vfn, align 4
  %call = call zeroext i1 %3(%class.btKinematicCharacterController* %this1)
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %frombool = zext i1 %call to i8
  store i8 %frombool, i8* %m_wasOnGround, align 4, !tbaa !31
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 11
  %4 = load float, float* %m_gravity, align 4, !tbaa !28
  %5 = load float, float* %dt.addr, align 4, !tbaa !6
  %mul = fmul float %4, %5
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %6 = load float, float* %m_verticalVelocity, align 4, !tbaa !26
  %sub = fsub float %6, %mul
  store float %sub, float* %m_verticalVelocity, align 4, !tbaa !26
  %m_verticalVelocity2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %7 = load float, float* %m_verticalVelocity2, align 4, !tbaa !26
  %conv3 = fpext float %7 to double
  %cmp4 = fcmp ogt double %conv3, 0.000000e+00
  br i1 %cmp4, label %land.lhs.true5, label %if.end11

land.lhs.true5:                                   ; preds = %if.end
  %m_verticalVelocity6 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %8 = load float, float* %m_verticalVelocity6, align 4, !tbaa !26
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %9 = load float, float* %m_jumpSpeed, align 4, !tbaa !30
  %cmp7 = fcmp ogt float %8, %9
  br i1 %cmp7, label %if.then8, label %if.end11

if.then8:                                         ; preds = %land.lhs.true5
  %m_jumpSpeed9 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %10 = load float, float* %m_jumpSpeed9, align 4, !tbaa !30
  %m_verticalVelocity10 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float %10, float* %m_verticalVelocity10, align 4, !tbaa !26
  br label %if.end11

if.end11:                                         ; preds = %if.then8, %land.lhs.true5, %if.end
  %m_verticalVelocity12 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %11 = load float, float* %m_verticalVelocity12, align 4, !tbaa !26
  %conv13 = fpext float %11 to double
  %cmp14 = fcmp olt double %conv13, 0.000000e+00
  br i1 %cmp14, label %land.lhs.true15, label %if.end24

land.lhs.true15:                                  ; preds = %if.end11
  %m_verticalVelocity16 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %12 = load float, float* %m_verticalVelocity16, align 4, !tbaa !26
  %call17 = call float @_Z6btFabsf(float %12)
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %13 = load float, float* %m_fallSpeed, align 4, !tbaa !29
  %call18 = call float @_Z6btFabsf(float %13)
  %cmp19 = fcmp ogt float %call17, %call18
  br i1 %cmp19, label %if.then20, label %if.end24

if.then20:                                        ; preds = %land.lhs.true15
  %m_fallSpeed21 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %14 = load float, float* %m_fallSpeed21, align 4, !tbaa !29
  %call22 = call float @_Z6btFabsf(float %14)
  %fneg = fneg float %call22
  %m_verticalVelocity23 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float %fneg, float* %m_verticalVelocity23, align 4, !tbaa !26
  br label %if.end24

if.end24:                                         ; preds = %if.then20, %land.lhs.true15, %if.end11
  %m_verticalVelocity25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %15 = load float, float* %m_verticalVelocity25, align 4, !tbaa !26
  %16 = load float, float* %dt.addr, align 4, !tbaa !6
  %mul26 = fmul float %15, %16
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float %mul26, float* %m_verticalOffset, align 4, !tbaa !27
  %17 = bitcast %class.btTransform* %xform to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %17) #6
  %call27 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xform)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %18 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4, !tbaa !20
  %19 = bitcast %class.btPairCachingGhostObject* %18 to %class.btCollisionObject*
  %call28 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %19)
  %call29 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xform, %class.btTransform* nonnull align 4 dereferenceable(64) %call28)
  %20 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  call void @_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %20)
  %m_useWalkDirection30 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  %21 = load i8, i8* %m_useWalkDirection30, align 1, !tbaa !24, !range !49
  %tobool31 = trunc i8 %21 to i1
  br i1 %tobool31, label %if.then32, label %if.else

if.then32:                                        ; preds = %if.end24
  %22 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  call void @_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection)
  br label %if.end39

if.else:                                          ; preds = %if.end24
  %23 = bitcast float* %dtMoving to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load float, float* %dt.addr, align 4, !tbaa !6
  %m_velocityTimeInterval33 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 28
  %25 = load float, float* %m_velocityTimeInterval33, align 4, !tbaa !25
  %cmp34 = fcmp olt float %24, %25
  br i1 %cmp34, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %26 = load float, float* %dt.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_velocityTimeInterval35 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 28
  %27 = load float, float* %m_velocityTimeInterval35, align 4, !tbaa !25
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %26, %cond.true ], [ %27, %cond.false ]
  store float %cond, float* %dtMoving, align 4, !tbaa !6
  %28 = load float, float* %dt.addr, align 4, !tbaa !6
  %m_velocityTimeInterval36 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 28
  %29 = load float, float* %m_velocityTimeInterval36, align 4, !tbaa !25
  %sub37 = fsub float %29, %28
  store float %sub37, float* %m_velocityTimeInterval36, align 4, !tbaa !25
  %30 = bitcast %class.btVector3* %move to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #6
  %m_walkDirection38 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %move, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection38, float* nonnull align 4 dereferenceable(4) %dtMoving)
  %31 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  call void @_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %move)
  %32 = bitcast %class.btVector3* %move to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #6
  %33 = bitcast float* %dtMoving to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  br label %if.end39

if.end39:                                         ; preds = %cond.end, %if.then32
  %34 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %35 = load float, float* %dt.addr, align 4, !tbaa !6
  call void @_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %34, float %35)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %xform, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition)
  %m_ghostObject40 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %36 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject40, align 4, !tbaa !20
  %37 = bitcast %class.btPairCachingGhostObject* %36 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %37, %class.btTransform* nonnull align 4 dereferenceable(64) %xform)
  %38 = bitcast %class.btTransform* %xform to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %38) #6
  br label %return

return:                                           ; preds = %if.end39, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !40
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN30btKinematicCharacterController12setFallSpeedEf(%class.btKinematicCharacterController* %this, float %fallSpeed) #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %fallSpeed.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store float %fallSpeed, float* %fallSpeed.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %fallSpeed.addr, align 4, !tbaa !6
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  store float %0, float* %m_fallSpeed, align 4, !tbaa !29
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN30btKinematicCharacterController12setJumpSpeedEf(%class.btKinematicCharacterController* %this, float %jumpSpeed) #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %jumpSpeed.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store float %jumpSpeed, float* %jumpSpeed.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %jumpSpeed.addr, align 4, !tbaa !6
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  store float %0, float* %m_jumpSpeed, align 4, !tbaa !30
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN30btKinematicCharacterController16setMaxJumpHeightEf(%class.btKinematicCharacterController* %this, float %maxJumpHeight) #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %maxJumpHeight.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store float %maxJumpHeight, float* %maxJumpHeight.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %maxJumpHeight.addr, align 4, !tbaa !6
  %m_maxJumpHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  store float %0, float* %m_maxJumpHeight, align 4, !tbaa !87
  ret void
}

define hidden zeroext i1 @_ZNK30btKinematicCharacterController7canJumpEv(%class.btKinematicCharacterController* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*)***
  %vtable = load i1 (%class.btKinematicCharacterController*)**, i1 (%class.btKinematicCharacterController*)*** %0, align 4, !tbaa !10
  %vfn = getelementptr inbounds i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vtable, i64 12
  %1 = load i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vfn, align 4
  %call = call zeroext i1 %1(%class.btKinematicCharacterController* %this1)
  ret i1 %call
}

define hidden void @_ZN30btKinematicCharacterController4jumpEv(%class.btKinematicCharacterController* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*)***
  %vtable = load i1 (%class.btKinematicCharacterController*)**, i1 (%class.btKinematicCharacterController*)*** %0, align 4, !tbaa !10
  %vfn = getelementptr inbounds i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vtable, i64 10
  %1 = load i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vfn, align 4
  %call = call zeroext i1 %1(%class.btKinematicCharacterController* %this1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %2 = load float, float* %m_jumpSpeed, align 4, !tbaa !30
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float %2, float* %m_verticalVelocity, align 4, !tbaa !26
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  store i8 1, i8* %m_wasJumping, align 1, !tbaa !32
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN30btKinematicCharacterController10setGravityEf(%class.btKinematicCharacterController* %this, float %gravity) #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %gravity.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store float %gravity, float* %gravity.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %gravity.addr, align 4, !tbaa !6
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 11
  store float %0, float* %m_gravity, align 4, !tbaa !28
  ret void
}

; Function Attrs: nounwind
define hidden float @_ZNK30btKinematicCharacterController10getGravityEv(%class.btKinematicCharacterController* %this) #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 11
  %0 = load float, float* %m_gravity, align 4, !tbaa !28
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind
define hidden float @_ZNK30btKinematicCharacterController11getMaxSlopeEv(%class.btKinematicCharacterController* %this) #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_maxSlopeRadians = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 9
  %0 = load float, float* %m_maxSlopeRadians, align 4, !tbaa !37
  ret float %0
}

; Function Attrs: nounwind
define hidden zeroext i1 @_ZNK30btKinematicCharacterController8onGroundEv(%class.btKinematicCharacterController* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %0 = load float, float* %m_verticalVelocity, align 4, !tbaa !26
  %conv = fpext float %0 to double
  %cmp = fcmp oeq double %conv, 0.000000e+00
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %1 = load float, float* %m_verticalOffset, align 4, !tbaa !27
  %conv2 = fpext float %1 to double
  %cmp3 = fcmp oeq double %conv2, 0.000000e+00
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %2
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #6

; Function Attrs: nounwind
define hidden void @_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw(%class.btKinematicCharacterController* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN30btKinematicCharacterController16setUpInterpolateEb(%class.btKinematicCharacterController* %this, i1 zeroext %value) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %value.addr = alloca i8, align 1
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  %frombool = zext i1 %value to i8
  store i8 %frombool, i8* %value.addr, align 1, !tbaa !39
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load i8, i8* %value.addr, align 1, !tbaa !39, !range !49
  %tobool = trunc i8 %0 to i1
  %m_interpolateUp = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 30
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_interpolateUp, align 4, !tbaa !33
  ret void
}

define linkonce_odr hidden void @_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, float %deltaTime) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %deltaTime.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4, !tbaa !2
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  store float %deltaTime, float* %deltaTime.addr, align 4, !tbaa !6
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %1 = bitcast %class.btKinematicCharacterController* %this1 to void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)***
  %vtable = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)**, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)*** %1, align 4, !tbaa !10
  %vfn = getelementptr inbounds void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)** %vtable, i64 8
  %2 = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)** %vfn, align 4
  call void %2(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %0)
  %3 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !2
  %4 = load float, float* %deltaTime.addr, align 4, !tbaa !6
  %5 = bitcast %class.btKinematicCharacterController* %this1 to void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)***
  %vtable2 = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)**, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)*** %5, align 4, !tbaa !10
  %vfn3 = getelementptr inbounds void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)** %vtable2, i64 9
  %6 = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)** %vfn3, align 4
  call void %6(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %3, float %4)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  %0 = bitcast %class.btActionInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV17btActionInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !10
  ret %class.btActionInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceD2Ev(%class.btActionInterface* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  ret %class.btActionInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN30btCharacterControllerInterfaceD0Ev(%class.btCharacterControllerInterface* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCharacterControllerInterface*, align 4
  store %class.btCharacterControllerInterface* %this, %class.btCharacterControllerInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCharacterControllerInterface*, %class.btCharacterControllerInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btActionInterfaceD0Ev(%class.btActionInterface* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !40
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !40
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !40
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !6
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

define linkonce_odr hidden %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestConvexResultCallback"* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %convexFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %convexToWorld) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  %convexFromWorld.addr = alloca %class.btVector3*, align 4
  %convexToWorld.addr = alloca %class.btVector3*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %convexFromWorld, %class.btVector3** %convexFromWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %convexToWorld, %class.btVector3** %convexToWorld.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call = call %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackC2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %0)
  %1 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !10
  %m_convexFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %convexFromWorld.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %m_convexFromWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !40
  %m_convexToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %convexToWorld.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %m_convexToWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !40
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormalWorld)
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPointWorld)
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  store %class.btCollisionObject* null, %class.btCollisionObject** %m_hitCollisionObject, align 4, !tbaa !88
  ret %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev(%class.btKinematicClosestNotMeConvexResultCallback* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btKinematicClosestNotMeConvexResultCallback*, align 4
  store %class.btKinematicClosestNotMeConvexResultCallback* %this, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btKinematicClosestNotMeConvexResultCallback*, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %call = call %class.btKinematicClosestNotMeConvexResultCallback* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to %class.btKinematicClosestNotMeConvexResultCallback* (%class.btKinematicClosestNotMeConvexResultCallback*)*)(%class.btKinematicClosestNotMeConvexResultCallback* %this1) #6
  %0 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy(%"struct.btCollisionWorld::ConvexResultCallback"* %this, %struct.btBroadphaseProxy* %proxy0) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %collides) #6
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 1
  %1 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !74
  %conv = sext i16 %1 to i32
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 3
  %2 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !78
  %conv2 = sext i16 %2 to i32
  %and = and i32 %conv, %conv2
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1, !tbaa !39
  %3 = load i8, i8* %collides, align 1, !tbaa !39, !range !49
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_collisionFilterGroup3 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 2
  %4 = load i16, i16* %m_collisionFilterGroup3, align 4, !tbaa !75
  %conv4 = sext i16 %4 to i32
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %m_collisionFilterMask5 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 2
  %6 = load i16, i16* %m_collisionFilterMask5, align 2, !tbaa !77
  %conv6 = sext i16 %6 to i32
  %and7 = and i32 %conv4, %conv6
  %tobool8 = icmp ne i32 %and7, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %7 = phi i1 [ false, %entry ], [ %tobool8, %land.rhs ]
  %frombool9 = zext i1 %7 to i8
  store i8 %frombool9, i8* %collides, align 1, !tbaa !39
  %8 = load i8, i8* %collides, align 1, !tbaa !39, !range !49
  %tobool10 = trunc i8 %8 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %collides) #6
  ret i1 %tobool10
}

define linkonce_odr hidden float @_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb(%class.btKinematicClosestNotMeConvexResultCallback* %this, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %convexResult, i1 zeroext %normalInWorldSpace) unnamed_addr #0 comdat {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btKinematicClosestNotMeConvexResultCallback*, align 4
  %convexResult.addr = alloca %"struct.btCollisionWorld::LocalConvexResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %hitNormalWorld = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %dotUp = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btKinematicClosestNotMeConvexResultCallback* %this, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::LocalConvexResult"* %convexResult, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1, !tbaa !39
  %this1 = load %class.btKinematicClosestNotMeConvexResultCallback*, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %0, i32 0, i32 0
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4, !tbaa !90
  %m_me = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %m_me, align 4, !tbaa !82
  %cmp = icmp eq %class.btCollisionObject* %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitCollisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject2, align 4, !tbaa !90
  %call = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %4)
  br i1 %call, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %5 = bitcast %class.btVector3* %hitNormalWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %hitNormalWorld)
  %6 = load i8, i8* %normalInWorldSpace.addr, align 1, !tbaa !39, !range !49
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end4
  %7 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %7, i32 0, i32 2
  %8 = bitcast %class.btVector3* %hitNormalWorld to i8*
  %9 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !40
  br label %if.end11

if.else:                                          ; preds = %if.end4
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #6
  %11 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitCollisionObject7 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %11, i32 0, i32 0
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject7, align 4, !tbaa !90
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %12)
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call8)
  %13 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitNormalLocal10 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %13, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalLocal10)
  %14 = bitcast %class.btVector3* %hitNormalWorld to i8*
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !40
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #6
  br label %if.end11

if.end11:                                         ; preds = %if.else, %if.then6
  %17 = bitcast float* %dotUp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  %m_up = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 2
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_up, %class.btVector3* nonnull align 4 dereferenceable(16) %hitNormalWorld)
  store float %call12, float* %dotUp, align 4, !tbaa !6
  %18 = load float, float* %dotUp, align 4, !tbaa !6
  %m_minSlopeDot = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 3
  %19 = load float, float* %m_minSlopeDot, align 4, !tbaa !84
  %cmp13 = fcmp olt float %18, %19
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end11
  store float 1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %if.end11
  %20 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %21 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %22 = load i8, i8* %normalInWorldSpace.addr, align 1, !tbaa !39, !range !49
  %tobool16 = trunc i8 %22 to i1
  %call17 = call float @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %20, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %21, i1 zeroext %tobool16)
  store float %call17, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end15, %if.then14
  %23 = bitcast float* %dotUp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  %24 = bitcast %class.btVector3* %hitNormalWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #6
  br label %return

return:                                           ; preds = %cleanup, %if.then3, %if.then
  %25 = load float, float* %retval, align 4
  ret float %25
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackC2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ConvexResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld20ConvexResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !10
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %m_closestHitFraction, align 4, !tbaa !80
  %m_collisionFilterGroup = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 2
  store i16 1, i16* %m_collisionFilterGroup, align 4, !tbaa !75
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 3
  store i16 -1, i16* %m_collisionFilterMask, align 2, !tbaa !78
  ret %"struct.btCollisionWorld::ConvexResultCallback"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  ret %"struct.btCollisionWorld::ConvexResultCallback"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %call = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to %"struct.btCollisionWorld::ClosestConvexResultCallback"* (%"struct.btCollisionWorld::ClosestConvexResultCallback"*)*)(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1) #6
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

define linkonce_odr hidden float @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %convexResult, i1 zeroext %normalInWorldSpace) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  %convexResult.addr = alloca %"struct.btCollisionWorld::LocalConvexResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::LocalConvexResult"* %convexResult, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1, !tbaa !39
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitFraction = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %0, i32 0, i32 4
  %1 = load float, float* %m_hitFraction, align 4, !tbaa !92
  %2 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %2, i32 0, i32 1
  store float %1, float* %m_closestHitFraction, align 4, !tbaa !80
  %3 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4, !tbaa !90
  %m_hitCollisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  store %class.btCollisionObject* %4, %class.btCollisionObject** %m_hitCollisionObject2, align 4, !tbaa !88
  %5 = load i8, i8* %normalInWorldSpace.addr, align 1, !tbaa !39, !range !49
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %6, i32 0, i32 2
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %7 = bitcast %class.btVector3* %m_hitNormalWorld to i8*
  %8 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !40
  br label %if.end

if.else:                                          ; preds = %entry
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #6
  %m_hitCollisionObject3 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject3, align 4, !tbaa !88
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %10)
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  %11 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitNormalLocal5 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %11, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalLocal5)
  %m_hitNormalWorld6 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %12 = bitcast %class.btVector3* %m_hitNormalWorld6 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !40
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %15 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitPointLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %15, i32 0, i32 3
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 4
  %16 = bitcast %class.btVector3* %m_hitPointWorld to i8*
  %17 = bitcast %class.btVector3* %m_hitPointLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !40
  %18 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4, !tbaa !2
  %m_hitFraction7 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %18, i32 0, i32 4
  %19 = load float, float* %m_hitFraction7, align 4, !tbaa !92
  ret float %19
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btCollisionWorld20ConvexResultCallbackD0Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !6
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !8
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !6
  %0 = load float, float* %y.addr, align 4, !tbaa !6
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !6
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !6
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !6
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !6
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !40
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !40
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #6
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !40
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !40
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !40
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #9

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !93
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4, !tbaa !62
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !63
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !94
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !8
  store i32 %last, i32* %last.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %last.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !62
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !62
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !93, !range !49
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !62
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !62
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !93
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !62
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !8
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !94
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !94
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !8
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !8
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !8
  store i32 %end, i32* %end.addr, align 4, !tbaa !8
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %start.addr, align 4, !tbaa !8
  store i32 %1, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !8
  %3 = load i32, i32* %end.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !62
  %9 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { noreturn nounwind }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"vtable pointer", !5, i64 0}
!12 = !{!13, !9, i64 176}
!13 = !{!"_ZTS30btKinematicCharacterController", !7, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !14, i64 60, !14, i64 76, !14, i64 92, !7, i64 108, !14, i64 112, !15, i64 128, !17, i64 148, !14, i64 152, !17, i64 168, !17, i64 169, !17, i64 170, !17, i64 171, !7, i64 172, !9, i64 176, !17, i64 180, !17, i64 181, !17, i64 182}
!14 = !{!"_ZTS9btVector3", !4, i64 0}
!15 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !16, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!17 = !{!"bool", !4, i64 0}
!18 = !{!13, !7, i64 56}
!19 = !{!13, !17, i64 170}
!20 = !{!13, !3, i64 8}
!21 = !{!13, !7, i64 52}
!22 = !{!13, !7, i64 48}
!23 = !{!13, !3, i64 12}
!24 = !{!13, !17, i64 171}
!25 = !{!13, !7, i64 172}
!26 = !{!13, !7, i64 16}
!27 = !{!13, !7, i64 20}
!28 = !{!13, !7, i64 44}
!29 = !{!13, !7, i64 24}
!30 = !{!13, !7, i64 28}
!31 = !{!13, !17, i64 168}
!32 = !{!13, !17, i64 169}
!33 = !{!13, !17, i64 180}
!34 = !{!13, !7, i64 108}
!35 = !{!13, !17, i64 181}
!36 = !{!13, !17, i64 182}
!37 = !{!13, !7, i64 36}
!38 = !{!13, !7, i64 40}
!39 = !{!17, !17, i64 0}
!40 = !{i64 0, i64 16, !41}
!41 = !{!4, !4, i64 0}
!42 = !{!43, !3, i64 0}
!43 = !{!"_ZTS16btBroadphasePair", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12}
!44 = !{!45, !3, i64 0}
!45 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !46, i64 4, !46, i64 6, !3, i64 8, !9, i64 12, !14, i64 16, !14, i64 32}
!46 = !{!"short", !4, i64 0}
!47 = !{!43, !3, i64 4}
!48 = !{!43, !3, i64 8}
!49 = !{i8 0, i8 2}
!50 = !{!51, !3, i64 68}
!51 = !{!"_ZTS16btCollisionWorld", !52, i64 4, !3, i64 24, !54, i64 28, !3, i64 68, !3, i64 72, !17, i64 76}
!52 = !{!"_ZTS20btAlignedObjectArrayIP17btCollisionObjectE", !53, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!53 = !{!"_ZTS18btAlignedAllocatorIP17btCollisionObjectLj16EE"}
!54 = !{!"_ZTS16btDispatcherInfo", !7, i64 0, !9, i64 4, !9, i64 8, !7, i64 12, !17, i64 16, !3, i64 20, !17, i64 24, !17, i64 25, !17, i64 26, !7, i64 28, !17, i64 32, !7, i64 36}
!55 = !{!56, !3, i64 188}
!56 = !{!"_ZTS17btCollisionObject", !57, i64 4, !57, i64 68, !14, i64 132, !14, i64 148, !14, i64 164, !9, i64 180, !7, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !9, i64 204, !9, i64 208, !9, i64 212, !9, i64 216, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !9, i64 236, !4, i64 240, !7, i64 244, !7, i64 248, !7, i64 252, !9, i64 256, !9, i64 260}
!57 = !{!"_ZTS11btTransform", !58, i64 0, !14, i64 48}
!58 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!59 = !{!51, !3, i64 24}
!60 = !{!61, !3, i64 284}
!61 = !{!"_ZTS24btPairCachingGhostObject", !3, i64 284}
!62 = !{!15, !3, i64 12}
!63 = !{!15, !9, i64 4}
!64 = !{!65, !3, i64 12}
!65 = !{!"_ZTS20btAlignedObjectArrayI16btBroadphasePairE", !66, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !17, i64 16}
!66 = !{!"_ZTS18btAlignedAllocatorI16btBroadphasePairLj16EE"}
!67 = !{!56, !9, i64 204}
!68 = !{!69, !3, i64 740}
!69 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !9, i64 748, !7, i64 752, !7, i64 756, !9, i64 760, !9, i64 764, !9, i64 768}
!70 = !{!69, !9, i64 748}
!71 = !{!72, !7, i64 80}
!72 = !{!"_ZTS15btManifoldPoint", !14, i64 0, !14, i64 16, !14, i64 32, !14, i64 48, !14, i64 64, !7, i64 80, !7, i64 84, !7, i64 88, !7, i64 92, !9, i64 96, !9, i64 100, !9, i64 104, !9, i64 108, !3, i64 112, !17, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !7, i64 136, !7, i64 140, !7, i64 144, !9, i64 148, !14, i64 152, !14, i64 168}
!73 = !{!56, !9, i64 260}
!74 = !{!45, !46, i64 4}
!75 = !{!76, !46, i64 8}
!76 = !{!"_ZTSN16btCollisionWorld20ConvexResultCallbackE", !7, i64 4, !46, i64 8, !46, i64 10}
!77 = !{!45, !46, i64 6}
!78 = !{!76, !46, i64 10}
!79 = !{!54, !7, i64 28}
!80 = !{!76, !7, i64 4}
!81 = !{!"branch_weights", i32 1, i32 1048575}
!82 = !{!83, !3, i64 80}
!83 = !{!"_ZTS43btKinematicClosestNotMeConvexResultCallback", !3, i64 80, !14, i64 84, !7, i64 100}
!84 = !{!83, !7, i64 100}
!85 = !{!13, !17, i64 148}
!86 = !{!65, !9, i64 4}
!87 = !{!13, !7, i64 32}
!88 = !{!89, !3, i64 76}
!89 = !{!"_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE", !14, i64 12, !14, i64 28, !14, i64 44, !14, i64 60, !3, i64 76}
!90 = !{!91, !3, i64 0}
!91 = !{!"_ZTSN16btCollisionWorld17LocalConvexResultE", !3, i64 0, !3, i64 4, !14, i64 8, !14, i64 24, !7, i64 40}
!92 = !{!91, !7, i64 40}
!93 = !{!15, !17, i64 16}
!94 = !{!15, !9, i64 8}
