; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btPoint2PointConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], %class.btVector3, %class.btVector3, i32, float, float, i8, %struct.btConstraintSetting }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btVector3 = type { [4 x float] }
%struct.btConstraintSetting = type { float, float, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon.0, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon.0 = type { i8* }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32*, i32, float }
%class.btAlignedObjectArray.1 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btPoint2PointConstraintFloatData = type { %struct.btTypedConstraintData, %struct.btVector3FloatData, %struct.btVector3FloatData }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN19btConstraintSettingC2Ev = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody22getInvInertiaDiagLocalEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK23btPoint2PointConstraint11getPivotInAEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_ = comdat any

$_ZNK23btPoint2PointConstraint11getPivotInBEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN23btPoint2PointConstraintD0Ev = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK23btPoint2PointConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK23btPoint2PointConstraint9serializeEPvP12btSerializer = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN23btPoint2PointConstraintdlEPv = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

@_ZTV23btPoint2PointConstraint = hidden unnamed_addr constant { [13 x i8*] } { [13 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btPoint2PointConstraint to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to i8*), i8* bitcast (void (%class.btPoint2PointConstraint*)* @_ZN23btPoint2PointConstraintD0Ev to i8*), i8* bitcast (void (%class.btPoint2PointConstraint*)* @_ZN23btPoint2PointConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.1*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btPoint2PointConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btPoint2PointConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btPoint2PointConstraint*, i32, float, i32)* @_ZN23btPoint2PointConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btPoint2PointConstraint*, i32, i32)* @_ZNK23btPoint2PointConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btPoint2PointConstraint*)* @_ZNK23btPoint2PointConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btPoint2PointConstraint*, i8*, %class.btSerializer*)* @_ZNK23btPoint2PointConstraint9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btPoint2PointConstraint = hidden constant [26 x i8] c"23btPoint2PointConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI23btPoint2PointConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btPoint2PointConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [33 x i8] c"btPoint2PointConstraintFloatData\00", align 1

@_ZN23btPoint2PointConstraintC1ER11btRigidBodyS1_RK9btVector3S4_ = hidden unnamed_addr alias %class.btPoint2PointConstraint* (%class.btPoint2PointConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*), %class.btPoint2PointConstraint* (%class.btPoint2PointConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*)* @_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_
@_ZN23btPoint2PointConstraintC1ER11btRigidBodyRK9btVector3 = hidden unnamed_addr alias %class.btPoint2PointConstraint* (%class.btPoint2PointConstraint*, %class.btRigidBody*, %class.btVector3*), %class.btPoint2PointConstraint* (%class.btPoint2PointConstraint*, %class.btRigidBody*, %class.btVector3*)* @_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3

define hidden %class.btPoint2PointConstraint* @_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_(%class.btPoint2PointConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB) unnamed_addr #0 {
entry:
  %retval = alloca %class.btPoint2PointConstraint*, align 4
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  store %class.btPoint2PointConstraint* %this1, %class.btPoint2PointConstraint** %retval, align 4
  %0 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 3, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1, %class.btRigidBody* nonnull align 4 dereferenceable(616) %2)
  %3 = bitcast %class.btPoint2PointConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV23btPoint2PointConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4, !tbaa !6
  %m_jac = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_pivotInA = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 2
  %4 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %m_pivotInA to i8*
  %6 = bitcast %class.btVector3* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !8
  %m_pivotInB = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 3
  %7 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %m_pivotInB to i8*
  %9 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !8
  %m_flags = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 4
  store i32 0, i32* %m_flags, align 4, !tbaa !10
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 7
  store i8 0, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !17
  %m_setting = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 8
  %call3 = call %struct.btConstraintSetting* @_ZN19btConstraintSettingC2Ev(%struct.btConstraintSetting* %m_setting)
  %10 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %retval, align 4
  ret %class.btPoint2PointConstraint* %10
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616), %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btConstraintSetting* @_ZN19btConstraintSettingC2Ev(%struct.btConstraintSetting* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btConstraintSetting*, align 4
  store %struct.btConstraintSetting* %this, %struct.btConstraintSetting** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btConstraintSetting*, %struct.btConstraintSetting** %this.addr, align 4
  %m_tau = getelementptr inbounds %struct.btConstraintSetting, %struct.btConstraintSetting* %this1, i32 0, i32 0
  store float 0x3FD3333340000000, float* %m_tau, align 4, !tbaa !18
  %m_damping = getelementptr inbounds %struct.btConstraintSetting, %struct.btConstraintSetting* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %m_damping, align 4, !tbaa !19
  %m_impulseClamp = getelementptr inbounds %struct.btConstraintSetting, %struct.btConstraintSetting* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_impulseClamp, align 4, !tbaa !20
  ret %struct.btConstraintSetting* %this1
}

define hidden %class.btPoint2PointConstraint* @_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3(%class.btPoint2PointConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(616) %rbA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA) unnamed_addr #0 {
entry:
  %retval = alloca %class.btPoint2PointConstraint*, align 4
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  store %class.btPoint2PointConstraint* %this1, %class.btPoint2PointConstraint** %retval, align 4
  %0 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* %0, i32 3, %class.btRigidBody* nonnull align 4 dereferenceable(616) %1)
  %2 = bitcast %class.btPoint2PointConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV23btPoint2PointConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !6
  %m_jac = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_pivotInA = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 2
  %3 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %m_pivotInA to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !8
  %m_pivotInB = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 3
  %6 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %7 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %m_pivotInB, %class.btTransform* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %m_flags = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 4
  store i32 0, i32* %m_flags, align 4, !tbaa !10
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 7
  store i8 0, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !17
  %m_setting = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 8
  %call4 = call %struct.btConstraintSetting* @_ZN19btConstraintSettingC2Ev(%struct.btConstraintSetting* %m_setting)
  %8 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %retval, align 4
  ret %class.btPoint2PointConstraint* %8
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(616)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #6
  ret void
}

define hidden void @_ZN23btPoint2PointConstraint13buildJacobianEv(%class.btPoint2PointConstraint* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp6 = alloca %class.btMatrix3x3, align 4
  %ref.tmp9 = alloca %class.btMatrix3x3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %0 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !21
  %1 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #6
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !23
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !23
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %normal, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store i32 0, i32* %i, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !24
  %cmp = icmp slt i32 %9, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %11 = load i32, i32* %i, align 4, !tbaa !24
  %arrayidx = getelementptr inbounds float, float* %call4, i32 %11
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !23
  %m_jac = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 1
  %12 = load i32, i32* %i, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 %12
  %13 = bitcast %class.btJacobianEntry* %arrayidx5 to i8*
  %14 = bitcast i8* %13 to %class.btJacobianEntry*
  %15 = bitcast %class.btMatrix3x3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %15) #6
  %16 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %16, i32 0, i32 8
  %17 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !25
  %call7 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %17)
  %call8 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call7)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp6, %class.btMatrix3x3* %call8)
  %18 = bitcast %class.btMatrix3x3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %18) #6
  %19 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %19, i32 0, i32 9
  %20 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !26
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %20)
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call10)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp9, %class.btMatrix3x3* %call11)
  %21 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #6
  %22 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #6
  %23 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA14 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %23, i32 0, i32 8
  %24 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA14, align 4, !tbaa !25
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %24)
  %m_pivotInA = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp13, %class.btTransform* %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInA)
  %25 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA16 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %25, i32 0, i32 8
  %26 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA16, align 4, !tbaa !25
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %26)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %call17)
  %27 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #6
  %28 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #6
  %29 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB20 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %29, i32 0, i32 9
  %30 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB20, align 4, !tbaa !26
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %30)
  %m_pivotInB = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, %class.btTransform* %call21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInB)
  %31 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB22 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %31, i32 0, i32 9
  %32 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB22, align 4, !tbaa !26
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %32)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %call23)
  %33 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA24 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %33, i32 0, i32 8
  %34 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA24, align 4, !tbaa !25
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %34)
  %35 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA26 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %35, i32 0, i32 8
  %36 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA26, align 4, !tbaa !25
  %call27 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %36)
  %37 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB28 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %37, i32 0, i32 9
  %38 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB28, align 4, !tbaa !26
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %38)
  %39 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB30 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %39, i32 0, i32 9
  %40 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB30, align 4, !tbaa !26
  %call31 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %40)
  %call32 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %14, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp6, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %call25, float %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %call29, float %call31)
  %41 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #6
  %42 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #6
  %43 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #6
  %44 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #6
  %45 = bitcast %class.btMatrix3x3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %45) #6
  %46 = bitcast %class.btMatrix3x3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %46) #6
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %47 = load i32, i32* %i, align 4, !tbaa !24
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 %47
  store float 0.000000e+00, float* %arrayidx34, align 4, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %i, align 4, !tbaa !24
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %i, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %49 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !23
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !23
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !23
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  ret %class.btVector3* %m_invInertiaLocal
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !27
  ret float %0
}

define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, float %massInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB, float %massInvB) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %massInvA.addr = alloca float, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %massInvB.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  store float %massInvA, float* %massInvA.addr, align 4, !tbaa !23
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  store float %massInvB, float* %massInvB.addr, align 4, !tbaa !23
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %m_linearJointAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !8
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4, !tbaa !2
  %m_linearJointAxis6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_aJ7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %7 = bitcast %class.btVector3* %m_aJ7 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !8
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #6
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #6
  %11 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #6
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #6
  %14 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #6
  %m_linearJointAxis11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis11)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_bJ12 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !8
  %18 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #6
  %19 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #6
  %20 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  %21 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #6
  %22 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4, !tbaa !2
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %23 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !8
  %25 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #6
  %26 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #6
  %27 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4, !tbaa !2
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %28 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !8
  %30 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #6
  %31 = load float, float* %massInvA.addr, align 4, !tbaa !23
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %add = fadd float %31, %call21
  %32 = load float, float* %massInvB.addr, align 4, !tbaa !23
  %add22 = fadd float %add, %32
  %m_1MinvJt23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ24 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ24)
  %add26 = fadd float %add22, %call25
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add26, float* %m_Adiag, align 4, !tbaa !32
  ret %class.btJacobianEntry* %this1
}

define hidden void @_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btPoint2PointConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  call void @_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btPoint2PointConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo1"* %0)
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btPoint2PointConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) #3 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 7
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 4, !tbaa !17, !range !34
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4, !tbaa !35
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4, !tbaa !37
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 3, i32* %m_numConstraintRows2, align 4, !tbaa !35
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4, !tbaa !2
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 3, i32* %nub3, align 4, !tbaa !37
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define hidden void @_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btPoint2PointConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  %3 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 9
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4, !tbaa !26
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  call void @_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_(%class.btPoint2PointConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  ret void
}

define hidden void @_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_(%class.btPoint2PointConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %body0_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %body1_trans) #0 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %body0_trans.addr = alloca %class.btTransform*, align 4
  %body1_trans.addr = alloca %class.btTransform*, align 4
  %a1 = alloca %class.btVector3, align 4
  %angular0 = alloca %class.btVector3*, align 4
  %angular1 = alloca %class.btVector3*, align 4
  %angular2 = alloca %class.btVector3*, align 4
  %a1neg = alloca %class.btVector3, align 4
  %a2 = alloca %class.btVector3, align 4
  %angular027 = alloca %class.btVector3*, align 4
  %angular128 = alloca %class.btVector3*, align 4
  %angular232 = alloca %class.btVector3*, align 4
  %currERP = alloca float, align 4
  %k = alloca float, align 4
  %j = alloca i32, align 4
  %impulseClamp = alloca float, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  store %class.btTransform* %body0_trans, %class.btTransform** %body0_trans.addr, align 4, !tbaa !2
  store %class.btTransform* %body1_trans, %class.btTransform** %body1_trans.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %0, i32 0, i32 2
  %1 = load float*, float** %m_J1linearAxis, align 4, !tbaa !38
  %arrayidx = getelementptr inbounds float, float* %1, i32 0
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !23
  %2 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %2, i32 0, i32 2
  %3 = load float*, float** %m_J1linearAxis2, align 4, !tbaa !38
  %4 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %4, i32 0, i32 6
  %5 = load i32, i32* %rowskip, align 4, !tbaa !40
  %add = add nsw i32 %5, 1
  %arrayidx3 = getelementptr inbounds float, float* %3, i32 %add
  store float 1.000000e+00, float* %arrayidx3, align 4, !tbaa !23
  %6 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1linearAxis4 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %6, i32 0, i32 2
  %7 = load float*, float** %m_J1linearAxis4, align 4, !tbaa !38
  %8 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip5 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %8, i32 0, i32 6
  %9 = load i32, i32* %rowskip5, align 4, !tbaa !40
  %mul = mul nsw i32 2, %9
  %add6 = add nsw i32 %mul, 2
  %arrayidx7 = getelementptr inbounds float, float* %7, i32 %add6
  store float 1.000000e+00, float* %arrayidx7, align 4, !tbaa !23
  %10 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #6
  %11 = load %class.btTransform*, %class.btTransform** %body0_trans.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %11)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInAEv(%class.btPoint2PointConstraint* %this1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %12 = bitcast %class.btVector3** %angular0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %13, i32 0, i32 3
  %14 = load float*, float** %m_J1angularAxis, align 4, !tbaa !41
  %15 = bitcast float* %14 to %class.btVector3*
  store %class.btVector3* %15, %class.btVector3** %angular0, align 4, !tbaa !2
  %16 = bitcast %class.btVector3** %angular1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %17, i32 0, i32 3
  %18 = load float*, float** %m_J1angularAxis9, align 4, !tbaa !41
  %19 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip10 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %19, i32 0, i32 6
  %20 = load i32, i32* %rowskip10, align 4, !tbaa !40
  %add.ptr = getelementptr inbounds float, float* %18, i32 %20
  %21 = bitcast float* %add.ptr to %class.btVector3*
  store %class.btVector3* %21, %class.btVector3** %angular1, align 4, !tbaa !2
  %22 = bitcast %class.btVector3** %angular2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  %23 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J1angularAxis11 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %23, i32 0, i32 3
  %24 = load float*, float** %m_J1angularAxis11, align 4, !tbaa !41
  %25 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %25, i32 0, i32 6
  %26 = load i32, i32* %rowskip12, align 4, !tbaa !40
  %mul13 = mul nsw i32 2, %26
  %add.ptr14 = getelementptr inbounds float, float* %24, i32 %mul13
  %27 = bitcast float* %add.ptr14 to %class.btVector3*
  store %class.btVector3* %27, %class.btVector3** %angular2, align 4, !tbaa !2
  %28 = bitcast %class.btVector3* %a1neg to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #6
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %a1neg, %class.btVector3* nonnull align 4 dereferenceable(16) %a1)
  %29 = load %class.btVector3*, %class.btVector3** %angular0, align 4, !tbaa !2
  %30 = load %class.btVector3*, %class.btVector3** %angular1, align 4, !tbaa !2
  %31 = load %class.btVector3*, %class.btVector3** %angular2, align 4, !tbaa !2
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a1neg, %class.btVector3* %29, %class.btVector3* %30, %class.btVector3* %31)
  %32 = bitcast %class.btVector3* %a1neg to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #6
  %33 = bitcast %class.btVector3** %angular2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #6
  %34 = bitcast %class.btVector3** %angular1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  %35 = bitcast %class.btVector3** %angular0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #6
  %36 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %36, i32 0, i32 4
  %37 = load float*, float** %m_J2linearAxis, align 4, !tbaa !42
  %arrayidx15 = getelementptr inbounds float, float* %37, i32 0
  store float -1.000000e+00, float* %arrayidx15, align 4, !tbaa !23
  %38 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %38, i32 0, i32 4
  %39 = load float*, float** %m_J2linearAxis16, align 4, !tbaa !42
  %40 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip17 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %40, i32 0, i32 6
  %41 = load i32, i32* %rowskip17, align 4, !tbaa !40
  %add18 = add nsw i32 %41, 1
  %arrayidx19 = getelementptr inbounds float, float* %39, i32 %add18
  store float -1.000000e+00, float* %arrayidx19, align 4, !tbaa !23
  %42 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2linearAxis20 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %42, i32 0, i32 4
  %43 = load float*, float** %m_J2linearAxis20, align 4, !tbaa !42
  %44 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip21 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %44, i32 0, i32 6
  %45 = load i32, i32* %rowskip21, align 4, !tbaa !40
  %mul22 = mul nsw i32 2, %45
  %add23 = add nsw i32 %mul22, 2
  %arrayidx24 = getelementptr inbounds float, float* %43, i32 %add23
  store float -1.000000e+00, float* %arrayidx24, align 4, !tbaa !23
  %46 = bitcast %class.btVector3* %a2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #6
  %47 = load %class.btTransform*, %class.btTransform** %body1_trans.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %47)
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInBEv(%class.btPoint2PointConstraint* %this1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call25, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %48 = bitcast %class.btVector3** %angular027 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  %49 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %49, i32 0, i32 5
  %50 = load float*, float** %m_J2angularAxis, align 4, !tbaa !43
  %51 = bitcast float* %50 to %class.btVector3*
  store %class.btVector3* %51, %class.btVector3** %angular027, align 4, !tbaa !2
  %52 = bitcast %class.btVector3** %angular128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  %53 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis29 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %53, i32 0, i32 5
  %54 = load float*, float** %m_J2angularAxis29, align 4, !tbaa !43
  %55 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip30 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %55, i32 0, i32 6
  %56 = load i32, i32* %rowskip30, align 4, !tbaa !40
  %add.ptr31 = getelementptr inbounds float, float* %54, i32 %56
  %57 = bitcast float* %add.ptr31 to %class.btVector3*
  store %class.btVector3* %57, %class.btVector3** %angular128, align 4, !tbaa !2
  %58 = bitcast %class.btVector3** %angular232 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  %59 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_J2angularAxis33 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %59, i32 0, i32 5
  %60 = load float*, float** %m_J2angularAxis33, align 4, !tbaa !43
  %61 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip34 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %61, i32 0, i32 6
  %62 = load i32, i32* %rowskip34, align 4, !tbaa !40
  %mul35 = mul nsw i32 2, %62
  %add.ptr36 = getelementptr inbounds float, float* %60, i32 %mul35
  %63 = bitcast float* %add.ptr36 to %class.btVector3*
  store %class.btVector3* %63, %class.btVector3** %angular232, align 4, !tbaa !2
  %64 = load %class.btVector3*, %class.btVector3** %angular027, align 4, !tbaa !2
  %65 = load %class.btVector3*, %class.btVector3** %angular128, align 4, !tbaa !2
  %66 = load %class.btVector3*, %class.btVector3** %angular232, align 4, !tbaa !2
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a2, %class.btVector3* %64, %class.btVector3* %65, %class.btVector3* %66)
  %67 = bitcast %class.btVector3** %angular232 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast %class.btVector3** %angular128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast %class.btVector3** %angular027 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast float* %currERP to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #6
  %m_flags = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 4
  %71 = load i32, i32* %m_flags, align 4, !tbaa !10
  %and = and i32 %71, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_erp = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 5
  %72 = load float, float* %m_erp, align 4, !tbaa !44
  br label %cond.end

cond.false:                                       ; preds = %entry
  %73 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %73, i32 0, i32 1
  %74 = load float, float* %erp, align 4, !tbaa !45
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %72, %cond.true ], [ %74, %cond.false ]
  store float %cond, float* %currERP, align 4, !tbaa !23
  %75 = bitcast float* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #6
  %76 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %76, i32 0, i32 0
  %77 = load float, float* %fps, align 4, !tbaa !46
  %78 = load float, float* %currERP, align 4, !tbaa !23
  %mul37 = fmul float %77, %78
  store float %mul37, float* %k, align 4, !tbaa !23
  %79 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  store i32 0, i32* %j, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %80 = load i32, i32* %j, align 4, !tbaa !24
  %cmp = icmp slt i32 %80, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %81 = load float, float* %k, align 4, !tbaa !23
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a2)
  %82 = load i32, i32* %j, align 4, !tbaa !24
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %82
  %83 = load float, float* %arrayidx39, align 4, !tbaa !23
  %84 = load %class.btTransform*, %class.btTransform** %body1_trans.addr, align 4, !tbaa !2
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %84)
  %call41 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call40)
  %85 = load i32, i32* %j, align 4, !tbaa !24
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 %85
  %86 = load float, float* %arrayidx42, align 4, !tbaa !23
  %add43 = fadd float %83, %86
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a1)
  %87 = load i32, i32* %j, align 4, !tbaa !24
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 %87
  %88 = load float, float* %arrayidx45, align 4, !tbaa !23
  %sub = fsub float %add43, %88
  %89 = load %class.btTransform*, %class.btTransform** %body0_trans.addr, align 4, !tbaa !2
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %89)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %90 = load i32, i32* %j, align 4, !tbaa !24
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %90
  %91 = load float, float* %arrayidx48, align 4, !tbaa !23
  %sub49 = fsub float %sub, %91
  %mul50 = fmul float %81, %sub49
  %92 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %92, i32 0, i32 7
  %93 = load float*, float** %m_constraintError, align 4, !tbaa !47
  %94 = load i32, i32* %j, align 4, !tbaa !24
  %95 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip51 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %95, i32 0, i32 6
  %96 = load i32, i32* %rowskip51, align 4, !tbaa !40
  %mul52 = mul nsw i32 %94, %96
  %arrayidx53 = getelementptr inbounds float, float* %93, i32 %mul52
  store float %mul50, float* %arrayidx53, align 4, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %97 = load i32, i32* %j, align 4, !tbaa !24
  %inc = add nsw i32 %97, 1
  store i32 %inc, i32* %j, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_flags54 = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 4
  %98 = load i32, i32* %m_flags54, align 4, !tbaa !10
  %and55 = and i32 %98, 2
  %tobool56 = icmp ne i32 %and55, 0
  br i1 %tobool56, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  store i32 0, i32* %j, align 4, !tbaa !24
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc63, %if.then
  %99 = load i32, i32* %j, align 4, !tbaa !24
  %cmp58 = icmp slt i32 %99, 3
  br i1 %cmp58, label %for.body59, label %for.end65

for.body59:                                       ; preds = %for.cond57
  %m_cfm = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 6
  %100 = load float, float* %m_cfm, align 4, !tbaa !48
  %101 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %101, i32 0, i32 8
  %102 = load float*, float** %cfm, align 4, !tbaa !49
  %103 = load i32, i32* %j, align 4, !tbaa !24
  %104 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip60 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %104, i32 0, i32 6
  %105 = load i32, i32* %rowskip60, align 4, !tbaa !40
  %mul61 = mul nsw i32 %103, %105
  %arrayidx62 = getelementptr inbounds float, float* %102, i32 %mul61
  store float %100, float* %arrayidx62, align 4, !tbaa !23
  br label %for.inc63

for.inc63:                                        ; preds = %for.body59
  %106 = load i32, i32* %j, align 4, !tbaa !24
  %inc64 = add nsw i32 %106, 1
  store i32 %inc64, i32* %j, align 4, !tbaa !24
  br label %for.cond57

for.end65:                                        ; preds = %for.cond57
  br label %if.end

if.end:                                           ; preds = %for.end65, %for.end
  %107 = bitcast float* %impulseClamp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #6
  %m_setting = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 8
  %m_impulseClamp = getelementptr inbounds %struct.btConstraintSetting, %struct.btConstraintSetting* %m_setting, i32 0, i32 2
  %108 = load float, float* %m_impulseClamp, align 4, !tbaa !50
  store float %108, float* %impulseClamp, align 4, !tbaa !23
  store i32 0, i32* %j, align 4, !tbaa !24
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc80, %if.end
  %109 = load i32, i32* %j, align 4, !tbaa !24
  %cmp67 = icmp slt i32 %109, 3
  br i1 %cmp67, label %for.body68, label %for.end82

for.body68:                                       ; preds = %for.cond66
  %m_setting69 = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 8
  %m_impulseClamp70 = getelementptr inbounds %struct.btConstraintSetting, %struct.btConstraintSetting* %m_setting69, i32 0, i32 2
  %110 = load float, float* %m_impulseClamp70, align 4, !tbaa !50
  %cmp71 = fcmp ogt float %110, 0.000000e+00
  br i1 %cmp71, label %if.then72, label %if.end79

if.then72:                                        ; preds = %for.body68
  %111 = load float, float* %impulseClamp, align 4, !tbaa !23
  %fneg = fneg float %111
  %112 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %112, i32 0, i32 9
  %113 = load float*, float** %m_lowerLimit, align 4, !tbaa !51
  %114 = load i32, i32* %j, align 4, !tbaa !24
  %115 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip73 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %115, i32 0, i32 6
  %116 = load i32, i32* %rowskip73, align 4, !tbaa !40
  %mul74 = mul nsw i32 %114, %116
  %arrayidx75 = getelementptr inbounds float, float* %113, i32 %mul74
  store float %fneg, float* %arrayidx75, align 4, !tbaa !23
  %117 = load float, float* %impulseClamp, align 4, !tbaa !23
  %118 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %118, i32 0, i32 10
  %119 = load float*, float** %m_upperLimit, align 4, !tbaa !52
  %120 = load i32, i32* %j, align 4, !tbaa !24
  %121 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %rowskip76 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %121, i32 0, i32 6
  %122 = load i32, i32* %rowskip76, align 4, !tbaa !40
  %mul77 = mul nsw i32 %120, %122
  %arrayidx78 = getelementptr inbounds float, float* %119, i32 %mul77
  store float %117, float* %arrayidx78, align 4, !tbaa !23
  br label %if.end79

if.end79:                                         ; preds = %if.then72, %for.body68
  br label %for.inc80

for.inc80:                                        ; preds = %if.end79
  %123 = load i32, i32* %j, align 4, !tbaa !24
  %inc81 = add nsw i32 %123, 1
  store i32 %inc81, i32* %j, align 4, !tbaa !24
  br label %for.cond66

for.end82:                                        ; preds = %for.cond66
  %m_setting83 = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 8
  %m_damping = getelementptr inbounds %struct.btConstraintSetting, %struct.btConstraintSetting* %m_setting83, i32 0, i32 1
  %124 = load float, float* %m_damping, align 4, !tbaa !53
  %125 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4, !tbaa !2
  %m_damping84 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %125, i32 0, i32 13
  store float %124, float* %m_damping84, align 4, !tbaa !54
  %126 = bitcast float* %impulseClamp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #6
  %127 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #6
  %128 = bitcast float* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #6
  %129 = bitcast float* %currERP to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #6
  %130 = bitcast %class.btVector3* %a2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %130) #6
  %131 = bitcast %class.btVector3* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %131) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !23
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !23
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !23
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInAEv(%class.btPoint2PointConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %m_pivotInA = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 2
  ret %class.btVector3* %m_pivotInA
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !23
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !23
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !23
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !23
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %this, %class.btVector3* %v0, %class.btVector3* %v1, %class.btVector3* %v2) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !23
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %3 = load float, float* %call, align 4, !tbaa !23
  %fneg = fneg float %3
  store float %fneg, float* %ref.tmp2, align 4, !tbaa !23
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %call3)
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !23
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %9 = load float, float* %call7, align 4, !tbaa !23
  %fneg8 = fneg float %9
  store float %fneg8, float* %ref.tmp6, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %6, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %13 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  %14 = load float, float* %call10, align 4, !tbaa !23
  %fneg11 = fneg float %14
  store float %fneg11, float* %ref.tmp9, align 4, !tbaa !23
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %15 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %12, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %call12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %16 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInBEv(%class.btPoint2PointConstraint* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %m_pivotInB = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 3
  ret %class.btVector3* %m_pivotInB
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define hidden void @_ZN23btPoint2PointConstraint9updateRHSEf(%class.btPoint2PointConstraint* %this, float %timeStep) #3 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !23
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN23btPoint2PointConstraint8setParamEifi(%class.btPoint2PointConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !24
  store float %value, float* %value.addr, align 4, !tbaa !23
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !24
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %0 = load i32, i32* %axis.addr, align 4, !tbaa !24
  %cmp = icmp ne i32 %0, -1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %if.end

if.else:                                          ; preds = %entry
  %1 = load i32, i32* %num.addr, align 4, !tbaa !24
  switch i32 %1, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb2
    i32 4, label %sw.bb2
  ]

sw.bb:                                            ; preds = %if.else, %if.else
  %2 = load float, float* %value.addr, align 4, !tbaa !23
  %m_erp = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 5
  store float %2, float* %m_erp, align 4, !tbaa !44
  %m_flags = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 4
  %3 = load i32, i32* %m_flags, align 4, !tbaa !10
  %or = or i32 %3, 1
  store i32 %or, i32* %m_flags, align 4, !tbaa !10
  br label %sw.epilog

sw.bb2:                                           ; preds = %if.else, %if.else
  %4 = load float, float* %value.addr, align 4, !tbaa !23
  %m_cfm = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 6
  store float %4, float* %m_cfm, align 4, !tbaa !48
  %m_flags3 = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 4
  %5 = load i32, i32* %m_flags3, align 4, !tbaa !10
  %or4 = or i32 %5, 2
  store i32 %or4, i32* %m_flags3, align 4, !tbaa !10
  br label %sw.epilog

sw.default:                                       ; preds = %if.else
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb2, %sw.bb
  br label %if.end

if.end:                                           ; preds = %sw.epilog, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden float @_ZNK23btPoint2PointConstraint8getParamEii(%class.btPoint2PointConstraint* %this, i32 %num, i32 %axis) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store i32 %num, i32* %num.addr, align 4, !tbaa !24
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !24
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %0 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 0x47EFFFFFE0000000, float* %retVal, align 4, !tbaa !23
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !24
  %cmp = icmp ne i32 %1, -1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %if.end

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %num.addr, align 4, !tbaa !24
  switch i32 %2, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb2
    i32 4, label %sw.bb2
  ]

sw.bb:                                            ; preds = %if.else, %if.else
  %m_erp = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 5
  %3 = load float, float* %m_erp, align 4, !tbaa !44
  store float %3, float* %retVal, align 4, !tbaa !23
  br label %sw.epilog

sw.bb2:                                           ; preds = %if.else, %if.else
  %m_cfm = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 6
  %4 = load float, float* %m_cfm, align 4, !tbaa !48
  store float %4, float* %retVal, align 4, !tbaa !23
  br label %sw.epilog

sw.default:                                       ; preds = %if.else
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb2, %sw.bb
  br label %if.end

if.end:                                           ; preds = %sw.epilog, %if.then
  %5 = load float, float* %retVal, align 4, !tbaa !23
  %6 = bitcast float* %retVal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  ret float %5
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btPoint2PointConstraintD0Ev(%class.btPoint2PointConstraint* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %call = call %class.btPoint2PointConstraint* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to %class.btPoint2PointConstraint* (%class.btPoint2PointConstraint*)*)(%class.btPoint2PointConstraint* %this1) #6
  %0 = bitcast %class.btPoint2PointConstraint* %this1 to i8*
  call void @_ZN23btPoint2PointConstraintdlEPv(i8* %0) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.1* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.1* %ca, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4, !tbaa !24
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4, !tbaa !24
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !23
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %ca.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4, !tbaa !2
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4, !tbaa !2
  store float %2, float* %.addr2, align 4, !tbaa !23
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK23btPoint2PointConstraint28calculateSerializeBufferSizeEv(%class.btPoint2PointConstraint* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  ret i32 84
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK23btPoint2PointConstraint9serializeEPvP12btSerializer(%class.btPoint2PointConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %p2pData = alloca %struct.btPoint2PointConstraintFloatData*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %0 = bitcast %struct.btPoint2PointConstraintFloatData** %p2pData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btPoint2PointConstraintFloatData*
  store %struct.btPoint2PointConstraintFloatData* %2, %struct.btPoint2PointConstraintFloatData** %p2pData, align 4, !tbaa !2
  %3 = bitcast %class.btPoint2PointConstraint* %this1 to %class.btTypedConstraint*
  %4 = load %struct.btPoint2PointConstraintFloatData*, %struct.btPoint2PointConstraintFloatData** %p2pData, align 4, !tbaa !2
  %m_typeConstraintData = getelementptr inbounds %struct.btPoint2PointConstraintFloatData, %struct.btPoint2PointConstraintFloatData* %4, i32 0, i32 0
  %5 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %3, i8* %5, %class.btSerializer* %6)
  %m_pivotInA = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 2
  %7 = load %struct.btPoint2PointConstraintFloatData*, %struct.btPoint2PointConstraintFloatData** %p2pData, align 4, !tbaa !2
  %m_pivotInA2 = getelementptr inbounds %struct.btPoint2PointConstraintFloatData, %struct.btPoint2PointConstraintFloatData* %7, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_pivotInA, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_pivotInA2)
  %m_pivotInB = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 3
  %8 = load %struct.btPoint2PointConstraintFloatData*, %struct.btPoint2PointConstraintFloatData** %p2pData, align 4, !tbaa !2
  %m_pivotInB3 = getelementptr inbounds %struct.btPoint2PointConstraintFloatData, %struct.btPoint2PointConstraintFloatData* %8, i32 0, i32 2
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_pivotInB, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_pivotInB3)
  %9 = bitcast %struct.btPoint2PointConstraintFloatData** %p2pData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  ret i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !23
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !23
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !23
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !24
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !24
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !23
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !23
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !23
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !23
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !23
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !23
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !23
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !23
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !23
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !23
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !23
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !23
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !23
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !23
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !23
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !23
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !23
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !23
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !23
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !23
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !23
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !23
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !23
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btPoint2PointConstraintdlEPv(i8* %ptr) #5 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !24
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !24
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !23
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !24
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{i64 0, i64 16, !9}
!9 = !{!4, !4, i64 0}
!10 = !{!11, !13, i64 332}
!11 = !{!"_ZTS23btPoint2PointConstraint", !4, i64 48, !12, i64 300, !12, i64 316, !13, i64 332, !14, i64 336, !14, i64 340, !15, i64 344, !16, i64 348}
!12 = !{!"_ZTS9btVector3", !4, i64 0}
!13 = !{!"int", !4, i64 0}
!14 = !{!"float", !4, i64 0}
!15 = !{!"bool", !4, i64 0}
!16 = !{!"_ZTS19btConstraintSetting", !14, i64 0, !14, i64 4, !14, i64 8}
!17 = !{!11, !15, i64 344}
!18 = !{!16, !14, i64 0}
!19 = !{!16, !14, i64 4}
!20 = !{!16, !14, i64 8}
!21 = !{!22, !14, i64 36}
!22 = !{!"_ZTS17btTypedConstraint", !13, i64 8, !4, i64 12, !14, i64 16, !15, i64 20, !15, i64 21, !13, i64 24, !3, i64 28, !3, i64 32, !14, i64 36, !14, i64 40, !3, i64 44}
!23 = !{!14, !14, i64 0}
!24 = !{!13, !13, i64 0}
!25 = !{!22, !3, i64 28}
!26 = !{!22, !3, i64 32}
!27 = !{!28, !14, i64 344}
!28 = !{!"_ZTS11btRigidBody", !29, i64 264, !12, i64 312, !12, i64 328, !14, i64 344, !12, i64 348, !12, i64 364, !12, i64 380, !12, i64 396, !12, i64 412, !12, i64 428, !14, i64 444, !14, i64 448, !15, i64 452, !14, i64 456, !14, i64 460, !14, i64 464, !14, i64 468, !14, i64 472, !14, i64 476, !3, i64 480, !30, i64 484, !13, i64 504, !13, i64 508, !12, i64 512, !12, i64 528, !12, i64 544, !12, i64 560, !12, i64 576, !12, i64 592, !13, i64 608, !13, i64 612}
!29 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!30 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !31, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !15, i64 16}
!31 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!32 = !{!33, !14, i64 80}
!33 = !{!"_ZTS15btJacobianEntry", !12, i64 0, !12, i64 16, !12, i64 32, !12, i64 48, !12, i64 64, !14, i64 80}
!34 = !{i8 0, i8 2}
!35 = !{!36, !13, i64 0}
!36 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo1E", !13, i64 0, !13, i64 4}
!37 = !{!36, !13, i64 4}
!38 = !{!39, !3, i64 8}
!39 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo2E", !14, i64 0, !14, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !13, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !13, i64 48, !14, i64 52}
!40 = !{!39, !13, i64 24}
!41 = !{!39, !3, i64 12}
!42 = !{!39, !3, i64 16}
!43 = !{!39, !3, i64 20}
!44 = !{!11, !14, i64 336}
!45 = !{!39, !14, i64 4}
!46 = !{!39, !14, i64 0}
!47 = !{!39, !3, i64 28}
!48 = !{!11, !14, i64 340}
!49 = !{!39, !3, i64 32}
!50 = !{!11, !14, i64 356}
!51 = !{!39, !3, i64 36}
!52 = !{!39, !3, i64 40}
!53 = !{!11, !14, i64 352}
!54 = !{!39, !14, i64 52}
