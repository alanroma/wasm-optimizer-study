; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCollisionObject.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCollisionObject.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%struct.btBroadphaseProxy = type opaque
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btSerializer = type { i32 (...)** }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN17btCollisionObjectdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ = comdat any

$_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape = comdat any

$_ZNK17btCollisionObject28calculateSerializeBufferSizeEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

@_ZTV17btCollisionObject = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btCollisionObject to i8*), i8* bitcast (i1 (%class.btCollisionObject*, %class.btCollisionObject*)* @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ to i8*), i8* bitcast (%class.btCollisionObject* (%class.btCollisionObject*)* @_ZN17btCollisionObjectD1Ev to i8*), i8* bitcast (void (%class.btCollisionObject*)* @_ZN17btCollisionObjectD0Ev to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btCollisionShape*)* @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape to i8*), i8* bitcast (i32 (%class.btCollisionObject*)* @_ZNK17btCollisionObject28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)* @_ZNK17btCollisionObject9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btSerializer*)* @_ZNK17btCollisionObject21serializeSingleObjectEP12btSerializer to i8*)] }, align 4
@.str = private unnamed_addr constant [27 x i8] c"btCollisionObjectFloatData\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS17btCollisionObject = hidden constant [20 x i8] c"17btCollisionObject\00", align 1
@_ZTI17btCollisionObject = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btCollisionObject, i32 0, i32 0) }, align 4

@_ZN17btCollisionObjectC1Ev = hidden unnamed_addr alias %class.btCollisionObject* (%class.btCollisionObject*), %class.btCollisionObject* (%class.btCollisionObject*)* @_ZN17btCollisionObjectC2Ev
@_ZN17btCollisionObjectD1Ev = hidden unnamed_addr alias %class.btCollisionObject* (%class.btCollisionObject*), %class.btCollisionObject* (%class.btCollisionObject*)* @_ZN17btCollisionObjectD2Ev

define hidden %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV17btCollisionObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_worldTransform)
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_interpolationWorldTransform)
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_interpolationLinearVelocity)
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_interpolationAngularVelocity)
  %m_anisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 5
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 1.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_anisotropicFriction, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %4 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #6
  %m_hasAnisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 6
  store i32 0, i32* %m_hasAnisotropicFriction, align 4, !tbaa !10
  %m_contactProcessingThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 7
  store float 0x43ABC16D60000000, float* %m_contactProcessingThreshold, align 4, !tbaa !16
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4, !tbaa !17
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  store %class.btCollisionShape* null, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !18
  %m_extensionPointer = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 10
  store i8* null, i8** %m_extensionPointer, align 4, !tbaa !19
  %m_rootCollisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 11
  store %class.btCollisionShape* null, %class.btCollisionShape** %m_rootCollisionShape, align 4, !tbaa !20
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  store i32 1, i32* %m_collisionFlags, align 4, !tbaa !21
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  store i32 -1, i32* %m_islandTag1, align 4, !tbaa !22
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  store i32 -1, i32* %m_companionId, align 4, !tbaa !23
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  store i32 1, i32* %m_activationState1, align 4, !tbaa !24
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_deactivationTime, align 4, !tbaa !25
  %m_friction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  store float 5.000000e-01, float* %m_friction, align 4, !tbaa !26
  %m_restitution = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_restitution, align 4, !tbaa !27
  %m_rollingFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_rollingFriction, align 4, !tbaa !28
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  store i32 1, i32* %m_internalType, align 4, !tbaa !29
  %7 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 21
  %m_userObjectPointer = bitcast %union.anon* %7 to i8**
  store i8* null, i8** %m_userObjectPointer, align 4, !tbaa !30
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 22
  store float 1.000000e+00, float* %m_hitFraction, align 4, !tbaa !31
  %m_ccdSweptSphereRadius = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 23
  store float 0.000000e+00, float* %m_ccdSweptSphereRadius, align 4, !tbaa !32
  %m_ccdMotionThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  store float 0.000000e+00, float* %m_ccdMotionThreshold, align 4, !tbaa !33
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 25
  store i32 0, i32* %m_checkCollideWith, align 4, !tbaa !34
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  store i32 0, i32* %m_updateRevision, align 4, !tbaa !35
  %m_worldTransform8 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_worldTransform8)
  ret %class.btCollisionObject* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #6
  ret void
}

; Function Attrs: nounwind
define hidden %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV17btCollisionObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btCollisionObject* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN17btCollisionObjectD0Ev(%class.btCollisionObject* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call %class.btCollisionObject* @_ZN17btCollisionObjectD1Ev(%class.btCollisionObject* %this1) #6
  %0 = bitcast %class.btCollisionObject* %this1 to i8*
  call void @_ZN17btCollisionObjectdlEPv(i8* %0) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btCollisionObjectdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %this, i32 %newState) #3 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %newState.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store i32 %newState, i32* %newState.addr, align 4, !tbaa !36
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %0 = load i32, i32* %m_activationState1, align 4, !tbaa !24
  %cmp = icmp ne i32 %0, 4
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_activationState12 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %1 = load i32, i32* %m_activationState12, align 4, !tbaa !24
  %cmp3 = icmp ne i32 %1, 5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load i32, i32* %newState.addr, align 4, !tbaa !36
  %m_activationState14 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  store i32 %2, i32* %m_activationState14, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZNK17btCollisionObject20forceActivationStateEi(%class.btCollisionObject* %this, i32 %newState) #3 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %newState.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store i32 %newState, i32* %newState.addr, align 4, !tbaa !36
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i32, i32* %newState.addr, align 4, !tbaa !36
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  store i32 %0, i32* %m_activationState1, align 4, !tbaa !24
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %this, i1 zeroext %forceActivation) #3 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %forceActivation.addr = alloca i8, align 1
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %frombool = zext i1 %forceActivation to i8
  store i8 %frombool, i8* %forceActivation.addr, align 1, !tbaa !37
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i8, i8* %forceActivation.addr, align 1, !tbaa !37, !range !39
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %1 = load i32, i32* %m_collisionFlags, align 4, !tbaa !21
  %and = and i32 %1, 3
  %tobool2 = icmp ne i32 %and, 0
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %this1, i32 1)
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_deactivationTime, align 4, !tbaa !25
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  ret void
}

define hidden i8* @_ZNK17btCollisionObject9serializeEPvP12btSerializer(%class.btCollisionObject* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %dataOut = alloca %struct.btCollisionObjectFloatData*, align 4
  %name = alloca i8*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = bitcast %struct.btCollisionObjectFloatData** %dataOut to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btCollisionObjectFloatData*
  store %struct.btCollisionObjectFloatData* %2, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %3 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_worldTransform2 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %3, i32 0, i32 4
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_worldTransform, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_worldTransform2)
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  %4 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_interpolationWorldTransform3 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %4, i32 0, i32 5
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_interpolationWorldTransform, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_interpolationWorldTransform3)
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_interpolationLinearVelocity4 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %5, i32 0, i32 6
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_interpolationLinearVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_interpolationLinearVelocity4)
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 4
  %6 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_interpolationAngularVelocity5 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %6, i32 0, i32 7
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_interpolationAngularVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_interpolationAngularVelocity5)
  %m_anisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 5
  %7 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_anisotropicFriction6 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %7, i32 0, i32 8
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_anisotropicFriction, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_anisotropicFriction6)
  %m_hasAnisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 6
  %8 = load i32, i32* %m_hasAnisotropicFriction, align 4, !tbaa !10
  %9 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_hasAnisotropicFriction7 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %9, i32 0, i32 17
  store i32 %8, i32* %m_hasAnisotropicFriction7, align 4, !tbaa !40
  %m_contactProcessingThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 7
  %10 = load float, float* %m_contactProcessingThreshold, align 4, !tbaa !16
  %11 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_contactProcessingThreshold8 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %11, i32 0, i32 9
  store float %10, float* %m_contactProcessingThreshold8, align 4, !tbaa !45
  %12 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_broadphaseHandle = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %12, i32 0, i32 0
  store i8* null, i8** %m_broadphaseHandle, align 4, !tbaa !46
  %13 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %14 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !18
  %15 = bitcast %class.btCollisionShape* %14 to i8*
  %16 = bitcast %class.btSerializer* %13 to i8* (%class.btSerializer*, i8*)***
  %vtable = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %16, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable, i64 7
  %17 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn, align 4
  %call = call i8* %17(%class.btSerializer* %13, i8* %15)
  %18 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_collisionShape9 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %18, i32 0, i32 1
  store i8* %call, i8** %m_collisionShape9, align 4, !tbaa !47
  %19 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_rootCollisionShape = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %19, i32 0, i32 2
  store %struct.btCollisionShapeData* null, %struct.btCollisionShapeData** %m_rootCollisionShape, align 4, !tbaa !48
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %20 = load i32, i32* %m_collisionFlags, align 4, !tbaa !21
  %21 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_collisionFlags10 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %21, i32 0, i32 18
  store i32 %20, i32* %m_collisionFlags10, align 4, !tbaa !49
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %22 = load i32, i32* %m_islandTag1, align 4, !tbaa !22
  %23 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_islandTag111 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %23, i32 0, i32 19
  store i32 %22, i32* %m_islandTag111, align 4, !tbaa !50
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  %24 = load i32, i32* %m_companionId, align 4, !tbaa !23
  %25 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_companionId12 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %25, i32 0, i32 20
  store i32 %24, i32* %m_companionId12, align 4, !tbaa !51
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %26 = load i32, i32* %m_activationState1, align 4, !tbaa !24
  %27 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_activationState113 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %27, i32 0, i32 21
  store i32 %26, i32* %m_activationState113, align 4, !tbaa !52
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %28 = load float, float* %m_deactivationTime, align 4, !tbaa !25
  %29 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_deactivationTime14 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %29, i32 0, i32 10
  store float %28, float* %m_deactivationTime14, align 4, !tbaa !53
  %m_friction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  %30 = load float, float* %m_friction, align 4, !tbaa !26
  %31 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_friction15 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %31, i32 0, i32 11
  store float %30, float* %m_friction15, align 4, !tbaa !54
  %m_rollingFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 19
  %32 = load float, float* %m_rollingFriction, align 4, !tbaa !28
  %33 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_rollingFriction16 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %33, i32 0, i32 12
  store float %32, float* %m_rollingFriction16, align 4, !tbaa !55
  %m_restitution = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 18
  %34 = load float, float* %m_restitution, align 4, !tbaa !27
  %35 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_restitution17 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %35, i32 0, i32 13
  store float %34, float* %m_restitution17, align 4, !tbaa !56
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  %36 = load i32, i32* %m_internalType, align 4, !tbaa !29
  %37 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_internalType18 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %37, i32 0, i32 22
  store i32 %36, i32* %m_internalType18, align 4, !tbaa !57
  %38 = bitcast i8** %name to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  %39 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %40 = bitcast %class.btCollisionObject* %this1 to i8*
  %41 = bitcast %class.btSerializer* %39 to i8* (%class.btSerializer*, i8*)***
  %vtable19 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %41, align 4, !tbaa !6
  %vfn20 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable19, i64 10
  %42 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn20, align 4
  %call21 = call i8* %42(%class.btSerializer* %39, i8* %40)
  store i8* %call21, i8** %name, align 4, !tbaa !2
  %43 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %44 = load i8*, i8** %name, align 4, !tbaa !2
  %45 = bitcast %class.btSerializer* %43 to i8* (%class.btSerializer*, i8*)***
  %vtable22 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %45, align 4, !tbaa !6
  %vfn23 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable22, i64 7
  %46 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn23, align 4
  %call24 = call i8* %46(%class.btSerializer* %43, i8* %44)
  %47 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_name = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %47, i32 0, i32 3
  store i8* %call24, i8** %m_name, align 4, !tbaa !58
  %48 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_name25 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %48, i32 0, i32 3
  %49 = load i8*, i8** %m_name25, align 4, !tbaa !58
  %tobool = icmp ne i8* %49, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %50 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %51 = load i8*, i8** %name, align 4, !tbaa !2
  %52 = bitcast %class.btSerializer* %50 to void (%class.btSerializer*, i8*)***
  %vtable26 = load void (%class.btSerializer*, i8*)**, void (%class.btSerializer*, i8*)*** %52, align 4, !tbaa !6
  %vfn27 = getelementptr inbounds void (%class.btSerializer*, i8*)*, void (%class.btSerializer*, i8*)** %vtable26, i64 12
  %53 = load void (%class.btSerializer*, i8*)*, void (%class.btSerializer*, i8*)** %vfn27, align 4
  call void %53(%class.btSerializer* %50, i8* %51)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 22
  %54 = load float, float* %m_hitFraction, align 4, !tbaa !31
  %55 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_hitFraction28 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %55, i32 0, i32 14
  store float %54, float* %m_hitFraction28, align 4, !tbaa !59
  %m_ccdSweptSphereRadius = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 23
  %56 = load float, float* %m_ccdSweptSphereRadius, align 4, !tbaa !32
  %57 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_ccdSweptSphereRadius29 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %57, i32 0, i32 15
  store float %56, float* %m_ccdSweptSphereRadius29, align 4, !tbaa !60
  %m_ccdMotionThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %58 = load float, float* %m_ccdMotionThreshold, align 4, !tbaa !33
  %59 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_ccdMotionThreshold30 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %59, i32 0, i32 16
  store float %58, float* %m_ccdMotionThreshold30, align 4, !tbaa !61
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 25
  %60 = load i32, i32* %m_checkCollideWith, align 4, !tbaa !34
  %61 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4, !tbaa !2
  %m_checkCollideWith31 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %61, i32 0, i32 23
  store i32 %60, i32* %m_checkCollideWith31, align 4, !tbaa !62
  %62 = bitcast i8** %name to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  %63 = bitcast %struct.btCollisionObjectFloatData** %dataOut to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #6
  ret i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !36
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !8
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !36
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define hidden void @_ZNK17btCollisionObject21serializeSingleObjectEP12btSerializer(%class.btCollisionObject* %this, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast %class.btCollisionObject* %this1 to i32 (%class.btCollisionObject*)***
  %vtable = load i32 (%class.btCollisionObject*)**, i32 (%class.btCollisionObject*)*** %1, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vtable, i64 4
  %2 = load i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vfn, align 4
  %call = call i32 %2(%class.btCollisionObject* %this1)
  store i32 %call, i32* %len, align 4, !tbaa !36
  %3 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %5 = load i32, i32* %len, align 4, !tbaa !36
  %6 = bitcast %class.btSerializer* %4 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable2 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %6, align 4, !tbaa !6
  %vfn3 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable2, i64 4
  %7 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn3, align 4
  %call4 = call %class.btChunk* %7(%class.btSerializer* %4, i32 %5, i32 1)
  store %class.btChunk* %call4, %class.btChunk** %chunk, align 4, !tbaa !2
  %8 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %9, i32 0, i32 2
  %10 = load i8*, i8** %m_oldPtr, align 4, !tbaa !63
  %11 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %12 = bitcast %class.btCollisionObject* %this1 to i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)***
  %vtable5 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)**, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*** %12, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vtable5, i64 5
  %13 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vfn6, align 4
  %call7 = call i8* %13(%class.btCollisionObject* %this1, i8* %10, %class.btSerializer* %11)
  store i8* %call7, i8** %structType, align 4, !tbaa !2
  %14 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %15 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %16 = load i8*, i8** %structType, align 4, !tbaa !2
  %17 = bitcast %class.btCollisionObject* %this1 to i8*
  %18 = bitcast %class.btSerializer* %14 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable8 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %18, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable8, i64 5
  %19 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn9, align 4
  call void %19(%class.btSerializer* %14, %class.btChunk* %15, i8* %16, i32 1245859651, i8* %17)
  %20 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  %21 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_(%class.btCollisionObject* %this, %class.btCollisionObject* %0) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %0, %class.btCollisionObject** %.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  ret i1 true
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape(%class.btCollisionObject* %this, %class.btCollisionShape* %collisionShape) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !35
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4, !tbaa !35
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !18
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %m_rootCollisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 11
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_rootCollisionShape, align 4, !tbaa !20
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject28calculateSerializeBufferSizeEv(%class.btCollisionObject* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  ret i32 256
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !8
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #6
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #6
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #6
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !36
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %3
  %4 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !36
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !36
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !36
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !15, i64 180}
!11 = !{!"_ZTS17btCollisionObject", !12, i64 4, !12, i64 68, !14, i64 132, !14, i64 148, !14, i64 164, !15, i64 180, !9, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !15, i64 204, !15, i64 208, !15, i64 212, !15, i64 216, !9, i64 220, !9, i64 224, !9, i64 228, !9, i64 232, !15, i64 236, !4, i64 240, !9, i64 244, !9, i64 248, !9, i64 252, !15, i64 256, !15, i64 260}
!12 = !{!"_ZTS11btTransform", !13, i64 0, !14, i64 48}
!13 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!14 = !{!"_ZTS9btVector3", !4, i64 0}
!15 = !{!"int", !4, i64 0}
!16 = !{!11, !9, i64 184}
!17 = !{!11, !3, i64 188}
!18 = !{!11, !3, i64 192}
!19 = !{!11, !3, i64 196}
!20 = !{!11, !3, i64 200}
!21 = !{!11, !15, i64 204}
!22 = !{!11, !15, i64 208}
!23 = !{!11, !15, i64 212}
!24 = !{!11, !15, i64 216}
!25 = !{!11, !9, i64 220}
!26 = !{!11, !9, i64 224}
!27 = !{!11, !9, i64 228}
!28 = !{!11, !9, i64 232}
!29 = !{!11, !15, i64 236}
!30 = !{!4, !4, i64 0}
!31 = !{!11, !9, i64 244}
!32 = !{!11, !9, i64 248}
!33 = !{!11, !9, i64 252}
!34 = !{!11, !15, i64 256}
!35 = !{!11, !15, i64 260}
!36 = !{!15, !15, i64 0}
!37 = !{!38, !38, i64 0}
!38 = !{!"bool", !4, i64 0}
!39 = !{i8 0, i8 2}
!40 = !{!41, !15, i64 224}
!41 = !{!"_ZTS26btCollisionObjectFloatData", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !42, i64 16, !42, i64 80, !44, i64 144, !44, i64 160, !44, i64 176, !9, i64 192, !9, i64 196, !9, i64 200, !9, i64 204, !9, i64 208, !9, i64 212, !9, i64 216, !9, i64 220, !15, i64 224, !15, i64 228, !15, i64 232, !15, i64 236, !15, i64 240, !15, i64 244, !15, i64 248, !4, i64 252}
!42 = !{!"_ZTS20btTransformFloatData", !43, i64 0, !44, i64 48}
!43 = !{!"_ZTS20btMatrix3x3FloatData", !4, i64 0}
!44 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!45 = !{!41, !9, i64 192}
!46 = !{!41, !3, i64 0}
!47 = !{!41, !3, i64 4}
!48 = !{!41, !3, i64 8}
!49 = !{!41, !15, i64 228}
!50 = !{!41, !15, i64 232}
!51 = !{!41, !15, i64 236}
!52 = !{!41, !15, i64 240}
!53 = !{!41, !9, i64 196}
!54 = !{!41, !9, i64 200}
!55 = !{!41, !9, i64 204}
!56 = !{!41, !9, i64 208}
!57 = !{!41, !15, i64 244}
!58 = !{!41, !3, i64 12}
!59 = !{!41, !9, i64 212}
!60 = !{!41, !9, i64 216}
!61 = !{!41, !9, i64 220}
!62 = !{!41, !15, i64 248}
!63 = !{!64, !3, i64 8}
!64 = !{!"_ZTS7btChunk", !15, i64 0, !15, i64 4, !3, i64 8, !15, i64 12, !15, i64 16}
