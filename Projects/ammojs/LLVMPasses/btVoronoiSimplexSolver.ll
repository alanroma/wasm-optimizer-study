; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btVoronoiSimplexSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btVoronoiSimplexSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%class.btVector3 = type { [4 x float] }
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }

$_ZNK22btVoronoiSimplexSolver11numVerticesEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN25btSubSimplexClosestResult5resetEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff = comdat any

$_ZN25btSubSimplexClosestResult7isValidEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector39distance2ERKS_ = comdat any

$_ZNK9btVector3eqERKS_ = comdat any

$_ZN15btUsageBitfield5resetEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN25btSubSimplexClosestResultC2Ev = comdat any

$_ZN15btUsageBitfieldC2Ev = comdat any

; Function Attrs: nounwind
define hidden void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this, i32 %index) #0 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %index.addr = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_numVertices, align 4, !tbaa !8
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %m_numVertices2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %1 = load i32, i32* %m_numVertices2, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %1
  %m_simplexVectorW3 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %2 = load i32, i32* %index.addr, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW3, i32 0, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx4 to i8*
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !16
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %m_numVertices5 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %5 = load i32, i32* %m_numVertices5, align 4, !tbaa !8
  %arrayidx6 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 %5
  %m_simplexPointsP7 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %6 = load i32, i32* %index.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP7, i32 0, i32 %6
  %7 = bitcast %class.btVector3* %arrayidx8 to i8*
  %8 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !16
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %m_numVertices9 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %9 = load i32, i32* %m_numVertices9, align 4, !tbaa !8
  %arrayidx10 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 %9
  %m_simplexPointsQ11 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %10 = load i32, i32* %index.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ11, i32 0, i32 %10
  %11 = bitcast %class.btVector3* %arrayidx12 to i8*
  %12 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !16
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

define hidden void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %usedVerts) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %usedVerts.addr = alloca %struct.btUsageBitfield*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %struct.btUsageBitfield* %usedVerts, %struct.btUsageBitfield** %usedVerts.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp = icmp sge i32 %call, 4
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %0 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btUsageBitfield* %0 to i8*
  %bf.load = load i8, i8* %1, align 2
  %bf.lshr = lshr i8 %bf.load, 3
  %bf.clear = and i8 %bf.lshr, 1
  %bf.cast = zext i8 %bf.clear to i16
  %tobool = icmp ne i16 %bf.cast, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %call2 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp3 = icmp sge i32 %call2, 3
  br i1 %cmp3, label %land.lhs.true4, label %if.end11

land.lhs.true4:                                   ; preds = %if.end
  %2 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4, !tbaa !2
  %3 = bitcast %struct.btUsageBitfield* %2 to i8*
  %bf.load5 = load i8, i8* %3, align 2
  %bf.lshr6 = lshr i8 %bf.load5, 2
  %bf.clear7 = and i8 %bf.lshr6, 1
  %bf.cast8 = zext i8 %bf.clear7 to i16
  %tobool9 = icmp ne i16 %bf.cast8, 0
  br i1 %tobool9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %land.lhs.true4
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 2)
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %land.lhs.true4, %if.end
  %call12 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp13 = icmp sge i32 %call12, 2
  br i1 %cmp13, label %land.lhs.true14, label %if.end21

land.lhs.true14:                                  ; preds = %if.end11
  %4 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4, !tbaa !2
  %5 = bitcast %struct.btUsageBitfield* %4 to i8*
  %bf.load15 = load i8, i8* %5, align 2
  %bf.lshr16 = lshr i8 %bf.load15, 1
  %bf.clear17 = and i8 %bf.lshr16, 1
  %bf.cast18 = zext i8 %bf.clear17 to i16
  %tobool19 = icmp ne i16 %bf.cast18, 0
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %land.lhs.true14
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 1)
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %land.lhs.true14, %if.end11
  %call22 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp23 = icmp sge i32 %call22, 1
  br i1 %cmp23, label %land.lhs.true24, label %if.end30

land.lhs.true24:                                  ; preds = %if.end21
  %6 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btUsageBitfield* %6 to i8*
  %bf.load25 = load i8, i8* %7, align 2
  %bf.clear26 = and i8 %bf.load25, 1
  %bf.cast27 = zext i8 %bf.clear26 to i16
  %tobool28 = icmp ne i16 %bf.cast27, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %land.lhs.true24
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 0)
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %land.lhs.true24, %if.end21
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  ret i32 %0
}

define hidden void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* %this) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_cachedValidClosest = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest, align 4, !tbaa !18
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  store i32 0, i32* %m_numVertices, align 4, !tbaa !8
  %m_needsUpdate = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  store i8 1, i8* %m_needsUpdate, align 4, !tbaa !19
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #5
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store float 0x43ABC16D60000000, float* %ref.tmp2, align 4, !tbaa !20
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4, !tbaa !20
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !20
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %4 = bitcast %class.btVector3* %m_lastW to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !16
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #5
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #5
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %m_cachedBC)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !20
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !20
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !20
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !20
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !20
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !20
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !20
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

define linkonce_odr hidden void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_degenerate = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 4
  store i8 0, i8* %m_degenerate, align 4, !tbaa !21
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %this1, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices)
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #0 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %1 = bitcast %class.btVector3* %m_lastW to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !16
  %m_needsUpdate = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  store i8 1, i8* %m_needsUpdate, align 4, !tbaa !19
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %4 = load i32, i32* %m_numVertices, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !16
  %7 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %m_numVertices2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %8 = load i32, i32* %m_numVertices2, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 %8
  %9 = bitcast %class.btVector3* %arrayidx3 to i8*
  %10 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !16
  %11 = load %class.btVector3*, %class.btVector3** %q.addr, align 4, !tbaa !2
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %m_numVertices4 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %12 = load i32, i32* %m_numVertices4, align 4, !tbaa !8
  %arrayidx5 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 %12
  %13 = bitcast %class.btVector3* %arrayidx5 to i8*
  %14 = bitcast %class.btVector3* %11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !16
  %m_numVertices6 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %15 = load i32, i32* %m_numVertices6, align 4, !tbaa !8
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %m_numVertices6, align 4, !tbaa !8
  ret void
}

define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv(%class.btVoronoiSimplexSolver* %this) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %from = alloca %class.btVector3*, align 4
  %to = alloca %class.btVector3*, align 4
  %nearest = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %diff = alloca %class.btVector3, align 4
  %v = alloca %class.btVector3, align 4
  %t = alloca float, align 4
  %dotVV = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca %class.btVector3, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %ref.tmp64 = alloca %class.btVector3, align 4
  %ref.tmp70 = alloca %class.btVector3, align 4
  %p81 = alloca %class.btVector3, align 4
  %ref.tmp82 = alloca float, align 4
  %ref.tmp83 = alloca float, align 4
  %ref.tmp84 = alloca float, align 4
  %a = alloca %class.btVector3*, align 4
  %b = alloca %class.btVector3*, align 4
  %c = alloca %class.btVector3*, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp101 = alloca %class.btVector3, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %ref.tmp114 = alloca %class.btVector3, align 4
  %ref.tmp115 = alloca %class.btVector3, align 4
  %ref.tmp116 = alloca %class.btVector3, align 4
  %ref.tmp122 = alloca %class.btVector3, align 4
  %ref.tmp128 = alloca %class.btVector3, align 4
  %ref.tmp135 = alloca %class.btVector3, align 4
  %p146 = alloca %class.btVector3, align 4
  %ref.tmp147 = alloca float, align 4
  %ref.tmp148 = alloca float, align 4
  %ref.tmp149 = alloca float, align 4
  %a151 = alloca %class.btVector3*, align 4
  %b154 = alloca %class.btVector3*, align 4
  %c157 = alloca %class.btVector3*, align 4
  %d = alloca %class.btVector3*, align 4
  %hasSeperation = alloca i8, align 1
  %ref.tmp167 = alloca %class.btVector3, align 4
  %ref.tmp168 = alloca %class.btVector3, align 4
  %ref.tmp169 = alloca %class.btVector3, align 4
  %ref.tmp170 = alloca %class.btVector3, align 4
  %ref.tmp176 = alloca %class.btVector3, align 4
  %ref.tmp182 = alloca %class.btVector3, align 4
  %ref.tmp188 = alloca %class.btVector3, align 4
  %ref.tmp195 = alloca %class.btVector3, align 4
  %ref.tmp196 = alloca %class.btVector3, align 4
  %ref.tmp197 = alloca %class.btVector3, align 4
  %ref.tmp198 = alloca %class.btVector3, align 4
  %ref.tmp204 = alloca %class.btVector3, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %ref.tmp216 = alloca %class.btVector3, align 4
  %ref.tmp223 = alloca %class.btVector3, align 4
  %ref.tmp237 = alloca float, align 4
  %ref.tmp238 = alloca float, align 4
  %ref.tmp239 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_needsUpdate = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  %0 = load i8, i8* %m_needsUpdate, align 4, !tbaa !19, !range !22
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end252

if.then:                                          ; preds = %entry
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %m_cachedBC)
  %m_needsUpdate2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  store i8 0, i8* %m_needsUpdate2, align 4, !tbaa !19
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  switch i32 %call, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb3
    i32 2, label %sw.bb12
    i32 3, label %sw.bb80
    i32 4, label %sw.bb145
  ]

sw.bb:                                            ; preds = %if.then
  %m_cachedValidClosest = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest, align 4, !tbaa !18
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 0
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %1 = bitcast %class.btVector3* %m_cachedP1 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !16
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx4 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 0
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %3 = bitcast %class.btVector3* %m_cachedP2 to i8*
  %4 = bitcast %class.btVector3* %arrayidx4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !16
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #5
  %m_cachedP15 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP26 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP15, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP26)
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %6 = bitcast %class.btVector3* %m_cachedV to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !16
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #5
  %m_cachedBC7 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %m_cachedBC7)
  %m_cachedBC8 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %m_cachedBC8, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00)
  %m_cachedBC9 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call10 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC9)
  %m_cachedValidClosest11 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool = zext i1 %call10 to i8
  store i8 %frombool, i8* %m_cachedValidClosest11, align 4, !tbaa !18
  br label %sw.epilog

sw.bb12:                                          ; preds = %if.then
  %9 = bitcast %class.btVector3** %from to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 0
  store %class.btVector3* %arrayidx13, %class.btVector3** %from, align 4, !tbaa !2
  %10 = bitcast %class.btVector3** %to to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %m_simplexVectorW14 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx15 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW14, i32 0, i32 1
  store %class.btVector3* %arrayidx15, %class.btVector3** %to, align 4, !tbaa !2
  %11 = bitcast %class.btVector3* %nearest to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #5
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest)
  %12 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #5
  %13 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !20
  %14 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !20
  %15 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !20
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %16 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast %class.btVector3* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #5
  %20 = load %class.btVector3*, %class.btVector3** %from, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %20)
  %21 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #5
  %22 = load %class.btVector3*, %class.btVector3** %to, align 4, !tbaa !2
  %23 = load %class.btVector3*, %class.btVector3** %from, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23)
  %24 = bitcast float* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %diff)
  store float %call21, float* %t, align 4, !tbaa !20
  %25 = load float, float* %t, align 4, !tbaa !20
  %cmp = fcmp ogt float %25, 0.000000e+00
  br i1 %cmp, label %if.then22, label %if.else40

if.then22:                                        ; preds = %sw.bb12
  %26 = bitcast float* %dotVV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  %call23 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  store float %call23, float* %dotVV, align 4, !tbaa !20
  %27 = load float, float* %t, align 4, !tbaa !20
  %28 = load float, float* %dotVV, align 4, !tbaa !20
  %cmp24 = fcmp olt float %27, %28
  br i1 %cmp24, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.then22
  %29 = load float, float* %dotVV, align 4, !tbaa !20
  %30 = load float, float* %t, align 4, !tbaa !20
  %div = fdiv float %30, %29
  store float %div, float* %t, align 4, !tbaa !20
  %31 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp26, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26)
  %32 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #5
  %m_cachedBC28 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC28, i32 0, i32 1
  %33 = bitcast %struct.btUsageBitfield* %m_usedVertices to i8*
  %bf.load = load i8, i8* %33, align 4
  %bf.clear = and i8 %bf.load, -2
  %bf.set = or i8 %bf.clear, 1
  store i8 %bf.set, i8* %33, align 4
  %m_cachedBC29 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices30 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC29, i32 0, i32 1
  %34 = bitcast %struct.btUsageBitfield* %m_usedVertices30 to i8*
  %bf.load31 = load i8, i8* %34, align 4
  %bf.clear32 = and i8 %bf.load31, -3
  %bf.set33 = or i8 %bf.clear32, 2
  store i8 %bf.set33, i8* %34, align 4
  br label %if.end

if.else:                                          ; preds = %if.then22
  store float 1.000000e+00, float* %t, align 4, !tbaa !20
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %m_cachedBC35 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices36 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC35, i32 0, i32 1
  %35 = bitcast %struct.btUsageBitfield* %m_usedVertices36 to i8*
  %bf.load37 = load i8, i8* %35, align 4
  %bf.clear38 = and i8 %bf.load37, -3
  %bf.set39 = or i8 %bf.clear38, 2
  store i8 %bf.set39, i8* %35, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then25
  %36 = bitcast float* %dotVV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  br label %if.end46

if.else40:                                        ; preds = %sw.bb12
  store float 0.000000e+00, float* %t, align 4, !tbaa !20
  %m_cachedBC41 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices42 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC41, i32 0, i32 1
  %37 = bitcast %struct.btUsageBitfield* %m_usedVertices42 to i8*
  %bf.load43 = load i8, i8* %37, align 4
  %bf.clear44 = and i8 %bf.load43, -2
  %bf.set45 = or i8 %bf.clear44, 1
  store i8 %bf.set45, i8* %37, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.else40, %if.end
  %m_cachedBC47 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %38 = load float, float* %t, align 4, !tbaa !20
  %sub = fsub float 1.000000e+00, %38
  %39 = load float, float* %t, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %m_cachedBC47, float %sub, float %39, float 0.000000e+00, float 0.000000e+00)
  %40 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #5
  %41 = load %class.btVector3*, %class.btVector3** %from, align 4, !tbaa !2
  %42 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* nonnull align 4 dereferenceable(16) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp49)
  %43 = bitcast %class.btVector3* %nearest to i8*
  %44 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !16
  %45 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #5
  %46 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #5
  %47 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #5
  %m_simplexPointsP51 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx52 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP51, i32 0, i32 0
  %48 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #5
  %49 = bitcast %class.btVector3* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #5
  %m_simplexPointsP55 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx56 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP55, i32 0, i32 1
  %m_simplexPointsP57 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx58 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP57, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp54, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx56, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx58)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp53, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp54)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp50, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx52, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp53)
  %m_cachedP159 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %50 = bitcast %class.btVector3* %m_cachedP159 to i8*
  %51 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false), !tbaa.struct !16
  %52 = bitcast %class.btVector3* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #5
  %53 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #5
  %54 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #5
  %55 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #5
  %m_simplexPointsQ61 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx62 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ61, i32 0, i32 0
  %56 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #5
  %57 = bitcast %class.btVector3* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %57) #5
  %m_simplexPointsQ65 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx66 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ65, i32 0, i32 1
  %m_simplexPointsQ67 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx68 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ67, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp64, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx66, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx68)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp63, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp64)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp60, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx62, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp63)
  %m_cachedP269 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %58 = bitcast %class.btVector3* %m_cachedP269 to i8*
  %59 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 16, i1 false), !tbaa.struct !16
  %60 = bitcast %class.btVector3* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #5
  %61 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #5
  %62 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #5
  %63 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %63) #5
  %m_cachedP171 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP272 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp70, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP171, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP272)
  %m_cachedV73 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %64 = bitcast %class.btVector3* %m_cachedV73 to i8*
  %65 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %64, i8* align 4 %65, i32 16, i1 false), !tbaa.struct !16
  %66 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #5
  %m_cachedBC74 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices75 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC74, i32 0, i32 1
  call void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this1, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %m_usedVertices75)
  %m_cachedBC76 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call77 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC76)
  %m_cachedValidClosest78 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool79 = zext i1 %call77 to i8
  store i8 %frombool79, i8* %m_cachedValidClosest78, align 4, !tbaa !18
  %67 = bitcast float* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #5
  %69 = bitcast %class.btVector3* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %69) #5
  %70 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %70) #5
  %71 = bitcast %class.btVector3* %nearest to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %71) #5
  %72 = bitcast %class.btVector3** %to to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #5
  %73 = bitcast %class.btVector3** %from to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  br label %sw.epilog

sw.bb80:                                          ; preds = %if.then
  %74 = bitcast %class.btVector3* %p81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #5
  %75 = bitcast float* %ref.tmp82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #5
  store float 0.000000e+00, float* %ref.tmp82, align 4, !tbaa !20
  %76 = bitcast float* %ref.tmp83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #5
  store float 0.000000e+00, float* %ref.tmp83, align 4, !tbaa !20
  %77 = bitcast float* %ref.tmp84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #5
  store float 0.000000e+00, float* %ref.tmp84, align 4, !tbaa !20
  %call85 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p81, float* nonnull align 4 dereferenceable(4) %ref.tmp82, float* nonnull align 4 dereferenceable(4) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84)
  %78 = bitcast float* %ref.tmp84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  %79 = bitcast float* %ref.tmp83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #5
  %80 = bitcast float* %ref.tmp82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #5
  %81 = bitcast %class.btVector3** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #5
  %m_simplexVectorW86 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx87 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW86, i32 0, i32 0
  store %class.btVector3* %arrayidx87, %class.btVector3** %a, align 4, !tbaa !2
  %82 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #5
  %m_simplexVectorW88 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx89 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW88, i32 0, i32 1
  store %class.btVector3* %arrayidx89, %class.btVector3** %b, align 4, !tbaa !2
  %83 = bitcast %class.btVector3** %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #5
  %m_simplexVectorW90 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx91 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW90, i32 0, i32 2
  store %class.btVector3* %arrayidx91, %class.btVector3** %c, align 4, !tbaa !2
  %84 = load %class.btVector3*, %class.btVector3** %a, align 4, !tbaa !2
  %85 = load %class.btVector3*, %class.btVector3** %b, align 4, !tbaa !2
  %86 = load %class.btVector3*, %class.btVector3** %c, align 4, !tbaa !2
  %m_cachedBC92 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call93 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %p81, %class.btVector3* nonnull align 4 dereferenceable(16) %84, %class.btVector3* nonnull align 4 dereferenceable(16) %85, %class.btVector3* nonnull align 4 dereferenceable(16) %86, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %m_cachedBC92)
  %87 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %87) #5
  %88 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %88) #5
  %89 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %89) #5
  %m_simplexPointsP97 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx98 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP97, i32 0, i32 0
  %m_cachedBC99 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC99, i32 0, i32 3
  %arrayidx100 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx98, float* nonnull align 4 dereferenceable(4) %arrayidx100)
  %90 = bitcast %class.btVector3* %ref.tmp101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %90) #5
  %m_simplexPointsP102 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx103 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP102, i32 0, i32 1
  %m_cachedBC104 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords105 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC104, i32 0, i32 3
  %arrayidx106 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords105, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp101, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx103, float* nonnull align 4 dereferenceable(4) %arrayidx106)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp101)
  %91 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %91) #5
  %m_simplexPointsP108 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx109 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP108, i32 0, i32 2
  %m_cachedBC110 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords111 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC110, i32 0, i32 3
  %arrayidx112 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords111, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx109, float* nonnull align 4 dereferenceable(4) %arrayidx112)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp107)
  %m_cachedP1113 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %92 = bitcast %class.btVector3* %m_cachedP1113 to i8*
  %93 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %92, i8* align 4 %93, i32 16, i1 false), !tbaa.struct !16
  %94 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #5
  %95 = bitcast %class.btVector3* %ref.tmp101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #5
  %96 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %96) #5
  %97 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #5
  %98 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #5
  %99 = bitcast %class.btVector3* %ref.tmp114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %99) #5
  %100 = bitcast %class.btVector3* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %100) #5
  %101 = bitcast %class.btVector3* %ref.tmp116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %101) #5
  %m_simplexPointsQ117 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx118 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ117, i32 0, i32 0
  %m_cachedBC119 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords120 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC119, i32 0, i32 3
  %arrayidx121 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords120, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp116, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx121)
  %102 = bitcast %class.btVector3* %ref.tmp122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %102) #5
  %m_simplexPointsQ123 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx124 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ123, i32 0, i32 1
  %m_cachedBC125 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords126 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC125, i32 0, i32 3
  %arrayidx127 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords126, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp122, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx124, float* nonnull align 4 dereferenceable(4) %arrayidx127)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp115, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp116, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp122)
  %103 = bitcast %class.btVector3* %ref.tmp128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %103) #5
  %m_simplexPointsQ129 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx130 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ129, i32 0, i32 2
  %m_cachedBC131 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords132 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC131, i32 0, i32 3
  %arrayidx133 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords132, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp128, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx130, float* nonnull align 4 dereferenceable(4) %arrayidx133)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp114, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp115, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp128)
  %m_cachedP2134 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %104 = bitcast %class.btVector3* %m_cachedP2134 to i8*
  %105 = bitcast %class.btVector3* %ref.tmp114 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %104, i8* align 4 %105, i32 16, i1 false), !tbaa.struct !16
  %106 = bitcast %class.btVector3* %ref.tmp128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %106) #5
  %107 = bitcast %class.btVector3* %ref.tmp122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %107) #5
  %108 = bitcast %class.btVector3* %ref.tmp116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %108) #5
  %109 = bitcast %class.btVector3* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #5
  %110 = bitcast %class.btVector3* %ref.tmp114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %110) #5
  %111 = bitcast %class.btVector3* %ref.tmp135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %111) #5
  %m_cachedP1136 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP2137 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp135, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP1136, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP2137)
  %m_cachedV138 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %112 = bitcast %class.btVector3* %m_cachedV138 to i8*
  %113 = bitcast %class.btVector3* %ref.tmp135 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %112, i8* align 4 %113, i32 16, i1 false), !tbaa.struct !16
  %114 = bitcast %class.btVector3* %ref.tmp135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %114) #5
  %m_cachedBC139 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices140 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC139, i32 0, i32 1
  call void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this1, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %m_usedVertices140)
  %m_cachedBC141 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call142 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC141)
  %m_cachedValidClosest143 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool144 = zext i1 %call142 to i8
  store i8 %frombool144, i8* %m_cachedValidClosest143, align 4, !tbaa !18
  %115 = bitcast %class.btVector3** %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #5
  %116 = bitcast %class.btVector3** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  %117 = bitcast %class.btVector3** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast %class.btVector3* %p81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %118) #5
  br label %sw.epilog

sw.bb145:                                         ; preds = %if.then
  %119 = bitcast %class.btVector3* %p146 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %119) #5
  %120 = bitcast float* %ref.tmp147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #5
  store float 0.000000e+00, float* %ref.tmp147, align 4, !tbaa !20
  %121 = bitcast float* %ref.tmp148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #5
  store float 0.000000e+00, float* %ref.tmp148, align 4, !tbaa !20
  %122 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #5
  store float 0.000000e+00, float* %ref.tmp149, align 4, !tbaa !20
  %call150 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p146, float* nonnull align 4 dereferenceable(4) %ref.tmp147, float* nonnull align 4 dereferenceable(4) %ref.tmp148, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  %123 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  %124 = bitcast float* %ref.tmp148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = bitcast float* %ref.tmp147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #5
  %126 = bitcast %class.btVector3** %a151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #5
  %m_simplexVectorW152 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx153 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW152, i32 0, i32 0
  store %class.btVector3* %arrayidx153, %class.btVector3** %a151, align 4, !tbaa !2
  %127 = bitcast %class.btVector3** %b154 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #5
  %m_simplexVectorW155 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx156 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW155, i32 0, i32 1
  store %class.btVector3* %arrayidx156, %class.btVector3** %b154, align 4, !tbaa !2
  %128 = bitcast %class.btVector3** %c157 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #5
  %m_simplexVectorW158 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx159 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW158, i32 0, i32 2
  store %class.btVector3* %arrayidx159, %class.btVector3** %c157, align 4, !tbaa !2
  %129 = bitcast %class.btVector3** %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #5
  %m_simplexVectorW160 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx161 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW160, i32 0, i32 3
  store %class.btVector3* %arrayidx161, %class.btVector3** %d, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasSeperation) #5
  %130 = load %class.btVector3*, %class.btVector3** %a151, align 4, !tbaa !2
  %131 = load %class.btVector3*, %class.btVector3** %b154, align 4, !tbaa !2
  %132 = load %class.btVector3*, %class.btVector3** %c157, align 4, !tbaa !2
  %133 = load %class.btVector3*, %class.btVector3** %d, align 4, !tbaa !2
  %m_cachedBC162 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call163 = call zeroext i1 @_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %p146, %class.btVector3* nonnull align 4 dereferenceable(16) %130, %class.btVector3* nonnull align 4 dereferenceable(16) %131, %class.btVector3* nonnull align 4 dereferenceable(16) %132, %class.btVector3* nonnull align 4 dereferenceable(16) %133, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %m_cachedBC162)
  %frombool164 = zext i1 %call163 to i8
  store i8 %frombool164, i8* %hasSeperation, align 1, !tbaa !23
  %134 = load i8, i8* %hasSeperation, align 1, !tbaa !23, !range !22
  %tobool165 = trunc i8 %134 to i1
  br i1 %tobool165, label %if.then166, label %if.else229

if.then166:                                       ; preds = %sw.bb145
  %135 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %135) #5
  %136 = bitcast %class.btVector3* %ref.tmp168 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %136) #5
  %137 = bitcast %class.btVector3* %ref.tmp169 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %137) #5
  %138 = bitcast %class.btVector3* %ref.tmp170 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %138) #5
  %m_simplexPointsP171 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx172 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP171, i32 0, i32 0
  %m_cachedBC173 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords174 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC173, i32 0, i32 3
  %arrayidx175 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords174, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp170, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx172, float* nonnull align 4 dereferenceable(4) %arrayidx175)
  %139 = bitcast %class.btVector3* %ref.tmp176 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %139) #5
  %m_simplexPointsP177 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx178 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP177, i32 0, i32 1
  %m_cachedBC179 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords180 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC179, i32 0, i32 3
  %arrayidx181 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords180, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp176, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx178, float* nonnull align 4 dereferenceable(4) %arrayidx181)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp169, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp170, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp176)
  %140 = bitcast %class.btVector3* %ref.tmp182 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %140) #5
  %m_simplexPointsP183 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx184 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP183, i32 0, i32 2
  %m_cachedBC185 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords186 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC185, i32 0, i32 3
  %arrayidx187 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords186, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp182, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx184, float* nonnull align 4 dereferenceable(4) %arrayidx187)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp168, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp169, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp182)
  %141 = bitcast %class.btVector3* %ref.tmp188 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %141) #5
  %m_simplexPointsP189 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx190 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP189, i32 0, i32 3
  %m_cachedBC191 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords192 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC191, i32 0, i32 3
  %arrayidx193 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords192, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp188, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx190, float* nonnull align 4 dereferenceable(4) %arrayidx193)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp167, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp168, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp188)
  %m_cachedP1194 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %142 = bitcast %class.btVector3* %m_cachedP1194 to i8*
  %143 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %142, i8* align 4 %143, i32 16, i1 false), !tbaa.struct !16
  %144 = bitcast %class.btVector3* %ref.tmp188 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %144) #5
  %145 = bitcast %class.btVector3* %ref.tmp182 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %145) #5
  %146 = bitcast %class.btVector3* %ref.tmp176 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %146) #5
  %147 = bitcast %class.btVector3* %ref.tmp170 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %147) #5
  %148 = bitcast %class.btVector3* %ref.tmp169 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %148) #5
  %149 = bitcast %class.btVector3* %ref.tmp168 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %149) #5
  %150 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %150) #5
  %151 = bitcast %class.btVector3* %ref.tmp195 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %151) #5
  %152 = bitcast %class.btVector3* %ref.tmp196 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %152) #5
  %153 = bitcast %class.btVector3* %ref.tmp197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %153) #5
  %154 = bitcast %class.btVector3* %ref.tmp198 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %154) #5
  %m_simplexPointsQ199 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx200 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ199, i32 0, i32 0
  %m_cachedBC201 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords202 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC201, i32 0, i32 3
  %arrayidx203 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords202, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx200, float* nonnull align 4 dereferenceable(4) %arrayidx203)
  %155 = bitcast %class.btVector3* %ref.tmp204 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %155) #5
  %m_simplexPointsQ205 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx206 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ205, i32 0, i32 1
  %m_cachedBC207 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords208 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC207, i32 0, i32 3
  %arrayidx209 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords208, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp204, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx206, float* nonnull align 4 dereferenceable(4) %arrayidx209)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp204)
  %156 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %156) #5
  %m_simplexPointsQ211 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx212 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ211, i32 0, i32 2
  %m_cachedBC213 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords214 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC213, i32 0, i32 3
  %arrayidx215 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords214, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp210, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx212, float* nonnull align 4 dereferenceable(4) %arrayidx215)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp196, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp210)
  %157 = bitcast %class.btVector3* %ref.tmp216 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %157) #5
  %m_simplexPointsQ217 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx218 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ217, i32 0, i32 3
  %m_cachedBC219 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords220 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC219, i32 0, i32 3
  %arrayidx221 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords220, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp216, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx218, float* nonnull align 4 dereferenceable(4) %arrayidx221)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp195, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp196, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp216)
  %m_cachedP2222 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %158 = bitcast %class.btVector3* %m_cachedP2222 to i8*
  %159 = bitcast %class.btVector3* %ref.tmp195 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %158, i8* align 4 %159, i32 16, i1 false), !tbaa.struct !16
  %160 = bitcast %class.btVector3* %ref.tmp216 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %160) #5
  %161 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %161) #5
  %162 = bitcast %class.btVector3* %ref.tmp204 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %162) #5
  %163 = bitcast %class.btVector3* %ref.tmp198 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %163) #5
  %164 = bitcast %class.btVector3* %ref.tmp197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %164) #5
  %165 = bitcast %class.btVector3* %ref.tmp196 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %165) #5
  %166 = bitcast %class.btVector3* %ref.tmp195 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %166) #5
  %167 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %167) #5
  %m_cachedP1224 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP2225 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp223, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP1224, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP2225)
  %m_cachedV226 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %168 = bitcast %class.btVector3* %m_cachedV226 to i8*
  %169 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %168, i8* align 4 %169, i32 16, i1 false), !tbaa.struct !16
  %170 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %170) #5
  %m_cachedBC227 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices228 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC227, i32 0, i32 1
  call void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this1, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %m_usedVertices228)
  br label %if.end241

if.else229:                                       ; preds = %sw.bb145
  %m_cachedBC230 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_degenerate = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC230, i32 0, i32 4
  %171 = load i8, i8* %m_degenerate, align 4, !tbaa !24, !range !22
  %tobool231 = trunc i8 %171 to i1
  br i1 %tobool231, label %if.then232, label %if.else234

if.then232:                                       ; preds = %if.else229
  %m_cachedValidClosest233 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest233, align 4, !tbaa !18
  br label %if.end240

if.else234:                                       ; preds = %if.else229
  %m_cachedValidClosest235 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 1, i8* %m_cachedValidClosest235, align 4, !tbaa !18
  %m_cachedV236 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %172 = bitcast float* %ref.tmp237 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %172) #5
  store float 0.000000e+00, float* %ref.tmp237, align 4, !tbaa !20
  %173 = bitcast float* %ref.tmp238 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #5
  store float 0.000000e+00, float* %ref.tmp238, align 4, !tbaa !20
  %174 = bitcast float* %ref.tmp239 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %174) #5
  store float 0.000000e+00, float* %ref.tmp239, align 4, !tbaa !20
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_cachedV236, float* nonnull align 4 dereferenceable(4) %ref.tmp237, float* nonnull align 4 dereferenceable(4) %ref.tmp238, float* nonnull align 4 dereferenceable(4) %ref.tmp239)
  %175 = bitcast float* %ref.tmp239 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  %176 = bitcast float* %ref.tmp238 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = bitcast float* %ref.tmp237 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  br label %if.end240

if.end240:                                        ; preds = %if.else234, %if.then232
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end241:                                        ; preds = %if.then166
  %m_cachedBC242 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call243 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC242)
  %m_cachedValidClosest244 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool245 = zext i1 %call243 to i8
  store i8 %frombool245, i8* %m_cachedValidClosest244, align 4, !tbaa !18
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end241, %if.end240
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasSeperation) #5
  %178 = bitcast %class.btVector3** %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast %class.btVector3** %c157 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  %180 = bitcast %class.btVector3** %b154 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #5
  %181 = bitcast %class.btVector3** %a151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #5
  %182 = bitcast %class.btVector3* %p146 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %182) #5
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  %m_cachedValidClosest251 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest251, align 4, !tbaa !18
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %cleanup, %sw.bb80, %if.end46, %sw.bb3, %sw.bb
  br label %if.end252

if.end252:                                        ; preds = %sw.epilog, %entry
  %m_cachedValidClosest253 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %183 = load i8, i8* %m_cachedValidClosest253, align 4, !tbaa !18, !range !22
  %tobool254 = trunc i8 %183 to i1
  ret i1 %tobool254
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !20
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !20
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !20
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !20
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !20
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !20
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !20
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !20
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !20
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %this, float %a, float %b, float %c, float %d) #0 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  %c.addr = alloca float, align 4
  %d.addr = alloca float, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4, !tbaa !2
  store float %a, float* %a.addr, align 4, !tbaa !20
  store float %b, float* %b.addr, align 4, !tbaa !20
  store float %c, float* %c.addr, align 4, !tbaa !20
  store float %d, float* %d.addr, align 4, !tbaa !20
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %0 = load float, float* %a.addr, align 4, !tbaa !20
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  store float %0, float* %arrayidx, align 4, !tbaa !20
  %1 = load float, float* %b.addr, align 4, !tbaa !20
  %m_barycentricCoords2 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords2, i32 0, i32 1
  store float %1, float* %arrayidx3, align 4, !tbaa !20
  %2 = load float, float* %c.addr, align 4, !tbaa !20
  %m_barycentricCoords4 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords4, i32 0, i32 2
  store float %2, float* %arrayidx5, align 4, !tbaa !20
  %3 = load float, float* %d.addr, align 4, !tbaa !20
  %m_barycentricCoords6 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords6, i32 0, i32 3
  store float %3, float* %arrayidx7, align 4, !tbaa !20
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %valid = alloca i8, align 1
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %valid) #5
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !20
  %cmp = fcmp oge float %0, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %m_barycentricCoords2 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !20
  %cmp4 = fcmp oge float %1, 0.000000e+00
  br i1 %cmp4, label %land.lhs.true5, label %land.end

land.lhs.true5:                                   ; preds = %land.lhs.true
  %m_barycentricCoords6 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords6, i32 0, i32 2
  %2 = load float, float* %arrayidx7, align 4, !tbaa !20
  %cmp8 = fcmp oge float %2, 0.000000e+00
  br i1 %cmp8, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true5
  %m_barycentricCoords9 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords9, i32 0, i32 3
  %3 = load float, float* %arrayidx10, align 4, !tbaa !20
  %cmp11 = fcmp oge float %3, 0.000000e+00
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true5, %land.lhs.true, %entry
  %4 = phi i1 [ false, %land.lhs.true5 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp11, %land.rhs ]
  %frombool = zext i1 %4 to i8
  store i8 %frombool, i8* %valid, align 1, !tbaa !23
  %5 = load i8, i8* %valid, align 1, !tbaa !23, !range !22
  %tobool = trunc i8 %5 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %valid) #5
  ret i1 %tobool
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !20
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !20
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !20
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !20
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !20
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !20
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !20
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !20
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !20
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !20
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !20
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !20
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !20
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !20
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !20
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !20
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !20
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !20
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !20
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !20
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !20
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !20
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !20
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !20
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  ret void
}

define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %result) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %result.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %ab = alloca %class.btVector3, align 4
  %ac = alloca %class.btVector3, align 4
  %ap = alloca %class.btVector3, align 4
  %d1 = alloca float, align 4
  %d2 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %bp = alloca %class.btVector3, align 4
  %d3 = alloca float, align 4
  %d4 = alloca float, align 4
  %vc = alloca float, align 4
  %v = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %cp = alloca %class.btVector3, align 4
  %d5 = alloca float, align 4
  %d6 = alloca float, align 4
  %vb = alloca float, align 4
  %w = alloca float, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %va = alloca float, align 4
  %w84 = alloca float, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp91 = alloca %class.btVector3, align 4
  %denom = alloca float, align 4
  %v106 = alloca float, align 4
  %w108 = alloca float, align 4
  %ref.tmp110 = alloca %class.btVector3, align 4
  %ref.tmp111 = alloca %class.btVector3, align 4
  %ref.tmp112 = alloca %class.btVector3, align 4
  %ref.tmp113 = alloca %class.btVector3, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  store %struct.btSubSimplexClosestResult* %result, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %0, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices)
  %1 = bitcast %class.btVector3* %ab to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #5
  %2 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = bitcast %class.btVector3* %ac to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #5
  %5 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = bitcast %class.btVector3* %ap to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #5
  %8 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %9 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ap, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %10 = bitcast float* %d1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %ap)
  store float %call, float* %d1, align 4, !tbaa !20
  %11 = bitcast float* %d2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %ap)
  store float %call2, float* %d2, align 4, !tbaa !20
  %12 = load float, float* %d1, align 4, !tbaa !20
  %cmp = fcmp ole float %12, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %13 = load float, float* %d2, align 4, !tbaa !20
  %cmp3 = fcmp ole float %13, 0.000000e+00
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %14 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %15 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %15, i32 0, i32 0
  %16 = bitcast %class.btVector3* %m_closestPointOnSimplex to i8*
  %17 = bitcast %class.btVector3* %14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !16
  %18 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices4 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %18, i32 0, i32 1
  %19 = bitcast %struct.btUsageBitfield* %m_usedVertices4 to i8*
  %bf.load = load i8, i8* %19, align 4
  %bf.clear = and i8 %bf.load, -2
  %bf.set = or i8 %bf.clear, 1
  store i8 %bf.set, i8* %19, align 4
  %20 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %20, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup137

if.end:                                           ; preds = %land.lhs.true, %entry
  %21 = bitcast %class.btVector3* %bp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #5
  %22 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %23 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %bp, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23)
  %24 = bitcast float* %d3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %bp)
  store float %call5, float* %d3, align 4, !tbaa !20
  %25 = bitcast float* %d4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  %call6 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %bp)
  store float %call6, float* %d4, align 4, !tbaa !20
  %26 = load float, float* %d3, align 4, !tbaa !20
  %cmp7 = fcmp oge float %26, 0.000000e+00
  br i1 %cmp7, label %land.lhs.true8, label %if.end16

land.lhs.true8:                                   ; preds = %if.end
  %27 = load float, float* %d4, align 4, !tbaa !20
  %28 = load float, float* %d3, align 4, !tbaa !20
  %cmp9 = fcmp ole float %27, %28
  br i1 %cmp9, label %if.then10, label %if.end16

if.then10:                                        ; preds = %land.lhs.true8
  %29 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %30 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex11 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %30, i32 0, i32 0
  %31 = bitcast %class.btVector3* %m_closestPointOnSimplex11 to i8*
  %32 = bitcast %class.btVector3* %29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false), !tbaa.struct !16
  %33 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices12 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %33, i32 0, i32 1
  %34 = bitcast %struct.btUsageBitfield* %m_usedVertices12 to i8*
  %bf.load13 = load i8, i8* %34, align 4
  %bf.clear14 = and i8 %bf.load13, -3
  %bf.set15 = or i8 %bf.clear14, 2
  store i8 %bf.set15, i8* %34, align 4
  %35 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %35, float 0.000000e+00, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup134

if.end16:                                         ; preds = %land.lhs.true8, %if.end
  %36 = bitcast float* %vc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #5
  %37 = load float, float* %d1, align 4, !tbaa !20
  %38 = load float, float* %d4, align 4, !tbaa !20
  %mul = fmul float %37, %38
  %39 = load float, float* %d3, align 4, !tbaa !20
  %40 = load float, float* %d2, align 4, !tbaa !20
  %mul17 = fmul float %39, %40
  %sub = fsub float %mul, %mul17
  store float %sub, float* %vc, align 4, !tbaa !20
  %41 = load float, float* %vc, align 4, !tbaa !20
  %cmp18 = fcmp ole float %41, 0.000000e+00
  br i1 %cmp18, label %land.lhs.true19, label %if.end36

land.lhs.true19:                                  ; preds = %if.end16
  %42 = load float, float* %d1, align 4, !tbaa !20
  %cmp20 = fcmp oge float %42, 0.000000e+00
  br i1 %cmp20, label %land.lhs.true21, label %if.end36

land.lhs.true21:                                  ; preds = %land.lhs.true19
  %43 = load float, float* %d3, align 4, !tbaa !20
  %cmp22 = fcmp ole float %43, 0.000000e+00
  br i1 %cmp22, label %if.then23, label %if.end36

if.then23:                                        ; preds = %land.lhs.true21
  %44 = bitcast float* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  %45 = load float, float* %d1, align 4, !tbaa !20
  %46 = load float, float* %d1, align 4, !tbaa !20
  %47 = load float, float* %d3, align 4, !tbaa !20
  %sub24 = fsub float %46, %47
  %div = fdiv float %45, %sub24
  store float %div, float* %v, align 4, !tbaa !20
  %48 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #5
  %49 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %50 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp25, float* nonnull align 4 dereferenceable(4) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ab)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %49, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25)
  %51 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex26 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %51, i32 0, i32 0
  %52 = bitcast %class.btVector3* %m_closestPointOnSimplex26 to i8*
  %53 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 16, i1 false), !tbaa.struct !16
  %54 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #5
  %55 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #5
  %56 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices27 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %56, i32 0, i32 1
  %57 = bitcast %struct.btUsageBitfield* %m_usedVertices27 to i8*
  %bf.load28 = load i8, i8* %57, align 4
  %bf.clear29 = and i8 %bf.load28, -2
  %bf.set30 = or i8 %bf.clear29, 1
  store i8 %bf.set30, i8* %57, align 4
  %58 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices31 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %58, i32 0, i32 1
  %59 = bitcast %struct.btUsageBitfield* %m_usedVertices31 to i8*
  %bf.load32 = load i8, i8* %59, align 4
  %bf.clear33 = and i8 %bf.load32, -3
  %bf.set34 = or i8 %bf.clear33, 2
  store i8 %bf.set34, i8* %59, align 4
  %60 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %61 = load float, float* %v, align 4, !tbaa !20
  %sub35 = fsub float 1.000000e+00, %61
  %62 = load float, float* %v, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %60, float %sub35, float %62, float 0.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %63 = bitcast float* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  br label %cleanup133

if.end36:                                         ; preds = %land.lhs.true21, %land.lhs.true19, %if.end16
  %64 = bitcast %class.btVector3* %cp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #5
  %65 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %66 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %cp, %class.btVector3* nonnull align 4 dereferenceable(16) %65, %class.btVector3* nonnull align 4 dereferenceable(16) %66)
  %67 = bitcast float* %d5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #5
  %call37 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %cp)
  store float %call37, float* %d5, align 4, !tbaa !20
  %68 = bitcast float* %d6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #5
  %call38 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %cp)
  store float %call38, float* %d6, align 4, !tbaa !20
  %69 = load float, float* %d6, align 4, !tbaa !20
  %cmp39 = fcmp oge float %69, 0.000000e+00
  br i1 %cmp39, label %land.lhs.true40, label %if.end48

land.lhs.true40:                                  ; preds = %if.end36
  %70 = load float, float* %d5, align 4, !tbaa !20
  %71 = load float, float* %d6, align 4, !tbaa !20
  %cmp41 = fcmp ole float %70, %71
  br i1 %cmp41, label %if.then42, label %if.end48

if.then42:                                        ; preds = %land.lhs.true40
  %72 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %73 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex43 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %73, i32 0, i32 0
  %74 = bitcast %class.btVector3* %m_closestPointOnSimplex43 to i8*
  %75 = bitcast %class.btVector3* %72 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 16, i1 false), !tbaa.struct !16
  %76 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices44 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %76, i32 0, i32 1
  %77 = bitcast %struct.btUsageBitfield* %m_usedVertices44 to i8*
  %bf.load45 = load i8, i8* %77, align 4
  %bf.clear46 = and i8 %bf.load45, -5
  %bf.set47 = or i8 %bf.clear46, 4
  store i8 %bf.set47, i8* %77, align 4
  %78 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %78, float 0.000000e+00, float 0.000000e+00, float 1.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end48:                                         ; preds = %land.lhs.true40, %if.end36
  %79 = bitcast float* %vb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #5
  %80 = load float, float* %d5, align 4, !tbaa !20
  %81 = load float, float* %d2, align 4, !tbaa !20
  %mul49 = fmul float %80, %81
  %82 = load float, float* %d1, align 4, !tbaa !20
  %83 = load float, float* %d6, align 4, !tbaa !20
  %mul50 = fmul float %82, %83
  %sub51 = fsub float %mul49, %mul50
  store float %sub51, float* %vb, align 4, !tbaa !20
  %84 = load float, float* %vb, align 4, !tbaa !20
  %cmp52 = fcmp ole float %84, 0.000000e+00
  br i1 %cmp52, label %land.lhs.true53, label %if.end72

land.lhs.true53:                                  ; preds = %if.end48
  %85 = load float, float* %d2, align 4, !tbaa !20
  %cmp54 = fcmp oge float %85, 0.000000e+00
  br i1 %cmp54, label %land.lhs.true55, label %if.end72

land.lhs.true55:                                  ; preds = %land.lhs.true53
  %86 = load float, float* %d6, align 4, !tbaa !20
  %cmp56 = fcmp ole float %86, 0.000000e+00
  br i1 %cmp56, label %if.then57, label %if.end72

if.then57:                                        ; preds = %land.lhs.true55
  %87 = bitcast float* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #5
  %88 = load float, float* %d2, align 4, !tbaa !20
  %89 = load float, float* %d2, align 4, !tbaa !20
  %90 = load float, float* %d6, align 4, !tbaa !20
  %sub58 = fsub float %89, %90
  %div59 = fdiv float %88, %sub58
  store float %div59, float* %w, align 4, !tbaa !20
  %91 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %91) #5
  %92 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %93 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp61, float* nonnull align 4 dereferenceable(4) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %ac)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp60, %class.btVector3* nonnull align 4 dereferenceable(16) %92, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp61)
  %94 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex62 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %94, i32 0, i32 0
  %95 = bitcast %class.btVector3* %m_closestPointOnSimplex62 to i8*
  %96 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %95, i8* align 4 %96, i32 16, i1 false), !tbaa.struct !16
  %97 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #5
  %98 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #5
  %99 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices63 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %99, i32 0, i32 1
  %100 = bitcast %struct.btUsageBitfield* %m_usedVertices63 to i8*
  %bf.load64 = load i8, i8* %100, align 4
  %bf.clear65 = and i8 %bf.load64, -2
  %bf.set66 = or i8 %bf.clear65, 1
  store i8 %bf.set66, i8* %100, align 4
  %101 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices67 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %101, i32 0, i32 1
  %102 = bitcast %struct.btUsageBitfield* %m_usedVertices67 to i8*
  %bf.load68 = load i8, i8* %102, align 4
  %bf.clear69 = and i8 %bf.load68, -5
  %bf.set70 = or i8 %bf.clear69, 4
  store i8 %bf.set70, i8* %102, align 4
  %103 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %104 = load float, float* %w, align 4, !tbaa !20
  %sub71 = fsub float 1.000000e+00, %104
  %105 = load float, float* %w, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %103, float %sub71, float 0.000000e+00, float %105, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %106 = bitcast float* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  br label %cleanup129

if.end72:                                         ; preds = %land.lhs.true55, %land.lhs.true53, %if.end48
  %107 = bitcast float* %va to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #5
  %108 = load float, float* %d3, align 4, !tbaa !20
  %109 = load float, float* %d6, align 4, !tbaa !20
  %mul73 = fmul float %108, %109
  %110 = load float, float* %d5, align 4, !tbaa !20
  %111 = load float, float* %d4, align 4, !tbaa !20
  %mul74 = fmul float %110, %111
  %sub75 = fsub float %mul73, %mul74
  store float %sub75, float* %va, align 4, !tbaa !20
  %112 = load float, float* %va, align 4, !tbaa !20
  %cmp76 = fcmp ole float %112, 0.000000e+00
  br i1 %cmp76, label %land.lhs.true77, label %if.end102

land.lhs.true77:                                  ; preds = %if.end72
  %113 = load float, float* %d4, align 4, !tbaa !20
  %114 = load float, float* %d3, align 4, !tbaa !20
  %sub78 = fsub float %113, %114
  %cmp79 = fcmp oge float %sub78, 0.000000e+00
  br i1 %cmp79, label %land.lhs.true80, label %if.end102

land.lhs.true80:                                  ; preds = %land.lhs.true77
  %115 = load float, float* %d5, align 4, !tbaa !20
  %116 = load float, float* %d6, align 4, !tbaa !20
  %sub81 = fsub float %115, %116
  %cmp82 = fcmp oge float %sub81, 0.000000e+00
  br i1 %cmp82, label %if.then83, label %if.end102

if.then83:                                        ; preds = %land.lhs.true80
  %117 = bitcast float* %w84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #5
  %118 = load float, float* %d4, align 4, !tbaa !20
  %119 = load float, float* %d3, align 4, !tbaa !20
  %sub85 = fsub float %118, %119
  %120 = load float, float* %d4, align 4, !tbaa !20
  %121 = load float, float* %d3, align 4, !tbaa !20
  %sub86 = fsub float %120, %121
  %122 = load float, float* %d5, align 4, !tbaa !20
  %123 = load float, float* %d6, align 4, !tbaa !20
  %sub87 = fsub float %122, %123
  %add = fadd float %sub86, %sub87
  %div88 = fdiv float %sub85, %add
  store float %div88, float* %w84, align 4, !tbaa !20
  %124 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #5
  %125 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %126 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %126) #5
  %127 = bitcast %class.btVector3* %ref.tmp91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %127) #5
  %128 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %129 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp91, %class.btVector3* nonnull align 4 dereferenceable(16) %128, %class.btVector3* nonnull align 4 dereferenceable(16) %129)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp90, float* nonnull align 4 dereferenceable(4) %w84, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp91)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %125, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %130 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex92 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %130, i32 0, i32 0
  %131 = bitcast %class.btVector3* %m_closestPointOnSimplex92 to i8*
  %132 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %131, i8* align 4 %132, i32 16, i1 false), !tbaa.struct !16
  %133 = bitcast %class.btVector3* %ref.tmp91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %133) #5
  %134 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %134) #5
  %135 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %135) #5
  %136 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices93 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %136, i32 0, i32 1
  %137 = bitcast %struct.btUsageBitfield* %m_usedVertices93 to i8*
  %bf.load94 = load i8, i8* %137, align 4
  %bf.clear95 = and i8 %bf.load94, -3
  %bf.set96 = or i8 %bf.clear95, 2
  store i8 %bf.set96, i8* %137, align 4
  %138 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices97 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %138, i32 0, i32 1
  %139 = bitcast %struct.btUsageBitfield* %m_usedVertices97 to i8*
  %bf.load98 = load i8, i8* %139, align 4
  %bf.clear99 = and i8 %bf.load98, -5
  %bf.set100 = or i8 %bf.clear99, 4
  store i8 %bf.set100, i8* %139, align 4
  %140 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %141 = load float, float* %w84, align 4, !tbaa !20
  %sub101 = fsub float 1.000000e+00, %141
  %142 = load float, float* %w84, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %140, float 0.000000e+00, float %sub101, float %142, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %143 = bitcast float* %w84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #5
  br label %cleanup

if.end102:                                        ; preds = %land.lhs.true80, %land.lhs.true77, %if.end72
  %144 = bitcast float* %denom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #5
  %145 = load float, float* %va, align 4, !tbaa !20
  %146 = load float, float* %vb, align 4, !tbaa !20
  %add103 = fadd float %145, %146
  %147 = load float, float* %vc, align 4, !tbaa !20
  %add104 = fadd float %add103, %147
  %div105 = fdiv float 1.000000e+00, %add104
  store float %div105, float* %denom, align 4, !tbaa !20
  %148 = bitcast float* %v106 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #5
  %149 = load float, float* %vb, align 4, !tbaa !20
  %150 = load float, float* %denom, align 4, !tbaa !20
  %mul107 = fmul float %149, %150
  store float %mul107, float* %v106, align 4, !tbaa !20
  %151 = bitcast float* %w108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %151) #5
  %152 = load float, float* %vc, align 4, !tbaa !20
  %153 = load float, float* %denom, align 4, !tbaa !20
  %mul109 = fmul float %152, %153
  store float %mul109, float* %w108, align 4, !tbaa !20
  %154 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %154) #5
  %155 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %155) #5
  %156 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %157 = bitcast %class.btVector3* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %157) #5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp112, %class.btVector3* nonnull align 4 dereferenceable(16) %ab, float* nonnull align 4 dereferenceable(4) %v106)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %156, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp112)
  %158 = bitcast %class.btVector3* %ref.tmp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %158) #5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp113, %class.btVector3* nonnull align 4 dereferenceable(16) %ac, float* nonnull align 4 dereferenceable(4) %w108)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp110, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp113)
  %159 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex114 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %159, i32 0, i32 0
  %160 = bitcast %class.btVector3* %m_closestPointOnSimplex114 to i8*
  %161 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %160, i8* align 4 %161, i32 16, i1 false), !tbaa.struct !16
  %162 = bitcast %class.btVector3* %ref.tmp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %162) #5
  %163 = bitcast %class.btVector3* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %163) #5
  %164 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %164) #5
  %165 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %165) #5
  %166 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices115 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %166, i32 0, i32 1
  %167 = bitcast %struct.btUsageBitfield* %m_usedVertices115 to i8*
  %bf.load116 = load i8, i8* %167, align 4
  %bf.clear117 = and i8 %bf.load116, -2
  %bf.set118 = or i8 %bf.clear117, 1
  store i8 %bf.set118, i8* %167, align 4
  %168 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices119 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %168, i32 0, i32 1
  %169 = bitcast %struct.btUsageBitfield* %m_usedVertices119 to i8*
  %bf.load120 = load i8, i8* %169, align 4
  %bf.clear121 = and i8 %bf.load120, -3
  %bf.set122 = or i8 %bf.clear121, 2
  store i8 %bf.set122, i8* %169, align 4
  %170 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %m_usedVertices123 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %170, i32 0, i32 1
  %171 = bitcast %struct.btUsageBitfield* %m_usedVertices123 to i8*
  %bf.load124 = load i8, i8* %171, align 4
  %bf.clear125 = and i8 %bf.load124, -5
  %bf.set126 = or i8 %bf.clear125, 4
  store i8 %bf.set126, i8* %171, align 4
  %172 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4, !tbaa !2
  %173 = load float, float* %v106, align 4, !tbaa !20
  %sub127 = fsub float 1.000000e+00, %173
  %174 = load float, float* %w108, align 4, !tbaa !20
  %sub128 = fsub float %sub127, %174
  %175 = load float, float* %v106, align 4, !tbaa !20
  %176 = load float, float* %w108, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %172, float %sub128, float %175, float %176, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %177 = bitcast float* %w108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  %178 = bitcast float* %v106 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast float* %denom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  br label %cleanup

cleanup:                                          ; preds = %if.end102, %if.then83
  %180 = bitcast float* %va to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #5
  br label %cleanup129

cleanup129:                                       ; preds = %cleanup, %if.then57
  %181 = bitcast float* %vb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #5
  br label %cleanup130

cleanup130:                                       ; preds = %cleanup129, %if.then42
  %182 = bitcast float* %d6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #5
  %183 = bitcast float* %d5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #5
  %184 = bitcast %class.btVector3* %cp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %184) #5
  br label %cleanup133

cleanup133:                                       ; preds = %cleanup130, %if.then23
  %185 = bitcast float* %vc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #5
  br label %cleanup134

cleanup134:                                       ; preds = %cleanup133, %if.then10
  %186 = bitcast float* %d4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #5
  %187 = bitcast float* %d3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #5
  %188 = bitcast %class.btVector3* %bp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %188) #5
  br label %cleanup137

cleanup137:                                       ; preds = %cleanup134, %if.then
  %189 = bitcast float* %d2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #5
  %190 = bitcast float* %d1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #5
  %191 = bitcast %class.btVector3* %ap to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %191) #5
  %192 = bitcast %class.btVector3* %ac to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %192) #5
  %193 = bitcast %class.btVector3* %ab to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %193) #5
  %194 = load i1, i1* %retval, align 1
  ret i1 %194
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !20
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !20
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !20
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !20
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !20
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !20
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !20
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !20
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !20
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  ret void
}

define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %class.btVector3* nonnull align 4 dereferenceable(16) %d, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %finalResult) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %finalResult.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %tempResult = alloca %struct.btSubSimplexClosestResult, align 4
  %pointOutsideABC = alloca i32, align 4
  %pointOutsideACD = alloca i32, align 4
  %pointOutsideADB = alloca i32, align 4
  %pointOutsideBDC = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %bestSqDist = alloca float, align 4
  %q = alloca %class.btVector3, align 4
  %sqDist = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %q77 = alloca %class.btVector3, align 4
  %sqDist79 = alloca float, align 4
  %ref.tmp80 = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %q129 = alloca %class.btVector3, align 4
  %sqDist131 = alloca float, align 4
  %ref.tmp132 = alloca %class.btVector3, align 4
  %ref.tmp133 = alloca %class.btVector3, align 4
  %q181 = alloca %class.btVector3, align 4
  %sqDist183 = alloca float, align 4
  %ref.tmp184 = alloca %class.btVector3, align 4
  %ref.tmp185 = alloca %class.btVector3, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  store %struct.btSubSimplexClosestResult* %finalResult, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = bitcast %struct.btSubSimplexClosestResult* %tempResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %0) #5
  %call = call %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* %tempResult)
  %1 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %2 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %2, i32 0, i32 0
  %3 = bitcast %class.btVector3* %m_closestPointOnSimplex to i8*
  %4 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !16
  %5 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %5, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices)
  %6 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices2 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %6, i32 0, i32 1
  %7 = bitcast %struct.btUsageBitfield* %m_usedVertices2 to i8*
  %bf.load = load i8, i8* %7, align 4
  %bf.clear = and i8 %bf.load, -2
  %bf.set = or i8 %bf.clear, 1
  store i8 %bf.set, i8* %7, align 4
  %8 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices3 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %8, i32 0, i32 1
  %9 = bitcast %struct.btUsageBitfield* %m_usedVertices3 to i8*
  %bf.load4 = load i8, i8* %9, align 4
  %bf.clear5 = and i8 %bf.load4, -3
  %bf.set6 = or i8 %bf.clear5, 2
  store i8 %bf.set6, i8* %9, align 4
  %10 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices7 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %10, i32 0, i32 1
  %11 = bitcast %struct.btUsageBitfield* %m_usedVertices7 to i8*
  %bf.load8 = load i8, i8* %11, align 4
  %bf.clear9 = and i8 %bf.load8, -5
  %bf.set10 = or i8 %bf.clear9, 4
  store i8 %bf.set10, i8* %11, align 4
  %12 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices11 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %12, i32 0, i32 1
  %13 = bitcast %struct.btUsageBitfield* %m_usedVertices11 to i8*
  %bf.load12 = load i8, i8* %13, align 4
  %bf.clear13 = and i8 %bf.load12, -9
  %bf.set14 = or i8 %bf.clear13, 8
  store i8 %bf.set14, i8* %13, align 4
  %14 = bitcast i32* %pointOutsideABC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %16 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %17 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %18 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %19 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %call15 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19)
  store i32 %call15, i32* %pointOutsideABC, align 4, !tbaa !6
  %20 = bitcast i32* %pointOutsideACD to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %22 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %23 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %25 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call16 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25)
  store i32 %call16, i32* %pointOutsideACD, align 4, !tbaa !6
  %26 = bitcast i32* %pointOutsideADB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  %27 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %28 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %29 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %30 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %31 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call17 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %28, %class.btVector3* nonnull align 4 dereferenceable(16) %29, %class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31)
  store i32 %call17, i32* %pointOutsideADB, align 4, !tbaa !6
  %32 = bitcast i32* %pointOutsideBDC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %35 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %36 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %37 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call18 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34, %class.btVector3* nonnull align 4 dereferenceable(16) %35, %class.btVector3* nonnull align 4 dereferenceable(16) %36, %class.btVector3* nonnull align 4 dereferenceable(16) %37)
  store i32 %call18, i32* %pointOutsideBDC, align 4, !tbaa !6
  %38 = load i32, i32* %pointOutsideABC, align 4, !tbaa !6
  %cmp = icmp slt i32 %38, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %39 = load i32, i32* %pointOutsideACD, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %39, 0
  br i1 %cmp19, label %if.then, label %lor.lhs.false20

lor.lhs.false20:                                  ; preds = %lor.lhs.false
  %40 = load i32, i32* %pointOutsideADB, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %40, 0
  br i1 %cmp21, label %if.then, label %lor.lhs.false22

lor.lhs.false22:                                  ; preds = %lor.lhs.false20
  %41 = load i32, i32* %pointOutsideBDC, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %41, 0
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false22, %lor.lhs.false20, %lor.lhs.false, %entry
  %42 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_degenerate = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %42, i32 0, i32 4
  store i8 1, i8* %m_degenerate, align 4, !tbaa !21
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup259

if.end:                                           ; preds = %lor.lhs.false22
  %43 = load i32, i32* %pointOutsideABC, align 4, !tbaa !6
  %tobool = icmp ne i32 %43, 0
  br i1 %tobool, label %if.end30, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end
  %44 = load i32, i32* %pointOutsideACD, align 4, !tbaa !6
  %tobool24 = icmp ne i32 %44, 0
  br i1 %tobool24, label %if.end30, label %land.lhs.true25

land.lhs.true25:                                  ; preds = %land.lhs.true
  %45 = load i32, i32* %pointOutsideADB, align 4, !tbaa !6
  %tobool26 = icmp ne i32 %45, 0
  br i1 %tobool26, label %if.end30, label %land.lhs.true27

land.lhs.true27:                                  ; preds = %land.lhs.true25
  %46 = load i32, i32* %pointOutsideBDC, align 4, !tbaa !6
  %tobool28 = icmp ne i32 %46, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %land.lhs.true27
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup259

if.end30:                                         ; preds = %land.lhs.true27, %land.lhs.true25, %land.lhs.true, %if.end
  %47 = bitcast float* %bestSqDist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  store float 0x47EFFFFFE0000000, float* %bestSqDist, align 4, !tbaa !20
  %48 = load i32, i32* %pointOutsideABC, align 4, !tbaa !6
  %tobool31 = icmp ne i32 %48, 0
  br i1 %tobool31, label %if.then32, label %if.end73

if.then32:                                        ; preds = %if.end30
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %50 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %51 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %52 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call33 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %49, %class.btVector3* nonnull align 4 dereferenceable(16) %50, %class.btVector3* nonnull align 4 dereferenceable(16) %51, %class.btVector3* nonnull align 4 dereferenceable(16) %52, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %53 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #5
  %m_closestPointOnSimplex34 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %54 = bitcast %class.btVector3* %q to i8*
  %55 = bitcast %class.btVector3* %m_closestPointOnSimplex34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %55, i32 16, i1 false), !tbaa.struct !16
  %56 = bitcast float* %sqDist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #5
  %57 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %57) #5
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %58)
  %59 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %59) #5
  %60 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %60)
  %call36 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35)
  %61 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #5
  %62 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #5
  store float %call36, float* %sqDist, align 4, !tbaa !20
  %63 = load float, float* %sqDist, align 4, !tbaa !20
  %64 = load float, float* %bestSqDist, align 4, !tbaa !20
  %cmp37 = fcmp olt float %63, %64
  br i1 %cmp37, label %if.then38, label %if.end72

if.then38:                                        ; preds = %if.then32
  %65 = load float, float* %sqDist, align 4, !tbaa !20
  store float %65, float* %bestSqDist, align 4, !tbaa !20
  %66 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex39 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %66, i32 0, i32 0
  %67 = bitcast %class.btVector3* %m_closestPointOnSimplex39 to i8*
  %68 = bitcast %class.btVector3* %q to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 16, i1 false), !tbaa.struct !16
  %69 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices40 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %69, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices40)
  %m_usedVertices41 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %70 = bitcast %struct.btUsageBitfield* %m_usedVertices41 to i8*
  %bf.load42 = load i8, i8* %70, align 4
  %bf.clear43 = and i8 %bf.load42, 1
  %bf.cast = zext i8 %bf.clear43 to i16
  %71 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices44 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %71, i32 0, i32 1
  %72 = bitcast %struct.btUsageBitfield* %m_usedVertices44 to i8*
  %73 = trunc i16 %bf.cast to i8
  %bf.load45 = load i8, i8* %72, align 4
  %bf.value = and i8 %73, 1
  %bf.clear46 = and i8 %bf.load45, -2
  %bf.set47 = or i8 %bf.clear46, %bf.value
  store i8 %bf.set47, i8* %72, align 4
  %m_usedVertices48 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %74 = bitcast %struct.btUsageBitfield* %m_usedVertices48 to i8*
  %bf.load49 = load i8, i8* %74, align 4
  %bf.lshr = lshr i8 %bf.load49, 1
  %bf.clear50 = and i8 %bf.lshr, 1
  %bf.cast51 = zext i8 %bf.clear50 to i16
  %75 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices52 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %75, i32 0, i32 1
  %76 = bitcast %struct.btUsageBitfield* %m_usedVertices52 to i8*
  %77 = trunc i16 %bf.cast51 to i8
  %bf.load53 = load i8, i8* %76, align 4
  %bf.value54 = and i8 %77, 1
  %bf.shl = shl i8 %bf.value54, 1
  %bf.clear55 = and i8 %bf.load53, -3
  %bf.set56 = or i8 %bf.clear55, %bf.shl
  store i8 %bf.set56, i8* %76, align 4
  %m_usedVertices57 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %78 = bitcast %struct.btUsageBitfield* %m_usedVertices57 to i8*
  %bf.load58 = load i8, i8* %78, align 4
  %bf.lshr59 = lshr i8 %bf.load58, 2
  %bf.clear60 = and i8 %bf.lshr59, 1
  %bf.cast61 = zext i8 %bf.clear60 to i16
  %79 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices62 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %79, i32 0, i32 1
  %80 = bitcast %struct.btUsageBitfield* %m_usedVertices62 to i8*
  %81 = trunc i16 %bf.cast61 to i8
  %bf.load63 = load i8, i8* %80, align 4
  %bf.value64 = and i8 %81, 1
  %bf.shl65 = shl i8 %bf.value64, 2
  %bf.clear66 = and i8 %bf.load63, -5
  %bf.set67 = or i8 %bf.clear66, %bf.shl65
  store i8 %bf.set67, i8* %80, align 4
  %82 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  %83 = load float, float* %arrayidx, align 4, !tbaa !20
  %m_barycentricCoords68 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx69 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords68, i32 0, i32 1
  %84 = load float, float* %arrayidx69, align 4, !tbaa !20
  %m_barycentricCoords70 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx71 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords70, i32 0, i32 2
  %85 = load float, float* %arrayidx71, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %82, float %83, float %84, float %85, float 0.000000e+00)
  br label %if.end72

if.end72:                                         ; preds = %if.then38, %if.then32
  %86 = bitcast float* %sqDist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #5
  %87 = bitcast %class.btVector3* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #5
  br label %if.end73

if.end73:                                         ; preds = %if.end72, %if.end30
  %88 = load i32, i32* %pointOutsideACD, align 4, !tbaa !6
  %tobool74 = icmp ne i32 %88, 0
  br i1 %tobool74, label %if.then75, label %if.end125

if.then75:                                        ; preds = %if.end73
  %89 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %90 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %91 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %92 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %call76 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %89, %class.btVector3* nonnull align 4 dereferenceable(16) %90, %class.btVector3* nonnull align 4 dereferenceable(16) %91, %class.btVector3* nonnull align 4 dereferenceable(16) %92, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %93 = bitcast %class.btVector3* %q77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #5
  %m_closestPointOnSimplex78 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %94 = bitcast %class.btVector3* %q77 to i8*
  %95 = bitcast %class.btVector3* %m_closestPointOnSimplex78 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %94, i8* align 4 %95, i32 16, i1 false), !tbaa.struct !16
  %96 = bitcast float* %sqDist79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #5
  %97 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #5
  %98 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp80, %class.btVector3* nonnull align 4 dereferenceable(16) %q77, %class.btVector3* nonnull align 4 dereferenceable(16) %98)
  %99 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %99) #5
  %100 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %q77, %class.btVector3* nonnull align 4 dereferenceable(16) %100)
  %call82 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp80, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp81)
  %101 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %101) #5
  %102 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %102) #5
  store float %call82, float* %sqDist79, align 4, !tbaa !20
  %103 = load float, float* %sqDist79, align 4, !tbaa !20
  %104 = load float, float* %bestSqDist, align 4, !tbaa !20
  %cmp83 = fcmp olt float %103, %104
  br i1 %cmp83, label %if.then84, label %if.end124

if.then84:                                        ; preds = %if.then75
  %105 = load float, float* %sqDist79, align 4, !tbaa !20
  store float %105, float* %bestSqDist, align 4, !tbaa !20
  %106 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex85 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %106, i32 0, i32 0
  %107 = bitcast %class.btVector3* %m_closestPointOnSimplex85 to i8*
  %108 = bitcast %class.btVector3* %q77 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %107, i8* align 4 %108, i32 16, i1 false), !tbaa.struct !16
  %109 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices86 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %109, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices86)
  %m_usedVertices87 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %110 = bitcast %struct.btUsageBitfield* %m_usedVertices87 to i8*
  %bf.load88 = load i8, i8* %110, align 4
  %bf.clear89 = and i8 %bf.load88, 1
  %bf.cast90 = zext i8 %bf.clear89 to i16
  %111 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices91 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %111, i32 0, i32 1
  %112 = bitcast %struct.btUsageBitfield* %m_usedVertices91 to i8*
  %113 = trunc i16 %bf.cast90 to i8
  %bf.load92 = load i8, i8* %112, align 4
  %bf.value93 = and i8 %113, 1
  %bf.clear94 = and i8 %bf.load92, -2
  %bf.set95 = or i8 %bf.clear94, %bf.value93
  store i8 %bf.set95, i8* %112, align 4
  %m_usedVertices96 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %114 = bitcast %struct.btUsageBitfield* %m_usedVertices96 to i8*
  %bf.load97 = load i8, i8* %114, align 4
  %bf.lshr98 = lshr i8 %bf.load97, 1
  %bf.clear99 = and i8 %bf.lshr98, 1
  %bf.cast100 = zext i8 %bf.clear99 to i16
  %115 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices101 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %115, i32 0, i32 1
  %116 = bitcast %struct.btUsageBitfield* %m_usedVertices101 to i8*
  %117 = trunc i16 %bf.cast100 to i8
  %bf.load102 = load i8, i8* %116, align 4
  %bf.value103 = and i8 %117, 1
  %bf.shl104 = shl i8 %bf.value103, 2
  %bf.clear105 = and i8 %bf.load102, -5
  %bf.set106 = or i8 %bf.clear105, %bf.shl104
  store i8 %bf.set106, i8* %116, align 4
  %m_usedVertices107 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %118 = bitcast %struct.btUsageBitfield* %m_usedVertices107 to i8*
  %bf.load108 = load i8, i8* %118, align 4
  %bf.lshr109 = lshr i8 %bf.load108, 2
  %bf.clear110 = and i8 %bf.lshr109, 1
  %bf.cast111 = zext i8 %bf.clear110 to i16
  %119 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices112 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %119, i32 0, i32 1
  %120 = bitcast %struct.btUsageBitfield* %m_usedVertices112 to i8*
  %121 = trunc i16 %bf.cast111 to i8
  %bf.load113 = load i8, i8* %120, align 4
  %bf.value114 = and i8 %121, 1
  %bf.shl115 = shl i8 %bf.value114, 3
  %bf.clear116 = and i8 %bf.load113, -9
  %bf.set117 = or i8 %bf.clear116, %bf.shl115
  store i8 %bf.set117, i8* %120, align 4
  %122 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_barycentricCoords118 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords118, i32 0, i32 0
  %123 = load float, float* %arrayidx119, align 4, !tbaa !20
  %m_barycentricCoords120 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx121 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords120, i32 0, i32 1
  %124 = load float, float* %arrayidx121, align 4, !tbaa !20
  %m_barycentricCoords122 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx123 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords122, i32 0, i32 2
  %125 = load float, float* %arrayidx123, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %122, float %123, float 0.000000e+00, float %124, float %125)
  br label %if.end124

if.end124:                                        ; preds = %if.then84, %if.then75
  %126 = bitcast float* %sqDist79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #5
  %127 = bitcast %class.btVector3* %q77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %127) #5
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %if.end73
  %128 = load i32, i32* %pointOutsideADB, align 4, !tbaa !6
  %tobool126 = icmp ne i32 %128, 0
  br i1 %tobool126, label %if.then127, label %if.end177

if.then127:                                       ; preds = %if.end125
  %129 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %130 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %131 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %132 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call128 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %129, %class.btVector3* nonnull align 4 dereferenceable(16) %130, %class.btVector3* nonnull align 4 dereferenceable(16) %131, %class.btVector3* nonnull align 4 dereferenceable(16) %132, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %133 = bitcast %class.btVector3* %q129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %133) #5
  %m_closestPointOnSimplex130 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %134 = bitcast %class.btVector3* %q129 to i8*
  %135 = bitcast %class.btVector3* %m_closestPointOnSimplex130 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %134, i8* align 4 %135, i32 16, i1 false), !tbaa.struct !16
  %136 = bitcast float* %sqDist131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #5
  %137 = bitcast %class.btVector3* %ref.tmp132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %137) #5
  %138 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp132, %class.btVector3* nonnull align 4 dereferenceable(16) %q129, %class.btVector3* nonnull align 4 dereferenceable(16) %138)
  %139 = bitcast %class.btVector3* %ref.tmp133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %139) #5
  %140 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp133, %class.btVector3* nonnull align 4 dereferenceable(16) %q129, %class.btVector3* nonnull align 4 dereferenceable(16) %140)
  %call134 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp132, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp133)
  %141 = bitcast %class.btVector3* %ref.tmp133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #5
  %142 = bitcast %class.btVector3* %ref.tmp132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %142) #5
  store float %call134, float* %sqDist131, align 4, !tbaa !20
  %143 = load float, float* %sqDist131, align 4, !tbaa !20
  %144 = load float, float* %bestSqDist, align 4, !tbaa !20
  %cmp135 = fcmp olt float %143, %144
  br i1 %cmp135, label %if.then136, label %if.end176

if.then136:                                       ; preds = %if.then127
  %145 = load float, float* %sqDist131, align 4, !tbaa !20
  store float %145, float* %bestSqDist, align 4, !tbaa !20
  %146 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex137 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %146, i32 0, i32 0
  %147 = bitcast %class.btVector3* %m_closestPointOnSimplex137 to i8*
  %148 = bitcast %class.btVector3* %q129 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %147, i8* align 4 %148, i32 16, i1 false), !tbaa.struct !16
  %149 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices138 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %149, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices138)
  %m_usedVertices139 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %150 = bitcast %struct.btUsageBitfield* %m_usedVertices139 to i8*
  %bf.load140 = load i8, i8* %150, align 4
  %bf.clear141 = and i8 %bf.load140, 1
  %bf.cast142 = zext i8 %bf.clear141 to i16
  %151 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices143 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %151, i32 0, i32 1
  %152 = bitcast %struct.btUsageBitfield* %m_usedVertices143 to i8*
  %153 = trunc i16 %bf.cast142 to i8
  %bf.load144 = load i8, i8* %152, align 4
  %bf.value145 = and i8 %153, 1
  %bf.clear146 = and i8 %bf.load144, -2
  %bf.set147 = or i8 %bf.clear146, %bf.value145
  store i8 %bf.set147, i8* %152, align 4
  %m_usedVertices148 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %154 = bitcast %struct.btUsageBitfield* %m_usedVertices148 to i8*
  %bf.load149 = load i8, i8* %154, align 4
  %bf.lshr150 = lshr i8 %bf.load149, 2
  %bf.clear151 = and i8 %bf.lshr150, 1
  %bf.cast152 = zext i8 %bf.clear151 to i16
  %155 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices153 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %155, i32 0, i32 1
  %156 = bitcast %struct.btUsageBitfield* %m_usedVertices153 to i8*
  %157 = trunc i16 %bf.cast152 to i8
  %bf.load154 = load i8, i8* %156, align 4
  %bf.value155 = and i8 %157, 1
  %bf.shl156 = shl i8 %bf.value155, 1
  %bf.clear157 = and i8 %bf.load154, -3
  %bf.set158 = or i8 %bf.clear157, %bf.shl156
  store i8 %bf.set158, i8* %156, align 4
  %m_usedVertices159 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %158 = bitcast %struct.btUsageBitfield* %m_usedVertices159 to i8*
  %bf.load160 = load i8, i8* %158, align 4
  %bf.lshr161 = lshr i8 %bf.load160, 1
  %bf.clear162 = and i8 %bf.lshr161, 1
  %bf.cast163 = zext i8 %bf.clear162 to i16
  %159 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices164 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %159, i32 0, i32 1
  %160 = bitcast %struct.btUsageBitfield* %m_usedVertices164 to i8*
  %161 = trunc i16 %bf.cast163 to i8
  %bf.load165 = load i8, i8* %160, align 4
  %bf.value166 = and i8 %161, 1
  %bf.shl167 = shl i8 %bf.value166, 3
  %bf.clear168 = and i8 %bf.load165, -9
  %bf.set169 = or i8 %bf.clear168, %bf.shl167
  store i8 %bf.set169, i8* %160, align 4
  %162 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_barycentricCoords170 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx171 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords170, i32 0, i32 0
  %163 = load float, float* %arrayidx171, align 4, !tbaa !20
  %m_barycentricCoords172 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx173 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords172, i32 0, i32 2
  %164 = load float, float* %arrayidx173, align 4, !tbaa !20
  %m_barycentricCoords174 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx175 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords174, i32 0, i32 1
  %165 = load float, float* %arrayidx175, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %162, float %163, float %164, float 0.000000e+00, float %165)
  br label %if.end176

if.end176:                                        ; preds = %if.then136, %if.then127
  %166 = bitcast float* %sqDist131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #5
  %167 = bitcast %class.btVector3* %q129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %167) #5
  br label %if.end177

if.end177:                                        ; preds = %if.end176, %if.end125
  %168 = load i32, i32* %pointOutsideBDC, align 4, !tbaa !6
  %tobool178 = icmp ne i32 %168, 0
  br i1 %tobool178, label %if.then179, label %if.end230

if.then179:                                       ; preds = %if.end177
  %169 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %170 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %171 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %172 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call180 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %169, %class.btVector3* nonnull align 4 dereferenceable(16) %170, %class.btVector3* nonnull align 4 dereferenceable(16) %171, %class.btVector3* nonnull align 4 dereferenceable(16) %172, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %173 = bitcast %class.btVector3* %q181 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %173) #5
  %m_closestPointOnSimplex182 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %174 = bitcast %class.btVector3* %q181 to i8*
  %175 = bitcast %class.btVector3* %m_closestPointOnSimplex182 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %174, i8* align 4 %175, i32 16, i1 false), !tbaa.struct !16
  %176 = bitcast float* %sqDist183 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %176) #5
  %177 = bitcast %class.btVector3* %ref.tmp184 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %177) #5
  %178 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp184, %class.btVector3* nonnull align 4 dereferenceable(16) %q181, %class.btVector3* nonnull align 4 dereferenceable(16) %178)
  %179 = bitcast %class.btVector3* %ref.tmp185 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %179) #5
  %180 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp185, %class.btVector3* nonnull align 4 dereferenceable(16) %q181, %class.btVector3* nonnull align 4 dereferenceable(16) %180)
  %call186 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp184, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp185)
  %181 = bitcast %class.btVector3* %ref.tmp185 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %181) #5
  %182 = bitcast %class.btVector3* %ref.tmp184 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %182) #5
  store float %call186, float* %sqDist183, align 4, !tbaa !20
  %183 = load float, float* %sqDist183, align 4, !tbaa !20
  %184 = load float, float* %bestSqDist, align 4, !tbaa !20
  %cmp187 = fcmp olt float %183, %184
  br i1 %cmp187, label %if.then188, label %if.end229

if.then188:                                       ; preds = %if.then179
  %185 = load float, float* %sqDist183, align 4, !tbaa !20
  store float %185, float* %bestSqDist, align 4, !tbaa !20
  %186 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_closestPointOnSimplex189 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %186, i32 0, i32 0
  %187 = bitcast %class.btVector3* %m_closestPointOnSimplex189 to i8*
  %188 = bitcast %class.btVector3* %q181 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %187, i8* align 4 %188, i32 16, i1 false), !tbaa.struct !16
  %189 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices190 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %189, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices190)
  %m_usedVertices191 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %190 = bitcast %struct.btUsageBitfield* %m_usedVertices191 to i8*
  %bf.load192 = load i8, i8* %190, align 4
  %bf.clear193 = and i8 %bf.load192, 1
  %bf.cast194 = zext i8 %bf.clear193 to i16
  %191 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices195 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %191, i32 0, i32 1
  %192 = bitcast %struct.btUsageBitfield* %m_usedVertices195 to i8*
  %193 = trunc i16 %bf.cast194 to i8
  %bf.load196 = load i8, i8* %192, align 4
  %bf.value197 = and i8 %193, 1
  %bf.shl198 = shl i8 %bf.value197, 1
  %bf.clear199 = and i8 %bf.load196, -3
  %bf.set200 = or i8 %bf.clear199, %bf.shl198
  store i8 %bf.set200, i8* %192, align 4
  %m_usedVertices201 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %194 = bitcast %struct.btUsageBitfield* %m_usedVertices201 to i8*
  %bf.load202 = load i8, i8* %194, align 4
  %bf.lshr203 = lshr i8 %bf.load202, 2
  %bf.clear204 = and i8 %bf.lshr203, 1
  %bf.cast205 = zext i8 %bf.clear204 to i16
  %195 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices206 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %195, i32 0, i32 1
  %196 = bitcast %struct.btUsageBitfield* %m_usedVertices206 to i8*
  %197 = trunc i16 %bf.cast205 to i8
  %bf.load207 = load i8, i8* %196, align 4
  %bf.value208 = and i8 %197, 1
  %bf.shl209 = shl i8 %bf.value208, 2
  %bf.clear210 = and i8 %bf.load207, -5
  %bf.set211 = or i8 %bf.clear210, %bf.shl209
  store i8 %bf.set211, i8* %196, align 4
  %m_usedVertices212 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %198 = bitcast %struct.btUsageBitfield* %m_usedVertices212 to i8*
  %bf.load213 = load i8, i8* %198, align 4
  %bf.lshr214 = lshr i8 %bf.load213, 1
  %bf.clear215 = and i8 %bf.lshr214, 1
  %bf.cast216 = zext i8 %bf.clear215 to i16
  %199 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices217 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %199, i32 0, i32 1
  %200 = bitcast %struct.btUsageBitfield* %m_usedVertices217 to i8*
  %201 = trunc i16 %bf.cast216 to i8
  %bf.load218 = load i8, i8* %200, align 4
  %bf.value219 = and i8 %201, 1
  %bf.shl220 = shl i8 %bf.value219, 3
  %bf.clear221 = and i8 %bf.load218, -9
  %bf.set222 = or i8 %bf.clear221, %bf.shl220
  store i8 %bf.set222, i8* %200, align 4
  %202 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_barycentricCoords223 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx224 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords223, i32 0, i32 0
  %203 = load float, float* %arrayidx224, align 4, !tbaa !20
  %m_barycentricCoords225 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx226 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords225, i32 0, i32 2
  %204 = load float, float* %arrayidx226, align 4, !tbaa !20
  %m_barycentricCoords227 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx228 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords227, i32 0, i32 1
  %205 = load float, float* %arrayidx228, align 4, !tbaa !20
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %202, float 0.000000e+00, float %203, float %204, float %205)
  br label %if.end229

if.end229:                                        ; preds = %if.then188, %if.then179
  %206 = bitcast float* %sqDist183 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #5
  %207 = bitcast %class.btVector3* %q181 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %207) #5
  br label %if.end230

if.end230:                                        ; preds = %if.end229, %if.end177
  %208 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices231 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %208, i32 0, i32 1
  %209 = bitcast %struct.btUsageBitfield* %m_usedVertices231 to i8*
  %bf.load232 = load i8, i8* %209, align 4
  %bf.clear233 = and i8 %bf.load232, 1
  %bf.cast234 = zext i8 %bf.clear233 to i16
  %tobool235 = icmp ne i16 %bf.cast234, 0
  br i1 %tobool235, label %land.lhs.true236, label %if.end258

land.lhs.true236:                                 ; preds = %if.end230
  %210 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices237 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %210, i32 0, i32 1
  %211 = bitcast %struct.btUsageBitfield* %m_usedVertices237 to i8*
  %bf.load238 = load i8, i8* %211, align 4
  %bf.lshr239 = lshr i8 %bf.load238, 1
  %bf.clear240 = and i8 %bf.lshr239, 1
  %bf.cast241 = zext i8 %bf.clear240 to i16
  %tobool242 = icmp ne i16 %bf.cast241, 0
  br i1 %tobool242, label %land.lhs.true243, label %if.end258

land.lhs.true243:                                 ; preds = %land.lhs.true236
  %212 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices244 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %212, i32 0, i32 1
  %213 = bitcast %struct.btUsageBitfield* %m_usedVertices244 to i8*
  %bf.load245 = load i8, i8* %213, align 4
  %bf.lshr246 = lshr i8 %bf.load245, 2
  %bf.clear247 = and i8 %bf.lshr246, 1
  %bf.cast248 = zext i8 %bf.clear247 to i16
  %tobool249 = icmp ne i16 %bf.cast248, 0
  br i1 %tobool249, label %land.lhs.true250, label %if.end258

land.lhs.true250:                                 ; preds = %land.lhs.true243
  %214 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4, !tbaa !2
  %m_usedVertices251 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %214, i32 0, i32 1
  %215 = bitcast %struct.btUsageBitfield* %m_usedVertices251 to i8*
  %bf.load252 = load i8, i8* %215, align 4
  %bf.lshr253 = lshr i8 %bf.load252, 3
  %bf.clear254 = and i8 %bf.lshr253, 1
  %bf.cast255 = zext i8 %bf.clear254 to i16
  %tobool256 = icmp ne i16 %bf.cast255, 0
  br i1 %tobool256, label %if.then257, label %if.end258

if.then257:                                       ; preds = %land.lhs.true250
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end258:                                        ; preds = %land.lhs.true250, %land.lhs.true243, %land.lhs.true236, %if.end230
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end258, %if.then257
  %216 = bitcast float* %bestSqDist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #5
  br label %cleanup259

cleanup259:                                       ; preds = %cleanup, %if.then29, %if.then
  %217 = bitcast i32* %pointOutsideBDC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #5
  %218 = bitcast i32* %pointOutsideADB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #5
  %219 = bitcast i32* %pointOutsideACD to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #5
  %220 = bitcast i32* %pointOutsideABC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #5
  %221 = bitcast %struct.btSubSimplexClosestResult* %tempResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %221) #5
  %222 = load i1, i1* %retval, align 1
  ret i1 %222
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !20
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !20
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !20
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !20
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !20
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !20
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !20
  ret void
}

define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %succes = alloca i8, align 1
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %succes) #5
  %call = call zeroext i1 @_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv(%class.btVoronoiSimplexSolver* %this1)
  %frombool = zext i1 %call to i8
  store i8 %frombool, i8* %succes, align 1, !tbaa !23
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_cachedV to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !16
  %3 = load i8, i8* %succes, align 1, !tbaa !23, !range !22
  %tobool = trunc i8 %3 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %succes) #5
  ret i1 %tobool
}

define hidden float @_ZN22btVoronoiSimplexSolver9maxVertexEv(%class.btVoronoiSimplexSolver* %this) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %i = alloca i32, align 4
  %numverts = alloca i32, align 4
  %maxV = alloca float, align 4
  %curLen2 = alloca float, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  store i32 %call, i32* %numverts, align 4, !tbaa !6
  %2 = bitcast float* %maxV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store float 0.000000e+00, float* %maxV, align 4, !tbaa !20
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %numverts, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast float* %curLen2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %6
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %arrayidx)
  store float %call2, float* %curLen2, align 4, !tbaa !20
  %7 = load float, float* %maxV, align 4, !tbaa !20
  %8 = load float, float* %curLen2, align 4, !tbaa !20
  %cmp3 = fcmp olt float %7, %8
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %9 = load float, float* %curLen2, align 4, !tbaa !20
  store float %9, float* %maxV, align 4, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %10 = bitcast float* %curLen2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load float, float* %maxV, align 4, !tbaa !20
  %13 = bitcast float* %maxV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  ret float %12
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

define hidden i32 @_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* %pBuf, %class.btVector3* %qBuf, %class.btVector3* %yBuf) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %pBuf.addr = alloca %class.btVector3*, align 4
  %qBuf.addr = alloca %class.btVector3*, align 4
  %yBuf.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pBuf, %class.btVector3** %pBuf.addr, align 4, !tbaa !2
  store %class.btVector3* %qBuf, %class.btVector3** %qBuf.addr, align 4, !tbaa !2
  store %class.btVector3* %yBuf, %class.btVector3** %yBuf.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %2
  %3 = load %class.btVector3*, %class.btVector3** %yBuf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx2 to i8*
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !16
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 %7
  %8 = load %class.btVector3*, %class.btVector3** %pBuf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %arrayidx4 to i8*
  %11 = bitcast %class.btVector3* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !16
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 %12
  %13 = load %class.btVector3*, %class.btVector3** %qBuf.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 %14
  %15 = bitcast %class.btVector3* %arrayidx6 to i8*
  %16 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  ret i32 %call7
}

define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %found = alloca i8, align 1
  %i = alloca i32, align 4
  %numverts = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %found) #5
  store i8 0, i8* %found, align 1, !tbaa !23
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  store i32 %call, i32* %numverts, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %numverts, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %4
  %5 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK9btVector39distance2ERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %m_equalVertexThreshold = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 8
  %6 = load float, float* %m_equalVertexThreshold, align 4, !tbaa !25
  %cmp3 = fcmp ole float %call2, %6
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i8 1, i8* %found, align 1, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %call4 = call zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lastW)
  br i1 %call4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %for.end
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %for.end
  %9 = load i8, i8* %found, align 1, !tbaa !23, !range !22
  %tobool = trunc i8 %9 to i1
  store i1 %tobool, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end6, %if.then5
  %10 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %found) #5
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector39distance2ERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #5
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #5
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %0 = load float, float* %arrayidx, align 4, !tbaa !20
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 3
  %2 = load float, float* %arrayidx3, align 4, !tbaa !20
  %cmp = fcmp oeq float %0, %2
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4, !tbaa !20
  %4 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4, !tbaa !20
  %cmp8 = fcmp oeq float %3, %5
  br i1 %cmp8, label %land.lhs.true9, label %land.end

land.lhs.true9:                                   ; preds = %land.lhs.true
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %6 = load float, float* %arrayidx11, align 4, !tbaa !20
  %7 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 1
  %8 = load float, float* %arrayidx13, align 4, !tbaa !20
  %cmp14 = fcmp oeq float %6, %8
  br i1 %cmp14, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true9
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4, !tbaa !20
  %10 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 0
  %11 = load float, float* %arrayidx18, align 4, !tbaa !20
  %cmp19 = fcmp oeq float %9, %11
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true9, %land.lhs.true, %entry
  %12 = phi i1 [ false, %land.lhs.true9 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp19, %land.rhs ]
  ret i1 %12
}

; Function Attrs: nounwind
define hidden void @_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #0 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_cachedV to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !16
  ret void
}

define hidden zeroext i1 @_ZNK22btVoronoiSimplexSolver12emptySimplexEv(%class.btVoronoiSimplexSolver* %this) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp = icmp eq i32 %call, 0
  ret i1 %cmp
}

define hidden void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) #2 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4, !tbaa !2
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call zeroext i1 @_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv(%class.btVoronoiSimplexSolver* %this1)
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_cachedP1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !16
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %3 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_cachedP2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !16
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  %0 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load = load i8, i8* %0, align 2
  %bf.clear = and i8 %bf.load, -2
  store i8 %bf.clear, i8* %0, align 2
  %1 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load2 = load i8, i8* %1, align 2
  %bf.clear3 = and i8 %bf.load2, -3
  store i8 %bf.clear3, i8* %1, align 2
  %2 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load4 = load i8, i8* %2, align 2
  %bf.clear5 = and i8 %bf.load4, -5
  store i8 %bf.clear5, i8* %2, align 2
  %3 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load6 = load i8, i8* %3, align 2
  %bf.clear7 = and i8 %bf.load6, -9
  store i8 %bf.clear7, i8* %3, align 2
  ret void
}

define hidden i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %signp = alloca float, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %signd = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4, !tbaa !2
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #5
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #5
  %2 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #5
  %5 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %normal, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #5
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #5
  %9 = bitcast float* %signp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #5
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4, !tbaa !2
  %12 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %13 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #5
  store float %call, float* %signp, align 4, !tbaa !20
  %14 = bitcast float* %signd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #5
  %16 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %17 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %18 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #5
  store float %call5, float* %signd, align 4, !tbaa !20
  %19 = load float, float* %signd, align 4, !tbaa !20
  %20 = load float, float* %signd, align 4, !tbaa !20
  %mul = fmul float %19, %20
  %cmp = fcmp olt float %mul, 0x3E45798EC0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %21 = load float, float* %signp, align 4, !tbaa !20
  %22 = load float, float* %signd, align 4, !tbaa !20
  %mul6 = fmul float %21, %22
  %cmp7 = fcmp olt float %mul6, 0.000000e+00
  %conv = zext i1 %cmp7 to i32
  store i32 %conv, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %23 = bitcast float* %signd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  %24 = bitcast float* %signp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %25 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #5
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !20
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !20
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !20
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !20
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !20
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !20
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !20
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !20
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !20
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !20
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !20
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !20
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !20
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !20
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !20
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_closestPointOnSimplex)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  %call2 = call %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* %m_usedVertices)
  ret %struct.btSubSimplexClosestResult* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this1)
  ret %struct.btUsageBitfield* %this1
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 0}
!9 = !{!"_ZTS22btVoronoiSimplexSolver", !7, i64 0, !4, i64 4, !4, i64 84, !4, i64 164, !10, i64 244, !10, i64 260, !10, i64 276, !10, i64 292, !11, i64 308, !12, i64 312, !13, i64 316, !12, i64 356}
!10 = !{!"_ZTS9btVector3", !4, i64 0}
!11 = !{!"float", !4, i64 0}
!12 = !{!"bool", !4, i64 0}
!13 = !{!"_ZTS25btSubSimplexClosestResult", !10, i64 0, !14, i64 16, !4, i64 20, !12, i64 36}
!14 = !{!"_ZTS15btUsageBitfield", !15, i64 0, !15, i64 0, !15, i64 0, !15, i64 0, !15, i64 0, !15, i64 0, !15, i64 0, !15, i64 0}
!15 = !{!"short", !4, i64 0}
!16 = !{i64 0, i64 16, !17}
!17 = !{!4, !4, i64 0}
!18 = !{!9, !12, i64 312}
!19 = !{!9, !12, i64 356}
!20 = !{!11, !11, i64 0}
!21 = !{!13, !12, i64 36}
!22 = !{i8 0, i8 2}
!23 = !{!12, !12, i64 0}
!24 = !{!9, !12, i64 352}
!25 = !{!9, !11, i64 308}
