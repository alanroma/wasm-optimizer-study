; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, float, %class.btAlignedObjectArray.4 }
%class.btMultiBody = type <{ %class.btMultiBodyLinkCollider*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i32, float, float, i8, [3 x i8], float, i8, [3 x i8] }>
%class.btMultiBodyLinkCollider = type opaque
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btMultibodyLink = type { float, float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i8, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btMultiBodyLinkCollider*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, %class.btVector3, %class.btVector3, i32, i32, %class.btVector3, %class.btVector3, i32, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32 }
%union.anon = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16*, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.20, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon.19, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon.19 = type { i8* }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%class.btTypedConstraint = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZNK11btMultiBody14getCompanionIdEv = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN11btMultiBody14setCompanionIdEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN9btVector3C2Ev = comdat any

$_Z6btFabsf = comdat any

$_ZNK11btMultiBody17getVelocityVectorEv = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody17getWorldTransformEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZNK11btRigidBody16getAngularFactorEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

@_ZTV21btMultiBodyConstraint = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btMultiBodyConstraint to i8*), i8* bitcast (%class.btMultiBodyConstraint* (%class.btMultiBodyConstraint*)* @_ZN21btMultiBodyConstraintD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyConstraint*)* @_ZN21btMultiBodyConstraintD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btMultiBodyConstraint = hidden constant [24 x i8] c"21btMultiBodyConstraint\00", align 1
@_ZTI21btMultiBodyConstraint = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btMultiBodyConstraint, i32 0, i32 0) }, align 4

@_ZN21btMultiBodyConstraintD1Ev = hidden unnamed_addr alias %class.btMultiBodyConstraint* (%class.btMultiBodyConstraint*), %class.btMultiBodyConstraint* (%class.btMultiBodyConstraint*)* @_ZN21btMultiBodyConstraintD2Ev

define hidden %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* returned %this, %class.btMultiBody* %bodyA, %class.btMultiBody* %bodyB, i32 %linkA, i32 %linkB, i32 %numRows, i1 zeroext %isUnilateral) unnamed_addr #0 {
entry:
  %retval = alloca %class.btMultiBodyConstraint*, align 4
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %bodyA.addr = alloca %class.btMultiBody*, align 4
  %bodyB.addr = alloca %class.btMultiBody*, align 4
  %linkA.addr = alloca i32, align 4
  %linkB.addr = alloca i32, align 4
  %numRows.addr = alloca i32, align 4
  %isUnilateral.addr = alloca i8, align 1
  %ref.tmp = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody* %bodyA, %class.btMultiBody** %bodyA.addr, align 4, !tbaa !2
  store %class.btMultiBody* %bodyB, %class.btMultiBody** %bodyB.addr, align 4, !tbaa !2
  store i32 %linkA, i32* %linkA.addr, align 4, !tbaa !6
  store i32 %linkB, i32* %linkB.addr, align 4, !tbaa !6
  store i32 %numRows, i32* %numRows.addr, align 4, !tbaa !6
  %frombool = zext i1 %isUnilateral to i8
  store i8 %frombool, i8* %isUnilateral.addr, align 1, !tbaa !8
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  store %class.btMultiBodyConstraint* %this1, %class.btMultiBodyConstraint** %retval, align 4
  %0 = bitcast %class.btMultiBodyConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV21btMultiBodyConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !10
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 1
  %1 = load %class.btMultiBody*, %class.btMultiBody** %bodyA.addr, align 4, !tbaa !2
  store %class.btMultiBody* %1, %class.btMultiBody** %m_bodyA, align 4, !tbaa !12
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %bodyB.addr, align 4, !tbaa !2
  store %class.btMultiBody* %2, %class.btMultiBody** %m_bodyB, align 4, !tbaa !17
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 3
  %3 = load i32, i32* %linkA.addr, align 4, !tbaa !6
  store i32 %3, i32* %m_linkA, align 4, !tbaa !18
  %m_linkB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 4
  %4 = load i32, i32* %linkB.addr, align 4, !tbaa !6
  store i32 %4, i32* %m_linkB, align 4, !tbaa !19
  %m_num_rows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %5 = load i32, i32* %numRows.addr, align 4, !tbaa !6
  store i32 %5, i32* %m_num_rows, align 4, !tbaa !20
  %m_isUnilateral = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 9
  %6 = load i8, i8* %isUnilateral.addr, align 1, !tbaa !8, !range !21
  %tobool = trunc i8 %6 to i1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_isUnilateral, align 4, !tbaa !22
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 10
  store float 1.000000e+02, float* %m_maxAppliedImpulse, align 4, !tbaa !23
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* %m_data)
  %7 = load %class.btMultiBody*, %class.btMultiBody** %bodyA.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %7)
  %add = add nsw i32 6, %call3
  %m_jac_size_A = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  store i32 %add, i32* %m_jac_size_A, align 4, !tbaa !24
  %m_jac_size_A4 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  %8 = load i32, i32* %m_jac_size_A4, align 4, !tbaa !24
  %9 = load %class.btMultiBody*, %class.btMultiBody** %bodyB.addr, align 4, !tbaa !2
  %tobool5 = icmp ne %class.btMultiBody* %9, null
  br i1 %tobool5, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load %class.btMultiBody*, %class.btMultiBody** %bodyB.addr, align 4, !tbaa !2
  %call6 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %10)
  %add7 = add nsw i32 6, %call6
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add7, %cond.true ], [ 0, %cond.false ]
  %add8 = add nsw i32 %8, %cond
  %m_jac_size_both = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  store i32 %add8, i32* %m_jac_size_both, align 4, !tbaa !25
  %m_jac_size_both9 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %11 = load i32, i32* %m_jac_size_both9, align 4, !tbaa !25
  %add10 = add nsw i32 1, %11
  %m_num_rows11 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %12 = load i32, i32* %m_num_rows11, align 4, !tbaa !20
  %mul = mul nsw i32 %add10, %12
  %m_pos_offset = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 8
  store i32 %mul, i32* %m_pos_offset, align 4, !tbaa !26
  %m_data12 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %m_jac_size_both13 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %13 = load i32, i32* %m_jac_size_both13, align 4, !tbaa !25
  %add14 = add nsw i32 2, %13
  %m_num_rows15 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %14 = load i32, i32* %m_num_rows15, align 4, !tbaa !20
  %mul16 = mul nsw i32 %add14, %14
  %15 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_data12, i32 %mul16, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %16 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %retval, align 4
  ret %class.btMultiBodyConstraint* %17
}

define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %links)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store float* %fillData, float** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !28
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %18 = load float*, float** %m_data11, align 4, !tbaa !28
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 %19
  %20 = bitcast float* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float*
  %22 = load float*, float** %fillData.addr, align 4, !tbaa !2
  %23 = load float, float* %22, align 4, !tbaa !27
  store float %23, float* %21, align 4, !tbaa !27
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !29
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV21btMultiBodyConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !10
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* %m_data) #8
  ret %class.btMultiBodyConstraint* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN21btMultiBodyConstraintD0Ev(%class.btMultiBodyConstraint* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #4

define hidden float @_ZN21btMultiBodyConstraint35fillConstraintRowMultiBodyMultiBodyER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK19btContactSolverInfofff(%class.btMultiBodyConstraint* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %constraintRow, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, float* %jacOrgA, float* %jacOrgB, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, float %desiredVelocity, float %lowerLimit, float %upperLimit) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %constraintRow.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %jacOrgA.addr = alloca float*, align 4
  %jacOrgB.addr = alloca float*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %desiredVelocity.addr = alloca float, align 4
  %lowerLimit.addr = alloca float, align 4
  %upperLimit.addr = alloca float, align 4
  %multiBodyA = alloca %class.btMultiBody*, align 4
  %multiBodyB = alloca %class.btMultiBody*, align 4
  %ndofA = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %i = alloca i32, align 4
  %delta = alloca float*, align 4
  %ndofB = alloca i32, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %i61 = alloca i32, align 4
  %ref.tmp78 = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %jacB = alloca float*, align 4
  %jacA = alloca float*, align 4
  %lambdaA = alloca float*, align 4
  %lambdaB = alloca float*, align 4
  %ndofA89 = alloca i32, align 4
  %i100 = alloca i32, align 4
  %j = alloca float, align 4
  %l = alloca float, align 4
  %ndofB114 = alloca i32, align 4
  %i123 = alloca i32, align 4
  %j128 = alloca float, align 4
  %l130 = alloca float, align 4
  %i141 = alloca i32, align 4
  %d = alloca float, align 4
  %rel_vel = alloca float, align 4
  %ndofA165 = alloca i32, align 4
  %ndofB166 = alloca i32, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %jacA173 = alloca float*, align 4
  %i177 = alloca i32, align 4
  %jacB195 = alloca float*, align 4
  %i199 = alloca i32, align 4
  %velocityError = alloca float, align 4
  %erp = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %constraintRow, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  store float* %jacOrgA, float** %jacOrgA.addr, align 4, !tbaa !2
  store float* %jacOrgB, float** %jacOrgB.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4, !tbaa !27
  store float %lowerLimit, float* %lowerLimit.addr, align 4, !tbaa !27
  store float %upperLimit, float* %upperLimit.addr, align 4, !tbaa !27
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 1
  %0 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4, !tbaa !12
  %1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %1, i32 0, i32 23
  store %class.btMultiBody* %0, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !30
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4, !tbaa !17
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 0, i32 26
  store %class.btMultiBody* %2, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !33
  %4 = bitcast %class.btMultiBody** %multiBodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_multiBodyA2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %5, i32 0, i32 23
  %6 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA2, align 4, !tbaa !30
  store %class.btMultiBody* %6, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %7 = bitcast %class.btMultiBody** %multiBodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_multiBodyB3 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 0, i32 26
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB3, align 4, !tbaa !33
  store %class.btMultiBody* %9, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %10 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBody* %10, null
  br i1 %tobool, label %if.then, label %if.end35

if.then:                                          ; preds = %entry
  %11 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %12)
  %add = add nsw i32 %call, 6
  store i32 %add, i32* %ndofA, align 4, !tbaa !6
  %13 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call4 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %13)
  %14 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %14, i32 0, i32 0
  store i32 %call4, i32* %m_deltaVelAindex, align 4, !tbaa !34
  %15 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelAindex5 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %15, i32 0, i32 0
  %16 = load i32, i32* %m_deltaVelAindex5, align 4, !tbaa !34
  %cmp = icmp slt i32 %16, 0
  br i1 %cmp, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  %17 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %17, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities)
  %18 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelAindex8 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %18, i32 0, i32 0
  store i32 %call7, i32* %m_deltaVelAindex8, align 4, !tbaa !34
  %19 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %20 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelAindex9 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %20, i32 0, i32 0
  %21 = load i32, i32* %m_deltaVelAindex9, align 4, !tbaa !34
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %19, i32 %21)
  %22 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities10 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %22, i32 0, i32 2
  %23 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities11 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %23, i32 0, i32 2
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities11)
  %24 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add13 = add nsw i32 %call12, %24
  %25 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocities10, i32 %add13, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %26 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  br label %if.end

if.else:                                          ; preds = %if.then
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  %27 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %27, i32 0, i32 0
  %call14 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians)
  %28 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %28, i32 0, i32 3
  store i32 %call14, i32* %m_jacAindex, align 4, !tbaa !35
  %29 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians15 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %29, i32 0, i32 0
  %30 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians16 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %30, i32 0, i32 0
  %call17 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians16)
  %31 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add18 = add nsw i32 %call17, %31
  %32 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_jacobians15, i32 %add18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %33 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %34, i32 0, i32 1
  %35 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse20 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %35, i32 0, i32 1
  %call21 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse20)
  %36 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add22 = add nsw i32 %call21, %36
  %37 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse, i32 %add22, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %38 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %ndofA, align 4, !tbaa !6
  %cmp24 = icmp slt i32 %40, %41
  br i1 %cmp24, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %42 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %43 = load float*, float** %jacOrgA.addr, align 4, !tbaa !2
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %43, i32 %44
  %45 = load float, float* %arrayidx, align 4, !tbaa !27
  %46 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians25 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %46, i32 0, i32 0
  %47 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacAindex26 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %47, i32 0, i32 3
  %48 = load i32, i32* %m_jacAindex26, align 4, !tbaa !35
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %add27 = add nsw i32 %48, %49
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians25, i32 %add27)
  store float %45, float* %call28, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %50, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %51 = bitcast float** %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #8
  %52 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse29 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %52, i32 0, i32 1
  %53 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacAindex30 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %53, i32 0, i32 3
  %54 = load i32, i32* %m_jacAindex30, align 4, !tbaa !35
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse29, i32 %54)
  store float* %call31, float** %delta, align 4, !tbaa !2
  %55 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %56 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians32 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %56, i32 0, i32 0
  %57 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacAindex33 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %57, i32 0, i32 3
  %58 = load i32, i32* %m_jacAindex33, align 4, !tbaa !35
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians32, i32 %58)
  %59 = load float*, float** %delta, align 4, !tbaa !2
  %60 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %60, i32 0, i32 3
  %61 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %61, i32 0, i32 4
  call void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %55, float* %call34, float* %59, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v)
  %62 = bitcast float** %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #8
  %63 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #8
  br label %if.end35

if.end35:                                         ; preds = %for.end, %entry
  %64 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool36 = icmp ne %class.btMultiBody* %64, null
  br i1 %tobool36, label %if.then37, label %if.end87

if.then37:                                        ; preds = %if.end35
  %65 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #8
  %66 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call38 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %66)
  %add39 = add nsw i32 %call38, 6
  store i32 %add39, i32* %ndofB, align 4, !tbaa !6
  %67 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call40 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %67)
  %68 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %68, i32 0, i32 4
  store i32 %call40, i32* %m_deltaVelBindex, align 4, !tbaa !36
  %69 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelBindex41 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %69, i32 0, i32 4
  %70 = load i32, i32* %m_deltaVelBindex41, align 4, !tbaa !36
  %cmp42 = icmp slt i32 %70, 0
  br i1 %cmp42, label %if.then43, label %if.end53

if.then43:                                        ; preds = %if.then37
  %71 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities44 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %71, i32 0, i32 2
  %call45 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities44)
  %72 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelBindex46 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %72, i32 0, i32 4
  store i32 %call45, i32* %m_deltaVelBindex46, align 4, !tbaa !36
  %73 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %74 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_deltaVelBindex47 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %74, i32 0, i32 4
  %75 = load i32, i32* %m_deltaVelBindex47, align 4, !tbaa !36
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %73, i32 %75)
  %76 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities48 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %76, i32 0, i32 2
  %77 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities49 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %77, i32 0, i32 2
  %call50 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities49)
  %78 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add51 = add nsw i32 %call50, %78
  %79 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  store float 0.000000e+00, float* %ref.tmp52, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocities48, i32 %add51, float* nonnull align 4 dereferenceable(4) %ref.tmp52)
  %80 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  br label %if.end53

if.end53:                                         ; preds = %if.then43, %if.then37
  %81 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians54 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %81, i32 0, i32 0
  %call55 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians54)
  %82 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %82, i32 0, i32 7
  store i32 %call55, i32* %m_jacBindex, align 4, !tbaa !37
  %83 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians56 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %83, i32 0, i32 0
  %84 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians57 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %84, i32 0, i32 0
  %call58 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians57)
  %85 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add59 = add nsw i32 %call58, %85
  %86 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #8
  store float 0.000000e+00, float* %ref.tmp60, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_jacobians56, i32 %add59, float* nonnull align 4 dereferenceable(4) %ref.tmp60)
  %87 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast i32* %i61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #8
  store i32 0, i32* %i61, align 4, !tbaa !6
  br label %for.cond62

for.cond62:                                       ; preds = %for.inc71, %if.end53
  %89 = load i32, i32* %i61, align 4, !tbaa !6
  %90 = load i32, i32* %ndofB, align 4, !tbaa !6
  %cmp63 = icmp slt i32 %89, %90
  br i1 %cmp63, label %for.body65, label %for.cond.cleanup64

for.cond.cleanup64:                               ; preds = %for.cond62
  %91 = bitcast i32* %i61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  br label %for.end73

for.body65:                                       ; preds = %for.cond62
  %92 = load float*, float** %jacOrgB.addr, align 4, !tbaa !2
  %93 = load i32, i32* %i61, align 4, !tbaa !6
  %arrayidx66 = getelementptr inbounds float, float* %92, i32 %93
  %94 = load float, float* %arrayidx66, align 4, !tbaa !27
  %95 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians67 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %95, i32 0, i32 0
  %96 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacBindex68 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %96, i32 0, i32 7
  %97 = load i32, i32* %m_jacBindex68, align 4, !tbaa !37
  %98 = load i32, i32* %i61, align 4, !tbaa !6
  %add69 = add nsw i32 %97, %98
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians67, i32 %add69)
  store float %94, float* %call70, align 4, !tbaa !27
  br label %for.inc71

for.inc71:                                        ; preds = %for.body65
  %99 = load i32, i32* %i61, align 4, !tbaa !6
  %inc72 = add nsw i32 %99, 1
  store i32 %inc72, i32* %i61, align 4, !tbaa !6
  br label %for.cond62

for.end73:                                        ; preds = %for.cond.cleanup64
  %100 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse74 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %100, i32 0, i32 1
  %101 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse75 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %101, i32 0, i32 1
  %call76 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse75)
  %102 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add77 = add nsw i32 %call76, %102
  %103 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #8
  store float 0.000000e+00, float* %ref.tmp78, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse74, i32 %add77, float* nonnull align 4 dereferenceable(4) %ref.tmp78)
  %104 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #8
  %105 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %106 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians79 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %106, i32 0, i32 0
  %107 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacBindex80 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %107, i32 0, i32 7
  %108 = load i32, i32* %m_jacBindex80, align 4, !tbaa !37
  %call81 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians79, i32 %108)
  %109 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse82 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %109, i32 0, i32 1
  %110 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacBindex83 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %110, i32 0, i32 7
  %111 = load i32, i32* %m_jacBindex83, align 4, !tbaa !37
  %call84 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse82, i32 %111)
  %112 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_r85 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %112, i32 0, i32 3
  %113 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_v86 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %113, i32 0, i32 4
  call void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %105, float* %call81, float* %call84, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r85, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v86)
  %114 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #8
  br label %if.end87

if.end87:                                         ; preds = %for.end73, %if.end35
  %115 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %115) #8
  %call88 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  %116 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #8
  store float 0.000000e+00, float* %denom0, align 4, !tbaa !27
  %117 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #8
  store float 0.000000e+00, float* %denom1, align 4, !tbaa !27
  %118 = bitcast float** %jacB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #8
  store float* null, float** %jacB, align 4, !tbaa !2
  %119 = bitcast float** %jacA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #8
  store float* null, float** %jacA, align 4, !tbaa !2
  %120 = bitcast float** %lambdaA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #8
  store float* null, float** %lambdaA, align 4, !tbaa !2
  %121 = bitcast float** %lambdaB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #8
  store float* null, float** %lambdaB, align 4, !tbaa !2
  %122 = bitcast i32* %ndofA89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #8
  store i32 0, i32* %ndofA89, align 4, !tbaa !6
  %123 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool90 = icmp ne %class.btMultiBody* %123, null
  br i1 %tobool90, label %if.then91, label %if.end111

if.then91:                                        ; preds = %if.end87
  %124 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call92 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %124)
  %add93 = add nsw i32 %call92, 6
  store i32 %add93, i32* %ndofA89, align 4, !tbaa !6
  %125 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians94 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %125, i32 0, i32 0
  %126 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacAindex95 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %126, i32 0, i32 3
  %127 = load i32, i32* %m_jacAindex95, align 4, !tbaa !35
  %call96 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians94, i32 %127)
  store float* %call96, float** %jacA, align 4, !tbaa !2
  %128 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse97 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %128, i32 0, i32 1
  %129 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacAindex98 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %129, i32 0, i32 3
  %130 = load i32, i32* %m_jacAindex98, align 4, !tbaa !35
  %call99 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse97, i32 %130)
  store float* %call99, float** %lambdaA, align 4, !tbaa !2
  %131 = bitcast i32* %i100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #8
  store i32 0, i32* %i100, align 4, !tbaa !6
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc108, %if.then91
  %132 = load i32, i32* %i100, align 4, !tbaa !6
  %133 = load i32, i32* %ndofA89, align 4, !tbaa !6
  %cmp102 = icmp slt i32 %132, %133
  br i1 %cmp102, label %for.body104, label %for.cond.cleanup103

for.cond.cleanup103:                              ; preds = %for.cond101
  %134 = bitcast i32* %i100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #8
  br label %for.end110

for.body104:                                      ; preds = %for.cond101
  %135 = bitcast float* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #8
  %136 = load float*, float** %jacA, align 4, !tbaa !2
  %137 = load i32, i32* %i100, align 4, !tbaa !6
  %arrayidx105 = getelementptr inbounds float, float* %136, i32 %137
  %138 = load float, float* %arrayidx105, align 4, !tbaa !27
  store float %138, float* %j, align 4, !tbaa !27
  %139 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #8
  %140 = load float*, float** %lambdaA, align 4, !tbaa !2
  %141 = load i32, i32* %i100, align 4, !tbaa !6
  %arrayidx106 = getelementptr inbounds float, float* %140, i32 %141
  %142 = load float, float* %arrayidx106, align 4, !tbaa !27
  store float %142, float* %l, align 4, !tbaa !27
  %143 = load float, float* %j, align 4, !tbaa !27
  %144 = load float, float* %l, align 4, !tbaa !27
  %mul = fmul float %143, %144
  %145 = load float, float* %denom0, align 4, !tbaa !27
  %add107 = fadd float %145, %mul
  store float %add107, float* %denom0, align 4, !tbaa !27
  %146 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #8
  %147 = bitcast float* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #8
  br label %for.inc108

for.inc108:                                       ; preds = %for.body104
  %148 = load i32, i32* %i100, align 4, !tbaa !6
  %inc109 = add nsw i32 %148, 1
  store i32 %inc109, i32* %i100, align 4, !tbaa !6
  br label %for.cond101

for.end110:                                       ; preds = %for.cond.cleanup103
  br label %if.end111

if.end111:                                        ; preds = %for.end110, %if.end87
  %149 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool112 = icmp ne %class.btMultiBody* %149, null
  br i1 %tobool112, label %if.then113, label %if.end137

if.then113:                                       ; preds = %if.end111
  %150 = bitcast i32* %ndofB114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #8
  %151 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call115 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %151)
  %add116 = add nsw i32 %call115, 6
  store i32 %add116, i32* %ndofB114, align 4, !tbaa !6
  %152 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians117 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %152, i32 0, i32 0
  %153 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacBindex118 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %153, i32 0, i32 7
  %154 = load i32, i32* %m_jacBindex118, align 4, !tbaa !37
  %call119 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians117, i32 %154)
  store float* %call119, float** %jacB, align 4, !tbaa !2
  %155 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse120 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %155, i32 0, i32 1
  %156 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacBindex121 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %156, i32 0, i32 7
  %157 = load i32, i32* %m_jacBindex121, align 4, !tbaa !37
  %call122 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse120, i32 %157)
  store float* %call122, float** %lambdaB, align 4, !tbaa !2
  %158 = bitcast i32* %i123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #8
  store i32 0, i32* %i123, align 4, !tbaa !6
  br label %for.cond124

for.cond124:                                      ; preds = %for.inc134, %if.then113
  %159 = load i32, i32* %i123, align 4, !tbaa !6
  %160 = load i32, i32* %ndofB114, align 4, !tbaa !6
  %cmp125 = icmp slt i32 %159, %160
  br i1 %cmp125, label %for.body127, label %for.cond.cleanup126

for.cond.cleanup126:                              ; preds = %for.cond124
  %161 = bitcast i32* %i123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #8
  br label %for.end136

for.body127:                                      ; preds = %for.cond124
  %162 = bitcast float* %j128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #8
  %163 = load float*, float** %jacB, align 4, !tbaa !2
  %164 = load i32, i32* %i123, align 4, !tbaa !6
  %arrayidx129 = getelementptr inbounds float, float* %163, i32 %164
  %165 = load float, float* %arrayidx129, align 4, !tbaa !27
  store float %165, float* %j128, align 4, !tbaa !27
  %166 = bitcast float* %l130 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %166) #8
  %167 = load float*, float** %lambdaB, align 4, !tbaa !2
  %168 = load i32, i32* %i123, align 4, !tbaa !6
  %arrayidx131 = getelementptr inbounds float, float* %167, i32 %168
  %169 = load float, float* %arrayidx131, align 4, !tbaa !27
  store float %169, float* %l130, align 4, !tbaa !27
  %170 = load float, float* %j128, align 4, !tbaa !27
  %171 = load float, float* %l130, align 4, !tbaa !27
  %mul132 = fmul float %170, %171
  %172 = load float, float* %denom1, align 4, !tbaa !27
  %add133 = fadd float %172, %mul132
  store float %add133, float* %denom1, align 4, !tbaa !27
  %173 = bitcast float* %l130 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #8
  %174 = bitcast float* %j128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #8
  br label %for.inc134

for.inc134:                                       ; preds = %for.body127
  %175 = load i32, i32* %i123, align 4, !tbaa !6
  %inc135 = add nsw i32 %175, 1
  store i32 %inc135, i32* %i123, align 4, !tbaa !6
  br label %for.cond124

for.end136:                                       ; preds = %for.cond.cleanup126
  %176 = bitcast i32* %ndofB114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #8
  br label %if.end137

if.end137:                                        ; preds = %for.end136, %if.end111
  %177 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool138 = icmp ne %class.btMultiBody* %177, null
  br i1 %tobool138, label %land.lhs.true, label %if.end157

land.lhs.true:                                    ; preds = %if.end137
  %178 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %179 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %cmp139 = icmp eq %class.btMultiBody* %178, %179
  br i1 %cmp139, label %if.then140, label %if.end157

if.then140:                                       ; preds = %land.lhs.true
  %180 = bitcast i32* %i141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %180) #8
  store i32 0, i32* %i141, align 4, !tbaa !6
  br label %for.cond142

for.cond142:                                      ; preds = %for.inc154, %if.then140
  %181 = load i32, i32* %i141, align 4, !tbaa !6
  %182 = load i32, i32* %ndofA89, align 4, !tbaa !6
  %cmp143 = icmp slt i32 %181, %182
  br i1 %cmp143, label %for.body145, label %for.cond.cleanup144

for.cond.cleanup144:                              ; preds = %for.cond142
  %183 = bitcast i32* %i141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #8
  br label %for.end156

for.body145:                                      ; preds = %for.cond142
  %184 = load float*, float** %jacB, align 4, !tbaa !2
  %185 = load i32, i32* %i141, align 4, !tbaa !6
  %arrayidx146 = getelementptr inbounds float, float* %184, i32 %185
  %186 = load float, float* %arrayidx146, align 4, !tbaa !27
  %187 = load float*, float** %lambdaA, align 4, !tbaa !2
  %188 = load i32, i32* %i141, align 4, !tbaa !6
  %arrayidx147 = getelementptr inbounds float, float* %187, i32 %188
  %189 = load float, float* %arrayidx147, align 4, !tbaa !27
  %mul148 = fmul float %186, %189
  %190 = load float, float* %denom1, align 4, !tbaa !27
  %add149 = fadd float %190, %mul148
  store float %add149, float* %denom1, align 4, !tbaa !27
  %191 = load float*, float** %jacA, align 4, !tbaa !2
  %192 = load i32, i32* %i141, align 4, !tbaa !6
  %arrayidx150 = getelementptr inbounds float, float* %191, i32 %192
  %193 = load float, float* %arrayidx150, align 4, !tbaa !27
  %194 = load float*, float** %lambdaB, align 4, !tbaa !2
  %195 = load i32, i32* %i141, align 4, !tbaa !6
  %arrayidx151 = getelementptr inbounds float, float* %194, i32 %195
  %196 = load float, float* %arrayidx151, align 4, !tbaa !27
  %mul152 = fmul float %193, %196
  %197 = load float, float* %denom1, align 4, !tbaa !27
  %add153 = fadd float %197, %mul152
  store float %add153, float* %denom1, align 4, !tbaa !27
  br label %for.inc154

for.inc154:                                       ; preds = %for.body145
  %198 = load i32, i32* %i141, align 4, !tbaa !6
  %inc155 = add nsw i32 %198, 1
  store i32 %inc155, i32* %i141, align 4, !tbaa !6
  br label %for.cond142

for.end156:                                       ; preds = %for.cond.cleanup144
  br label %if.end157

if.end157:                                        ; preds = %for.end156, %land.lhs.true, %if.end137
  %199 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %199) #8
  %200 = load float, float* %denom0, align 4, !tbaa !27
  %201 = load float, float* %denom1, align 4, !tbaa !27
  %add158 = fadd float %200, %201
  store float %add158, float* %d, align 4, !tbaa !27
  %202 = load float, float* %d, align 4, !tbaa !27
  %call159 = call float @_Z6btFabsf(float %202)
  %cmp160 = fcmp ogt float %call159, 0x3E80000000000000
  br i1 %cmp160, label %if.then161, label %if.else162

if.then161:                                       ; preds = %if.end157
  %203 = load float, float* %d, align 4, !tbaa !27
  %div = fdiv float 1.000000e+00, %203
  %204 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %204, i32 0, i32 13
  store float %div, float* %m_jacDiagABInv, align 4, !tbaa !38
  br label %if.end164

if.else162:                                       ; preds = %if.end157
  %205 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacDiagABInv163 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %205, i32 0, i32 13
  store float 1.000000e+00, float* %m_jacDiagABInv163, align 4, !tbaa !38
  br label %if.end164

if.end164:                                        ; preds = %if.else162, %if.then161
  %206 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #8
  %207 = bitcast i32* %ndofA89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #8
  %208 = bitcast float** %lambdaB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #8
  %209 = bitcast float** %lambdaA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #8
  %210 = bitcast float** %jacA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #8
  %211 = bitcast float** %jacB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #8
  %212 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #8
  %213 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #8
  %214 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %214) #8
  %215 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %215) #8
  store float 0.000000e+00, float* %rel_vel, align 4, !tbaa !27
  %216 = bitcast i32* %ndofA165 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %216) #8
  store i32 0, i32* %ndofA165, align 4, !tbaa !6
  %217 = bitcast i32* %ndofB166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %217) #8
  store i32 0, i32* %ndofB166, align 4, !tbaa !6
  %218 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %218) #8
  %call167 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %219 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %219) #8
  %call168 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %220 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool169 = icmp ne %class.btMultiBody* %220, null
  br i1 %tobool169, label %if.then170, label %if.end190

if.then170:                                       ; preds = %if.end164
  %221 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call171 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %221)
  %add172 = add nsw i32 %call171, 6
  store i32 %add172, i32* %ndofA165, align 4, !tbaa !6
  %222 = bitcast float** %jacA173 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %222) #8
  %223 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians174 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %223, i32 0, i32 0
  %224 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacAindex175 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %224, i32 0, i32 3
  %225 = load i32, i32* %m_jacAindex175, align 4, !tbaa !35
  %call176 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians174, i32 %225)
  store float* %call176, float** %jacA173, align 4, !tbaa !2
  %226 = bitcast i32* %i177 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %226) #8
  store i32 0, i32* %i177, align 4, !tbaa !6
  br label %for.cond178

for.cond178:                                      ; preds = %for.inc187, %if.then170
  %227 = load i32, i32* %i177, align 4, !tbaa !6
  %228 = load i32, i32* %ndofA165, align 4, !tbaa !6
  %cmp179 = icmp slt i32 %227, %228
  br i1 %cmp179, label %for.body181, label %for.cond.cleanup180

for.cond.cleanup180:                              ; preds = %for.cond178
  %229 = bitcast i32* %i177 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #8
  br label %for.end189

for.body181:                                      ; preds = %for.cond178
  %230 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call182 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %230)
  %231 = load i32, i32* %i177, align 4, !tbaa !6
  %arrayidx183 = getelementptr inbounds float, float* %call182, i32 %231
  %232 = load float, float* %arrayidx183, align 4, !tbaa !27
  %233 = load float*, float** %jacA173, align 4, !tbaa !2
  %234 = load i32, i32* %i177, align 4, !tbaa !6
  %arrayidx184 = getelementptr inbounds float, float* %233, i32 %234
  %235 = load float, float* %arrayidx184, align 4, !tbaa !27
  %mul185 = fmul float %232, %235
  %236 = load float, float* %rel_vel, align 4, !tbaa !27
  %add186 = fadd float %236, %mul185
  store float %add186, float* %rel_vel, align 4, !tbaa !27
  br label %for.inc187

for.inc187:                                       ; preds = %for.body181
  %237 = load i32, i32* %i177, align 4, !tbaa !6
  %inc188 = add nsw i32 %237, 1
  store i32 %inc188, i32* %i177, align 4, !tbaa !6
  br label %for.cond178

for.end189:                                       ; preds = %for.cond.cleanup180
  %238 = bitcast float** %jacA173 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #8
  br label %if.end190

if.end190:                                        ; preds = %for.end189, %if.end164
  %239 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool191 = icmp ne %class.btMultiBody* %239, null
  br i1 %tobool191, label %if.then192, label %if.end212

if.then192:                                       ; preds = %if.end190
  %240 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call193 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %240)
  %add194 = add nsw i32 %call193, 6
  store i32 %add194, i32* %ndofB166, align 4, !tbaa !6
  %241 = bitcast float** %jacB195 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %241) #8
  %242 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians196 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %242, i32 0, i32 0
  %243 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacBindex197 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %243, i32 0, i32 7
  %244 = load i32, i32* %m_jacBindex197, align 4, !tbaa !37
  %call198 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians196, i32 %244)
  store float* %call198, float** %jacB195, align 4, !tbaa !2
  %245 = bitcast i32* %i199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %245) #8
  store i32 0, i32* %i199, align 4, !tbaa !6
  br label %for.cond200

for.cond200:                                      ; preds = %for.inc209, %if.then192
  %246 = load i32, i32* %i199, align 4, !tbaa !6
  %247 = load i32, i32* %ndofB166, align 4, !tbaa !6
  %cmp201 = icmp slt i32 %246, %247
  br i1 %cmp201, label %for.body203, label %for.cond.cleanup202

for.cond.cleanup202:                              ; preds = %for.cond200
  %248 = bitcast i32* %i199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #8
  br label %for.end211

for.body203:                                      ; preds = %for.cond200
  %249 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call204 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %249)
  %250 = load i32, i32* %i199, align 4, !tbaa !6
  %arrayidx205 = getelementptr inbounds float, float* %call204, i32 %250
  %251 = load float, float* %arrayidx205, align 4, !tbaa !27
  %252 = load float*, float** %jacB195, align 4, !tbaa !2
  %253 = load i32, i32* %i199, align 4, !tbaa !6
  %arrayidx206 = getelementptr inbounds float, float* %252, i32 %253
  %254 = load float, float* %arrayidx206, align 4, !tbaa !27
  %mul207 = fmul float %251, %254
  %255 = load float, float* %rel_vel, align 4, !tbaa !27
  %add208 = fadd float %255, %mul207
  store float %add208, float* %rel_vel, align 4, !tbaa !27
  br label %for.inc209

for.inc209:                                       ; preds = %for.body203
  %256 = load i32, i32* %i199, align 4, !tbaa !6
  %inc210 = add nsw i32 %256, 1
  store i32 %inc210, i32* %i199, align 4, !tbaa !6
  br label %for.cond200

for.end211:                                       ; preds = %for.cond.cleanup202
  %257 = bitcast float** %jacB195 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #8
  br label %if.end212

if.end212:                                        ; preds = %for.end211, %if.end190
  %258 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %258, i32 0, i32 12
  store float 0.000000e+00, float* %m_friction, align 4, !tbaa !39
  %259 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %259, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !40
  %260 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %260, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4, !tbaa !41
  %261 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %261) #8
  %262 = load float, float* %desiredVelocity.addr, align 4, !tbaa !27
  %263 = load float, float* %rel_vel, align 4, !tbaa !27
  %sub = fsub float %262, %263
  store float %sub, float* %velocityError, align 4, !tbaa !27
  %264 = bitcast float* %erp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %264) #8
  %265 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %266 = bitcast %struct.btContactSolverInfo* %265 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %266, i32 0, i32 9
  %267 = load float, float* %m_erp2, align 4, !tbaa !42
  store float %267, float* %erp, align 4, !tbaa !27
  %268 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %268) #8
  %269 = load float, float* %velocityError, align 4, !tbaa !27
  %270 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_jacDiagABInv213 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %270, i32 0, i32 13
  %271 = load float, float* %m_jacDiagABInv213, align 4, !tbaa !38
  %mul214 = fmul float %269, %271
  store float %mul214, float* %velocityImpulse, align 4, !tbaa !27
  %272 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %273 = bitcast %struct.btContactSolverInfo* %272 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %273, i32 0, i32 11
  %274 = load i32, i32* %m_splitImpulse, align 4, !tbaa !44
  %tobool215 = icmp ne i32 %274, 0
  br i1 %tobool215, label %if.else217, label %if.then216

if.then216:                                       ; preds = %if.end212
  %275 = load float, float* %velocityImpulse, align 4, !tbaa !27
  %276 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %276, i32 0, i32 14
  store float %275, float* %m_rhs, align 4, !tbaa !45
  %277 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %277, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4, !tbaa !46
  br label %if.end220

if.else217:                                       ; preds = %if.end212
  %278 = load float, float* %velocityImpulse, align 4, !tbaa !27
  %279 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_rhs218 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %279, i32 0, i32 14
  store float %278, float* %m_rhs218, align 4, !tbaa !45
  %280 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_rhsPenetration219 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %280, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration219, align 4, !tbaa !46
  br label %if.end220

if.end220:                                        ; preds = %if.else217, %if.then216
  %281 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %281, i32 0, i32 15
  store float 0.000000e+00, float* %m_cfm, align 4, !tbaa !47
  %282 = load float, float* %lowerLimit.addr, align 4, !tbaa !27
  %283 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %283, i32 0, i32 16
  store float %282, float* %m_lowerLimit, align 4, !tbaa !48
  %284 = load float, float* %upperLimit.addr, align 4, !tbaa !27
  %285 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %285, i32 0, i32 17
  store float %284, float* %m_upperLimit, align 4, !tbaa !49
  %286 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %286) #8
  %287 = bitcast float* %erp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #8
  %288 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #8
  %289 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %289) #8
  %290 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %290) #8
  %291 = load float, float* %rel_vel, align 4, !tbaa !27
  %292 = bitcast i32* %ndofB166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #8
  %293 = bitcast i32* %ndofA165 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #8
  %294 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #8
  %295 = bitcast %class.btMultiBody** %multiBodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %295) #8
  %296 = bitcast %class.btMultiBody** %multiBodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #8
  ret float %291
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 21
  %0 = load i32, i32* %m_companionId, align 4, !tbaa !50
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !29
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %this, i32 %id) #3 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %id.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %id, i32* %id.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4, !tbaa !6
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 21
  store i32 %0, i32* %m_companionId, align 4, !tbaa !50
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !28
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

declare void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody*, float*, float*, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17)) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #5 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !27
  %0 = load float, float* %x.addr, align 4, !tbaa !27
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

define linkonce_odr hidden float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_real_buf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_real_buf, i32 0)
  ret float* %call
}

define hidden void @_ZN21btMultiBodyConstraint13applyDeltaVeeER23btMultiBodyJacobianDataPffii(%class.btMultiBodyConstraint* %this, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, float* %delta_vee, float %impulse, i32 %velocityIndex, i32 %ndof) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %delta_vee.addr = alloca float*, align 4
  %impulse.addr = alloca float, align 4
  %velocityIndex.addr = alloca i32, align 4
  %ndof.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  store float* %delta_vee, float** %delta_vee.addr, align 4, !tbaa !2
  store float %impulse, float* %impulse.addr, align 4, !tbaa !27
  store i32 %velocityIndex, i32* %velocityIndex.addr, align 4, !tbaa !6
  store i32 %ndof, i32* %ndof.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %ndof.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %delta_vee.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4, !tbaa !27
  %7 = load float, float* %impulse.addr, align 4, !tbaa !27
  %mul = fmul float %6, %7
  %8 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %8, i32 0, i32 2
  %9 = load i32, i32* %velocityIndex.addr, align 4, !tbaa !6
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %9, %10
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocities, i32 %add)
  %11 = load float, float* %call, align 4, !tbaa !27
  %add2 = fadd float %11, %mul
  store float %add2, float* %call, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define hidden void @_ZN21btMultiBodyConstraint28fillMultiBodyConstraintMixedER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataRK9btVector3S6_S6_fRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraint* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %solverConstraint, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, %class.btVector3* nonnull align 4 dereferenceable(16) %contactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %posAworld, %class.btVector3* nonnull align 4 dereferenceable(16) %posBworld, float %position, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, float* nonnull align 4 dereferenceable(4) %relaxation, i1 zeroext %isFriction, float %desiredVelocity, float %cfmSlip) #0 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %solverConstraint.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %contactNormalOnB.addr = alloca %class.btVector3*, align 4
  %posAworld.addr = alloca %class.btVector3*, align 4
  %posBworld.addr = alloca %class.btVector3*, align 4
  %position.addr = alloca float, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %relaxation.addr = alloca float*, align 4
  %isFriction.addr = alloca i8, align 1
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %multiBodyA = alloca %class.btMultiBody*, align 4
  %multiBodyB = alloca %class.btMultiBody*, align 4
  %pos1 = alloca %class.btVector3*, align 4
  %pos2 = alloca %class.btVector3*, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ndofA = alloca i32, align 4
  %ref.tmp46 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %jac1 = alloca float*, align 4
  %delta = alloca float*, align 4
  %torqueAxis0 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp74 = alloca %class.btVector3, align 4
  %ref.tmp78 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ndofB = alloca i32, align 4
  %ref.tmp100 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp115 = alloca %class.btVector3, align 4
  %torqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp131 = alloca %class.btVector3, align 4
  %ref.tmp134 = alloca %class.btVector3, align 4
  %ref.tmp136 = alloca %class.btVector3, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp144 = alloca %class.btVector3, align 4
  %ref.tmp145 = alloca %class.btVector3, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %jacB = alloca float*, align 4
  %jacA = alloca float*, align 4
  %lambdaA = alloca float*, align 4
  %lambdaB = alloca float*, align 4
  %ndofA148 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca float, align 4
  %l = alloca float, align 4
  %ref.tmp165 = alloca %class.btVector3, align 4
  %ndofB174 = alloca i32, align 4
  %i183 = alloca i32, align 4
  %j188 = alloca float, align 4
  %l190 = alloca float, align 4
  %ref.tmp200 = alloca %class.btVector3, align 4
  %ref.tmp201 = alloca %class.btVector3, align 4
  %i211 = alloca i32, align 4
  %d = alloca float, align 4
  %restitution = alloca float, align 4
  %penetration = alloca float, align 4
  %rel_vel = alloca float, align 4
  %ndofA241 = alloca i32, align 4
  %ndofB242 = alloca i32, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %jacA249 = alloca float*, align 4
  %i253 = alloca i32, align 4
  %ref.tmp269 = alloca %class.btVector3, align 4
  %jacB279 = alloca float*, align 4
  %i283 = alloca i32, align 4
  %ref.tmp299 = alloca %class.btVector3, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %erp = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %solverConstraint, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  store %class.btVector3* %contactNormalOnB, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  store %class.btVector3* %posAworld, %class.btVector3** %posAworld.addr, align 4, !tbaa !2
  store %class.btVector3* %posBworld, %class.btVector3** %posBworld.addr, align 4, !tbaa !2
  store float %position, float* %position.addr, align 4, !tbaa !27
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store float* %relaxation, float** %relaxation.addr, align 4, !tbaa !2
  %frombool = zext i1 %isFriction to i8
  store i8 %frombool, i8* %isFriction.addr, align 1, !tbaa !8
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4, !tbaa !27
  store float %cfmSlip, float* %cfmSlip.addr, align 4, !tbaa !27
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %posAworld.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %rel_pos1 to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !62
  %4 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %posBworld.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %rel_pos2 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !62
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 1
  %8 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4, !tbaa !12
  %9 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %9, i32 0, i32 23
  store %class.btMultiBody* %8, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !30
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 2
  %10 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4, !tbaa !17
  %11 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %11, i32 0, i32 26
  store %class.btMultiBody* %10, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !33
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 3
  %12 = load i32, i32* %m_linkA, align 4, !tbaa !18
  %13 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_linkA2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %13, i32 0, i32 24
  store i32 %12, i32* %m_linkA2, align 4, !tbaa !64
  %m_linkB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 4
  %14 = load i32, i32* %m_linkB, align 4, !tbaa !19
  %15 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_linkB3 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %15, i32 0, i32 27
  store i32 %14, i32* %m_linkB3, align 4, !tbaa !65
  %16 = bitcast %class.btMultiBody** %multiBodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_multiBodyA4 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %17, i32 0, i32 23
  %18 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA4, align 4, !tbaa !30
  store %class.btMultiBody* %18, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %19 = bitcast %class.btMultiBody** %multiBodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_multiBodyB5 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %20, i32 0, i32 26
  %21 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB5, align 4, !tbaa !33
  store %class.btMultiBody* %21, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %22 = bitcast %class.btVector3** %pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btVector3*, %class.btVector3** %posAworld.addr, align 4, !tbaa !2
  store %class.btVector3* %23, %class.btVector3** %pos1, align 4, !tbaa !2
  %24 = bitcast %class.btVector3** %pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load %class.btVector3*, %class.btVector3** %posBworld.addr, align 4, !tbaa !2
  store %class.btVector3* %25, %class.btVector3** %pos2, align 4, !tbaa !2
  %26 = bitcast %struct.btSolverBody** %bodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBody* %27, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %28 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_solverBodyPool = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %28, i32 0, i32 6
  %29 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %m_solverBodyPool, align 4, !tbaa !66
  %30 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %30, i32 0, i32 22
  %31 = load i32, i32* %m_solverBodyIdA, align 4, !tbaa !68
  %call = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi(%class.btAlignedObjectArray.16* %29, i32 %31)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btSolverBody* [ null, %cond.true ], [ %call, %cond.false ]
  store %struct.btSolverBody* %cond, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %32 = bitcast %struct.btSolverBody** %bodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  %33 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool6 = icmp ne %class.btMultiBody* %33, null
  br i1 %tobool6, label %cond.true7, label %cond.false8

cond.true7:                                       ; preds = %cond.end
  br label %cond.end11

cond.false8:                                      ; preds = %cond.end
  %34 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_solverBodyPool9 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %34, i32 0, i32 6
  %35 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %m_solverBodyPool9, align 4, !tbaa !66
  %36 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %36, i32 0, i32 25
  %37 = load i32, i32* %m_solverBodyIdB, align 4, !tbaa !69
  %call10 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi(%class.btAlignedObjectArray.16* %35, i32 %37)
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false8, %cond.true7
  %cond12 = phi %struct.btSolverBody* [ null, %cond.true7 ], [ %call10, %cond.false8 ]
  store %struct.btSolverBody* %cond12, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %38 = bitcast %class.btRigidBody** %rb0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #8
  %39 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool13 = icmp ne %class.btMultiBody* %39, null
  br i1 %tobool13, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %cond.end11
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end11
  %40 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %40, i32 0, i32 12
  %41 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !70
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true14
  %cond17 = phi %class.btRigidBody* [ null, %cond.true14 ], [ %41, %cond.false15 ]
  store %class.btRigidBody* %cond17, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %42 = bitcast %class.btRigidBody** %rb1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #8
  %43 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool18 = icmp ne %class.btMultiBody* %43, null
  br i1 %tobool18, label %cond.true19, label %cond.false20

cond.true19:                                      ; preds = %cond.end16
  br label %cond.end22

cond.false20:                                     ; preds = %cond.end16
  %44 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %m_originalBody21 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %44, i32 0, i32 12
  %45 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody21, align 4, !tbaa !70
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false20, %cond.true19
  %cond23 = phi %class.btRigidBody* [ null, %cond.true19 ], [ %45, %cond.false20 ]
  store %class.btRigidBody* %cond23, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %46 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %tobool24 = icmp ne %struct.btSolverBody* %46, null
  br i1 %tobool24, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end22
  %47 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #8
  %48 = load %class.btVector3*, %class.btVector3** %pos1, align 4, !tbaa !2
  %49 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %49)
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call25)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %48, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %50 = bitcast %class.btVector3* %rel_pos1 to i8*
  %51 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false), !tbaa.struct !62
  %52 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end22
  %53 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %tobool27 = icmp ne %struct.btSolverBody* %53, null
  br i1 %tobool27, label %if.then28, label %if.end32

if.then28:                                        ; preds = %if.end
  %54 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #8
  %55 = load %class.btVector3*, %class.btVector3** %pos2, align 4, !tbaa !2
  %56 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4, !tbaa !2
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %56)
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call30)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %55, %class.btVector3* nonnull align 4 dereferenceable(16) %call31)
  %57 = bitcast %class.btVector3* %rel_pos2 to i8*
  %58 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 16, i1 false), !tbaa.struct !62
  %59 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #8
  br label %if.end32

if.end32:                                         ; preds = %if.then28, %if.end
  %60 = load float*, float** %relaxation.addr, align 4, !tbaa !2
  store float 1.000000e+00, float* %60, align 4, !tbaa !27
  %61 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool33 = icmp ne %class.btMultiBody* %61, null
  br i1 %tobool33, label %if.then34, label %if.else70

if.then34:                                        ; preds = %if.end32
  %62 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #8
  %63 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call35 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %63)
  %add = add nsw i32 %call35, 6
  store i32 %add, i32* %ndofA, align 4, !tbaa !6
  %64 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call36 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %64)
  %65 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %65, i32 0, i32 0
  store i32 %call36, i32* %m_deltaVelAindex, align 4, !tbaa !34
  %66 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex37 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %66, i32 0, i32 0
  %67 = load i32, i32* %m_deltaVelAindex37, align 4, !tbaa !34
  %cmp = icmp slt i32 %67, 0
  br i1 %cmp, label %if.then38, label %if.else

if.then38:                                        ; preds = %if.then34
  %68 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %68, i32 0, i32 2
  %call39 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities)
  %69 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex40 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %69, i32 0, i32 0
  store i32 %call39, i32* %m_deltaVelAindex40, align 4, !tbaa !34
  %70 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %71 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelAindex41 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %71, i32 0, i32 0
  %72 = load i32, i32* %m_deltaVelAindex41, align 4, !tbaa !34
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %70, i32 %72)
  %73 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities42 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %73, i32 0, i32 2
  %74 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities43 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %74, i32 0, i32 2
  %call44 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities43)
  %75 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add45 = add nsw i32 %call44, %75
  %76 = bitcast float* %ref.tmp46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #8
  store float 0.000000e+00, float* %ref.tmp46, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocities42, i32 %add45, float* nonnull align 4 dereferenceable(4) %ref.tmp46)
  %77 = bitcast float* %ref.tmp46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  br label %if.end47

if.else:                                          ; preds = %if.then34
  br label %if.end47

if.end47:                                         ; preds = %if.else, %if.then38
  %78 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %78, i32 0, i32 0
  %call48 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians)
  %79 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %79, i32 0, i32 3
  store i32 %call48, i32* %m_jacAindex, align 4, !tbaa !35
  %80 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians49 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %80, i32 0, i32 0
  %81 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians50 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %81, i32 0, i32 0
  %call51 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians50)
  %82 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add52 = add nsw i32 %call51, %82
  %83 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #8
  store float 0.000000e+00, float* %ref.tmp53, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_jacobians49, i32 %add52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %84 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #8
  %85 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %85, i32 0, i32 1
  %86 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse54 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %86, i32 0, i32 1
  %call55 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse54)
  %87 = load i32, i32* %ndofA, align 4, !tbaa !6
  %add56 = add nsw i32 %call55, %87
  %88 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #8
  store float 0.000000e+00, float* %ref.tmp57, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse, i32 %add56, float* nonnull align 4 dereferenceable(4) %ref.tmp57)
  %89 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = bitcast float** %jac1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #8
  %91 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians58 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %91, i32 0, i32 0
  %92 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex59 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %92, i32 0, i32 3
  %93 = load i32, i32* %m_jacAindex59, align 4, !tbaa !35
  %call60 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians58, i32 %93)
  store float* %call60, float** %jac1, align 4, !tbaa !2
  %94 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %95 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_linkA61 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %95, i32 0, i32 24
  %96 = load i32, i32* %m_linkA61, align 4, !tbaa !64
  %97 = load %class.btVector3*, %class.btVector3** %posAworld.addr, align 4, !tbaa !2
  %98 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  %99 = load float*, float** %jac1, align 4, !tbaa !2
  %100 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %100, i32 0, i32 3
  %101 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %101, i32 0, i32 4
  %102 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_m = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %102, i32 0, i32 5
  call void @_ZNK11btMultiBody19fillContactJacobianEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %94, i32 %96, %class.btVector3* nonnull align 4 dereferenceable(16) %97, %class.btVector3* nonnull align 4 dereferenceable(16) %98, float* %99, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %scratch_m)
  %103 = bitcast float** %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #8
  %104 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse62 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %104, i32 0, i32 1
  %105 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex63 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %105, i32 0, i32 3
  %106 = load i32, i32* %m_jacAindex63, align 4, !tbaa !35
  %call64 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse62, i32 %106)
  store float* %call64, float** %delta, align 4, !tbaa !2
  %107 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %108 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians65 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %108, i32 0, i32 0
  %109 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex66 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %109, i32 0, i32 3
  %110 = load i32, i32* %m_jacAindex66, align 4, !tbaa !35
  %call67 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians65, i32 %110)
  %111 = load float*, float** %delta, align 4, !tbaa !2
  %112 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_r68 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %112, i32 0, i32 3
  %113 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_v69 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %113, i32 0, i32 4
  call void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %107, float* %call67, float* %111, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r68, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v69)
  %114 = bitcast float** %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #8
  %115 = bitcast float** %jac1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #8
  %116 = bitcast i32* %ndofA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #8
  br label %if.end83

if.else70:                                        ; preds = %if.end32
  %117 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %117) #8
  %118 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis0, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %118)
  %119 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %119) #8
  %120 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %tobool72 = icmp ne %class.btRigidBody* %120, null
  %121 = bitcast %class.btVector3* %ref.tmp74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %121) #8
  %122 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #8
  %123 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #8
  %124 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #8
  br i1 %tobool72, label %cond.true73, label %cond.false77

cond.true73:                                      ; preds = %if.else70
  %125 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %call75 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %125)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp74, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call75, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis0)
  %126 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %call76 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %126)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp74, %class.btVector3* nonnull align 4 dereferenceable(16) %call76)
  br label %cond.end82

cond.false77:                                     ; preds = %if.else70
  store float 0.000000e+00, float* %ref.tmp78, align 4, !tbaa !27
  store float 0.000000e+00, float* %ref.tmp79, align 4, !tbaa !27
  store float 0.000000e+00, float* %ref.tmp80, align 4, !tbaa !27
  %call81 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp78, float* nonnull align 4 dereferenceable(4) %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80)
  br label %cond.end82

cond.end82:                                       ; preds = %cond.false77, %cond.true73
  %127 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %127, i32 0, i32 8
  %128 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %129 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %128, i8* align 4 %129, i32 16, i1 false), !tbaa.struct !62
  %130 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #8
  %131 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #8
  %132 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #8
  %133 = bitcast %class.btVector3* %ref.tmp74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %133) #8
  %134 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %134) #8
  %135 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %135, i32 0, i32 1
  %136 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %137 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %136, i8* align 4 %137, i32 16, i1 false), !tbaa.struct !62
  %138 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  %139 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %139, i32 0, i32 2
  %140 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %141 = bitcast %class.btVector3* %138 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %140, i8* align 4 %141, i32 16, i1 false), !tbaa.struct !62
  %142 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %142) #8
  br label %if.end83

if.end83:                                         ; preds = %cond.end82, %if.end47
  %143 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool84 = icmp ne %class.btMultiBody* %143, null
  br i1 %tobool84, label %if.then85, label %if.else130

if.then85:                                        ; preds = %if.end83
  %144 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #8
  %145 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call86 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %145)
  %add87 = add nsw i32 %call86, 6
  store i32 %add87, i32* %ndofB, align 4, !tbaa !6
  %146 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call88 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %146)
  %147 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %147, i32 0, i32 4
  store i32 %call88, i32* %m_deltaVelBindex, align 4, !tbaa !36
  %148 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex89 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %148, i32 0, i32 4
  %149 = load i32, i32* %m_deltaVelBindex89, align 4, !tbaa !36
  %cmp90 = icmp slt i32 %149, 0
  br i1 %cmp90, label %if.then91, label %if.end101

if.then91:                                        ; preds = %if.then85
  %150 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities92 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %150, i32 0, i32 2
  %call93 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities92)
  %151 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex94 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %151, i32 0, i32 4
  store i32 %call93, i32* %m_deltaVelBindex94, align 4, !tbaa !36
  %152 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %153 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_deltaVelBindex95 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %153, i32 0, i32 4
  %154 = load i32, i32* %m_deltaVelBindex95, align 4, !tbaa !36
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %152, i32 %154)
  %155 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities96 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %155, i32 0, i32 2
  %156 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocities97 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %156, i32 0, i32 2
  %call98 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities97)
  %157 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add99 = add nsw i32 %call98, %157
  %158 = bitcast float* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #8
  store float 0.000000e+00, float* %ref.tmp100, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocities96, i32 %add99, float* nonnull align 4 dereferenceable(4) %ref.tmp100)
  %159 = bitcast float* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #8
  br label %if.end101

if.end101:                                        ; preds = %if.then91, %if.then85
  %160 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians102 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %160, i32 0, i32 0
  %call103 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians102)
  %161 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %161, i32 0, i32 7
  store i32 %call103, i32* %m_jacBindex, align 4, !tbaa !37
  %162 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians104 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %162, i32 0, i32 0
  %163 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians105 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %163, i32 0, i32 0
  %call106 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians105)
  %164 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add107 = add nsw i32 %call106, %164
  %165 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #8
  store float 0.000000e+00, float* %ref.tmp108, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_jacobians104, i32 %add107, float* nonnull align 4 dereferenceable(4) %ref.tmp108)
  %166 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #8
  %167 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse109 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %167, i32 0, i32 1
  %168 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse110 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %168, i32 0, i32 1
  %call111 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse110)
  %169 = load i32, i32* %ndofB, align 4, !tbaa !6
  %add112 = add nsw i32 %call111, %169
  %170 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #8
  store float 0.000000e+00, float* %ref.tmp113, align 4, !tbaa !27
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse109, i32 %add112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  %171 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #8
  %172 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %173 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_linkB114 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %173, i32 0, i32 27
  %174 = load i32, i32* %m_linkB114, align 4, !tbaa !65
  %175 = load %class.btVector3*, %class.btVector3** %posBworld.addr, align 4, !tbaa !2
  %176 = bitcast %class.btVector3* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %176) #8
  %177 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp115, %class.btVector3* nonnull align 4 dereferenceable(16) %177)
  %178 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians116 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %178, i32 0, i32 0
  %179 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex117 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %179, i32 0, i32 7
  %180 = load i32, i32* %m_jacBindex117, align 4, !tbaa !37
  %call118 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians116, i32 %180)
  %181 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_r119 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %181, i32 0, i32 3
  %182 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_v120 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %182, i32 0, i32 4
  %183 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_m121 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %183, i32 0, i32 5
  call void @_ZNK11btMultiBody19fillContactJacobianEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %172, i32 %174, %class.btVector3* nonnull align 4 dereferenceable(16) %175, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp115, float* %call118, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r119, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v120, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %scratch_m121)
  %184 = bitcast %class.btVector3* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %184) #8
  %185 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %186 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians122 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %186, i32 0, i32 0
  %187 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex123 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %187, i32 0, i32 7
  %188 = load i32, i32* %m_jacBindex123, align 4, !tbaa !37
  %call124 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians122, i32 %188)
  %189 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse125 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %189, i32 0, i32 1
  %190 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex126 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %190, i32 0, i32 7
  %191 = load i32, i32* %m_jacBindex126, align 4, !tbaa !37
  %call127 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse125, i32 %191)
  %192 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_r128 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %192, i32 0, i32 3
  %193 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %scratch_v129 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %193, i32 0, i32 4
  call void @_ZNK11btMultiBody22calcAccelerationDeltasEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %185, float* %call124, float* %call127, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r128, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v129)
  %194 = bitcast i32* %ndofB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #8
  br label %if.end146

if.else130:                                       ; preds = %if.end83
  %195 = bitcast %class.btVector3* %torqueAxis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %195) #8
  %196 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis1, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %196)
  %197 = bitcast %class.btVector3* %ref.tmp131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %197) #8
  %198 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %tobool132 = icmp ne %class.btRigidBody* %198, null
  %199 = bitcast %class.btVector3* %ref.tmp134 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %199) #8
  %200 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %200) #8
  %201 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %201) #8
  %202 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #8
  %203 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %203) #8
  br i1 %tobool132, label %cond.true133, label %cond.false138

cond.true133:                                     ; preds = %if.else130
  %204 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %call135 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %204)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp136, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp134, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call135, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp136)
  %205 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %call137 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %205)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp131, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp134, %class.btVector3* nonnull align 4 dereferenceable(16) %call137)
  br label %cond.end143

cond.false138:                                    ; preds = %if.else130
  store float 0.000000e+00, float* %ref.tmp139, align 4, !tbaa !27
  store float 0.000000e+00, float* %ref.tmp140, align 4, !tbaa !27
  store float 0.000000e+00, float* %ref.tmp141, align 4, !tbaa !27
  %call142 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp131, float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %ref.tmp141)
  br label %cond.end143

cond.end143:                                      ; preds = %cond.false138, %cond.true133
  %206 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %206, i32 0, i32 9
  %207 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %208 = bitcast %class.btVector3* %ref.tmp131 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %207, i8* align 4 %208, i32 16, i1 false), !tbaa.struct !62
  %209 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #8
  %210 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #8
  %211 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #8
  %212 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %212) #8
  %213 = bitcast %class.btVector3* %ref.tmp134 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %213) #8
  %214 = bitcast %class.btVector3* %ref.tmp131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %214) #8
  %215 = bitcast %class.btVector3* %ref.tmp144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %215) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp144, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  %216 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %216, i32 0, i32 5
  %217 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %218 = bitcast %class.btVector3* %ref.tmp144 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %217, i8* align 4 %218, i32 16, i1 false), !tbaa.struct !62
  %219 = bitcast %class.btVector3* %ref.tmp144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %219) #8
  %220 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %220) #8
  %221 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp145, %class.btVector3* nonnull align 4 dereferenceable(16) %221)
  %222 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %222, i32 0, i32 6
  %223 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %224 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %223, i8* align 4 %224, i32 16, i1 false), !tbaa.struct !62
  %225 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %225) #8
  %226 = bitcast %class.btVector3* %torqueAxis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %226) #8
  br label %if.end146

if.end146:                                        ; preds = %cond.end143, %if.end101
  %227 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %227) #8
  %call147 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  %228 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %228) #8
  store float 0.000000e+00, float* %denom0, align 4, !tbaa !27
  %229 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %229) #8
  store float 0.000000e+00, float* %denom1, align 4, !tbaa !27
  %230 = bitcast float** %jacB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %230) #8
  store float* null, float** %jacB, align 4, !tbaa !2
  %231 = bitcast float** %jacA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %231) #8
  store float* null, float** %jacA, align 4, !tbaa !2
  %232 = bitcast float** %lambdaA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %232) #8
  store float* null, float** %lambdaA, align 4, !tbaa !2
  %233 = bitcast float** %lambdaB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %233) #8
  store float* null, float** %lambdaB, align 4, !tbaa !2
  %234 = bitcast i32* %ndofA148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %234) #8
  store i32 0, i32* %ndofA148, align 4, !tbaa !6
  %235 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool149 = icmp ne %class.btMultiBody* %235, null
  br i1 %tobool149, label %if.then150, label %if.else162

if.then150:                                       ; preds = %if.end146
  %236 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call151 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %236)
  %add152 = add nsw i32 %call151, 6
  store i32 %add152, i32* %ndofA148, align 4, !tbaa !6
  %237 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians153 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %237, i32 0, i32 0
  %238 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex154 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %238, i32 0, i32 3
  %239 = load i32, i32* %m_jacAindex154, align 4, !tbaa !35
  %call155 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians153, i32 %239)
  store float* %call155, float** %jacA, align 4, !tbaa !2
  %240 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse156 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %240, i32 0, i32 1
  %241 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex157 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %241, i32 0, i32 3
  %242 = load i32, i32* %m_jacAindex157, align 4, !tbaa !35
  %call158 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse156, i32 %242)
  store float* %call158, float** %lambdaA, align 4, !tbaa !2
  %243 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %243) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then150
  %244 = load i32, i32* %i, align 4, !tbaa !6
  %245 = load i32, i32* %ndofA148, align 4, !tbaa !6
  %cmp159 = icmp slt i32 %244, %245
  br i1 %cmp159, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %246 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %247 = bitcast float* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %247) #8
  %248 = load float*, float** %jacA, align 4, !tbaa !2
  %249 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %248, i32 %249
  %250 = load float, float* %arrayidx, align 4, !tbaa !27
  store float %250, float* %j, align 4, !tbaa !27
  %251 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #8
  %252 = load float*, float** %lambdaA, align 4, !tbaa !2
  %253 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx160 = getelementptr inbounds float, float* %252, i32 %253
  %254 = load float, float* %arrayidx160, align 4, !tbaa !27
  store float %254, float* %l, align 4, !tbaa !27
  %255 = load float, float* %j, align 4, !tbaa !27
  %256 = load float, float* %l, align 4, !tbaa !27
  %mul = fmul float %255, %256
  %257 = load float, float* %denom0, align 4, !tbaa !27
  %add161 = fadd float %257, %mul
  store float %add161, float* %denom0, align 4, !tbaa !27
  %258 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #8
  %259 = bitcast float* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %260 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %260, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end171

if.else162:                                       ; preds = %if.end146
  %261 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %tobool163 = icmp ne %class.btRigidBody* %261, null
  br i1 %tobool163, label %if.then164, label %if.end170

if.then164:                                       ; preds = %if.else162
  %262 = bitcast %class.btVector3* %ref.tmp165 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %262) #8
  %263 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentA166 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %263, i32 0, i32 8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp165, %class.btVector3* %m_angularComponentA166, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %264 = bitcast %class.btVector3* %vec to i8*
  %265 = bitcast %class.btVector3* %ref.tmp165 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %264, i8* align 4 %265, i32 16, i1 false), !tbaa.struct !62
  %266 = bitcast %class.btVector3* %ref.tmp165 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %266) #8
  %267 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %call167 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %267)
  %268 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  %call168 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %268, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add169 = fadd float %call167, %call168
  store float %add169, float* %denom0, align 4, !tbaa !27
  br label %if.end170

if.end170:                                        ; preds = %if.then164, %if.else162
  br label %if.end171

if.end171:                                        ; preds = %if.end170, %for.end
  %269 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool172 = icmp ne %class.btMultiBody* %269, null
  br i1 %tobool172, label %if.then173, label %if.else197

if.then173:                                       ; preds = %if.end171
  %270 = bitcast i32* %ndofB174 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %270) #8
  %271 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call175 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %271)
  %add176 = add nsw i32 %call175, 6
  store i32 %add176, i32* %ndofB174, align 4, !tbaa !6
  %272 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians177 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %272, i32 0, i32 0
  %273 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex178 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %273, i32 0, i32 7
  %274 = load i32, i32* %m_jacBindex178, align 4, !tbaa !37
  %call179 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians177, i32 %274)
  store float* %call179, float** %jacB, align 4, !tbaa !2
  %275 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_deltaVelocitiesUnitImpulse180 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %275, i32 0, i32 1
  %276 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex181 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %276, i32 0, i32 7
  %277 = load i32, i32* %m_jacBindex181, align 4, !tbaa !37
  %call182 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse180, i32 %277)
  store float* %call182, float** %lambdaB, align 4, !tbaa !2
  %278 = bitcast i32* %i183 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %278) #8
  store i32 0, i32* %i183, align 4, !tbaa !6
  br label %for.cond184

for.cond184:                                      ; preds = %for.inc194, %if.then173
  %279 = load i32, i32* %i183, align 4, !tbaa !6
  %280 = load i32, i32* %ndofB174, align 4, !tbaa !6
  %cmp185 = icmp slt i32 %279, %280
  br i1 %cmp185, label %for.body187, label %for.cond.cleanup186

for.cond.cleanup186:                              ; preds = %for.cond184
  %281 = bitcast i32* %i183 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #8
  br label %for.end196

for.body187:                                      ; preds = %for.cond184
  %282 = bitcast float* %j188 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %282) #8
  %283 = load float*, float** %jacB, align 4, !tbaa !2
  %284 = load i32, i32* %i183, align 4, !tbaa !6
  %arrayidx189 = getelementptr inbounds float, float* %283, i32 %284
  %285 = load float, float* %arrayidx189, align 4, !tbaa !27
  store float %285, float* %j188, align 4, !tbaa !27
  %286 = bitcast float* %l190 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %286) #8
  %287 = load float*, float** %lambdaB, align 4, !tbaa !2
  %288 = load i32, i32* %i183, align 4, !tbaa !6
  %arrayidx191 = getelementptr inbounds float, float* %287, i32 %288
  %289 = load float, float* %arrayidx191, align 4, !tbaa !27
  store float %289, float* %l190, align 4, !tbaa !27
  %290 = load float, float* %j188, align 4, !tbaa !27
  %291 = load float, float* %l190, align 4, !tbaa !27
  %mul192 = fmul float %290, %291
  %292 = load float, float* %denom1, align 4, !tbaa !27
  %add193 = fadd float %292, %mul192
  store float %add193, float* %denom1, align 4, !tbaa !27
  %293 = bitcast float* %l190 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #8
  %294 = bitcast float* %j188 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #8
  br label %for.inc194

for.inc194:                                       ; preds = %for.body187
  %295 = load i32, i32* %i183, align 4, !tbaa !6
  %inc195 = add nsw i32 %295, 1
  store i32 %inc195, i32* %i183, align 4, !tbaa !6
  br label %for.cond184

for.end196:                                       ; preds = %for.cond.cleanup186
  %296 = bitcast i32* %ndofB174 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #8
  br label %if.end207

if.else197:                                       ; preds = %if.end171
  %297 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %tobool198 = icmp ne %class.btRigidBody* %297, null
  br i1 %tobool198, label %if.then199, label %if.end206

if.then199:                                       ; preds = %if.else197
  %298 = bitcast %class.btVector3* %ref.tmp200 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %298) #8
  %299 = bitcast %class.btVector3* %ref.tmp201 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %299) #8
  %300 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_angularComponentB202 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %300, i32 0, i32 9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp201, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB202)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp200, %class.btVector3* %ref.tmp201, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %301 = bitcast %class.btVector3* %vec to i8*
  %302 = bitcast %class.btVector3* %ref.tmp200 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %301, i8* align 4 %302, i32 16, i1 false), !tbaa.struct !62
  %303 = bitcast %class.btVector3* %ref.tmp201 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %303) #8
  %304 = bitcast %class.btVector3* %ref.tmp200 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %304) #8
  %305 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %call203 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %305)
  %306 = load %class.btVector3*, %class.btVector3** %contactNormalOnB.addr, align 4, !tbaa !2
  %call204 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %306, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add205 = fadd float %call203, %call204
  store float %add205, float* %denom1, align 4, !tbaa !27
  br label %if.end206

if.end206:                                        ; preds = %if.then199, %if.else197
  br label %if.end207

if.end207:                                        ; preds = %if.end206, %for.end196
  %307 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool208 = icmp ne %class.btMultiBody* %307, null
  br i1 %tobool208, label %land.lhs.true, label %if.end227

land.lhs.true:                                    ; preds = %if.end207
  %308 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %309 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %cmp209 = icmp eq %class.btMultiBody* %308, %309
  br i1 %cmp209, label %if.then210, label %if.end227

if.then210:                                       ; preds = %land.lhs.true
  %310 = bitcast i32* %i211 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %310) #8
  store i32 0, i32* %i211, align 4, !tbaa !6
  br label %for.cond212

for.cond212:                                      ; preds = %for.inc224, %if.then210
  %311 = load i32, i32* %i211, align 4, !tbaa !6
  %312 = load i32, i32* %ndofA148, align 4, !tbaa !6
  %cmp213 = icmp slt i32 %311, %312
  br i1 %cmp213, label %for.body215, label %for.cond.cleanup214

for.cond.cleanup214:                              ; preds = %for.cond212
  %313 = bitcast i32* %i211 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #8
  br label %for.end226

for.body215:                                      ; preds = %for.cond212
  %314 = load float*, float** %jacB, align 4, !tbaa !2
  %315 = load i32, i32* %i211, align 4, !tbaa !6
  %arrayidx216 = getelementptr inbounds float, float* %314, i32 %315
  %316 = load float, float* %arrayidx216, align 4, !tbaa !27
  %317 = load float*, float** %lambdaA, align 4, !tbaa !2
  %318 = load i32, i32* %i211, align 4, !tbaa !6
  %arrayidx217 = getelementptr inbounds float, float* %317, i32 %318
  %319 = load float, float* %arrayidx217, align 4, !tbaa !27
  %mul218 = fmul float %316, %319
  %320 = load float, float* %denom1, align 4, !tbaa !27
  %add219 = fadd float %320, %mul218
  store float %add219, float* %denom1, align 4, !tbaa !27
  %321 = load float*, float** %jacA, align 4, !tbaa !2
  %322 = load i32, i32* %i211, align 4, !tbaa !6
  %arrayidx220 = getelementptr inbounds float, float* %321, i32 %322
  %323 = load float, float* %arrayidx220, align 4, !tbaa !27
  %324 = load float*, float** %lambdaB, align 4, !tbaa !2
  %325 = load i32, i32* %i211, align 4, !tbaa !6
  %arrayidx221 = getelementptr inbounds float, float* %324, i32 %325
  %326 = load float, float* %arrayidx221, align 4, !tbaa !27
  %mul222 = fmul float %323, %326
  %327 = load float, float* %denom1, align 4, !tbaa !27
  %add223 = fadd float %327, %mul222
  store float %add223, float* %denom1, align 4, !tbaa !27
  br label %for.inc224

for.inc224:                                       ; preds = %for.body215
  %328 = load i32, i32* %i211, align 4, !tbaa !6
  %inc225 = add nsw i32 %328, 1
  store i32 %inc225, i32* %i211, align 4, !tbaa !6
  br label %for.cond212

for.end226:                                       ; preds = %for.cond.cleanup214
  br label %if.end227

if.end227:                                        ; preds = %for.end226, %land.lhs.true, %if.end207
  %329 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %329) #8
  %330 = load float, float* %denom0, align 4, !tbaa !27
  %331 = load float, float* %denom1, align 4, !tbaa !27
  %add228 = fadd float %330, %331
  store float %add228, float* %d, align 4, !tbaa !27
  %332 = load float, float* %d, align 4, !tbaa !27
  %call229 = call float @_Z6btFabsf(float %332)
  %cmp230 = fcmp ogt float %call229, 0x3E80000000000000
  br i1 %cmp230, label %if.then231, label %if.else232

if.then231:                                       ; preds = %if.end227
  %333 = load float*, float** %relaxation.addr, align 4, !tbaa !2
  %334 = load float, float* %333, align 4, !tbaa !27
  %335 = load float, float* %d, align 4, !tbaa !27
  %div = fdiv float %334, %335
  %336 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %336, i32 0, i32 13
  store float %div, float* %m_jacDiagABInv, align 4, !tbaa !38
  br label %if.end234

if.else232:                                       ; preds = %if.end227
  %337 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv233 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %337, i32 0, i32 13
  store float 1.000000e+00, float* %m_jacDiagABInv233, align 4, !tbaa !38
  br label %if.end234

if.end234:                                        ; preds = %if.else232, %if.then231
  %338 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #8
  %339 = bitcast i32* %ndofA148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %339) #8
  %340 = bitcast float** %lambdaB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #8
  %341 = bitcast float** %lambdaA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #8
  %342 = bitcast float** %jacA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #8
  %343 = bitcast float** %jacB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %343) #8
  %344 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %344) #8
  %345 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %345) #8
  %346 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %346) #8
  %347 = bitcast float* %restitution to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %347) #8
  store float 0.000000e+00, float* %restitution, align 4, !tbaa !27
  %348 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %348) #8
  %349 = load i8, i8* %isFriction.addr, align 1, !tbaa !8, !range !21
  %tobool235 = trunc i8 %349 to i1
  br i1 %tobool235, label %cond.true236, label %cond.false237

cond.true236:                                     ; preds = %if.end234
  br label %cond.end239

cond.false237:                                    ; preds = %if.end234
  %350 = load float, float* %position.addr, align 4, !tbaa !27
  %351 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %352 = bitcast %struct.btContactSolverInfo* %351 to %struct.btContactSolverInfoData*
  %m_linearSlop = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %352, i32 0, i32 14
  %353 = load float, float* %m_linearSlop, align 4, !tbaa !73
  %add238 = fadd float %350, %353
  br label %cond.end239

cond.end239:                                      ; preds = %cond.false237, %cond.true236
  %cond240 = phi float [ 0.000000e+00, %cond.true236 ], [ %add238, %cond.false237 ]
  store float %cond240, float* %penetration, align 4, !tbaa !27
  %354 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %354) #8
  store float 0.000000e+00, float* %rel_vel, align 4, !tbaa !27
  %355 = bitcast i32* %ndofA241 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %355) #8
  store i32 0, i32* %ndofA241, align 4, !tbaa !6
  %356 = bitcast i32* %ndofB242 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %356) #8
  store i32 0, i32* %ndofB242, align 4, !tbaa !6
  %357 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %357) #8
  %call243 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %358 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %358) #8
  %call244 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %359 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %tobool245 = icmp ne %class.btMultiBody* %359, null
  br i1 %tobool245, label %if.then246, label %if.else266

if.then246:                                       ; preds = %cond.end239
  %360 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call247 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %360)
  %add248 = add nsw i32 %call247, 6
  store i32 %add248, i32* %ndofA241, align 4, !tbaa !6
  %361 = bitcast float** %jacA249 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %361) #8
  %362 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians250 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %362, i32 0, i32 0
  %363 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacAindex251 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %363, i32 0, i32 3
  %364 = load i32, i32* %m_jacAindex251, align 4, !tbaa !35
  %call252 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians250, i32 %364)
  store float* %call252, float** %jacA249, align 4, !tbaa !2
  %365 = bitcast i32* %i253 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %365) #8
  store i32 0, i32* %i253, align 4, !tbaa !6
  br label %for.cond254

for.cond254:                                      ; preds = %for.inc263, %if.then246
  %366 = load i32, i32* %i253, align 4, !tbaa !6
  %367 = load i32, i32* %ndofA241, align 4, !tbaa !6
  %cmp255 = icmp slt i32 %366, %367
  br i1 %cmp255, label %for.body257, label %for.cond.cleanup256

for.cond.cleanup256:                              ; preds = %for.cond254
  %368 = bitcast i32* %i253 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %368) #8
  br label %for.end265

for.body257:                                      ; preds = %for.cond254
  %369 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4, !tbaa !2
  %call258 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %369)
  %370 = load i32, i32* %i253, align 4, !tbaa !6
  %arrayidx259 = getelementptr inbounds float, float* %call258, i32 %370
  %371 = load float, float* %arrayidx259, align 4, !tbaa !27
  %372 = load float*, float** %jacA249, align 4, !tbaa !2
  %373 = load i32, i32* %i253, align 4, !tbaa !6
  %arrayidx260 = getelementptr inbounds float, float* %372, i32 %373
  %374 = load float, float* %arrayidx260, align 4, !tbaa !27
  %mul261 = fmul float %371, %374
  %375 = load float, float* %rel_vel, align 4, !tbaa !27
  %add262 = fadd float %375, %mul261
  store float %add262, float* %rel_vel, align 4, !tbaa !27
  br label %for.inc263

for.inc263:                                       ; preds = %for.body257
  %376 = load i32, i32* %i253, align 4, !tbaa !6
  %inc264 = add nsw i32 %376, 1
  store i32 %inc264, i32* %i253, align 4, !tbaa !6
  br label %for.cond254

for.end265:                                       ; preds = %for.cond.cleanup256
  %377 = bitcast float** %jacA249 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %377) #8
  br label %if.end274

if.else266:                                       ; preds = %cond.end239
  %378 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  %tobool267 = icmp ne %class.btRigidBody* %378, null
  br i1 %tobool267, label %if.then268, label %if.end273

if.then268:                                       ; preds = %if.else266
  %379 = bitcast %class.btVector3* %ref.tmp269 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %379) #8
  %380 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp269, %class.btRigidBody* %380, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %381 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal1270 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %381, i32 0, i32 2
  %call271 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp269, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1270)
  %382 = load float, float* %rel_vel, align 4, !tbaa !27
  %add272 = fadd float %382, %call271
  store float %add272, float* %rel_vel, align 4, !tbaa !27
  %383 = bitcast %class.btVector3* %ref.tmp269 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %383) #8
  br label %if.end273

if.end273:                                        ; preds = %if.then268, %if.else266
  br label %if.end274

if.end274:                                        ; preds = %if.end273, %for.end265
  %384 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %tobool275 = icmp ne %class.btMultiBody* %384, null
  br i1 %tobool275, label %if.then276, label %if.else296

if.then276:                                       ; preds = %if.end274
  %385 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call277 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %385)
  %add278 = add nsw i32 %call277, 6
  store i32 %add278, i32* %ndofB242, align 4, !tbaa !6
  %386 = bitcast float** %jacB279 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %386) #8
  %387 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %m_jacobians280 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %387, i32 0, i32 0
  %388 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacBindex281 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %388, i32 0, i32 7
  %389 = load i32, i32* %m_jacBindex281, align 4, !tbaa !37
  %call282 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians280, i32 %389)
  store float* %call282, float** %jacB279, align 4, !tbaa !2
  %390 = bitcast i32* %i283 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %390) #8
  store i32 0, i32* %i283, align 4, !tbaa !6
  br label %for.cond284

for.cond284:                                      ; preds = %for.inc293, %if.then276
  %391 = load i32, i32* %i283, align 4, !tbaa !6
  %392 = load i32, i32* %ndofB242, align 4, !tbaa !6
  %cmp285 = icmp slt i32 %391, %392
  br i1 %cmp285, label %for.body287, label %for.cond.cleanup286

for.cond.cleanup286:                              ; preds = %for.cond284
  %393 = bitcast i32* %i283 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #8
  br label %for.end295

for.body287:                                      ; preds = %for.cond284
  %394 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4, !tbaa !2
  %call288 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %394)
  %395 = load i32, i32* %i283, align 4, !tbaa !6
  %arrayidx289 = getelementptr inbounds float, float* %call288, i32 %395
  %396 = load float, float* %arrayidx289, align 4, !tbaa !27
  %397 = load float*, float** %jacB279, align 4, !tbaa !2
  %398 = load i32, i32* %i283, align 4, !tbaa !6
  %arrayidx290 = getelementptr inbounds float, float* %397, i32 %398
  %399 = load float, float* %arrayidx290, align 4, !tbaa !27
  %mul291 = fmul float %396, %399
  %400 = load float, float* %rel_vel, align 4, !tbaa !27
  %add292 = fadd float %400, %mul291
  store float %add292, float* %rel_vel, align 4, !tbaa !27
  br label %for.inc293

for.inc293:                                       ; preds = %for.body287
  %401 = load i32, i32* %i283, align 4, !tbaa !6
  %inc294 = add nsw i32 %401, 1
  store i32 %inc294, i32* %i283, align 4, !tbaa !6
  br label %for.cond284

for.end295:                                       ; preds = %for.cond.cleanup286
  %402 = bitcast float** %jacB279 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %402) #8
  br label %if.end304

if.else296:                                       ; preds = %if.end274
  %403 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  %tobool297 = icmp ne %class.btRigidBody* %403, null
  br i1 %tobool297, label %if.then298, label %if.end303

if.then298:                                       ; preds = %if.else296
  %404 = bitcast %class.btVector3* %ref.tmp299 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %404) #8
  %405 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4, !tbaa !2
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp299, %class.btRigidBody* %405, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %406 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_contactNormal2300 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %406, i32 0, i32 6
  %call301 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp299, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2300)
  %407 = load float, float* %rel_vel, align 4, !tbaa !27
  %add302 = fadd float %407, %call301
  store float %add302, float* %rel_vel, align 4, !tbaa !27
  %408 = bitcast %class.btVector3* %ref.tmp299 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %408) #8
  br label %if.end303

if.end303:                                        ; preds = %if.then298, %if.else296
  br label %if.end304

if.end304:                                        ; preds = %if.end303, %for.end295
  %409 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %409, i32 0, i32 12
  store float 0.000000e+00, float* %m_friction, align 4, !tbaa !39
  %410 = load float, float* %restitution, align 4, !tbaa !27
  %411 = load float, float* %rel_vel, align 4, !tbaa !27
  %fneg = fneg float %411
  %mul305 = fmul float %410, %fneg
  store float %mul305, float* %restitution, align 4, !tbaa !27
  %412 = load float, float* %restitution, align 4, !tbaa !27
  %cmp306 = fcmp ole float %412, 0.000000e+00
  br i1 %cmp306, label %if.then307, label %if.end308

if.then307:                                       ; preds = %if.end304
  store float 0.000000e+00, float* %restitution, align 4, !tbaa !27
  br label %if.end308

if.end308:                                        ; preds = %if.then307, %if.end304
  %413 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %413) #8
  %414 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %414) #8
  %415 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %415, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse, align 4, !tbaa !40
  %416 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %416, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4, !tbaa !41
  %417 = bitcast float* %positionalError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %417) #8
  store float 0.000000e+00, float* %positionalError, align 4, !tbaa !27
  %418 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %418) #8
  %419 = load float, float* %restitution, align 4, !tbaa !27
  %420 = load float, float* %rel_vel, align 4, !tbaa !27
  %sub = fsub float %419, %420
  store float %sub, float* %velocityError, align 4, !tbaa !27
  %421 = bitcast float* %erp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %421) #8
  %422 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %423 = bitcast %struct.btContactSolverInfo* %422 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %423, i32 0, i32 9
  %424 = load float, float* %m_erp2, align 4, !tbaa !42
  store float %424, float* %erp, align 4, !tbaa !27
  %425 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %426 = bitcast %struct.btContactSolverInfo* %425 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %426, i32 0, i32 11
  %427 = load i32, i32* %m_splitImpulse, align 4, !tbaa !44
  %tobool309 = icmp ne i32 %427, 0
  br i1 %tobool309, label %lor.lhs.false, label %if.then311

lor.lhs.false:                                    ; preds = %if.end308
  %428 = load float, float* %penetration, align 4, !tbaa !27
  %429 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %430 = bitcast %struct.btContactSolverInfo* %429 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %430, i32 0, i32 12
  %431 = load float, float* %m_splitImpulsePenetrationThreshold, align 4, !tbaa !74
  %cmp310 = fcmp ogt float %428, %431
  br i1 %cmp310, label %if.then311, label %if.end312

if.then311:                                       ; preds = %lor.lhs.false, %if.end308
  %432 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %433 = bitcast %struct.btContactSolverInfo* %432 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %433, i32 0, i32 8
  %434 = load float, float* %m_erp, align 4, !tbaa !75
  store float %434, float* %erp, align 4, !tbaa !27
  br label %if.end312

if.end312:                                        ; preds = %if.then311, %lor.lhs.false
  %435 = load float, float* %penetration, align 4, !tbaa !27
  %cmp313 = fcmp ogt float %435, 0.000000e+00
  br i1 %cmp313, label %if.then314, label %if.else317

if.then314:                                       ; preds = %if.end312
  store float 0.000000e+00, float* %positionalError, align 4, !tbaa !27
  %436 = load float, float* %penetration, align 4, !tbaa !27
  %fneg315 = fneg float %436
  %437 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %438 = bitcast %struct.btContactSolverInfo* %437 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %438, i32 0, i32 3
  %439 = load float, float* %m_timeStep, align 4, !tbaa !76
  %div316 = fdiv float %fneg315, %439
  store float %div316, float* %velocityError, align 4, !tbaa !27
  br label %if.end322

if.else317:                                       ; preds = %if.end312
  %440 = load float, float* %penetration, align 4, !tbaa !27
  %fneg318 = fneg float %440
  %441 = load float, float* %erp, align 4, !tbaa !27
  %mul319 = fmul float %fneg318, %441
  %442 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %443 = bitcast %struct.btContactSolverInfo* %442 to %struct.btContactSolverInfoData*
  %m_timeStep320 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %443, i32 0, i32 3
  %444 = load float, float* %m_timeStep320, align 4, !tbaa !76
  %div321 = fdiv float %mul319, %444
  store float %div321, float* %positionalError, align 4, !tbaa !27
  br label %if.end322

if.end322:                                        ; preds = %if.else317, %if.then314
  %445 = bitcast float* %penetrationImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %445) #8
  %446 = load float, float* %positionalError, align 4, !tbaa !27
  %447 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv323 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %447, i32 0, i32 13
  %448 = load float, float* %m_jacDiagABInv323, align 4, !tbaa !38
  %mul324 = fmul float %446, %448
  store float %mul324, float* %penetrationImpulse, align 4, !tbaa !27
  %449 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %449) #8
  %450 = load float, float* %velocityError, align 4, !tbaa !27
  %451 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_jacDiagABInv325 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %451, i32 0, i32 13
  %452 = load float, float* %m_jacDiagABInv325, align 4, !tbaa !38
  %mul326 = fmul float %450, %452
  store float %mul326, float* %velocityImpulse, align 4, !tbaa !27
  %453 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %454 = bitcast %struct.btContactSolverInfo* %453 to %struct.btContactSolverInfoData*
  %m_splitImpulse327 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %454, i32 0, i32 11
  %455 = load i32, i32* %m_splitImpulse327, align 4, !tbaa !44
  %tobool328 = icmp ne i32 %455, 0
  br i1 %tobool328, label %lor.lhs.false329, label %if.then332

lor.lhs.false329:                                 ; preds = %if.end322
  %456 = load float, float* %penetration, align 4, !tbaa !27
  %457 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %458 = bitcast %struct.btContactSolverInfo* %457 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold330 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %458, i32 0, i32 12
  %459 = load float, float* %m_splitImpulsePenetrationThreshold330, align 4, !tbaa !74
  %cmp331 = fcmp ogt float %456, %459
  br i1 %cmp331, label %if.then332, label %if.else334

if.then332:                                       ; preds = %lor.lhs.false329, %if.end322
  %460 = load float, float* %penetrationImpulse, align 4, !tbaa !27
  %461 = load float, float* %velocityImpulse, align 4, !tbaa !27
  %add333 = fadd float %460, %461
  %462 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %462, i32 0, i32 14
  store float %add333, float* %m_rhs, align 4, !tbaa !45
  %463 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %463, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4, !tbaa !46
  br label %if.end337

if.else334:                                       ; preds = %lor.lhs.false329
  %464 = load float, float* %velocityImpulse, align 4, !tbaa !27
  %465 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhs335 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %465, i32 0, i32 14
  store float %464, float* %m_rhs335, align 4, !tbaa !45
  %466 = load float, float* %penetrationImpulse, align 4, !tbaa !27
  %467 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_rhsPenetration336 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %467, i32 0, i32 18
  store float %466, float* %m_rhsPenetration336, align 4, !tbaa !46
  br label %if.end337

if.end337:                                        ; preds = %if.else334, %if.then332
  %468 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %468, i32 0, i32 15
  store float 0.000000e+00, float* %m_cfm, align 4, !tbaa !47
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 10
  %469 = load float, float* %m_maxAppliedImpulse, align 4, !tbaa !23
  %fneg338 = fneg float %469
  %470 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %470, i32 0, i32 16
  store float %fneg338, float* %m_lowerLimit, align 4, !tbaa !48
  %m_maxAppliedImpulse339 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 10
  %471 = load float, float* %m_maxAppliedImpulse339, align 4, !tbaa !23
  %472 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4, !tbaa !2
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %472, i32 0, i32 17
  store float %471, float* %m_upperLimit, align 4, !tbaa !49
  %473 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %473) #8
  %474 = bitcast float* %penetrationImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %474) #8
  %475 = bitcast float* %erp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %475) #8
  %476 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %476) #8
  %477 = bitcast float* %positionalError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %477) #8
  %478 = bitcast i32* %ndofB242 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %478) #8
  %479 = bitcast i32* %ndofA241 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %479) #8
  %480 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %480) #8
  %481 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %481) #8
  %482 = bitcast float* %restitution to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %482) #8
  %483 = bitcast %class.btRigidBody** %rb1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %483) #8
  %484 = bitcast %class.btRigidBody** %rb0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %484) #8
  %485 = bitcast %struct.btSolverBody** %bodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %485) #8
  %486 = bitcast %struct.btSolverBody** %bodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %486) #8
  %487 = bitcast %class.btVector3** %pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %487) #8
  %488 = bitcast %class.btVector3** %pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %488) #8
  %489 = bitcast %class.btMultiBody** %multiBodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %489) #8
  %490 = bitcast %class.btMultiBody** %multiBodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %490) #8
  %491 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %491) #8
  %492 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %492) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi(%class.btAlignedObjectArray.16* %this, i32 %n) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4, !tbaa !77
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !27
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !27
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !27
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !27
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !27
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !27
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !27
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

declare void @_ZNK11btMultiBody19fillContactJacobianEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float*, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17)) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !27
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !27
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !27
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !27
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !27
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !27
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !27
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !27
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !27
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !27
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !27
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !27
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !27
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !27
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !27
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !27
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !27
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !27
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !27
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !27
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !27
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !27
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !27
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !27
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  ret %class.btVector3* %m_angularFactor
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !27
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !27
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !27
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !27
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !27
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !27
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !27
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !27
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !27
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !27
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !27
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !80
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !27
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !27
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !27
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !27
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !27
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !27
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %1 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !84
  ret i32 %0
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !28
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !27
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !27
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !27
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !27
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !27
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !27
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !27
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !27
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !27
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !85
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4, !tbaa !28
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !29
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !86
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4, !tbaa !28
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !28
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !85, !range !21
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4, !tbaa !28
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4, !tbaa !28
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %this, float* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store float* %ptr, float** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float*
  store float* %3, float** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load float*, float** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, float* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !85
  %5 = load float*, float** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* %5, float** %m_data, align 4, !tbaa !28
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !86
  %7 = bitcast float** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !86
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, float* %dest) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store float* %dest, float** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = bitcast float* %arrayidx to i8*
  %7 = bitcast i8* %6 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !28
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !27
  store float %10, float* %7, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %this, i32 %n, float** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store float** %hint, float*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { cold noreturn nounwind }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"bool", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"vtable pointer", !5, i64 0}
!12 = !{!13, !3, i64 4}
!13 = !{!"_ZTS21btMultiBodyConstraint", !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !9, i64 36, !14, i64 40, !15, i64 44}
!14 = !{!"float", !4, i64 0}
!15 = !{!"_ZTS20btAlignedObjectArrayIfE", !16, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !9, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!17 = !{!13, !3, i64 8}
!18 = !{!13, !7, i64 12}
!19 = !{!13, !7, i64 16}
!20 = !{!13, !7, i64 20}
!21 = !{i8 0, i8 2}
!22 = !{!13, !9, i64 36}
!23 = !{!13, !14, i64 40}
!24 = !{!13, !7, i64 24}
!25 = !{!13, !7, i64 28}
!26 = !{!13, !7, i64 32}
!27 = !{!14, !14, i64 0}
!28 = !{!15, !3, i64 12}
!29 = !{!15, !7, i64 4}
!30 = !{!31, !3, i64 164}
!31 = !{!"_ZTS27btMultiBodySolverConstraint", !7, i64 0, !32, i64 4, !32, i64 20, !7, i64 36, !7, i64 40, !32, i64 44, !32, i64 60, !7, i64 76, !32, i64 80, !32, i64 96, !14, i64 112, !14, i64 116, !14, i64 120, !14, i64 124, !14, i64 128, !14, i64 132, !14, i64 136, !14, i64 140, !14, i64 144, !4, i64 148, !7, i64 152, !7, i64 156, !7, i64 160, !3, i64 164, !7, i64 168, !7, i64 172, !3, i64 176, !7, i64 180}
!32 = !{!"_ZTS9btVector3", !4, i64 0}
!33 = !{!31, !3, i64 176}
!34 = !{!31, !7, i64 0}
!35 = !{!31, !7, i64 36}
!36 = !{!31, !7, i64 40}
!37 = !{!31, !7, i64 76}
!38 = !{!31, !14, i64 124}
!39 = !{!31, !14, i64 120}
!40 = !{!31, !14, i64 116}
!41 = !{!31, !14, i64 112}
!42 = !{!43, !14, i64 36}
!43 = !{!"_ZTS23btContactSolverInfoData", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !7, i64 20, !14, i64 24, !14, i64 28, !14, i64 32, !14, i64 36, !14, i64 40, !7, i64 44, !14, i64 48, !14, i64 52, !14, i64 56, !14, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !14, i64 76, !14, i64 80}
!44 = !{!43, !7, i64 44}
!45 = !{!31, !14, i64 128}
!46 = !{!31, !14, i64 144}
!47 = !{!31, !14, i64 132}
!48 = !{!31, !14, i64 136}
!49 = !{!31, !14, i64 140}
!50 = !{!51, !7, i64 388}
!51 = !{!"_ZTS11btMultiBody", !3, i64 0, !32, i64 4, !52, i64 20, !14, i64 36, !32, i64 40, !32, i64 56, !32, i64 72, !53, i64 88, !55, i64 108, !15, i64 128, !57, i64 148, !59, i64 168, !61, i64 188, !61, i64 236, !61, i64 284, !61, i64 332, !9, i64 380, !9, i64 381, !9, i64 382, !14, i64 384, !7, i64 388, !14, i64 392, !14, i64 396, !9, i64 400, !14, i64 404, !9, i64 408}
!52 = !{!"_ZTS12btQuaternion"}
!53 = !{!"_ZTS20btAlignedObjectArrayI15btMultibodyLinkE", !54, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !9, i64 16}
!54 = !{!"_ZTS18btAlignedAllocatorI15btMultibodyLinkLj16EE"}
!55 = !{!"_ZTS20btAlignedObjectArrayIP23btMultiBodyLinkColliderE", !56, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !9, i64 16}
!56 = !{!"_ZTS18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE"}
!57 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !58, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !9, i64 16}
!58 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!59 = !{!"_ZTS20btAlignedObjectArrayI11btMatrix3x3E", !60, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !9, i64 16}
!60 = !{!"_ZTS18btAlignedAllocatorI11btMatrix3x3Lj16EE"}
!61 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!62 = !{i64 0, i64 16, !63}
!63 = !{!4, !4, i64 0}
!64 = !{!31, !7, i64 168}
!65 = !{!31, !7, i64 180}
!66 = !{!67, !3, i64 120}
!67 = !{!"_ZTS23btMultiBodyJacobianData", !15, i64 0, !15, i64 20, !15, i64 40, !15, i64 60, !57, i64 80, !59, i64 100, !3, i64 120, !7, i64 124}
!68 = !{!31, !7, i64 160}
!69 = !{!31, !7, i64 172}
!70 = !{!71, !3, i64 240}
!71 = !{!"_ZTS12btSolverBody", !72, i64 0, !32, i64 64, !32, i64 80, !32, i64 96, !32, i64 112, !32, i64 128, !32, i64 144, !32, i64 160, !32, i64 176, !32, i64 192, !32, i64 208, !32, i64 224, !3, i64 240}
!72 = !{!"_ZTS11btTransform", !61, i64 0, !32, i64 48}
!73 = !{!43, !14, i64 56}
!74 = !{!43, !14, i64 48}
!75 = !{!43, !14, i64 32}
!76 = !{!43, !14, i64 12}
!77 = !{!78, !3, i64 12}
!78 = !{!"_ZTS20btAlignedObjectArrayI12btSolverBodyE", !79, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !9, i64 16}
!79 = !{!"_ZTS18btAlignedAllocatorI12btSolverBodyLj16EE"}
!80 = !{!81, !14, i64 344}
!81 = !{!"_ZTS11btRigidBody", !61, i64 264, !32, i64 312, !32, i64 328, !14, i64 344, !32, i64 348, !32, i64 364, !32, i64 380, !32, i64 396, !32, i64 412, !32, i64 428, !14, i64 444, !14, i64 448, !9, i64 452, !14, i64 456, !14, i64 460, !14, i64 464, !14, i64 468, !14, i64 472, !14, i64 476, !3, i64 480, !82, i64 484, !7, i64 504, !7, i64 508, !32, i64 512, !32, i64 528, !32, i64 544, !32, i64 560, !32, i64 576, !32, i64 592, !7, i64 608, !7, i64 612}
!82 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !83, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !9, i64 16}
!83 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!84 = !{!53, !7, i64 4}
!85 = !{!15, !9, i64 16}
!86 = !{!15, !7, i64 8}
